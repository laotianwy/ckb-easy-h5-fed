/*
 * @Author: huajian
 * @Date: 2024-03-07 12:38:13
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-11 14:40:24
 * @Description:
 */
export const atomWebViewPageState: any = null;
export const atomUpdateGlobalWebview: any = null;
