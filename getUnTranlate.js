/*
 * @Author: shiguang
 * @Date: 2023-07-10 16:19:03
 * @LastEditors: shiguang
 * @LastEditTime: 2023-08-29 11:06:24
 * @Description: shiguang
 */
const data = require('./src/i18n/locales/ja_JP.json');

const getUnTranslateData = (data) => {
    return Object.entries(data).reduce((pre, cur) => {
        const [key, config] = cur;
        pre[key] = Object.entries(config).reduce((_pre, _cur) => {
            const [_key, _value] = _cur;
            if ('' === _value) {
                _pre[_key] = '';
            }
            return _pre;
        }, {});
        return pre;
    }, {});
};

const unTranslateData = getUnTranslateData(data);
const unTranslateWords = (() => {
    const objList = Object.values(unTranslateData).map((item) => {
        return Object.keys(item);
    });
    return objList.flat();
})();

if (unTranslateWords.length) {
    console.log(unTranslateData);
    console.log('存在未完成的翻译!!!!!');
    console.log(JSON.stringify(unTranslateWords));
    console.log('存在未完成的翻译!!!!!');
    process.exit(1);
}
