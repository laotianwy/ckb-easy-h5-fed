/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/**
 * AppAccountRespDTO
 * 转账
 */
export interface AppAccountRespDTO {
    /**
     * 账号类型：1-主账号；2-子账号；
     * @format int32
     * @example 1
     */
    accountType?: number;
    /**
     * 店铺ID
     * @format int64
     * @example 1
     */
    customerShopId?: number;
    /**
     * 店铺名称
     * @example "专业推广者"
     */
    customerShopName?: string;
    /**
     * 客户店铺平台: 0-其他; 1-Base; 2-Shopify; 3-Stores; 4-Amazon
     * @format int32
     * @example 1
     */
    customerShopPlatform?: number;
    /**
     * 登录名称
     * @example "dawn"
     */
    loginName?: string;
    /**
     * 来源：1-d2c；2-b2b；
     * @format int32
     * @example 1
     */
    systemSource?: number;
}

/**
 * AppChangeAmountReqDTO
 * 转账
 */
export interface AppChangeAmountReqDTO {
    /**
     * 账号类型：1-主账号；2-子账号；
     * @format int32
     * @example 1
     */
    accountType?: number;
    /**
     * 转账金额
     * @example 1.1
     */
    changeAmount?: string;
    /**
     * 店铺ID
     * @format int64
     * @example 1
     */
    customerShopId?: number;
    /**
     * 店铺名称
     * @example "专业推广者"
     */
    customerShopName?: string;
    /**
     * 客户店铺平台: 0-其他; 1-Base; 2-Shopify; 3-Stores; 4-Amazon
     * @format int32
     * @example 1
     */
    customerShopPlatform?: number;
    /**
     * 来源：1-d2c；2-b2b；
     * @format int32
     * @example 1
     */
    systemSource?: number;
}

/**
 * AppMemberQueryReqDTO
 * 会员查询
 */
export interface AppMemberQueryReqDTO {
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppMemberRespDTO
 * 关联用户
 */
export interface AppMemberRespDTO {
    /**
     * 注册时间
     * @example "2023-2-28 23:59:59"
     */
    memberCreateTime?: string;
    /**
     * 邮箱
     * @example "邮箱"
     */
    memberEmail?: string;
    /**
     * 当前会员等级 LEVEL1:经济舱 LEVEL2:标准舱 LEVEL3:商务舱 SUPER:头等舱
     * @example "LEVEL1"
     */
    memberLevel?: string;
    /**
     * 用户名
     * @example "用户名"
     */
    memberName?: string;
}

/**
 * AppMemberRewardPlanQueryReqDTO
 * 会员奖励-支付计划-查询
 */
export interface AppMemberRewardPlanQueryReqDTO {
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 被推广者用户名
     * @example "w123123-账户"
     */
    passiveLoginName?: string;
    /**
     * 支付计划状态 UNPAID:待发放 ALL:已发放 CANCEL:已取消
     * @example "UNPAID"
     */
    payPlanStatus?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppMemberRewardPlanRespDTO
 * 会员奖励-支付计划
 */
export interface AppMemberRewardPlanRespDTO {
    /**
     * 本期应发奖励金额（元）
     * @example "2023-2-28 23:59:59"
     */
    currentPayableAmount?: string;
    /**
     * 本期应发奖励金额（元）
     * @example "2023-2-28 23:59:59"
     */
    currentPayableAmountFormat?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 支付计划状态 UNPAID:待发放 ALL:已发放 CANCEL:已取消
     * @example "UNPAID"
     */
    payPlanStatus?: string;
    /**
     * 奖励发放状态 UNPAID:待发放 ALL:已发放 CANCEL:已取消
     * @example "待发放"
     */
    payPlanStatusDesc?: string;
    /**
     * 已发放时间
     * @example "2023-2-28 23:59:59"
     */
    payTime?: string;
    /**
     * 实际应发奖励（元）
     * @example "2023-2-28 23:59:59"
     */
    payableAmount?: string;
    /**
     * 实际应发奖励（元）
     * @example "2023-2-28 23:59:59"
     */
    payableAmountFormat?: string;
    /**
     * 期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 第2期(共2期)
     * @example "第2期(共2期)"
     */
    phaseNunDesc?: string;
    /**
     * 预计发放时间
     * @example "2023-2-28 23:59:59"
     */
    planPayTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 奖励结算结束时间
     * @example "2023-2-28 23:59:59"
     */
    settleEndTime?: string;
    /**
     * 奖励结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /**
     * 结算周期
     * @example "2023-2-1 00:00:00-2023-2-1 00:00:00"
     */
    settleRange?: string;
    /**
     * 奖励结算开始时间
     * @example "2023-2-1 00:00:00"
     */
    settleStartTime?: string;
    /**
     * 奖励结算时间
     * @example "2023-2-1 00:00:00"
     */
    settleTime?: string;
    /**
     * 总期数
     * @format int32
     * @example 2
     */
    totalPhaseNum?: number;
}

/**
 * AppMemberRewardQueryReqDTO
 * 成交会员-查询
 */
export interface AppMemberRewardQueryReqDTO {
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppMemberRewardRespDTO
 * 会员奖励-成交会员
 */
export interface AppMemberRewardRespDTO {
    /**
     * 用户邮箱
     * @example "test@163.com"
     */
    memberEmail?: string;
    /**
     * 会员用户ID
     * @format int64
     * @example 76582
     */
    memberId?: number;
    /**
     * 会员等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    memberLevel?: string;
    /**
     * 会员用户名
     * @example "w123123-账户"
     */
    memberName?: string;
    /**
     * 会员金额（元）
     * @example 1.1
     */
    memberOrderPrice?: string;
    /**
     * 购买时间
     * @example "2023-01-01 01:01:01"
     */
    memberPayTime?: string;
    /**
     * 对应奖励（元）
     * @example 1.1
     */
    settleAmount?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
}

/**
 * AppOrderRewardPlanQueryReqDTO
 * 订单奖励-支付计划-查询
 */
export interface AppOrderRewardPlanQueryReqDTO {
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 被推广者用户名
     * @example "w123123-账户"
     */
    passiveLoginName?: string;
    /**
     * 支付计划状态 UNPAID:待发放 ALL:已发放 CANCEL:已取消
     * @example "UNPAID"
     */
    payPlanStatus?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppOrderRewardPlanRespDTO
 * 订单奖励-支付计划
 */
export interface AppOrderRewardPlanRespDTO {
    /**
     * 本期应发奖励金额（元）
     * @example "2023-2-28 23:59:59"
     */
    currentPayableAmount?: string;
    /**
     * 本期应发奖励金额（元）
     * @example "2023-2-28 23:59:59"
     */
    currentPayableAmountFormat?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 支付计划状态 UNPAID:待发放 ALL:已发放 CANCEL:已取消
     * @example "UNPAID"
     */
    payPlanStatus?: string;
    /**
     * 奖励发放状态
     * @example "已发放"
     */
    payPlanStatusDesc?: string;
    /**
     * 已发放时间
     * @example "2023-2-28 23:59:59"
     */
    payTime?: string;
    /**
     * 实际应发奖励（元）
     * @example "2023-2-28 23:59:59"
     */
    payableAmount?: string;
    /**
     * 实际应发奖励（元）
     * @example "2023-2-28 23:59:59"
     */
    payableAmountFormat?: string;
    /**
     * 期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 第2期(共2期)
     * @example "第2期(共2期)"
     */
    phaseNunDesc?: string;
    /**
     * 预计发放时间
     * @example "2023-2-28 23:59:59"
     */
    planPayTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 奖励结算结束时间
     * @example "2023-2-28 23:59:59"
     */
    settleEndTime?: string;
    /**
     * 奖励结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /**
     * 结算周期
     * @example "2023-2-1 00:00:00-2023-2-1 00:00:00"
     */
    settleRange?: string;
    /**
     * 奖励结算开始时间
     * @example "2023-2-1 00:00:00"
     */
    settleStartTime?: string;
    /**
     * 奖励结算时间
     * @example "2023-2-1 00:00:00"
     */
    settleTime?: string;
    /**
     * 总期数
     * @format int32
     * @example 2
     */
    totalPhaseNum?: number;
}

/**
 * AppOrderRewardQueryReqDTO
 * 成交订单-查询
 */
export interface AppOrderRewardQueryReqDTO {
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppOrderRewardRespDTO
 * 订单奖励-成交订单
 */
export interface AppOrderRewardRespDTO {
    /**
     * 用户邮箱
     * @example "test@163.com"
     */
    memberEmail?: string;
    /**
     * 会员用户ID
     * @format int64
     * @example 76582
     */
    memberId?: number;
    /**
     * 会员等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    memberLevel?: string;
    /**
     * 会员用户名
     * @example "w123123-账户"
     */
    memberName?: string;
    /**
     * 订单金额（元）
     * @example 1.1
     */
    platformOrderPrice?: string;
    /**
     * 对应奖励（元）
     * @example 1.1
     */
    settleAmount?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
}

/**
 * AppPopReqDTO
 * 弹窗
 */
export interface AppPopReqDTO {
    /**
     * 弹窗类型 1:FIRST_LOGIN:首次登录弹窗 2:FIRST_REWARD:首次奖励弹窗
     * @example "FIRST_LOGIN"
     */
    popType?: string;
}

/**
 * AppProPromoRespDTO
 * 专业推广者
 */
export interface AppProPromoRespDTO {
    /**
     * 订单金额构成 LOGI_COST:国内运费 PRODUCT_AMOUNT:商品代金 REFUND_AMOUNT:售后退款
     * @example "LOGI_COST"
     */
    amountComposeList?: string[];
    /**
     * 计费方式 FEE_RATE:会费比例 FIXED_AMOUNT:固定金额
     * @example "FIXED_AMOUNT"
     */
    costType?: string;
    /**
     * 有效计费天数
     * @format int32
     * @example 1
     */
    costValidDays?: number;
    /**
     * 本周期结算周期
     * @example "11-1~11-31"
     */
    currentSettleRange?: string;
    /** 赠送规则 */
    handsel?: AppPromoRuleHandselRespDTO;
    /** 订单奖励阶梯 */
    ladderList?: AppPromoProRuleOrderLadderRespDTO[];
    /**
     * 一级会员
     * @example 1.1
     */
    level1?: string;
    /**
     * 二级会员
     * @example 1.1
     */
    level2?: string;
    /**
     * 三级会员
     * @example 1.1
     */
    level3?: string;
    /**
     * 上周期已发奖励（对应国家币种）
     * @example 1.1
     */
    paidAmount?: string;
    /**
     * 上周期已发奖励（人民币）
     * @example 1.1
     */
    paidAmountCny?: string;
    /** 专业推广规则-支付分期 */
    pay?: AppPromoProRulePayRespDTO;
    /**
     * 应发奖励（对应国家币种）
     * @example 1.1
     */
    payableAmount?: string;
    /**
     * 上周期应发奖励（人民币）
     * @example 1.1
     */
    payableAmountCny?: string;
    /**
     * 本周期预估奖励金额（对应国家币种）
     * @example 1.1
     */
    preAmount?: string;
    /**
     * 本周期预估奖励金额（人民币）
     * @example 1.1
     */
    preAmountCny?: string;
    /**
     * 奖励金额取值 MEMBER:会员奖励 ORDER:订单奖励 MEMBER_AND_ORDER:会员订单奖励之和 MAX_MEMBER_ORDER:会员订单奖励最大
     * @example "ORDER"
     */
    rewardAmountRange?: string;
    /**
     * 结算日
     * @format int32
     * @example 1
     */
    settleDay?: number;
    /**
     * 结算周期 NATURAL_MONTH:自然月月底结 FIXED_DAY:指定日月结
     * @example "REG_DAY_NUM"
     */
    settlePeriod?: string;
    /**
     * 上周期结算周期
     * @example "11-1~11-31"
     */
    settleRange?: string;
    /**
     * 结算周期
     * @example "11.1-11.30"
     */
    settleTimeRange?: string;
    /**
     * 专属会员
     * @example 1.1
     */
    specialLevel?: string;
    /**
     * 超级会员
     * @example 1.1
     */
    superLevel?: string;
}

/**
 * AppPromoProRuleOrderLadderRespDTO
 * 专业推广规则-订单奖励阶梯
 */
export interface AppPromoProRuleOrderLadderRespDTO {
    /**
     * 结束金额
     * @example 1.1
     */
    amountRangeEnd?: string;
    /**
     * 开始金额
     * @example 1.1
     */
    amountRangeStart?: string;
    /**
     * 奖励比例
     * @format double
     * @example 1.1
     */
    rewardRate?: number;
}

/**
 * AppPromoProRulePayRespDTO
 * 专业推广规则-支付分期
 */
export interface AppPromoProRulePayRespDTO {
    /**
     * 支付日期
     * @example 1
     */
    payDay?: string;
    /**
     * 支付奖励百分比
     * @example [1.1,1.2]
     */
    payRateList?: string[];
    /**
     * 分期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 分期类型 MONTH_1:月 MONTH_3:季度 MONTH_6:半年
     * @example "MONTH_1"
     */
    phaseType?: string;
}

/**
 * AppPromoPureRulePayRespDTO
 * 普通推广规则-支付分期
 */
export interface AppPromoPureRulePayRespDTO {
    /**
     * 支付奖励百分比
     * @example 1.1
     */
    payRate?: string;
    /**
     * 分期值
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 时间单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    timeUnit?: string;
    /**
     * 距离入会后天数
     * @example 1
     */
    timeValue?: string;
}

/**
 * AppPromoRuleHandselRespDTO
 * 推广规则-赠送
 */
export interface AppPromoRuleHandselRespDTO {
    /**
     * 赠送会员
     * @example 1
     */
    activityCountLimit?: string;
    /**
     * 活动时间限制
     * @format int32
     * @example 1
     */
    activityTimeLimit?: number;
    /**
     * 活动时间限制单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    activityTimeUnit?: string;
    /**
     * 赠送时长
     * @format int32
     * @example 1
     */
    handselTimeLength?: number;
    /**
     * 赠送会员 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    handselVipLevel?: string;
}

/**
 * AppPromotionChannelReqDTO
 * 推广者-申请资料-渠道
 */
export interface AppPromotionChannelReqDTO {
    /**
     * 宣传渠道 1:SNS:SNS 1:CN2:セミナー 1:YOUTUBE:Youtube 1:CN4:ネット記事 1:CN5:その他
     * @example 1
     */
    channel?: string;
    /**
     * 宣传渠道说明
     * @example 1
     */
    channelField1?: string;
    /**
     * 宣传渠道粉丝数
     * @example 1
     */
    channelField2?: string;
}

/**
 * AppPromotionChannelRespDTO
 * 推广者-申请资料-渠道
 */
export interface AppPromotionChannelRespDTO {
    /**
     * 宣传渠道 1:SNS:SNS 1:CN2:セミナー 1:YOUTUBE:Youtube 1:CN4:ネット記事 1:CN5:その他
     * @example 1
     */
    channel?: string;
    /**
     * 宣传渠道说明
     * @example 1
     */
    channelField1?: string;
    /**
     * 宣传渠道粉丝数
     * @example 1
     */
    channelField2?: string;
}

/**
 * AppPromotionExtendReqDTO
 * 推广者-申请资料
 */
export interface AppPromotionExtendReqDTO {
    /** 宣传渠道 */
    channelList?: AppPromotionChannelReqDTO[];
    /**
     * chatwork ID
     * @example 1
     */
    chatworkId?: string;
    /**
     * 佣金
     * @example 1
     */
    commission?: string;
    /**
     * 公司名称
     * @example 1
     */
    company?: string;
    /**
     * 公司主页
     * @example 1
     */
    companyHome?: string;
    /**
     * 国家
     * @example "JP  KR"
     */
    country?: string;
    /**
     * 人数
     * @example 1
     */
    personNum?: string;
    /**
     * 联系电话
     * @example 1
     */
    phone?: string;
    /**
     * 推广计划
     * @example 1
     */
    plan?: string;
    /**
     * 协议
     * @example "专业推广联盟加入须知"
     */
    protocol?: string;
    /**
     * 协议版本
     * @example 1
     */
    protocolVersion?: string;
    /**
     * 月平均采购额
     * @example 1
     */
    purchaseAmountAvg?: string;
}

/**
 * AppPromotionFlagRespDTO
 * 推广者标识
 */
export interface AppPromotionFlagRespDTO {
    /**
     * 赠送时长
     * @format int32
     * @example 1
     */
    handselTimeLength?: number;
    /**
     * 赠送会员 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    handselVipLevel?: string;
    /**
     * 邀请码
     * @example 236452
     */
    invitationCode?: string;
    /**
     * 邀请码链接
     * @example "https://s.theckb.com/signup?channel=0&code=236452"
     */
    invitationCodeUrl?: string;
    /**
     * 登录状态 false:未登录 true:登录
     * @example true
     */
    isLogin?: boolean;
    /**
     * 推广者状态 false:非推广者 true:推广者
     * @example true
     */
    isPromo?: boolean;
    /**
     * 首次登录弹窗
     * @example true
     */
    popFirstLogin?: boolean;
    /**
     * 首次奖励弹窗
     * @example true
     */
    popFirstReward?: boolean;
    /**
     * 专业推广者申请状态:AUDITING:待审核 SU:成功 FAIL:失败
     * @example 1
     */
    proApplyStatus?: string;
    /**
     * 登录人是被推广者，则表示推广者的赠送会员 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    promoHandselVipLevel?: string;
    /**
     * 登录人是被推广者，则表示推广者的赠送时长
     * @format int32
     * @example 1
     */
    promoHhandselTimeLength?: number;
    /**
     * 推广者状态 AUDITING:待审核 SU:成功 FAIL:失败
     * @example "AUDITING"
     */
    promoStatus?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
}

/**
 * AppPromotionQueryReqDTO
 * 推广者-查询
 */
export interface AppPromotionQueryReqDTO {
    /**
     * 弹窗类型 FIRST_LOGIN:首次登录弹窗
     * @example true
     */
    firstLogin?: boolean;
    /**
     * 弹窗类型 FIRST_REWARD:首次奖励弹窗
     * @example true
     */
    firstReward?: boolean;
    /**
     * 主键
     * @format int64
     * @example 76582
     */
    id?: number;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
}

/**
 * AppPromotionReqDTO
 * 推广者
 */
export interface AppPromotionReqDTO {
    /** 申请资料 */
    extend?: AppPromotionExtendReqDTO;
    /**
     * 主键
     * @format int64
     * @example 76582
     */
    id?: number;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
}

/**
 * AppPromotionRespDTO
 * 推广者
 */
export interface AppPromotionRespDTO {
    /**
     * 活动剩余人数
     * @format int32
     * @example 1
     */
    activityRemainingCount?: number;
    /**
     * 活动剩余天数
     * @format int32
     * @example 1
     */
    activityRemainingDay?: number;
    /** 注册资料 */
    extend?: PromotionExtendRespDTO;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 邀请码
     * @example 236452
     */
    invitationCode?: string;
    /**
     * 邀请码链接
     * @example "https://s.theckb.com/signup?channel=0&code=236452"
     */
    invitationCodeUrl?: string;
    /** 专业推广者 */
    proPromo?: AppProPromoRespDTO;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /** 普通推广者 */
    purePromo?: AppPurePromoRespDTO;
    /**
     * 关联用户数
     * @format int64
     * @example 1
     */
    relationCustomerCount?: number;
    /**
     * 邀请码链接
     * @example "https://s.theckb.com/signup?channel=0&code=236452"
     */
    specialInvitationCodeUrl?: string;
    /**
     * 用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
    /**
     * 一级会员人数
     * @format int32
     * @example 10
     */
    totalLevel1Count?: number;
    /**
     * 二级会员人数
     * @format int32
     * @example 10
     */
    totalLevel2Count?: number;
    /**
     * 三级会员人数
     * @format int32
     * @example 10
     */
    totalLevel3Count?: number;
    /**
     * 特殊会员人数
     * @format int32
     * @example 10
     */
    totalSpecialLevelCount?: number;
    /**
     * 超级会员人数
     * @format int32
     * @example 10
     */
    totalSuperLevelCount?: number;
    /**
     * 生效会员数
     * @format int64
     * @example 1
     */
    validCustomerCount?: number;
}

/**
 * AppPurePromoRespDTO
 * 普通推广者
 */
export interface AppPurePromoRespDTO {
    /**
     * 计费方式 FIXED_AMOUNT:固定金额 ONE_MONTH_FEE_RATE:一个月会费比例
     * @example "FIXED_AMOUNT"
     */
    costType?: string;
    /**
     * 有效计费天数
     * @format int32
     * @example 1
     */
    costValidDays?: number;
    /** 赠送规则 */
    handsel?: AppPromoRuleHandselRespDTO;
    /**
     * 一级会员
     * @example 1.1
     */
    level1?: string;
    /**
     * 二级会员
     * @example 1.1
     */
    level2?: string;
    /**
     * 三级会员
     * @example 1.1
     */
    level3?: string;
    /**
     * 已发奖励（人民币）
     * @example 1.1
     */
    paidAmount?: string;
    /**
     * 已发奖励（日元）
     * @example 1.1
     */
    paidAmountCny?: string;
    /** 普通推广规则-支付分期 */
    payList?: AppPromoPureRulePayRespDTO[];
    /**
     * 应发奖励（对应国家币种）
     * @example 1.1
     */
    payableAmount?: string;
    /**
     * 应发奖励（人民币）
     * @example 1.1
     */
    payableAmountCny?: string;
    /**
     * 结算日
     * @format int32
     * @example 1
     */
    settleDay?: number;
    /**
     * 结算周期 REG_DAY_NUM:距离入会天数
     * @example "REG_DAY_NUM"
     */
    settlePeriod?: string;
    /**
     * 专属会员
     * @example 1.1
     */
    specialLevel?: string;
    /**
     * 超级会员
     * @example 1.1
     */
    superLevel?: string;
}

/**
 * AppWalletOrderQueryReqDTO
 * 钱包出入帐单据-查询
 */
export interface AppWalletOrderQueryReqDTO {
    /**
     * 结束时间
     * @example "2023-03-02 01:01:01"
     */
    createEndTime?: string;
    /**
     * 开始时间
     * @example "2023-03-02 01:01:01"
     */
    createStartTime?: string;
    /**
     * 明细类型 MEMBER:会员奖励 ORDER:订单奖励 TR_OUT:钱包转出
     * @example "MEMBER"
     */
    detailType?: string;
    /**
     * 费用类型 COMMISSION:推广联盟返佣 OUT:振替（出金）
     * @example "OUT"
     */
    feeType?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 出入帐流水号
     * @example "WO1101001"
     */
    orderRecordNo?: string;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 结算单号
     * @example "JS1101001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppWalletOrderRecordQueryReqDTO
 * 钱包出入帐流水-查询
 */
export interface AppWalletOrderRecordQueryReqDTO {
    /**
     * 结束时间
     * @example "2023-03-02 01:01:01"
     */
    createEndTime?: string;
    /**
     * 开始时间
     * @example "2023-03-02 01:01:01"
     */
    createStartTime?: string;
    /**
     * 明细类型 MEMBER:会员奖励 ORDER:订单奖励 TR_OUT:钱包转出
     * @example "MEMBER"
     */
    detailType?: string;
    /**
     * 费用类型 COMMISSION:推广联盟返佣 OUT:振替（出金）
     * @example "OUT"
     */
    feeType?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 出入帐流水号
     * @example "WO1101001"
     */
    orderRecordNo?: string;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 结算单号
     * @example "JS1101001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppWalletOrderRecordRespDTO
 * 钱包出入帐流水
 */
export interface AppWalletOrderRecordRespDTO {
    /**
     * 转账金额
     * @example 1.1
     */
    amount?: string;
    /**
     * 出账/入账（元）
     * @example 1.1
     */
    amountDesc?: string;
    /**
     * 余额（元）
     * @example 1.1
     */
    beforeAvailableAmount?: string;
    /**
     * 日期
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 明细类型 MEMBER:会员奖励 ORDER:订单奖励 TR_OUT:钱包转出
     * @example "MEMBER"
     */
    detailType?: string;
    /**
     * 明细类型 MEMBER:会员奖励 ORDER:订单奖励 TR_OUT:钱包转出
     * @example "会员奖励"
     */
    detailTypeDesc?: string;
    /**
     * 费用类型 COMMISSION:推广联盟返佣 OUT:振替（出金）
     * @example "COMMISSION"
     */
    feeType?: string;
    /**
     * 费用类型 COMMISSION:推广联盟返佣 OUT:振替（出金）
     * @example "推广联盟返佣"
     */
    feeTypeDesc?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 出入账类型 NONE:非入账非出账 IN:入账 OUT:出账
     * @example "IN"
     */
    inOutType?: string;
    /**
     * 流水号
     * @example "WO1101001"
     */
    orderNo?: string;
    /**
     * 出入帐流水号
     * @example "WO1101001"
     */
    orderRecordNo?: string;
    /**
     * 期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 奖励结算结束时间
     * @example "2023-2-28 23:59:59"
     */
    settleEndTime?: string;
    /**
     * 结算号
     * @example "JS20202020"
     */
    settleNo?: string;
    /**
     * 奖励结算开始时间
     * @example "2023-2-1 00:00:00"
     */
    settleStartTime?: string;
    /**
     * 总期数
     * @format int32
     * @example 1
     */
    totalPhaseNum?: number;
}

/**
 * AppWalletOrderRespDTO
 * 钱包出入帐单据
 */
export interface AppWalletOrderRespDTO {
    /**
     * 账号类型：1-主账号；2-子账号；
     * @format int32
     * @example 1
     */
    accountType?: number;
    /**
     * 转账金额
     * @example 1.1
     */
    amount?: string;
    /**
     * 出账/入账（元）
     * @example 1.1
     */
    amountDesc?: string;
    /**
     * 出账/入账（元）
     * @example 1.1
     */
    amountFormat?: string;
    /**
     * 余额
     * @example 1.1
     */
    beforeAvailableAmount?: string;
    /**
     * 余额
     * @example 1.1
     */
    beforeAvailableAmountFormat?: string;
    /**
     * 日期
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 店铺ID
     * @format int64
     * @example 1
     */
    customerShopId?: number;
    /**
     * 店铺名称
     * @example "专业推广者"
     */
    customerShopName?: string;
    /**
     * 客户店铺平台: 0-其他; 1-Base; 2-Shopify; 3-Stores; 4-Amazon
     * @format int32
     * @example 1
     */
    customerShopPlatform?: number;
    /**
     * 明细类型 MEMBER:会员奖励 ORDER:订单奖励 TR_OUT:钱包转出
     * @example "MEMBER"
     */
    detailType?: string;
    /**
     * 明细类型 MEMBER:会员奖励 ORDER:订单奖励 TR_OUT:钱包转出
     * @example "会员奖励"
     */
    detailTypeDesc?: string;
    /**
     * 费用类型 COMMISSION:推广联盟返佣 OUT:振替（出金）
     * @example "COMMISSION"
     */
    feeType?: string;
    /**
     * 费用类型 COMMISSION:推广联盟返佣 OUT:振替（出金）
     * @example "推广联盟返佣"
     */
    feeTypeDesc?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 出入账类型 NONE:非入账非出账 IN:入账 OUT:出账
     * @example "IN"
     */
    inOutType?: string;
    /**
     * 日期
     * @example "2023-01-01 01:01:01"
     */
    jpCreateTime?: string;
    /**
     * 登录名
     * @example "loginUser"
     */
    loginName?: string;
    /**
     * 流水号
     * @example "WO1101001"
     */
    orderNo?: string;
    /**
     * 出入帐流水号
     * @example "WO1101001"
     */
    orderRecordNo?: string;
    /**
     * 期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 奖励结算结束时间
     * @example "2023-2-28 23:59:59"
     */
    settleEndTime?: string;
    /**
     * 结算号
     * @example "JS20202020"
     */
    settleNo?: string;
    /**
     * 奖励结算开始时间
     * @example "2023-2-1 00:00:00"
     */
    settleStartTime?: string;
    /**
     * 来源：1-d2c；2-b2b；
     * @format int32
     * @example 1
     */
    systemSource?: number;
    /**
     * 来源描述：1-d2c；2-b2b；
     * @example "d2c"
     */
    systemSourceDesc?: string;
    /**
     * 总期数
     * @format int32
     * @example 1
     */
    totalPhaseNum?: number;
}

/**
 * AppWalletQueryReqDTO
 * 钱包-查询
 */
export interface AppWalletQueryReqDTO {
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /** @format int32 */
    startIndex?: number;
}

/**
 * AppWalletRespDTO
 * 钱包
 */
export interface AppWalletRespDTO {
    /**
     * 余额
     * @example 1.1
     */
    balance?: string;
    /**
     * 余额（人民币）
     * @example 7.1
     */
    balanceCny?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
}

/**
 * MemberRewardMemberRespDTO
 * 成交会员-会员
 */
export interface MemberRewardMemberRespDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 国家
     * @example "日本"
     */
    countryDesc?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 会员是否赠送
     * @example "是"
     */
    handselTypeDesc?: string;
    /**
     * 用户邮箱
     * @example "test@163.com"
     */
    memberEmail?: string;
    /**
     * 会员用户ID
     * @format int64
     * @example 76582
     */
    memberId?: number;
    /**
     * 会员等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    memberLevel?: string;
    /**
     * 会员用户名
     * @example "w123123-账户"
     */
    memberName?: string;
    /**
     * 会员购买月份数
     * @format int32
     * @example 1
     */
    monthNum?: number;
    /**
     * 会员会费金额
     * @example 1.1
     */
    orderPrice?: string;
    /**
     * 购买时间
     * @example "2023-01-01 01:01:01"
     */
    payTime?: string;
    /**
     * 是否复购
     * @example "是"
     */
    repurchaseTypeDesc?: string;
    /**
     * 会员推广来源
     * @example "Facebook"
     */
    source?: string;
    /**
     * 业务线 1:D2C 2:B2B
     * @format int32
     * @example 1
     */
    systemSource?: number;
    /**
     * 业务线 1:D2C 2:B2B
     * @example "D2C"
     */
    systemSourceDesc?: string;
}

/**
 * MemberRewardPayPlanQueryReqDTO
 * 会员奖励结算-支付分期查询
 */
export interface MemberRewardPayPlanQueryReqDTO {
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 支付ID
     * @format int64
     * @example 1
     */
    paymentId?: number;
    /** @format int32 */
    startIndex?: number;
}

/**
 * MemberRewardPayPlanRespDTO
 * 会员奖励结算-支付分期
 */
export interface MemberRewardPayPlanRespDTO {
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 已支付金额
     * @example 1.1
     */
    paidAmount?: string;
    /**
     * 支付单号
     * @example "ZF0000000001"
     */
    payOrderNo?: string;
    /**
     * 支付计划单号
     * @example "ZF0000000001"
     */
    payPlanNo?: string;
    /**
     * 支付计划状态 UNPAID:未支付 ALL:已支付 CANCEL:取消支付
     * @example "UNPAID"
     */
    payPlanStatus?: string;
    /**
     * 支付计划状态 UNPAID:未支付 ALL:已支付 CANCEL:取消支付
     * @example "未支付"
     */
    payPlanStatusDesc?: string;
    /**
     * 实际支付时间
     * @example "2023-01-01 01:01:01"
     */
    payTime?: string;
    /**
     * 分期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 预计支付时间
     * @example "2023-01-01 01:01:01"
     */
    planPayTime?: string;
    /**
     * 待支付金额
     * @example 1.1
     */
    unpaidAmount?: string;
}

/**
 * MemberRewardPayQueryReqDTO
 * 会员奖励支付-查询
 */
export interface MemberRewardPayQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 会员用户名
     * @example 12
     */
    memberName?: string;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 支付结束时间
     * @example "2023-01-01 01:01:01"
     */
    payEndTime?: string;
    /**
     * 支付单号
     * @example "ZF0000000001"
     */
    payOrderNo?: string;
    /**
     * 支付开始时间
     * @example "2023-01-01 01:01:01"
     */
    payStartTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
    /**
     * 推广者用户ID
     * @example 76582
     */
    superCustomerId?: string;
}

/**
 * MemberRewardPayRespDTO
 * 会员奖励支付
 */
export interface MemberRewardPayRespDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 推广者所属国家描述
     * @example "JP"
     */
    countryDesc?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /**
     * 支付金额
     * @example 1.1
     */
    payAmount?: string;
    /**
     * 支付流水号
     * @example "ZF0000000001"
     */
    payOrderNo?: string;
    /**
     * 支付时间
     * @example "2023-01-01 01:01:01"
     */
    payTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 推广者身份
     * @example "普通"
     */
    promoTypeDesc?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 推广者用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
}

/**
 * MemberRewardQueryReqDTO
 * 成交会员-查询
 */
export interface MemberRewardQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 会员等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    memberLevel?: string;
    /**
     * 会员用户名
     * @example "w123123-账户"
     */
    memberName?: string;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 购买结束时间
     * @example "2023-01-01 01:01:01"
     */
    payEndTime?: string;
    /**
     * 购买开始时间
     * @example "2023-01-01 01:01:01"
     */
    payStartTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 是否复购 0:否 1:是
     * @format int32
     * @example 1
     */
    repurchaseType?: number;
    /**
     * 结算类型：0:待结算 1:已结算
     * @format int32
     * @example 1
     */
    settleType?: number;
    /**
     * 会员推广来源
     * @example "Facebook"
     */
    source?: string;
    /** @format int32 */
    startIndex?: number;
    /**
     * 业务线 1:D2C 2:B2B
     * @format int32
     * @example 1
     */
    systemSource?: number;
}

/**
 * MemberRewardRespDTO
 * 成交会员
 */
export interface MemberRewardRespDTO {
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 奖励币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 奖励币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /** 会员 */
    member?: MemberRewardMemberRespDTO;
    /**
     * 奖励流水号
     * @example "LS0000000001"
     */
    orderNo?: string;
    /**
     * 已支付奖励金额
     * @example 1.1
     */
    paidAmount?: string;
    /**
     * 奖励预估金额
     * @example 1.1
     */
    preSettleAmount?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 推广者身份
     * @example "普通"
     */
    promoTypeDesc?: string;
    /**
     * 奖励金额取值 MEMBER:会员奖励 ORDER:订单奖励 MEMBER_AND_ORDER:会员订单奖励之和 MAX_MEMBER_ORDER:会员订单奖励最大
     * @example "MEMBER"
     */
    rewardAmountRange?: string;
    /**
     * 奖励金额取值
     * @example "会员奖励"
     */
    rewardAmountRangeDesc?: string;
    /**
     * 规则ID（预估金额计算）
     * @format int64
     * @example 1
     */
    ruleId?: number;
    /**
     * 奖励结算金额
     * @example 1.1
     */
    settleAmount?: string;
    /**
     * 结算流水号
     * @example "JSLS0000000002"
     */
    settleNo?: string;
    /**
     * 结算时间
     * @example "2023-01-01 01:01:01"
     */
    settleTime?: string;
    /**
     * 结算类型：0:待结算 1:已结算
     * @format int32
     * @example 1
     */
    settleType?: number;
    /**
     * 结算类型
     * @example "已结算"
     */
    settleTypeDesc?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 推广者用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
    /**
     * 待支付奖励金额
     * @example 1.1
     */
    unpaidAmount?: string;
}

/**
 * MemberRewardSettleQueryReqDTO
 * 会员奖励结算-查询
 */
export interface MemberRewardSettleQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 创建结束时间
     * @example "2023-01-01 01:01:01"
     */
    createEndTime?: string;
    /**
     * 创建开始时间
     * @example "2023-01-01 01:01:01"
     */
    createStartTime?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 被推广者用户ID
     * @format int64
     * @example 76582
     */
    passiveSuperCustomerId?: number;
    /**
     * 支付结束时间
     * @example "2023-01-01 01:01:01"
     */
    payEndTime?: string;
    /**
     * 支付流水号
     * @example "ZF0000000001"
     */
    payRecordNo?: string;
    /**
     * 支付开始时间
     * @example "2023-01-01 01:01:01"
     */
    payStartTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
}

/**
 * MemberRewardSettleRespDTO
 * 会员奖励结算
 */
export interface MemberRewardSettleRespDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 推广者所属国家描述
     * @example "JP"
     */
    countryDesc?: string;
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 已支付金额
     * @example 1.1
     */
    paidAmount?: string;
    /**
     * 总分期数
     * @format int32
     * @example 1
     */
    payPeriod?: number;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 推广者身份
     * @example "普通"
     */
    promoTypeDesc?: string;
    /**
     * 规则ID
     * @format int64
     * @example 1
     */
    ruleId?: number;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /**
     * 奖励结算周期
     * @example "2023/2/1 00:00:00—2023/2/28 23:59:59"
     */
    settleTimeRange?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 推广者用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
    /**
     * 总待付金额
     * @example 1.1
     */
    unpaidAmount?: string;
}

/**
 * OperateQueryReqDTO
 * 操作记录-查询
 */
export interface OperateQueryReqDTO {
    /**
     * 业务单号
     * @example 1
     */
    bizNo?: string;
    /**
     * 业务类型 1:PURE_RULE:普通推广规则 2:PRO_RULE:专业推广规则 3:PROMO:推广者
     * @example "PROMO"
     */
    bizType?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /** @format int32 */
    startIndex?: number;
}

/**
 * OperateRespDTO
 * 操作记录
 */
export interface OperateRespDTO {
    /**
     * 业务单号
     * @example 1
     */
    bizNo?: string;
    /**
     * 业务类型 1:PURE_RULE:普通推广规则 2:PRO_RULE:专业推广规则 3:PROMO:推广者
     * @example "PROMO"
     */
    bizType?: string;
    /**
     * 操作内容
     * @example "规则变更，原规则支付设置错了"
     */
    content?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 操作时间
     * @example "2023-01-01 01:01:01"
     */
    opTime?: string;
    /**
     * 操作类型
     * @example 1
     */
    opType?: string;
    /**
     * 操作人
     * @example 1
     */
    operatorName?: string;
}

/**
 * OptionRespDTO
 * 下拉框内容
 */
export interface OptionRespDTO {
    /**
     * 下拉框内容
     * @example "专业推广者"
     */
    label?: string;
    /**
     * 下拉框ID
     * @example "PURE_RULE"
     */
    value?: string;
}

/**
 * OrderRewardPayPlanQueryReqDTO
 * 订单奖励结算-支付分期查询
 */
export interface OrderRewardPayPlanQueryReqDTO {
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 支付ID
     * @format int64
     * @example 1
     */
    paymentId?: number;
    /** @format int32 */
    startIndex?: number;
}

/**
 * OrderRewardPayPlanRespDTO
 * 订单奖励结算-支付分期
 */
export interface OrderRewardPayPlanRespDTO {
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 已支付金额
     * @example 1.1
     */
    paidAmount?: string;
    /**
     * 支付单号
     * @example "ZF0000000001"
     */
    payOrderNo?: string;
    /**
     * 支付计划单号
     * @example "ZF0000000001"
     */
    payPlanNo?: string;
    /**
     * 支付计划状态 UNPAID:未支付 ALL:已支付 CANCEL:取消支付
     * @example "UNPAID"
     */
    payPlanStatus?: string;
    /**
     * 支付计划状态 UNPAID:未支付 ALL:已支付 CANCEL:取消支付
     * @example "未支付"
     */
    payPlanStatusDesc?: string;
    /**
     * 实际支付时间
     * @example "2023-01-01 01:01:01"
     */
    payTime?: string;
    /**
     * 分期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 预计支付时间
     * @example "2023-01-01 01:01:01"
     */
    planPayTime?: string;
    /**
     * 待支付金额
     * @example 1.1
     */
    unpaidAmount?: string;
}

/**
 * OrderRewardPayQueryReqDTO
 * 订单奖励支付-查询
 */
export interface OrderRewardPayQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /**
     * 会员用户名
     * @example 12
     */
    memberName?: string;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 支付结束时间
     * @example "2023-01-01 01:01:01"
     */
    payEndTime?: string;
    /**
     * 支付单号
     * @example "ZF0000000001"
     */
    payOrderNo?: string;
    /**
     * 支付开始时间
     * @example "2023-01-01 01:01:01"
     */
    payStartTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
}

/**
 * OrderRewardPayRespDTO
 * 订单奖励支付
 */
export interface OrderRewardPayRespDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 推广者所属国家描述
     * @example "JP"
     */
    countryDesc?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /**
     * 支付金额
     * @example 1.1
     */
    payAmount?: string;
    /**
     * 支付流水号
     * @example "ZF0000000001"
     */
    payOrderNo?: string;
    /**
     * 支付时间
     * @example "2023-01-01 01:01:01"
     */
    payTime?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 推广者身份
     * @example "普通"
     */
    promoTypeDesc?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 推广者用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
}

/**
 * OrderRewardPromotionRespDTO
 * 成交订单-被推广者
 */
export interface OrderRewardPromotionRespDTO {
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 被推广者邮箱
     * @example "test@163.com"
     */
    email?: string;
    /**
     * 被推广者会员等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    level?: string;
    /**
     * 被推广者会员等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "一级会员"
     */
    levelDesc?: string;
    /**
     * 被推广者用户名
     * @example "w123123-账户"
     */
    loginName?: string;
    /**
     * 被推广者订单费用金额
     * @example 1.1
     */
    orderPrice?: string;
    /**
     * 被推广者订单时间
     * @example "2023-01-01 01:01:01"
     */
    orderTime?: string;
    /**
     * 被推广者订单号
     * @example "LS0000000001"
     */
    platformOrderNo?: string;
    /**
     * 被推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 被推广者业务线 1:D2C 2:B2B
     * @example 1
     */
    systemSource?: string;
}

/**
 * OrderRewardQueryReqDTO
 * 成交订单-查询
 */
export interface OrderRewardQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 订单结束时间
     * @example "2023-01-01 01:01:01"
     */
    orderPayEndTime?: string;
    /**
     * 订单开始时间
     * @example "2023-01-01 01:01:01"
     */
    orderPayStartTime?: string;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 被推广者等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    passiveLevel?: string;
    /**
     * 被推广者用户名
     * @example "w123123-账户"
     */
    passiveLoginName?: string;
    /**
     * 被推广者订单号
     * @example "JS0000000001"
     */
    platformOrderNo?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /**
     * 奖励状态 0:待结算 1:已结算
     * @format int32
     * @example 1
     */
    settleType?: number;
    /** @format int32 */
    startIndex?: number;
    /**
     * 业务线 1:D2C 2:B2B
     * @format int32
     * @example 1
     */
    systemSource?: number;
}

/**
 * OrderRewardRespDTO
 * 成交订单-查询
 */
export interface OrderRewardRespDTO {
    /**
     * 订单金额构成 LOGI_COST:国内运费 PRODUCT_AMOUNT:商品代金 REFUND_AMOUNT:售后退款
     * @example "国内运费,商品代金"
     */
    amountComposeDesc?: string;
    /**
     * 订单金额构成 LOGI_COST:国内运费 PRODUCT_AMOUNT:商品代金 REFUND_AMOUNT:售后退款
     * @example ["LOGI_COST","PRODUCT_AMOUNT"]
     */
    amountComposeList?: string[];
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 推广者所属国家描述
     * @example "JP"
     */
    countryDesc?: string;
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 奖励币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 奖励币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 订单流水号
     * @example "LS0000000001"
     */
    orderNo?: string;
    /**
     * 已支付奖励金额
     * @example 1.1
     */
    paidAmount?: string;
    /** 被推广者 */
    passivePromotion?: OrderRewardPromotionRespDTO;
    /**
     * 奖励预估金额
     * @example 1.1
     */
    preSettleAmount?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 推广者身份
     * @example "普通"
     */
    promoTypeDesc?: string;
    /**
     * 奖励金额取值 MEMBER:会员奖励 ORDER:订单奖励 MEMBER_AND_ORDER:会员订单奖励之和 MAX_MEMBER_ORDER:会员订单奖励最大
     * @example "MEMBER"
     */
    rewardAmountRange?: string;
    /**
     * 奖励金额取值
     * @example "会员奖励"
     */
    rewardAmountRangeDesc?: string;
    /** 订单奖励流水号 */
    rewardNo?: string;
    /**
     * 订单费用结算节点
     * @example "商品代金,国内运费"
     */
    rewardTypeDetail?: string;
    /**
     * 规则ID（预估金额计算）
     * @format int64
     * @example 1
     */
    ruleId?: number;
    /**
     * 规则编号（预估金额计算）
     * @example "RN20"
     */
    ruleNo?: string;
    /**
     * 奖励结算金额
     * @example 1.1
     */
    settleAmount?: string;
    /**
     * 结算流水号
     * @example "JSLS0000000002"
     */
    settleNo?: string;
    /**
     * 结算时间
     * @example "2023-01-01 01:01:01"
     */
    settleTime?: string;
    /**
     * 结算类型：0:待结算 1:已结算
     * @format int32
     * @example 1
     */
    settleType?: number;
    /**
     * 结算类型
     * @example "已结算"
     */
    settleTypeDesc?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 推广者用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
    /**
     * 待支付奖励金额
     * @example 1.1
     */
    unpaidAmount?: string;
}

/**
 * OrderRewardSettleQueryReqDTO
 * 订单奖励结算-查询
 */
export interface OrderRewardSettleQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 被推广者用户ID
     * @example 76582
     */
    passiveSuperCustomerId?: string;
    /**
     * 支付计划状态 UNPAID:未支付 PART:部分支付 ALL:已支付
     * @example "UNPAID"
     */
    payStatus?: string;
    /**
     * 预计支付结束时间
     * @example "2023-01-01 01:01:01"
     */
    planPayEndTime?: string;
    /**
     * 预计支付开始时间
     * @example "2023-01-01 01:01:01"
     */
    planPayStartTime?: string;
    /**
     * 被推广者订单号
     * @example "JS0000000001"
     */
    platformOrderNo?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 奖励流水号
     * @example "JS0000000001"
     */
    rewardNo?: string;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /** @format int32 */
    startIndex?: number;
    /**
     * 推广者用户ID
     * @example 76582
     */
    superCustomerId?: string;
}

/**
 * OrderRewardSettleRespDTO
 * 订单奖励结算-查询
 */
export interface OrderRewardSettleRespDTO {
    /**
     * 审核时间
     * @example "2023-01-01 01:01:01"
     */
    auditTime?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 推广者所属国家描述
     * @example "JP"
     */
    countryDesc?: string;
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /**
     * 已支付金额
     * @example 1.1
     */
    paidAmount?: string;
    /**
     * 总分期数
     * @format int32
     * @example 1
     */
    payPeriod?: number;
    /**
     * 支付状态
     * @example "已支付"
     */
    payStatusDesc?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 推广者身份
     * @example "普通"
     */
    promoTypeDesc?: string;
    /**
     * 规则ID
     * @format int64
     * @example 1
     */
    ruleId?: number;
    /**
     * 结算流水号
     * @example "JS0000000001"
     */
    settleNo?: string;
    /**
     * 奖励结算周期
     * @example "2023/2/1 00:00:00—2023/2/28 23:59:59"
     */
    settleTimeRange?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 推广者用户名
     * @example 76582
     */
    superCustomerName?: string;
    /**
     * 总待付金额
     * @example 1.1
     */
    unpaidAmount?: string;
}

/**
 * ProRuleDetailRespDTO
 * 专业推广规则-详情
 */
export interface ProRuleDetailRespDTO {
    /**
     * 订单金额构成
     * @example "国内运费,商品代金"
     */
    amountComposeDesc?: string;
    /**
     * 订单金额构成 LOGI_COST:国内运费 PRODUCT_AMOUNT:商品代金 REFUND_AMOUNT:售后退款
     * @example ""[LOGI_COST","PRODUCT_AMOUNT"]"
     */
    amountComposeList?: string[];
    /**
     * 审核时间
     * @example "2023-01-01 01:01:01"
     */
    auditTime?: string;
    /**
     * 审核人
     * @example "dawn"
     */
    auditorName?: string;
    /**
     * 计费方式 FEE_RATE:会费比例 FIXED_AMOUNT:固定金额
     * @example "FEE_RATE"
     */
    costType?: string;
    /**
     * 计费方式
     * @example "会费比例"
     */
    costTypeDesc?: string;
    /**
     * 有效计费天数
     * @example 1
     */
    costValidDays?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 国家
     * @example "日本"
     */
    countryDesc?: string;
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 创建人
     * @example "dawn"
     */
    creatorName?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /** 赠送 */
    handsel?: ProRuleHandselRespDTO;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** 订单奖励阶梯 */
    ladderList?: ProRuleOrderLadderRespDTO[];
    /**
     * 一级会员
     * @example 1.1
     */
    level1?: string;
    /**
     * 二级会员
     * @example 1.1
     */
    level2?: string;
    /**
     * 三级会员
     * @example 1.1
     */
    level3?: string;
    /**
     * 特殊会员配置
     * @format int32
     * @example "0-默认仅展示普通会员,1-仅展示特殊会员,2-展示全部会员"
     */
    membershipShowStatus?: number;
    /**
     * 备注
     * @example "备注"
     */
    memo?: string;
    /** 支付分期 */
    pay?: ProRulePayRespDTO;
    /**
     * 奖励金额取值
     * @example "MEMBER:会员奖励 ORDER:订单奖励 MEMBER_AND_ORDER:会员订单奖励之和 MAX_MEMBER_ORDER:会员订单奖励最大"
     */
    rewardAmountRange?: string;
    /**
     * 规则名称
     * @example "日本市场模板1"
     */
    ruleName?: string;
    /**
     * 规则编号
     * @example "MB00001"
     */
    ruleNo?: string;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /**
     * 规则状态
     * @example "草稿"
     */
    ruleStatusDesc?: string;
    /**
     * 结算日
     * @format int32
     * @example 1
     */
    settleDay?: number;
    /**
     * 结算周期 NATURAL_MONTH:自然月月底结 FIXED_DAY:指定日月结
     * @example "NATURAL_MONTH"
     */
    settlePeriod?: string;
    /**
     * 特殊会员
     * @example 1.1
     */
    specialLevel?: string;
    /**
     * 提交时间
     * @example "2023-01-01 01:01:01"
     */
    submitTime?: string;
    /**
     * 提交人
     * @example "dawn"
     */
    submitterName?: string;
    /**
     * 超级会员
     * @example 1.1
     */
    superLevel?: string;
}

/**
 * ProRuleHandselReqDTO
 * 专业推广规则-赠送
 */
export interface ProRuleHandselReqDTO {
    /**
     * 赠送会员
     * @example 1
     */
    activityCountLimit?: string;
    /**
     * 活动时间限制
     * @format int32
     * @example 1
     */
    activityTimeLimit?: number;
    /**
     * 活动时间限制单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    activityTimeUnit?: string;
    /**
     * 赠送时长
     * @format int32
     * @example 1
     */
    handselTimeLength?: number;
    /**
     * 赠送会员 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员 SPECIAL:专属会员
     * @example "LEVEL1"
     */
    handselVipLevel?: string;
}

/**
 * ProRuleHandselRespDTO
 * 专业推广规则-赠送
 */
export interface ProRuleHandselRespDTO {
    /**
     * 赠送会员
     * @format int32
     * @example 1
     */
    activityCountLimit?: number;
    /**
     * 活动时间限制
     * @format int32
     * @example 1
     */
    activityTimeLimit?: number;
    /**
     * 活动时间限制单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    activityTimeUnit?: string;
    /**
     * 赠送时长
     * @format int32
     * @example 1
     */
    handselTimeLength?: number;
    /**
     * 赠送会员 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员 SPECIAL:特殊会员
     * @example "LEVEL1"
     */
    handselVipLevel?: string;
}

/**
 * ProRuleOrderLadderReqDTO
 * 专业推广规则-订单奖励阶梯
 */
export interface ProRuleOrderLadderReqDTO {
    /**
     * 结束金额
     * @format double
     * @example 1.1
     */
    amountRangeEnd?: number;
    /**
     * 开始金额
     * @format double
     * @example 1.1
     */
    amountRangeStart?: number;
    /**
     * 奖励比例
     * @format double
     * @example 1.1
     */
    rewardRate?: number;
}

/**
 * ProRuleOrderLadderRespDTO
 * 专业推广规则-订单奖励阶梯
 */
export interface ProRuleOrderLadderRespDTO {
    /**
     * 结束金额
     * @format double
     * @example 1.1
     */
    amountRangeEnd?: number;
    /**
     * 开始金额
     * @format double
     * @example 1.1
     */
    amountRangeStart?: number;
    /**
     * 奖励比例
     * @format double
     * @example 1.1
     */
    rewardRate?: number;
}

/**
 * ProRulePayReqDTO
 * 专业推广规则-支付分期
 */
export interface ProRulePayReqDTO {
    /**
     * 支付日期
     * @example 1
     */
    payDay?: string;
    /**
     * 支付奖励百分比
     * @example [1.1,1.2]
     */
    payRateList?: string[];
    /**
     * 分期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 分期类型 MONTH_1:月 MONTH_3:季度 MONTH_6:半年
     * @example "MONTH_1"
     */
    phaseType?: string;
}

/**
 * ProRulePayRespDTO
 * 专业推广规则-支付分期
 */
export interface ProRulePayRespDTO {
    /**
     * 支付日期
     * @example 1
     */
    payDay?: string;
    /**
     * 支付奖励百分比
     * @example [1.1,1.2]
     */
    payRateList?: string[];
    /**
     * 分期数
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 分期类型 MONTH_1:月 MONTH_3:季度 MONTH_6:半年
     * @example "MONTH_1"
     */
    phaseType?: string;
}

/**
 * ProRuleQueryReqDTO
 * 专业推广规则-查询
 */
export interface ProRuleQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 规则名称
     * @example "日本市场模板1"
     */
    ruleName?: string;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /** @format int32 */
    startIndex?: number;
    /**
     * 用户ID
     * @format int64
     * @example 1
     */
    superCustomerId?: number;
}

/**
 * ProRuleReqDTOProRuleReqDTO
 * 专业推广规则
 */
export interface ProRuleReqDTOProRuleReqDTO {
    /**
     * 订单金额构成 LOGI_COST:国内运费 PRODUCT_AMOUNT:商品代金 REFUND_AMOUNT:售后退款
     * @example ["LOGI_COST","PRODUCT_AMOUNT"]
     */
    amountComposeList?: string[];
    /**
     * 计费方式 FEE_RATE:会费比例 FIXED_AMOUNT:固定金额
     * @example "FEE_RATE"
     */
    costType?: string;
    /**
     * 有效计费天数
     * @example 1
     */
    costValidDays?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /** 赠送 */
    handsel?: ProRuleHandselReqDTO;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** 订单奖励阶梯 */
    ladderList?: ProRuleOrderLadderReqDTO[];
    /**
     * 一级会员
     * @example 1.1
     */
    level1?: string;
    /**
     * 二级会员
     * @example 1.1
     */
    level2?: string;
    /**
     * 三级会员
     * @example 1.1
     */
    level3?: string;
    /**
     * 专属会员配置：0-默认仅展示普通会员,1-仅展示特殊会员,2-展示全部会员
     * @format int32
     * @example 1.1
     */
    membershipShowStatus?: number;
    /** 支付分期 */
    pay?: ProRulePayReqDTO;
    /**
     * 奖励金额取值 MEMBER:会员奖励 ORDER:订单奖励 MEMBER_AND_ORDER:会员订单奖励之和 MAX_MEMBER_ORDER:会员订单奖励最大
     * @example "MEMBER"
     */
    rewardAmountRange?: string;
    /**
     * 规则名称
     * @example "日本市场模板1"
     */
    ruleName?: string;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 AUDIT_PASS:待生效 ENABLE:已启用 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /**
     * 结算日
     * @format int32
     * @example 1
     */
    settleDay?: number;
    /**
     * 结算周期 NATURAL_MONTH:自然月月底结 FIXED_DAY:指定日月结
     * @example "FIXED_DAY"
     */
    settlePeriod?: string;
    /**
     * 特殊会员(会员费奖励)
     * @example 1.1
     */
    specialLevel?: string;
    /**
     * 生效时间
     * @example "2023-01-01 01:01:01"
     */
    startTime?: string;
    /**
     * 超级会员
     * @example 1.1
     */
    superLevel?: string;
}

/**
 * ProRuleRespDTO
 * 专业推广规则
 */
export interface ProRuleRespDTO {
    /**
     * 审核时间
     * @example "2023-01-01 01:01:01"
     */
    auditTime?: string;
    /**
     * 审核人
     * @example "dawn"
     */
    auditorName?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 国家
     * @example "日本"
     */
    countryDesc?: string;
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 创建人
     * @example "dawn"
     */
    creatorName?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 终止时间
     * @example "2023-01-01 01:01:01"
     */
    endTime?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 专属会员配置
     * @format int64
     * @example 1
     */
    membershipConfigId?: number;
    /**
     * 专属会员配置
     * @format int32
     * @example 1
     */
    membershipShowStatus?: number;
    /**
     * 规则名称
     * @example "日本市场模板1"
     */
    ruleName?: string;
    /**
     * 规则编号
     * @example "MB00001"
     */
    ruleNo?: string;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /**
     * 规则状态
     * @example "草稿"
     */
    ruleStatusDesc?: string;
    /**
     * 推广者类型
     * @example "规则类型 PURE:普通 PRO:专业"
     */
    ruleType?: string;
    /**
     * 生效时间
     * @example "2023-01-01 01:01:01"
     */
    startTime?: string;
}

/**
 * PromoProRuleQueryReqDTO
 * 专业推广规则-查询
 */
export interface PromoProRuleQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 推广者ID
     * @format int64
     * @example 1
     */
    promotionId?: number;
    /**
     * 规则名称
     * @example "日本市场模板1"
     */
    ruleName?: string;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * PromoPureRuleQueryReqDTO
 * 普通推广规则
 */
export interface PromoPureRuleQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 推广者ID
     * @format int64
     * @example 1
     */
    promotionId?: number;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 AUDIT_PASS:待生效 ENABLE:已启用 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * PromotionDetailRespDTO
 * 推广者-详情
 */
export interface PromotionDetailRespDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 国家
     * @example "日本"
     */
    countryDesc?: string;
    /** 申请资料 */
    extend?: PromotionExtendRespDTO;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** 专业推广规则 */
    proRule?: ProRuleDetailRespDTO;
    /** 普通推广规则 */
    pureRule?: PureRuleDetailRespDTO;
}

/**
 * PromotionExtendRespDTO
 * 推广者-申请资料
 */
export interface PromotionExtendRespDTO {
    /** 宣传渠道 */
    channelList?: AppPromotionChannelRespDTO[];
    /**
     * chatwork ID
     * @example 1
     */
    chatworkId?: string;
    /**
     * 佣金
     * @example 1
     */
    commission?: string;
    /**
     * 公司名称
     * @example 1
     */
    company?: string;
    /**
     * 公司主页
     * @example 1
     */
    companyHome?: string;
    /**
     * 人数
     * @example 1
     */
    personNum?: string;
    /**
     * 联系电话
     * @example 1
     */
    phone?: string;
    /**
     * 推广计划
     * @example 1
     */
    plan?: string;
    /**
     * 协议
     * @example "专业推广联盟加入须知"
     */
    protocol?: string;
    /**
     * 协议版本
     * @example 1
     */
    protocolVersion?: string;
    /**
     * 月平均采购额
     * @example 1
     */
    purchaseAmountAvg?: string;
}

/**
 * PromotionOpReqDTO
 * 推广者
 */
export interface PromotionOpReqDTO {
    /**
     * 拒绝内容
     * @example "拒绝"
     */
    content?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 规则Id
     * @format int64
     * @example 123
     */
    ruleId?: number;
    /**
     * 规则编号
     * @example "RN209"
     */
    ruleNo?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * PromotionProRuleReqDTO
 * 专业推广者-专业推广规则
 */
export interface PromotionProRuleReqDTO {
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 备注
     * @example "备注"
     */
    memo?: string;
    /** 专业推广规则 */
    proRule?: ProRuleReqDTOProRuleReqDTO;
}

/**
 * PromotionPureRuleReqDTO
 * 普通推广者-普通推广规则
 */
export interface PromotionPureRuleReqDTO {
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 备注
     * @example "备注"
     */
    memo?: string;
    /** 普通推广规则 */
    pureRule?: PureRuleReqDTO;
}

/**
 * PromotionQueryReqDTO
 * 推广者-查询
 */
export interface PromotionQueryReqDTO {
    /**
     * 申请结束时间
     * @example "2023-01-01 01:01:01"
     */
    applyEndTime?: string;
    /**
     * 申请开始时间
     * @example "2023-01-01 01:01:01"
     */
    applyStartTime?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 推广者状态 AUDITING:待审核 SU:成功 FAIL:失败
     * @example "AUDITING"
     */
    promoStatue?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /** @format int32 */
    startIndex?: number;
    /**
     * 用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
}

/**
 * PromotionRespDTO
 * 推广者
 */
export interface PromotionRespDTO {
    /**
     * 申请时间
     * @example "2023-01-01 01:01:01"
     */
    applyTime?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 国家
     * @example "日本"
     */
    countryDesc?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 备注
     * @example "系统自动审核通过"
     */
    memo?: string;
    /**
     * 推广者状态 AUDITING:待审核 SU:成功 FAIL:失败
     * @example "AUDITING"
     */
    promoStatue?: string;
    /**
     * 推广者状态
     * @example "待审核"
     */
    promoStatueDesc?: string;
    /**
     * 推广者身份 PURE:普通 PRO:专业
     * @example "PURE"
     */
    promoType?: string;
    /**
     * 推广者身份
     * @example "普通"
     */
    promoTypeDesc?: string;
    /**
     * 规则ID
     * @format int64
     * @example 1
     */
    ruleId?: number;
    /**
     * 用户邮箱
     * @example "test@163.com"
     */
    superCustomerEmail?: string;
    /**
     * 用户ID
     * @format int64
     * @example 76582
     */
    superCustomerId?: number;
    /**
     * 用户名
     * @example "w123123-账户"
     */
    superCustomerName?: string;
    /**
     * 业务线 1:D2C 2:B2B
     * @format int32
     * @example 1
     */
    systemSource?: number;
    /**
     * 会员等级 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    vipLevel?: string;
    /**
     * 会员等级
     * @example "一级会员"
     */
    vipLevelDesc?: string;
}

/**
 * PureRuleDetailRespDTO
 * 普通推广规则-详情
 */
export interface PureRuleDetailRespDTO {
    /**
     * 审核时间
     * @example "2023-01-01 01:01:01"
     */
    auditTime?: string;
    /**
     * 审核人
     * @example "dawn"
     */
    auditorName?: string;
    /**
     * 计费方式 FIXED_AMOUNT:固定金额 ONE_MONTH_FEE_RATE:一个月会费比例
     * @example "FIXED_AMOUNT"
     */
    costType?: string;
    /**
     * 计费方式
     * @example "固定金额"
     */
    costTypeDesc?: string;
    /**
     * 有效计费天数
     * @example 1
     */
    costValidDays?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 国家
     * @example "日本"
     */
    countryDesc?: string;
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 创建人
     * @example "dawn"
     */
    creatorName?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /** 赠送 */
    handsel?: PureRuleHandselRespDTO;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 一级会员
     * @example 1.1
     */
    level1?: string;
    /**
     * 二级会员
     * @example 1.1
     */
    level2?: string;
    /**
     * 三级会员
     * @example 1.1
     */
    level3?: string;
    /**
     * 备注
     * @example "备注"
     */
    memo?: string;
    /** 支付分期 */
    payList?: PureRulePayRespDTO[];
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 AUDIT_PASS:待生效 ENABLE:已启用 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /**
     * 规则状态
     * @example "草稿"
     */
    ruleStatusDesc?: string;
    /**
     * 结算日
     * @format int32
     * @example 1
     */
    settleDay?: number;
    /**
     * 结算周期 REG_DAY_NUM:距离入会天数
     * @example "REG_DAY_NUM"
     */
    settlePeriod?: string;
    /**
     * 生效时间
     * @example "2023-01-01 01:01:01"
     */
    startTime?: string;
    /**
     * 提交时间
     * @example "2023-01-01 01:01:01"
     */
    submitTime?: string;
    /**
     * 提交人
     * @example "dawn"
     */
    submitterName?: string;
    /**
     * 超级会员
     * @example 1.1
     */
    superLevel?: string;
}

/**
 * PureRuleHandselReqDTO
 * 普通推广规则-赠送
 */
export interface PureRuleHandselReqDTO {
    /**
     * 赠送会员
     * @example 1
     */
    activityCountLimit?: string;
    /**
     * 活动时间限制
     * @format int32
     * @example 1
     */
    activityTimeLimit?: number;
    /**
     * 活动时间限制单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    activityTimeUnit?: string;
    /**
     * 赠送时长
     * @format int32
     * @example 1
     */
    handselTimeLength?: number;
    /**
     * 赠送会员 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    handselVipLevel?: string;
}

/**
 * PureRuleHandselRespDTO
 * 普通推广规则-赠送
 */
export interface PureRuleHandselRespDTO {
    /**
     * 赠送会员
     * @format int32
     * @example 1
     */
    activityCountLimit?: number;
    /**
     * 活动时间限制
     * @format int32
     * @example 1
     */
    activityTimeLimit?: number;
    /**
     * 活动时间限制单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    activityTimeUnit?: string;
    /**
     * 赠送时长
     * @format int32
     * @example 1
     */
    handselTimeLength?: number;
    /**
     * 赠送会员 LEVEL1:一级会员 LEVEL2:二级会员 LEVEL3:三级会员 SUPER:超级会员
     * @example "LEVEL1"
     */
    handselVipLevel?: string;
}

/**
 * PureRuleOpReqDTO
 * 普通推广规则操作模型
 */
export interface PureRuleOpReqDTO {
    /**
     * 拒绝原因
     * @example "JP"
     */
    content?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
}

/**
 * PureRulePayReqDTO
 * 普通推广规则-支付分期
 */
export interface PureRulePayReqDTO {
    /**
     * 支付奖励百分比
     * @example 1.1
     */
    payRate?: string;
    /**
     * 分期值
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 时间单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    timeUnit?: string;
    /**
     * 距离入会后天数
     * @example 1
     */
    timeValue?: string;
}

/**
 * PureRulePayRespDTO
 * 普通推广规则-支付分期
 */
export interface PureRulePayRespDTO {
    /**
     * 支付奖励百分比
     * @example 1.1
     */
    payRate?: string;
    /**
     * 分期值
     * @format int32
     * @example 1
     */
    phaseNum?: number;
    /**
     * 时间单位 DAY:天 MONTH:月
     * @example "DAY"
     */
    timeUnit?: string;
    /**
     * 距离入会后天数
     * @example 1
     */
    timeValue?: string;
}

/**
 * PureRuleQueryReqDTO
 * 普通推广规则
 */
export interface PureRuleQueryReqDTO {
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 AUDIT_PASS:待生效 ENABLE:已启用 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /** @format int32 */
    startIndex?: number;
}

/**
 * PureRuleReqDTO
 * 普通推广规则
 */
export interface PureRuleReqDTO {
    /**
     * 计费方式 FIXED_AMOUNT:固定金额 ONE_MONTH_FEE_RATE:一个月会费比例
     * @example "FIXED_AMOUNT"
     */
    costType?: string;
    /**
     * 有效计费天数
     * @example 1
     */
    costValidDays?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /** 赠送 */
    handsel?: PureRuleHandselReqDTO;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 一级会员
     * @example 1.1
     */
    level1?: string;
    /**
     * 二级会员
     * @example 1.1
     */
    level2?: string;
    /**
     * 三级会员
     * @example 1.1
     */
    level3?: string;
    /** 支付分期 */
    payList?: PureRulePayReqDTO[];
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 AUDIT_PASS:待生效 ENABLE:已启用 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /**
     * 结算日
     * @format int32
     * @example 1
     */
    settleDay?: number;
    /**
     * 结算周期 REG_DAY_NUM:距离入会天数
     * @example "REG_DAY_NUM"
     */
    settlePeriod?: string;
    /**
     * 生效时间
     * @example "2023-01-01 01:01:01"
     */
    startTime?: string;
    /**
     * 超级会员
     * @example 1.1
     */
    superLevel?: string;
}

/**
 * PureRuleRespDTO
 * 普通推广规则
 */
export interface PureRuleRespDTO {
    /**
     * 审核时间
     * @example "2023-01-01 01:01:01"
     */
    auditTime?: string;
    /**
     * 审核人
     * @example "dawn"
     */
    auditorName?: string;
    /**
     * 国家 JP:日本
     * @example "JP"
     */
    country?: string;
    /**
     * 国家
     * @example "日本"
     */
    countryDesc?: string;
    /**
     * 创建时间
     * @example "2023-01-01 01:01:01"
     */
    createTime?: string;
    /**
     * 创建人
     * @example "dawn"
     */
    creatorName?: string;
    /**
     * 币种 CNY:人民币 JPY:日元
     * @example "JPY"
     */
    currency?: string;
    /**
     * 币种
     * @example "日元"
     */
    currencyDesc?: string;
    /**
     * 失效时间
     * @example "2023-01-01 01:01:01"
     */
    endTime?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 规则状态 DRAFT:草稿 SUBMIT:已提交 REJECT:驳回 AUDIT_PASS:待生效 ENABLE:已启用 STOP:已停用
     * @example "DRAFT"
     */
    ruleStatus?: string;
    /**
     * 规则状态
     * @example "草稿"
     */
    ruleStatusDesc?: string;
    /**
     * 生效时间
     * @example "2023-01-01 01:01:01"
     */
    startTime?: string;
    /**
     * 提交时间
     * @example "2023-01-01 01:01:01"
     */
    submitTime?: string;
    /**
     * 提交人名称
     * @example "dawn"
     */
    submitterName?: string;
}

/** RefreshPureRuleReqDTO */
export interface RefreshPureRuleReqDTO {
    countryCodes?: string[];
}

/**
 * WalletOrderQueryReqDTO
 * 钱包出入帐单据-查询
 */
export interface WalletOrderQueryReqDTO {
    /**
     * 出入帐单号
     * @example "WO1101001"
     */
    orderNo?: string;
    /** @format int32 */
    pageNum?: number;
    /** @format int32 */
    pageSize?: number;
    /** @format int32 */
    startIndex?: number;
}

/**
 * WalletOrderRespDTO
 * 钱包出入帐单据
 */
export interface WalletOrderRespDTO {
    /**
     * 转账金额
     * @example 1.1
     */
    amount?: number;
    /**
     * 操作前可用余额
     * @example 1.1
     */
    beforeAvailableAmount?: number;
    /**
     * 完成时间
     * @example "2023-01-01 01:01:01"
     */
    finishedTime?: string;
    /**
     * 主键
     * @format int64
     * @example 1
     */
    id?: number;
    /**
     * 钱包出入帐单号
     * @example "WO1101001"
     */
    orderNo?: string;
    /**
     * 关联用户ID
     * @format int64
     * @example 1
     */
    relationCustomerId?: number;
    /**
     * 结算单号
     * @example "JS20202020"
     */
    settleNo?: string;
    /**
     * 推广者用户ID
     * @format int64
     * @example 1
     */
    superCustomerId?: number;
}

/** BizResponse«AppPromotionFlagRespDTO» */
export interface BizResponseAppPromotionFlagRespDTO {
    code?: string;
    /** 推广者标识 */
    data?: AppPromotionFlagRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«AppPromotionRespDTO» */
export interface BizResponseAppPromotionRespDTO {
    code?: string;
    /** 推广者 */
    data?: AppPromotionRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«AppWalletOrderRespDTO» */
export interface BizResponseAppWalletOrderRespDTO {
    code?: string;
    /** 钱包出入帐单据 */
    data?: AppWalletOrderRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«AppWalletRespDTO» */
export interface BizResponseAppWalletRespDTO {
    code?: string;
    /** 钱包 */
    data?: AppWalletRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«List«AppAccountRespDTO»» */
export interface BizResponseListAppAccountRespDTO {
    code?: string;
    data?: AppAccountRespDTO[];
    msg?: string;
    success?: boolean;
}

/** BizResponse«MemberRewardPayRespDTO» */
export interface BizResponseMemberRewardPayRespDTO {
    code?: string;
    /** 会员奖励支付 */
    data?: MemberRewardPayRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«MemberRewardRespDTO» */
export interface BizResponseMemberRewardRespDTO {
    code?: string;
    /** 成交会员 */
    data?: MemberRewardRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«MemberRewardSettleRespDTO» */
export interface BizResponseMemberRewardSettleRespDTO {
    code?: string;
    /** 会员奖励结算 */
    data?: MemberRewardSettleRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«OrderRewardPayRespDTO» */
export interface BizResponseOrderRewardPayRespDTO {
    code?: string;
    /** 订单奖励支付 */
    data?: OrderRewardPayRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«OrderRewardRespDTO» */
export interface BizResponseOrderRewardRespDTO {
    code?: string;
    /** 成交订单-查询 */
    data?: OrderRewardRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«OrderRewardSettleRespDTO» */
export interface BizResponseOrderRewardSettleRespDTO {
    code?: string;
    /** 订单奖励结算-查询 */
    data?: OrderRewardSettleRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«AppMemberRespDTO»» */
export interface BizResponsePageResultAppMemberRespDTO {
    code?: string;
    data?: PageResultAppMemberRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«AppMemberRewardPlanRespDTO»» */
export interface BizResponsePageResultAppMemberRewardPlanRespDTO {
    code?: string;
    data?: PageResultAppMemberRewardPlanRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«AppMemberRewardRespDTO»» */
export interface BizResponsePageResultAppMemberRewardRespDTO {
    code?: string;
    data?: PageResultAppMemberRewardRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«AppOrderRewardPlanRespDTO»» */
export interface BizResponsePageResultAppOrderRewardPlanRespDTO {
    code?: string;
    data?: PageResultAppOrderRewardPlanRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«AppOrderRewardRespDTO»» */
export interface BizResponsePageResultAppOrderRewardRespDTO {
    code?: string;
    data?: PageResultAppOrderRewardRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«AppWalletOrderRecordRespDTO»» */
export interface BizResponsePageResultAppWalletOrderRecordRespDTO {
    code?: string;
    data?: PageResultAppWalletOrderRecordRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«AppWalletOrderRespDTO»» */
export interface BizResponsePageResultAppWalletOrderRespDTO {
    code?: string;
    data?: PageResultAppWalletOrderRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«MemberRewardPayPlanRespDTO»» */
export interface BizResponsePageResultMemberRewardPayPlanRespDTO {
    code?: string;
    data?: PageResultMemberRewardPayPlanRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«MemberRewardPayRespDTO»» */
export interface BizResponsePageResultMemberRewardPayRespDTO {
    code?: string;
    data?: PageResultMemberRewardPayRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«MemberRewardRespDTO»» */
export interface BizResponsePageResultMemberRewardRespDTO {
    code?: string;
    data?: PageResultMemberRewardRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«MemberRewardSettleRespDTO»» */
export interface BizResponsePageResultMemberRewardSettleRespDTO {
    code?: string;
    data?: PageResultMemberRewardSettleRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«OperateRespDTO»» */
export interface BizResponsePageResultOperateRespDTO {
    code?: string;
    data?: PageResultOperateRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«OptionRespDTO»» */
export interface BizResponsePageResultOptionRespDTO {
    code?: string;
    data?: PageResultOptionRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«OrderRewardPayPlanRespDTO»» */
export interface BizResponsePageResultOrderRewardPayPlanRespDTO {
    code?: string;
    data?: PageResultOrderRewardPayPlanRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«OrderRewardPayRespDTO»» */
export interface BizResponsePageResultOrderRewardPayRespDTO {
    code?: string;
    data?: PageResultOrderRewardPayRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«OrderRewardRespDTO»» */
export interface BizResponsePageResultOrderRewardRespDTO {
    code?: string;
    data?: PageResultOrderRewardRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«OrderRewardSettleRespDTO»» */
export interface BizResponsePageResultOrderRewardSettleRespDTO {
    code?: string;
    data?: PageResultOrderRewardSettleRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«ProRuleRespDTO»» */
export interface BizResponsePageResultProRuleRespDTO {
    code?: string;
    data?: PageResultProRuleRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«PromotionRespDTO»» */
export interface BizResponsePageResultPromotionRespDTO {
    code?: string;
    data?: PageResultPromotionRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PageResult«PureRuleRespDTO»» */
export interface BizResponsePageResultPureRuleRespDTO {
    code?: string;
    data?: PageResultPureRuleRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«ProRuleDetailRespDTO» */
export interface BizResponseProRuleDetailRespDTO {
    code?: string;
    /** 专业推广规则-详情 */
    data?: ProRuleDetailRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«ProRuleRespDTO» */
export interface BizResponseProRuleRespDTO {
    code?: string;
    /** 专业推广规则 */
    data?: ProRuleRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PromotionDetailRespDTO» */
export interface BizResponsePromotionDetailRespDTO {
    code?: string;
    /** 推广者-详情 */
    data?: PromotionDetailRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«PureRuleDetailRespDTO» */
export interface BizResponsePureRuleDetailRespDTO {
    code?: string;
    /** 普通推广规则-详情 */
    data?: PureRuleDetailRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«Void» */
export interface BizResponseVoid {
    code?: string;
    msg?: string;
    success?: boolean;
}

/** BizResponse«WalletOrderRespDTO» */
export interface BizResponseWalletOrderRespDTO {
    code?: string;
    /** 钱包出入帐单据 */
    data?: WalletOrderRespDTO;
    msg?: string;
    success?: boolean;
}

/** BizResponse«boolean» */
export interface BizResponseBoolean {
    code?: string;
    data?: boolean;
    msg?: string;
    success?: boolean;
}

/** BizResponse«long» */
export interface BizResponseLong {
    code?: string;
    /** @format int64 */
    data?: number;
    msg?: string;
    success?: boolean;
}

/** BizResponse«object» */
export interface BizResponseObject {
    code?: string;
    data?: object;
    msg?: string;
    success?: boolean;
}

/** BizResponse«string» */
export interface BizResponseString {
    code?: string;
    data?: string;
    msg?: string;
    success?: boolean;
}

/** PageResult«AppMemberRespDTO» */
export interface PageResultAppMemberRespDTO {
    /** @format int64 */
    current?: number;
    records?: AppMemberRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«AppMemberRewardPlanRespDTO» */
export interface PageResultAppMemberRewardPlanRespDTO {
    /** @format int64 */
    current?: number;
    records?: AppMemberRewardPlanRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«AppMemberRewardRespDTO» */
export interface PageResultAppMemberRewardRespDTO {
    /** @format int64 */
    current?: number;
    records?: AppMemberRewardRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«AppOrderRewardPlanRespDTO» */
export interface PageResultAppOrderRewardPlanRespDTO {
    /** @format int64 */
    current?: number;
    records?: AppOrderRewardPlanRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«AppOrderRewardRespDTO» */
export interface PageResultAppOrderRewardRespDTO {
    /** @format int64 */
    current?: number;
    records?: AppOrderRewardRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«AppWalletOrderRecordRespDTO» */
export interface PageResultAppWalletOrderRecordRespDTO {
    /** @format int64 */
    current?: number;
    records?: AppWalletOrderRecordRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«AppWalletOrderRespDTO» */
export interface PageResultAppWalletOrderRespDTO {
    /** @format int64 */
    current?: number;
    records?: AppWalletOrderRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«MemberRewardPayPlanRespDTO» */
export interface PageResultMemberRewardPayPlanRespDTO {
    /** @format int64 */
    current?: number;
    records?: MemberRewardPayPlanRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«MemberRewardPayRespDTO» */
export interface PageResultMemberRewardPayRespDTO {
    /** @format int64 */
    current?: number;
    records?: MemberRewardPayRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«MemberRewardRespDTO» */
export interface PageResultMemberRewardRespDTO {
    /** @format int64 */
    current?: number;
    records?: MemberRewardRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«MemberRewardSettleRespDTO» */
export interface PageResultMemberRewardSettleRespDTO {
    /** @format int64 */
    current?: number;
    records?: MemberRewardSettleRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«OperateRespDTO» */
export interface PageResultOperateRespDTO {
    /** @format int64 */
    current?: number;
    records?: OperateRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«OptionRespDTO» */
export interface PageResultOptionRespDTO {
    /** @format int64 */
    current?: number;
    records?: OptionRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«OrderRewardPayPlanRespDTO» */
export interface PageResultOrderRewardPayPlanRespDTO {
    /** @format int64 */
    current?: number;
    records?: OrderRewardPayPlanRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«OrderRewardPayRespDTO» */
export interface PageResultOrderRewardPayRespDTO {
    /** @format int64 */
    current?: number;
    records?: OrderRewardPayRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«OrderRewardRespDTO» */
export interface PageResultOrderRewardRespDTO {
    /** @format int64 */
    current?: number;
    records?: OrderRewardRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«OrderRewardSettleRespDTO» */
export interface PageResultOrderRewardSettleRespDTO {
    /** @format int64 */
    current?: number;
    records?: OrderRewardSettleRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«ProRuleRespDTO» */
export interface PageResultProRuleRespDTO {
    /** @format int64 */
    current?: number;
    records?: ProRuleRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«PromotionRespDTO» */
export interface PageResultPromotionRespDTO {
    /** @format int64 */
    current?: number;
    records?: PromotionRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

/** PageResult«PureRuleRespDTO» */
export interface PageResultPureRuleRespDTO {
    /** @format int64 */
    current?: number;
    records?: PureRuleRespDTO[];
    /** @format int64 */
    size?: number;
    /** @format int64 */
    total?: number;
    /** @format int64 */
    totalPage?: number;
}

import axios, { AxiosInstance, AxiosRequestConfig, HeadersDefaults, ResponseType } from 'axios';

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, 'data' | 'params' | 'url' | 'responseType'> {
    /** set parameter to `true` for call `securityWorker` for this request */
    secure?: boolean;
    /** request path */
    path: string;
    /** content type of request body */
    type?: ContentType;
    /** query params */
    query?: QueryParamsType;
    /** format of response (i.e. response.json() -> format: "json") */
    format?: ResponseType;
    /** request body */
    body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, 'data' | 'cancelToken'> {
    securityWorker?: (
        securityData: SecurityDataType | null
    ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
    secure?: boolean;
    format?: ResponseType;
}

export enum ContentType {
    Json = 'application/json',
    FormData = 'multipart/form-data',
    UrlEncoded = 'application/x-www-form-urlencoded',
    Text = 'text/plain'
}

export class HttpClient<SecurityDataType = unknown> {
    public instance: AxiosInstance;
    private securityData: SecurityDataType | null = null;
    private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
    private secure?: boolean;
    private format?: ResponseType;

    constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
        this.instance = axios.create({ ...axiosConfig, baseURL: axiosConfig.baseURL || '//192.168.201.216:61001/smc' });
        this.secure = secure;
        this.format = format;
        this.securityWorker = securityWorker;
    }

    public setSecurityData = (data: SecurityDataType | null) => {
        this.securityData = data;
    };

    protected mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
        const method = params1.method || (params2 && params2.method);

        return {
            ...this.instance.defaults,
            ...params1,
            ...(params2 || {}),
            headers: {
                ...((method && this.instance.defaults.headers[method.toLowerCase() as keyof HeadersDefaults]) || {}),
                ...(params1.headers || {}),
                ...((params2 && params2.headers) || {})
            }
        };
    }

    protected stringifyFormItem(formItem: unknown) {
        if (typeof formItem === 'object' && formItem !== null) {
            return JSON.stringify(formItem);
        } else {
            return `${formItem}`;
        }
    }

    protected createFormData(input: Record<string, unknown>): FormData {
        return Object.keys(input || {}).reduce((formData, key) => {
            const property = input[key];
            const propertyContent: any[] = property instanceof Array ? property : [property];

            for (const formItem of propertyContent) {
                const isFileType = formItem instanceof Blob || formItem instanceof File;
                formData.append(key, isFileType ? formItem : this.stringifyFormItem(formItem));
            }

            return formData;
        }, new FormData());
    }

    public request = async <T = any, _E = any>({
        secure,
        path,
        type,
        query,
        format,
        body,
        ...params
    }: FullRequestParams): Promise<T> => {
        const secureParams =
            ((typeof secure === 'boolean' ? secure : this.secure) &&
                this.securityWorker &&
                (await this.securityWorker(this.securityData))) ||
            {};
        const requestParams = this.mergeRequestParams(params, secureParams);
        const responseFormat = format || this.format || undefined;

        if (type === ContentType.FormData && body && body !== null && typeof body === 'object') {
            body = this.createFormData(body as Record<string, unknown>);
        }

        if (type === ContentType.Text && body && body !== null && typeof body !== 'string') {
            body = JSON.stringify(body);
        }

        return this.instance
            .request({
                ...requestParams,
                headers: {
                    ...(requestParams.headers || {}),
                    ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {})
                },
                params: query,
                responseType: responseFormat,
                data: body,
                url: path
            })
            .then((response) => response.data);
    };
}

/**
 * @title API文档
 * @version v1.0.0
 * @baseUrl //192.168.201.216:61001/smc
 * @contact 史尼芙 (https://www.taobaockb.com/)
 *
 * 史尼芙API文档
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
    probe = {
        /**
         * No description
         *
         * @tags 健康检查探针
         * @name Test
         * @summary 执行一次健康检查探针
         * @request GET:/Probe/test
         */
        test: (params: RequestParams = {}) =>
            this.request<BizResponseObject, any>({
                path: `/Probe/test`,
                method: 'GET',
                ...params
            })
    };
    app = {
        /**
         * No description
         *
         * @tags App-会员管理
         * @name MemberList
         * @summary 列表
         * @request POST:/app/member/list
         */
        memberList: (req: AppMemberQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultAppMemberRespDTO, any>({
                path: `/app/member/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-会员奖励管理
         * @name MemberRewardExport
         * @summary 导出
         * @request POST:/app/memberReward/export
         */
        memberRewardExport: (req: AppMemberRewardPlanQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/app/memberReward/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-会员奖励管理
         * @name MemberRewardList
         * @summary 列表
         * @request POST:/app/memberReward/list
         */
        memberRewardList: (req: AppMemberRewardPlanQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultAppMemberRewardPlanRespDTO, any>({
                path: `/app/memberReward/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-会员奖励管理
         * @name MemberRewardRewardList
         * @summary 会员奖励-成交会员列表
         * @request POST:/app/memberReward/rewardList
         */
        memberRewardRewardList: (req: AppMemberRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultAppMemberRewardRespDTO, any>({
                path: `/app/memberReward/rewardList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-订单奖励管理
         * @name OrderRewardExport
         * @summary 导出
         * @request POST:/app/orderReward/export
         */
        orderRewardExport: (req: AppOrderRewardPlanQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/app/orderReward/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-订单奖励管理
         * @name OrderRewardList
         * @summary 列表
         * @request POST:/app/orderReward/list
         */
        orderRewardList: (req: AppOrderRewardPlanQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultAppOrderRewardPlanRespDTO, any>({
                path: `/app/orderReward/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-订单奖励管理
         * @name OrderRewardRewardList
         * @summary 订单奖励-成交订单列表
         * @request POST:/app/orderReward/rewardList
         */
        orderRewardRewardList: (req: AppOrderRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultAppOrderRewardRespDTO, any>({
                path: `/app/orderReward/rewardList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-弹窗
         * @name PopPop
         * @summary 弹窗标识
         * @request POST:/app/pop/pop
         */
        popPop: (req: AppPopReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseVoid, any>({
                path: `/app/pop/pop`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-弹窗
         * @name PopPopSu
         * @summary 弹窗成功
         * @request POST:/app/pop/popSu
         */
        popPopSu: (req: AppPopReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseVoid, any>({
                path: `/app/pop/popSu`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-推广者管理
         * @name PromotionApply
         * @summary 申请
         * @request POST:/app/promotion/apply
         */
        promotionApply: (req: AppPromotionReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/app/promotion/apply`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-推广者管理
         * @name PromotionDetail
         * @summary 详情
         * @request POST:/app/promotion/detail
         */
        promotionDetail: (req: AppPromotionQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseAppPromotionRespDTO, any>({
                path: `/app/promotion/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-推广者管理
         * @name PromotionGetPromoFlag
         * @summary 推广者标识
         * @request POST:/app/promotion/getPromoFlag
         */
        promotionGetPromoFlag: (req: AppPromotionQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseAppPromotionFlagRespDTO, any>({
                path: `/app/promotion/getPromoFlag`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-推广者管理
         * @name PromotionReapply
         * @summary 重新申请
         * @request POST:/app/promotion/reapply
         */
        promotionReapply: (req: AppPromotionReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/app/promotion/reapply`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包管理
         * @name WalletAccountOptionList
         * @summary 转账账户
         * @request POST:/app/wallet/accountOptionList
         */
        walletAccountOptionList: (params: RequestParams = {}) =>
            this.request<BizResponseListAppAccountRespDTO, any>({
                path: `/app/wallet/accountOptionList`,
                method: 'POST',
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包管理
         * @name WalletChangeAmount
         * @summary 转账
         * @request POST:/app/wallet/changeAmount
         */
        walletChangeAmount: (req: AppChangeAmountReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseVoid, any>({
                path: `/app/wallet/changeAmount`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包管理
         * @name WalletGetWallet
         * @summary 钱包
         * @request POST:/app/wallet/getWallet
         */
        walletGetWallet: (req: AppWalletQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseAppWalletRespDTO, any>({
                path: `/app/wallet/getWallet`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包出入帐流水管理
         * @name WalletRecordDetail
         * @summary 钱包出入帐流水详情
         * @request POST:/app/walletRecord/detail
         */
        walletRecordDetail: (req: AppWalletOrderQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseAppWalletOrderRespDTO, any>({
                path: `/app/walletRecord/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包出入帐流水管理
         * @name WalletRecordExport
         * @summary 钱包出入帐单据导出
         * @request POST:/app/walletRecord/export
         */
        walletRecordExport: (req: AppWalletOrderQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/app/walletRecord/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包出入帐流水管理
         * @name WalletRecordList
         * @summary 钱包出入帐单据
         * @request POST:/app/walletRecord/list
         */
        walletRecordList: (req: AppWalletOrderQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultAppWalletOrderRespDTO, any>({
                path: `/app/walletRecord/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包出入帐流水管理
         * @name WalletRecordRecordExport
         * @summary 钱包出入帐流水导出
         * @request POST:/app/walletRecord/recordExport
         */
        walletRecordRecordExport: (req: AppWalletOrderRecordQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/app/walletRecord/recordExport`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags App-钱包出入帐流水管理
         * @name WalletRecordRecordList
         * @summary 钱包出入帐流水
         * @request POST:/app/walletRecord/recordList
         */
        walletRecordRecordList: (req: AppWalletOrderRecordQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultAppWalletOrderRecordRespDTO, any>({
                path: `/app/walletRecord/recordList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    demo = {
        /**
         * No description
         *
         * @tags 测试
         * @name Demo
         * @summary 测试
         * @request GET:/demo/demo
         */
        demo: (params: RequestParams = {}) =>
            this.request<BizResponseVoid, any>({
                path: `/demo/demo`,
                method: 'GET',
                ...params
            })
    };
    dingding = {
        /**
         * No description
         *
         * @tags 钉钉事件监听管理
         * @name DingCallback
         * @summary 回调
         * @request POST:/dingding/dingCallback
         */
        dingCallback: (
            body: Record<string, object>,
            query?: {
                /** nonce */
                nonce?: string;
                /** signature */
                signature?: string;
                /** timestamp */
                timestamp?: string;
            },
            params: RequestParams = {}
        ) =>
            this.request<Record<string, string>, any>({
                path: `/dingding/dingCallback`,
                method: 'POST',
                query: query,
                body: body,
                type: ContentType.Json,
                ...params
            })
    };
    memberReward = {
        /**
         * No description
         *
         * @tags 成交会员管理
         * @name Detail
         * @summary 详情
         * @request POST:/memberReward/detail
         */
        detail: (req: MemberRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseMemberRewardRespDTO, any>({
                path: `/memberReward/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 成交会员管理
         * @name Export
         * @summary 导出
         * @request POST:/memberReward/export
         */
        export: (req: MemberRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/memberReward/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 成交会员管理
         * @name List
         * @summary 列表
         * @request POST:/memberReward/list
         */
        list: (req: MemberRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultMemberRewardRespDTO, any>({
                path: `/memberReward/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    memberRewardPay = {
        /**
         * No description
         *
         * @tags 会员奖励支付管理
         * @name Detail
         * @summary 详情
         * @request POST:/memberRewardPay/detail
         */
        detail: (req: MemberRewardPayQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseMemberRewardPayRespDTO, any>({
                path: `/memberRewardPay/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 会员奖励支付管理
         * @name Export
         * @summary 导出
         * @request POST:/memberRewardPay/export
         */
        export: (req: MemberRewardPayQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/memberRewardPay/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 会员奖励支付管理
         * @name List
         * @summary 列表
         * @request POST:/memberRewardPay/list
         */
        list: (req: MemberRewardPayQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultMemberRewardPayRespDTO, any>({
                path: `/memberRewardPay/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    memberRewardSettle = {
        /**
         * No description
         *
         * @tags 会员奖励结算管理
         * @name Detail
         * @summary 详情
         * @request POST:/memberRewardSettle/detail
         */
        detail: (req: MemberRewardSettleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseMemberRewardSettleRespDTO, any>({
                path: `/memberRewardSettle/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 会员奖励结算管理
         * @name Export
         * @summary 导出
         * @request POST:/memberRewardSettle/export
         */
        export: (req: MemberRewardSettleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/memberRewardSettle/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 会员奖励结算管理
         * @name List
         * @summary 列表
         * @request POST:/memberRewardSettle/list
         */
        list: (req: MemberRewardSettleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultMemberRewardSettleRespDTO, any>({
                path: `/memberRewardSettle/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 会员奖励结算管理
         * @name PayPlanList
         * @summary 支付分期列表
         * @request POST:/memberRewardSettle/payPlanList
         */
        payPlanList: (req: MemberRewardPayPlanQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultMemberRewardPayPlanRespDTO, any>({
                path: `/memberRewardSettle/payPlanList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    operate = {
        /**
         * No description
         *
         * @tags 操作记录管理
         * @name List
         * @summary 列表
         * @request POST:/operate/list
         */
        list: (req: OperateQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultOperateRespDTO, any>({
                path: `/operate/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    orderReward = {
        /**
         * No description
         *
         * @tags 成交订单管理
         * @name Detail
         * @summary 详情
         * @request POST:/orderReward/detail
         */
        detail: (req: OrderRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseOrderRewardRespDTO, any>({
                path: `/orderReward/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 成交订单管理
         * @name Export
         * @summary 导出
         * @request POST:/orderReward/export
         */
        export: (req: OrderRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/orderReward/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 成交订单管理
         * @name List
         * @summary 列表
         * @request POST:/orderReward/list
         */
        list: (req: OrderRewardQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultOrderRewardRespDTO, any>({
                path: `/orderReward/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    orderRewardPay = {
        /**
         * No description
         *
         * @tags 订单奖励支付管理
         * @name Detail
         * @summary 详情
         * @request POST:/orderRewardPay/detail
         */
        detail: (req: OrderRewardPayQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseOrderRewardPayRespDTO, any>({
                path: `/orderRewardPay/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 订单奖励支付管理
         * @name Export
         * @summary 导出
         * @request POST:/orderRewardPay/export
         */
        export: (req: OrderRewardPayQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/orderRewardPay/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 订单奖励支付管理
         * @name List
         * @summary 列表
         * @request POST:/orderRewardPay/list
         */
        list: (req: OrderRewardPayQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultOrderRewardPayRespDTO, any>({
                path: `/orderRewardPay/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    orderRewardSettle = {
        /**
         * No description
         *
         * @tags 订单奖励结算管理
         * @name Detail
         * @summary 详情
         * @request POST:/orderRewardSettle/detail
         */
        detail: (req: OrderRewardSettleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseOrderRewardSettleRespDTO, any>({
                path: `/orderRewardSettle/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 订单奖励结算管理
         * @name Export
         * @summary 导出
         * @request POST:/orderRewardSettle/export
         */
        export: (req: OrderRewardSettleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/orderRewardSettle/export`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 订单奖励结算管理
         * @name List
         * @summary 列表
         * @request POST:/orderRewardSettle/list
         */
        list: (req: OrderRewardSettleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultOrderRewardSettleRespDTO, any>({
                path: `/orderRewardSettle/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 订单奖励结算管理
         * @name PayPlanList
         * @summary 支付分期列表
         * @request POST:/orderRewardSettle/payPlanList
         */
        payPlanList: (req: OrderRewardPayPlanQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultOrderRewardPayPlanRespDTO, any>({
                path: `/orderRewardSettle/payPlanList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    proRule = {
        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name Delete
         * @summary 专业推广规则删除
         * @request POST:/proRule/delete
         */
        delete: (req: ProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/proRule/delete`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name Detail
         * @summary 专业推广规则详情
         * @request POST:/proRule/detail
         */
        detail: (req: ProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseProRuleDetailRespDTO, any>({
                path: `/proRule/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name GetSuperCustomerId
         * @summary getProRuleBySuperCustomerId
         * @request POST:/proRule/get/superCustomerId
         */
        getSuperCustomerId: (req: ProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseProRuleRespDTO, any>({
                path: `/proRule/get/superCustomerId`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name List
         * @summary 专业推广规则列表
         * @request POST:/proRule/list
         */
        list: (req: ProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultProRuleRespDTO, any>({
                path: `/proRule/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name OptionList
         * @summary 专业推广者模版下拉
         * @request POST:/proRule/optionList
         */
        optionList: (req: ProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultOptionRespDTO, any>({
                path: `/proRule/optionList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name PromoList
         * @summary 专业推广者-专业推广规则列表
         * @request POST:/proRule/promoList
         */
        promoList: (req: PromoProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultProRuleRespDTO, any>({
                path: `/proRule/promoList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name SaveOrSubmit
         * @summary 专业推广规则保存或者提交
         * @request POST:/proRule/saveOrSubmit
         */
        saveOrSubmit: (req: ProRuleReqDTOProRuleReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseLong, any>({
                path: `/proRule/saveOrSubmit`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name Stop
         * @summary 专业推广规则停用
         * @request POST:/proRule/stop
         */
        stop: (req: ProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/proRule/stop`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 专业推广规则管理
         * @name SubmitById
         * @summary 专业推广规则提交
         * @request POST:/proRule/submitById
         */
        submitById: (req: ProRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/proRule/submitById`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    promotion = {
        /**
         * No description
         *
         * @tags 推广者管理
         * @name Detail
         * @summary 详情
         * @request POST:/promotion/detail
         */
        detail: (req: PromotionQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePromotionDetailRespDTO, any>({
                path: `/promotion/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name List
         * @summary 列表
         * @request POST:/promotion/list
         */
        list: (req: PromotionQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultPromotionRespDTO, any>({
                path: `/promotion/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name Pass
         * @summary 专业推广者-审核通过
         * @request POST:/promotion/pass
         */
        pass: (req: PromotionOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/promotion/pass`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name RefreshPureRule
         * @summary 普通推广者规则刷新
         * @request POST:/promotion/refreshPureRule
         */
        refreshPureRule: (req: RefreshPureRuleReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseString, any>({
                path: `/promotion/refreshPureRule`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name Reject
         * @summary 专业推广者-驳回
         * @request POST:/promotion/reject
         */
        reject: (req: PromotionOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/promotion/reject`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name RuleDetail
         * @summary 规则详情
         * @request POST:/promotion/ruleDetail
         */
        ruleDetail: (req: PromotionQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePromotionDetailRespDTO, any>({
                path: `/promotion/ruleDetail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name StopRule
         * @summary 规则-终止
         * @request POST:/promotion/stopRule
         */
        stopRule: (promotionOpReqDTO: PromotionOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/promotion/stopRule`,
                method: 'POST',
                body: promotionOpReqDTO,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name UpdateProRule
         * @summary 专业推广者-变更规则
         * @request POST:/promotion/updateProRule
         */
        updateProRule: (req: PromotionProRuleReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/promotion/updateProRule`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name UpdatePureRule
         * @summary 普通推广者-变更规则
         * @request POST:/promotion/updatePureRule
         */
        updatePureRule: (req: PromotionPureRuleReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/promotion/updatePureRule`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 推广者管理
         * @name ValidProRule
         * @summary 专业推广者-生效规则
         * @request POST:/promotion/validProRule
         */
        validProRule: (req: PromotionOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/promotion/validProRule`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    pureRule = {
        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name Delete
         * @summary 普通推广规则删除
         * @request POST:/pureRule/delete
         */
        delete: (req: PureRuleOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/pureRule/delete`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name Detail
         * @summary 普通推广规则详情
         * @request POST:/pureRule/detail
         */
        detail: (req: PureRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePureRuleDetailRespDTO, any>({
                path: `/pureRule/detail`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name List
         * @summary 普通推广规则列表
         * @request POST:/pureRule/list
         */
        list: (req: PureRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultPureRuleRespDTO, any>({
                path: `/pureRule/list`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name Pass
         * @summary 普通推广规则审核通过
         * @request POST:/pureRule/pass
         */
        pass: (req: PureRuleOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/pureRule/pass`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name PromoList
         * @summary 普通推广者-普通推广规则列表
         * @request POST:/pureRule/promoList
         */
        promoList: (req: PromoPureRuleQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponsePageResultPureRuleRespDTO, any>({
                path: `/pureRule/promoList`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name Reject
         * @summary 普通推广规则驳回
         * @request POST:/pureRule/reject
         */
        reject: (req: PureRuleOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/pureRule/reject`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name SaveOrSubmit
         * @summary 普通推广规则保存或者提交
         * @request POST:/pureRule/saveOrSubmit
         */
        saveOrSubmit: (req: PureRuleReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseLong, any>({
                path: `/pureRule/saveOrSubmit`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name Stop
         * @summary 普通推广规则停用
         * @request POST:/pureRule/stop
         */
        stop: (req: PureRuleOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/pureRule/stop`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name SubmitById
         * @summary 普通推广规则提交
         * @request POST:/pureRule/submitById
         */
        submitById: (req: PureRuleOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/pureRule/submitById`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            }),

        /**
         * No description
         *
         * @tags 普通推广规则管理
         * @name Valid
         * @summary 普通推广规则自动生效
         * @request POST:/pureRule/valid
         */
        valid: (req: PureRuleOpReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseBoolean, any>({
                path: `/pureRule/valid`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
    walletOrder = {
        /**
         * No description
         *
         * @tags 钱包出入帐单据
         * @name GetByOrderNo
         * @summary 获取钱包出入帐单据
         * @request POST:/walletOrder/getByOrderNo
         */
        getByOrderNo: (req: WalletOrderQueryReqDTO, params: RequestParams = {}) =>
            this.request<BizResponseWalletOrderRespDTO, any>({
                path: `/walletOrder/getByOrderNo`,
                method: 'POST',
                body: req,
                type: ContentType.Json,
                ...params
            })
    };
}
