/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/** CartAddReq */
export interface CartAddReq {
	/** 加购sku */
	skuList?: CartAddSkuReq[];
}

/** CartAddSkuReq */
export interface CartAddSkuReq {
	/** 平台商品SKU */
	productSku?: string;
	/**
	 * 数量
	 * @format int32
	 */
	quantity?: number;
}

/** CartCalculatePriceReq */
export interface CartCalculatePriceReq {
	/** 购物车id */
	ids?: number[];
}

/** CartCalculatePriceResp */
export interface CartCalculatePriceResp {
	/** 购物车信息 */
	productList?: OrderProductDTO[];
	/** 商品总金额 */
	productTotalAmount?: number;
	/**
	 * 商品总数量
	 * @format int32
	 */
	productTotalQuantity?: number;
}

/** CartDeleteReq */
export interface CartDeleteReq {
	/** 购物车id */
	ids?: number[];
}

/** CartUpdateReq */
export interface CartUpdateReq {
	/**
	 * 购物车id
	 * @format int64
	 */
	id?: number;
	/**
	 * 数量
	 * @format int32
	 */
	quantity?: number;
}

/** OrderAdminSearchResp */
export interface OrderAdminSearchResp {
	/** 实付总金额(支付回调) */
	actualAmount?: number;
	/**
	 * 代采店铺id
	 * @format int64
	 */
	agencyPurchaseShopId?: number;
	/** 扩展字段 */
	bizExt?: string;
	/**
	 * 取消时间
	 * @format date-time
	 */
	cancelTime?: string;
	/** 关闭原因 */
	closeReason?: string;
	/**
	 * 关闭时间
	 * @format date-time
	 */
	closeTime?: string;
	/**
	 * 完成时间
	 * @format date-time
	 */
	completeTime?: string;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createdTime?: string;
	/**
	 * 客户id
	 * @format int64
	 */
	customerId?: number;
	/** 客户名称 */
	customerName?: string;
	/** 用户备注 */
	customerRemark?: string;
	/**
	 * 直采店铺id
	 * @format int64
	 */
	customerShopId?: number;
	/**
	 * 发货时间
	 * @format date-time
	 */
	deliverTime?: string;
	/** 优惠金额 */
	discountAmount?: number;
	/** @format int64 */
	id?: number;
	/** 国际运费金额 */
	internationalShippingAmount?: number;
	/** 物流信息 */
	logisticsList?: string[];
	/** 子订单列表 */
	orderItemList?: OrderItemDTO[];
	/** 主订单编号 */
	orderNo?: string;
	/**
	 * 订单状态(0：待支付，1：已支付(待发货)，2：已发货，3：已取消，4：已关闭)
	 * @format int32
	 */
	orderStatus?: number;
	/** 订单类型：枚举值，b2b/d2c */
	orderType?: string;
	/** 外部主订单编号(代采主订单编号) */
	outOrderNo?: string;
	/** 支付单信息 */
	payDetail?: string;
	/**
	 * 支付时间
	 * @format date-time
	 */
	payTime?: string;
	/** 支付类型 */
	payTypeCode?: string;
	/** 应付总金额(商品总金额+国际运费-优惠金额) */
	payableAmount?: number;
	/** 下代采订单失败原因 */
	performErrorMessage?: string;
	/**
	 * 履约状态 0 未履约 1 下代采单成功 2 下代采单失败
	 * @format int32
	 */
	performStatus?: number;
	/** 下单人姓名 */
	placeOrderName?: string;
	/**
	 * 商品总数量
	 * @format int32
	 */
	productNum?: number;
	/** 商品总金额 */
	productTotalAmount?: number;
	/** 收货人姓名 */
	receiveName?: string;
	/** 所属店铺 */
	shopName?: string;
	/**
	 * 渠道来源 1 pc客户端 2 h5
	 * @format int32
	 */
	sourceChannel?: number;
	/** 站点代码(日本:JapanStation,韩国:KoreaStation,英国:UkStation) */
	stationCode?: string;
	/**
	 * 超时时间
	 * @format date-time
	 */
	timeoutTime?: string;
	/**
	 * 交易类型(1:普通)
	 * @format int32
	 */
	tradeType?: number;
	/**
	 * 更新时间
	 * @format date-time
	 */
	updatedTime?: string;
	/** 仓库编码 */
	wareCode?: string;
	/** 仓库名称 */
	wareName?: string;
}

/** OrderCalculatePriceReq */
export interface OrderCalculatePriceReq {
	/** 购物车id */
	cartIds?: number[];
	/** 订单收货地址 */
	orderReceivingAddress?: OrderReceivingAddressDTO;
	/** 商品信息(商详页下单) */
	productList?: ProductReq[];
}

/** OrderCalculatePriceResp */
export interface OrderCalculatePriceResp {
	/** 包邮金额 */
	freeShippingAmount?: number;
	/** 国际运费金额 */
	internationalShippingAmount?: number;
	/** 应付金额 */
	payableAmount?: number;
	/** 商品列表 */
	productList?: OrderProductDTO[];
	/** 商品总金额 */
	productTotalAmount?: number;
	/**
	 * 商品总数量
	 * @format int32
	 */
	productTotalQuantity?: number;
}

/** OrderCancelPopupsConfirmReq */
export interface OrderCancelPopupsConfirmReq {
	/**
	 * 确认类型 1 立即查看 2 稍后查看
	 * @format int32
	 */
	confirmType?: number;
}

/** OrderCancelPopupsResp */
export interface OrderCancelPopupsResp {
	/** 取消原因 */
	cancelReason?: string;
	/** 取消订单列表 */
	orderList?: OrderDTO[];
}

/** OrderCancelReq */
export interface OrderCancelReq {
	/** 取消原因 */
	cancelReason?: string;
	/** 直采订单号 */
	orderNo?: string;
	/** 代采子订单号 */
	systemOrderNo?: string;
}

/** OrderCreateReq */
export interface OrderCreateReq {
	/** 购物车id */
	cartIds?: number[];
	/** 客户备注 */
	customerRemark?: string;
	/** 订单收货地址 */
	orderReceivingAddress?: OrderReceivingAddressDTO;
	/** 下单人地址 */
	placeOrderAddress?: PlaceOrderAddressDTO;
	/** 商品信息(商详页下单) */
	productList?: ProductReq[];
	/**
	 * 渠道来源 1 pc客户端 2 h5
	 * @format int32
	 */
	sourceChannel?: number;
}

/** OrderCreateResp */
export interface OrderCreateResp {
	/** 订单号 */
	orderNo?: string;
	/** 应付总金额 */
	payableAmount?: number;
}

/** OrderDTO */
export interface OrderDTO {
	/** 实付总金额(支付回调) */
	actualAmount?: number;
	/**
	 * 代采店铺id
	 * @format int64
	 */
	agencyPurchaseShopId?: number;
	/** 扩展字段 */
	bizExt?: string;
	/**
	 * 取消时间
	 * @format date-time
	 */
	cancelTime?: string;
	/** 关闭原因 */
	closeReason?: string;
	/**
	 * 关闭时间
	 * @format date-time
	 */
	closeTime?: string;
	/**
	 * 完成时间
	 * @format date-time
	 */
	completeTime?: string;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createdTime?: string;
	/**
	 * 客户id
	 * @format int64
	 */
	customerId?: number;
	/** 客户名称 */
	customerName?: string;
	/** 用户备注 */
	customerRemark?: string;
	/**
	 * 直采店铺id
	 * @format int64
	 */
	customerShopId?: number;
	/**
	 * 发货时间
	 * @format date-time
	 */
	deliverTime?: string;
	/** 优惠金额 */
	discountAmount?: number;
	/** @format int64 */
	id?: number;
	/** 国际运费金额 */
	internationalShippingAmount?: number;
	/** 子订单列表 */
	orderItemList?: OrderItemDTO[];
	/** 主订单编号 */
	orderNo?: string;
	/**
	 * 订单状态(0：待支付，1：已支付(待发货)，2：已发货，3：已取消，4：已关闭)
	 * @format int32
	 */
	orderStatus?: number;
	/** 订单类型：枚举值，b2b/d2c */
	orderType?: string;
	/** 外部主订单编号(代采主订单编号) */
	outOrderNo?: string;
	/** 支付单信息 */
	payDetail?: string;
	/**
	 * 支付时间
	 * @format date-time
	 */
	payTime?: string;
	/** 支付类型 */
	payTypeCode?: string;
	/** 应付总金额(商品总金额+国际运费-优惠金额) */
	payableAmount?: number;
	/** 下代采订单失败原因 */
	performErrorMessage?: string;
	/**
	 * 履约状态 0 未履约 1 下代采单成功 2 下代采单失败
	 * @format int32
	 */
	performStatus?: number;
	/** 下单人姓名 */
	placeOrderName?: string;
	/**
	 * 商品总数量
	 * @format int32
	 */
	productNum?: number;
	/** 商品总金额 */
	productTotalAmount?: number;
	/** 收货人姓名 */
	receiveName?: string;
	/**
	 * 渠道来源 1 pc客户端 2 h5
	 * @format int32
	 */
	sourceChannel?: number;
	/** 站点代码(日本:JapanStation,韩国:KoreaStation,英国:UkStation) */
	stationCode?: string;
	/**
	 * 超时时间
	 * @format date-time
	 */
	timeoutTime?: string;
	/**
	 * 交易类型(1:普通)
	 * @format int32
	 */
	tradeType?: number;
	/**
	 * 更新时间
	 * @format date-time
	 */
	updatedTime?: string;
	/** 仓库编码 */
	wareCode?: string;
	/** 仓库名称 */
	wareName?: string;
}

/** OrderDeliverReq */
export interface OrderDeliverReq {
	/** 发货信息 */
	logisticsList?: OrderLogisticsReq[];
	/** 直采订单号 */
	orderNo?: string;
	/** 代采子订单号 */
	systemOrderNo?: string;
}

/** OrderDetailDTO */
export interface OrderDetailDTO {
	/** 实付总金额(支付回调) */
	actualAmount?: number;
	/**
	 * 代采店铺id
	 * @format int64
	 */
	agencyPurchaseShopId?: number;
	/** 扩展字段 */
	bizExt?: string;
	/**
	 * 取消时间
	 * @format date-time
	 */
	cancelTime?: string;
	/** 关闭原因 */
	closeReason?: string;
	/**
	 * 关闭时间
	 * @format date-time
	 */
	closeTime?: string;
	/**
	 * 完成时间
	 * @format date-time
	 */
	completeTime?: string;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createdTime?: string;
	/**
	 * 客户id
	 * @format int64
	 */
	customerId?: number;
	/** 客户名称 */
	customerName?: string;
	/** 用户备注 */
	customerRemark?: string;
	/**
	 * 直采店铺id
	 * @format int64
	 */
	customerShopId?: number;
	/**
	 * 发货时间
	 * @format date-time
	 */
	deliverTime?: string;
	/** 优惠金额 */
	discountAmount?: number;
	/** @format int64 */
	id?: number;
	/** 国际运费金额 */
	internationalShippingAmount?: number;
	/** 订单物流信息 */
	logisticsList?: OrderLogisticsDTO[];
	/** 子订单列表 */
	orderItemList?: OrderItemDTO[];
	/** 主订单编号 */
	orderNo?: string;
	/**
	 * 订单状态(0：待支付，1：已支付(待发货)，2：已发货，3：已取消，4：已关闭)
	 * @format int32
	 */
	orderStatus?: number;
	/** 订单类型：枚举值，b2b/d2c */
	orderType?: string;
	/** 外部主订单编号(代采主订单编号) */
	outOrderNo?: string;
	/** 支付单信息 */
	payDetail?: string;
	/**
	 * 支付时间
	 * @format date-time
	 */
	payTime?: string;
	/** 支付类型 */
	payTypeCode?: string;
	/** 应付总金额(商品总金额+国际运费-优惠金额) */
	payableAmount?: number;
	/** 下代采订单失败原因 */
	performErrorMessage?: string;
	/**
	 * 履约状态 0 未履约 1 下代采单成功 2 下代采单失败
	 * @format int32
	 */
	performStatus?: number;
	/** 下单人地址 */
	placeOrderAddress?: PlaceOrderAddressDTO;
	/** 下单人姓名 */
	placeOrderName?: string;
	/**
	 * 商品总数量
	 * @format int32
	 */
	productNum?: number;
	/** 商品总金额 */
	productTotalAmount?: number;
	/** 收货人姓名 */
	receiveName?: string;
	/** 订单收货地址 */
	receivingAddress?: OrderReceivingAddressDTO;
	/**
	 * 渠道来源 1 pc客户端 2 h5
	 * @format int32
	 */
	sourceChannel?: number;
	/** 站点代码(日本:JapanStation,韩国:KoreaStation,英国:UkStation) */
	stationCode?: string;
	/**
	 * 超时时间
	 * @format date-time
	 */
	timeoutTime?: string;
	/**
	 * 交易类型(1:普通)
	 * @format int32
	 */
	tradeType?: number;
	/**
	 * 更新时间
	 * @format date-time
	 */
	updatedTime?: string;
	/** 仓库编码 */
	wareCode?: string;
	/** 仓库名称 */
	wareName?: string;
}

/** OrderItemAdminSearchResp */
export interface OrderItemAdminSearchResp {
	/** 实付总金额(支付回调) */
	actualAmount?: number;
	/**
	 * 代采店铺id
	 * @format int64
	 */
	agencyPurchaseShopId?: number;
	/** 扩展字段 */
	bizExt?: string;
	/**
	 * 取消时间
	 * @format date-time
	 */
	cancelTime?: string;
	/** 关闭原因 */
	closeReason?: string;
	/**
	 * 关闭时间
	 * @format date-time
	 */
	closeTime?: string;
	/**
	 * 完成时间
	 * @format date-time
	 */
	completeTime?: string;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createdTime?: string;
	/**
	 * 客户id
	 * @format int64
	 */
	customerId?: number;
	/** 客户名称 */
	customerName?: string;
	/** 用户备注 */
	customerRemark?: string;
	/**
	 * 直采店铺id
	 * @format int64
	 */
	customerShopId?: number;
	/**
	 * 发货时间
	 * @format date-time
	 */
	deliverTime?: string;
	/** 优惠金额 */
	discountAmount?: number;
	/** @format int64 */
	id?: number;
	/** 国际运费金额 */
	internationalShippingAmount?: number;
	/**
	 * 起订量单价
	 * @format int32
	 */
	minimumPrice?: number;
	/**
	 * 起订量
	 * @format int32
	 */
	minimumQuantity?: number;
	/** 子订单列表 */
	orderItemList?: OrderItemDTO[];
	/** 主订单编号 */
	orderNo?: string;
	/**
	 * 订单状态(0：待支付，1：已支付(待发货)，2：已发货，3：已取消，4：已关闭)
	 * @format int32
	 */
	orderStatus?: number;
	/** 订单类型：枚举值，b2b/d2c */
	orderType?: string;
	/** 外部主订单编号(代采主订单编号) */
	outOrderNo?: string;
	/** 支付单信息 */
	payDetail?: string;
	/**
	 * 支付时间
	 * @format date-time
	 */
	payTime?: string;
	/** 支付类型 */
	payTypeCode?: string;
	/** 应付总金额(商品总金额+国际运费-优惠金额) */
	payableAmount?: number;
	/** 下代采订单失败原因 */
	performErrorMessage?: string;
	/**
	 * 履约状态 0 未履约 1 下代采单成功 2 下代采单失败
	 * @format int32
	 */
	performStatus?: number;
	/** 下单人姓名 */
	placeOrderName?: string;
	/** 直采商详链接 */
	productDetailUrl?: string;
	/**
	 * 商品总数量
	 * @format int32
	 */
	productNum?: number;
	/** 商品总金额 */
	productTotalAmount?: number;
	/** 收货人姓名 */
	receiveName?: string;
	/**
	 * 渠道来源 1 pc客户端 2 h5
	 * @format int32
	 */
	sourceChannel?: number;
	/** 站点代码(日本:JapanStation,韩国:KoreaStation,英国:UkStation) */
	stationCode?: string;
	/** 1688连接 */
	thirdProductDetailUrl?: string;
	/**
	 * 超时时间
	 * @format date-time
	 */
	timeoutTime?: string;
	/**
	 * 交易类型(1:普通)
	 * @format int32
	 */
	tradeType?: number;
	/**
	 * 更新时间
	 * @format date-time
	 */
	updatedTime?: string;
	/** 仓库编码 */
	wareCode?: string;
	/** 仓库名称 */
	wareName?: string;
}

/** OrderItemDTO */
export interface OrderItemDTO {
	/** 实付金额 */
	actualAmount?: number;
	/** 扩展字段 */
	bizExt?: string;
	/** 取消原因 */
	cancelReason?: string;
	/**
	 * 取消时间
	 * @format date-time
	 */
	cancelTime?: string;
	/**
	 * 商品后台一级类目
	 * @format int64
	 */
	cateCodeLevel1?: number;
	/**
	 * 商品后台二级类目
	 * @format int64
	 */
	cateCodeLevel2?: number;
	/**
	 * 商品后台三级类目
	 * @format int64
	 */
	cateCodeLevel3?: number;
	/**
	 * 创建时间
	 * @format date-time
	 */
	createdTime?: string;
	/**
	 * 发货时间
	 * @format date-time
	 */
	deliverTime?: string;
	/** 优惠金额 */
	discountAmount?: number;
	/** 汇率 */
	exchangeRateSnapshot?: number;
	/** @format int64 */
	id?: number;
	/** 子订单编号 */
	orderItemNo?: string;
	/**
	 * 子订单状态(0:待支付，1:待发货，2：已发货，3：已取消，4：已关闭)
	 * @format int32
	 */
	orderItemStatus?: number;
	/** 主订单编号 */
	orderNo?: string;
	/** 外部子订单编号(代采子订单编号) */
	outOrderItemNo?: string;
	/** 应付金额(商品总金额-优惠金额) */
	payableAmount?: number;
	/** 平台商品SPU */
	productCode?: string;
	/** 商品图片 */
	productImg?: string;
	/** 商品名称 */
	productName?: string;
	/** 商品金额 */
	productPrice?: number;
	/** 商品规格 */
	productPropertiesName?: string;
	/**
	 * 下单数量
	 * @format int32
	 */
	productQuantity?: number;
	/** 平台商品SKU */
	productSku?: string;
	/** 商品快照,json格式 */
	productSnapshot?: string;
	/** 商品总金额(数量*单价) */
	productTotalAmount?: number;
	/** 站点代码(日本:JapanStation,韩国:KoreaStation,英国:UkStation) */
	stationCode?: string;
	/**
	 * 更新时间
	 * @format date-time
	 */
	updatedTime?: string;
}

/** OrderLogisticsDTO */
export interface OrderLogisticsDTO {
	/** 扩展字段 */
	bizExt?: string;
	/**
	 * 生成时间
	 * @format date-time
	 */
	createDeliveryTaskTime?: string;
	/**
	 * 发货状态(0:待处理 1:拣货中 2:打标中 3:打包中 4:打包完成 5:已发货 10:已取消)
	 * @format int32
	 */
	deliveryStatus?: number;
	/** 发货编号 */
	deliveryTaskCode?: string;
	/**
	 * 发货时间
	 * @format date-time
	 */
	deliveryTime?: string;
	/**
	 * id
	 * @format int64
	 */
	id?: number;
	/** 国际运单号 */
	logisticsCode?: string;
	/** 订单号 */
	orderNo?: string;
	/** 发货商品信息 */
	productList?: OrderLogisticsProductDTO[];
	/** 站点代码(日本:JapanStation,韩国:KoreaStation,英国:UkStation) */
	stationCode?: string;
	/** 运输方式 */
	transportMode?: string;
}

/** OrderLogisticsProductDTO */
export interface OrderLogisticsProductDTO {
	/** 扩展字段 */
	bizExt?: string;
	/**
	 * 发货数量
	 * @format int32
	 */
	deliveryNum?: number;
	/**
	 * id
	 * @format int64
	 */
	id?: number;
	/**
	 * 订单物流信息id
	 * @format int64
	 */
	orderLogisticsId?: number;
	/** 平台商品SPU */
	productCode?: string;
	/** 商品图片 */
	productImg?: string;
	/** 商品单价 */
	productPrice?: number;
	/** 商品规格 */
	productPropertiesName?: string;
	/** 平台商品SKU */
	productSku?: string;
	/** 合计金额 */
	productTotalAmount?: number;
	/** 站点代码(日本:JapanStation,韩国:KoreaStation,英国:UkStation) */
	stationCode?: string;
}

/** OrderLogisticsProductReq */
export interface OrderLogisticsProductReq {
	/**
	 * 发货数量
	 * @format int32
	 */
	deliveryNum?: number;
	/** 平台商品SPU */
	productCode?: string;
	/** 平台商品SKU */
	productSku?: string;
}

/** OrderLogisticsReq */
export interface OrderLogisticsReq {
	/**
	 * 生成时间
	 * @format date-time
	 */
	createDeliveryTaskTime?: string;
	/**
	 * 发货状态(0:待处理 1:拣货中 2:打标中 3:打包中 4:打包完成 5:已发货 10:已取消)
	 * @format int32
	 */
	deliveryStatus?: number;
	/** 发货编号 */
	deliveryTaskCode?: string;
	/**
	 * 发货时间
	 * @format date-time
	 */
	deliveryTime?: string;
	/** 国际运单号 */
	logisticsCode?: string;
	/** 发货商品信息 */
	productList?: OrderLogisticsProductReq[];
	/** 运输方式 */
	transportMode?: string;
}

/** OrderProductDTO */
export interface OrderProductDTO {
	/**
	 * 是否到达起订量
	 * @format int32
	 */
	arrivalMinimumQuantityFlag?: number;
	/**
	 * 购物车id
	 * @format int64
	 */
	cartId?: number;
	/**
	 * 商品后台一级类目
	 * @format int64
	 */
	cateCodeLevel1?: number;
	/**
	 * 商品后台二级类目
	 * @format int64
	 */
	cateCodeLevel2?: number;
	/**
	 * 商品后台三级类目
	 * @format int64
	 */
	cateCodeLevel3?: number;
	/** 包邮金额 */
	freeShippingAmount?: number;
	/**
	 * 起订量
	 * @format int32
	 */
	minimumQuantity?: number;
	/**
	 * 是否一元购商品 0 否 1 是
	 * @format int32
	 */
	oneBuyFlag?: number;
	/** 商品spu */
	productCode?: string;
	/** 商品图片 */
	productImg?: string;
	/** 商品名称 */
	productName?: string;
	/** 商品价格 */
	productPrice?: number;
	/** 商品规格名称 */
	productPropertiesName?: string;
	/** 商品sku */
	productSku?: string;
	/**
	 * 商品库存
	 * @format int32
	 */
	productStockQuantity?: number;
	/** 商品总价 */
	productTotalAmount?: number;
	/** 商品属性 */
	productType?: string;
	/**
	 * 商品重量
	 * @format double
	 */
	productWeight?: number;
	/**
	 * 数量
	 * @format int32
	 */
	quantity?: number;
	/**
	 * 商品售卖状态 1 正常 2 售罄 3 下架
	 * @format int32
	 */
	sellStatus?: number;
	/**
	 * spu数量
	 * @format int32
	 */
	spuQuantity?: number;
}

/** OrderReceivingAddressDTO */
export interface OrderReceivingAddressDTO {
	/** 地址 */
	address?: string;
	/**
	 * 市id
	 * @format int64
	 */
	cityId?: number;
	/** 市(日文) */
	cityName?: string;
	/** 国家代码 */
	countryCode?: string;
	/** 国家id */
	countryId?: string;
	/** 国家(日文) */
	countryName?: string;
	/** 邮箱 */
	email?: string;
	/** 订单号 */
	orderNo?: string;
	/** 邮政编码 */
	postalCode?: string;
	/** 省id */
	provinceId?: string;
	/** 省(日文) */
	provinceName?: string;
	/** 收货人名称 */
	receiveName?: string;
	/** 收货人电话或者手机 */
	receiveTel?: string;
}

/** OrderSearchDetailReq */
export interface OrderSearchDetailReq {
	/** 订单号 */
	orderNo?: string;
}

/** OrderSearchReq */
export interface OrderSearchReq {
	/** 发货时间结束 */
	deliverTimeEnd?: string;
	/** 发货时间开始 */
	deliverTimeStart?: string;
	/** 订单创建时间结束 */
	orderCreateTimeEnd?: string;
	/**
	 * 订单创建时间开始
	 * @format date-time
	 */
	orderCreateTimeStart?: string;
	/**
	 * 子订单状态(0:待支付，1:待发货，2：已发货，3：已取消，4：已关闭)
	 * @format int32
	 */
	orderItemStatus?: number;
	/** 订单号 */
	orderNos?: string[];
	/**
	 * 主订单状态(0:待支付，1:待发货，2：已发货，3：已取消，4：已关闭)
	 * @format int32
	 */
	orderStatus?: number;
	/** @format int32 */
	pageNum?: number;
	/** @format int32 */
	pageSize?: number;
	/** 商品名称 */
	productName?: string;
	/** sku */
	productSkus?: string[];
	/** 收货人名称 */
	receiveName?: string;
	/** orderNo/商品名称/sku */
	searchKey?: string;
	/** @format int32 */
	startIndex?: number;
}

/** PlaceOrderAddressDTO */
export interface PlaceOrderAddressDTO {
	/** 地址 */
	address?: string;
	/** 地址(英文) */
	addressEn?: string;
	/**
	 * 市id
	 * @format int64
	 */
	cityId?: number;
	/** 市(日文) */
	cityName?: string;
	/** 市(英文) */
	cityNameEn?: string;
	/**
	 * 通关类型 1 企业 2 用户
	 * @format int32
	 */
	clearanceType?: number;
	/** 会社编号 */
	companyNo?: string;
	/** 国家代码 */
	countryCode?: string;
	/** 国家id */
	countryId?: string;
	/** 国家(日文) */
	countryName?: string;
	/** 邮箱 */
	email?: string;
	/** 订单号 */
	orderNo?: string;
	/** 公司名 */
	placeOrderCompanyName?: string;
	/** 公司名(英文) */
	placeOrderCompanyNameEn?: string;
	/** 姓名 */
	placeOrderName?: string;
	/** 姓名(英文) */
	placeOrderNameEn?: string;
	/** 邮政编码 */
	postalCode?: string;
	/** 省id */
	provinceId?: string;
	/** 省(日文) */
	provinceName?: string;
	/** 省(英文) */
	provinceNameEn?: string;
	/** 联系电话 */
	tel?: string;
}

/** ProductReq */
export interface ProductReq {
	/** 平台商品SPU */
	productCode?: string;
	/** 平台商品SKU */
	productSku?: string;
	/**
	 * 商品数量
	 * @format int32
	 */
	quantity?: number;
}

/** UserOrderCloseReq */
export interface UserOrderCloseReq {
	/** 订单号 */
	orderNo?: string;
}

/** UserPlaceOrderAddressDTO */
export interface UserPlaceOrderAddressDTO {
	/** 地址 */
	address?: string;
	/** 地址(英文) */
	addressEn?: string;
	/**
	 * 市id
	 * @format int64
	 */
	cityId?: number;
	/** 市(日文) */
	cityName?: string;
	/** 市(英文) */
	cityNameEn?: string;
	/**
	 * 通关类型 1 企业 2 用户
	 * @format int32
	 */
	clearanceType?: number;
	/** 会社编号 */
	companyNo?: string;
	/** 国家代码 */
	countryCode?: string;
	/** 国家id */
	countryId?: string;
	/** 国家(日文) */
	countryName?: string;
	/**
	 * id
	 * @format int64
	 */
	customerId?: number;
	/**
	 * 是否默认 0 否 1 是
	 * @format int32
	 */
	defaultFlag?: number;
	/** 邮箱 */
	email?: string;
	/**
	 * id
	 * @format int64
	 */
	id?: number;
	/** 公司名 */
	placeOrderCompanyName?: string;
	/** 公司名(英文) */
	placeOrderCompanyNameEn?: string;
	/** 姓名 */
	placeOrderName?: string;
	/** 姓名(英文) */
	placeOrderNameEn?: string;
	/** 邮政编码 */
	postalCode?: string;
	/** 省id */
	provinceId?: string;
	/** 省(日文) */
	provinceName?: string;
	/** 省(英文) */
	provinceNameEn?: string;
	stationCode?: string;
	/** 联系电话 */
	tel?: string;
}

/** UserReceivingAddressDTO */
export interface UserReceivingAddressDTO {
	/** 地址 */
	address?: string;
	/**
	 * 市
	 * @format int64
	 */
	cityId?: number;
	/** 市(日文) */
	cityName?: string;
	/** 国家代码 */
	countryCode?: string;
	/** 国家 */
	countryId?: string;
	/** 国家(日文) */
	countryName?: string;
	/**
	 * 客户id
	 * @format int64
	 */
	customerId?: number;
	/**
	 * 是否默认 0 否 1 是
	 * @format int32
	 */
	defaultFlag?: number;
	/** 邮箱 */
	email?: string;
	/**
	 * id
	 * @format int64
	 */
	id?: number;
	/** 邮政编码 */
	postalCode?: string;
	/** 省 */
	provinceId?: string;
	/** 省(日文) */
	provinceName?: string;
	/** 收货人名称 */
	receiveName?: string;
	/** 收货人电话或者手机 */
	receiveTel?: string;
	stationCode?: string;
}

/** BizResponse«CartCalculatePriceResp» */
export interface BizResponseCartCalculatePriceResp {
	code?: string;
	data?: CartCalculatePriceResp;
	msg?: string;
	success?: boolean;
}

/** BizResponse«List«OrderItemAdminSearchResp»» */
export interface BizResponseListOrderItemAdminSearchResp {
	code?: string;
	data?: OrderItemAdminSearchResp[];
	msg?: string;
	success?: boolean;
}

/** BizResponse«List«OrderLogisticsDTO»» */
export interface BizResponseListOrderLogisticsDTO {
	code?: string;
	data?: OrderLogisticsDTO[];
	msg?: string;
	success?: boolean;
}

/** BizResponse«List«OrderProductDTO»» */
export interface BizResponseListOrderProductDTO {
	code?: string;
	data?: OrderProductDTO[];
	msg?: string;
	success?: boolean;
}

/** BizResponse«List«UserPlaceOrderAddressDTO»» */
export interface BizResponseListUserPlaceOrderAddressDTO {
	code?: string;
	data?: UserPlaceOrderAddressDTO[];
	msg?: string;
	success?: boolean;
}

/** BizResponse«List«UserReceivingAddressDTO»» */
export interface BizResponseListUserReceivingAddressDTO {
	code?: string;
	data?: UserReceivingAddressDTO[];
	msg?: string;
	success?: boolean;
}

/** BizResponse«OrderCalculatePriceResp» */
export interface BizResponseOrderCalculatePriceResp {
	code?: string;
	data?: OrderCalculatePriceResp;
	msg?: string;
	success?: boolean;
}

/** BizResponse«OrderCancelPopupsResp» */
export interface BizResponseOrderCancelPopupsResp {
	code?: string;
	data?: OrderCancelPopupsResp;
	msg?: string;
	success?: boolean;
}

/** BizResponse«OrderCreateResp» */
export interface BizResponseOrderCreateResp {
	code?: string;
	data?: OrderCreateResp;
	msg?: string;
	success?: boolean;
}

/** BizResponse«OrderDTO» */
export interface BizResponseOrderDTO {
	code?: string;
	data?: OrderDTO;
	msg?: string;
	success?: boolean;
}

/** BizResponse«OrderDetailDTO» */
export interface BizResponseOrderDetailDTO {
	code?: string;
	data?: OrderDetailDTO;
	msg?: string;
	success?: boolean;
}

/** BizResponse«PageResult«OrderDTO»» */
export interface BizResponsePageResultOrderDTO {
	code?: string;
	data?: PageResultOrderDTO;
	msg?: string;
	success?: boolean;
}

/** BizResponse«PlaceOrderAddressDTO» */
export interface BizResponsePlaceOrderAddressDTO {
	code?: string;
	data?: PlaceOrderAddressDTO;
	msg?: string;
	success?: boolean;
}

/** BizResponse«UserReceivingAddressDTO» */
export interface BizResponseUserReceivingAddressDTO {
	code?: string;
	data?: UserReceivingAddressDTO;
	msg?: string;
	success?: boolean;
}

/** BizResponse«Void» */
export interface BizResponseVoid {
	code?: string;
	msg?: string;
	success?: boolean;
}

/** BizResponse«object» */
export interface BizResponseObject {
	code?: string;
	data?: object;
	msg?: string;
	success?: boolean;
}

/** PageResult«OrderAdminSearchResp» */
export interface PageResultOrderAdminSearchResp {
	/** @format int64 */
	current?: number;
	records?: OrderAdminSearchResp[];
	/** @format int64 */
	size?: number;
	/** @format int64 */
	total?: number;
	/** @format int64 */
	totalPage?: number;
}

/** PageResult«OrderDTO» */
export interface PageResultOrderDTO {
	/** @format int64 */
	current?: number;
	records?: OrderDTO[];
	/** @format int64 */
	size?: number;
	/** @format int64 */
	total?: number;
	/** @format int64 */
	totalPage?: number;
}

import axios, { AxiosInstance, AxiosRequestConfig, HeadersDefaults, ResponseType } from 'axios';

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, 'data' | 'params' | 'url' | 'responseType'> {
	/** set parameter to `true` for call `securityWorker` for this request */
	secure?: boolean;
	/** request path */
	path: string;
	/** content type of request body */
	type?: ContentType;
	/** query params */
	query?: QueryParamsType;
	/** format of response (i.e. response.json() -> format: "json") */
	format?: ResponseType;
	/** request body */
	body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, 'data' | 'cancelToken'> {
	securityWorker?: (
		securityData: SecurityDataType | null
	) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
	secure?: boolean;
	format?: ResponseType;
}

export enum ContentType {
	Json = 'application/json',
	FormData = 'multipart/form-data',
	UrlEncoded = 'application/x-www-form-urlencoded',
	Text = 'text/plain'
}

export class HttpClient<SecurityDataType = unknown> {
	public instance: AxiosInstance;
	private securityData: SecurityDataType | null = null;
	private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
	private secure?: boolean;
	private format?: ResponseType;

	constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
		this.instance = axios.create({
			...axiosConfig,
			baseURL: axiosConfig.baseURL || '//192.168.102.99:60089/easy/order'
		});
		this.secure = secure;
		this.format = format;
		this.securityWorker = securityWorker;
	}

	public setSecurityData = (data: SecurityDataType | null) => {
		this.securityData = data;
	};

	protected mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
		const method = params1.method || (params2 && params2.method);

		return {
			...this.instance.defaults,
			...params1,
			...(params2 || {}),
			headers: {
				...((method && this.instance.defaults.headers[method.toLowerCase() as keyof HeadersDefaults]) || {}),
				...(params1.headers || {}),
				...((params2 && params2.headers) || {})
			}
		};
	}

	protected stringifyFormItem(formItem: unknown) {
		if (typeof formItem === 'object' && formItem !== null) {
			return JSON.stringify(formItem);
		} else {
			return `${formItem}`;
		}
	}

	protected createFormData(input: Record<string, unknown>): FormData {
		return Object.keys(input || {}).reduce((formData, key) => {
			const property = input[key];
			const propertyContent: any[] = property instanceof Array ? property : [property];

			for (const formItem of propertyContent) {
				const isFileType = formItem instanceof Blob || formItem instanceof File;
				formData.append(key, isFileType ? formItem : this.stringifyFormItem(formItem));
			}

			return formData;
		}, new FormData());
	}

	public request = async <T = any, _E = any>({
		secure,
		path,
		type,
		query,
		format,
		body,
		...params
	}: FullRequestParams): Promise<T> => {
		const secureParams =
			((typeof secure === 'boolean' ? secure : this.secure) &&
				this.securityWorker &&
				(await this.securityWorker(this.securityData))) ||
			{};
		const requestParams = this.mergeRequestParams(params, secureParams);
		const responseFormat = format || this.format || undefined;

		if (type === ContentType.FormData && body && body !== null && typeof body === 'object') {
			body = this.createFormData(body as Record<string, unknown>);
		}

		if (type === ContentType.Text && body && body !== null && typeof body !== 'string') {
			body = JSON.stringify(body);
		}

		return this.instance
			.request({
				...requestParams,
				headers: {
					...(requestParams.headers || {}),
					...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {})
				},
				params: query,
				responseType: responseFormat,
				data: body,
				url: path
			})
			.then((response) => response.data);
	};
}

/**
 * @title API文档
 * @version v1.0.0
 * @baseUrl //192.168.102.99:60089/easy/order
 * @contact 史尼芙 (https://www.taobaockb.com/)
 *
 * 史尼芙API文档
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
	probe = {
		/**
		 * No description
		 *
		 * @tags livens-probe-controller
		 * @name Test
		 * @summary 执行一次健康检查探针
		 * @request GET:/Probe/test
		 */
		test: (params: RequestParams = {}) =>
			this.request<BizResponseObject, any>({
				path: `/Probe/test`,
				method: 'GET',
				...params
			})
	};
	userPlaceOrderAddress = {
		/**
		 * No description
		 *
		 * @tags 用户下单人相关接口
		 * @name Delete
		 * @summary 删除下单人地址
		 * @request POST:/userPlaceOrderAddress/delete
		 */
		delete: (req: UserPlaceOrderAddressDTO, params: RequestParams = {}) =>
			this.request<BizResponseVoid, any>({
				path: `/userPlaceOrderAddress/delete`,
				method: 'POST',
				body: req,
				type: ContentType.Json,
				...params
			})
	};
}
