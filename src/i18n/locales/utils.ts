/*
 * @Author: shiguang
 * @Date: 2023-05-05 13:18:52
 * @LastEditors: huajian
 * @LastEditTime: 2023-05-18 10:29:27
 * @Description: utils
 */

type MultipleLangConfigJSON = Record<string, string | Record<string, string>>;
type ConfigLang = 'JP' | 'zh_CN';

const initLang = (configJSON: MultipleLangConfigJSON, lang: ConfigLang) => {
	return Object.keys(configJSON).reduce((pre, cur) => {
		pre[cur] = configJSON[cur]?.[lang];
		return pre;
	}, {} as MultipleLangConfigJSON);
};

export const createConfig = (_config: MultipleLangConfigJSON) => {
	const config = ['zh-CN', 'JP'].reduce(
		(pre, cur) => {
			pre[cur] = initLang(
				_config,
				cur.split('-').join('_') as ConfigLang
			);
			return pre;
		},
		{} as Record<string, MultipleLangConfigJSON>
	);
	return config;
};

export const configToResources = (config: ReturnType<typeof createConfig>) => {
	return Object.keys(config).reduce(
		(pre, cur) => {
			pre[cur] = {
				translation: config[cur]
			};
			return pre;
		},
		{} as Record<string, { translation: MultipleLangConfigJSON }>
	);
};

export const format = (_config: MultipleLangConfigJSON) => {
	return Object.keys(_config).reduce((pre, cur) => {
		pre[cur] = {
			zh_CN: cur,
			JP: _config[cur]
		};
		return pre;
	}, {} as any);
};
