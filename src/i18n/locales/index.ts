/*
 * @Author: shiguang
 * @Date: 2023-05-05 10:49:54
 * @LastEditors: huajian
 * @LastEditTime: 2023-05-24 14:11:25
 * @Description: locales index
 */
import locales from './locales';
import { configToResources, createConfig, format } from './utils';
export const config = createConfig({ ...format(locales) });
export const resources = configToResources(config);
