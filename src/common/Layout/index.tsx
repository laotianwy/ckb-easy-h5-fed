/*
 * @Author: shiguang
 * @Date: 2023-04-26 20:23:35
 * @LastEditors: yusha
 * @LastEditTime: 2023-10-31 10:24:22
 * @Description: 自定义tabbar
 */
import React, { ReactNode, useEffect, useState } from 'react';
import { TabBar } from 'antd-mobile';
import { LeftOutline } from 'antd-mobile-icons';
import { useLocation, useNavigate, useNavigationType } from 'react-router-dom';
import Return from '@/common/icons/Return';
import ShoppingCartLarge from '../icons/ShoppingCartLarge';
import Avatar from '../icons/Avatar';
interface CommonLayoutProps {
	children?: (xxx: any) => React.ReactNode;
	/** 标题 */
	title?: string;
	/** 是否展示tabbar */
	showTabBar?: boolean;
	/** 是否展示头部 */
	showHeader?: boolean;
	/** 是否需要展示头部右侧自定义按钮 */
	isNeedNavBarRight?: boolean;
}
/** 页面级别共同props */
export interface PageProps {
	/** title部分的右侧自定义按钮, 默认不存在 */
	setNavBarRight: (val: ReactNode) => void;
	/** 是否展示tabbar，默认使用router中的配置 */
	setShowTabBar: (val: boolean) => void;
	/** 自定义NavBar（页面最上面标题部分） */
	setCustomNavBar: (val: ReactNode) => void;
	setScrollState: (val: any) => void;
}
const tabs = [
	{
		key: '/goods/home',
		title: window._$m.t('首页'),
		icon: (actions) => {
			return <ShoppingCartLarge />;
		}
	},
	{
		key: '/goods/classify',
		title: window._$m.t('分类'),
		icon: (actions) => {
			return <ShoppingCartLarge />;
		}
	},
	{
		key: '/order/shopcart',
		title: window._$m.t('购物车'),
		badge: 99,
		icon: (actions) => {
			return <ShoppingCartLarge />;
		}
	},
	{
		key: '/user/mine',
		title: window._$m.t('我的'),
		icon: () => {
			return <Avatar />;
		}
	}
];

const CommonLayout = (props: CommonLayoutProps) => {
	const {
		children,
		showTabBar = false,
		showHeader,
		isNeedNavBarRight = false
	} = props;
	/** 是否展示tabbar，两种情况，一种页面链接配置，一种是在当前页面去修改 */
	const [isShowTabBar, setShowTabBar] = useState<boolean>(false);
	/** NavBar右侧自定义按钮 */
	const [navBarRight, setNavBarRight] = useState<ReactNode>(null);
	/** 自定义NavBar */
	const [customNavBar, setCustomNavBar] = useState<ReactNode>(null);

	// const [scrollState, setScrollState] = useState<ScrollState>({
	// 	isNeedTopMargin: true,
	// 	isNeedBottomMargin: false
	// });

	const location = useLocation();
	const navigationType = useNavigationType();
	const { pathname } = location;
	const navigate = useNavigate();

	/** 跳转 */
	const setRouteActive = (value: string) => {
		navigate(value);
	};
	useEffect(() => {
		setShowTabBar(showTabBar);
	}, [showTabBar]);
	useEffect(() => {
		if (!isNeedNavBarRight) {
			setNavBarRight(null);
		}
	}, [isNeedNavBarRight, pathname]);
	const getNavBarRight = (val) => {
		// setRightReactNode(val);
		if (isNeedNavBarRight) {
			setNavBarRight(val);
		} else {
			setNavBarRight(null);
		}
	};
	const getCustomNavBar = () => {
		if (customNavBar) {
			return customNavBar;
		}
		return (
			<div className="layout-header">
				{window.history.length > 1 && !isShowTabBar && (
					<div
						className="left-style"
						onClick={() => {
							navigate(-1);
						}}
					>
						<Return
							style={{
								width: '0.14rem',
								height: '0.14rem'
							}}
						/>
					</div>
				)}

				<div className="title-style">{props.title ?? ''}</div>
				<div className="right-style">{navBarRight && navBarRight}</div>
			</div>
		);
	};
	return (
		// <KeepAlive
		// 	include={['/order/list', '/order/search', '/order/shopcart']}
		// >
		<div className="layout-style">
			{showHeader && <>{getCustomNavBar()}</>}
			<div className={`layout-content`}>
				{children?.({
					setNavBarRight: getNavBarRight,
					setShowTabBar,
					setCustomNavBar
					// setScrollState: getScrollState
				})}
			</div>
			{isShowTabBar && (
				<div className="layout-footer">
					<TabBar
						safeArea
						activeKey={pathname}
						onChange={(value) => {
							setRouteActive(value);
						}}
					>
						{tabs.map((item) => (
							<TabBar.Item
								badge={item.badge ?? null}
								key={item.key}
								icon={item.icon}
								title={item.title}
							/>
						))}
					</TabBar>
				</div>
			)}
		</div>
		// </KeepAlive>
	);
};
export default CommonLayout;
