/*
 * @Author: yusha
 * @Date: 2023-10-20 13:22:36
 * @LastEditors: huajian
 * @LastEditTime: 2024-01-22 23:44:55
 * @Description: 布局-footer
 */

import { TabBar } from 'antd-mobile';
import { HTMLAttributes, memo } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { useAtom } from 'jotai';
import ShoppingCartLarge from '@/common/icons/ShoppingCartLarge';
import Avatar from '@/common/icons/Avatar';
import BottomnavigationHomepageClick from '@/common/icons/BottomnavigationHomepageClick';
import BottomnavigationHomepageDefault from '@/common/icons/BottomnavigationHomepageDefault';
import './index.scss';
import BottomnavigationCategoryDefault from '@/common/icons/BottomnavigationCategoryDefault';
import BottomnavigationCategoryClick from '@/common/icons/BottomnavigationCategoryClick';
import BottomnavigationShoppingcartClick from '@/common/icons/BottomnavigationShoppingcartClick';
import BottomnavigationShoppingcartDefault from '@/common/icons/BottomnavigationShoppingcartDefault';
import BottomnavigationMineDefault from '@/common/icons/BottomnavigationMineDefault';
import BottomnavigationMineClick from '@/common/icons/BottomnavigationMineClick';
import { Order } from '@/Atoms';
import { checkLogin } from '@/config/request/login';
import { goToLogin } from '@/config/request/interceptors';
interface CustomTabbarProps extends HTMLAttributes<HTMLDivElement> {
	navBarRight?: React.ReactNode;
	title?: string;
	isFixed?: boolean;
	onBack?: () => void;
}
const tabs = [
	{
		key: '/goods/home',
		title: window._$m.t('首页'),
		icon: (active) => {
			if (active) {
				return <BottomnavigationHomepageClick />;
			}
			return <BottomnavigationHomepageDefault />;
		}
	},
	{
		key: '/goods/classify',
		title: window._$m.t('分类'),
		icon: (active) => {
			if (active) {
				return <BottomnavigationCategoryClick />;
			}
			return <BottomnavigationCategoryDefault />;
		}
	},
	{
		key: '/order/shopcart',
		title: window._$m.t('购物车'),
		badge: 0,
		icon: (active) => {
			if (active) {
				return <BottomnavigationShoppingcartClick />;
			}
			return <BottomnavigationShoppingcartDefault />;
		}
	},
	{
		key: '/user/mine',
		title: window._$m.t('我的'),
		icon: (active) => {
			if (active) {
				return <BottomnavigationMineClick />;
			}
			return <BottomnavigationMineDefault />;
		}
	}
];

const CustomTabbar = (props: CustomTabbarProps) => {
	const { className, onBack, isFixed, ...divRestProps } = props;
	const location = useLocation();
	const { pathname } = location;
	const navigate = useNavigate();
	/** 跳转 */
	const setRouteActive = async (value: string) => {
		sessionStorage.removeItem('isFromOtherDomain');
		// navigate(value);
		const token = await checkLogin();
		// todo
		if (value === '/goods/home' || value === '/goods/classify' || token) {
			if (value === '/goods/home') {
				sessionStorage.removeItem('isFromOtherDomain');
			}
			navigate(value);
			return;
		}
		goToLogin();
	};
	const [cartOriginalData] = useAtom(Order.cartOriginalData);
	return (
		<>
			<div
				{...divRestProps}
				id="layout-footer"
				className={`layout-footer ${className ?? ''} ${isFixed ? 'custom-tabbar-fixed' : ''} `}
			>
				<TabBar
					safeArea
					activeKey={pathname}
					onChange={(value) => {
						setRouteActive(value);
					}}
				>
					{tabs.map((item) => {
						let badge = item.badge ?? null;
						if (item.key === '/order/shopcart') {
							badge =
								cartOriginalData.cartNum > 0
									? cartOriginalData.cartNum
									: null;
						}
						return (
							<TabBar.Item
								badge={badge}
								key={item.key}
								icon={item.icon}
								title={item.title}
							/>
						);
					})}
				</TabBar>
			</div>
		</>
	);
};
export default memo(CustomTabbar);
