import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function ShoppingCart(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fill="none" fillRule="evenodd"><mask id="f68a0__a" fill="#fff" /><path d="M13 10.667H4.333L2.667 4h12L13 10.667Z" mask="url(#f68a0__a)" /><path d="M1.645 2.666H1V1.334h1.167c.272 0 .512.165.615.41l.031.095.374 1.495h11.48c.4 0 .702.348.663.732l-.017.097-1.666 6.666a.667.667 0 0 1-.547.498l-.1.007H4.333a.667.667 0 0 1-.615-.41l-.031-.095L2.02 4.162l-.375-1.496Zm12.167 2H3.52L4.854 10h7.625l1.333-5.334Z" fill="#FF5010" fillRule="nonzero" /><path d="M4.333 11.333a1.667 1.667 0 1 1 0 3.334 1.667 1.667 0 0 1 0-3.334Zm0 1.334a.333.333 0 1 0 0 .666.333.333 0 0 0 0-.666ZM13 11.333a1.667 1.667 0 1 1 0 3.334 1.667 1.667 0 0 1 0-3.334Zm0 1.334a.333.333 0 1 0 0 .666.333.333 0 0 0 0-.666Z" fill="#FF5010" fillRule="nonzero" /></g></g>
        </svg>);

}