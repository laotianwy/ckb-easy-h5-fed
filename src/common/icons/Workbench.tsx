import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Workbench(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="#FFF" d="M18.333 2.083c.46 0 .834.373.834.834V13.75c0 .46-.373.833-.834.833h-7.5v1.667H15a.833.833 0 1 1 0 1.667H5a.833.833 0 1 1 0-1.667h4.167v-1.667h-7.5a.833.833 0 0 1-.834-.833V2.917c0-.46.374-.834.834-.834h16.666ZM17.5 3.75h-15v9.167h15V3.75ZM6.667 8.333c.46 0 .833.373.833.834v1.666a.833.833 0 1 1-1.667 0V9.167c0-.46.374-.834.834-.834ZM10 6.667c.46 0 .833.373.833.833v3.333a.833.833 0 0 1-1.666 0V7.5c0-.46.373-.833.833-.833ZM13.333 5c.46 0 .834.373.834.833v5a.833.833 0 0 1-1.667 0v-5c0-.46.373-.833.833-.833Z" fillRule="evenodd" fill={_fill} /></g>
        </svg>);

}