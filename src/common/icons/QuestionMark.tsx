import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function QuestionMark(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 14 14"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="#BCBCBC" fillRule="evenodd" d="M7 14A7 7 0 1 1 7 0a7 7 0 0 1 0 14Zm.529-4.466c.15.14.23.337.223.541a.754.754 0 0 1-.234.552.795.795 0 0 1-.551.213.76.76 0 0 1-.553-.224.734.734 0 0 1-.223-.54c0-.224.074-.404.223-.542a.75.75 0 0 1 .553-.213.76.76 0 0 1 .562.212v.001Zm1.284-5.86c.426.37.638.88.638 1.518 0 .52-.138.955-.404 1.295-.096.116-.392.392-.89.827-.203.17-.352.362-.447.552a1.42 1.42 0 0 0-.17.701v.18H6.403v-.18c0-.392.064-.732.213-1.008.139-.297.541-.732 1.2-1.316l.18-.202c.192-.245.298-.5.298-.775 0-.372-.106-.658-.308-.871-.212-.212-.521-.318-.903-.318-.478 0-.828.148-1.04.456-.192.255-.287.616-.287 1.083H4.64c0-.776.223-1.38.67-1.826.445-.457 1.061-.68 1.846-.68.68 0 1.233.18 1.656.563Z" fill={_fill} /></g>
        </svg>);

}