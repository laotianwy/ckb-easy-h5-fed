import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Xingzhuang32x(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 18 18"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-stroke="currentColor" data-follow-fill="currentColor" d="m16.87 16.096-3.028-3.026c1.079-1.287 1.775-2.922 1.775-4.766A7.305 7.305 0 0 0 8.308 1a7.305 7.305 0 1 0 0 14.609c1.845 0 3.48-.696 4.768-1.774l3.028 3.026a.523.523 0 0 0 .383.139.52.52 0 0 0 .382-.14.626.626 0 0 0 0-.764Zm-8.562-1.53c-3.445 0-6.264-2.818-6.264-6.262 0-3.443 2.819-6.26 6.264-6.26 3.446 0 6.265 2.817 6.265 6.26 0 3.444-2.82 6.261-6.265 6.261Z" strokeWidth=".5" fill={_fill} stroke={_stroke} /></g>
        </svg>);

}