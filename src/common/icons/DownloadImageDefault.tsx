import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function DownloadImageDefault(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 18 18"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fill="none" fillRule="evenodd"><path d="M0 0h18v18H0z" /><path data-follow-fill="currentColor" d="M14.735 2c.603 0 1.151.21 1.558.548.052.044.102.09.15.138l.07.073c.034.037.066.076.096.116l.015.018.045.06.028.042a2.108 2.108 0 0 1 .162.304l.009.022.009.024.034.092a1.875 1.875 0 0 1 .081.389l.008.151v10.044c0 .052-.004.102-.007.152l-.009.075c-.002.025-.007.048-.01.073l-.002.011-.02.09a1.78 1.78 0 0 1-.01.045c-.01.03-.019.064-.03.095-.007.026-.017.053-.026.078a1.882 1.882 0 0 1-.045.106c-.007.019-.016.036-.025.055-.02.04-.04.08-.064.12l-.021.034-.022.035-.037.056-.008.01-.01.015-.022.031-.028.036-.022.027a1.424 1.424 0 0 1-.066.076l-.062.066a2.12 2.12 0 0 1-.118.11c-.002.004-.005.005-.009.008-.009.008-.018.018-.028.025-.088.073-.183.14-.283.2l-.153.085-.048.023c-.032.016-.066.03-.098.044a2.324 2.324 0 0 1-.416.134c-.13.03-.266.05-.405.058l-.094.004-.096.002H3.283c-.103 0-.205-.006-.305-.019a2.526 2.526 0 0 1-.69-.18l-.05-.022-.049-.023-.048-.023a2.762 2.762 0 0 1-.093-.05c-.014-.01-.03-.017-.044-.027a2.465 2.465 0 0 1-.216-.145l-.051-.04-.05-.041-.048-.042a1.91 1.91 0 0 1-.634-1.249l-.006-.143V4.002a1.835 1.835 0 0 1 .051-.43 1.826 1.826 0 0 1 .436-.813c.01-.013.023-.025.034-.038l.061-.062.066-.06.039-.035.03-.024.05-.04.02-.016c.04-.031.084-.06.127-.089l.044-.028a2.444 2.444 0 0 1 1.1-.359L3.265 2h11.471Zm-1.731 7.47a.8.8 0 0 0-1.059-.005l-4.461 3.902a.4.4 0 0 1-.527 0l-2.222-1.95a.5.5 0 0 0-.666.005L2.14 13.17v.622a1.2 1.2 0 0 0 1.2 1.2h11.308a1.2 1.2 0 0 0 1.2-1.2v-1.789Zm1.702-6.467H3.283a1.21 1.21 0 0 0-.805.295.944.944 0 0 0-.33.591l-.007.113v7.61l1.505-1.317c.415-.364 1.075-.392 1.53-.084l.108.084 2.04 1.784 4.32-3.78c.415-.364 1.075-.392 1.53-.084l.108.085 2.567 2.245V4.002a.929.929 0 0 0-.338-.704 1.211 1.211 0 0 0-.805-.295Zm-9.713.999c.947 0 1.714.756 1.714 1.689 0 .932-.767 1.688-1.714 1.688A1.701 1.701 0 0 1 3.28 5.691c0-.933.767-1.689 1.713-1.689Z" fill={_fill} /></g></g>
        </svg>);

}