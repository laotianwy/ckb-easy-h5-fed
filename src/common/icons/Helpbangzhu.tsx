import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Helpbangzhu(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path strokeLinejoin="round" strokeWidth="1.6" d="M8 14.667c1.84 0 3.508-.746 4.714-1.953A6.646 6.646 0 0 0 14.667 8c0-1.84-.747-3.508-1.953-4.714A6.646 6.646 0 0 0 8 1.333a6.647 6.647 0 0 0-4.714 1.953A6.646 6.646 0 0 0 1.333 8c0 1.84.746 3.508 1.953 4.714A6.646 6.646 0 0 0 8 14.667Z" data-follow-stroke="#B0B7C2" stroke={_stroke} /><path strokeLinejoin="round" strokeLinecap="round" strokeWidth="1.6" d="M8 9.542V8.208a2 2 0 1 0-2-2" data-follow-stroke="#B0B7C2" stroke={_stroke} /><path d="M8 12.542a.833.833 0 1 0 0-1.667.833.833 0 0 0 0 1.667Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#B0B7C2" fill={_fill} /></g>
        </svg>);

}