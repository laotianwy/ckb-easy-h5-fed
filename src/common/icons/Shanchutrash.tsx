import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Shanchutrash(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M4 3.333c0-1.235 1.19-2 2.333-2h3.333c1.143 0 2.334.765 2.334 2v.89c0 .744-.694 1.11-1.223 1.11H5.223C4.693 5.333 4 4.967 4 4.223v-.89Zm2.333-.666c-.699 0-1 .43-1 .666V4h5.333v-.667c0-.237-.3-.666-1-.666H6.333Zm4.45 1.332h-.002.002ZM5.217 4Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#7E8694" fill={_fill} /><path d="M1.333 4.667C1.333 4.298 1.631 4 2 4h12a.667.667 0 0 1 0 1.333H2a.667.667 0 0 1-.667-.666Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#7E8694" fill={_fill} /><path d="M2.617 4h10.765l-.629 8.81a2 2 0 0 1-1.995 1.857H5.241a2 2 0 0 1-1.995-1.858L2.617 4Zm1.432 1.333.527 7.381c.025.349.315.62.665.62h5.517c.35 0 .64-.271.665-.62l.527-7.38h-7.9Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#7E8694" fill={_fill} /><path d="M6.666 12A.667.667 0 0 1 6 11.333v-4a.667.667 0 0 1 1.333 0v4a.667.667 0 0 1-.667.667Zm2.667 0a.667.667 0 0 1-.667-.667v-4a.667.667 0 0 1 1.334 0v4a.667.667 0 0 1-.667.667Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#7E8694" fill={_fill} /></g>
        </svg>);

}