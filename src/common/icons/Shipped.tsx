import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Shipped(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 26 22"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="currentColor" d="m16.856.818 2.075 5.727 6.796 1.99v10.738h-4.035a3.82 3.82 0 0 1-7.202 0h-2.98a3.82 3.82 0 0 1-7.202 0H.273V.818h16.583ZM7.91 16.091a1.91 1.91 0 1 0 0 3.818 1.91 1.91 0 0 0 0-3.818Zm10.182 0a1.91 1.91 0 1 0 0 3.818 1.91 1.91 0 0 0 0-3.818ZM15.517 2.727H2.182v14.637h1.962a3.82 3.82 0 0 1 7.53 0h2.652a3.82 3.82 0 0 1 7.53 0h1.962v-7.4l-6.353-1.858-1.948-5.379Z" fill={_fill} /></g>
        </svg>);

}