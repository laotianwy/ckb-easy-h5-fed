import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Eye(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M2.388 4.905c1.174-1.18 3.005-2.238 5.611-2.238 2.607 0 4.438 1.059 5.612 2.238a7.289 7.289 0 0 1 1.28 1.728c.14.264.246.513.319.733.068.206.123.431.123.634s-.055.428-.123.634a5.04 5.04 0 0 1-.318.733 7.289 7.289 0 0 1-1.281 1.728c-1.174 1.18-3.005 2.238-5.612 2.238-2.606 0-4.437-1.059-5.611-2.238a7.289 7.289 0 0 1-1.281-1.728 4.983 4.983 0 0 1-.318-.733A2.1 2.1 0 0 1 .666 8a2.1 2.1 0 0 1 .123-.634c.073-.22.18-.469.318-.733a7.288 7.288 0 0 1 1.28-1.728ZM2 8v.002c.002.014.011.082.054.212.047.141.123.324.233.533.22.42.564.923 1.046 1.408C4.289 11.115 5.79 12 7.999 12c2.209 0 3.711-.885 4.667-1.845a5.955 5.955 0 0 0 1.045-1.408c.11-.21.186-.392.233-.533A1.2 1.2 0 0 0 14 7.998a1.2 1.2 0 0 0-.055-.212 3.663 3.663 0 0 0-.233-.533 5.954 5.954 0 0 0-1.045-1.408C11.71 4.885 10.208 4 7.999 4c-2.208 0-3.71.885-4.666 1.845a5.957 5.957 0 0 0-1.046 1.408c-.11.21-.186.392-.233.533-.043.13-.052.198-.054.212V8Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /><path d="M7.999 6.667a1.333 1.333 0 1 0 0 2.666 1.333 1.333 0 0 0 0-2.666ZM5.332 8a2.667 2.667 0 1 1 5.333 0 2.667 2.667 0 0 1-5.333 0Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}