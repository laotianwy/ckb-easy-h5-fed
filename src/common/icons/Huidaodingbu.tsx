import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Huidaodingbu(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 28 28"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M14.133 19.131c-1.656.014-2.65.816-2.664 2.653-.009 1.137.56 2.395 1.67 3.8.058.073.124.138.197.196a1.21 1.21 0 0 0 1.639-.152l.054-.063.13-.167c1.044-1.353 1.588-2.564 1.597-3.657.015-1.836-.966-2.622-2.623-2.61Zm1.246-16.3-.186-.099-.185-.092a1.689 1.689 0 0 0-1.48.008c-2.172 1.064-3.636 2.88-4.368 5.399a12.798 12.798 0 0 0-.49 3.313l-.003.321.003.298-.041.05c-.87 1.078-1.419 3.104-1.723 6.13l-.026.261-.042.478a.71.71 0 0 0 .982.713l.063-.03 3.19-1.666.116.02c.732.124 1.472.199 2.213.226l.37.008.369.002a17.264 17.264 0 0 0 3.286-.345l.108-.024 3.279 1.677a.732.732 0 0 0 1.055-.575l.003-.063-.002-.061c-.257-3.819-.878-6.205-2.028-7.209l-.076-.063-.045-.035.001-.183c-.003-.974-.101-1.9-.296-2.774l-.068-.29-.069-.26c-.646-2.34-1.953-4.063-3.91-5.135Zm1.148 7.987a2.25 2.25 0 0 1-2.255 2.245 2.25 2.25 0 0 1-2.255-2.245 2.25 2.25 0 0 1 2.255-2.245 2.25 2.25 0 0 1 2.255 2.245Z" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}