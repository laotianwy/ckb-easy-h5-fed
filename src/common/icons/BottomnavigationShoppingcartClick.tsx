import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function BottomnavigationShoppingcartClick(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 24 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><defs><linearGradient x1="-2.206%" y1="52.5%" x2="97.794%" y2="52.5%" id="f77f8__a"><stop stopColor="#FF7E3E" offset="0%" /><stop stopColor="#FF5010" offset="50%" /><stop stopColor="#FE4B0F" offset="100%" /></linearGradient></defs><path fillRule="evenodd" fill="url(#f77f8__a)" transform="translate(.75)" d="M6 15a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5Zm13 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5ZM6 17a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1Zm13 0a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1ZM2.75 0a1 1 0 0 1 .97.757L4.281 3H21.5a1 1 0 0 1 .991 1.131l-.02.112-2.5 10A1 1 0 0 1 19 15H6a1 1 0 0 1-.97-.757l-2.5-10L1.968 2H1a1 1 0 0 1-.993-.883L0 1a1 1 0 0 1 1-1h1.75Z" /></g>
        </svg>);

}