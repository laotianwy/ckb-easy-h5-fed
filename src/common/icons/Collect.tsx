import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Collect(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 22 22"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-stroke="currentColor" fill="none" d="m10.056 18.17-2.455 1.314a1.892 1.892 0 0 1-2.759-1.982l.486-2.884a2 2 0 0 0-.563-1.752l-2.038-2.022A1.924 1.924 0 0 1 3.8 7.575l2.755-.408a2 2 0 0 0 1.507-1.106L9.3 3.506a1.889 1.889 0 0 1 3.4 0l1.238 2.555a2 2 0 0 0 1.507 1.106l2.755.408a1.924 1.924 0 0 1 1.073 3.269l-2.038 2.022a2 2 0 0 0-.563 1.752l.486 2.884a1.892 1.892 0 0 1-2.759 1.982l-2.455-1.314a2 2 0 0 0-1.888 0Z" strokeWidth="1.7" stroke={_stroke} /></g>
        </svg>);

}