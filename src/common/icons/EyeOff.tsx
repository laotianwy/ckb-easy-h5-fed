import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function EyeOff(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M7.419 2.703c.168-.017.371-.037.583-.037 2.655 0 4.49 1.102 5.654 2.329a7.366 7.366 0 0 1 1.258 1.793c.255.518.421 1.034.421 1.393 0 .179-.049.35-.097.481a3.352 3.352 0 0 1-.205.446 7.47 7.47 0 0 1-.688 1.02.667.667 0 1 1-1.047-.825c.251-.32.44-.607.563-.832a2.044 2.044 0 0 0 .14-.309 1.738 1.738 0 0 0-.054-.225 3.914 3.914 0 0 0-.23-.56 6.034 6.034 0 0 0-1.028-1.465C11.744 4.917 10.246 4 8.002 4c-.14 0-.282.014-.462.032a.667.667 0 0 1-.133-1.327l.012-.001Zm-4.826 3.42c.29.228.34.647.112.936-.251.32-.44.607-.563.832a2.05 2.05 0 0 0-.138.308c.004.029.017.093.051.196.047.138.121.314.23.516.217.404.557.887 1.037 1.35.95.918 2.455 1.766 4.68 1.766a4.4 4.4 0 0 0 .46-.032.667.667 0 1 1 .133 1.327l-.01.001c-.168.017-.372.038-.583.038-2.6 0-4.43-1.005-5.606-2.141A6.938 6.938 0 0 1 1.11 9.543a4.704 4.704 0 0 1-.32-.724 2.086 2.086 0 0 1-.123-.638c0-.178.049-.35.097-.48.051-.143.122-.293.205-.446.166-.306.4-.656.687-1.02a.667.667 0 0 1 .936-.112ZM1.53 1.529c.26-.26.682-.26.942 0l12 12a.667.667 0 0 1-.943.942l-12-12a.667.667 0 0 1 0-.942Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}