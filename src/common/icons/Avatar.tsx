import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Avatar(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fill="none" fillRule="evenodd"><path d="M0 0h16v16H0z" /><path data-follow-fill="currentColor" d="M8 9.07c-2.527 0-4.58-2.037-4.58-4.535C3.42 2.036 5.473 0 8 0c2.526 0 4.58 2.036 4.58 4.535 0 2.498-2.054 4.534-4.58 4.534Zm0-7.9c-1.874 0-3.4 1.511-3.4 3.366 0 1.856 1.526 3.367 3.4 3.367 1.874 0 3.4-1.511 3.4-3.367C11.4 2.681 9.874 1.17 8 1.17ZM14.537 16H1.462c-.809 0-1.463-.55-1.463-1.224v-.75c0-.052.01-.103.03-.15C1.013 11.595 4.29 10 8 10s6.987 1.595 7.97 3.876c.02.047.031.098.031.15v.75c0 .676-.658 1.224-1.463 1.224ZM1.4 13.877v.536c0 .158.183.287.408.287h12.384c.225 0 .408-.13.408-.287v-.538c-.83-1.52-3.53-2.575-6.601-2.575-3.073 0-5.772 1.055-6.599 2.577Z" fill={_fill} /></g></g>
        </svg>);

}