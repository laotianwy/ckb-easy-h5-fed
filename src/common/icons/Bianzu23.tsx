import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Bianzu23(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 10 8"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path fillRule="nonzero" d="M7.677 1.06c.67 0 1.212.518 1.212 1.157v4.626C8.889 7.483 8.346 8 7.677 8H1.212C.542 8 0 7.482 0 6.843V2.217C0 1.578.543 1.06 1.212 1.06h6.465Zm-1.87 2.795a.418.418 0 0 0-.284.11l-.051.057-1.876 2.573-.999-1.666a.411.411 0 0 0-.354-.196.417.417 0 0 0-.302.129l-.051.067-.927 1.546a.373.373 0 0 0 .001.387.406.406 0 0 0 .267.185l.085.009h6.257a.41.41 0 0 0 .36-.208.372.372 0 0 0 .017-.327l-.043-.075-1.766-2.424a.414.414 0 0 0-.335-.167ZM8.416 0c.837 0 1.521.619 1.579 1.402L10 1.51v4.627h-.74V1.51c0-.414-.329-.755-.75-.798L8.417.707H1.953V0h6.465Zm-6.8 2.024a.593.593 0 0 0-.607.578c0 .32.271.579.606.579a.593.593 0 0 0 .606-.579.593.593 0 0 0-.606-.578Z" data-follow-fill="#FFF" fill={_fill} /></g>
        </svg>);

}