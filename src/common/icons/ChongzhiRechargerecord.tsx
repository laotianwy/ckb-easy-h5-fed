import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function ChongzhiRechargerecord(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="currentColor" fillRule="evenodd" d="M16.474 1a1 1 0 0 1 1 1l-.001 11.115h-1.365V2.5H3.365v14h5.351V18H3a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h13.474Zm-3.418 13 1.147.921-.45.534H19l-.022 1.429H12.55l-1.5.001v-.542L13.056 14ZM10 11a.7.7 0 0 1 0 1.4H6.4a.7.7 0 0 1 0-1.4H10Zm3.6-3a.7.7 0 0 1 0 1.4H6.4a.7.7 0 0 1 0-1.4h7.2Zm0-3a.7.7 0 0 1 0 1.4H6.4a.7.7 0 0 1 0-1.4h7.2Z" fill={_fill} /></g>
        </svg>);

}