import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function ClearexpiredItems(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fill="none" fillRule="evenodd"><path d="M0 0h16v16H0z" /><path data-follow-fill="currentColor" d="M8.888 1.46c.18-.495.748-.742 1.267-.553l1.32.48c.517.189.794.743.614 1.237l-.977 2.685 2.813 1.024a.995.995 0 0 1 .668 1.01l-.29 3.137c-.046.502-.494.856-1.013.832a7.645 7.645 0 0 1-.443 1.823c-.332.914-.814 1.626-1.435 2.117-.262.213-.636.27-.972.148l-9.157-3.333a1.018 1.018 0 0 1-.57-.504.922.922 0 0 1 .25-1.14c.47-.368.83-.895 1.074-1.568l.516-1.415c-.448-.223-.684-.721-.517-1.18l.104-.203 1.799-2.592c.249-.36.726-.502 1.16-.345L7.91 4.144ZM3.676 7.87l-.508 1.396c-.272.748-.669 1.366-1.18 1.842l1.55.564c.297-.416.563-.958.791-1.629l.948.326a7.466 7.466 0 0 1-.777 1.653l1.13.41c.297-.416.562-.958.79-1.628l.948.326a7.524 7.524 0 0 1-.776 1.653l1.654.602c.298-.416.563-.958.79-1.627l.949.323a7.475 7.475 0 0 1-.778 1.654l1.51.55c.421-.364.756-.887 1.001-1.561.243-.668.38-1.264.408-1.779L3.676 7.87Zm6.277-5.82L8.65 5.63 4.854 4.247l-1.57 2.266 9.85 3.586.254-2.745L9.591 5.97l1.303-3.579-.941-.343Z" fill={_fill} /></g></g>
        </svg>);

}