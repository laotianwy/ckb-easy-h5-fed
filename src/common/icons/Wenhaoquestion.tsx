import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Wenhaoquestion(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M8 2a6 6 0 1 0 0 12A6 6 0 0 0 8 2ZM.667 8a7.333 7.333 0 1 1 14.667 0A7.333 7.333 0 0 1 .667 8Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#B0B7C2" fill={_fill} /><path d="M8 11.333a.667.667 0 1 1 0 1.334.667.667 0 0 1 0-1.334Z" data-follow-fill="#B0B7C2" fill={_fill} /><path d="M5.333 6.667a2.667 2.667 0 1 1 3.333 2.582v.756a.667.667 0 1 1-1.333 0V8.667C7.333 8.298 7.631 8 8 8a1.333 1.333 0 1 0-1.334-1.333.667.667 0 0 1-1.333 0Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#B0B7C2" fill={_fill} /></g>
        </svg>);

}