import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function XiaoyancheckOne1(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 32 32"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M16 29.333a13.29 13.29 0 0 0 9.428-3.905A13.291 13.291 0 0 0 29.333 16a13.29 13.29 0 0 0-3.905-9.428A13.292 13.292 0 0 0 16 2.667a13.292 13.292 0 0 0-9.428 3.905A13.292 13.292 0 0 0 2.667 16a13.29 13.29 0 0 0 3.905 9.428A13.292 13.292 0 0 0 16 29.333Z" stroke="#fff" strokeWidth="2.667" strokeLinejoin="round" /><path d="m10.666 16 4 4 8-8" stroke="#fff" strokeWidth="2.667" strokeLinecap="round" strokeLinejoin="round" /></g>
        </svg>);

}