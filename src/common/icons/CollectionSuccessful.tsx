import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function CollectionSuccessful(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 18 18"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="currentColor" d="m8.68 14.116-3.723 1.622a.8.8 0 0 1-1.116-.81l.392-4.043a.8.8 0 0 0-.198-.608l-2.693-3.04a.8.8 0 0 1 .426-1.312l3.966-.876a.8.8 0 0 0 .517-.376l2.06-3.5a.8.8 0 0 1 1.379 0l2.059 3.5a.8.8 0 0 0 .517.376l3.966.876a.8.8 0 0 1 .426 1.312l-2.693 3.04a.8.8 0 0 0-.198.608l.392 4.042a.8.8 0 0 1-1.116.811L9.32 14.116a.8.8 0 0 0-.638 0Z" fillRule="evenodd" fill={_fill} /></g>
        </svg>);

}