import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Chakangengduomore(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path strokeLinejoin="round" strokeWidth="1.6" d="M15.66 15.66a7.508 7.508 0 0 1-5.327 2.207 7.508 7.508 0 0 1-5.327-2.207A7.507 7.507 0 0 1 2.8 10.333a7.51 7.51 0 0 1 2.206-5.327A7.507 7.507 0 0 1 10.333 2.8c2.08 0 3.963.842 5.327 2.206a7.508 7.508 0 0 1 2.207 5.327 7.51 7.51 0 0 1-2.207 5.327Z" data-follow-stroke="#7E8694" stroke={_stroke} /><path strokeLinejoin="round" strokeLinecap="round" strokeWidth="1.6" d="m11.7 8.9 1.878 2.193h-7.07" data-follow-stroke="#7E8694" stroke={_stroke} /></g>
        </svg>);

}