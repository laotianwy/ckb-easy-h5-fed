import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Notify(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fill="none" fillRule="evenodd"><path d="M0 0h20v20H0z" /><path d="M10 1.819a5.727 5.727 0 0 0-5.727 5.727v8.182h11.454V7.546A5.727 5.727 0 0 0 10 1.819Z" /><path data-follow-fill="#FFF" fillRule="nonzero" d="M10 1a6.546 6.546 0 0 1 6.546 6.546v7.364h1.636a.818.818 0 0 1 0 1.636h-5.347a2.864 2.864 0 0 1-5.67 0H1.818a.818.818 0 0 1 0-1.636h1.636V7.546A6.546 6.546 0 0 1 10 1Zm1.158 15.546H8.842a1.227 1.227 0 0 0 2.316 0ZM10 2.636a4.91 4.91 0 0 0-4.91 4.91v7.364h9.82V7.546A4.91 4.91 0 0 0 10 2.636Z" fill={_fill} /></g></g>
        </svg>);

}