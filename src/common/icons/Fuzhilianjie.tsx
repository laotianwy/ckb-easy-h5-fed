import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Fuzhilianjie(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 40 40"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><circle fill="#F0F0F0" r="20" cy="20" cx="20" /><path fill="#5D5D5D" d="M21.874 18.161a3.7 3.7 0 0 0-.382-.33.76.76 0 0 0-1.108 1.034.759.759 0 0 0 .187.162c.072.059.142.117.208.182l.062.063c.787.786.552 2.14-.235 2.926l-3.357 3.356a2.02 2.02 0 0 1-2.852 0l-.063-.063a2.017 2.017 0 0 1 0-2.851l1.483-1.483a.823.823 0 0 0-.512-1.47.82.82 0 0 0-.463.142l-.002-.002-.016.015a.829.829 0 0 0-.156.146l-1.542 1.442a3.737 3.737 0 0 0 0 5.27l.063.062a3.737 3.737 0 0 0 5.268 0l3.356-3.357c1.447-1.449 1.57-3.734.122-5.182l-.061-.062Z" /><path fill="#5D5D5D" d="m26.711 13.238-.062-.062a3.735 3.735 0 0 0-5.268 0l-3.356 3.357c-1.449 1.448-1.529 3.54-.08 4.99l.062.061c.066.066.134.128.204.187.05.057.11.105.175.143l.002.001a.717.717 0 0 0 1.002-.933c-.095-.213-.272-.342-.395-.465l-.061-.06c-.787-.787-.488-1.93.298-2.717l3.358-3.356a2.017 2.017 0 0 1 2.851 0l.063.062a2.02 2.02 0 0 1 0 2.853l-1.478 1.479a.817.817 0 0 0 .915 1.354l.002.005.023-.022a.82.82 0 0 0 .186-.171l1.558-1.438a3.735 3.735 0 0 0 .001-5.268Z" /></g>
        </svg>);

}