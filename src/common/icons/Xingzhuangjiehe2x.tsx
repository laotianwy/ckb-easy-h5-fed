import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Xingzhuangjiehe2x(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 12 12"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="currentColor" fillRule="evenodd" d="M11.5 10.875c0 .58-.439 1.057-1.002 1.118l-.123.007h-6.75c-.58 0-1.057-.439-1.118-1.002l-.007-.123V8.999L1.625 9C1.045 9 .568 8.561.507 7.998L.5 7.875v-6.75C.5.545.939.068 1.502.007L1.625 0h6.75c.58 0 1.057.439 1.118 1.002l.007.123.001 1.874.874.001c.58 0 1.057.439 1.118 1.002l.007.123v6.75ZM8.5 8V1h-7v7h7Zm2 3V4h-1v3.875c0 .58-.439 1.057-1.002 1.118L8.375 9H3.5v2h7Z" fill={_fill} /></g>
        </svg>);

}