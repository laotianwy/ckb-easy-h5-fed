import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function BottomnavigationMineDefault(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="currentColor" fillRule="evenodd" d="M10 0a4.5 4.5 0 1 1 0 9 4.5 4.5 0 0 1 0-9Zm0 2a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5Zm2.6 9c3.001 0 3.715.058 4.67.545a5 5 0 0 1 2.185 2.185c.487.955.545 1.669.545 4.67v.6a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1v-.6c0-3.001.058-3.715.545-4.67a5 5 0 0 1 2.185-2.185C3.685 11.058 4.4 11 7.4 11h5.2Zm.527 2H6.873c-2.16.008-2.731.07-3.235.327a3 3 0 0 0-1.311 1.311c-.257.504-.319 1.075-.326 3.235L2 18h15.999v-.127c-.007-2.077-.065-2.685-.297-3.176l-.029-.059a3 3 0 0 0-1.311-1.311c-.504-.257-1.075-.319-3.235-.326Z" fill={_fill} /></g>
        </svg>);

}