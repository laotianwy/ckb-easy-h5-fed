import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function DownloadimageHoverclick(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 18 18"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fill="none" fillRule="evenodd"><path d="M0 0h18v18H0z" /><path data-follow-fill="#FF5010" d="M14.735 2c.603 0 1.151.21 1.558.548.052.044.102.09.15.138l.07.073a1.514 1.514 0 0 1 .126.155l.03.04.028.04a2.108 2.108 0 0 1 .162.305c.005.014.013.03.018.046l.034.092a1.84 1.84 0 0 1 .09.54H17v10.044c0 .052-.004.102-.007.152l-.009.075c-.002.025-.007.048-.01.073l-.002.011-.02.09a1.78 1.78 0 0 1-.01.045c-.01.03-.019.064-.03.095-.007.026-.017.053-.026.078a1.882 1.882 0 0 1-.045.106c-.007.019-.016.036-.025.055-.02.04-.04.08-.064.12l-.043.069-.037.056-.008.01-.021.03-.01.016-.03.036-.02.027a1.424 1.424 0 0 1-.067.076l-.062.066a2.12 2.12 0 0 1-.118.11c-.002.004-.005.005-.009.008-.009.008-.018.018-.028.025-.133.11-.279.205-.436.285l-.048.023c-.032.016-.066.03-.098.044a2.324 2.324 0 0 1-.416.134c-.13.03-.266.05-.405.058-.063.003-.125.006-.19.006H3.283c-.103 0-.205-.006-.305-.019a2.526 2.526 0 0 1-.69-.18 1.806 1.806 0 0 1-.099-.045l-.048-.023a2.762 2.762 0 0 1-.093-.05c-.014-.01-.03-.017-.044-.027a2.465 2.465 0 0 1-.316-.226l-.049-.042a1.91 1.91 0 0 1-.589-.97 1.745 1.745 0 0 1-.05-.422V4.002a1.835 1.835 0 0 1 .051-.43 1.826 1.826 0 0 1 .436-.813c.01-.013.023-.025.034-.038a1.85 1.85 0 0 1 .127-.123l.039-.034.059-.049.02-.015.02-.016c.042-.031.085-.06.128-.089l.044-.028A2.46 2.46 0 0 1 3.264 2Zm-1.731 7.47a.8.8 0 0 0-1.059-.005l-4.461 3.902a.4.4 0 0 1-.527 0l-2.222-1.95a.5.5 0 0 0-.666.005L2.14 13.17v.622a1.2 1.2 0 0 0 1.2 1.2h11.308a1.2 1.2 0 0 0 1.2-1.2v-1.789Zm-8.01-5.468c-.947 0-1.714.756-1.714 1.689 0 .932.767 1.688 1.713 1.688.947 0 1.714-.756 1.714-1.688 0-.933-.767-1.689-1.714-1.689Z" fill={_fill} /></g></g>
        </svg>);

}