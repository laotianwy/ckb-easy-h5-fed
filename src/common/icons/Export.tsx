import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Export(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fillRule="evenodd" fill="none"><path d="M20 0v20H0V0z" /><path data-follow-fill="currentColor" d="M8.983 17.812c0 .508-.327.909-.806.909h-4.84a1.607 1.607 0 0 1-1.604-1.605V2.605C1.733 1.72 2.453 1 3.338 1H8.2c.425.03.782.364.782.877 0 .514-.327.876-.754.876a.665.665 0 0 1-.074.005l-4.641.002.006 14.218 4.657-.007c.528 0 .806.332.806.84Zm9.537-9.086a1.607 1.607 0 0 1 0 2.27l-2.856 2.856a.806.806 0 0 1-1.14-1.139l2.052-2.051-9.456-.001a.806.806 0 0 1-.001-1.611h9.454l-2.049-2.042a.805.805 0 1 1 1.139-1.14Z" fill={_fill} /></g></g>
        </svg>);

}