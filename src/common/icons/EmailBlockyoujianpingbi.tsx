import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function EmailBlockyoujianpingbi(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 49 48"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path strokeLinejoin="round" strokeLinecap="round" strokeWidth="4" d="M44.5 35V9h-40v28h22" data-follow-stroke="#333" stroke={_stroke} /><path strokeWidth="4" d="M35.5 44a9 9 0 1 0 0-18 9 9 0 0 0 0 18Z" data-follow-stroke="#333" stroke={_stroke} /><path strokeLinejoin="round" strokeLinecap="round" strokeWidth="3" d="M35.25 32v2.5m0 4V38" data-follow-stroke="#333" stroke={_stroke} /><path strokeLinejoin="round" strokeLinecap="round" strokeWidth="4" d="m4.5 9 20 13 20-13" data-follow-stroke="#333" stroke={_stroke} /></g>
        </svg>);

}