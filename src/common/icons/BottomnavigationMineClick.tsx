import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function BottomnavigationMineClick(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><defs><linearGradient x1="-2.206%" y1="52.5%" x2="97.794%" y2="52.5%" id="f77fa__a"><stop stopColor="#FF7E3E" offset="0%" /><stop stopColor="#FF5010" offset="50%" /><stop stopColor="#FE4B0F" offset="100%" /></linearGradient></defs><path fillRule="evenodd" fill="url(#f77fa__a)" d="M10 0a4.5 4.5 0 1 1 0 9 4.5 4.5 0 0 1 0-9Zm7.27 11.545a5 5 0 0 1 2.185 2.185c.487.955.545 1.669.545 4.67v.6a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1l.001-1.19c.01-2.51.09-3.19.544-4.08a5 5 0 0 1 2.185-2.185c.89-.453 1.57-.535 4.08-.544h6.38c2.51.01 3.19.09 4.08.544Z" /></g>
        </svg>);

}