import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Bianzu32x(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 24 24"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><defs><linearGradient id="ff456__a" y2="50%" x2="100%" y1="50%" x1="0%"><stop offset="0%" stopColor="#FF7E3E" /><stop offset="50%" stopColor="#FF5010" /><stop offset="100%" stopColor="#FE4B0F" /></linearGradient></defs><g fillRule="evenodd" fill="none"><circle r="12" cy="12" cx="12" opacity=".095" data-follow-fill="#FE4B0F" fill={_fill} /><path fill="url(#ff456__a)" d="M10.808 8.64a2.146 2.146 0 0 1-1.296 1.976 2.053 2.053 0 0 1-.832.168c-.299 0-.576-.056-.832-.168a2.146 2.146 0 0 1-.672-3.48c.192-.192.416-.344.672-.456.256-.112.533-.168.832-.168.299 0 .576.056.832.168a2.146 2.146 0 0 1 1.296 1.96Zm-7.92 4.256c-.299 0-.565-.027-.8-.08a1.445 1.445 0 0 1-.592-.272 1.213 1.213 0 0 1-.368-.52C1.043 11.805 1 11.531 1 11.2V5.792c0-.597.141-1.013.424-1.248.283-.235.728-.352 1.336-.352h2.688c.17 0 .312-.032.424-.096a.782.782 0 0 0 .272-.256 1.9 1.9 0 0 0 .184-.376c.053-.144.112-.29.176-.44.107-.277.293-.517.56-.72.267-.203.57-.304.912-.304h1.488c.395 0 .717.11.968.328.25.219.419.45.504.696.117.32.272.595.464.824.192.23.395.344.608.344h2.032c.47-.01.843.117 1.12.384.277.267.416.656.416 1.168v5.488c0 .544-.144.957-.432 1.24-.288.283-.672.424-1.152.424H2.888Zm5.76-7.6c-.47 0-.907.088-1.312.264a3.314 3.314 0 0 0-1.056.72 3.48 3.48 0 0 0-.712 1.064 3.219 3.219 0 0 0-.264 1.296c0 .47.088.907.264 1.312.176.405.413.757.712 1.056.299.299.65.536 1.056.712.405.176.843.264 1.312.264.459 0 .89-.088 1.296-.264A3.381 3.381 0 0 0 11 11.008c.299-.299.536-.65.712-1.056.176-.405.264-.843.264-1.312 0-.459-.088-.89-.264-1.296A3.48 3.48 0 0 0 11 6.28a3.314 3.314 0 0 0-1.056-.72 3.219 3.219 0 0 0-1.296-.264Zm1.12-1.728c0-.17-.008-.304-.024-.4-.016-.096-.125-.144-.328-.144H8.088c-.203-.01-.312.032-.328.128a2.711 2.711 0 0 0-.024.416c-.01.203 0 .347.032.432.032.085.139.128.32.128h1.328c.203 0 .312-.048.328-.144.016-.096.024-.235.024-.416Z" transform="translate(4 4)" /></g></g>
        </svg>);

}