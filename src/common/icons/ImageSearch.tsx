import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function ImageSearch(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="currentColor" d="M7.684 2.354a.5.5 0 0 1 .452.285L8.784 4H13a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V6a2 2 0 0 1 1.123-1.798l.741-1.562a.5.5 0 0 1 .452-.286h4.368ZM2.86 5.4l-.122.06a.6.6 0 0 0-.33.443L2.4 6v7a.6.6 0 0 0 .503.592L3 13.6h10a.6.6 0 0 0 .592-.503L13.6 13V6a.6.6 0 0 0-.503-.592L13 5.4H2.86ZM8 6.5a3 3 0 1 1-1.168 5.764l-.14-.064a3 3 0 0 1-.002-5.4l.142-.064A2.99 2.99 0 0 1 8 6.5Zm0 1.2a1.8 1.8 0 1 0 0 3.6 1.8 1.8 0 0 0 0-3.6Zm3.7-1.3a.7.7 0 1 1 0 1.4.7.7 0 0 1 0-1.4ZM7.116 3.754H3.885l-.118.245h3.465l-.116-.245Z" fill={_fill} /></g>
        </svg>);

}