import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Xihuanlike2(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g filter="url(#1186ab__a)" clipPath="url(#1186ab__b)"><path fill="#000" d="M6.25 3.333a4.583 4.583 0 0 0-4.583 4.584c0 4.583 5.417 8.75 8.333 9.719 2.917-.97 8.334-5.136 8.334-9.72a4.583 4.583 0 0 0-7.709-3.352c-.132.123.928 3.353.928 3.353S9.914 11.69 9.375 12.5L10 8.125l-1.25-1.25.19-2.67a4.563 4.563 0 0 0-2.69-.872Z" /></g><defs><clipPath id="1186ab__b"><path fill="#fff" d="M0 0h20v20H0z" /></clipPath><filter colorInterpolationFilters="sRGB" filterUnits="userSpaceOnUse" height="18.302" width="20.667" y="2.333" x="-.333" id="1186ab__a"><feFlood result="BackgroundImageFix" floodOpacity="0" /><feColorMatrix result="hardAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" in="SourceAlpha" /><feOffset dy="1" /><feGaussianBlur stdDeviation="1" /><feComposite operator="out" in2="hardAlpha" /><feColorMatrix values="0 0 0 0 0.887768 0 0 0 0 0.887768 0 0 0 0 0.887768 0 0 0 1 0" /><feBlend result="effect1_dropShadow_5873_616" in2="BackgroundImageFix" /><feBlend result="shape" in2="effect1_dropShadow_5873_616" in="SourceGraphic" /></filter></defs></g>
        </svg>);

}