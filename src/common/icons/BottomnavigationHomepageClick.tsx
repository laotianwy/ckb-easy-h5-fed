import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function BottomnavigationHomepageClick(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 18 20"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><defs><linearGradient x1="-2.206%" y1="52.5%" x2="97.794%" y2="52.5%" id="f77f6__a"><stop stopColor="#FF7E3E" offset="0%" /><stop stopColor="#FF5010" offset="50%" /><stop stopColor="#FE4B0F" offset="100%" /></linearGradient></defs><path fillRule="evenodd" fill="url(#f77f6__a)" transform="translate(.375)" d="m9.125.22 7.5 6A1 1 0 0 1 17 7v12a1 1 0 0 1-.883.993L16 20H1a1 1 0 0 1-1-1V7a1 1 0 0 1 .375-.78l7.5-6a1 1 0 0 1 1.25 0ZM10 13.5H7V18h3v-4.5Z" /></g>
        </svg>);

}