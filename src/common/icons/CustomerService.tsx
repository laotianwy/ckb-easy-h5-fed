import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function CustomerService(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 22 22"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g fill="none" fillRule="evenodd"><path data-follow-stroke="currentColor" d="M18.5 5.35a1.146 1.146 0 0 1 1.15 1.15v9.587a1.146 1.146 0 0 1-1.15 1.15h-1.44v2.5l-4.626-2.5H3.5a1.146 1.146 0 0 1-1.15-1.15V6.5A1.146 1.146 0 0 1 3.5 5.35Z" strokeWidth="1.7" stroke={_stroke} /><g data-follow-fill="currentColor" transform="translate(5.5 10)" fill={_fill}><circle cx="9.25" cy="1.25" r="1.25" /><circle cx="5.25" cy="1.25" r="1.25" /><circle cx="1.25" cy="1.25" r="1.25" /></g></g></g>
        </svg>);

}