import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function GouwucheNormal(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M4.5 11.333a1.667 1.667 0 1 1 0 3.334 1.667 1.667 0 0 1 0-3.334Zm8.667 0a1.667 1.667 0 1 1 0 3.334 1.667 1.667 0 0 1 0-3.334ZM4.5 12.667a.333.333 0 1 0 0 .666.333.333 0 0 0 0-.666Zm8.667 0a.333.333 0 1 0 0 .666.333.333 0 0 0 0-.666ZM2.333 1.333c.306 0 .573.209.647.505l.374 1.495h11.48c.408 0 .713.361.66.755l-.014.074-1.667 6.666a.667.667 0 0 1-.646.505H4.5a.667.667 0 0 1-.647-.505L2.187 4.162l-.375-1.495h-.645a.667.667 0 0 1-.663-.59L.5 2c0-.368.298-.667.667-.667h1.166ZM13.98 4.667H3.687L5.021 10h7.624l1.334-5.333Z" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}