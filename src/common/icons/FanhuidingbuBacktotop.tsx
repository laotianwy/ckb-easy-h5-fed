import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function FanhuidingbuBacktotop(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 18"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><defs><filter id="f8e7b__a" filterUnits="objectBoundingBox" height="100%" width="100%" y="0%" x="0%"><feGaussianBlur in="SourceGraphic" /></filter></defs><path filter="url(#f8e7b__a)" d="m540.7 425.292 7.01 7.043a1 1 0 0 1 0 1.409.988.988 0 0 1-1.402 0l-5.317-5.343v13.603c0 .55-.443.996-.99.996a.994.994 0 0 1-.992-.996l-.001-13.604-5.316 5.344a.988.988 0 0 1-1.402 0 1 1 0 0 1 0-1.409l7.01-7.043a.988.988 0 0 1 1.4 0Z" transform="translate(-532 -425)" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}