import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Address(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g data-follow-fill="currentColor" fill={_fill}><path d="M8 10.284c-1.897 0-3.495-1.498-3.495-3.395S6.103 3.395 8 3.395s3.495 1.497 3.495 3.394S9.897 10.284 8 10.284ZM5.704 6.889c0 1.198.998 2.197 2.296 2.197 1.298 0 2.296-.999 2.296-2.197S9.298 4.693 8 4.693c-1.298-.1-2.296.898-2.296 2.196Z" /><path d="M8 1.198c3.195 0 5.791 2.496 5.791 5.691 0 .7-.2 1.398-.5 2.197-.299.699-.698 1.497-1.297 2.196-.999 1.398-2.396 2.596-3.794 3.395-.1 0-.2.1-.2.1-.1 0-.2 0-.2-.1-1.398-.699-2.795-1.997-3.794-3.395-.499-.699-.998-1.497-1.298-2.196-.3-.7-.499-1.498-.499-2.197C2.209 3.794 4.805 1.2 8 1.2M8 0C4.106 0 1.01 3.095 1.01 6.89c0 3.494 3.096 7.288 6.191 8.885.5.3 1.099.3 1.598 0 2.995-1.597 6.19-5.391 6.19-8.886C14.99 3.095 11.894 0 8 0Z" /></g></g>
        </svg>);

}