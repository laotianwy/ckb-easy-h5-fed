import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Xiangjicamera(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M5.862 1.529a.667.667 0 0 1 .472-.196h3.333c.177 0 .346.07.471.196l1.139 1.138h2.057a2 2 0 0 1 2 2V12a2 2 0 0 1-2 2H2.667a2 2 0 0 1-2-2V4.667a2 2 0 0 1 2-2h2.057l1.138-1.138Zm.748 1.138L5.472 3.805A.667.667 0 0 1 5 4H2.667A.667.667 0 0 0 2 4.667V12c0 .368.3.667.667.667h10.667A.667.667 0 0 0 14 12V4.667A.667.667 0 0 0 13.334 4H11a.667.667 0 0 1-.471-.195L9.39 2.667H6.61Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /><path d="M8 6a2 2 0 1 0 0 4 2 2 0 0 0 0-4ZM4.667 8a3.334 3.334 0 1 1 6.667 0 3.334 3.334 0 0 1-6.667 0Zm8-3.34c.368 0 .667.299.667.667v.006a.667.667 0 0 1-1.333 0v-.006c0-.368.298-.667.666-.667Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}