import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Shoujianrenxinxi(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M3.495 2.617c1.02-1.183 2.54-1.95 4.506-1.95 1.965 0 3.485.767 4.504 1.95C13.512 3.786 14 5.322 14 6.833c0 2.725-1.467 4.843-2.86 6.243a14.117 14.117 0 0 1-2.564 2.027 8.187 8.187 0 0 1-.236.138l-.015.008-.005.003c-.001 0-.001.001-.318-.585l-.318.586-.002-.001-.005-.003-.014-.008a6.801 6.801 0 0 1-.236-.138 14.123 14.123 0 0 1-2.564-2.027C3.467 11.676 2 9.558 2 6.833c0-1.511.488-3.048 1.495-4.216Zm4.506 12.05-.318.586a.669.669 0 0 0 .635 0l-.317-.586Zm0-.776a12.792 12.792 0 0 0 2.194-1.755c1.273-1.28 2.472-3.078 2.472-5.303 0-1.238-.402-2.452-1.172-3.346C10.737 2.607 9.59 2 8.001 2c-1.59 0-2.737.607-3.496 1.487-.77.894-1.172 2.108-1.172 3.346 0 2.225 1.2 4.023 2.473 5.303a12.784 12.784 0 0 0 2.195 1.755Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /><path d="M8 5.333a1.334 1.334 0 1 0 0 2.668 1.334 1.334 0 0 0 0-2.668ZM5.333 6.667a2.667 2.667 0 1 1 5.334 0 2.667 2.667 0 0 1-5.334 0Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}