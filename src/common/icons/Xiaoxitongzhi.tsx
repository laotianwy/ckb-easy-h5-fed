import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Xiaoxitongzhi(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 20 20"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><g clipPath="url(#104ad8__a)" strokeLinejoin="round" strokeLinecap="round" strokeWidth="1.67" data-follow-stroke="#505762" stroke={_stroke}><path d="M4.167 15.834V7.5a5.833 5.833 0 1 1 11.666 0v8.334m-14.166 0h16.666" /><path d="M10 18.333c1.15 0 2.083-.933 2.083-2.083v-.417H7.917v.417c0 1.15.932 2.083 2.083 2.083Z" /></g><defs><clipPath id="104ad8__a"><path d="M0 0h20v20H0z" data-follow-fill="#fff" fill={_fill} /></clipPath></defs></g>
        </svg>);

}