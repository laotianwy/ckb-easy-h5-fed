import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function Tipstishi(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      fill="none"
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path strokeLinejoin="round" strokeWidth="1.6" d="M7.667 14.333a6.65 6.65 0 0 0 4.714-1.952 6.646 6.646 0 0 0 1.952-4.714 6.646 6.646 0 0 0-1.952-4.714A6.646 6.646 0 0 0 7.667 1a6.643 6.643 0 0 0-4.714 1.953A6.646 6.646 0 0 0 1 7.667c0 1.84.746 3.507 1.953 4.714a6.646 6.646 0 0 0 4.714 1.952Z" data-follow-stroke="#FF7A45" stroke={_stroke} /><path strokeLinejoin="round" strokeLinecap="round" strokeWidth="1.6" d="M8 9V4.5" data-follow-stroke="#FF7A45" stroke={_stroke} /><path d="M8 12.042a.833.833 0 1 0 0-1.667.833.833 0 0 0 0 1.667Z" clipRule="evenodd" fillRule="evenodd" data-follow-fill="#FF7A45" fill={_fill} /></g>
        </svg>);

}