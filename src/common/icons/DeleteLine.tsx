import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function DeleteLine(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 14 14"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path data-follow-fill="#000" fillRule="evenodd" d="M8.271 3.18v-.636a1.272 1.272 0 1 0-2.544 0v.736H.685a.686.686 0 1 1 0-1.372h3.85a2.547 2.547 0 0 1 4.93 0h3.849a.686.686 0 0 1 0 1.372H8.27v-.1Zm3.183 1.273a.636.636 0 1 1 1.273 0v7.716c0 1.01-.82 1.83-1.83 1.83H3.102a1.83 1.83 0 0 1-1.83-1.83V4.453a.636.636 0 0 1 1.272 0v7.358c0 .505.41.915.915.915h7.08c.506 0 .915-.41.915-.915V4.453Zm-6.363.637c.351 0 .636.284.636.636v5.091a.636.636 0 0 1-1.272 0V5.726c0-.352.283-.636.636-.636Zm3.817 0c.352 0 .637.284.637.636v5.091a.636.636 0 1 1-1.273 0V5.726c0-.352.285-.636.636-.636Zm-.636-1.232h1.272-1.272Zm-3.818 0h1.273-1.273Z" fill={_fill} /></g>
        </svg>);

}