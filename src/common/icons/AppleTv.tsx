import React, { useEffect, useRef } from 'react';
import styles from './style.module.css';
interface IconProps extends React.SVGProps<SVGSVGElement> {
  size?: string | number;
  width?: string | number;
  height?: string | number;
  spin?: boolean;
  rtl?: boolean;
  color?: string;
  fill?: string;
  stroke?: string;
}

export default function AppleTv(props: IconProps) {
  const root = useRef<SVGSVGElement>(null);
  const { size = '1em', width, height, spin, rtl, color, fill, stroke, className, ...rest } = props;
  const _width = width || size;
  const _height = height || size;
  const _stroke = stroke || color;
  const _fill = fill || color;
  useEffect(() => {
    if (!_fill) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-fill]').forEach((item) => {
        item.setAttribute('fill', item.getAttribute('data-follow-fill') || '');
      });
    }
    if (!_stroke) {
      (root.current as SVGSVGElement)?.querySelectorAll('[data-follow-stroke]').forEach((item) => {
        item.setAttribute('stroke', item.getAttribute('data-follow-stroke') || '');
      });
    }
  }, [stroke, color, fill]);
  return (
    <svg
      ref={root}
      width={_width}
      height={_height}
      viewBox="0 0 880 106"
      preserveAspectRatio="xMidYMid meet"
      fill=""
      role="presentation"
      xmlns="http://www.w3.org/2000/svg"
      className={`${className || ''} ${spin ? styles.spin : ''} ${rtl ? styles.rtl : ''}`.trim()}
      {...rest}>

          <g><path d="M624 0v106h-16V0h16Zm106.021 1.698a124.102 124.102 0 0 1 5.62 14.98l-86.87 32.577a4 4 0 0 0 0 7.49l86.87 32.577a124.102 124.102 0 0 1-5.62 14.98l-86.867-32.575a20 20 0 0 1-11.705-11.705c-3.878-10.342 1.362-21.87 11.705-25.749ZM592 0v16h-75c-20.435 0-37 16.565-37 37s16.565 37 37 37h75v16h-75c-29.271 0-53-23.729-53-53s23.729-53 53-53h75Zm160 0h97.5C866.345 0 880 13.655 880 30.5c0 8.908-3.819 16.924-9.909 22.5 6.09 5.576 9.909 13.592 9.909 22.5 0 16.845-13.655 30.5-30.5 30.5H752V0Zm97.5 61H768v29h81.5c8.008 0 14.5-6.492 14.5-14.5S857.508 61 849.5 61Zm0-45H768v29h81.5c8.008 0 14.5-6.492 14.5-14.5S857.508 16 849.5 16ZM416 0v16H304v29h104v16H304v29h112v16H288V0h128ZM160 0v45h96V0h16v106h-16V61h-96v45h-16V0h16ZM0 16V0h128v16H72v90H56V16H0Z" data-follow-fill="#1C2026" fill={_fill} /></g>
        </svg>);

}