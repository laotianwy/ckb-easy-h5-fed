/*
 * @Author: shiguang
 * @Date: 2023-10-16 16:29:34
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-30 17:45:26
 * @Description:
 */
import {
	PayPalScriptProvider,
	PayPalHostedField,
	PayPalHostedFieldsProvider,
	usePayPalHostedFields
} from '@paypal/react-paypal-js';
import { useLocation, useNavigate } from 'react-router-dom';
import { OnApproveData } from '@paypal/paypal-js/types/components/buttons';
import { useRequest } from 'ahooks';
import { Toast } from 'antd-mobile';
import dayjs from 'dayjs';
import {
	Dispatch,
	SetStateAction,
	useCallback,
	useEffect,
	useState
} from 'react';
import queryString from 'query-string';
import { request } from '@/config/request';
import './index.scss';
import { useEvent } from '@/utils/hooks/useEvent';
import CustomModal from '@/component/CustomModal';
import PaypalLoading from './component/PaypalLoading';
const requestParams = {
	bizNo: '432437',
	bizType: 'EASY_ORDER',
	payTypeKey: 'PAYPAL_CREDIT',
	requestTime: '2023-10-27 10:28:08'
};
const onApproveParams = {
	billingToken: null,
	facilitatorAccessToken:
		'A21AAIl_dL_tRa-b3_ipUidHbFFNwpQHmvFe-Q8GR1S6AvMHtTJ4jCqGBDNwQBozRcLfW_aBIjD23l0EmsPFe4ItWs1oo55Ug',
	orderID: '9X286780R0879452N',
	payerID: 'XPXKAV4L4S8P2',
	paymentID: '9X286780R0879452N',
	paymentSource: 'paypal'
};
type OnApproveParams = typeof onApproveParams;
const initialOptions = {
	clientId: 'test',
	currency: 'USD',
	intent: 'capture'
};

// This value is from the props in the UI
const style = {
	background: 'red'
};
async function onApprove(data: OnApproveData) {
	await request.pay.front.payCapture({
		thirdId: data.orderID
	});
}
export interface SubmitPaymentProps {
	xx?: string;
	onCaptrueEnd?: (err?: any) => void;
}
const SubmitPayment = (props: SubmitPaymentProps) => {
	const { onCaptrueEnd } = props;
	const hostedField = usePayPalHostedFields();
	const [loadingType, setPaypalLoadingType] =
		useState<ENUM_PAYPAL_LOADING_TYPE>(ENUM_PAYPAL_LOADING_TYPE.NO_LOADING);
	const isLoading = loadingType !== ENUM_PAYPAL_LOADING_TYPE.NO_LOADING;
	const [amount, setAmount] = useState(200);
	const handleClick = async () => {
		try {
			// setPaying(true);
			setPaypalLoadingType(ENUM_PAYPAL_LOADING_TYPE.PAYPAL_PAY_ING);
			if (!hostedField?.cardFields) {
				const childErrorMessage =
					'Unable to find any child components in the <PayPalHostedFieldsProvider />';
				throw new Error(childErrorMessage);
			}
			const isFormInvalid = Object.values(
				hostedField.cardFields.getState().fields
			).some((field) => !field.isValid);
			if (isFormInvalid || !amount) {
				setPaypalLoadingType(ENUM_PAYPAL_LOADING_TYPE.NO_LOADING);
				Toast.show({
					content: window._$m.t('请填写正确的信用卡信息')
				});
				return;
			}
			const hostSubmitRes = await hostedField.cardFields.submit();

			/** 回调后端接口捕获 */
			await onApprove({
				orderID: hostSubmitRes.orderId
			} as any);
			setPaypalLoadingType(ENUM_PAYPAL_LOADING_TYPE.NO_LOADING);
		} catch (e: any) {
			const errorContent = `${e.type ?? ''} ${e.name ?? ''} ${e.message ?? ''}`;
			// 存在e为error字符串的情况
			if (errorContent) {
				Toast.show({
					content: errorContent
				});
			}
			console.log(e);
			setPaypalLoadingType(ENUM_PAYPAL_LOADING_TYPE.NO_LOADING);
			onCaptrueEnd?.(e);
			return;
		}
		onCaptrueEnd?.();
	};
	// const isPaypalLoading = false;
	// useEffect(() => {
	// 	if (isSniffRequestEnd === true) {
	// 		setPaypalLoadingType(ENUM_PAYPAL_LOADING_TYPE.NO_LOADING);
	// 	}
	// }, [isSniffRequestEnd]);

	return (
		<div>
			<PaypalLoading visible={isLoading} />
			<div
				style={{
					position: 'absolute',
					left: '50%',
					bottom: '27%',
					color: '#999',
					fontSize: '12px',
					transform: 'translate(-50%, -50%)'
				}}
			>
				<img
					src="https://theckbstest-oss.theckbs.com/client/252aa6fd431.png"
					style={{
						width: '66px',
						height: '16px'
					}}
					alt=""
				/>

				<span
					style={{
						position: 'relative',
						top: '-3px',
						left: '4px'
					}}
				>
					{window._$m.t('によるスービス提供')}
				</span>
			</div>
			<button
				// className={`btn${paying ? "" : "btn-primary"}` }
				className={isLoading ? '' : 'btn-primary'}
				style={{
					width: '96%',
					position: 'absolute',
					left: '2%',
					bottom: '22%',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					borderRadius: '22px',
					height: '44px',
					color: '#FFF',
					background: amount ? '#0070BA' : '#a4d0ed',
					border: 'none'
				}}
				onClick={isLoading ? undefined : handleClick}
			>
				{window._$m.t('お支払いへ')}
			</button>
		</div>
	);
};
export enum ENUM_PAYPAL_LOADING_TYPE {
	/** 不展示 loading */
	NO_LOADING,
	/** 初始化 sniff 数据 */
	INIT_SNIFF_DATA,
	/** paypal 加载SDK 中 */
	PAYPAL_INIT_SDK,
	/** paypal 支付中 */
	PAYPAL_PAY_ING
}
export interface PayPalCardPayProps {
	/** 订单编号 */
	orderNo: string;
	onCaptrueEnd?: (error?: any) => void;
}
const PayPalCardPay = (props: PayPalCardPayProps) => {
	// const navigate = useNavigate();
	const { orderNo, onCaptrueEnd } = props;
	const { data } = useRequest(request.pay.front.paypalClientId);
	const { data: paypalTokenData } = useRequest(request.pay.front.paypalToken);
	const clientId = data?.data;
	const dataClientToken = paypalTokenData?.data;
	const isSniffRequestEnd = Boolean(clientId && dataClientToken);
	// const wrapperOrderNo = orderNo;
	const createOrder = useEvent(async function (bizNo?: string) {
		const requestParams = {
			bizNo: orderNo,
			bizType: 'EASY_ORDER',
			payTypeKey: 'PAYPAL_CREDIT',
			requestTime: dayjs().format('YYYY-MM-DD HH:mm:ss')
		};
		const res = await request.pay.front.postFront(requestParams);
		// 该笔订单已支付，请勿重复支付
		if (res.code === '3006') {
			CustomModal.confirm({
				content: res.msg,
				showCloseButton: false,
				onClose: () => {
					const { channel } = queryString.parse(
						window.location.search
					);
					let _searchObj: any = {
						orderNo
					};
					if (channel) {
						_searchObj = {
							..._searchObj,
							channel
						};
					}
					const _search = queryString.stringify(_searchObj);
					window.location.replace('/order/detail?' + _search);
				},
				cancelText: window._$m.t('查看订单详情')
			});
			return '';
		}
		return res.data!.thirdId!;
	});
	return (
		<div className="page-paypal-card-pay">
			{!isSniffRequestEnd && (
				<PaypalLoading
					description={window._$m.t('paypal 加载中')}
					visible={true}
				/>
			)}

			{!isSniffRequestEnd ? null : (
				<PayPalScriptProvider
					options={{
						clientId: clientId!,
						dataClientToken,
						components: 'buttons,hosted-fields,funding-eligibility',
						intent: 'capture',
						currency: 'JPY'
					}}
				>
					<PayPalHostedFieldsProvider
						createOrder={createOrder}
						styles={{
							'.valid': {
								color: '#28a745'
							},
							'.invalid': {
								color: '#dc3545'
							},
							input: {
								'font-family': 'monospace',
								'font-size': '16px'
							}
						}}
					>
						<div
							style={{
								display: 'flex',
								alignItems: 'center',
								marginTop: '15px'
							}}
						>
							<label
								style={{
									width: '33%',
									fontSize: '14px',
									color: '#333333'
								}}
								htmlFor="card-number"
							>
								{window._$m.t('クレジットカード番号')}
								<span
									style={{
										color: '#dc3545'
									}}
								>
									*
								</span>
							</label>
							<PayPalHostedField
								id="card-number"
								className="paypal-card-field"
								hostedFieldType="number"
								style={{
									height: '36px',
									paddingTop: '5px',
									paddingBottom: '5px',
									fontSize: '14px'
								}}
								options={{
									selector: '#card-number',
									placeholder:
										window._$m.t('数字のみ入力可能')
								}}
							/>
						</div>
						<div
							style={{
								display: 'flex',
								alignItems: 'center',
								marginTop: '15px'
							}}
						>
							<label
								style={{
									width: '33%',
									fontSize: '14px',
									color: '#333333'
								}}
								htmlFor="expiration-date"
							>
								{window._$m.t('有効期限')}
								<span
									style={{
										color: '#dc3545'
									}}
								>
									*
								</span>
							</label>
							<PayPalHostedField
								id="expiration-date"
								className="paypal-card-field"
								hostedFieldType="expirationDate"
								style={{
									height: '36px',
									paddingTop: '5px',
									paddingBottom: '5px',
									fontSize: '14px'
								}}
								options={{
									selector: '#expiration-date',
									placeholder:
										window._$m.t(
											'数字のみ入力可能　例：05/2022'
										)
								}}
							/>
						</div>
						<div
							style={{
								display: 'flex',
								alignItems: 'center',
								marginTop: '15px'
							}}
						>
							<label
								style={{
									width: '33%',
									fontSize: '14px',
									color: '#333333'
								}}
								htmlFor="cvv"
							>
								セキュリティコード
								<span
									style={{
										color: '#dc3545'
									}}
								>
									*
								</span>
							</label>
							<PayPalHostedField
								id="cvv"
								className="paypal-card-field"
								hostedFieldType="cvv"
								style={{
									height: '36px',
									paddingTop: '5px',
									paddingBottom: '5px',
									fontSize: '14px'
								}}
								options={{
									selector: '#cvv',
									placeholder:
										window._$m.t('3-4桁の数字のみ入力可能'),
									maskInput: true
								}}
							/>
						</div>
						<SubmitPayment onCaptrueEnd={onCaptrueEnd} />
					</PayPalHostedFieldsProvider>
				</PayPalScriptProvider>
			)}
		</div>
	);
};
export default PayPalCardPay;
