import {
	PayPalScriptProvider,
	PayPalButtons,
	usePayPalScriptReducer
} from '@paypal/react-paypal-js';
import { OnApproveData } from '@paypal/paypal-js/types/components/buttons';
import { useRequest } from 'ahooks';
import { CSSProperties } from 'react';
import dayjs from 'dayjs';
import { Navigate, useNavigate } from 'react-router-dom';
import queryString from 'query-string';
import { request } from '@/config/request';
import { useEvent } from '@/utils/hooks/useEvent';
import CustomModal from '@/component/CustomModal';
import PaypalLoading from '../PayPalCardPay/component/PaypalLoading';
import { PayPalCardPayProps } from '../PayPalCardPay';
const onApproveParams = {
	billingToken: null,
	facilitatorAccessToken:
		'A21AAIl_dL_tRa-b3_ipUidHbFFNwpQHmvFe-Q8GR1S6AvMHtTJ4jCqGBDNwQBozRcLfW_aBIjD23l0EmsPFe4ItWs1oo55Ug',
	orderID: '9X286780R0879452N',
	payerID: 'XPXKAV4L4S8P2',
	paymentID: '9X286780R0879452N',
	paymentSource: 'paypal'
};
type OnApproveParams = typeof onApproveParams;
const initialOptions = {
	clientId: 'test',
	currency: 'USD',
	intent: 'capture'
};

// This value is from the props in the UI
const style = {
	background: 'red'
};
const requestParams = {
	bizNo: '432433',
	bizType: 'EASY_ORDER',
	payTypeKey: 'PAYPAL',
	requestTime: '2023-10-27 10:28:08'
};
export interface ButtonWrapperProps extends PayPalCardPayProps {
	/** 展示旋转亮片 */
	showSpinner?: boolean;
	pendingDom?: React.ReactNode;
}
const ButtonWrapper = ({
	showSpinner,
	onCaptrueEnd,
	orderNo
}: ButtonWrapperProps) => {
	// const navigate = useNavigate();
	const createOrder = useEvent(async function (bizNo?: string) {
		const requestParams = {
			bizNo: orderNo,
			bizType: 'EASY_ORDER',
			payTypeKey: 'PAYPAL',
			requestTime: dayjs().format('YYYY-MM-DD HH:mm:ss')
		};
		const res = await request.pay.front.postFront(requestParams);
		// 该笔订单已支付，请勿重复支付
		if (res.code === '3006') {
			CustomModal.confirm({
				content: res.msg,
				showCloseButton: false,
				onClose: () => {
					// window.location.replace('/order/detail?orderNo=' + orderNo);
					const { channel } = queryString.parse(
						window.location.search
					);
					let _searchObj: any = {
						orderNo
					};
					if (channel) {
						_searchObj = {
							..._searchObj,
							channel
						};
					}
					const _search = queryString.stringify(_searchObj);
					window.location.replace('/order/detail?' + _search);
				},
				cancelText: window._$m.t('查看订单详情')
			});
			return '';
		}
		return res.data!.thirdId!;
	});
	const [{ isPending }] = usePayPalScriptReducer();
	const pendingDom = <div>{window._$m.t('paypal 加载中')}</div>;
	async function onApprove(data: OnApproveData) {
		try {
			await request.pay.front.payCapture({
				thirdId: data.orderID
			});
		} catch (error) {
			onCaptrueEnd?.(error);
		}
		onCaptrueEnd?.();
	}
	return (
		<div
			style={{
				position: 'relative'
			}}
		>
			{showSpinner && isPending && pendingDom}

			<PayPalButtons
				style={{
					label: 'checkout',
					layout: 'vertical',
					shape: 'pill'
				}}
				disabled={false}
				// forceReRender={[style]}
				fundingSource="paypal"
				createOrder={createOrder}
				onApprove={onApprove}
			/>
		</div>
	);
};
export interface PayPalPayProps extends PayPalCardPayProps {
	style?: CSSProperties;
}
const PayPalPay = (props: PayPalPayProps) => {
	const { style, orderNo, onCaptrueEnd } = props;
	const { data } = useRequest(request.pay.front.paypalClientId);
	const clientId = data?.data;
	if (!clientId) return <div>{window._$m.t('paypal 加载中')}</div>;
	return (
		<div
			style={{
				// width: '1.7rem',
				// minHeight: '.4rem',
				// overflow: 'hidden',
				...style
			}}
		>
			<PayPalScriptProvider
				options={{
					clientId,
					components: 'buttons',
					currency: 'JPY'
				}}
			>
				<ButtonWrapper
					showSpinner={true}
					orderNo={orderNo}
					onCaptrueEnd={onCaptrueEnd}
				/>
			</PayPalScriptProvider>
		</div>
	);
};
export default PayPalPay;
