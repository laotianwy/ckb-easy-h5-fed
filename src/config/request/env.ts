/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-08 18:18:27
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-05 15:05:14
 * @FilePath: /ckb-easy-h5-fed/src/config/request/env.ts
 * @Description: 跳转到登录项目
 */
export const getEnv = () => {
	return '';
};
export const jumpToLogin = (url?: string) => {
	const _env = (process.env.REACT_APP_ENV as any) ?? 'test';

	if (_env === 'test') {
		window.location.href = `https://test-login-m.3fbox.com${url}`;
	}
	if (_env === 'pre') {
		// 跳转登陆页面（目前先写本地的登录）
		window.location.href = `https://pre-login-m.3fbox.com${url}`;
	}
	if (_env === 'prod') {
		// 跳转登陆页面（目前先写本地的登录）
		window.location.href = `https://login-m.3fbox.com${url}`;
	}
};
