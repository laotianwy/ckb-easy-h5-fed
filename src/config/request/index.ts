/*
 * @Author: shiguang
 * @Date: 2023-04-28 11:48:47
 * @LastEditors: shiguang
 * @LastEditTime: 2023-04-28 11:53:41
 * @Description: request
 */
export { request } from './interceptors';
