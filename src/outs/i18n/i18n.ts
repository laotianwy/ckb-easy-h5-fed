/*
 * @Author: shiguang
 * @Date: 2023-02-23 15:13:00
 * @LastEditTime: 2024-03-08 15:39:32
 * @LastEditors: huajian
 * @Description: react-i18next-config
 */

import i18n from 'i18next';
import { createContext } from 'react';
import queryString from 'query-string';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import * as SDK from '@snifftest/sdk/lib/rn';
import extractConfig from '../i18n/locales/extractConfig.json';
import { resources } from '../i18n/locales';
import jp from '../i18n/locales/ja_JP.json';
import kr from '../i18n/locales/ko_KR.json';
import zh from '../i18n/locales/zh_CN.json';
import en from '../i18n/locales/en_GB.json';
const isH5 = SDK.isH5;

export const lang: string | undefined = (() => {
	if (isH5()) {
		const qs = queryString.parse(window.location.search);
		let lang = qs?.lang as string | undefined;
		lang = lang ? lang.toLocaleLowerCase() : undefined;

		if (lang === 'ja' || lang === 'jp') {
			lang = 'ja';
		}
		return lang;
	}
})();

/** 本地语言文件枚举，从query中lang的语言类型 */
export enum Local {
	ZH = 'zh',
	/** 日文 */
	JA = 'ja',
	/** 韩文 */
	KO = 'ko',
	/** 英文 */
	EN = 'en'
}

/** 语言枚举 */
export enum Lang {
	zh_CN = 'zh_CN',
	/** 日文 */
	ja_JP = 'ja_JP',
	/** 韩文 */
	ko_KR = 'ko_KR',
	/** 英文 */
	en_GB = 'en_GB'
}

export const getLocalByLang = (() => {
	const getLocalByLangMap = new Map();
	getLocalByLangMap.set(Lang.zh_CN, Local.ZH);
	getLocalByLangMap.set(Lang.ja_JP, Local.JA);
	getLocalByLangMap.set(Lang.ko_KR, Local.KO);
	getLocalByLangMap.set(Lang.en_GB, Local.EN);

	return getLocalByLangMap;
})();

// url query 获取带过来的语言
const getQuerylang = (): Lang => {
	const query = queryString.parse(window.location.search) as { lang: Local };
	return {
		[Local.ZH]: Lang.zh_CN,
		[Local.JA]: Lang.ja_JP,
		[Local.KO]: Lang.ko_KR
		// [Local.EN]: Lang.en_GB
	}[query.lang];
};

/** 从缓存中获取当前的语言 */
const getOldLocalLang = () => {
	const lang = localStorage.getItem('__lang__') || Local.ZH;
	return (
		{
			[Local.ZH]: Lang.zh_CN,
			[Local.JA]: Lang.ja_JP,
			[Local.KO]: Lang.ko_KR
			// [Local.EN]: Lang.en_GB
		}[lang] || Lang.zh_CN
	);
};

/** 初始化获取当前的语言 */
export const getLang = () => {
	return Lang.ja_JP;
	// // 1.获取从query来的语言。如果存在就设置到缓存中。并返回

	// // ja_JP
	// const queryLang = getQuerylang();
	// if (queryLang) {
	//     localStorage.setItem('lang', queryLang);
	//     const _queryLang = {
	//         [Lang.zh_CN]: Local.ZH,
	//         [Lang.ja_JP]: Local.JA,
	//         [Lang.ko_KR]: Local.KO
	//         // [Lang.en_GB]: Local.EN
	//     }[queryLang];

	//     // __lang__ === ja
	//     localStorage.setItem('__lang__', _queryLang);
	//     return queryLang;
	// }

	// // 2.如果query没有数据。获取缓存中的语言数据。oldLocalLang === ja_JP
	// const oldLocalLang = getOldLocalLang();
	// if (oldLocalLang) {
	//     localStorage.setItem('lang', oldLocalLang);
	//     return oldLocalLang;
	// }
	// const lang = localStorage.getItem('lang');
	// if (lang) {
	//     return lang;
	// }
	// localStorage.setItem('lang', Lang.ja_JP);
};

const initLang = getLang();
export const LocalContext = createContext(initLang);

window._$m = {
	t: (text: string, options: { data?: Record<string, any> } = {}): string => {
		if (!isH5()) {
			return text;
		}
		const { pages, common } = extractConfig;
		const urlPathname = window.location.pathname;
		const item = pages
			.map((item: any) => {
				return {
					...item,
					linkTo: [...(item?.linkTo ?? []), item.path]
				};
			})
			.find((item) => {
				// 案例： item.urlPathname='/easy/goods/detail/:id'
				// urlPathname='/easy/goods/detail/AM-1018762624'
				if (item.urlPathname.includes('/:')) {
					const _urlPathname = item.urlPathname?.split('/:');
					if (urlPathname.includes(_urlPathname[0])) {
						return true;
					}
				}
				return item.urlPathname === urlPathname;
			})!;

		const linkTo = item?.linkTo ?? [];
		const ja_JP = window.g_locales
			? window.g_locales[initLang]
			: window._$m[initLang] ?? {};
		const commonTranslates = common.reduce((pre, cur) => {
			return {
				...pre,
				...(ja_JP?.[cur.path] ?? {})
			};
		}, {} as any);
		let getConfig = (() => {
			return linkTo.reduce((pre, cur) => {
				return {
					...commonTranslates,
					...pre,
					...ja_JP?.[cur]
				};
			}, {} as any);
		})();

		if (!isH5()) {
			getConfig = (() => {
				let result = {};
				Object.keys(jp).forEach((key) => {
					result = { ...result, ...jp[key] };
				});
				return result;
			})();
		}

		let lastText = ['', undefined].includes(getConfig[text])
			? text
			: (getConfig[text] as string);

		if (options.data) {
			for (const key in options.data) {
				if (Object.prototype.hasOwnProperty.call(options.data, key)) {
					const value = options.data[key];
					const templateStr = `{{${key}}}`;
					lastText = lastText.replace(templateStr, value as string);
				}
			}
		}

		return lastText;
	},
	[Lang.zh_CN]: zh,
	[Lang.ja_JP]: jp as any,
	[Lang.ko_KR]: kr,
	[Lang.en_GB]: en
};

export const strings = {
	t: (text: string, options: { data?: Record<string, any> } = {}): string => {
		if (isH5() && window?.location?.href?.indexOf('lang=zh') > -1) {
			return text;
		}
		const { pages, common } = extractConfig;
		const urlPathname = isH5() ? window.location.pathname : '';
		const item = pages
			.map((item: any) => {
				return {
					...item,
					linkTo: [...(item?.linkTo ?? []), item.path]
				};
			})
			.find((item) => {
				// 案例： item.urlPathname='/easy/goods/detail/:id'
				// urlPathname='/easy/goods/detail/AM-1018762624'
				if (item.urlPathname.includes('/:')) {
					const _urlPathname = item.urlPathname?.split('/:');
					if (urlPathname.includes(_urlPathname[0])) {
						return true;
					}
				}
				return item.urlPathname === urlPathname;
			})!;
		const linkTo = item?.linkTo ?? [];
		let getConfig;
		if (isH5()) {
			getConfig = (() => {
				const ja_JP = window.g_locales
					? window.g_locales[initLang]
					: window._$m[initLang] ?? {};
				const commonTranslates = common.reduce((pre, cur) => {
					return {
						...pre,
						...(ja_JP?.[cur.path] ?? {})
					};
				}, {} as any);
				return linkTo.reduce((pre, cur) => {
					return {
						...commonTranslates,
						...pre,
						...ja_JP?.[cur]
					};
				}, {} as any);
			})();
		} else {
			getConfig = (() => {
				let result = {};
				Object.keys(jp).forEach((key) => {
					result = { ...result, ...jp[key] };
				});
				return result;
			})();
		}

		let lastText = ['', undefined].includes(getConfig[text])
			? text
			: (getConfig[text] as string);

		if (options.data) {
			for (const key in options.data) {
				if (Object.prototype.hasOwnProperty.call(options.data, key)) {
					const value = options.data[key];
					const templateStr = `{{${key}}}`;
					lastText = lastText.replace(templateStr, value as string);
				}
			}
		}

		return lastText;
	},
	[Lang.zh_CN]: zh,
	[Lang.ja_JP]: jp as any,
	[Lang.ko_KR]: kr,
	[Lang.en_GB]: en
};

i18n.use(initReactI18next)
	.use(LanguageDetector)
	// 初始化
	.init({
		// 本地多语言数据
		lng: lang || window.localStorage?.getItem('i18nextLng') || 'ja_JP',
		resources,
		fallbackLng:
			lang || window.localStorage?.getItem('i18nextLng') || 'ja_JP',
		detection: {
			caches: ['localStorage', 'sessionStorage', 'cookie']
		}
	});
export default i18n;
