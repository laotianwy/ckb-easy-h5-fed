/*
 * @Author: yusha
 * @Date: 2023-12-14 10:46:09
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-01-22 19:44:59
 * @Description:
 */

/**
 * asyncStorage 缓存的Key
 * @type {string}
 */
export const ACCESSTOKEN = 'ACCESSTOKEN'; // token
/** 缓存用户手机号 */
export const PHONE_NUMBER = 'USERNAME';
/** 缓存用户名 */
export const USERNAME = 'USERNAME';
/** 缓存邮箱地址 */
export const USEREMAIL = 'USEREMAIL';
/** 缓存用户密码 */
export const PASSWORD = 'PASSWORD';
/** 应用提交审账户 */
export const EXAMINEUSER = 'EXAMINEUSER';
/** 首页新人指引 */
export const HOME_GUIDE = 'HOME_GUIDE';
/** 首页新人指引埋点 */
export const HOME_GUIDE_POINT = 'HOME_GUIDE_POINT';