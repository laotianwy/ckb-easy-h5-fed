/*
 * @Author: yusha
 * @Date: 2024-02-27 17:01:16
 * @LastEditors: yusha
 * @LastEditTime: 2024-02-28 18:05:12
 * @Description: 优惠券组件
 */
import { Image, Text, XStack, YStack } from '@snifftest/tamagui';
import { memo } from 'react';
import { ImageBackground, View } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { tool } from '@/outs/utils/tool';
import { CouponRespDTO } from '@/outs/service/easyMarket';

const fitSize = SDK.fitSize;
interface CouponProps {
  detail: CouponRespDTO;
}
const Coupon = (props: CouponProps) => {
  // 优惠劵类型 1:满减劵 2:满折劵
  const {
    couponType = 1,
    // 满金额条件
    feeToCut = 20000,
    // 	满件数条件
    numToCut = 0,
    title = tool.strings(
      '情人节限定优惠券情人节限定优惠券情人节限定优惠券情人节限定优惠券'
    ),
    couponContent = tool.strings(
      '仅限情人节专区使用情人节限定优惠券情人节限定优惠券情人节限定优惠券情人节限定优惠券'
    ),
    endDate = '2024.04.29 23:59',
    startDate = '2024.04.28 12:00',
    discountValue = 200
  } = props.detail;
  return (
    <ImageBackground
      resizeMode="contain"
      source={{
        uri: 'https://static-jp.theckb.com/static-asset/easy-app/couponbg.png'
      }}
      style={{
        width: fitSize(335),
        height: fitSize(100),
        display: 'flex',
        flexDirection: 'row'
      }}>

			<YStack
        justifyContent="center"
        alignItems="center"
        width={fitSize(90)}
        height={'100%'}>

				{couponType === 2 &&
        <XStack alignItems="center">
						<Text
            fontWeight={'600'}
            fontSize={fitSize(32)}
            color={'#FF5010'}>

							{discountValue}
						</Text>
						<YStack>
							<Text
              color={'#FF5010'}
              fontWeight={'600'}
              fontSize={fitSize(12)}>

								%
							</Text>
							<Text
              color={'#FF5010'}
              fontWeight={'600'}
              fontSize={fitSize(12)}>

								OFF
							</Text>
						</YStack>
					</XStack>}


				{couponType !== 2 &&
        <XStack alignItems="baseline" justifyContent="center">
						<Text
            color={'#FF5010'}
            fontSize={fitSize(32)}
            fontWeight={'600'}>

							{discountValue}
						</Text>
						<Text
            color={'#FF5010'}
            fontSize={fitSize(11)}
            fontWeight={'600'}>

							{tool.strings('円')}
						</Text>
					</XStack>}


				<Text
          color={'#FF5010'}
          fontWeight={'500'}
          fontSize={fitSize(12)}>

					{feeToCut > 0 && `满${feeToCut}円可用`}
					{numToCut > 0 && `满${numToCut}件可用`}
				</Text>
			</YStack>
			<YStack
        flex={1}
        height={'100%'}
        marginLeft={fitSize(8)}
        justifyContent="center"
        paddingRight={fitSize(12)}>

				<Text
          fontWeight={'400'}
          fontSize={fitSize(16)}
          color={'#242526'}
          lineHeight={fitSize(22)}
          marginBottom={fitSize(4)}
          numberOfLines={1}>

					{title}
				</Text>
				<Text
          fontWeight={'400'}
          fontSize={fitSize(12)}
          color={'#242526'}
          lineHeight={fitSize(17)}
          marginBottom={fitSize(8)}
          numberOfLines={1}>

					{couponContent}
				</Text>
				<Text
          fontWeight={'400'}
          fontSize={fitSize(12)}
          color={'#5B504D'}
          lineHeight={fitSize(17)}>

					{startDate} - {endDate}
				</Text>
			</YStack>
		</ImageBackground>);

};
export default memo(Coupon);