/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-28 10:01:29
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';

export const styles = StyleSheet.create({
  root: {
    backgroundColor: SDK.isH5() ? undefined : '#f0f2f5',
    paddingHorizontal: SDK.fitSize(12),
    flex: 1
  }
});