/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-26 17:34:57
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginBottom: px2dp(12)
  },
  input: {
    backgroundColor: '#fff',
    paddingLeft: px2dp(16),
    paddingRight: px2dp(20),
    fontSize: px2dp(14),
    padding: 0,
    height: px2dp(40),
    color: '#000',
    borderRadius: px2dp(20)
  },
  inputText: {
    height: px2dp(40),
    lineHeight: px2dp(40),
    color: 'rgba(176, 183, 194, 1)'
  },
  icon: {
    position: 'absolute',
    top: px2dp(13),
    left: px2dp(13),
    width: px2dp(20),
    height: px2dp(13)
  },
  camera: {
    position: 'absolute',
    right: px2dp(63),
    top: px2dp(11),
    width: px2dp(16),
    height: px2dp(16)
  },
  searchBtnWrap: {
    backgroundColor: '#000000',
    width: px2dp(48),
    height: px2dp(32),
    borderRadius: px2dp(16),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    right: px2dp(4),
    top: px2dp(4),
    position: 'absolute'
  },
  searchBtn: {
    width: px2dp(16),
    height: px2dp(16)
  }
});