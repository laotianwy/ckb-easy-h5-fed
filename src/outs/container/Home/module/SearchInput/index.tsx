/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-29 15:57:14
 * @Description: 头部模块
 */
import React from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { tool } from '@/outs/utils/tool';
import { styles } from './styles';

interface Props {
  channelTag?: string;
  navIndex?: number;
  onClick?: () => void;
}
export const SearchInput = ({ channelTag, navIndex, onClick }: Props) => {
  const nav = SDK.useNav();
  const goSearch = () => {
    onClick?.();
    if (navIndex === 3) {
      nav({ path: '/goods/search', name: 'Search' });
      return;
    }
    nav({ path: '/goods/search', name: 'Search' });
  };
  return (
    <View style={styles.container}>
			<TouchableOpacity activeOpacity={1} onPress={() => goSearch()}>
				<View style={styles.input}>
					<Text style={styles.inputText}>
						{tool.strings('请输入商品名称')}
					</Text>
				</View>
			</TouchableOpacity>
			<TouchableOpacity
        style={styles.searchBtnWrap}
        onPress={() => goSearch()}>

				<Image
          style={styles.searchBtn}
          source={{
            uri: 'https://static-s.theckb.com/BusinessMarket/App/home/search3625982.png'
          }} />

			</TouchableOpacity>
		</View>);

};