/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 10:23:46
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { tool } from '@/outs/utils/tool';

export const styles = StyleSheet.create({
  container: {},
  imgsWrap: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: tool.px2dp(4)
  },
  catesWrap: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});