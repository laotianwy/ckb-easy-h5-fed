/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 10:22:48
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const getStyles = (count: 2 | 4) =>
StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: '#fff',
    padding: px2dp(8),
    paddingHorizontal: px2dp(8.3),
    paddingVertical: px2dp(4),
    marginBottom: px2dp(12),
    display: 'flex',
    borderRadius: px2dp(8),
    width: count === 4 ? '100%' : 'auto'
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: px2dp(24),
    alignItems: 'center',
    marginBottom: px2dp(4)
  },
  headerTitle: {
    fontWeight: '600',
    fontSize: px2dp(14),
    fontFamily: 'PingFang SC',
    color: '#1C2026',
    width: px2dp(140)
  },
  headerTitle4: {
    width: px2dp(300)
  },
  headerImg: {
    width: px2dp(16),
    height: px2dp(16)
  },
  list: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  item: {
    width: px2dp(73),
    display: 'flex',
    marginLeft: px2dp(8)
  },
  itemCount4: {
    marginLeft: px2dp(14)
  },
  item0: {
    marginLeft: px2dp(0)
  },
  itemImg: {
    width: px2dp(73),
    height: px2dp(73),
    borderRadius: px2dp(4)
  },
  price: {
    color: 'rgb(28, 32, 38)',
    fontWeight: '500',
    textAlign: 'center',
    fontSize: px2dp(12),
    height: px2dp(20),
    fontFamily: 'PingFang SC',
    marginTop: px2dp(5)
  },
  showTitle: {
    color: '#FF5010',
    fontSize: 12,
    width: px2dp(73)
  }
});