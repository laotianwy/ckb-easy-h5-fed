/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 14:34:13
 * @Description: 头部模块
 */
import React, { useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  ImageBackground } from
'react-native';
import { useNavigation } from '@react-navigation/native';
import { useAtomValue } from 'jotai';
import * as SDK from '@snifftest/sdk/lib/rn';
import { changeImageCdn, toThousands } from '@/outs/utils';
import { tool } from '@/outs/utils/tool';
import {
  ActivityAreaVO,
  BarSimpleVO,
  ProductVO } from
'@/outs/service/easyMarket';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { PageProps } from '../../..';
import { getStyles } from './styles';

/** 背景色渐变 */
const MASK_BG =
'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/mask_bg.png';
interface Props extends Pick<PageProps, 'atomUserDetail'> {
  /** 活动数据 */
  data: ActivityAreaVO;
  // 显示的数量 一个区域按2个或者4个显示
  count: 2 | 4;
  channel: BarSimpleVO;
}
export const Goods = (props: Props) => {
  const navigationWebViewH5 = useNavigationWebViewH5();
  const useDetail = useAtomValue(
    SDK.isH5() ? props.atomUserDetail : atomUserDetail
  );
  /** 单个item paddingRight间距 */
  const ITEN_PADDING_RIGHT = props.count === 2 ? 10 : 14;
  const [data, setData] = useState<ProductVO[]>([]);
  const isLogin = useDetail?.customerId;
  /** 偏移量（偏移几个item） */
  const [contentOffset, setContentOffset] = useState<number>(0);
  /** 用于滚动到指定位置 */
  const scrollRef = useRef<any>(null);
  /** 单个item的宽度 */
  const sigleItemWidthRef = useRef<Number>(0);
  /** 样式 */
  const styles = getStyles(props.count);
  useEffect(() => {
    const topProductList = props.data!.topProductList ?? [];
    // 如果为2，则展示前两个商品，且不可滑动
    const _data =
    props.count === 2 ? topProductList.slice(0, 2) : topProductList;

    setData(_data);
  }, [props.count, props.data]);
  /** 前往商品列表 */
  const goToGoodsList = () => {
    const url = `/goods/list?keyword=&originSearch=false&noInput=true&pageTitle=${
    props.data.moduleName
    }&hotId=${props.data.id}&joinHot=true&showPrice=${!!props.data.
    showPrice}`;
    SDK.TA.track({
      event: TaEvent.BANNER_CLICK,
      properties: {
        position: 'product',
        channel_code: props.channel.barCode,
        channel_name: props.channel.barName,
        url
      }
    });
    if (SDK.isH5()) {
      window.location.href = url;
      return;
    }
    navigationWebViewH5(url);
  };

  return (
    <View style={[styles.container]}>
			<ImageBackground
        source={{ uri: MASK_BG }}
        style={{
          width: SDK.fitSize(172),
          height: SDK.fitSize(142),
          position: 'absolute',
          left: 0,
          top: 0
        }} />


			<TouchableOpacity activeOpacity={1} onPress={goToGoodsList}>
				<View style={styles.header}>
					<Text
            style={[
            styles.headerTitle,
            props.count === 4 ? styles.headerTitle4 : null]}

            numberOfLines={1}>

						{props.data.moduleName}
					</Text>

					<Image
            source={{
              uri: 'https://static-s.theckb.com/BusinessMarket/Easy/app/arrow-right.png'
            }}
            style={styles.headerImg} />

				</View>
				<View
          style={[
          styles.list,
          {
            borderRadius: SDK.fitSize(12)
          }]}>


					<View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              position: 'relative'
            }}>

						<ScrollView
              scrollEventThrottle={16}
              horizontal
              showsHorizontalScrollIndicator={false}
              ref={scrollRef}
              contentContainerStyle={{
                flexGrow: 1,
                flexDirection: 'row',
                alignItems: 'center'
              }}
              bounces={false}
              // 禁止回弹
              alwaysBounceHorizontal={false}
              alwaysBounceVertical={false}
              onScroll={(event) => {
                const width = ((sigleItemWidthRef.current ??
                0) as number);
                if (sigleItemWidthRef.current && width > 0) {
                  const { contentOffset } = event.nativeEvent;
                  const distance = contentOffset.x;
                  const num = Math.round(distance / width);
                  setContentOffset(num);
                }
              }}>

							{data.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={goToGoodsList}
                    activeOpacity={1}
                    key={index}
                    style={{
                      paddingRight:
                      index < data.length - 1 ?
                      SDK.fitSize(
                        ITEN_PADDING_RIGHT
                      ) :
                      SDK.fitSize(0)
                    }}
                    onLayout={(event) => {
                      const { width } =
                      event.nativeEvent.layout;
                      // 需要加上padding
                      sigleItemWidthRef.current = width;
                    }}>

										<Image
                      source={{
                        uri: changeImageCdn(
                          item.productMainImg,
                          150
                        )
                      }}
                      style={styles.itemImg} />


										<Text
                      style={[
                      styles.price,
                      isLogin || props.data.showPrice ?
                      undefined :
                      styles.showTitle]}

                      numberOfLines={1}>

											{isLogin || props.data.showPrice ?
                      toThousands(
                        item.activitySellPriceJpy ||
                        item.productLowestPrice
                      ) + tool.strings('元') :
                      tool.strings(
                        '免费注册查看价格'
                      )}
										</Text>
									</TouchableOpacity>);

              })}
						</ScrollView>

						{contentOffset > 0 &&
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {
                if (scrollRef.current) {
                  // Number和number不兼容
                  const _contentOffset = ((
                  contentOffset > 0 ?
                  contentOffset - 1 :
                  0) as
                  number);
                  setContentOffset(_contentOffset);
                  // Number和number不兼容
                  const width = (
                  (sigleItemWidthRef.current ??
                  0) as number);
                  scrollRef.current.scrollTo({
                    x: _contentOffset * width
                  });
                }
              }}
              style={{
                borderBottomRightRadius: 4,
                borderTopRightRadius: 4,
                paddingTop: SDK.fitSize(8),
                paddingBottom: SDK.fitSize(8),
                backgroundColor: 'rgba(0, 0, 0, 0.20)',
                position: 'absolute',
                left: SDK.fitSize(-8),
                top: SDK.fitSize(20)
              }}>

								<Image
                style={{
                  width: SDK.fitSize(16),
                  height: SDK.fitSize(16)
                }}
                source={{
                  uri: 'https://static-jp.theckb.com/static-asset/easy-app/arrow-left@3.x.png'
                }} />

							</TouchableOpacity>}


						{data.length > props.count &&
            contentOffset < data.length - props.count &&
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {
                if (scrollRef.current) {
                  // 数据减去一屏展示的四个
                  const _contentOffset =
                  contentOffset >=
                  data.length - props.count ?
                  data.length - props.count :
                  contentOffset + 1;
                  // Number和number不兼容
                  const itemWidth = (
                  sigleItemWidthRef.current as number);
                  setContentOffset(_contentOffset);
                  scrollRef.current.scrollTo({
                    x: itemWidth * _contentOffset
                  });
                }
              }}
              style={{
                borderBottomLeftRadius: 4,
                borderTopLeftRadius: 4,
                paddingTop: SDK.fitSize(8),
                paddingBottom: SDK.fitSize(8),
                backgroundColor: 'rgba(0, 0, 0, 0.20)',
                position: 'absolute',
                right: SDK.fitSize(-8),
                top: SDK.fitSize(20),
                zIndex: 100
              }}>

									<Image
                style={{
                  width: SDK.fitSize(16),
                  height: SDK.fitSize(16)
                }}
                source={{
                  uri: 'https://static-jp.theckb.com/static-asset/easy-app/arrow-right1.png'
                }} />

								</TouchableOpacity>}

					</View>
				</View>
			</TouchableOpacity>
		</View>);

};