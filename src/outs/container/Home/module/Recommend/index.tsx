/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 14:33:43
 * @Description: 头部模块
 */
import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  ActivityAreaVO,
  BarSimpleVO,
  FrontBarVO } from
'@/outs/service/easyMarket';
import { PageProps } from '../..';
import { styles } from './styles';
import { Img } from './Img';
import { Goods } from './Goods';

interface Props extends PageProps {
  /** 推荐数据 */
  data: FrontBarVO;
  channel: BarSimpleVO;
}

enum EnumHomeCoverShowType {
  // 商品推荐
  goods,
  // 图片推荐
  pic,
}

export const Recommend = (props: Props) => {
  const data = props.data;
  /** 图片块 */
  const [imgAreaList, setImgAreaList] = useState<ActivityAreaVO[]>();
  /** 分类块 */
  const [cateAreaList, setCateAreaList] = useState<ActivityAreaVO[]>();

  useEffect(() => {
    const imgAreas =
    data.activityAreaList?.filter((item) => {
      return item.homeCoverShowType === EnumHomeCoverShowType.pic;
    }) || [];
    const cateAreas =
    data.activityAreaList?.filter((item) => {
      if (item.homeCoverShowType !== EnumHomeCoverShowType.goods) {
        return false;
      }
      if (!item.topProductList?.length) {
        return false;
      }
      if (item.topProductList.length < 4) {
        return false;
      }
      return true;
    }) || [];
    // 如果图片块小于二 不展示
    if (imgAreas.length <= 1) {
      setImgAreaList([]);
    } else {
      // 如果是奇数 减1展示
      if (imgAreas.length % 2) {
        setImgAreaList(imgAreas.slice(0, imgAreas.length - 1));
      } else {
        setImgAreaList(imgAreas);
      }
    }
    // 类目块最多取6组
    setCateAreaList(cateAreas.slice(0, 6).filter((item) => !!item));
  }, [data]);

  // 类目块显示格式 1 组 4个，2组 22，三组 224，四组 2222，五组 22224 六组 2222222
  // 详见文档 https://www.figma.com/file/axGDRV8ywUq5xrTAYMysWS/ckb-appV2.0?type=design&node-id=872-8389&mode=design&t=TmBztWBUmurmpXHc-0
  let cateCounts: (2 | 4)[] = [];
  switch (cateAreaList?.length) {
    case 1:
      cateCounts = [4];
      break;
    case 2:
      cateCounts = [2, 2];
      break;
    case 3:
      cateCounts = [2, 2, 4];
      break;
    case 4:
      cateCounts = [2, 2, 2, 2];
      break;
    case 5:
      cateCounts = [2, 2, 2, 2, 4];
      break;
    case 6:
      cateCounts = [2, 2, 2, 2, 2, 2];
      break;
  }
  if (!imgAreaList?.length && !cateAreaList?.length) {
    return null;
  }

  return (
    <View style={styles.container}>
			{!!imgAreaList?.length &&
      <View style={styles.imgsWrap}>
					{imgAreaList?.map((imgArea, index) => {
          return (
            <TouchableOpacity key={index}>
								<Img
                data={imgArea}
                key={index}
                channel={props.channel} />

							</TouchableOpacity>);

        })}
				</View>}


			<View style={styles.catesWrap}>
				{!!cateAreaList?.length &&
        cateCounts.map((count, index) => {
          return (
            <Goods
              channel={props.channel}
              data={cateAreaList[index]}
              key={index}
              count={count}
              atomUserDetail={props.atomUserDetail} />);


        })}
			</View>
		</View>);

};