/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-19 10:20:15
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  container: {
    width: px2dp(171),
    height: px2dp(90),
    marginBottom: px2dp(8)
  },
  img: {
    width: px2dp(171),
    height: px2dp(90),
    borderRadius: px2dp(8)
  }
});