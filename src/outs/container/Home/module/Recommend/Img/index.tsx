/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 14:48:01
 * @Description: 头部模块
 */
import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { changeImageCdn } from '@/outs/utils';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { ActivityAreaVO, BarSimpleVO } from '@/outs/service/easyMarket';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { styles } from './styles';

interface Props {
  /** 活动数据 */
  data: ActivityAreaVO;
  channel: BarSimpleVO;
}

export const Img = (props: Props) => {
  const nav = SDK.useNav();
  const navigationWebViewH5 = useNavigationWebViewH5();
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        const url = `/goods/list?keyword=&originSearch=false&noInput=true&pageTitle=${
        props.data.moduleName
        }&hotId=${props.data.id}&joinHot=true&showPrice=${!!props.data.
        showPrice}`;
        SDK.TA.track({
          event: TaEvent.BANNER_CLICK,
          properties: {
            position: 'photo',
            channel_code: props.channel?.barCode,
            channel_name: props.channel?.barName,
            url
          }
        });
        if (SDK.isH5()) {
          nav({ path: url });
          return;
        }
        navigationWebViewH5(url);
      }}>

			<Image
        style={styles.img}
        source={{
          uri: changeImageCdn(props.data.homeImgUrl!, 750)
        }} />

		</TouchableOpacity>);

};