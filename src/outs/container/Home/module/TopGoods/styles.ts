/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-18 10:34:24
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  }
});