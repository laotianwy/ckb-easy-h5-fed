/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 20:24:26
 * @Description: 头部模块
 */
import React from 'react';
import { useAtomValue } from 'jotai';
import { View } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import { CusGoods } from '@/outs/components/CusGoods';
import { FrontBarVO } from '@/outs/service/easyMarket';
import { PageProps } from '../..';
import { styles } from './styles';

interface Props extends PageProps {
	/** 频道top数据 */
	data: FrontBarVO;
	favoriteCode?: string;
	changeFavorite?: (code: string) => void;
	useDetail?: any;
}

export const TopGoods = (props: Props) => {
	const { useDetail } = props;
	const isLogin = !!useDetail?.customerId;
	let list = props.data.topProductList;
	if (!props.data.topProductList?.length) {
		return null;
	}
	if (props.data.topProductList.length % 2) {
		list = props.data.topProductList.slice(
			0,
			props.data.topProductList.length - 1
		);
	}

	return (
		<View style={styles.container}>
			{list?.map((item, index) => {
				if (!item) {
					return null;
				}
				return (
					<CusGoods
						favoriteCode={props.favoriteCode}
						useDetail={useDetail}
						changeFavorite={props.changeFavorite}
						key={item.productCode}
						isLogin={isLogin}
						data={item}
						showPrice={isLogin || !!props.data.showPrice}
					/>
				);
			})}
		</View>
	);
};
