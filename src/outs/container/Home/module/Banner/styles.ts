/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-28 10:32:17
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';

export const styles = StyleSheet.create({
  container: {
    borderRadius: SDK.fitSize(12),
    overflow: 'hidden',
    marginBottom: SDK.fitSize(12)
  },
  img: {
    width: SDK.fitSize(351),
    height: SDK.fitSize(120),
    borderRadius: SDK.fitSize(12)
  }
});