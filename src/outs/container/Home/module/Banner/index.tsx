/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 14:46:33
 * @Description: 头部模块
 */
import React, { useEffect, useState } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Swiper from '@nutui/nutui-react-native/components/swiper';
import SwiperItem from '@nutui/nutui-react-native/components/swiperitem';
import * as SDK from '@snifftest/sdk/lib/rn';
import { Img } from '@snifftest/img';
import { request } from '@/config/request/interceptors';
import { BannerRespDTO, BarSimpleVO } from '@/outs/service/easyMarket';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { styles } from './styles';

interface Props {
  /** 频道code值 */
  barCode?: string;
  channel?: BarSimpleVO;
}

export const Banner = (props: Props) => {
  const nav = SDK.useNav();
  const navigationWebViewH5 = useNavigationWebViewH5();
  const [banners, setBanners] = useState<BannerRespDTO[]>();
  useEffect(() => {
    if (!props.barCode) {
      return;
    }
    request.easyMarket.banner.
    show(
      {
        barCode: props.barCode,
        bizScene: '1',
        sourceChannel: '2'
      },
      { useMock: false }
    ).
    then((res) => {
      setBanners(res.data);
    });
  }, [props.barCode]);
  if (!banners?.length) {
    return null;
  }
  return (
    <View style={styles.container}>
			<Swiper
        width={('100%' as any)}
        height={SDK.fitSize(120)}
        paginationColor="#000000"
        paginationBgColor="#426ddd"
        autoPlay={3000}
        initPage={0}
        paginationVisible>

				{banners?.map((banner, index) => {
          return (
            <SwiperItem key={index}>
							<TouchableOpacity
                onPress={() => {
                  if (
                  !(
                  typeof banner.jumpUrl ===
                  'string' &&
                  banner.jumpUrl.trim().length > 0))

                  {
                    return;
                  }
                  SDK.TA.track({
                    event: TaEvent.BANNER_CLICK,
                    properties: {
                      position: 'banner',
                      channel_code:
                      props.channel?.barCode,
                      channel_name:
                      props.channel?.barName,
                      url: banner.jumpUrl
                    }
                  });
                  // TODO 重点测试 跳转
                  if (
                  banner.jumpUrl?.includes('m.3fbox.com'))
                  {
                    if (SDK.isH5()) {
                      window.location.href =
                      banner.jumpUrl;
                      return;
                    }
                    navigationWebViewH5(banner.jumpUrl);
                    return;
                  }

                  nav({
                    name: 'WebviewOther',
                    path: banner.jumpUrl
                  });
                }}>

								<Img
                  src={banner.picUrl!}
                  width={SDK.fitSize(351)}
                  height={SDK.fitSize(120)} />

							</TouchableOpacity>
						</SwiperItem>);

        })}
			</Swiper>
		</View>);

};