/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-07 18:38:04
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';

export const styles = StyleSheet.create({
  containerWrap: {
    overflow: 'hidden',
    borderRadius: SDK.fitSize(12),
    marginBottom: SDK.fitSize(12),
    position: 'relative'
  },
  container: {
    position: 'relative',
    height: SDK.fitSize(32),
    overflow: 'scroll',
    paddingRight: SDK.fitSize(32)
  },
  tags: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-between'
  },
  tag: {
    padding: SDK.fitSize(6),
    paddingHorizontal: SDK.fitSize(12),
    color: '#000',
    backgroundColor: '#fff',
    borderRadius: SDK.fitSize(16),
    marginRight: SDK.fitSize(12),
    overflow: 'hidden'
  },
  selected: {
    backgroundColor: '#1C2026',
    color: '#fff'
  },
  cartWrap: {
    position: 'absolute',
    right: SDK.fitSize(-1),
    width: SDK.fitSize(32),
    height: SDK.fitSize(32),
    borderRadius: SDK.fitSize(16),
    backgroundColor: 'rgba(255,255,255,1)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cart: {
    width: SDK.fitSize(16),
    height: SDK.fitSize(16)
  },
  gradient: {
    position: 'absolute',
    right: 0,
    top: 0,
    height: SDK.fitSize(32),
    width: SDK.fitSize(50),
    borderRadius: SDK.fitSize(16),
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0
  },

  gradientH5: {
    position: 'absolute',
    right: 0,
    top: 0,
    width: SDK.fitSize(100),
    height: SDK.fitSize(32),
    'pointer-events': 'none',
    'border-radius': SDK.fitSize(16),
    'border-top-left-radius': 0,
    'border-bottom-left-radius': 0
  }
});