/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 16:29:06
 * @Description: 头部模块
 */
import React, { useEffect, useRef, useState } from 'react';
import { Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { View } from '@snifftest/tamagui';
import * as SDK from '@snifftest/sdk/lib/rn';
import { ProductCategoryFrontendShortRespDTO } from '@/outs/service/easyGoods';
import { request } from '@/config/request/interceptors';
import { useNavWebViewH5 } from '@/outs/hooks/common/useNavWebViewH5';
import { ENUM_NAVINDEX } from '../..';
import { styles } from './styles';

interface Props {
	/** 频道code值 */
	channelTag?: string;
	onChange: (item: ProductCategoryFrontendShortRespDTO) => void;
	navIndex?: number;
	propsCateId?: number;
	navigationWebViewH5: (url: string) => void;
}

export const Cates = (props: Props) => {
	const { navigationWebViewH5 } = props;
	const nav = SDK.useNav();
	const [items, setItems] = useState<ProductCategoryFrontendShortRespDTO[]>();
	const [cateId, setCateId] = useState<number>();
	const cateScrollRef = useRef<any>(null);

	/** 存放每个类目元素的宽度 */
	// const [elementWidths, setElementWidths] = useState<number[]>([]);
	const [elemetMap, setElemetMap] = useState<Map<string, number>>(new Map());

	useEffect(() => {
		if (!props.propsCateId && cateScrollRef.current) {
			cateScrollRef.current.scrollTo({
				x: 0,
				animated: true
			});
			setCateId(undefined);
		}
	}, [props.propsCateId]);

	useEffect(() => {
		if (!props.channelTag) {
			return;
		}
		if (props.navIndex === ENUM_NAVINDEX.OTHER) {
			return;
		}

		request.easyGoods.productCategoryFrontend.tree({}).then((res) => {
			setItems(res.data!);
			// 当channelTag发生变化时，置顶
			cateScrollRef.current.scrollTo({
				x: 0,
				animated: true
			});
			setCateId(undefined);
		});
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [props.navIndex]);

	useEffect(() => {
		if (props.navIndex !== ENUM_NAVINDEX.OTHER) {
			return;
		}
		request.easyGoods.productCategoryFrontend.tree({}).then((res) => {
			setItems(res.data!);
			// 当channelTag发生变化时，置顶
			cateScrollRef.current.scrollTo({
				x: 0,
				animated: true
			});

			setCateId(undefined);
		});
	}, [props.navIndex]);

	if (!props.channelTag) {
		return null;
	}

	return (
		<View style={styles.containerWrap}>
			<ScrollView
				horizontal
				showsHorizontalScrollIndicator={false}
				style={styles.container}
				ref={cateScrollRef}
			>
				<View style={styles.tags}>
					{items?.map((item, index) => {
						return (
							<View
								onLayout={(event) => {
									// 获取元素宽度
									const { width } = event.nativeEvent.layout;
									if (!elemetMap.get(item.cateNameJp!)) {
										elemetMap.set(item.cateNameJp!, width);
										setElemetMap(elemetMap);
									}
									// // 存放
									// elementWidths[index] = width;
									// setElementWidths(elementWidths);
								}}
								key={`${props.channelTag}_${item.productCategoryFrontendId}_${index}`}
								onPress={() => {
									props.onChange(item);
								}}
								style={{
									marginRight:
										index === items?.length - 1
											? SDK.fitSize(32)
											: 'auto'
								}}
							>
								<Text style={[styles.tag]}>
									{item.cateNameJp}
								</Text>
							</View>
						);
					})}
				</View>
			</ScrollView>
			{SDK.isH5() ? (
				<View className="gradientH5" />
			) : (
				<LinearGradient
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 0 }}
					style={styles.gradient}
					colors={['rgba(255, 255, 255, 0)', '#F0F3F6']}
				/>
			)}

			<View style={styles.cartWrap}>
				<TouchableOpacity
					onPress={() => {
						const url = '/goods/classify';
						if (SDK.isH5()) {
							nav({ path: url });
							return;
						}
						navigationWebViewH5(url);
					}}
				>
					<Image
						style={styles.cart}
						source={{
							uri: 'https://static-s.theckb.com/BusinessMarket/App/home/hamburger36252636.png'
						}}
					/>
				</TouchableOpacity>
			</View>
		</View>
	);
};
