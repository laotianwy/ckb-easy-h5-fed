/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-22 15:54:59
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-28 13:44:29
 * @FilePath: /ckb-fbox-app/src/container/goods/DirectHome/module/unLoginTips/index.tsx
 * @Description: 未登录首页提示
 */
import { Image, TouchableOpacity } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { goToLogin } from '@/config/request/interceptors';

const UNLOGIN_TIPS =
'https://static-jp.theckb.com/static-asset/easy-app/home_unLogin_tips.png';

interface UnLoginTipsProps {
  bottomPx: number;
}

const UnLoginTips = (props: UnLoginTipsProps) => {
  const nav = SDK.useNav();
  const { bottomPx } = props;
  return (
    <TouchableOpacity
      style={(
      {
        position: SDK.isH5() ? 'fixed' : 'absolute',
        bottom: SDK.fitSize(bottomPx + 8),
        left: SDK.fitSize(SDK.isH5() ? 12 : 0),
        right: 0
      } as any)}

      activeOpacity={1}
      onPress={() => {
        if (SDK.isH5()) {
          goToLogin();
          return;
        }
        nav({ name: 'Login' });
      }}>

			<Image
        source={{ uri: UNLOGIN_TIPS }}
        style={{
          width: SDK.getDeviceSize().width - 24,
          height: SDK.fitSize(44)
        }} />

		</TouchableOpacity>);

};

export default UnLoginTips;