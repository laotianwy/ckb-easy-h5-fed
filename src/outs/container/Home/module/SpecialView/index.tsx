/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-03-21 13:24:56
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 23:15:20
 * @FilePath: /3fbox-page-mid/src/outs/container/Home/module/SpecialView/index.tsx
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { useEffect, useState } from 'react';
import { InView } from 'react-native-intersection-observer';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { BarCodePrice, BARCODE } from '@/outs/const/index';
import { request } from '@/config/request/interceptors';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { tool } from '@/outs/utils/tool';
import { CollectModal } from '../Fund/components/CollectModal';
import { ENUM_TABBAR, ENUM_TABBAR_SPEC_TEXT } from '../Navigation/config';
interface SpeciaProductProps {
	name?: string;
	id?: string;
	price?: string;
}

interface SpecialViewProps {
	title?: string;
	list?: CollectionActivityRespDTO[];
	titleClick?: (title: string) => void;
	refuse: () => Promise<void>;
	barCode: string;
}

const SpecialView = (props: SpecialViewProps) => {
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const { list = [], titleClick, refuse, barCode } = props;
	const [activeId, setActiveId] = useState<number>(0);
	const [likeMaxWidth, setLikeMaxWidth] = useState(0);
	if (list?.length === 0) {
		return null;
	}

	const refuseList = () => {
		refuse();
		setActiveId(0);
	};

	return (
		<InView
			onChange={(inView) => {
				if (inView) {
					SDK.TA.track({
						event: TaEvent.PAGE_VIEW,
						properties: {
							position: 'hundred_area'
						}
					});
				}
			}}
		>
			<View
				style={{
					width: '100%',
					height: SDK.fitSize(246),
					marginBottom: SDK.fitSize(12),
					backgroundColor: '#fff',
					paddingVertical: SDK.fitSize(16),
					paddingLeft: SDK.fitSize(8),
					borderRadius: SDK.fitSize(8),
					overflow: 'hidden',
					marginTop: SDK.fitSize(12)
				}}
			>
				<TouchableOpacity
					onPress={() => {
						SDK.TA.track({
							event: TaEvent.BTN_CLICK,
							properties: {
								position: 'hundred_area_more'
							}
						});
						titleClick?.(ENUM_TABBAR_SPEC_TEXT[barCode]);
					}}
					style={{ flexDirection: 'row' }}
				>
					<Text
						style={{
							fontSize: SDK.fitSize(17),
							lineHeight: SDK.fitSize(22),
							fontWeight: '600',
							color: 'rgba(0, 0, 0, 0.88)'
						}}
					>
						{ENUM_TABBAR_SPEC_TEXT[barCode]}
					</Text>
					<Image
						source={{
							uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/right-arrow-spec.png'
						}}
						style={{
							width: SDK.fitSize(22),
							height: SDK.fitSize(22),
							marginLeft: SDK.fitSize(8)
						}}
					/>
				</TouchableOpacity>
				<ScrollView
					style={{
						flex: 1,
						marginTop: SDK.fitSize(16)
					}}
					showsHorizontalScrollIndicator={false}
					horizontal
				>
					{list.map((item, index) => {
						return index < 10 ? (
							<TouchableOpacity
								key={index}
								onPress={() => {
									SDK.TA.track({
										event: TaEvent.BTN_CLICK,
										properties: {
											position: 'hundred_area',
											collection_name: item.moduleName
										}
									});
									// 跳转到特辑的详情
									// const url = `/goods/collectionGoodsList?id=${item.id}`;
									const url1 = `/goods/list?fromCollection=true&originSearch=false&noInput=true&pageTitle=${encodeURIComponent(
										item.moduleName
									)}&hotId=${
										item.id
									}&joinHot=true&showPrice=true`;
									if (SDK.isH5()) {
										nav({ path: url1 });
										return;
									}
									navigationWebViewH5(url1);
								}}
							>
								<View
									key={index}
									style={{
										width: SDK.fitSize(160),
										height: SDK.fitSize(176),
										marginRight: SDK.fitSize(8)
									}}
								>
									<Image
										source={{
											uri: item.homeImgUrl
										}}
										style={{
											width: SDK.fitSize(160),
											height: SDK.fitSize(120),
											borderRadius: SDK.fitSize(8),
											overflow: 'hidden'
										}}
									/>
									<View style={{ marginTop: SDK.fitSize(8) }}>
										<Text
											style={{
												fontSize: SDK.fitSize(16),
												lineHeight: SDK.fitSize(22),
												color: 'rgba(0, 0, 0, 0.88)'
											}}
										>
											{item.barName}
										</Text>
										<View
											style={{
												flexDirection: 'row',
												justifyContent: 'space-between',
												paddingRight: SDK.fitSize(6),
												alignItems: 'center'
											}}
										>
											<Text
												numberOfLines={1}
												style={{
													width: SDK.fitSize(144),
													fontSize: SDK.fitSize(14),
													lineHeight: SDK.fitSize(22),
													fontWeight: '500',
													color: 'rgba(0, 0, 0, 0.65)'
												}}
											>
												{item.moduleName}
											</Text>
											<TouchableOpacity
												onPress={(e) => {
													e.stopPropagation();
													SDK.TA.track({
														event: TaEvent.BTN_CLICK,
														properties: {
															position:
																'hundred_area_operate'
														}
													});
													setActiveId(item.id);
												}}
											>
												<Image
													source={{
														uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/more_icon.png'
													}}
													resizeMethod="resize"
													style={{
														width: SDK.fitSize(16),
														height: SDK.fitSize(16)
													}}
												/>
											</TouchableOpacity>
										</View>
									</View>
									<CollectModal
										show={item.id === activeId}
										refuse={refuseList}
										width={160}
										height={176}
										id={item.id}
										setActiveId={setActiveId}
										item={item}
									/>
								</View>
							</TouchableOpacity>
						) : (
							<></>
						);
					})}
				</ScrollView>
			</View>
		</InView>
	);
};

export default SpecialView;
