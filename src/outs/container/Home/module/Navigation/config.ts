/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-22 16:41:01
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 09:54:15
 * @FilePath: /3fbox-page-mid/src/outs/container/Home/module/Navigation/config.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import * as SDK from '@snifftest/sdk/lib/rn';
import { TaEvent } from '@/outs/utils/TDAnalytics';

/** tabbar 对应接口barCode */
export enum ENUM_TABBAR {
	TABBAR_FIVE = 'BAR005',
	TABBAR_FIRST = 'BAR001',
	TABBAR_SECOND = 'BAR002',
	TABBAR_THIRD = 'BAR003',
	TABBAR_FOURTH = 'BAR004'
}

export enum ENUM_TABBAR_SPEC_TEXT {
	'BAR005' = '百円グッズ特集',
	'BAR001' = '100円グッズ特集',
	'BAR002' = '500円グッズ特集',
	'BAR003' = '1000円グッズ特集',
	'BAR004' = '百円グッズ特集'
}
/** 埋点类型 */
enum ENUM_TABBAR_TYPE {
	CHANNEL_FIND = 'channel_find',
	CHANNEL_100 = 'channel_100',
	CHANNEL_500 = 'channel_500',
	CHANNEL_1000 = 'channel_1000',
	CHANNEL_DISCOUNT_72 = 'channel_discount_72'
}
const boryingPointAction = (type: ENUM_TABBAR_TYPE) => {
	SDK.TA.track({
		event: TaEvent.BTN_CLICK,
		properties: {
			position: type
		}
	});
};

/** 底部tabbar_埋点 */
export const MAP_TABBAR = new Map([
	[
		ENUM_TABBAR.TABBAR_FIRST,
		{
			boryingPoint: () => boryingPointAction(ENUM_TABBAR_TYPE.CHANNEL_100)
		}
	],
	[
		ENUM_TABBAR.TABBAR_SECOND,
		{
			boryingPoint: () => boryingPointAction(ENUM_TABBAR_TYPE.CHANNEL_500)
		}
	],
	[
		ENUM_TABBAR.TABBAR_THIRD,
		{
			boryingPoint: () =>
				boryingPointAction(ENUM_TABBAR_TYPE.CHANNEL_1000)
		}
	],
	[
		ENUM_TABBAR.TABBAR_FOURTH,
		{
			boryingPoint: () =>
				boryingPointAction(ENUM_TABBAR_TYPE.CHANNEL_DISCOUNT_72)
		}
	],
	[
		ENUM_TABBAR.TABBAR_FIVE,
		{
			boryingPoint: () =>
				boryingPointAction(ENUM_TABBAR_TYPE.CHANNEL_FIND)
		}
	]
]);
