/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 17:22:31
 * @Description: 头部模块
 */
import React, { useEffect, useState } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { useMount } from 'ahooks';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useAtomValue } from 'jotai';
import * as SDK from '@snifftest/sdk/lib/rn';
import { request } from '@/config/request/interceptors';
import { BarSimpleVO } from '@/outs/service/easyMarket';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import UnLoginTips from '../unLoginTips';
import { PageProps } from '../..';
import { ENUM_TABBAR, MAP_TABBAR } from './config';
import { styles } from './styles';

export interface BarSimpleVOEx extends BarSimpleVO {
	isSeleced?: boolean;
	/** 第一个bar find执行的特殊操作 */
	specialOpearte?: boolean;
}

interface Props extends PageProps {
	onChange: (
		item: BarSimpleVOEx,
		index: number,
		excludeChannelTagList?: string[]
	) => void;
}

let excludeChannelTagList: string[] = [];

export const useGetNavigationBottom = () => {
	const [bottomPosi, setBottomPosi] = useState(0);
	const safeInsets = useSafeAreaInsets();
	useEffect(() => {
		if (SDK.isH5()) {
			return;
		}
		setBottomPosi(safeInsets.bottom + SDK.fitSize(4));
	}, [setBottomPosi, safeInsets]);

	return bottomPosi;
};

export const NAVIGATION_HEIGHT = 52;

export const Navigation = (props: Props) => {
	const [items, setItems] = useState<BarSimpleVOEx[]>();
	const bottomPx = useGetNavigationBottom();
	const useDetail = useAtomValue(
		SDK.isH5() ? props.atomUserDetail : atomUserDetail
	);
	useMount(() => {
		request.easyMarket.front.barListV2().then((res) => {
			// @ts-ignore
			res.data = (res.data ?? []).map((item) => ({
				...item,
				id: item.barCode === 'BAR005' ? '0' : item.id
			}));
			res.data = res.data?.sort(
				(s1, s2) => Number(s1.id) - Number(s2.id)
			);
			const resultRes = res.data?.map((item) => ({
				...item,
				specialOpearte: item.barCode === 'BAR005'
			}));
			if (resultRes?.length) {
				const tempItems = resultRes as any as BarSimpleVOEx[];
				tempItems[0].isSeleced = true;
				excludeChannelTagList = tempItems.map(
					(item) => item.barCode
				) as any;

				// 如果是其他正常的barcode，那么执行获取接口的操作
				props.onChange(tempItems[0], 0, excludeChannelTagList);
			}
			setItems(resultRes as any as BarSimpleVOEx[]);
			/** 执行第四个埋点 */
			MAP_TABBAR.get(ENUM_TABBAR.TABBAR_FOURTH)?.boryingPoint();
		});
	});

	return (
		<View
			style={[
				styles.container,
				{
					bottom: SDK.isH5() ? undefined : bottomPx,
					height: SDK.fitSize(NAVIGATION_HEIGHT)
				}
			]}
		>
			{!useDetail?.customerId && !SDK.isH5() && (
				<UnLoginTips bottomPx={NAVIGATION_HEIGHT} />
			)}

			{items?.map((item, index) => {
				return (
					<TouchableOpacity
						key={index}
						style={[styles.tag, item.isSeleced && styles.selected]}
						activeOpacity={0.8}
						onPress={() => {
							/** 通过接口barCode获取对应的埋点 */
							MAP_TABBAR.get(
								item?.barCode as ENUM_TABBAR
							)?.boryingPoint();
							items.forEach((tempItem) => {
								tempItem.isSeleced = false;
							});

							item.isSeleced = true;
							if (index === 0) {
								props.onChange(
									item,
									index,
									excludeChannelTagList
								);
							} else {
								props.onChange(item, index);
							}
							setItems([...items]);
						}}
					>
						<Image
							style={styles.tagImg}
							source={{
								uri: item.isSeleced
									? item.choseImgUrl
									: item.unChoseImgUrl
							}}
						/>
					</TouchableOpacity>
				);
			})}
		</View>
	);
};
