/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-22 09:54:24
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';

export const styles = StyleSheet.create({
	container: {
		backgroundColor: '#1C2026',
		position: SDK.isH5() ? 'fixed' : ('absolute' as any),
		bottom: SDK.isH5() ? SDK.fitSize(4) : ('auto' as any),
		left: 0,
		right: 0,
		padding: SDK.fitSize(2),
		borderRadius: SDK.fitSize(28),
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginLeft: SDK.fitSize(12),
		marginRight: SDK.fitSize(12)
	},
	tag: {
		width: SDK.fitSize(60),
		height: SDK.fitSize(48),
		borderRadius: SDK.fitSize(24),
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	tagImg: {
		width: SDK.fitSize(28),
		height: SDK.fitSize(28)
	},
	selected: {
		backgroundColor: '#fff'
	}
});
