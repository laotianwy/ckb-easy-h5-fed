/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-16 18:26:12
 * @Description: 头部模块
 */
import React, { useCallback, useEffect, useState } from 'react';
import { useAtom, useAtomValue } from 'jotai';
import { useRequest, useToggle } from 'ahooks';
import { View, Image, TouchableOpacity } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import * as SDK from '@snifftest/sdk/lib/rn';
import { Badge } from '@/outs/components/Badge';
import { request, goToLogin } from '@/config/request/interceptors';
import { atomUnreadCount } from '@/outs/atom/atomUnreadCount';
import { atomCartCount } from '@/outs/atom/atomCartCount';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { useNavigation } from '@/outs/hooks/base/useNavigation';
import { usePageFocuEffect } from '@/outs/hooks/common/usePageFocuEffect';
import { PageProps } from '../../';
import { styles } from './styles';

export const Header = (props: PageProps) => {
	const { HomeSlider } = props;
	const [homeSidebarVisible, { toggle }] = useToggle(false);
	const navigationWebViewH5 = useNavigationWebViewH5();
	const navigation = useNavigation() as any;
	const nav = SDK.useNav();
	const useDetail = useAtomValue(props.atomUserDetail);
	const [unreadCount, setUnreadCount] = useAtom(atomUnreadCount);
	/** 购物车数量 */
	const [cartCount, setCartCount] = useAtom(atomCartCount);
	const isLogin = useDetail?.customerId;
	/** 更新购物车数量  */
	const { runAsync: getCartList } = useRequest(request.easyOrder.cart.list, {
		manual: true
	});
	/** 获取私有未读消息数量 */
	const { runAsync: getPrivateUnreadCount } = useRequest(
		request.customer.notify.getPrivateUnreadCount,
		{ manual: true }
	);
	/** 获取消息未读数量 */
	const getUnreadCount = useCallback(() => {
		(async () => {
			// 未登录不需要后续请求
			if (!isLogin) {
				return setUnreadCount(0);
			}
			const data = await getPrivateUnreadCount();
			setUnreadCount(data.data ?? 0);
		})();
	}, [getPrivateUnreadCount, isLogin, setUnreadCount]);
	// 聚焦时就更新消息未读数量
	usePageFocuEffect(getUnreadCount);

	/** 获取购物车数量 */
	const getCount = useCallback(() => {
		(async function () {
			// 未登录不请求接口
			if (!isLogin) {
				setCartCount(0);
				return;
			}
			// 获取购物车数量
			const data = await getCartList({ useMock: false });
			setCartCount(data.data?.length ?? 0);
		})();
	}, [getCartList, isLogin, setCartCount]);

	usePageFocuEffect(getCount);

	const insets = useSafeAreaInsets();

	return (
		<>
			<View
				style={[
					styles.container,
					{ paddingTop: insets.top + SDK.fitSize(12) }
				]}
			>
				<View style={styles.header}>
					<View
						style={{
							width: SDK.fitSize(76)
						}}
					>
						<TouchableOpacity
							style={styles.avatar}
							accessibilityRole="button"
							onPress={() => {
								// 如果没有用户信息 未登录
								if (!isLogin) {
									if (SDK.isH5()) {
										goToLogin();
										return;
									}
									navigation.navigate('Login');
									return;
								}

								if (SDK.isH5()) {
									toggle();
									return;
								}

								if (!navigation?.openDrawer) {
									throw Error('navigation.openDrawer()');
								}
								navigation.openDrawer();
							}}
						>
							<Image
								style={styles.avatar}
								source={{
									uri:
										useDetail?.headerUrl ||
										'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/dirctAvatar.png'
								}}
							/>
						</TouchableOpacity>
					</View>
					<Image
						style={styles.logo}
						source={{
							uri: 'https://static-s.theckb.com/BusinessMarket/App/Icon/fbox.png'
						}}
					/>

					<View
						style={{
							flexDirection: 'row'
						}}
					>
						<View
							style={{
								marginRight: SDK.fitSize(12)
							}}
						>
							<Badge
								dot={Boolean(unreadCount)}
								iconStyle={{
									top: SDK.fitSize(5),
									right: SDK.fitSize(8),
									width: SDK.fitSize(8),
									height: SDK.fitSize(8)
								}}
							>
								<TouchableOpacity
									accessibilityRole="button"
									style={[styles.cartWrap]}
									onPress={() => {
										if (!isLogin) {
											if (SDK.isH5()) {
												goToLogin();
												return;
											}
											navigation.navigate('Login');
											return;
										}
										if (SDK.isH5()) {
											nav({
												path: '/user/message/center'
											});
											return;
										}
										navigationWebViewH5(
											'/user/message/center'
										);
									}}
								>
									<Image
										style={[styles.cart]}
										source={{
											uri: 'https://static-jp.theckb.com/static-asset/easy-app/bell@3.x.png'
										}}
									/>
								</TouchableOpacity>
							</Badge>
						</View>

						<Badge
							value={cartCount}
							iconStyle={{
								top: SDK.fitSize(4),
								right: SDK.fitSize(2)
							}}
							max={99}
						>
							<TouchableOpacity
								accessibilityRole="button"
								style={styles.cartWrap}
								onPress={() => {
									if (!isLogin) {
										if (SDK.isH5()) {
											goToLogin();
											return;
										}
										navigation.navigate('Login');
										return;
									}
									if (SDK.isH5()) {
										nav({
											path: '/order/shopcart'
										});
										return;
									}
									navigationWebViewH5('/order/shopcart');
								}}
							>
								<Image
									style={styles.cart}
									source={{
										uri: 'https://static-s.theckb.com/BusinessMarket/App/home/cart3625871.png'
									}}
								/>
							</TouchableOpacity>
						</Badge>
					</View>
				</View>
			</View>

			{SDK.isH5() && (
				<HomeSlider visible={homeSidebarVisible} toggle={toggle} />
			)}
		</>
	);
};
