/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 10:22:07
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  container: {
    marginBottom: px2dp(20)
  },

  header: {
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  avatar: {
    borderRadius: px2dp(50),
    width: px2dp(32),
    height: px2dp(32)
  },

  logo: {
    width: px2dp(87),
    height: px2dp(21)
  },

  cartWrap: {
    borderRadius: px2dp(50),
    width: px2dp(32),
    height: px2dp(32),
    backgroundColor: '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },

  cart: {
    width: px2dp(16),
    height: px2dp(16),
    display: 'flex'
  }
});