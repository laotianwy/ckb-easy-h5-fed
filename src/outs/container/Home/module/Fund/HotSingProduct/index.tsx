/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-03-21 16:13:49
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 21:25:31
 * @FilePath: /3fbox-page-mid/src/outs/container/Home/module/Fund/HotSingProduct/index.tsx
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	Animated,
	LayoutAnimation
} from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { useEffect, useRef, useState } from 'react';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { request } from '@/config/request/interceptors';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { tool } from '@/outs/utils/tool';
import { CollectModal } from '../components/CollectModal';
const { fitSize } = SDK;

interface HotProductProps {
	imageUrl?: string;
	name?: string;
}

interface HotSingProductProps {
	title?: string;
	list?: CollectionActivityRespDTO[];
	active?: boolean;
	id: number;
	refuse?: () => void;
}
const HotSingProduct = (props: HotSingProductProps) => {
	const { title, active, id, refuse } = props;

	const [activeId, setActiveId] = useState<number>(0);
	const [likeMaxWidth, setLikeMaxWidth] = useState(0);
	const [list, setList] = useState([]);
	const navigationWebViewH5 = useNavigationWebViewH5();
	const nav = SDK.useNav();
	useEffect(() => {
		setList(props.list);
	}, [props.list]);
	if (list?.length === 0) {
		return;
	}
	const refuseList = () => {
		setActiveId(0);
		refuse();
	};
	// 跳转到热门单品的列表
	const jumpPage = () => {
		SDK.TA.track({
			event: TaEvent.BTN_CLICK,
			properties: {
				position: 'self_define_module_more'
			}
		});
		const url = `/goods/hot?id=${id}`;
		if (SDK.isH5()) {
			nav({ path: url });
			return;
		}
		navigationWebViewH5(url);
	};

	// 跳转到特辑的详情
	const jumpDetail = (item: any) => {
		SDK.TA.track({
			event: TaEvent.BTN_CLICK,
			properties: {
				collection_name: item.moduleName,
				position: 'self_define_module'
			}
		});

		if (item.productCollection) {
			if (!item.singleProductCode) {
				return;
			}
			nav(
				{
					name: 'GoodDetail',
					path: `/goods/detail?productCode=${item.singleProductCode}`
				},
				{
					productCode: item.singleProductCode
				}
			);
		} else {
			const url = `/goods/collectionGoodsList?id=${item.id}`;
			if (SDK.isH5()) {
				nav({ path: url });
				return;
			}
			navigationWebViewH5(url);
		}
	};
	return (
		<View
			style={{
				width: fitSize(351),
				paddingVertical: fitSize(16),
				paddingHorizontal: fitSize(8),
				borderRadius: fitSize(8),
				overflow: 'hidden',
				backgroundColor: '#fff',
				marginTop: fitSize(8)
			}}
		>
			<TouchableOpacity
				onPress={jumpPage}
				style={{ flexDirection: 'row' }}
			>
				<Text
					style={{
						fontSize: fitSize(17),
						lineHeight: fitSize(22),
						fontWeight: '600',
						color: 'rgba(0, 0, 0, 0.88)'
					}}
				>
					{title}
				</Text>
				<Image
					source={{
						uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/right-arrow-spec.png'
					}}
					style={{
						width: SDK.fitSize(22),
						height: SDK.fitSize(22),
						marginLeft: SDK.fitSize(8)
					}}
				/>
			</TouchableOpacity>
			{/* 底部商品列表 */}
			<View
				style={{
					marginTop: fitSize(16),
					flexDirection: 'row',
					flexWrap: 'wrap'
				}}
			>
				{list.map((item, index) => {
					return (list.length - 1 === index &&
						list.length % 2 === 1) ||
						index > 3 ? (
						<></>
					) : (
						<View
							style={[
								{
									width: SDK.fitSize(163.5),
									height: SDK.fitSize(270),
									borderRadius: SDK.fitSize(8),
									overflow: 'hidden'
								},
								index === list.length - 1
									? {}
									: { marginBottom: SDK.fitSize(12) },
								index % 2 === 0
									? { marginRight: fitSize(7) }
									: {},
								index + 1 !== list.length
									? { marginBottom: fitSize(8) }
									: {}
							]}
							key={index}
						>
							<TouchableOpacity
								onPress={() => jumpDetail(item)}
								key={index}
								style={[
									{
										width: fitSize(163.5),
										position: 'relative'
									}
								]}
							>
								<Image
									source={{
										uri: item.homeImgUrl
									}}
									style={{
										width: SDK.fitSize(163),
										height: SDK.fitSize(240),
										borderRadius: SDK.fitSize(8),
										overflow: 'hidden'
									}}
								/>
								<View
									style={{
										marginTop: fitSize(8),
										width: '100%',
										height: fitSize(22),
										flexDirection: 'row',
										justifyContent: 'space-between',
										paddingRight: SDK.fitSize(6),
										alignItems: 'center'
									}}
								>
									<Text
										numberOfLines={1}
										style={{
											width: SDK.fitSize(144),
											fontSize: fitSize(14),
											lineHeight: fitSize(22),
											fontWeight: '500',
											color: 'rgba(0, 0, 0, 0.88)'
										}}
									>
										{item.moduleName}
									</Text>
									<TouchableOpacity
										onPress={() => {
											SDK.TA.track({
												event: TaEvent.BTN_CLICK,
												properties: {
													position:
														'self_define_module_operate'
												}
											});
											setActiveId(item.id);
										}}
									>
										<Image
											source={{
												uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/more_icon.png'
											}}
											resizeMethod="resize"
											style={{
												width: SDK.fitSize(16),
												height: SDK.fitSize(16)
											}}
										/>
									</TouchableOpacity>
								</View>
								<CollectModal
									show={item.id === activeId}
									setActiveId={setActiveId}
									width={163.5}
									height={270}
									refuse={refuseList}
									id={item.id}
									item={item}
								/>
							</TouchableOpacity>
						</View>
					);
				})}
			</View>
		</View>
	);
};

export default HotSingProduct;
