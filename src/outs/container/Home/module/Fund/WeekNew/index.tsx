/*
 * @Author: huajian
 * @Date: 2024-03-21 10:15:01
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-28 22:59:23
 * @Description:
 */
import { View, Image, Text, ScrollView, TouchableOpacity } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { IOScrollView, InView } from 'react-native-intersection-observer';
import { ProductKeywordRespDTO } from '@/outs/service/easyGoods';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { tool } from '@/outs/utils/tool';
import { Item } from './Item';
interface WEEK_PROP {
	data: ProductKeywordRespDTO[];
}
export const WeekNew = (props: WEEK_PROP) => {
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const { data } = props;
	if (data.length === 0) {
		return null;
	}
	return (
		<InView
			onChange={(inView) => {
				if (inView) {
					SDK.TA.track({
						event: TaEvent.PAGE_VIEW,
						properties: {
							position: 'week_new_product'
						}
					});
				}
			}}
		>
			<View
				style={{
					backgroundColor: '#fff',
					borderRadius: SDK.fitSize(8),
					paddingHorizontal: SDK.fitSize(8),
					paddingVertical: SDK.fitSize(16)
				}}
			>
				<TouchableOpacity
					onPress={() => {
						SDK.TA.track({
							event: TaEvent.BTN_CLICK,
							properties: {
								position: 'week_new_product_more'
							}
						});
						// TODO跳转新品的活动列表
						const url = `/goods/list?keyword=&originSearch=false&noInput=true&pageTitle=${encodeURIComponent(
							tool.strings('本周新到商品')
						)}&showPrice=true&newProductSort=true`;
						if (SDK.isH5()) {
							nav({ path: url });
							return;
						}
						navigationWebViewH5(url);
					}}
				>
					<View
						style={{ flexDirection: 'row', alignItems: 'center' }}
					>
						<View
							style={{
								backgroundColor: 'rgba(0, 0, 0, 0.88)',
								borderRadius: SDK.fitSize(40),
								height: SDK.fitSize(22),
								alignItems: 'center',
								paddingHorizontal: SDK.fitSize(4),
								display: 'flex',
								justifyContent: 'center'
							}}
						>
							<Text
								style={{
									fontSize: SDK.fitSize(12),
									color: '#fff'
								}}
							>
								NEW
							</Text>
						</View>
						<Text
							style={{
								paddingHorizontal: SDK.fitSize(8),
								fontSize: SDK.fitSize(17),
								fontWeight: '600',
								color: 'rgba(0, 0, 0, 0.88)'
							}}
						>
							{tool.strings('本周新到商品')}
						</Text>
						<Image
							width={SDK.fitSize(22)}
							height={SDK.fitSize(22)}
							source={{
								uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/arrow.png'
							}}
							style={{
								width: SDK.fitSize(22),
								height: SDK.fitSize(22)
							}}
						/>
					</View>
					<View
						style={{
							flexDirection: 'row',
							marginTop: SDK.fitSize(16)
						}}
					>
						{/* 10条商品数据 */}
						<IOScrollView
							style={{ flexDirection: 'row' }}
							showsHorizontalScrollIndicator={false}
							horizontal={true}
						>
							{data.map((item, index) => {
								return (
									<InView
										onChange={(inview) => {
											if (inview) {
												SDK.TA.track({
													event: TaEvent.PAGE_VIEW,
													properties: {
														position:
															item.productCode
													}
												});
											}
										}}
										key={item.productCode}
									>
										<Item index={index} item={item} />
									</InView>
								);
							})}
						</IOScrollView>
					</View>
				</TouchableOpacity>
			</View>
		</InView>
	);
};
