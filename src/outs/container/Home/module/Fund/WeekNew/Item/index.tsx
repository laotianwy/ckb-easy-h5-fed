/*
 * @Author: huajian
 * @Date: 2024-03-21 11:30:21
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-28 20:38:37
 * @Description:
 */
import { View, Image, Text, ScrollView, TouchableOpacity } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { tool } from '@/outs/utils/tool';
import { ProductKeywordRespDTO } from '@/outs/service/easyGoods';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { TaEvent } from '@/outs/utils/TDAnalytics';
interface ITEM_PROPS {
	item: ProductKeywordRespDTO;
	index: number;
}
export const Item = (props: ITEM_PROPS) => {
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const { item, index } = props;
	return (
		<TouchableOpacity
			onPress={() => {
				SDK.TA.track({
					event: TaEvent.PRODUCT_CLICK,
					properties: {
						product_code: item.productCode,
						product_title_ja: item.productTitle,
						position: 'week_new_product'
					}
				});
				if (SDK.isH5()) {
					nav({
						path: `/goods/detail?productCode=${item.productCode}`
					});
					return;
				}
				nav(
					{
						name: 'GoodDetail',
						path: `/goods/detail?productCode=${item.productCode}`
					},
					{ productCode: item.productCode }
				);
			}}
		>
			<View
				style={[
					{ width: SDK.fitSize(120) },
					index === 0 ? {} : { marginLeft: SDK.fitSize(8) }
				]}
			>
				<View
					style={{
						width: SDK.fitSize(120),
						height: SDK.fitSize(120)
					}}
				>
					<Image
						style={{
							width: '100%',
							height: '100%',
							borderRadius: SDK.fitSize(8),
							overflow: 'hidden'
						}}
						source={{
							uri: item.productMainImg
						}}
					/>
				</View>
				<View
					style={{
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'center'
					}}
				>
					{item.productSellPriceJa && item.activityPriceJa && (
						<>
							<Text
								style={{
									fontSize: SDK.fitSize(12),
									lineHeight: SDK.fitSize(16),
									fontWeight: '500',
									textAlign: 'center',
									color: 'rgba(28, 32, 38, 1)',
									marginTop: SDK.fitSize(8),
									marginRight: SDK.fitSize(8)
								}}
							>
								{item.activityPriceJa}
								{tool.strings('円')}
							</Text>
							{item.highPriceShow &&
								item.activityPriceJa <
									item.productSellPriceJa && (
									<Text
										style={{
											fontSize: SDK.fitSize(12),
											lineHeight: SDK.fitSize(16),
											fontWeight: '500',
											textAlign: 'center',
											color: 'rgba(0, 0, 0, 0.25)',
											textDecorationLine: 'line-through',
											marginTop: SDK.fitSize(8)
										}}
									>
										{item.productSellPriceJa}
										{tool.strings('円')}
									</Text>
								)}
						</>
					)}
				</View>
			</View>
		</TouchableOpacity>
	);
};
