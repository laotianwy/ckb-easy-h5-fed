/*
 * @Author: huajian
 * @Date: 2024-03-27 11:11:33
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-28 11:55:19
 * @Description:
 */
import { useContext, useState } from 'react';
import * as SDK from '@snifftest/sdk/lib/rn';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { tool } from '@/outs/utils/tool';
import { ContextPreView } from '@/outs/container/Home/context';
import { request } from '@/config/request/interceptors';
interface COLLECT_MODAL_PROPS {
	show: boolean;
	refuse: (isDislike?: boolean) => void;
	id: number;
	setActiveId: (num: number) => void;
	width: number;
	height: number;
	item: any;
}
export const CollectModal = (props: COLLECT_MODAL_PROPS) => {
	const { toast } = useContext(ContextPreView);
	const { show, refuse, id, setActiveId, width, height, item } = props;
	const [likeMaxWidth, setLikeMaxWidth] = useState(0);
	const message = (str: string) => {
		if (SDK.isH5()) {
			console.log('收藏成功-h5');
			toast(str);
		} else {
			console.log('收藏成功-app');
			tool.Toast.message(str);
		}
	};
	const collect = async () => {
		try {
			if (item.collectedFlag === 1) {
				await request.easyMarket.collection.removeCollectionList([
					Number(id)
				]);
				message(tool.strings('取消收藏成功'));
			} else {
				await request.easyMarket.collection.addActivityAreaById({
					id: Number(id)
				});
				message(tool.strings('收藏成功'));
			}
			refuse();
		} catch (error) {
			console.log('收藏失败！', error);
			return false;
		}
	};
	const removeSelf = async () => {
		try {
			await request.easyMarket.collection.dislikeActivityAreaById({
				id: Number(id)
			});
			message(tool.strings('操作成功'));
			refuse(true);
		} catch (error) {
			console.log('不喜欢失败！', error);
			return false;
		}
	};
	if (!show) return;
	return (
		<TouchableOpacity
			onPress={() => {
				setActiveId(0);
			}}
			style={{
				position: 'absolute',
				width: SDK.fitSize(width),
				height: SDK.fitSize(height),
				alignItems: 'center',
				justifyContent: 'center',
				borderRadius: SDK.fitSize(8)
			}}
		>
			<View
				style={{
					position: 'absolute',
					backgroundColor: 'rgba(0, 0, 0, 0.2)',
					width: SDK.fitSize(width),
					height: SDK.fitSize(height),
					alignItems: 'center',
					justifyContent: 'center',
					borderRadius: SDK.fitSize(8)
				}}
			>
				<TouchableOpacity
					style={{
						width: likeMaxWidth,
						backgroundColor: '#fff',
						paddingVertical: SDK.fitSize(8),
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'center',
						borderRadius: SDK.fitSize(4),
						overflow: 'hidden',
						marginBottom: SDK.fitSize(8)
					}}
					onPress={collect}
				>
					<View
						style={{
							flexDirection: 'row',
							alignItems: 'center'
						}}
					>
						<Image
							width={SDK.fitSize(20)}
							height={SDK.fitSize(20)}
							source={{
								uri: `https://static-jp.theckb.com/static-asset/easy-app/icon/like_icon${
									item.collectedFlag === 1 ? '' : '_un'
								}.png`
							}}
							style={{
								width: SDK.fitSize(20),
								height: SDK.fitSize(20)
							}}
						/>
						<Text
							style={{
								marginLeft: SDK.fitSize(4),
								fontSize: SDK.fitSize(12),
								fontWeight: '400',
								color: 'rgba(0, 0, 0, 1)'
							}}
						>
							{tool.strings('收藏该特集')}
						</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={removeSelf}
					key="不喜欢该特集"
					onLayout={(e) => {
						const { x, y, width, height } = e.nativeEvent.layout;
						setLikeMaxWidth(width);
					}}
					style={{
						backgroundColor: '#fff',
						paddingHorizontal: SDK.fitSize(14),
						paddingVertical: SDK.fitSize(8),
						flexDirection: 'row',
						alignItems: 'center',
						borderRadius: SDK.fitSize(4)
					}}
				>
					<Image
						width={SDK.fitSize(20)}
						height={SDK.fitSize(20)}
						source={{
							uri: `https://static-jp.theckb.com/static-asset/easy-app/icon/unlike_icon${
								item.dislikeFlag === 1 ? '' : '_un'
							}.png`
						}}
						style={{
							width: SDK.fitSize(20),
							height: SDK.fitSize(20)
						}}
					/>
					<Text
						style={{
							marginLeft: SDK.fitSize(4),
							fontSize: SDK.fitSize(12),
							fontWeight: '400',
							color: 'rgba(0, 0, 0, 1)'
						}}
					>
						{tool.strings('不喜欢该特集')}
					</Text>
				</TouchableOpacity>
			</View>
		</TouchableOpacity>
	);
};
