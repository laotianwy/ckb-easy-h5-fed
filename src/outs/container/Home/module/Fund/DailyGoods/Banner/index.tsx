/*
 * @Author: huajian
 * @Date: 2024-03-21 14:53:45
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-04-01 19:08:00
 * @Description:
 */
import { View, ScrollView, Text, Image, TouchableOpacity } from 'react-native';

import Swiper from '@nutui/nutui-react-native/components/swiper';
import SwiperItem from '@nutui/nutui-react-native/components/swiperitem';
import * as SDK from '@snifftest/sdk/lib/rn';
import { forwardRef, useEffect, useImperativeHandle, useRef } from 'react';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { TaEvent } from '@/outs/utils/TDAnalytics';
interface BANNER_PROPS {
	data: CollectionActivityRespDTO[];
	activeIndex: number;
	setActiveIndex: (index: number) => void;
}
export const Banner = forwardRef((props: BANNER_PROPS, wrapperRef: any) => {
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const { data, setActiveIndex } = props;
	const swiperRef = useRef<any>(null);
	const list = [
		'客厅',
		'厨房',
		'厨房',
		'厨房',
		'厨房',
		'厨房',
		'厨房',
		'厨房',
		'厨房'
	];

	useImperativeHandle(wrapperRef, () => ({
		changeIndex: (num: number) => {
			swiperRef.current?.to(num);
		}
	}));
	return (
		<View style={{ width: '100%', marginTop: SDK.fitSize(16) }}>
			<Swiper
				ref={swiperRef}
				/** @ts-ignore */
				width="100%"
				height={SDK.fitSize(120)}
				autoPlay={0}
				defaultValue={props.activeIndex}
				onChange={(num) => {
					setActiveIndex(num);
				}}
			>
				{data?.map((item, index) => {
					return (
						<SwiperItem key={index}>
							<TouchableOpacity
								onPress={() => {
									SDK.TA.track({
										event: TaEvent.BTN_CLICK,
										properties: {
											position: 'life_articles',
											collection_name: item.moduleName
										}
									});
									if (item.productCollection) {
										if (!item.singleProductCode) {
											return;
										}
										nav(
											{
												name: 'GoodDetail',
												path: `/goods/detail?productCode=${item.singleProductCode}`
											},
											{
												productCode:
													item.singleProductCode
											}
										);
									} else {
										const url = `/goods/collectionGoodsList?id=${item.id}`;
										if (SDK.isH5()) {
											nav({ path: url });
											return;
										}
										navigationWebViewH5(url);
									}
								}}
							>
								<View
									style={{
										width: '100%',
										paddingHorizontal: SDK.fitSize(6)
									}}
								>
									<Image
										key={index}
										source={{
											uri: item.homeImgUrl
										}}
										style={{
											width: '100%',
											height: SDK.fitSize(120),
											borderRadius: SDK.fitSize(8)
										}}
									/>
								</View>
							</TouchableOpacity>
						</SwiperItem>
					);
				})}
			</Swiper>
		</View>
		// <ScrollView
		// 	style={{ flexDirection: 'row', marginTop: SDK.fitSize(16) }}
		// 	horizontal={true}
		// 	showsHorizontalScrollIndicator={false}
		// >
		// 	{list.map((item, index) => {
		// 		return (
		// 			<Image
		// 				key={index}
		// 				source={{
		// 					uri: 'https://theckbstest-oss.theckbs.com/addition/hukpuaf.png'
		// 				}}
		// 				style={{
		// 					width: SDK.fitSize(328),
		// 					height: SDK.fitSize(120),
		// 					borderRadius: SDK.fitSize(8),
		// 					marginRight: SDK.fitSize(8)
		// 				}}
		// 			/>
		// 		);
		// 	})}
		// </ScrollView>
	);
});
