/*
 * @Author: huajian
 * @Date: 2024-03-21 11:30:21
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 00:56:01
 * @Description:
 */
import { View, Image, Text, ScrollView, TouchableOpacity } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import {
	ProductInfo,
	ProductVO
} from '@snifftest/coupon/lib/service/easyMarket';
import { tool } from '@/outs/utils/tool';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
interface ITEM_PROPS {
	item: ProductVO;
}
export const Item = (props: ITEM_PROPS) => {
	const { item } = props;
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	return (
		<TouchableOpacity
			onPress={() => {
				SDK.TA.track({
					event: TaEvent.PRODUCT_CLICK,
					properties: {
						product_code: item.productCode,
						product_title_ja: item.productTitle,
						position: 'life_articles'
					}
				});
				nav(
					{
						name: 'GoodDetail',
						path: `/goods/detail?productCode=${item.productCode}`
					},
					{ productCode: item.productCode }
				);
			}}
			style={{ width: SDK.fitSize(104), marginRight: SDK.fitSize(8) }}
		>
			<View
				style={{
					width: SDK.fitSize(104),
					marginRight: SDK.fitSize(8),
					marginTop: SDK.fitSize(8)
				}}
			>
				<Image
					source={{
						uri: item.productMainImg
					}}
					style={{
						width: SDK.fitSize(104),
						height: SDK.fitSize(104),
						borderRadius: SDK.fitSize(8),
						overflow: 'hidden'
					}}
				/>
				<Text
					style={{
						textAlign: 'center',
						paddingTop: SDK.fitSize(8),
						color: '#000',
						fontSize: SDK.fitSize(12),
						fontWeight: '600',
						lineHeight: SDK.fitSize(16),
						paddingBottom: SDK.fitSize(8)
					}}
				>
					{item.activitySellPriceJpy ?? item.productLowestPrice}
					{tool.strings('円')}
				</Text>
			</View>
		</TouchableOpacity>
	);
};
