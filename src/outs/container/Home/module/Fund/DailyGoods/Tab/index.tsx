/*
 * @Author: huajian
 * @Date: 2024-03-21 14:53:45
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-27 11:40:49
 * @Description:
 */
import {
	View,
	ScrollView,
	Text,
	NativeModules,
	TouchableOpacity
} from 'react-native';

import * as SDK from '@snifftest/sdk/lib/rn';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
interface TAB_PROPS {
	data: CollectionActivityRespDTO[];
	activeIndex: number;
	setActiveIndex: (v: number) => void;
	handleChangeSwiper: (index: number) => void;
}
export const Tab = (props: TAB_PROPS) => {
	const { data, activeIndex, setActiveIndex, handleChangeSwiper } = props;
	return (
		<ScrollView
			style={{ flexDirection: 'row' }}
			horizontal={true}
			showsHorizontalScrollIndicator={false}
		>
			{data.map((item, index) => {
				return (
					<TouchableOpacity
						onPress={() => {
							setActiveIndex(index);
							handleChangeSwiper(index);
						}}
						key={index}
					>
						<Text
							style={{
								height: SDK.fitSize(34),
								paddingHorizontal: SDK.fitSize(12),
								backgroundColor:
									activeIndex === index ? '#000' : '#FAFAFA',
								borderRadius: SDK.fitSize(16),
								color:
									activeIndex === index
										? '#fff'
										: 'rgba(0, 0, 0, 0.88)',
								lineHeight: SDK.fitSize(34),
								fontWeight: '600',
								marginRight: SDK.fitSize(21),
								overflow: 'hidden'
							}}
						>
							{item.moduleName}
						</Text>
					</TouchableOpacity>
				);
			})}
		</ScrollView>
	);
};
