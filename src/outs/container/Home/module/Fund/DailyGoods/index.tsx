/*
 * @Author: huajian
 * @Date: 2024-03-21 10:15:01
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 22:15:57
 * @Description:
 */
import { View, Image, Text, ScrollView, TouchableOpacity } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { useEffect, useRef, useState } from 'react';
import { InView } from 'react-native-intersection-observer';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { HOME_ENUM } from '@/outs/const';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { Item } from './Item';
import { Tab } from './Tab';
import { Banner } from './Banner';
interface DAILY_PROPS {
	data: CollectionActivityRespDTO[];
}
export const DailyGoods = (props: DAILY_PROPS) => {
	const { data } = props;
	const [activeIndex, setActiveIndex] = useState<number>(0);
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();

	const bannerRef = useRef(null);

	const handleChangeSwiper = (index: number) => {
		bannerRef.current?.changeIndex?.(index);
	};

	if (data.length === 0) {
		return null;
	}
	return (
		<InView
			onChange={(inView) => {
				if (inView) {
					SDK.TA.track({
						event: TaEvent.PAGE_VIEW,
						properties: {
							position: 'life_articles'
						}
					});
				}
			}}
		>
			<View
				style={{
					backgroundColor: '#fff',
					borderRadius: SDK.fitSize(8),
					paddingHorizontal: SDK.fitSize(8),
					paddingVertical: SDK.fitSize(16),
					marginTop: SDK.fitSize(12)
				}}
			>
				<View
					style={{
						flexDirection: 'row',
						alignItems: 'center',
						marginBottom: SDK.fitSize(16)
					}}
				>
					<TouchableOpacity
						onPress={() => {
							SDK.TA.track({
								event: TaEvent.BTN_CLICK,
								properties: {
									position: 'life_articles_more'
								}
							});
							const url = `/goods/collection?one=${
								HOME_ENUM.LIVE_PRODUCT
							}&title=${encodeURIComponent(
								data?.[0]?.collectionClassifyName
							)}`;
							if (SDK.isH5()) {
								nav({ path: url });
								return;
							}
							navigationWebViewH5(url);
						}}
						style={{ flexDirection: 'row' }}
					>
						<Text
							style={{
								fontSize: SDK.fitSize(17),
								fontWeight: '600',
								color: 'rgba(0, 0, 0, 0.88)'
							}}
						>
							{data?.[0]?.collectionClassifyName}
						</Text>
						<Image
							width={SDK.fitSize(22)}
							height={SDK.fitSize(22)}
							style={{
								width: SDK.fitSize(22),
								height: SDK.fitSize(22),
								marginLeft: SDK.fitSize(8)
							}}
							source={{
								uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/arrow.png'
							}}
						/>
					</TouchableOpacity>
				</View>
				{/* 最多10个 */}
				<Tab
					data={data}
					activeIndex={activeIndex}
					setActiveIndex={setActiveIndex}
					handleChangeSwiper={handleChangeSwiper}
				/>
				<View
					style={{
						width: '100%'
					}}
				>
					<Banner
						ref={bannerRef}
						data={data}
						setActiveIndex={setActiveIndex}
						activeIndex={activeIndex}
					/>
				</View>

				{data?.[activeIndex]?.productCollection !== 1 && (
					<View style={{ flexDirection: 'row' }}>
						{data[activeIndex]?.homePageProductsInfoList?.map(
							(item, index) => {
								return index < 3 ? (
									<Item key={item.productCode} item={item} />
								) : (
									<></>
								);
							}
						)}
					</View>
				)}
			</View>
		</InView>
	);
};
