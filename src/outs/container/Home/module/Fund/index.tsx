/*
 * @Author: huajian
 * @Date: 2024-03-21 10:09:42
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 22:15:21
 * @Description:
 */
import {
	View,
	ScrollView,
	Text,
	NativeModules,
	RefreshControl
} from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import React, {
	forwardRef,
	useImperativeHandle,
	useRef,
	useEffect,
	useState
} from 'react';
import { IOScrollView, InView } from 'react-native-intersection-observer';
import { request } from '@/config/request/interceptors';
import {
	CollectionActivityRespDTO,
	CollectionClassifylMergeRespDTO
} from '@/outs/service/easyMarket';
import { ProductKeywordRespDTO } from '@/outs/service/easyGoods';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { BARCODE } from '@/outs/const';
import BackToTop from '@/outs/components/BackToTop';
import { defHeight } from '@/outs/utils/tool/utils';
import SpecialView from '../SpecialView';
import { NAVIGATION_HEIGHT, useGetNavigationBottom } from '../Navigation';
import { WeekNew } from './WeekNew/index';
import { SiteBuy } from './SiteBuy/index';
import HotSingProduct from './HotSingProduct';

import { DailyGoods } from './DailyGoods/index';
interface FundProps {
	H5HotProductComponent: React.FC<{ data: any; title: string; id: number }>;
	H5SiteBuyComponent: React.FC<{ data: any }>;
	/** 是否登陆 */
	isLogin: boolean;
	/** tabbar的当前code */
	barCode: string;
	goLogin: () => void;
	useDetail?: any;
}

export const Fund = forwardRef((props: FundProps, wrapperRef: any) => {
	const {
		H5SiteBuyComponent,
		isLogin,
		barCode,
		H5HotProductComponent,
		useDetail
	} = props;

	const createHeight = useDetail?.customerId ? 0 : 55;
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const scrollViewRef = useRef(null);
	const [refreshing, setRefreshing] = useState(false);
	const bottomPx = useGetNavigationBottom();
	const [scrollHeight, setScrollHeight] = useState(0);
	useImperativeHandle(
		wrapperRef,
		() => ({
			scrollTop: () => {
				scrollViewRef.current?.scrollTo?.({
					offset: 0,
					animated: true
				});
			},
			getNewData: () => {
				// 执行刷新数据逻辑
				init();
			}
		}),
		[]
	);
	const [weekNewData, setWeekNewData] = useState<ProductKeywordRespDTO[]>([]);
	const [weekDataIndex, setWeekDataIndex] = useState(0);
	const [siteBuyData, setSiteBuyData] = useState<CollectionActivityRespDTO[]>(
		[]
	);
	// 生活用品合集
	const [dailyGoods, setDailyGoods] = useState<CollectionActivityRespDTO[]>(
		[]
	);
	// 热门单品
	const [hotProduct, setHotProduct] = useState<
		CollectionClassifylMergeRespDTO[]
	>([]);
	// 百元特辑
	const [hundredData, setHundredData] = useState<CollectionActivityRespDTO[]>(
		[]
	);
	const init = async () => {
		const { data } =
			await request.easyMarket.collection.homePageModulesList();

		// 生活数据过滤十条
		const newDailyLifeProductList = (
			data?.dailyLifeProductList ?? []
		).filter((item, index) => index <= 9);
		setSiteBuyData(data.oneStopProductList);
		setDailyGoods(newDailyLifeProductList);
		await getHundredData();

		await gethotData();
		const { data: weekData } =
			await request.easyGoods.product.searchNewProduct({});
		setWeekNewData(weekData);
		setWeekDataIndex((weekDataIndex) => weekDataIndex + 1);
		setRefreshing(false);
	};
	const gethotData = async () => {
		// 获取其他特辑的数据
		const res =
			await request.easyMarket.collection.qrySelfDefinedModulesList();
		setHotProduct(res.data);
	};
	const getHundredData = async () => {
		const { data: hundred } =
			await request.easyMarket.collection.qryHundredModulesList({
				barCode: BARCODE[barCode] ?? ''
			});
		setHundredData(hundred);
	};
	useEffect(() => {
		init();
	}, []);

	const onRefresh = () => {
		setRefreshing(true);
		init();
	};

	return (
		<>
			<BackToTop
				visible={scrollHeight > defHeight()}
				scrollView={scrollViewRef}
				bottom={
					useDetail?.customerId ? SDK.fitSize(100) : SDK.fitSize(130)
				}
			/>
			<IOScrollView
				ref={scrollViewRef}
				style={{
					flex: SDK.isH5() ? undefined : 1,
					height: SDK.isH5()
						? SDK.getDeviceSize().height - 165
						: undefined
				}}
				refreshControl={
					<RefreshControl
						refreshing={refreshing}
						onRefresh={onRefresh}
					/>
				}
				onScroll={(event) => {
					const { contentOffset } = event.nativeEvent;
					// 大于0代表滚动了
					// const value = contentOffset.y > 0 ? true : false;
					setScrollHeight(contentOffset.y);
				}}
				showsVerticalScrollIndicator={false}
			>
				{/* 每周新品 */}
				<WeekNew data={weekNewData} key={weekDataIndex} />
				{/* 一站式购物 */}
				{SDK.isH5() ? (
					<InView
						onChange={(inView) => {
							if (inView) {
								SDK.TA.track({
									event: TaEvent.PAGE_VIEW,
									properties: {
										position: 'one_step_shopping'
									}
								});
							}
						}}
					>
						<H5SiteBuyComponent data={siteBuyData} />
					</InView>
				) : (
					<InView
						onChange={(inView) => {
							if (inView) {
								SDK.TA.track({
									event: TaEvent.PAGE_VIEW,
									properties: {
										position: 'one_step_shopping'
									}
								});
							}
						}}
					>
						<SiteBuy isLogin={isLogin} data={siteBuyData} />
					</InView>
				)}
				{/* 生活用品合集 */}
				<DailyGoods data={dailyGoods} />
				{/* 热门单品-自定义特辑 需要循环来获取数据 */}
				{SDK.isH5()
					? hotProduct
							.sort((a, b) => a.sortNum - b.sortNum)
							.map((item, index) => {
								return item.activityList?.length > 1 ? (
									<InView
										key={item.collectionClassifyName}
										onChange={(inView) => {
											if (inView) {
												SDK.TA.track({
													event: TaEvent.PAGE_VIEW,
													properties: {
														position:
															'self_define_module'
													}
												});
											}
										}}
									>
										<H5HotProductComponent
											data={item.activityList}
											title={item.collectionClassifyName}
											id={item.id}
											key={index}
										/>
									</InView>
								) : (
									<></>
								);
							})
					: hotProduct
							.sort((a, b) => a.sortNum - b.sortNum)
							.map((item) => {
								return item.activityList?.length > 1 ? (
									<InView
										key={item.collectionClassifyName}
										onChange={(inView) => {
											if (inView) {
												SDK.TA.track({
													event: TaEvent.PAGE_VIEW,
													properties: {
														position:
															'self_define_module'
													}
												});
											}
										}}
									>
										<HotSingProduct
											key={item.collectionClassifyName}
											title={item.collectionClassifyName}
											list={item.activityList}
											id={item.id}
											refuse={gethotData}
										/>
									</InView>
								) : (
									<></>
								);
							})}
				{/* 百元特辑 */}
				<SpecialView
					list={hundredData || []}
					refuse={getHundredData}
					barCode={barCode}
					titleClick={(title: string) => {
						const url = `/goods/priceCollectionList?barCode=${barCode}&title=${encodeURIComponent(
							title
						)}`;
						if (SDK.isH5()) {
							nav({ path: url });
							return;
						}
						navigationWebViewH5(url);
					}}
				/>
				{SDK.isH5() ? null : (
					<View
						style={{
							height:
								SDK.fitSize(NAVIGATION_HEIGHT + createHeight) +
								bottomPx
						}}
					/>
				)}
			</IOScrollView>
		</>
	);
});
