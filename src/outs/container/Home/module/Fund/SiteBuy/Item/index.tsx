/*
 * @Author: huajian
 * @Date: 2024-03-21 11:30:21
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-27 23:35:23
 * @Description:
 */
import {
	View,
	Image,
	Text,
	Animated,
	Dimensions,
	Pressable,
	TouchableOpacity
} from 'react-native';
import { useRef } from 'react';
import * as SDK from '@snifftest/sdk/lib/rn';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { CollectModal } from '../../components/CollectModal';
interface RenderItemProp {
	active: boolean;
	removeCallback: (item: CollectionActivityRespDTO) => void;
	isLast?: boolean;
	item: CollectionActivityRespDTO;
	setActiveId: (id: number) => void;
	isLogin?: boolean;
	changeCollect: (item: CollectionActivityRespDTO) => void;
}

export const Item = (props: RenderItemProp) => {
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const { active, removeCallback, isLast, setActiveId, item, changeCollect } =
		props;
	const opacityRef = useRef(new Animated.Value(1)).current;
	const positionRef = useRef(new Animated.ValueXY()).current;
	const removeSelf = async (isDislike?: boolean) => {
		if (isDislike) {
			Animated.parallel([
				Animated.timing(positionRef, {
					duration: 500,
					toValue: {
						x: 0,
						y: 0
					},
					useNativeDriver: false
				})
			]);
			removeCallback({
				...item,
				dislikeFlag: 1
			});
		} else {
			changeCollect(item);
		}
		setActiveId(0);
	};
	return (
		<Animated.View
			style={[
				{
					width: SDK.fitSize(335),
					height: SDK.fitSize(100),
					borderRadius: SDK.fitSize(8),
					overflow: 'hidden'
				},
				isLast ? {} : { marginBottom: SDK.fitSize(12) },
				{ ...positionRef.getLayout(), opacity: opacityRef }
			]}
		>
			<View
				style={{
					position: 'relative',
					width: SDK.fitSize(335),
					height: SDK.fitSize(100)
				}}
			>
				<TouchableOpacity
					onPress={() => {
						SDK.TA.track({
							event: TaEvent.BTN_CLICK,
							properties: {
								position: 'one_step_shopping',
								collection_name: item.moduleName
							}
						});
						// 一站式购物车item。如果是productCollection = true，那么跳转到商品详情。否则跳转到特辑列表
						if (item.productCollection) {
							if (!item.singleProductCode) {
								return;
							}
							nav(
								{
									name: 'GoodDetail',
									path: `/goods/detail?productCode=${item.singleProductCode}`
								},
								{ productCode: item.singleProductCode }
							);
						} else {
							const url = `/goods/collectionGoodsList?id=${item.id}`;
							if (SDK.isH5()) {
								nav({ path: url });
								return;
							}
							navigationWebViewH5(url);
						}
					}}
					style={{
						width: '100%',
						height: '100%'
					}}
				>
					<Image
						style={{
							width: '100%',
							height: '100%'
						}}
						source={{
							uri: item?.homeImgUrl
						}}
					/>
				</TouchableOpacity>
				<Pressable
					hitSlop={20}
					style={{
						width: SDK.fitSize(16),
						height: SDK.fitSize(16),
						position: 'absolute',
						right: SDK.fitSize(8),
						bottom: SDK.fitSize(12)
					}}
					onPress={(e) => {
						e.stopPropagation();
						SDK.TA.track({
							event: TaEvent.BTN_CLICK,
							properties: {
								position: 'one_step_shopping_operate'
							}
						});
						setActiveId(item.id);
					}}
				>
					<Image
						style={{
							width: SDK.fitSize(16),
							height: SDK.fitSize(16)
						}}
						source={{
							uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/more_icon.png'
						}}
					/>
				</Pressable>
				<CollectModal
					show={active}
					refuse={removeSelf}
					setActiveId={setActiveId}
					id={item.id}
					width={335}
					height={100}
					item={item}
				/>
			</View>
		</Animated.View>
	);
};
