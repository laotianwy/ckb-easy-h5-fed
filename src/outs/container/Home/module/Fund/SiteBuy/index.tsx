/*
 * @Author: huajian
 * @Date: 2024-03-21 10:15:01
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-28 11:51:31
 * @Description:
 */
import {
	View,
	Image,
	Text,
	ScrollView,
	LayoutAnimation,
	TouchableOpacity
} from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';

import { useEffect, useState } from 'react';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { HOME_ENUM } from '@/outs/const';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { Item } from './Item';
interface SITE_BUY_PROPS {
	data: CollectionActivityRespDTO[];
	isLogin: boolean;
}
export const SiteBuy = (props: SITE_BUY_PROPS) => {
	const { data, isLogin } = props;
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const [list, setList] = useState<CollectionActivityRespDTO[]>([]);
	const [activeId, setActiveId] = useState<number>(0);
	// 子组件动画完成回调
	const removeCallback = (targetItem: CollectionActivityRespDTO) => {
		LayoutAnimation.configureNext({
			duration: 400,
			update: {
				duration: 400,
				type: LayoutAnimation.Types.easeInEaseOut,
				property: 'scaleXY'
			}
		});
		// 更新数据源
		const newList = list.filter((item) => item.id !== targetItem.id);
		const resultList = [...newList, targetItem];
		setList(resultList);
	};
	// 收藏修改数据
	const collectList = (item: CollectionActivityRespDTO) => {
		const newlist = list.map((item) => {
			return {
				...item,
				collectedFlag: item.collectedFlag === 1 ? 0 : 1
			};
		});
		setList(newlist);
	};
	useEffect(() => {
		setList(props.data);
	}, [props.data]);
	if (!list || list?.length === 0) {
		return;
	}
	return (
		<View
			style={{
				backgroundColor: '#fff',
				borderRadius: SDK.fitSize(8),
				paddingHorizontal: SDK.fitSize(8),
				paddingVertical: SDK.fitSize(16),
				marginTop: SDK.fitSize(12)
			}}
		>
			<View
				style={{
					flexDirection: 'row',
					alignItems: 'center',
					marginBottom: SDK.fitSize(16)
				}}
			>
				<TouchableOpacity
					onPress={() => {
						SDK.TA.track({
							event: TaEvent.BTN_CLICK,
							properties: {
								position: 'one_step_shopping_more'
							}
						});
						const url = `/goods/collection?one=${
							HOME_ENUM.ONE_PRODUCT
						}&title=${encodeURIComponent(
							list?.[0]?.collectionClassifyName
						)}`;
						if (SDK.isH5()) {
							nav({ path: url });
							return;
						}
						navigationWebViewH5(url);
					}}
					style={{ flexDirection: 'row' }}
				>
					<Text
						style={{
							paddingHorizontal: SDK.fitSize(8),
							fontSize: SDK.fitSize(17),
							fontWeight: '600',
							color: 'rgba(0, 0, 0, 0.88)'
						}}
					>
						{list?.[0]?.collectionClassifyName}
					</Text>
					<Image
						width={SDK.fitSize(22)}
						height={SDK.fitSize(22)}
						source={{
							uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon/arrow.png'
						}}
					/>
				</TouchableOpacity>
			</View>
			<View
				style={{
					height: SDK.fitSize(
						112 * (list.length < 4 ? list.length : 4) - 12
					),
					overflow: 'hidden'
				}}
			>
				{/* 最多4个特辑 */}
				{list?.map((item, index) => {
					const isLast = index + 1 === data.length;
					// 点击跳转列表或者商品详情
					return (
						<Item
							item={item}
							key={item.id}
							removeCallback={removeCallback}
							isLogin={isLogin}
							active={activeId === item.id}
							isLast={isLast}
							setActiveId={setActiveId}
							changeCollect={collectList}
						/>
					);
				})}
			</View>
		</View>
	);
};
