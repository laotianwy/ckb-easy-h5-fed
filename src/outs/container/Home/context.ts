/*
 * @Author: yusha
 * @Date: 2024-01-08 15:56:42
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-27 14:35:19
 * @Description:
 */
import { createContext, Dispatch, SetStateAction } from 'react';
import { ProductDetailRespDTO } from '@/outs/service/easyGoods';
export type SpuProps = Record<string, number>;
interface ContextParams {
	toast?: any;
}
export const ContextPreView = createContext<ContextParams>({
	toast: undefined
});
