/*
 * @Author: huajian
 * @Date: 2023-12-04 13:43:40
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-05-08 10:02:41
 * @Description: 直采首页
 */
import React, { useCallback, useEffect, useState, useRef } from 'react';
import { View, SafeAreaView, Text, NativeModules } from 'react-native';
import { PrimitiveAtom, useAtom, useAtomValue } from 'jotai';
import { InView } from 'react-native-intersection-observer';
import * as SDK from '@snifftest/sdk/lib/rn';
import { tool } from '@/outs/utils/tool';
import {
	BarSimpleVO,
	CollectionActivityRespDTO,
	FrontBarVO
} from '@/outs/service/easyMarket';
import { goToLogin, request } from '@/config/request/interceptors';

import {
	EasyProductResp,
	ProductKeyWordSearchReqDTO
} from '@/outs/service/easyGoods';
import { CusGoods } from '@/outs/components/CusGoods';
import { changeImageCdn, formatMoney } from '@/outs/utils';
import { BeautyBg } from '@/outs/components/BeautyBg';
import ModalOrderCancel from '@/outs/components/goods/ModalOrderCancel';
import ModalOperationsImage from '@/outs/components/goods/ModalOperationsImage';
import { GlobalLoading } from '@/outs/utils/tool/loading/globalLoading';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import DrawerFocus from '@/outs/components/DrawerFocus';
import { isAuditVersion } from '@/outs/atom/atomSetting';
import HotUpdate from '@/outs/components/CodePush';
import { IS_IOS, TENJIN_KEY, tenjinInit } from '@/outs/hooks/common/useTenjin';
import { atomAnalyzeOrignData } from '@/outs/atom/atomAnalyzeOrignData';
import BackToTop from '@/outs/components/BackToTop';
import { defHeight } from '@/outs/utils/tool/utils';
import Empty from '@/outs/components/base/Empty';
import { EMPTY_PAGE_IMAGE_BG_TRANSPARENT } from '@/outs/const/url/image';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { useSchemeJumpEvent } from '@/outs/hooks/base/useSchemeJumpEvent';
import { useExposureTrunPage } from '@/outs/hooks/base/useExposureTrunPage';
import ModalHomeGuide from '@/outs/components/goods/ModalHomeGuide';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { CouponNotice } from '@/outs/components/CouponNotice';
import { CustomerDetailRespDTO } from '@/outs/service/customer';
import { usePageFocuEffect } from '@/outs/hooks/common/usePageFocuEffect';
import { ContextPreView } from './context';
import { styles } from './styles';
import { TopGoods } from './module/TopGoods';
import { Recommend } from './module/Recommend';
import { BarSimpleVOEx, Navigation } from './module/Navigation';
import { Cates } from './module/Cates';
import { Banner } from './module/Banner';
import { SearchInput } from './module/SearchInput';
import { Header } from './module/Header';
import SpecialView from './module/SpecialView';
import { BARCODE } from './../../const/index';
import { Fund } from './module/Fund';
export interface ChangeNavInter {
	item: BarSimpleVO;
	index: number;
	excludeChannelTagList?: [string, string, string];
}

export enum ENUM_NAVINDEX {
	/** 频道 other */
	OTHER = 0
}

export interface PageProps {
	atomUserDetail: PrimitiveAtom<
		| (CustomerDetailRespDTO & {
				membership: {
					templateLevel: number;
				};
		  })
		| undefined
	>;

	HomeSlider?: React.FC<{ visible: boolean; toggle: () => void }>;
	H5SiteBuyComponent: React.FC;
	H5HotProductComponent: React.FC<{ data: any }>;
	toast?: any;
}

const DirectHome = (props: PageProps) => {
	const { H5SiteBuyComponent, H5HotProductComponent } = props;
	const params = SDK.useParams();
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	const [cateId, setCateId] = useState<number>();
	const [channel, setChannel] = useState<BarSimpleVO>({});
	const [data, setData] = useState<FrontBarVO>();
	const [navIndex, setNavIndex] = useState<number>(ENUM_NAVINDEX.OTHER);
	const isAppAuditVersion = useAtomValue(isAuditVersion);
	const newHomeScrollRef = useRef(null);
	const [isFound, setisFound] = useState(true);
	const useDetail = useAtomValue(
		SDK.isH5() ? props.atomUserDetail : atomUserDetail
	);
	const isLogin = !!useDetail?.customerId;
	/** 频道code */
	const [barCode, setBarCode] = useState<string>();
	const [favoriteCode, setFavoriteCode] = useState<string>('');
	const [excludeChannelTagList, setExcludeChannelTagListsetChannel] =
		useState<string[]>();
	const [couponNoticeVisible, setCouponNoticeVisible] = useState(false);

	const [analyzeOriginData, setAnalyzeOriginData] =
		useAtom(atomAnalyzeOrignData);

	// 百元特辑
	const [hundredData, setHundredData] = useState<CollectionActivityRespDTO[]>(
		[]
	);
	const getHundredData = async (code?: string) => {
		const { data: hundred } =
			await request.easyMarket.collection.qryHundredModulesList({
				barCode: BARCODE[code ?? barCode] ?? ''
			});
		setHundredData(hundred);
	};
	useEffect(() => {
		getHundredData();
	}, []);
	/** 列页面商品曝光时，触发逻辑 */
	const exposureProductItem = (inView: boolean, item: EasyProductResp) => {
		if (inView) {
			SDK.TA.track({
				event: TaEvent.PAGE_VIEW,
				properties: {
					product_code: item.productCode,
					product_title_ja: item.productTitle,
					online_time: item.createTime,
					product_main_image_url: item.productMainImg
				}
			});
		}
	};
	const [
		RenderTrunPage,
		setParams,
		{ scrollRef, scrollHeight, isNetWordRequest }
	] = useExposureTrunPage<ProductKeyWordSearchReqDTO>({
		request: request.easyGoods.product.searchKeyword,
		params: undefined,
		nestedScrollEnabled: true,
		useMock: false,
		numColumns: 2,
		rootMargin: { top: 0, bottom: 0 },
		ListFooterComponent: (
			<View
				style={{
					height: SDK.fitSize(63),
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center'
				}}
			>
				{/* 没有数据不展示该条文案 */}
				{/* <Text style={{ color: '#ccc' }}>
						{tool.strings('~没有啦，我也是有底线的~')}
						</Text> */}
			</View>
		),

		ListHeaderComponent: (
			<>
				{!cateId && (
					<View>
						<Banner barCode={barCode} channel={channel} />
						{/* 百元特辑商品 */}
						<SpecialView
							list={hundredData || []}
							barCode={barCode}
							refuse={getHundredData}
							titleClick={(title: string) => {
								const url = `/goods/priceCollectionList?barCode=${barCode}&title=${encodeURIComponent(
									title
								)}`;
								if (SDK.isH5()) {
									nav({ path: url });
									return;
								}
								navigationWebViewH5(url);
							}}
						/>
						{/* TODO:2024-03-21 推荐商品不再展示 */}
						{/* {!!data?.activityAreaList?.length && (
								<Recommend
									data={data}
									channel={channel}
									{...props}
								/>
							)} */}
						{data && !cateId && (
							<TopGoods
								data={data}
								favoriteCode={favoriteCode}
								useDetail={useDetail}
								changeFavorite={setFavoriteCode}
								{...props}
							/>
						)}
					</View>
				)}
			</>
		),

		renderItem: (res) => {
			if (!res.item) {
				return null;
			}
			const newItem = {
				...res.item,
				productMainImg: changeImageCdn(res.item.productMainImg, 300)
			};
			return (
				<InView
					key={res.item.productCode}
					onChange={(inView: boolean) => {
						exposureProductItem(inView, newItem);
					}}
				>
					<CusGoods
						type="search/keyword"
						data={newItem}
						useDetail={useDetail}
						key={res.item.productCode}
						showPrice={isLogin || !!data?.showPrice}
						changeFavorite={setFavoriteCode}
						favoriteCode={favoriteCode}
						style={{
							marginRight: SDK.fitSize(res.index % 2 ? 0 : 10)
						}}
					/>
				</InView>
			);
		},
		ListEmptyComponent: () => {
			if (isNetWordRequest) {
				return (
					<View
						style={{
							padding: SDK.fitSize(12),
							paddingBottom: SDK.fitSize(20)
						}}
					>
						<Empty
							image={EMPTY_PAGE_IMAGE_BG_TRANSPARENT}
							imageStyle={{
								width: SDK.fitSize(100),
								height: SDK.fitSize(100)
							}}
							description={tool.strings(
								'検索条件に合う商品はありません'
							)}
							descriptionStyle={{ color: '#000' }}
						/>
					</View>
				);
			}
		}
	});

	/** 切换导航 */
	const changeNav = (
		item: BarSimpleVOEx,
		index: number,
		excludeChannelTagList?: string[]
	) => {
		setCateId(undefined);
		setNavIndex(index);
		if (!item.barCode) {
			return;
		}

		setBarCode(item.barCode!);
		getHundredData(item.barCode);
		// 此处需要修改。如果是特殊的barcode。不在执行下面的逻辑
		if (item.specialOpearte) {
			if (isFound) {
				// 执行子组件的刷新逻辑
				newHomeScrollRef.current.getNewData();
			}
			setisFound(true);
			newHomeScrollRef.current?.scrollTop?.();
			return;
		}
		setisFound(false);
		// 切换导航时，需要置顶
		if (scrollRef.current) {
			(scrollRef.current! as any).scrollToOffset({
				offset: 0,
				animated: true
			});
		}

		setChannel(item);
		setExcludeChannelTagListsetChannel(excludeChannelTagList);
		GlobalLoading.showLoading();
		request.easyMarket.front
			.barDetail(
				{
					barCode: item.barCode!
				},
				{
					useMock: false
				}
			)
			.then((res) => {
				GlobalLoading.dismissLoading();
				if (res.data) {
					const newTopProductList = res.data.topProductList?.map(
						(item) => {
							return {
								...item,
								productMainImg: changeImageCdn(
									item.productMainImg,
									300
								)
							};
						}
					);
					const newData = {
						...res.data,
						topProductList: newTopProductList
					};
					setData(newData);
					if (item.barCode === 'BAR004') {
						setParams({
							excludeChannelTagList: [
								'BAR001',
								'BAR002',
								'BAR003'
							]
						});
						return;
					}

					setParams({
						channelTagList: item.barCode
							? [item.barCode]
							: undefined
					});
				}
			})
			.finally(GlobalLoading.dismissLoading);
	};

	const showATTModal = () => {
		if (IS_IOS && !analyzeOriginData) {
			NativeModules.ToolModule.getIDFA().then((res) => {
				tenjinInit(IS_IOS, TENJIN_KEY, setAnalyzeOriginData);
			});
		}
	};

	const currentPageFocus = useCallback(() => {
		SDK.TA.track({
			event: TaEvent.PAGE_VIEW,
			properties: {
				position: 'home_page'
			}
		});
	}, []);

	usePageFocuEffect(currentPageFocus);
	if (!SDK.isH5()) {
		// eslint-disable-next-line react-hooks/rules-of-hooks
		useSchemeJumpEvent();
	}
	return (
		<ContextPreView.Provider value={{ ...props }}>
			<View onTouchMove={showATTModal} style={styles.root}>
				{!SDK.isH5() && <BeautyBg />}
				<Header {...props} />
				<SearchInput channelTag={barCode} navIndex={navIndex} />
				{isFound ? (
					<Fund
						isLogin={isLogin}
						barCode={barCode}
						ref={newHomeScrollRef}
						H5SiteBuyComponent={H5SiteBuyComponent}
						H5HotProductComponent={H5HotProductComponent}
						useDetail={useDetail}
						goLogin={() => {
							if (SDK.isH5()) {
								goToLogin();
								return;
							}
							nav(
								{ name: 'Login' },
								{ redirectKey: 'DirectIndex' }
							);
						}}
					/>
				) : (
					<>
						<Cates
							propsCateId={cateId}
							navIndex={navIndex}
							channelTag={barCode}
							navigationWebViewH5={navigationWebViewH5}
							onChange={(item) => {
								const url = `/goods/classify?cateId=${
									item.productCategoryFrontendId
								}&pageTitle=${encodeURIComponent(
									item.cateNameJp ?? ''
								)}&level=1`;
								if (SDK.isH5()) {
									nav({ path: url });
									return;
								}
								navigationWebViewH5(url);
							}}
						/>

						<View
							style={{
								flex: SDK.isH5() ? undefined : 1,
								height: SDK.isH5()
									? SDK.getDeviceSize().height - 165
									: undefined
							}}
							key={(barCode || '') + (cateId || '')}
						>
							{RenderTrunPage}
						</View>
					</>
				)}
				<Navigation onChange={changeNav} {...props} />
				<DrawerFocus isHomePage={true} />
				{/* 审核版本干掉运营图和订单取消提示 */}
				{isAppAuditVersion ? null : (
					<>
						<ModalOrderCancel />
						{couponNoticeVisible && <ModalOperationsImage />}
						{!SDK.isH5() && (
							<ModalHomeGuide
								endBack={() => setCouponNoticeVisible(true)}
							/>
						)}

						<CouponNotice
							open={SDK.isH5() ? true : couponNoticeVisible}
							isLogin={isLogin}
							customerId={useDetail?.customerId}
							goLogin={() => {
								if (SDK.isH5()) {
									goToLogin();
									return;
								}
								nav(
									{ name: 'Login' },
									{ redirectKey: 'DirectIndex' }
								);
							}}
							burialPoint={{
								position: 'new_user_coupon'
							}}
							type="home"
						/>
					</>
				)}

				{!SDK.isH5() && <HotUpdate />}
				{isFound ? null : (
					<BackToTop
						visible={scrollHeight > defHeight()}
						scrollView={scrollRef}
						bottom={
							useDetail?.customerId
								? SDK.fitSize(100)
								: SDK.fitSize(130)
						}
					/>
				)}
			</View>
		</ContextPreView.Provider>
	);
};

export default DirectHome;
