/*
 * @Author: yusha
 * @Date: 2023-12-21 10:10:36
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-27 10:42:19
 * @Description: 蒙层
 */

import { memo } from 'react';
import { TouchableOpacity, View } from 'react-native';

interface MaskProps extends React.HTMLAttributes<HTMLDivElement> {
  visible: boolean;
  closeOnClickOverlay: boolean;
  onPress?: (e: any) => void;
  zIndex?: number;
  overlayStyle?: any;
}
const Mask = (props: MaskProps) => {
  const {
    children,
    visible,
    closeOnClickOverlay = true,
    onPress,
    zIndex = 1000,
    overlayStyle
  } = props;
  if (!visible) {
    return;
  }
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={(e) => {
        if (!closeOnClickOverlay) {
          return;
        }
        onPress?.(e);
      }}
      style={[
      {
        backgroundColor: 'rgba(0,0,0,0.4)',
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        zIndex
      },
      { ...overlayStyle }]}>


			<View
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}>

				{children}
			</View>
		</TouchableOpacity>);

};
export default memo(Mask);