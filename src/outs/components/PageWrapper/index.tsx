/*
 * @Author: yusha
 * @Date: 2024-01-23 14:06:07
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-23 20:22:26
 * @Description: 检测是否连接网络
 */
import { memo, useEffect, useState } from 'react';
import NetInfo from '@react-native-community/netinfo';
import { Alert, Image, SafeAreaView, Text, View } from 'react-native';
import { tool } from '@/outs/utils/tool';
import { px2dp } from '@/outs/utils';

interface PageWrapperProps {
  children?: React.ReactNode;
}
const ZERO_SIGNAL_IMAGE = require('./img/zeroSignal.png');
const PageWrapper = (props: PageWrapperProps) => {
  const [isConnected, setIsConnected] = useState(false);
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      setIsConnected(Boolean(state.isConnected));
      // console.log('网络状态:', state.isConnected ? '已连接' : '未连接');
      // console.log('类型:', state.type);
      // console.log('是否是 Wi-Fi:', state.isWifiEnabled);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  const { children } = props;
  return (
    <>
			{isConnected && children}
			{!isConnected &&
      <SafeAreaView style={{ flex: 1 }}>
					<View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fff'
          }}>

						<Image
            style={{
              width: px2dp(100),
              height: px2dp(100),
              backgroundColor: '#fff'
              // backgroundColor: 'red'
            }}
            source={ZERO_SIGNAL_IMAGE} />


						<View
            style={{
              width: '67%'
            }}>

							<Text
              style={{
                color: '#000',
                textAlign: 'center',
                fontSize: px2dp(12),
                fontWeight: '400'
              }}>

								{tool.strings(
                'ネットワーク接続が失われたようです。'
              )}
							</Text>
							<Text
              style={{
                color: '#000',
                textAlign: 'center',
                fontSize: px2dp(12),
                fontWeight: '400'
              }}>

								{tool.strings('後でもう一度お試しください。')}
							</Text>
						</View>
					</View>
				</SafeAreaView>}

		</>);

};
export default memo(PageWrapper);