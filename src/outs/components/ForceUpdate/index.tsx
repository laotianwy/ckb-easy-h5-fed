/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-23 17:53:43
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-23 20:08:53
 * @FilePath: /ckb-fbox-app/src/components/ForceUpdate/index.tsx
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import FastImage from '@sniff/react-native-fast-image';
import { View, Text, TouchableOpacity, Linking } from 'react-native';
import useAppUpdateModal from '@/outs/hooks/common/useAppUpdateModal';
import { px2dp } from '@/outs/utils';
import { defHeight, defWidth, isAndroid } from '@/outs/utils/tool/utils';

const UpdateModal = () => {
  const [appUpdateModalConfig, setAppUpdateModalConfig] = useAppUpdateModal();
  const forceUpdate = () => {
    const openDownUrl = isAndroid() ?
    'https://play.google.com/store/apps/details?id=com.theckb.fbox' :
    'https://apps.apple.com/us/app/3fbox/id6474284467';
    Linking.openURL(openDownUrl);
  };

  const closeForceUpdate = () => {
    setAppUpdateModalConfig(undefined);
  };

  if (!appUpdateModalConfig) {
    return null;
  }
  return (
    <View
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 1000,
        flexDirection: 'row',
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
      }}>

			<View
        style={{
          width: px2dp(320),
          height: px2dp(370),
          alignItems: 'center',
          position: 'relative'
        }}>

				<FastImage
          style={{
            width: px2dp(358),
            height: px2dp(482),
            position: 'absolute',
            bottom: 0,
            left: px2dp(-20),
            alignItems: 'center'
          }}
          source={{
            uri: 'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/direct-force-update.png'
          }} />


				<Text
          style={{
            width: px2dp(210),
            textAlign: 'center',
            fontSize: px2dp(20),
            lineHeight: px2dp(32),
            color: '#000000',
            marginTop: px2dp(167)
          }}>

					{appUpdateModalConfig.title}
				</Text>
				<Text
          style={{
            width: px2dp(276),
            marginTop: px2dp(14),
            textAlign: 'center'
          }}
          numberOfLines={3}>

					{appUpdateModalConfig.desc}
				</Text>
				<TouchableOpacity
          onPress={forceUpdate}
          style={{
            paddingHorizontal: px2dp(38),
            height: px2dp(50),
            backgroundColor: '#000000',
            borderRadius: px2dp(44),
            alignItems: 'center',
            justifyContent: 'center',
            position: 'absolute',
            bottom: px2dp(20)
          }}>

					<Text
            style={{
              fontSize: px2dp(16),
              fontWeight: '500',
              color: '#fff'
            }}>

						{appUpdateModalConfig.okText}
					</Text>
				</TouchableOpacity>
				{appUpdateModalConfig.updateType !== 'forceUpdate' ?
        <TouchableOpacity
          onPress={closeForceUpdate}
          style={{ position: 'absolute', bottom: px2dp(-48) }}>

						<FastImage
            source={{
              uri: 'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/update-close.png'
            }}
            style={{
              width: px2dp(32),
              height: px2dp(32)
            }} />

					</TouchableOpacity> :
        null}
			</View>
		</View>);

};

export default UpdateModal;