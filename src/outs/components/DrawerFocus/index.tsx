/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-07 18:40:52
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-29 19:26:48
 * @FilePath: /ckb-mall-app/src/components/DrawerFocus/index.tsx
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { useSetAtom } from 'jotai';
import { memo, useCallback } from 'react';
import { atomDrawerFlag } from '@/outs/atom/atomDrawer';
import { usePageFocuEffect } from '@/outs/hooks/common/usePageFocuEffect';

interface DrawerFocusProps {
  isHomePage?: boolean;
}

const DrawerFocus = (props: DrawerFocusProps) => {
  const { isHomePage } = props;
  const setAtomDrawerFlag = useSetAtom(atomDrawerFlag);

  const fn = useCallback(() => {
    if (isHomePage && global.GLOBAL_TOKEN) {
      setAtomDrawerFlag(true);
    } else {
      setAtomDrawerFlag(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  usePageFocuEffect(fn);
  return null;
};

export default memo(DrawerFocus);