/*
 * @Author: huajian
 * @Date: 2023-12-04 15:20:55
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-28 20:21:06
 * @Description: 头部模块
 */
import React, { useEffect, useState } from 'react';
import { useAtomValue, useSetAtom } from 'jotai';
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	StyleProp,
	ViewStyle,
	Alert
} from 'react-native';
import { useMount, useToggle } from 'ahooks';
import { useNavigation } from '@react-navigation/native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import { EasyProductResp } from '@/outs/service/easyGoods';
import { tool } from '@/outs/utils/tool';
import { request } from '@/config/request/interceptors';
import { toThousands } from '@/outs/utils';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { useNavigationWebViewH5 } from '@/outs/hooks/common/useNavigationWebViewH5';
import { styles } from './styles';

interface Props {
	data: EasyProductResp & {
		[key: string]: any;
	};
	/** 是否登录 默认为true */
	isLogin?: boolean;
	style?: StyleProp<ViewStyle>;
	// 类型 默认为search/keyword（搜索接口）
	type?: 'search/keyword';
	/** 是否显示价格 默认为true */
	showPrice?: boolean;
	/* 收藏id */
	favoriteCode?: string;
	/** 切换收藏弹窗 */
	changeFavorite?: (code: string) => void;
	useDetail?: any;
}

/** 订单状态 */
enum EnumProductStatus {
	/** 带上架 */
	waittingSell,
	/** 出售中 */
	selling,
	/** 待下架 */
	wattingOff,
	/** 停止出售 售罄 */
	sellOut,
	/** 删除 */
	del
}

export const CusGoods = (props: Props) => {
	const nav = SDK.useNav();
	const {
		showPrice = true,
		data,
		favoriteCode,
		changeFavorite,
		useDetail
	} = props;
	/** 该商品是否收藏 */
	const [isFav, setIsFav] = useState(!!props.data.favoriteFlag);
	const isLogin = !!useDetail?.customerId;
	let activityPrice =
		props.type === 'search/keyword'
			? data.activityPriceJa
			: data.activitySellPriceJpy;
	const sellPrice =
		props.type === 'search/keyword'
			? data.productSellPriceJa
			: data.productLowestPrice;
	if (!activityPrice) {
		activityPrice = sellPrice;
	}
	const goDetail = () => {
		const { productCode, productTitle } = data;
		SDK.TA.track({
			event: TaEvent.PRODUCT_CLICK,
			properties: {
				product_code: productCode,
				product_title_ja: productTitle
			}
		});
		nav(
			{
				name: 'GoodDetail',
				path: `/goods/detail?productCode=${data.productCode}`
			},
			{ productCode: data.productCode }
		);
	};

	useMount(() => {
		// 初始化时默认全部关闭收藏蒙层
		changeFavorite?.('');
	});

	return (
		<TouchableOpacity
			activeOpacity={1}
			style={[styles.container, props.style]}
			onPress={() => goDetail()}
		>
			<View style={styles.imgWrap}>
				<Image
					style={styles.img}
					source={{ uri: data.productMainImg }}
				/>

				{data.status === EnumProductStatus.sellOut && (
					<View style={[styles.cover, styles.sellOutWrap]}>
						<View style={styles.sellOut}>
							<Text style={styles.sellOutText}>
								{tool.strings('售罄')}
							</Text>
						</View>
					</View>
				)}
			</View>
			<Text style={styles.title} numberOfLines={2}>
				{data.productTitle}
			</Text>

			{showPrice && (
				<View style={styles.priceWrap}>
					<View style={styles.price}>
						<Text style={styles.newPrice}>
							{toThousands(activityPrice)}
							{tool.strings('元')}
						</Text>
					</View>
				</View>
			)}

			{!showPrice && (
				<Text style={styles.priceTip}>
					{tool.strings('免费注册查看价格')}
				</Text>
			)}

			{data.status !== EnumProductStatus.sellOut && isLogin && (
				<TouchableOpacity
					style={[styles.actionsIconWrap]}
					onPress={async () => {
						if (isFav) {
							await request.easyGoods.favoriteProduct.cancel({
								productCode: data.productCode
							});
							tool.Toast.success(tool.strings('取消收藏成功'));
							setIsFav(false);
							return;
						}
						await request.easyGoods.favoriteProduct.postFavoriteProduct(
							{
								favoriteProductType: data.productType,
								platformType: data.platformType,
								productCode: data.productCode,
								productMainImg: data.productMainImg,
								productSellPrice: activityPrice,
								productTitle: data.productTitle
							}
						);
						setIsFav(true);
						tool.Toast.success(tool.strings('收藏成功'));
					}}
				>
					<Image
						style={styles.favoriteIcon}
						source={{
							uri: isFav
								? 'https://static-jp.theckb.com/static-asset/easy-app/collect1@3.x.png'
								: 'https://static-jp.theckb.com/static-asset/easy-app/notCollected@3.x.png'
						}}
					/>
				</TouchableOpacity>
			)}
		</TouchableOpacity>
	);
};
