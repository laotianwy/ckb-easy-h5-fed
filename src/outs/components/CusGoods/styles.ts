/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-05 11:50:51
 * @Description: styles
 */

import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  container: {
    width: px2dp(171),
    height: px2dp(248),
    borderRadius: px2dp(12),
    marginBottom: px2dp(12),
    backgroundColor: '#fff',
    overflow: 'hidden',
    position: 'relative'
  },
  imgWrap: {
    position: 'relative'
  },
  cover: {
    position: 'absolute',
    right: px2dp(0),
    bottom: px2dp(0),
    left: 0,
    top: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buyProxyBtn: {
    paddingHorizontal: px2dp(8),
    paddingVertical: px2dp(6),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#fff',
    borderWidth: px2dp(1),
    borderRadius: px2dp(16),
    backgroundColor: 'rgba(0, 0, 0, 0.3)'
  },
  buyProxyBtnText: {
    color: '#fff',
    fontSize: px2dp(12),
    fontWeight: '500'
  },
  proxyBtn: {
    backgroundColor: 'rgba(255, 80, 16, 0.1)',
    width: px2dp(64),
    height: px2dp(24),
    borderStyle: 'solid',
    borderColor: 'rgba(255, 181, 154, 1)',
    borderRadius: px2dp(14),
    borderWidth: px2dp(1),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: px2dp(12),
    fontWeight: '500',
    right: px2dp(4),
    top: px2dp(4),
    position: 'absolute'
  },
  proxyText: {
    color: 'rgba(255, 80, 16, 1)'
  },
  sellOutWrap: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)'
  },
  sellOut: {
    width: px2dp(72),
    height: px2dp(32),
    borderColor: '#fff',
    borderWidth: px2dp(1),
    borderRadius: px2dp(16),
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  sellOutText: {
    color: '#fff',
    fontSize: px2dp(12),
    fontWeight: '500'
  },
  img: {
    width: px2dp(171),
    height: px2dp(171)
  },
  title: {
    height: px2dp(36),
    lineHeight: px2dp(18),
    color: 'rgba(28, 32, 38, 1)',
    margin: px2dp(8),
    marginLeft: px2dp(6),
    marginRight: px2dp(6),
    marginHorizontal: px2dp(12),
    marginBottom: px2dp(3)
  },
  priceWrap: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: px2dp(5)
  },
  price: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginLeft: px2dp(8)
  },
  newPrice: {
    color: 'rgba(28, 32, 38, 1)',
    fontSize: px2dp(12),
    fontWeight: '500'
  },
  oldPrice: {
    color: 'rgba(126, 134, 148, 1)',
    fontSize: px2dp(10),
    marginLeft: px2dp(5)
  },
  priceTip: {
    textAlign: 'center',
    color: 'rgba(255, 80, 16, 1)'
  },
  actionsIconWrap: {
    position: 'absolute',
    right: px2dp(8),
    bottom: px2dp(8)
  },
  actionsIcon: {
    width: px2dp(16),
    height: px2dp(16)
  },
  favoriteWrap: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)'
  },
  favoriteIcon: {
    width: px2dp(16),
    height: px2dp(16)
  }
});