/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2023-12-12 12:16:28
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0
  },
  header: {
    width: px2dp(375),
    height: px2dp(120),
    backgroundColor: '#fff',
    borderBottomWidth: 1
  },
  bg: {
    width: px2dp(300),
    height: px2dp(300),
    position: 'absolute'
  }
});