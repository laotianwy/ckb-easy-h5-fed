/*
 * @Author: huajian
 * @Date: 2023-12-12 11:07:44
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-21 16:37:41
 * @Description: 背景
 */
import RadialGradient from 'react-native-radial-gradient';
import { BlurView } from '@react-native-community/blur';
import { View } from 'react-native';
import { px2dp } from '@/outs/utils';
import { tool } from '@/outs/utils/tool';
import { styles } from './styles';

export const BeautyBg = () => {
  return (
    <View style={styles.container}>
			<RadialGradient
        style={[styles.bg, { right: px2dp(-170), top: px2dp(-100) }]}
        colors={['rgba(202, 49, 49, .5)', 'rgba(255, 255, 255, 0)']}
        stops={[0, 1]}
        center={[px2dp(150), px2dp(150)]}
        radius={px2dp(150)} />


			<RadialGradient
        style={[
        styles.bg,
        {
          left: px2dp(-180),
          top: tool.defHeight() / 2 - px2dp(175)
        }]}

        colors={['rgba(213, 169, 17, .5)', 'rgba(255, 255, 255, 0)']}
        stops={[0, 1]}
        center={[px2dp(150), px2dp(150)]}
        radius={px2dp(150)} />


			<RadialGradient
        style={[
        styles.bg,
        {
          left: px2dp(-180),
          bottom: -px2dp(175)
        }]}

        colors={['rgba(171, 29, 123, .5)', 'rgba(255, 255, 255, 0)']}
        stops={[0, 1]}
        center={[px2dp(150), px2dp(150)]}
        radius={px2dp(150)} />


			{/*
                                <BlurView
                                style={styles.container}
                                blurType="light"
                                blurAmount={10}
                                reducedTransparencyFallbackColor="white"
                                /> */}
		</View>);

};