/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-15 19:19:51
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-28 20:06:46
 * @FilePath: /ckb-fbox-app/src/components/base/Empty/index.tsx
 * @Description: 空状态
 */
import { View, Text, TextStyle, ImageStyle, Image } from 'react-native';
import React from 'react';
import { px2dp } from '@/outs/utils';

interface EmptyProps {
	/** 图片下方描述 */
	description?: string | React.ReactNode;
	/** 图片下方描述样式 */
	descriptionStyle?: TextStyle;
	/**
	 * 自定义图片
	 * string：是图片地址
	 * React.ReactNode ：自定义图片
	 */
	image?: string;
	imageNode?: React.ReactNode;
	/** 图片样式 */
	imageStyle?: ImageStyle;
}
const EMPTY_BG =
	'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/empty_bg.png';
/** 空状态组件 */
const Empty = (props: EmptyProps) => {
	const { description, descriptionStyle, image = EMPTY_BG } = props;
	return (
		<View style={{ justifyContent: 'center', alignItems: 'center' }}>
			{props.imageNode ? (
				props.imageNode
			) : (
				<Image
					style={[
						{
							height: px2dp(150),
							width: px2dp(180),
							marginTop: px2dp(200)
						},
						props.imageStyle
					]}
					resizeMode={'contain'}
					source={{ uri: image }}
				/>
			)}

			<Text
				style={[
					{
						marginTop: px2dp(10),
						color: '#999'
					},
					descriptionStyle
				]}
			>
				{description}
			</Text>
		</View>
	);
};

export default Empty;
