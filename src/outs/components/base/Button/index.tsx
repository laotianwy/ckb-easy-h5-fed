/*
 * @Author: shiguang
 * @Date: 2023-09-13 17:23:52
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 13:55:26
 * @Description:
 */

import React from 'react';
import {
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle } from
'react-native';

export type ButtonTypes = readonly [
  'default',
  'primary',
  'ghost',
  'dashed',
  'link',
  'text'];


export type ButtonType = ButtonTypes[number];
export type SizeType = 'small' | 'middle' | 'large' | undefined;

export interface ButtonProps extends
  Pick<TouchableOpacityProps, 'onPress' | 'style'> {
  /** 类型 */
  type?: ButtonType;
  /** 大小 */
  size?: SizeType;
  /** 不可点击 */
  disabled?: boolean;
  /** 是否幽灵按钮 */
  ghost?: boolean;
  /** 是否危险 */
  danger?: boolean;
  children?: React.ReactNode;
  title?: React.ReactNode;
  style?: ViewStyle;
}
interface ButtonTypeConfig {
  /** 文本颜色 */
  textColor?: string;
  /** 背景颜色 */
  backgroundColor?: string;
  /** disabled 背景颜色 */
  disabledBackgroundColor?: string;
  /** 边框颜色 */
  borderColor?: string;
}
type ButtonTypeKeys = ButtonType | `${ButtonType}-ghost`;
const MAP_BUTTON_TYPE = new Map<ButtonTypeKeys, ButtonTypeConfig>([
['default', { textColor: '#232323', borderColor: '#E5E5E5' }],
[
'primary',
{
  textColor: '#FFF',
  backgroundColor: '#008060',
  disabledBackgroundColor: '#B2D9CF'
}],


['primary-ghost', { textColor: '#008060', borderColor: '#008060' }]]
);

const Button = (props: ButtonProps) => {
  const {
    onPress,
    children: _children,
    title: _title,
    disabled = false,
    type: _type = 'default',
    ghost = false,
    style = {}
  } = props;
  const children = _children ?? _title;
  const height = 40;
  const type = ghost ? `${_type}-ghost` : _type;
  const typeConfig = MAP_BUTTON_TYPE.get((type as any))!;
  return (
    <TouchableOpacity
      activeOpacity={disabled ? 1 : 0.5}
      onPress={onPress}
      style={{
        // backgroundColor: agree && username && pwd ? '#008060' : '#B2D9CF',
        backgroundColor: disabled ?
        typeConfig.disabledBackgroundColor :
        typeConfig.backgroundColor,
        borderWidth:
        ghost === true || type === 'default' ? 1 : undefined,
        borderColor: typeConfig.borderColor,
        height,
        borderRadius: height / 2,
        justifyContent: 'center',
        alignItems: 'center',
        ...style
      }}>

			{typeof children === 'string' ?
      <Text
        style={{
          fontSize: 16,
          fontWeight: '500',
          color: typeConfig.textColor
        }}>

					{children}
				</Text> :

      children}

		</TouchableOpacity>);

};

export default Button;