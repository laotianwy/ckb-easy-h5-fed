/*
 * @Author: shiguang
 * @Date: 2023-09-14 17:08:00
 * @LastEditors: shiguang
 * @LastEditTime: 2023-09-15 11:27:52
 * @Description: Text
 */
import React from 'react';
import { Text, TextProps as _TextProps } from 'react-native';

export interface LinkProps extends _TextProps {}

const Link = (props: LinkProps) => {
  const { style, ...restProps } = props;
  const textDom =
  <Text style={[{ color: '#008060' }, style]} {...restProps} />;


  return textDom;
};

export default Link;