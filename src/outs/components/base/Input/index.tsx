/*
 * @Author: shiguang
 * @Date: 2023-09-11 18:09:53
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2023-12-07 19:53:19
 * @Description: Input
 */
import React, { useState } from 'react';
import { View, TextInput, Text, TextInputProps, ViewStyle } from 'react-native';
import { tool } from '@/outs/utils/tool';

export type InputType = 'text' | 'password';

// 'button'
// | 'checkbox'
// | 'color'
// | 'date'
// | 'datetime-local'
// | 'email'
// | 'file'
// | 'hidden'
// | 'image'
// | 'month'
// | 'number'
// | 'password'
// | 'radio'
// | 'range'
// | 'reset'
// | 'search'
// | 'submit'
// | 'tel'
// | 'text'
// | 'time'
// | 'url'
// | 'week'

export interface InputProps extends Omit<TextInputProps, 'onChange'> {
  /** 类型 */
  // type?: 'text' | 'number';
  prefix?: React.ReactNode;
  suffix?: React.ReactNode;
  onChange?: TextInputProps['onChangeText'];
  /** input外层样式 */
  wrapStyle?: ViewStyle;
}

const getBorderColor = (isFocus: boolean) => {
  if (isFocus) {
    return '#008060';
  }
  return '#F0F2F5';
};

const Input = (props: InputProps) => {
  const { prefix, suffix, onChange, onFocus, onBlur, wrapStyle, ...rest } =
  props;
  const [isFocus, setIsFocus] = useState(false);

  return (
    <View>
			<View
        style={[
        {
          borderColor: getBorderColor(isFocus),
          height: 40,
          borderWidth: 1,
          borderRadius: 22,
          flexDirection: 'row',
          alignItems: 'center'
        },
        wrapStyle]}>


				{prefix}
				<TextInput
          placeholder={tool.strings('请输入')}
          placeholderTextColor="#999999"
          style={{
            paddingLeft: 20,
            paddingRight: 20,
            fontSize: 14,
            padding: 0,
            flex: 1,
            height: 44,
            color: '#000'
          }}
          {...rest}
          onFocus={(e) => {
            setIsFocus(true);
            onFocus?.(e);
          }}
          onBlur={(e) => {
            setIsFocus(false);
            onBlur?.(e);
          }}
          onChangeText={onChange} />


				{suffix}
			</View>
		</View>);

};

export default Input;