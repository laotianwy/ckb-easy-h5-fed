/*
 * @Author: shiguang
 * @Date: 2023-09-14 17:08:00
 * @LastEditors: shiguang
 * @LastEditTime: 2023-09-14 17:18:49
 * @Description: Text
 */
import React from 'react';
import { TextProps as OriginTextProps, Text as OriginText } from 'react-native';

export interface TextProps extends OriginTextProps {
  type?: 'default' | 'secondary' | 'success' | 'warning' | 'danger';
}

const Text = (props: TextProps) => {
  const { type, ...restProps } = props;
  return <OriginText {...restProps} />;
};

export default Text;