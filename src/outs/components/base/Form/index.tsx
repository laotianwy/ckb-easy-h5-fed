/*
 * @Author: shiguang
 * @Date: 2023-09-11 14:14:41
 * @LastEditors: shiguang
 * @LastEditTime: 2023-09-17 14:54:49
 * @Description: Form
 */
import React, { createContext } from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';
// import type { FieldValues, UseFormReturn } from 'react-hook-form';
import {
  FormProvider,
  UseFormProps,
  useForm as _useForm,
  FieldValues,
  UseFormReturn,
  FieldErrors } from
'react-hook-form';
import { tool } from '@/outs/utils/tool';
import Item from '../FormItem';

export interface FormContextProps {
  /** 展示验证错误 */
  showValidateError?: boolean;
  /** 展示必填标签 */
  showRequiredTag?: boolean;
  /** 自定义渲染 label */
  renderLabel?: (label: {
    label: React.ReactNode;
    required: boolean;
  }) => React.ReactNode;
  /** form item 的样式 */
  formItemStyle?: StyleProp<ViewStyle>;
}

export const FormContext = createContext<FormContextProps>({
  showValidateError: true
});
const FormItem = Item;

const Form: Form = (props) => {
  const {
    children,
    form,
    showValidateError,
    showRequiredTag = true,
    renderLabel,
    formItemStyle,
    style
  } = props;
  if (!form) {
    return <View>{children}</View>;
  }
  return (
    <FormContext.Provider
      value={{
        showValidateError: showValidateError ?? true,
        showRequiredTag,
        renderLabel,
        formItemStyle
      }}>

			<FormProvider {...form}>
				<View style={style}>{children}</View>
			</FormProvider>
		</FormContext.Provider>);

};

export interface UseForm<
  TFieldValues extends FieldValues,
  TContext = any,
  TTransformedValues extends FieldValues | undefined = undefined>
{
  (
  props?: UseFormProps<TFieldValues, TContext>)
  : [UseFormReturn<TFieldValues, TContext, TTransformedValues>];

  // _useFormType<TFieldValues, TContext, TTransformedValues>
}

const useForm = <
  TFieldValues extends FieldValues,
  TContext = any,
  TTransformedValues extends FieldValues | undefined = undefined>(

props?: UseFormProps<TFieldValues, TContext>) =>
{
  const form = _useForm<TFieldValues, TContext, TTransformedValues>(props);
  return ([form] as const);
};
export interface FormProps<Values extends FieldValues> extends
  Pick<
    FormContextProps,
    'renderLabel' |
    'showRequiredTag' |
    'showValidateError' |
    'formItemStyle'>
{
  /** children */
  children?: React.ReactNode;
  form?: ReturnType<UseForm<Values>>[0];
  /** form 的样式 */
  style?: StyleProp<ViewStyle>;
  // /** 是否在表单上展示错误 */
  // showValidateError?: boolean;
  // /** 展示必填标签 */
  // showRequiredTag?: boolean;
  // /** 自定义渲染 label */
  // renderLabel?: (label: React.ReactNode) => React.ReactNode;
}
export const getFirstErrorMessage = (
fieldErrors: FieldErrors,
options: {isShowToast?: boolean;} = { isShowToast: true }) =>
{
  for (const key in fieldErrors) {
    if (Object.prototype.hasOwnProperty.call(fieldErrors, key)) {
      const errObj = fieldErrors[key];
      if (options.isShowToast) {
        tool.Toast.stop(errObj?.message);
      }
      return errObj?.message;
    }
  }
};

Form.Item = FormItem;
Form.useForm = useForm;

export interface Form {
  (props: FormProps<any>): JSX.Element;
  Item: typeof FormItem;
  useForm: typeof useForm;
}

export default Form;