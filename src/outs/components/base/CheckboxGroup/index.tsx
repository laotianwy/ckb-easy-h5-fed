/*
 * @Author: yusha
 * @Date: 2023-09-16 13:51:12
 * @LastEditors: yusha
 * @LastEditTime: 2023-09-16 17:29:38
 * @Description: 复选框group
 */
import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TextInputProps,
  TouchableOpacity,
  StyleSheet } from
'react-native';
import { tool } from '@/outs/utils/tool';
import { px2dp } from '@/outs/utils/tool/px2dp';
import Checkbox from '../Checkbox';

export type InputType = 'text' | 'password';

interface Value {
  label: string;
  value: string | number;
}
export interface CheckboxGroupProps {
  /** 配置项 */
  options: Value[];
  onChange?: (val: string[]) => void;
  value?: string[];
  /** 文本样式 */
  textStyle?: any;
}

const CheckboxGroup = (props: CheckboxGroupProps) => {
  const { onChange, options, value = [], textStyle } = props;
  /** 选中的 */
  const [selectedValueList, setSelectedValueList] = useState<string[]>([]);
  useEffect(() => {
    if (value.length) {
      // 重新设值
      setSelectedValueList(value);
    } else {
      setSelectedValueList([]);
    }
  }, [value.length]);
  const handleChange = (val: string) => {
    // 先去找下是否有已经点击过的
    const index = selectedValueList?.findIndex((item) => item === val);
    const _selectedValueList = JSON.parse(
      JSON.stringify(selectedValueList)
    );
    // 如果没有则追加值
    if (index === -1) {
      _selectedValueList.push(val);
    } else {
      // 有的话就删除
      _selectedValueList.splice(index, 1);
    }
    setSelectedValueList(_selectedValueList);
    onChange?.(_selectedValueList);
  };
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: tool.defWidth()
      }}>

			{options.map((item: Value, index) => {
        return (
          <TouchableOpacity key={index}>
						<Checkbox
              checked={selectedValueList.includes((
                item.value as string)
              )}
              isGroup={true}
              checkoutOption={{
                label: item.label,
                value: item.value
              }}
              onChange={(val) => {
                handleChange((val as string));
              }}
              textStyle={textStyle} />

					</TouchableOpacity>);

      })}
		</View>);

};

export default CheckboxGroup;