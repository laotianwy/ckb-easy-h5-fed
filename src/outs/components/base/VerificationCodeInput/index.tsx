/*
 * @Author: shiguang
 * @Date: 2023-09-12 19:44:41
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 15:00:03
 * @Description: VerificationCodeInput
 */
import React, { useEffect } from 'react';
import { Text, TextStyle, TouchableOpacity, View } from 'react-native';
import { omit } from 'ramda';
// import { CountdownControllers, useCountdown } from '@/outs/hooks/useCountdown';
import { tool } from '@/outs/utils/tool';
import { px2dp } from '@/outs/utils/tool/px2dp';
import {
  useCountdown,
  CountdownControllers } from
'@/outs/hooks/base/useCountdown';
import Input, { InputProps } from '../Input';

interface VerificationCodeInputProps extends InputProps {
  countStart?: number;
  onPressVerificationCode?: (
  countdownControllers: CountdownControllers) =>
  any;
  /** 验证码文字样式 */
  codeTextStyle?: TextStyle;
}

const VerificationCodeInput = (_props: VerificationCodeInputProps) => {
  const { countStart = 60, onPressVerificationCode, ...restProps } = _props;
  const props = omit(['type', 'prefix', 'suffix'], (restProps as any));
  const [count, countdownControllers] = useCountdown({
    countStart,
    intervalMs: 1000
  });
  const { startCountdown, resetCountdown } = countdownControllers;

  useEffect(() => {
    if (count === 0) {
      resetCountdown();
    }
  }, [resetCountdown, count]);
  return (
    <Input
      suffix={
      <View style={{ paddingHorizontal: px2dp(12) }}>
					<TouchableOpacity
          onPress={() => {
            if (countdownControllers.isCountdownRunning) {
              return;
            }
            if (onPressVerificationCode === undefined) {
              return startCountdown();
            }
            onPressVerificationCode(countdownControllers);
          }}>

						<Text
            style={[
            {
              fontSize: px2dp(14),
              color: '#008060',
              fontWeight: '500'
            },
            props.codeTextStyle]}>


							{count === countStart ?
            tool.strings('获取验证码') :
            `${count}s`}
						</Text>
					</TouchableOpacity>
				</View>}

      {...props} />);


};
export default VerificationCodeInput;