/*
 * @Author: shiguang
 * @Date: 2023-09-15 10:14:27
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-19 17:30:52

 * @Description: Header
 */
import { useNavigation } from '@react-navigation/native';
import React, { ReactNode } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
  GestureResponderEvent,
  Text } from
'react-native';
import { px2dp } from '@/outs/utils/tool/px2dp';
import { tool } from '@/outs/utils/tool';

interface HeaderProps {
  /** 展示关闭按钮 */
  showClose?: boolean;
  /** 展示返回按钮 默认为 true */
  showBack?: boolean;
  /** 点击关闭按钮 */
  onClosePress?: (event: GestureResponderEvent) => void;
  /** 左侧图标 */
  backIconSource?: ImageSourcePropType;
  /** 是否启用左侧返回 */
  backIconEnable?: boolean;
  /** 左侧图标点击 */
  onBackPress?: (event: GestureResponderEvent) => void;
  /** 标题 */
  title?: string;
  /** 左侧图标 */
  rightIconSource?: ImageSourcePropType;
  /** 右侧标题 */
  rightTitle?: string;
  /** 右侧点击 */
  onPressRight?: (event: GestureResponderEvent) => void;
  /** 左侧自定义reactnode */
  leftReactNode?: ReactNode;
  /** 右侧自定义reactnode */
  rightReactNode?: ReactNode;
}
const Header = (props: HeaderProps) => {
  const navigation = useNavigation();
  const {
    backIconSource = {
      uri: 'https://static-s.theckb.com/BusinessMarket/App/Icon/new-proxy-goback.png'
    },
    onBackPress = () => {
      navigation.goBack();
    },
    onClosePress = () => {
      navigation.goBack();
    },
    showClose = false,
    showBack = true,
    backIconEnable = true,
    title,
    rightIconSource,
    rightTitle,
    leftReactNode,
    onPressRight,
    rightReactNode
  } = props;

  return (
    <View
      style={{
        backgroundColor: 'rgba(0,0,0,0)',
        height: px2dp(50),
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'relative'
      }}>

			{/* 中间部分 */}
			<View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          justifyContent: 'center'
        }}>

				{title &&
        <Text
          style={{
            color: '#333333',
            fontWeight: '600',
            fontSize: px2dp(16)
          }}>

						{title}
					</Text>}

			</View>
			{/* left */}
			<View
        style={{
          display: 'flex',
          alignItems: 'center',
          height: '100%',
          justifyContent: 'center',
          flexDirection: 'row'
        }}>

				{leftReactNode && leftReactNode}
				{showBack &&
        <TouchableOpacity
          style={{ marginLeft: px2dp(12) }}
          onPress={onBackPress}>

						{backIconEnable &&
          <Image
            style={{
              height: px2dp(18),
              width: px2dp(18),
              resizeMode: 'contain'
            }}
            source={backIconSource} />}


					</TouchableOpacity>}


				{showClose &&
        <TouchableOpacity
          style={{ marginLeft: px2dp(12) }}
          onPress={onClosePress}>

						<Text>{tool.strings('关闭')}</Text>
					</TouchableOpacity>}

			</View>
			{/* right */}
			<View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginRight: 6
        }}>

				{rightReactNode && rightReactNode}
				{Boolean(rightIconSource) &&
        <TouchableOpacity
          style={{
            marginRight: 8,
            height: '100%',
            justifyContent: 'center'
          }}
          onPress={onBackPress}>

						<Image
            style={{
              height: 16,
              width: 16,
              resizeMode: 'contain'
            }}
            source={backIconSource} />

					</TouchableOpacity>}


				{Boolean(rightTitle) &&
        <TouchableOpacity
          style={{
            marginRight: 8,
            height: '100%',
            justifyContent: 'center'
          }}
          onPress={onPressRight}>

						<Text
            style={[
            {
              fontSize: px2dp(14),
              color: '#333D3B',
              textAlign: 'right',
              paddingRight: px2dp(10)
            },
            { paddingRight: px2dp(5) }]}>


							{rightTitle}
						</Text>
					</TouchableOpacity>}

			</View>
		</View>);

};

export default Header;