/*
 * @Author: yusha
 * @Date: 2023-09-16 10:58:21
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2023-09-22 11:55:21
 * @Description: 单选框
 */

import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TextInputProps,
  TouchableOpacity,
  StyleSheet } from
'react-native';
import { tool } from '@/outs/utils/tool';
import { px2dp } from '@/outs/utils/tool/px2dp';

export type InputType = 'text' | 'password';

interface Value {
  label: string;
  value: string | number;
}
export interface RadioGroupProps {
  /** 配置项 */
  options: Value[];
  onChange?: (val?: number | string) => void;
  value?: number | string;
  disabled?: boolean;
  /** 文本样式 */
  textStyle?: any;
  /** 默认值 */
  // defaultValue?: number | string;
}

const RadioGroup = (props: RadioGroupProps) => {
  // const { onChange, options, value, textStyle={} } = props;
  const { onChange: _onChange, options, value: _value, textStyle } = props;
  /** 选中的值 */
  const [selectValue, setSelectValue] = useState<
    string | number | undefined>(
  );
  /** 选中的 */
  const [_internalValue, _setInternalValue] = useState<string | number>();
  const value = _onChange ? _value : _internalValue;

  const onChange = (value?: string | number) => {
    if (!_onChange) {
      _setInternalValue(value);
      return;
    }
    _onChange?.(value);
  };
  // useEffect(() => {
  //     // 重新设值
  //     setSelectValue(value);
  // }, [value]);
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: tool.defWidth(),
        marginTop: 4
      }}>

			{options.map((item: Value, index) => {
        return (
          <TouchableOpacity
            key={index}
            onPress={() => {
              /** 取消选中 */
              if (item.value === selectValue) {
                setSelectValue('');
                onChange?.();
              } else {
                /** 选中 */
                setSelectValue(item.value);
                onChange?.(item.value);
              }
              // onChange?.(item.value);
              // setSelectValue(item.value);
            }}
            style={
            value === item.value ?
            [
            styles.selected,
            {
              borderColor: tool.defColor(),
              backgroundColor: tool.defColor()
            }] :

            styles.select}>


						<Text
              style={
              value === item.value ?
              {
                color: '#fff',
                ...textStyle
              } :
              { ...textStyle }}>


							{item.label}
						</Text>
					</TouchableOpacity>);

      })}
		</View>);

};

export default RadioGroup;

const styles = StyleSheet.create({
  select: {
    borderColor: '#F6F6F6',
    backgroundColor: '#F6F6F6',
    height: 32,
    width: px2dp(110),
    borderWidth: 1,
    borderRadius: 22,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 16,
    marginBottom: px2dp(8)
  },
  selected: {
    height: 32,
    borderWidth: 1,
    width: px2dp(110),
    borderRadius: 22,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 16,
    marginBottom: px2dp(8)
  }
});