/*
 * @Author: huajian
 * @Date: 2024-02-23 16:54:14
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 14:59:37
 * @Description:
 */
import React, { useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { omit } from 'ramda';
import { tool } from '@/outs/utils/tool';
import Input, { InputProps } from '../Input';

/** 眼睛_关闭 */
const ICON_EYE_OFF =
'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_eye_off.png';
/** 眼睛_打开 */
const ICON_EYE_ON =
'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_eye_on.png';

interface PasswordInputProps extends InputProps {}

const PasswordInput = (_props: PasswordInputProps) => {
  const props = omit(['type', 'prefix', 'suffix'], (_props as any));

  const [isOpenEye, setIsOpenEye] = useState(false);

  return (
    <Input
      placeholder={tool.strings('请输入密码')}
      secureTextEntry={!isOpenEye}
      suffix={
      <TouchableOpacity
        onPress={() => {
          setIsOpenEye((_isOpenEye) => !_isOpenEye);
        }}
        style={{
          height: 40,
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: 10,
          paddingRight: 20
        }}>

					<Image
          style={{ height: 16, width: 16 }}
          resizeMode={'contain'}
          // source={
          // 	isOpenEye
          // 		? LoginImage.icon_password_visible
          // 		: LoginImage.icon_password_invisible
          // }
          source={{ uri: isOpenEye ? ICON_EYE_ON : ICON_EYE_OFF }} />

				</TouchableOpacity>}

      {...props} />);


};
export default PasswordInput;