/*
 * @Author: shiguang
 * @Date: 2023-09-12 19:44:41
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-06 15:00:16
 * @Description: PhoneInput
 */
import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { omit } from 'ramda';
import { px2dp } from '@/outs/utils/tool/px2dp';
import { tool } from '@/outs/utils/tool';
import { useBoolean } from '@/outs/hooks/base/useBoolean';
import Input, { InputProps } from '../Input';

interface PhoneInput extends InputProps {
  /** 启用手机区号 */
  enableArea?: boolean;
  /** 显示的区号 */
  phoneArea?: string;
  /** 手机区号改变 */
  onChangeArea?: (area: string) => void;
}

const icon_arrow_down =
'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_arrow_down.png';
const icon_arrow_up =
'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_arrow_up.png';
interface countryCodeListProps {
  title: string;
  value: string;
}

/** 手机区号列表 */
const countryCodeList: countryCodeListProps[] = [
{ title: '+81', value: '+81' },
{ title: tool.strings('不写区号'), value: '' }];


const PhoneInput = (_props: PhoneInput) => {
  const props = omit(['type', 'prefix', 'suffix'], (_props as any));
  const { value: isShowCountryCode, toggle: setIsShowCountryCode } =
  useBoolean(false);

  const { enableArea, onChangeArea, phoneArea } = props;

  return (
    <Input
      prefix={
      <View
        style={{
          paddingLeft: px2dp(12),
          paddingRight: 0,
          flexDirection: 'row',
          position: 'relative'
        }}>

					{enableArea && isShowCountryCode ?
        <View
          style={{
            position: 'absolute',
            top: tool.px2dp(30),
            width: tool.px2dp(120),
            // borderWidth: BORDER_WIDTH,
            // borderColor: 'black',
            borderRadius: tool.px2dp(8),
            overflow: 'hidden',
            zIndex: 1000000,
            backgroundColor: '#fff'
          }}>

							{countryCodeList.map((item, index) => {
            return (
              <TouchableOpacity
                style={{
                  padding: tool.px2dp(12),
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  flex: 1,
                  zIndex: 100000
                }}
                onPress={() => {
                  onChangeArea?.(item.value);
                  setIsShowCountryCode();
                }}
                key={index}>

										<Text
                  style={{
                    color: '#333',
                    fontWeight: '500'
                  }}>

											{item.title}
										</Text>
										{item.value === phoneArea ?
                <Image
                  source={{
                    uri: 'https://static-s.theckb.com/BusinessMarket/Easy/app/check-small.png'
                  }}
                  style={{
                    width: tool.px2dp(17),
                    height: tool.px2dp(11)
                  }} /> :

                null}
									</TouchableOpacity>);

          })}
						</View> :
        null}
					<TouchableOpacity
          onPress={setIsShowCountryCode}
          style={{
            flexDirection: 'row'
          }}>

						<Text style={{ marginRight: 4, color: '#232323' }}>
							{phoneArea ? phoneArea : '-'}
						</Text>
						<Image
            style={{ width: px2dp(16), height: px2dp(16) }}
            source={{
              uri:
              enableArea && isShowCountryCode ?
              icon_arrow_up :
              icon_arrow_down
            }} />

					</TouchableOpacity>

					<View
          style={{
            paddingLeft: px2dp(12),
            borderRightWidth: px2dp(1),
            borderRightColor: '#DCDCDC',
            borderStyle: 'solid'
          }} />

				</View>}

      // suffix={
      //     <View style={{ padding: 12, paddingLeft: 0 }} >
      //         <Text style={{ fontSize: 14, color: '#008060', fontWeight: '500' }} >获取验证码</Text>
      //     </View>
      // }
      {...props} />);


};
export default PhoneInput;