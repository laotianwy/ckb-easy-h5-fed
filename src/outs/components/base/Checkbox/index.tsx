/*
 * @Author: yusha
 * @Date: 2023-09-16 14:11:27
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2023-09-21 10:05:30
 * @Description: checkbox
 */
import React, { useEffect, useState } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  View } from
'react-native';
import { tool } from '@/outs/utils/tool';
import { px2dp } from '@/outs/utils/tool/px2dp';

interface CheckoutOption {
  label: string | number;
  value: string | number;
}
interface CheckboxProps {
  /** 显示的内容 */
  checkoutOption: CheckoutOption;
  onChange?: (val?: string | number) => void;
  /** 是否需要group包含 */
  isGroup?: boolean;
  /** 选中的值 */
  value?: string | number;
  /** 是否选中-（和isGroup配套使用） */
  checked?: boolean;
  /** 文本样式 */
  textStyle?: any;
}
const Checkbox = (props: CheckboxProps) => {
  const {
    onChange,
    value,
    checkoutOption,
    isGroup = false,
    checked = false,
    textStyle = {}
  } = props;
  const [isChecked, setIsChecked] = useState(false);

  useEffect(() => {
    // 如果是直接使用该组件
    if (!isGroup) {
      // 判断是否选中
      if (value === checkoutOption.value) {
        setIsChecked(true);
      } else {
        setIsChecked(false);
      }
    }
  }, [checkoutOption.value, value, checked, isGroup]);
  useEffect(() => {
    // 如果是group包裹，则直接用父组件传的checked
    if (isGroup) {
      setIsChecked(checked);
    }
  }, [checkoutOption.value, value, checked, isGroup]);
  const change = () => {
    // 如果是单个使用checkbox且之前是选中需要取消选中
    if (!isGroup && isChecked) {
      onChange?.(undefined);
    } else {
      onChange?.(checkoutOption.value);
    }
    setIsChecked(!isChecked);
  };

  const _renderImage = () => {
    let uri =
    'https://static-s.theckb.com/BusinessMarket/App/Icon/ic_check_box_outline_blank@3x.png';

    if (isChecked) {
      uri =
      global.SYSTEMSOURCE === 2 ?
      'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_cart_checked_b2b@3x.png' :
      'https://static-s.theckb.com/app/ic_check_box@3x.png';
    }
    return (
      <Image
        source={{ uri }}
        resizeMode={'contain'}
        style={{
          minWidth: 20,
          height: tool.px2dp(20),
          width: tool.px2dp(20)
        }} />);


  };
  return (
    <TouchableHighlight
      underlayColor="transparent"
      onPress={() => change()}
      style={{
        marginRight: px2dp(16),
        marginBottom: px2dp(8),
        minWidth: px2dp(70),
        maxWidth: px2dp(120)
      }}>

			<View style={styles.container}>
				{_renderImage()}
				<Text style={{ marginLeft: 8, ...textStyle }}>
					{checkoutOption.label}
				</Text>
			</View>
		</TouchableHighlight>);

};
export default Checkbox;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 4
  }
});