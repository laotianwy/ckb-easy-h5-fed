/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-15 17:43:46
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-01-16 19:38:21
 * @FilePath: /ckb-fbox-app/src/components/base/BackTop/index.tsx
 * @Description: 返回顶部
 */
import { Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { px2dp } from '@/outs/utils';

/** 返回顶部ICON */
const ICON_BACK_TOP =
'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/icon_backTop.png';

interface BackTopProps {
  /** 是否显示 */
  visible?: boolean;
  /** 返回顶部目标 */
  target: React.MutableRefObject<any>;
  setIsShowBackTop?: React.Dispatch<React.SetStateAction<boolean>>;
}
/** 返回顶部 */
const BackTop = (props: BackTopProps) => {
  const { target } = props;

  /** 返回顶部 */
  const onPresBackTop = () => {
    target!.current!.scrollToOffset({
      offset: 0,
      animated: true
    });
    props.setIsShowBackTop && props.setIsShowBackTop(false);
  };
  return (
    <>
			{props.visible &&
      <TouchableOpacity
        style={{
          position: 'absolute',
          right: px2dp(12),
          bottom: px2dp(100),
          width: px2dp(48),
          height: px2dp(48),
          borderRadius: px2dp(24),
          backgroundColor: '#fff',
          justifyContent: 'center',
          alignItems: 'center',
          shadowColor: 'rgba(38, 35, 33,.7)',
          shadowOffset: { width: 0, height: 0 },
          elevation: 5,
          borderColor: 'rgba(38, 35, 33,.1)',
          borderWidth: px2dp(0.2)
        }}
        activeOpacity={0.7}
        onPress={onPresBackTop}>

					<Image
          source={{
            uri: ICON_BACK_TOP,
            width: px2dp(24),
            height: px2dp(24)
          }} />

				</TouchableOpacity>}

		</>);

};

export default BackTop;