/*
 * @Author: shiguang
 * @Date: 2023-09-14 17:04:47
 * @LastEditors: shiguang
 * @LastEditTime: 2023-09-14 17:28:55
 * @Description: Typography 排版样式
 */

import { View } from 'react-native';
import React from 'react';
import Text from '../Text';
import Link from '../Link';

/** 排版样式 */
const Typography: Typography = () => {
  return <View />;
};
Typography.Link = Link;
Typography.Text = Text;
/** 排版样式 */
export interface Typography {
  (): JSX.Element;
  /** 文本 */
  Text: typeof Text;
  /** 链接 */
  Link: typeof Link;
}

export default Typography;