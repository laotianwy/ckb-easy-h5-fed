/*
 * @Author: shiguang
 * @Date: 2023-09-11 14:14:41
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2023-12-07 14:14:20
 * @Description: Form
 */
import React, { useContext } from 'react';
import {
  ControllerProps,
  useController,
  useFormContext } from
'react-hook-form';
import { Text, TextStyle, View, ViewProps } from 'react-native';
import { tool } from '@/outs/utils/tool';
import { px2dp } from '@/outs/utils/tool/px2dp';
import { FormContext } from '../Form';

export interface FormItemProps extends
  Pick<ControllerProps, 'name'>,
  Pick<ViewProps, 'style'> {
  rules?: Rule[];
  // Omit<RegisterOptions<FieldValues, string>, 'disabled' | 'setValueAs' | 'valueAsNumber' | 'valueAsDate'> | undefined;
  /** label */
  label?: React.ReactNode;
  labelStyle?: TextStyle;
  /** children */
  children?: React.ReactNode;
  /** 是否必填 */
  required?: boolean;
  /** requiredNode dom节点 */
  requiredNode?: React.ReactNode;
  /** required dom节点在左边还是右边 */
  requiredNodeInsertSide?: 'left' | 'right';
  /** label右侧内容 */
  lableRightNode?: React.ReactNode;
  /** 额外 */
  extra?: React.ReactNode;
  trigger?: string;
  blur?: string;
  // valuePropName: 'value',
}

const getDefaultRequiredNode = () => {
  return (
    <View
      style={{
        backgroundColor: '#E03D3D',
        borderRadius: px2dp(2),
        height: px2dp(20),
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: px2dp(4),
        marginRight: px2dp(4)
      }}>

			<Text style={{ fontSize: px2dp(12), color: '#fff' }}>
				{tool.strings('必須')}
			</Text>
		</View>);

};

const getRequiredNode = (requiredNode?: React.ReactNode) => {
  if (!requiredNode) {
    return getDefaultRequiredNode();
  }
  return typeof requiredNode === 'string' ?
  <View>
			<View style={{ marginRight: px2dp(4) }}>
				<Text>{requiredNode}</Text>
			</View>
		</View> :

  requiredNode;

};

const getLabeloNode = (labeloNode?: React.ReactNode) => {
  return typeof labeloNode === 'string' ?
  <Text
    style={{
      fontSize: px2dp(12),
      color: '#1C2026',
      fontWeight: '400',
      lineHeight: px2dp(20)
    }}>

			{labeloNode}
		</Text> :
  null;
};

const renderExtra = (extraNode?: React.ReactNode) => {
  if (!extraNode) {
    return null;
  }
  return typeof extraNode === 'string' ?
  <View style={{ marginTop: px2dp(4) }}>
			<Text
      style={{
        fontSize: px2dp(12),
        color: '#777',
        fontWeight: '400'
      }}>

				{extraNode}
			</Text>
		</View> :

  extraNode;

};
// Validate<TFieldValue, TFormValues> = (value: TFieldValue, formValues: TFormValues) => ValidateResult | Promise<ValidateResult>;
interface RequiredRule {
  required: boolean;
  message: string;
}
interface MinRule {
  min: number;
  message: string;
}
interface MaxRule {
  max: number;
  message: string;
}
interface MaxLengthRule {
  maxLength: number;
  message: string;
}
interface MinLengthRule {
  minLength: number;
  message: string;
}

interface RegExpRule {
  regExp: RegExp;
  message: string;
}

// const xx = /a/
// RegExp

interface ValidatorRule {
  validator: (
  value: any,
  formValues: Record<string, any>) =>
  Promise<undefined | boolean | string> | undefined | boolean | string;
}

export type Rule =
RequiredRule |
MinRule |
MaxRule |
MaxLengthRule |
MinLengthRule |
ValidatorRule |
RegExpRule;

/**
 * 是否存在
 * @param value
 * @returns
 */
const validateRequired = (value: any) => {
  return ![undefined, null, ''].includes(value);
};

/**
 * 最小值校验
 * @param value
 * @returns
 */
const validateMin = (value: any, min: number) => {
  return value >= min;
};

/**
 * 最大值校验
 * @param value
 * @returns
 */
const validateMax = (value: any, max: number) => {
  return value <= max;
};

/**
 * 最大值校验
 * @param value
 * @returns
 */
const validateMaxLength = (value: any, max: number) => {
  return value?.length <= max;
};

/**
 * 最小值校验
 * @param value
 * @returns
 */
const validateMinLength = (value: any, min: number) => {
  return value?.length >= min;
};

/**
 * 最小值校验
 * @param value
 * @returns
 */
const validateRegExp = (value: any, regExp: RegExp) => {
  return regExp.test(value);
};

const Item = (props: FormItemProps) => {
  const {
    label: _label,
    children,
    required: _required = false,
    requiredNode: _requiredNode,
    requiredNodeInsertSide = 'left',
    lableRightNode,
    extra,
    name,
    trigger = 'onChange',
    blur = 'onBlur',
    rules = [],
    style
  } = props;
  const form = useFormContext();
  const { control, formState } = form;
  const {
    showValidateError,
    showRequiredTag,
    renderLabel: _renderLabel,
    formItemStyle
  } = useContext(FormContext);
  const required = (() => {
    if (!showRequiredTag) {
      return false;
    }
    if (_required) {
      return _required;
    }
    const item = rules.find((item) => {
      return 'required' in item && item.required;
    });
    return Boolean(item);
  })();
  const {
    field,
    fieldState: { invalid, isTouched, isDirty },
    formState: { touchedFields, dirtyFields }
  } = useController({
    name: name!,
    control,
    rules: {
      // required: { value: true, message: name + '必填' },
      validate: async (value, formValues) => {
        const isNotEmpty = !['', null, undefined].includes(value);
        for (let i = 0; i < rules.length; i++) {
          const curRule = rules[i];
          if ('required' in curRule && curRule.required) {
            const res = validateRequired(value);
            if (!res) {
              return curRule.message;
            }
          }
          if ('min' in curRule && isNotEmpty) {
            const res = validateMin(value, curRule.min);
            if (!res) {
              return curRule.message;
            }
          }
          if ('max' in curRule && isNotEmpty) {
            const res = validateMax(value, curRule.max);
            if (!res) {
              return curRule.message;
            }
          }
          if ('minLength' in curRule && isNotEmpty) {
            const res = validateMinLength(value, curRule.minLength);
            if (!res) {
              return curRule.message;
            }
          }
          if ('maxLength' in curRule && isNotEmpty) {
            const res = validateMaxLength(value, curRule.maxLength);
            if (!res) {
              return curRule.message;
            }
          }
          if ('regExp' in curRule && isNotEmpty) {
            const res = validateRegExp(value, curRule.regExp);
            if (!res) {
              return curRule.message;
            }
          }
          if ('validator' in curRule) {
            const res = await curRule.validator(value, formValues);
            if (res) {
              return res;
            }
          }
        }
        return undefined;
      }
    }
  });
  // form
  const { errors } = formState;
  const label = getLabeloNode(_label);
  const requiredNode = getRequiredNode(_requiredNode);
  const getCloneChildren = () => {
    const _children = (children as React.ReactElement);
    const originOnTrigger = _children.props[trigger];
    const originOnBlur = _children.props[blur];
    return React.cloneElement(_children, {
      ..._children.props,
      value: field.value,
      [trigger]: function (...params: any[]) {
        originOnTrigger?.(...params);
        field.onChange(...params);
      },
      [blur]: function (...params: any[]) {
        originOnBlur?.(...params);
        form.trigger(name);
        field.onBlur();
      },
      ref: field.ref
    });
  };
  const getErrMessage = () => {
    return errors?.[name!]?.message;
  };
  const errMessage = (getErrMessage() as string);
  /**
   * 渲染错误
   * @returns
   */
  const renderErr = () => {
    if (showValidateError && errMessage) {
      return (
        <Text style={{ color: '#EB4545', zIndex: -1 }}>
					{errMessage}
				</Text>);

    }
    return null;
  };
  /**
   * 渲染 label
   */
  const renderLabel = () => {
    if (!label) {
      return null;
    }
    if (!_renderLabel) {
      return (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginRight: 15
          }}>

					<View
            style={{
              flexDirection: 'row',
              height: 20,
              alignItems: 'center',
              marginBottom: 10
            }}>

						{required && requiredNodeInsertSide === 'left' ?
            requiredNode :
            null}
						{label}
						{required && requiredNodeInsertSide === 'right' ?
            requiredNode :
            null}
					</View>
					{lableRightNode}
				</View>);

    }
    return _renderLabel({ label, required });
  };
  return (
    <View style={style ?? formItemStyle}>
			{renderLabel()}
			<View>
				{name && <View>{getCloneChildren()}</View>}
				{!name && children}
			</View>
			{renderExtra(extra)}
			{renderErr()}
		</View>);

};

export default Item;