/*
 * @Author: yusha
 * @Date: 2023-12-26 14:02:10
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-26 14:08:48
 * @Description:
 */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import CodePush from 'react-native-code-push';
import { tool } from '@/outs/utils/tool';

let total,
  received,
  progress = 0;
class HotUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progressModalVisible: false
    };
    CodePush.disallowRestart(); // 禁止重启
  }

  componentDidMount() {
    CodePush.allowRestart();
    this.syncImmediate(); // 开始检查更新
  }

  syncImmediate() {
    CodePush.sync(
      {
        // installMode: CodePush.InstallMode.ON_NEXT_RESTART
        installMode: CodePush.InstallMode.IMMEDIATE
      },
      this.codePushStatusDidChange.bind(this),
      this.codePushDownloadDidProgress.bind(this)
    );
  }

  codePushStatusDidChange(syncStatus) {
    switch (syncStatus) {
      case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log(tool.strings('正在检正在检查更新...'));
        break;
      case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log(tool.strings('下载更新包中...'));
        break;
      case CodePush.SyncStatus.AWAITING_USER_ACTION:
        console.log(tool.strings('等待选择更新'));
        break;
      case CodePush.SyncStatus.INSTALLING_UPDATE:
        console.log(tool.strings('安装更新中...'));
        break;
      case CodePush.SyncStatus.UP_TO_DATE:
        console.log(tool.strings('更新成功'));
        break;
      case CodePush.SyncStatus.UPDATE_IGNORED:
        console.log(tool.strings('用户取消更新'));
        break;
      case CodePush.SyncStatus.UPDATE_INSTALLED:
        console.log(tool.strings('安装成功,等待重启!'));
        break;
      case CodePush.SyncStatus.UNKNOWN_ERROR:
        console.log(tool.strings('更新出错，请重启应用！'));
        break;
    }
  }

  codePushDownloadDidProgress(progress) {
    let temp = parseFloat(
      progress.receivedBytes / progress.totalBytes
    ).toFixed(2);
  }

  render() {
    return null;
  }
}

const codePushOptions = {
  checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME
  // checkFrequency : CodePush.CheckFrequency.ON_APP_START
};
export default CodePush(codePushOptions)(HotUpdate);