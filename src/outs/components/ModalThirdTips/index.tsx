/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-11-27 11:22:57
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2023-12-07 17:38:36
 * @FilePath: /theckbsniffmobile/App/components/ModalThirdTips/index.tsx
 * @Description: 三方登陆已注册提示
 */
import React, { memo } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { px2dp } from '@/outs/utils/tool/px2dp';
import { BORDER_WIDTH } from '@/outs/utils/tool/utils';
import { tool } from '@/outs/utils/tool';

interface TipsModalProps {
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

/** 三方提示modal */
const ModalThirdTips = (props: TipsModalProps) => {
  const { visible, setVisible } = props;
  return (
    <Modal
      backdropColor={'#000'}
      backdropOpacity={0.6}
      backdropTransitionOutTiming={0}
      isVisible={visible}
      style={{
        flex: 1,
        margin: 0,
        justifyContent: 'center',
        alignItems: 'center'
      }}>

			<View
        style={{
          width: px2dp(228),
          // height: px2dp(196),
          borderRadius: px2dp(11),
          alignItems: 'center',
          overflow: 'hidden',
          backgroundColor: '#fff',
          paddingTop: px2dp(22),
          paddingHorizontal: px2dp(20),
          paddingBottom: px2dp(17)
        }}>

				<Text
          style={{
            fontSize: px2dp(19),
            color: '#232323',
            fontWeight: '500',
            lineHeight: px2dp(27)
          }}>

					{tool.strings('绑定失败')}
				</Text>
				<Text
          style={{
            fontSize: px2dp(15),
            color: '#333',
            fontWeight: '400',
            lineHeight: px2dp(23),
            marginTop: px2dp(12)
          }}>

					{tool.strings(
            '您的直行便账户已绑定了{thirdName}账户，无法再绑定，请先解绑原先的后再进行绑定。'
          )}
				</Text>
				<TouchableOpacity
          onPress={() => setVisible(false)}
          style={{
            width: px2dp(88),
            height: px2dp(44),
            borderRadius: px2dp(24),
            marginTop: px2dp(12),
            borderWidth: px2dp(2),
            borderColor: '#CDD2DA',
            alignItems: 'center',
            justifyContent: 'center'
          }}>

					<Text
            style={{
              fontSize: px2dp(15),
              fontWeight: '500',
              color: '#1C2026'
            }}>

						{tool.strings('我知道了')}
					</Text>
				</TouchableOpacity>
			</View>
		</Modal>);

};

export default memo(ModalThirdTips);