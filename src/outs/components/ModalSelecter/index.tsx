import React, { useState, forwardRef, useImperativeHandle, memo } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Modal,
  TouchableOpacity } from
'react-native';
import { px2dp } from '@/outs/utils/tool/px2dp';
import { tool } from '@/outs/utils/tool';

interface BtnProps {
  title: string;
  onPress?: () => void;
}

export interface ModalSelecterRef {
  show: (btns: BtnProps[]) => void;
  hide: (callBack?: () => void) => void;
}

const ModalSelecter = forwardRef((props: any, ref: any) => {
  const [btns, setBtns] = useState<BtnProps[]>([]);
  const [visible, setVisible] = useState(false);

  const _show = (btns: BtnProps[]) => {
    setVisible(true);
    setBtns(btns.reverse());
  };

  const _hide = (callback?: () => void) => {
    setVisible(false);
    callback?.();
  };

  useImperativeHandle(
    ref,
    (): ModalSelecterRef => ({
      show: (btns: BtnProps[]) => {
        _show(btns);
      },
      hide: (callBack?: () => void) => {
        _hide(callBack);
      }
    }),
    []
  );

  /** 渲染传过来的列表按钮 */
  const renderBtns = () => {
    return btns.map((item, index) => {
      let btnStyle = (styles.btnMid as any);
      if (index === 0) {
        btnStyle = styles.btnBottom;
      }
      if (index === btns.length - 1) {
        btnStyle = styles.btnTop;
      }

      if (btns.length === 1) {
        btnStyle = styles.btn;
      }
      return (
        <TouchableOpacity
          key={item.title}
          activeOpacity={0.95}
          onPress={() => onBtnPress(item)}
          style={btnStyle}>

					<Text style={styles.btnTitle}>{item.title}</Text>
				</TouchableOpacity>);

    });
  };

  const onBtnPress = (item: BtnProps) => {
    item.onPress?.();
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={() => {
        _hide();
      }}>

			<TouchableWithoutFeedback
        onPress={() => {
          _hide();
        }}>

				<View style={styles.container}>
					<View style={styles.btnView}>
						<TouchableOpacity
              activeOpacity={0.95}
              onPress={() => {
                _hide();
              }}
              style={styles.btn}>

							<Text style={styles.cancelBtn}>
								{tool.strings('取消')}
							</Text>
						</TouchableOpacity>
						<View style={{ height: 9 }} />
						{renderBtns()}
					</View>
				</View>
			</TouchableWithoutFeedback>
		</Modal>);

});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column-reverse',
    height: '100%',
    width: '100%',
    backgroundColor: '#0008',
    paddingTop: 30,
    paddingBottom: 30
  },
  btnView: {
    alignItems: 'center',
    flexDirection: 'column-reverse'
  },
  btn: {
    width: '95%',
    height: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 13,
    marginTop: 0
  },
  btnTop: {
    width: '95%',
    height: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 13,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    margin: 0,
    marginTop: 0,
    borderBottomWidth: 0.5,
    borderColor: 'gray'
  },
  btnMid: {
    width: '95%',
    height: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0,
    marginTop: 0,

    borderBottomWidth: tool.BORDER_WIDTH,
    borderColor: 'gray'
  },
  btnBottom: {
    width: '95%',
    height: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 13,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    margin: 0,
    marginTop: 0
  },
  btnTitle: {
    color: '#666',
    fontSize: px2dp(14)
  },
  cancelBtn: {
    color: '#999',
    fontSize: px2dp(14)
  }
});

export default memo(ModalSelecter);