/*
 * @Author: yusha
 * @Date: 2024-01-11 10:00:59
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-05 16:48:49
 * @Description: 同构mask
 */
import { Stack, StackProps } from '@snifftest/tamagui';
import ReactDOM from 'react-dom';
import React from 'react';
import RootSiblingsManager from 'react-native-root-siblings';
import { isH5 } from '@snifftest/sdk/lib/rn';
interface Props extends StackProps {
  children: React.ReactNode;
  onClose?: () => void;
  open?: boolean;
  width?: number;
  autoClose?: boolean;
  maskColor?: string;
}
const showModal = (renderModal: any) => {
  let onClose = () => {};
  if (isH5()) {
    const el = document.createElement('div');
    let node = null;
    onClose = () => {
      document.body.removeChild(el);
      node = null;
    };
    node = (renderModal(onClose) as any);
    // 将元素插入到body中
    // eslint-disable-next-line react/no-deprecated
    ReactDOM.render((node as any), el);
    document.body.appendChild(el);
  } else {
    let node: any = null;
    onClose = () => {
      node?.destroy();
      node = (null as any);
    };
    node = new RootSiblingsManager(renderModal(onClose));
  }
  return onClose;
};
export function IsomorphismMask(props: Props) {
  const {
    children,
    onClose: propsOnClose,
    open,
    autoClose = true,
    maskColor,
    ...rest
  } = props;
  if (!open) {
    return null;
  }
  // console.log('-----showModal', 11112);
  showModal((onClose: () => void) => {
    return (
      <>
				<Stack
          position="absolute"
          left={0}
          right={0}
          top={0}
          bottom={0}
          backgroundColor={maskColor ?? 'rgba(0,0,0,0.1)'}
          onPress={() => {
            autoClose && onClose();
            propsOnClose && propsOnClose();
          }} />


				<Stack
          position="absolute"
          left={0}
          right={0}
          top={0}
          bottom={0}
          {...rest}>

					{children}
				</Stack>
			</>);

  });
  return null;
}