/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2023-11-16 10:03:55
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-05 17:00:53
 * @FilePath: /theckbsniffmobile/App/components/common/CommonModal/index.tsx
 * @Description: 公共组件/公共弹窗
 */
import {
  Modal,
  StyleSheet,
  Text,
  View,
  Image,
  TextStyle,
  ViewStyle } from
'react-native';
import React, { ReactNode, useEffect, useState } from 'react';
import { px2dp } from '@/outs/utils';
import { tool } from '@/outs/utils/tool';
import Button from '../base/Button';

interface CommonModalProps {
  /** 是否可见 */
  visible: boolean;
  content?: string | ReactNode;
  contentStyle?: TextStyle;
  modalWrapStyle?: ViewStyle;
  /** 确认按钮文字 */
  okText?: string;
  /** 取消按钮文字 */
  cancelText?: string;
  /** 关闭 */
  onCancel?: () => void;
  /** 点击确定 */
  onOk?: () => void;
  /** 打开 */
  onOpen?: () => void;
  /** 关闭时销毁 Modal 里的子元素 */
  destroyOnClose?: boolean;
  /** 头部内容，当不需要头部时，可以设为 header={null} */
  header?: ReactNode;
  /** 底部内容， 当不需要默认底部按钮时，可以设为 footer={null} */
  footer?: ReactNode;
}

const CommonModal = (props: CommonModalProps) => {
  const {
    visible,
    content,
    okText = tool.strings('确定'),
    cancelText = tool.strings('取消'),
    onCancel,
    onOk,
    onOpen,
    destroyOnClose,
    header = true,
    footer
  } = props;

  const [innerVisible, setInnerVisible] = useState(visible);
  /**  */
  const [showChildren, setShowChildren] = useState(true);
  /** 显示 */

  const open = () => {
    if (!innerVisible) {
      setInnerVisible(true);
    }
    if (destroyOnClose) {
      setShowChildren(true);
    }
    onOpen && onOpen();
  };

  /** 关闭 */
  const close = () => {
    onCancel && onCancel();
  };

  useEffect(() => {
    visible && open();
    !visible && close();
  }, [visible]);

  return (
    <Modal
      animationType="fade"
      visible={visible}
      transparent={true}
      presentationStyle={'overFullScreen'}
      statusBarTranslucent={true}
      onRequestClose={close}>

			<View
        style={[
        styles.centeredView,
        { backgroundColor: 'rgba(0, 0, 0, 0.5)' }]}>


				<View style={[styles.dialogModal, props.modalWrapStyle]}>
					{header &&
          <View style={styles.dialogHeader}>
							<Image
              style={{
                width: px2dp(20),
                height: px2dp(20)
              }}
              source={{
                uri: 'https://static-jp.theckb.com/static-asset/easy-app/icon_waring@3x.png'
              }} />


							<Text style={styles.headerTitle}>
								{tool.strings('提示')}
							</Text>
						</View>}


					<View style={[{ marginTop: px2dp(16) }]}>
						<Text
              style={[
              { color: '#333', fontWeight: '500' },
              props.contentStyle]}>


							{content}
						</Text>
					</View>
					<View
            style={{
              flexDirection: 'row',
              marginVertical: px2dp(16)
            }}>

						{footer ?
            footer :

            <>
								<Button
                onPress={close}
                type={('primary-ghost' as any)}
                style={{
                  height: px2dp(36),
                  flex: 1,
                  borderRadius: px2dp(21),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: px2dp(0.5),
                  borderColor: tool.defColor()
                }}>

									{cancelText}
								</Button>
								<Button
                onPress={onOk}
                type="primary"
                style={{
                  height: px2dp(36),
                  flex: 1,
                  borderRadius: px2dp(21),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: px2dp(0.5),
                  borderColor: tool.defColor(),
                  backgroundColor: tool.defColor(),
                  marginLeft: px2dp(16)
                }}>

									{okText}
								</Button>
							</>}

					</View>
				</View>
			</View>
		</Modal>);

};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dialogModal: {
    width: tool.defWidth() - px2dp(60),
    borderRadius: px2dp(8),
    backgroundColor: '#fff',
    padding: px2dp(16),
    paddingBottom: 0
  },
  dialogHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: px2dp(0.8),
    borderBottomColor: '#f5f5f5',
    paddingBottom: px2dp(16)
  },
  headerTitle: {
    textAlign: 'center',
    marginLeft: px2dp(5),
    color: '#333',
    fontWeight: '500',
    fontSize: px2dp(14)
  },
  dialogContent: {
    padding: px2dp(16)
  },
  contentTop: {
    fontSize: px2dp(14),
    fontWeight: '500'
  },
  footerBtn: {
    height: px2dp(36),
    flex: 1,
    borderRadius: px2dp(21),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: px2dp(16),
    borderWidth: px2dp(0.5),
    borderColor: '#d9d9d9'
  }
});

export default CommonModal;