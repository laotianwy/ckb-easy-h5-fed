/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-03-15 16:34:33
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-04-08 19:52:10
 * @FilePath: /3fbox-page-mid/src/outs/components/Share/utils.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import NativeShare, { Social } from 'react-native-share';
import { ShareDialog, SharePhotoContent } from 'react-native-fbsdk-next';
import { FacebookStoriesShareSingleOptions } from 'react-native-share/lib/typescript/src/types';
import { Linking } from 'react-native';
import { ClipboardText, isAndroid } from '@/outs/utils/tool/utils';
import { tool } from '@/outs/utils/tool';

export enum ENUM_SHARE_TYPE {
	FACEBOOK = 'facebook',
	LINE = 'line',
	INSTAGRAM = 'instagram',
	TWITTER = 'twitter',
	COPY = 'copy',
	SAVE = 'save'
}

export interface ActionParams {
	shareImageData: string;
	SHARE_TEXT_CONTENT_LINE_SHARE: string;
	SHARE_TEXT_CONTENT_OTHER_SHARE: string;
	save: () => void;
}

const MAP_SHARE = new Map([
	[
		ENUM_SHARE_TYPE.FACEBOOK,
		{
			appType: ENUM_SHARE_TYPE.FACEBOOK,
			icon: 'https://static-jp.theckb.com/static-asset/easy-app/FacebookIcon.png',
			// title: 'フェイスブック',
			title: () => tool.strings('FACEBOOK'),
			action: async (props: ActionParams) => {
				const { shareImageData } = props;
				if (isAndroid()) {
					const shareOptionsFACEBOOK_ANDROID = {
						social: NativeShare.Social.FACEBOOK,
						url: shareImageData,
						type: 'image/*'
					};
					const ShareResponse = await NativeShare.shareSingle(
						shareOptionsFACEBOOK_ANDROID as FacebookStoriesShareSingleOptions
					);
				} else {
					// fb已完结分享
					const sharePhotoContent = {
						contentType: 'photo',
						photos: [
							{
								imageUrl: shareImageData
							}
						]
					};
					ShareDialog.show(sharePhotoContent as SharePhotoContent);
				}
			}
		}
	],
	[
		ENUM_SHARE_TYPE.TWITTER,
		{
			appType: ENUM_SHARE_TYPE.TWITTER,
			icon: 'https://static-jp.theckb.com/static-asset/easy-app/TwitterIcon.png',
			// title: 'ツイッター',
			title: () => tool.strings('twitter'),
			action: async (props: ActionParams) => {
				const { SHARE_TEXT_CONTENT_OTHER_SHARE, shareImageData } =
					props;
				// twitter已完结分享
				const shareOptionsTwitter = {
					title: 'Share file',
					message: SHARE_TEXT_CONTENT_OTHER_SHARE,
					url: shareImageData,
					social: NativeShare.Social.TWITTER
				};
				const ShareResponse = await NativeShare.shareSingle(
					shareOptionsTwitter as FacebookStoriesShareSingleOptions
				);
			}
		}
	],
	[
		ENUM_SHARE_TYPE.LINE,
		{
			appType: ENUM_SHARE_TYPE.LINE,
			icon: 'https://static-jp.theckb.com/static-asset/easy-app/LineIcon.png',
			// title: 'ライン',
			title: () => tool.strings('line'),
			action: async (props: ActionParams) => {
				const { SHARE_TEXT_CONTENT_LINE_SHARE } = props;
				Linking.openURL(
					`https://line.me/R/share?text=${SHARE_TEXT_CONTENT_LINE_SHARE}`
				);
			}
		}
	],
	[
		ENUM_SHARE_TYPE.INSTAGRAM,
		{
			appType: ENUM_SHARE_TYPE.INSTAGRAM,
			icon: 'https://static-jp.theckb.com/static-asset/easy-app/insIcon.png',
			// title: 'Instagram',
			title: () => tool.strings('Instagram'),
			action: async (props: ActionParams) => {
				const { shareImageData } = props;
				// instagram已完结分享
				if (isAndroid()) {
					const shareOptionsINSTAGRAMSTORIES = {
						social: NativeShare.Social.INSTAGRAM,
						url: shareImageData,
						type: 'image/jpeg'
					};
					const ShareResponse = await NativeShare.shareSingle(
						shareOptionsINSTAGRAMSTORIES as FacebookStoriesShareSingleOptions
					);
				} else {
					const shareOptionsINSTAGRAMSTORIES = {
						social: NativeShare.Social.INSTAGRAM,
						url: shareImageData,
						type: 'image/*'
					};
					const ShareResponse = await NativeShare.shareSingle(
						shareOptionsINSTAGRAMSTORIES as FacebookStoriesShareSingleOptions
					);
				}
			}
			// action: instagramShare
		}
	],
	[
		ENUM_SHARE_TYPE.COPY,
		{
			appType: ENUM_SHARE_TYPE.COPY,
			icon: 'https://static-jp.theckb.com/static-asset/easy-app/copyIcon.png',
			// リンクコピー
			title: () => tool.strings('复制链接'),
			action: (props: ActionParams) => {
				const { SHARE_TEXT_CONTENT_OTHER_SHARE } = props;
				ClipboardText(SHARE_TEXT_CONTENT_OTHER_SHARE);
			}
		}
	],
	[
		ENUM_SHARE_TYPE.SAVE,
		{
			appType: ENUM_SHARE_TYPE.SAVE,
			icon: 'https://static-jp.theckb.com/static-asset/easy-app/save-camera.png',
			title: () => tool.strings('保存到相册'),
			action: async (props: ActionParams) => {
				props.save();
			}
		}
	]
]);

const MAP_SHARE_LIST = [];

MAP_SHARE.forEach((value) => {
	MAP_SHARE_LIST.push({
		...value
	});
});

export const NEW_MAP_SHARE_LIST = MAP_SHARE_LIST;
