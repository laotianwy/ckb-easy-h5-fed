/*
 * @Author: yusha
 * @Date: 2023-12-18 11:45:16
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 09:59:58
 * @Description:
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  mask: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  modal: {
    width: px2dp(343),
    height: px2dp(528),
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto'
  },
  content: {
    width: '100%',
    height: px2dp(480),
    marginTop: 'auto',
    borderRadius: px2dp(8),
    overflow: 'hidden'
  },
  close: {
    width: px2dp(30),
    height: px2dp(30),
    alignSelf: 'flex-end',
    fontSize: px2dp(30)
  },
  img: {
    width: '100%',
    height: '100%'
  }
});