/*
 * @Author: yusha
 * @Date: 2023-12-18 11:39:56
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-17 16:04:30
 * @Description: 首页运营位弹窗
 */
import { memo, useCallback, useEffect, useState } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { px2dp } from '@/outs/utils';
import Mask from '@/outs/components/Mask';
import { getItem, setItem } from '@/outs/utils/tool/AsyncStorage';
import { request } from '@/config/request/interceptors';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { HOME_GUIDE } from '@/outs/const/config';
import {
	useNavWebViewH5,
	INCLUDE_DOMAIN
} from '@/outs/hooks/common/useNavWebViewH5';
import { usePageFocuEffect } from '@/outs/hooks/common/usePageFocuEffect';
import { styles } from './style';

const ACTIVITY_OPERA_MODAL_URL = 'ACTIVITY_OPERA_MODAL_URL';
const ModalOperationsImage = () => {
	const nav = SDK.useNav();
	const navWebViewH5 = useNavWebViewH5();
	const jumpTo = SDK.useJumpTo();
	const [visible, setVisible] = useState(false);
	const [imageUrl, setImageUrl] = useState('');
	const [jumpUrl, setJumpUrl] = useState('');
	const initPage = useCallback(() => {
		(async () => {
			console.log(5859);
			const res = await request.easyMarket.popup.show(
				{
					bizScene: '1',
					sourceChannel: '2'
				}
				// { useMock: true }
			);
			if (!res.success) {
				return;
			}
			if (!res.data) {
				return;
			}
			/** 获取新人指引状态 */
			const homeGuideStatus = await getItem(HOME_GUIDE);
			if (!homeGuideStatus) {
				return;
			}
			const isShowModal = await getItem(ACTIVITY_OPERA_MODAL_URL);
			if (isShowModal !== res.data?.picUrl) {
				setImageUrl(res.data?.picUrl || '');
				setJumpUrl(res.data?.jumpUrl || '');
				setTimeout(() => {
					setVisible(true);
				}, 60);
			}
		})();
	}, []);
	usePageFocuEffect(initPage);
	/** 点击运营弹窗 */
	const handleClickJump = () => {
		/** 点击运营弹窗埋点 */
		SDK.TA.track({
			event: TaEvent.BTN_CLICK,
			properties: {
				position: 'newcomer_window'
			}
		});
		if (!(typeof jumpUrl === 'string' && jumpUrl.trim().length > 0)) {
			return;
		}
		if (jumpUrl?.includes(INCLUDE_DOMAIN)) {
			if (SDK.isH5()) {
				navWebViewH5(jumpUrl);
				return;
			}
			const [, productCode] = jumpUrl.split('goods/detail?productCode=');
			if (typeof productCode === 'string' && productCode.length > 0) {
				nav(
					{
						name: 'GoodDetail',
						path: `/goods/detail?productCode=${productCode}`
					},
					{ productCode }
				);
				return;
			}

			navWebViewH5(jumpUrl);
			return;
		}
		nav(
			{
				name: 'WebviewOther'
			},
			{ url: jumpUrl }
		);
	};

	/** 关闭弹窗 */
	const handleClickClose = () => {
		setItem(ACTIVITY_OPERA_MODAL_URL, imageUrl);
		setVisible(false);

		/** 运营弹窗显示埋，因埋点初始化完成比较慢 */
		SDK.TA.track({
			event: TaEvent.POPUP_WINDOW,
			properties: {
				position: 'newcomer_page'
			}
		});

		/** 关闭运营弹窗埋点 */
		SDK.TA.track({
			event: TaEvent.BTN_CLICK,
			properties: {
				position: 'close_newcomer_window'
			}
		});
	};

	const getImageUrl = () => {
		return imageUrl;
	};

	return (
		<Mask visible={visible} closeOnClickOverlay={false}>
			<View style={styles.modal}>
				<TouchableOpacity
					style={styles.close}
					onPress={handleClickClose}
					activeOpacity={1}
				>
					<Image
						style={{
							width: px2dp(32),
							height: px2dp(32)
						}}
						source={{
							uri: 'https://static-s.theckb.com/BusinessMarket/Easy/H5/close-1.png'
						}}
					/>
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.content}
					onPress={handleClickJump}
					activeOpacity={1}
				>
					<Image
						style={{
							width: '100%',
							height: '100%'
						}}
						source={{
							uri: getImageUrl()
						}}
					/>
				</TouchableOpacity>
			</View>
		</Mask>
	);
};
/** 运营图弹框 */
export default memo(ModalOperationsImage);
