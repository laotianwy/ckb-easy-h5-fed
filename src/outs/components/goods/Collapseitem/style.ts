/*
 * @Author: yusha
 * @Date: 2023-12-18 16:16:46
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 09:59:09
 * @Description:
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export default (() =>
StyleSheet.create({
  collapseItem__header: {
    flexDirection: 'row',
    // width: '100%',
    height: px2dp(36),
    backgroundColor: '#F6F6F6',
    // borderRadius: px2dp(8),
    overflow: 'hidden',
    alignItems: 'center',
    padding: px2dp(10),

    justifyContent: 'space-between'
  },
  // borderBt: {
  // 	borderBottomWidth: px2dp(1),
  // 	borderBottomColor: '#eeeff2'
  // },
  collapseItem__title: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  collapseItem__title__text: {
    color: '#303438',
    fontSize: px2dp(12)
  },
  collapseItem__subTitle: {
    fontSize: px2dp(12),
    color: '#777777',
    width: px2dp(138)
    // textAlign: 'center',
    // backgroundColor: 'red'
  },
  // collapseItem__title__disable: {
  // 	color: theme['$disable-color']
  // },
  // collapseItem__titleIconLeft: {
  // 	marginRight: px2dp(20),
  // 	alignItems: 'center'
  // },
  // collapseItem__titleIconRight: {
  // 	marginLeft: px2dp(20),
  // 	alignItems: 'center'
  // },
  // collapseItem__subTitle: {
  // 	flex: 1,
  // 	alignItems: 'flex-end',
  // 	paddingHorizontal: px2dp(40),
  // 	overflow: 'hidden',
  // 	textAlign: 'right',
  // 	color: theme['$title-color2']
  // },
  // collapseItem__iconBox: {
  // 	paddingRight: px2dp(80)
  // },
  // collapseItem__icon: {
  // 	alignItems: 'center',
  // 	transform: [{ translateY: -px2dp(20) }]
  // },
  // collapseItem__content: {
  // 	overflow: 'hidden',
  // 	color: theme['$background-color3'],
  // 	fontSize: px2dp(28),
  // 	lineHeight: 1.5,
  // 	backgroundColor: '#fff'
  // 	// transition: all 0.3s linear,
  // },
  // collapseItem__contentText: {
  // 	color: theme['$title-color'],
  // 	paddingVertical: px2dp(24),
  // 	paddingHorizontal: px2dp(52)
  // },
  collapseItem__rightContent: {
    alignItems: 'center',
    // justifyContent: 'center',
    flexDirection: 'row'
  }
}));