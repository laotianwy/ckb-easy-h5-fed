/*
 * @Author: yusha
 * @Date: 2023-12-18 16:14:52
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-05 16:49:35
 * @Descripx2dpion:
 */
import React, { FunctionComponent, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Animated, Image } from 'react-native';

import { px2dp } from '@/outs/utils';
import collapseStyles from './style';

export interface CollapseItemProps {
  title: string;
  name: string;
  isOpen: boolean;
  icon: string;
  iconSize: string;
  iconColor: string;
  disabled: boolean;
  rotate: number;
  subTitle: string;
  titleIcon: string;
  titleIconColor: string;
  titleIconPosition: string;
  titleIconSize: string;
  childnull: boolean;
  children: any;
  onToggle: (isOpen: boolean, name: string) => void;
}

const defaultProps = ({
  title: '',
  name: '',
  isOpen: false,
  icon: '',
  iconSize: '',
  iconColor: '',
  disabled: false,
  rotate: 180,
  subTitle: '',
  titleIcon: '',
  titleIconColor: '',
  titleIconPosition: '',
  titleIconSize: '',
  childnull: true
} as CollapseItemProps);

export const CollapseItem: FunctionComponent<Partial<CollapseItemProps>> = (
props) =>
{
  const {
    children,
    title,
    isOpen,
    onToggle,
    name,
    disabled,
    icon,
    rotate,
    subTitle,
    titleIcon,
    titleIconColor,
    titleIconPosition,
    titleIconSize,
    iconSize,
    iconColor,
    childnull
  } = {
    ...defaultProps,
    ...props
  };
  const styles = collapseStyles();

  const [currHeight, setCurrHeight] = useState<number | string>('auto'); // 设置content的高度
  const [iconStyle, setIconStyle] = useState<object>({
    transform: [{ translateY: -px2dp(20) }]
  });

  useEffect(() => {
    // 一开始content都有高度，在这里根据isOpen，改变其高度
    setTimeout(() => {
      const newIconStyle = isOpen ?
      {
        transform: [
        { translateY: -px2dp(20) },
        { rotate: `${rotate}deg` }]

      } :
      { transform: [{ translateY: -px2dp(20) }] };

      setIconStyle(newIconStyle);
    }, 10);
  }, [isOpen, rotate]);

  useEffect(() => {
    if (!isOpen) {
      setCurrHeight(0);
    } else {
      setCurrHeight('auto');
    }
  }, [children, isOpen]);

  return (
    <>
			<TouchableOpacity
        style={[styles.collapseItem__header]}
        activeOpacity={1}
        onPress={() => {
          if (disabled) {
            return;
          }
          onToggle?.(isOpen, name);
        }}>

				<View style={styles.collapseItem__title}>
					<Text style={[styles.collapseItem__title__text]}>
						{title}
					</Text>
				</View>
				<View style={styles.collapseItem__rightContent}>
					<Text
            numberOfLines={1}
            style={styles.collapseItem__subTitle}>

						{/* {subTitle ?? ''} */}
						D2C3892493348932421
					</Text>
					<Animated.View
            style={{
              width: px2dp(12),
              height: px2dp(12),
              transform: [{ rotate: isOpen ? '180deg' : '0deg' }]
            }}>

						<Image
              style={{
                width: px2dp(12),
                height: px2dp(12)
              }}
              source={{
                uri: 'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_arrow_down.png'
              }} />

					</Animated.View>
				</View>
			</TouchableOpacity>
			{childnull && isOpen &&
      <View>
					<View
          style={{
            paddingLeft: px2dp(12),
            paddingRight: px2dp(12),
            backgroundColor: '#f6f6f6'
          }}>

						{typeof children === 'string' ?
          <Text>{children}</Text> :

          children}

					</View>
				</View>}

		</>);

};

CollapseItem.defaultProps = defaultProps;
CollapseItem.displayName = 'CollapseItem';