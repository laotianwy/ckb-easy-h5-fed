/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-22 18:45:42
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-17 16:04:27
 * @FilePath: /3fbox-page-mid/src/outs/components/goods/ModalHomeGuide/config.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import * as SDK from '@snifftest/sdk/lib/rn';
import { TaEvent } from '@/outs/utils/TDAnalytics';

export enum ENUM_HOME_GUIDE {
	INIT_PAGE,
	FIRST_STEP,
	SECOND_STEP,
	THIRD_STEP,
	FOUR_STEP
}
/** 引导图片_网络图 */
export enum HOME_GUIDE_IMG {
	HOME_GUIDE_1 = 'https://static-jp.theckb.com/static-asset/easy-app/home_guide_1.png',
	HOME_GUIDE_2 = 'https://static-jp.theckb.com/static-asset/easy-app/home_guide_2.png',
	HOME_GUIDE_3 = 'https://static-jp.theckb.com/static-asset/easy-app/home_guide_3.png'
}
/** 本地图片 */
export const HOME_GUIDE_IMG_LOCAL = {
	HOME_GUIDE_1: require('./image/home_guide_1.jpg'),
	HOME_GUIDE_2: require('./image/home_guide_2.png'),
	HOME_GUIDE_3: require('./image/home_guide_4.png'),
	HOME_GUIDE_4: require('./image/home_guide_3.png')
};

/** 埋点类型 */
enum ENUM_GUIDE_TYPE {
	/** 初始化 */
	GUIDE_PAGE = 'guide_page',
	/** 第一步 */
	TRY_NOW = 'try_now',
	/** 第二步 */
	NEXT = 'next',
	/** 第三步 */
	BUY_NOW = 'buy_now'
}
const boryingPointAction = (type: ENUM_GUIDE_TYPE) => {
	SDK.TA.track({
		event: TaEvent.BTN_CLICK,
		properties: {
			position: type
		}
	});
};

/** 首页指引步骤_埋点 */
export const MAP_HOME_GUIDE = new Map([
	[
		ENUM_HOME_GUIDE.INIT_PAGE,
		{
			boryingPoint: () => {
				SDK.TA.track({
					event: TaEvent.PAGE_VIEW,
					properties: {
						position: ENUM_GUIDE_TYPE.GUIDE_PAGE
					}
				});
			}
		}
	],

	[
		ENUM_HOME_GUIDE.FIRST_STEP,
		{
			boryingPoint: () => boryingPointAction(ENUM_GUIDE_TYPE.TRY_NOW)
		}
	],

	[
		ENUM_HOME_GUIDE.SECOND_STEP,
		{
			boryingPoint: () => boryingPointAction(ENUM_GUIDE_TYPE.NEXT)
		}
	],

	[
		ENUM_HOME_GUIDE.THIRD_STEP,
		{
			boryingPoint: () => boryingPointAction(ENUM_GUIDE_TYPE.BUY_NOW)
		}
	]
]);
