/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-19 10:41:46
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-05-08 10:34:15
 * @FilePath: /3fbox-page-mid/src/outs/components/goods/ModalHomeGuide/index.tsx
 * @Description: 首页新人指引
 */
import { Image, Dimensions, TouchableOpacity } from 'react-native';
import React, { useEffect, useState } from 'react';
import { useAtomValue, useSetAtom } from 'jotai';
import Mask from '@/outs/components/Mask';
import { useNavigation } from '@/outs/hooks/base/useNavigation';
import { getItem, setItem } from '@/outs/utils/tool/AsyncStorage';
import { atomTDAnalyticsStatus } from '@/outs/atom/atomTDAnalyticsStatus';
import { HOME_GUIDE, HOME_GUIDE_POINT } from '@/outs/const/config';
import { atomToken } from '@/outs/atom/atomToken';
import {
	ENUM_HOME_GUIDE,
	HOME_GUIDE_IMG_LOCAL,
	MAP_HOME_GUIDE
} from './config';

interface Props {
	endBack: () => void;
}

/** 首页新人指引 */
const HomeGuide = (props: Props) => {
	const navigation = useNavigation();
	const [isShowGuide, setIsShowGuide] = useState(false);
	/** 步骤 */
	const [step, setStep] = useState<ENUM_HOME_GUIDE>(
		ENUM_HOME_GUIDE.FIRST_STEP
	);
	const deviceWidth = Dimensions.get('window').width;
	const deviceHeight = Dimensions.get('window').height;
	const TDAnalyticsStatus = useAtomValue(atomTDAnalyticsStatus);
	const token = useAtomValue(atomToken);
	/** 新人指引第一步 */
	const onPressGuideFirst = () => {
		setStep(ENUM_HOME_GUIDE.SECOND_STEP);
	};

	/** 新人指引第二步 */
	const onPressGuideSecond = () => {
		setStep(ENUM_HOME_GUIDE.THIRD_STEP);
	};

	/** 新人引导第三部 */
	const onPressGuideThree = () => {
		setStep(ENUM_HOME_GUIDE.FOUR_STEP);
	};

	/** 新人指引第三步_结束 */
	const onPressGuideEnd = () => {
		setIsShowGuide(false);
		setItem(HOME_GUIDE, true);
		props.endBack();
		if (!token) {
			// return navigation.navigate('Login', { source: 'guide_page' });
		}
	};
	useEffect(() => {
		(async () => {
			/** 显示过新人指引 */
			const homeGuide = await getItem(HOME_GUIDE);
			if (!homeGuide) {
				setIsShowGuide(true);
				return;
			}
			props.endBack();
		})();
	}, []);

	useEffect(() => {
		/** 监听埋点初始化完成，触发指引埋点 */
		(async () => {
			const homeGuidePoint = await getItem(HOME_GUIDE_POINT);
			// console.log('TDAnalyticsStatus', TDAnalyticsStatus);

			if (TDAnalyticsStatus && !homeGuidePoint) {
				MAP_HOME_GUIDE.forEach((item) => {
					return item.boryingPoint();
				});
				setItem(HOME_GUIDE_POINT, true);
			}
		})();
	}, [TDAnalyticsStatus]);

	return (
		<Mask
			visible={isShowGuide}
			closeOnClickOverlay={false}
			overlayStyle={{
				backgroundColor:
					step === ENUM_HOME_GUIDE.FIRST_STEP
						? '#fff'
						: 'rgba(0, 0, 0, 0.78)'
			}}
		>
			{step === ENUM_HOME_GUIDE.FIRST_STEP && (
				<TouchableOpacity activeOpacity={1} onPress={onPressGuideFirst}>
					<Image
						source={HOME_GUIDE_IMG_LOCAL.HOME_GUIDE_1}
						style={{ width: deviceWidth, height: deviceHeight }}
					/>
				</TouchableOpacity>
			)}

			{step === ENUM_HOME_GUIDE.SECOND_STEP && (
				<TouchableOpacity
					activeOpacity={1}
					onPress={onPressGuideSecond}
				>
					<Image
						source={HOME_GUIDE_IMG_LOCAL.HOME_GUIDE_2}
						style={{ width: deviceWidth, height: deviceHeight }}
					/>
				</TouchableOpacity>
			)}

			{step === ENUM_HOME_GUIDE.THIRD_STEP && (
				<TouchableOpacity activeOpacity={1} onPress={onPressGuideThree}>
					<Image
						source={HOME_GUIDE_IMG_LOCAL.HOME_GUIDE_3}
						style={{ width: deviceWidth, height: deviceHeight }}
					/>
				</TouchableOpacity>
			)}
			{step === ENUM_HOME_GUIDE.FOUR_STEP && (
				<TouchableOpacity activeOpacity={1} onPress={onPressGuideEnd}>
					<Image
						source={HOME_GUIDE_IMG_LOCAL.HOME_GUIDE_4}
						style={{ width: deviceWidth, height: deviceHeight }}
					/>
				</TouchableOpacity>
			)}
		</Mask>
	);
};

export default HomeGuide;
