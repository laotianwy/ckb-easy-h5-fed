/*
 * @Author: yusha
 * @Date: 2023-12-18 13:28:35
 * @LastEditors: yusha
 * @LastEditTime: 2024-03-06 15:27:47
 * @Description: 首页取消订单弹窗
 */
import { useAsyncEffect, useRequest } from 'ahooks';
import { useAtomValue } from 'jotai';
import { memo, useCallback, useEffect, useState } from 'react';
import { FlatList, Image, Text, View } from 'react-native';
import * as SDK from '@snifftest/sdk/lib/rn';
import { tool } from '@/outs/utils/tool';
import { formatDateCN2JP } from '@/outs/utils';
import Mask from '@/outs/components/Mask';
import { OrderDTO } from '@/outs/service/easyOrder';
import { request } from '@/config/request/interceptors';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import Collapse from '@/outs/components/goods/Collapse';
import CollapseItem from '@/outs/components/goods/Collapseitem';
import Button from '@/outs/components/base/Button';
import { useNavWebViewH5 } from '@/outs/hooks/common/useNavWebViewH5';
import { usePageFocuEffect } from '@/outs/hooks/common/usePageFocuEffect';
import { styles } from './style';

const ModalOrderCancel = () => {
	const navWebViewH5 = useNavWebViewH5();
	const nav = SDK.useNav();
	const [visible, setVisible] = useState(false);
	const [reason, setReason] = useState('');
	const [cancenlOrderList, setCancenlOrderList] = useState<OrderDTO[]>([]);
	const useDetail = useAtomValue(atomUserDetail);
	const isLogin = !!useDetail?.customerId;
	/** 是否展示取消弹窗 */
	const { runAsync: showPopCancelApi } = useRequest(
		request.easyOrder.orderSearch.orderCancelPopups,
		{
			manual: true
		}
	);
	/** 取消的订单列表 */
	const { runAsync: lookOrderApi } = useRequest(
		request.easyOrder.orderSearch.orderCancelPopupsConfirm,
		{
			manual: true
		}
	);
	/** 查看订单 */
	const lookOrder = async () => {
		const res = await lookOrderApi({
			confirmType: 1
		});
		if (res.success) {
			setVisible(false);
			if (cancenlOrderList.length === 1) {
				navWebViewH5(
					`/order/detail?orderNo=${cancenlOrderList[0].orderNo}`
				);
				return;
			}
			navWebViewH5('/order/list');
		}
	};
	useAsyncEffect(async () => {
		if (!isLogin) {
			return;
		}

		const res = await showPopCancelApi();
		if (res.success) {
			setReason(res.data?.cancelReason || '');
			const cancelOrderListData = res.data?.orderList ?? [];
			setCancenlOrderList(cancelOrderListData);
			if (cancelOrderListData.length > 0) {
				setVisible(true);
			}
		}
	}, []);
	const initPage = useCallback(() => {
		(async () => {
			if (!isLogin) {
				return;
			}

			const res = await showPopCancelApi();
			if (!res.success) {
				return;
			}

			setReason(res.data?.cancelReason || '');
			const cancelOrderListData = res.data?.orderList ?? [];
			setCancenlOrderList(cancelOrderListData);
			if (cancelOrderListData.length > 0) {
				setVisible(true);
			}
		})();
	}, [isLogin, showPopCancelApi]);
	usePageFocuEffect(initPage);
	/** 稍后查看 */
	const laterBack = async () => {
		const res = await lookOrderApi({
			confirmType: 2
		});
		if (res.success) {
			setVisible(false);
		}
	};

	const getRenderItem = ({ item: orderItem }) => {
		// const orderItem = item;
		return (
			<View style={{ marginBottom: SDK.fitSize(5) }}>
				<Collapse accordion iconSize={12}>
					<CollapseItem
						name={String(orderItem.orderNo)}
						key={String(orderItem.orderNo)}
						title={tool.strings('订单编号')}
						subTitle={orderItem.orderNo}
					>
						<View style={styles.itemsContent}>
							<View style={styles.item}>
								<Text style={styles.title}>
									{tool.strings('下单时间')}
								</Text>
								<Text style={styles.desc}>
									{formatDateCN2JP(orderItem.createdTime)}
								</Text>
							</View>
							<View style={styles.item}>
								<Text style={styles.title}>
									{tool.strings('商品图片')}
								</Text>
								<View style={styles.imgs}>
									{(orderItem.orderItemList ?? []).map(
										(
											orderItemListItem: any,
											orderItemIndex: number
										) => {
											if (orderItemIndex > 2) {
												return null;
											}
											return (
												<View
													key={
														orderItemListItem.productImg
													}
													style={styles.imageCount}
												>
													<Image
														source={{
															uri: orderItemListItem.productImg
														}}
														style={styles.goodImg}
													/>

													<Text style={styles.count}>
														x
														{
															orderItemListItem.productQuantity
														}
													</Text>
												</View>
											);
										}
									)}
								</View>
							</View>
						</View>
					</CollapseItem>
				</Collapse>
			</View>
		);
	};
	return (
		<Mask zIndex={10000} closeOnClickOverlay={false} visible={visible}>
			<View style={styles.container}>
				<Image
					style={styles.mainImg}
					source={{
						uri: 'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/order-cancel.png'
					}}
				/>

				<View style={{ flex: 1 }}>
					<View style={styles.textStyle}>
						<Text style={styles.cancelText}>
							{tool.strings(
								'下列商品被取消，取消原因：{{reason}}',
								{
									reason
								}
							)}
						</Text>
						<Text style={styles.actionText}>
							{tool.strings('退款事宜可与客服进行联系确认')}
						</Text>
					</View>
					<View>
						{cancenlOrderList.length === 1 && (
							<View style={styles.content}>
								<View style={styles.item}>
									<Text style={styles.title}>
										{tool.strings('订单编号')}
									</Text>
									<Text style={styles.desc}>
										{cancenlOrderList[0]?.orderNo}
									</Text>
								</View>
								<View style={styles.item}>
									<Text style={styles.title}>
										{tool.strings('下单时间')}
									</Text>
									<Text style={styles.desc}>
										{formatDateCN2JP(
											cancenlOrderList[0].createdTime
										)}
									</Text>
								</View>
								<View style={styles.item}>
									<Text style={styles.title}>
										{tool.strings('商品图片')}
									</Text>
									<View>
										{(
											cancenlOrderList[0]
												?.orderItemList ?? []
										).map((item, index) => {
											if (index > 3) {
												return null;
											}
											return (
												<View
													key={item.productImg}
													style={styles.imageCount}
												>
													<Image
														source={{
															uri: item.productImg
														}}
														style={styles.goodImg}
													/>

													<Text style={styles.count}>
														x{item.productQuantity}
													</Text>
												</View>
											);
										})}
									</View>
								</View>
							</View>
						)}

						{cancenlOrderList.length > 1 && (
							<FlatList
								style={styles.manyOrder}
								data={cancenlOrderList}
								renderItem={getRenderItem}
							/>
						)}
					</View>
					<View
						style={{
							flexDirection: 'row',
							marginTop: SDK.fitSize(15),
							justifyContent: 'center',
							alignItems: 'center'
						}}
					>
						<Button
							onPress={laterBack}
							style={{
								marginRight: SDK.fitSize(15),
								borderColor: '#CDD2DA',
								paddingLeft: SDK.fitSize(8),
								paddingRight: SDK.fitSize(8)
							}}
						>
							<Text
								style={{ color: '#1C2026', fontWeight: '500' }}
							>
								{tool.strings('稍后查看')}
							</Text>
						</Button>
						<Button
							onPress={lookOrder}
							style={{
								backgroundColor: '#1C2026',
								padding: SDK.fitSize(8)
							}}
						>
							<Text style={{ color: '#fff', fontWeight: '500' }}>
								{tool.strings('立即查看')}
							</Text>
						</Button>
					</View>
				</View>
			</View>
		</Mask>
	);
};
/** 订单取消提示 */
export default memo(ModalOrderCancel);
