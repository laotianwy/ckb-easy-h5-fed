/*
 * @Author: yusha
 * @Date: 2023-12-18 13:33:35
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 10:02:16
 * @Description:
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: px2dp(319),
    height: px2dp(381),
    borderRadius: px2dp(10),
    margin: 'auto'
  },
  mainImg: {
    width: px2dp(319),
    height: px2dp(60),
    marginTop: px2dp(-38)
  },
  textStyle: {
    marginTop: px2dp(15),
    marginBottom: px2dp(15)
  },
  cancelText: {
    fontSize: px2dp(14),
    color: '#555',
    fontWeight: '400',
    lineHeight: px2dp(20),
    textAlign: 'center',
    marginBottom: px2dp(5)
  },
  actionText: {
    fontSize: px2dp(12),
    fontWeight: '300',
    color: '#555',
    lineHeight: px2dp(17),
    textAlign: 'center'
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: px2dp(10)
  },
  goodImg: {
    width: px2dp(60),
    height: px2dp(60),
    backgroundColor: '#fff'
  },
  content: {
    backgroundColor: '#F6F6F6',
    borderRadius: px2dp(8),
    marginLeft: px2dp(12),
    marginRight: px2dp(12),
    padding: px2dp(10)
  },
  title: {
    fontSize: px2dp(12),
    color: '#303438',
    fontWeight: '400'
  },
  desc: {
    fontSize: px2dp(12),
    color: '#777777',
    fontWeight: '300'
  },
  imageCount: {
    position: 'relative',
    width: px2dp(60),
    height: px2dp(60),
    marginRight: px2dp(4)
  },
  count: {
    minWidth: px2dp(20),
    height: px2dp(20),
    borderRadius: px2dp(20),
    backgroundColor: 'rgba(0,0,0,0.5)',
    // textAlign: 'center',
    position: 'absolute',
    right: px2dp(2),
    bottom: px2dp(2),
    fontSize: px2dp(10),
    fontWeight: '400',
    color: '#fff',
    paddingTop: px2dp(2),
    textAlign: 'center'
  },
  manyOrder: {
    width: '100%',
    height: px2dp(143),
    // marginTop: px2dp(15),
    // backgroundColor: '#fff',
    // backgroundColor: 'red',
    paddingLeft: px2dp(12),
    paddingRight: px2dp(12)
  },
  itemsContent: {
    backgroundColor: '#F6F6F6'
  },
  imgs: {
    display: 'flex',
    flexDirection: 'row'
  }
});