/*
 * @Author: yusha
 * @Date: 2024-01-08 16:34:34
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-16 10:02:52
 * @Description: 图片可预览组件
 */

import { memo } from 'react';
import { useToggle } from 'ahooks';
import React from 'react';
import { Image, XStack, YStack } from '@snifftest/tamagui';
import { ImageStyle, Modal, StyleProp, ViewStyle } from 'react-native';
import { changeImageCdn, px2dp } from '@/outs/utils';
import Mask from '../Mask';

interface ImagePreviewProps {
  /** 图片地址 */
  uri: string;
  /** 缩略图样式 */
  thumbnailStyle?: StyleProp<ViewStyle | ImageStyle>;
  size?: number;
}
const ImagePreview = (props: ImagePreviewProps) => {
  const { uri, thumbnailStyle = {}, size } = props;
  const [state, { toggle }] = useToggle(false);
  return (
    <>
			<XStack
        style={[
        {
          width: '100%',
          height: '100%'
        },
        thumbnailStyle]}

        onPress={() => toggle()}>

				<Image
          source={{ uri: changeImageCdn(uri, size) }}
          style={[
          {
            width: '100%',
            height: '100%'
          }, (
          thumbnailStyle as ImageStyle)]} />


			</XStack>
			<Modal
        transparent={true}
        visible={Boolean(state && uri)}
        presentationStyle={'overFullScreen'}
        onRequestClose={() => {
          toggle();
        }}
        statusBarTranslucent={true}>

				<YStack
          style={{
            backgroundColor: 'rgba(0,0,0,0.4)',
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            zIndex: 6,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }}
          onPress={() => toggle()}>

					<YStack
            style={{
              width: px2dp(375),
              height: px2dp(375),
              borderRadius: px2dp(10)
            }}>

						<Image
              style={{
                width: '100%',
                height: '100%'
              }}
              source={{ uri }} />

					</YStack>
				</YStack>
			</Modal>
		</>);

};
export default memo(ImagePreview);