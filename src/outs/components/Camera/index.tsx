/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-09-13 13:40:24
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-22 10:20:30
 * @FilePath: /ckb-mall-app/src/_deprecated/components/Camera/index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useRef, memo, forwardRef, useImperativeHandle } from 'react';
import {
  TouchableOpacity,
  PermissionsAndroid,
  StyleProp,
  Image,
  ImageStyle,
  ImageResizeMode,
  ImageSourcePropType,
  ViewStyle } from
'react-native';
import { uploadImage as directUploadImage } from '@/outs/utils/tool/Upload/directUpload';
import { px2dp } from '@/outs/utils';
import ImagePick from '@/outs/utils/tool/ImagePick';
import { tool } from '@/outs/utils/tool';
import ModalSelecter, { ModalSelecterRef } from '../ModalSelecter';
// import { ENUM_USER_ROUTE as ENUM_ROUTE } from '@/container/user/utils';

interface CameraProps {
  /** 获取base64回传数据函数 */
  onPressCamera: (base64: string, url: string) => void;
  /** 按钮的样式 */
  btnStyle?: StyleProp<ViewStyle>;
  /** 图片的样式 */
  imageStyle?: StyleProp<ImageStyle>;
  /** 展示模式 */
  resizeMode?: ImageResizeMode;
  /** 延迟时间 */
  fadeDuration?: number;
  /** 图片 url 或者 资源地址 */
  source?: ImageSourcePropType;
  /** 是否直采页面 */
  isDerect?: boolean;
}

const Camera = forwardRef(
  (
  {
    imageStyle = {},
    btnStyle = {},
    resizeMode,
    fadeDuration,
    source,
    isDerect,
    onPressCamera
  }: CameraProps,
  ref: any) =>
  {
    const modalSelecterRef = useRef<ModalSelecterRef>(null);

    useImperativeHandle(
      ref,
      () => ({
        chooseCamera: clickChooseCamera
      }),
      []
    );

    const getBase64 = () => {
      // 相机权限已同意
      const callBack = async (response: any) => {
        modalSelecterRef.current?.hide();
        if (response.length > 0) {
          const imageBase64 = response[0].base64;
          const imageFilrUri = response[0].uri;
          const fileName = response[0].fileName;
          // 直采的使用无token方式上传图片
          const res = await directUploadImage(
            {},
            { name: fileName, file: imageFilrUri }
          );
          const { url } = res;
          onPressCamera?.(imageBase64, url);
        }
      };

      modalSelecterRef.current?.show([
      {
        title: tool.strings('拍摄照片'),
        onPress: () => {
          ImagePick.launchCamera(callBack);
        }
      },
      {
        title: tool.strings('从相册选取'),
        onPress: () => {
          ImagePick.launchImageLibrary(callBack);
        }
      }]
      );
    };

    const clickChooseCamera = async () => {
      getImageBase64();
    };

    const getImageBase64 = async () => {
      try {
        if (!tool.isAndroid()) {
          getBase64();
          return;
        }
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: tool.strings('应用程序相机权限'),
            message: tool.strings('应用程序需要访问您的相机'),
            buttonNeutral: tool.strings('暂不设置'),
            buttonNegative: tool.strings('許可しない'),
            buttonPositive: tool.strings('許可')
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getBase64();
        } else {
          // 获取相机权限拒绝，请添加权限
          tool.Toast.message(
            tool.strings('获取相机权限拒绝，请添加权限')
          );
        }
      } catch (err) {
        console.warn(err, 'err');
      }
    };

    return (
      <>
				<ModalSelecter ref={modalSelecterRef} />
				<TouchableOpacity
          onPress={() => clickChooseCamera()}
          style={[
          {
            height: px2dp(44),
            justifyContent: 'center',
            alignItems: 'center',
            paddingRight: px2dp(5)
          },
          btnStyle]}>


					<Image
            style={[
            { height: px2dp(26), width: px2dp(26) },
            imageStyle]}

            resizeMode={resizeMode ? resizeMode : 'center'}
            fadeDuration={fadeDuration ?? 0}
            source={
            source ?? {
              uri: 'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_search_camera@3x.png'
            }} />


				</TouchableOpacity>
			</>);

  }
);

export default memo(Camera);