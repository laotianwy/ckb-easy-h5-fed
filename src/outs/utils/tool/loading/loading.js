import React from 'react';
import {
	ActivityIndicator,
	StyleSheet,
	Text,
	View,
	Dimensions
} from 'react-native';

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;
export default class Loading extends React.Component {
	constructor(props) {
		super(props);
		this.minShowingTime = 200;
		this.state = {
			isLoading: false,
			setIsLoading: (isLoading) => {
				if (isLoading != this.state.isLoading) {
					let curTimeLong = new Date().getTime();
					if (isLoading) {
						this.startTime = curTimeLong;
						this.setState({
							isLoading
						});
					} else {
						let hasShowingTimeLong = curTimeLong - this.startTime;
						if (hasShowingTimeLong < this.minShowingTime) {
							setTimeout(() => {
								this.setState({
									isLoading
								});
							}, this.minShowingTime - hasShowingTimeLong);
						} else {
							this.setState({
								isLoading
							});
						}
					}
				}
			}
		};
	}

	showLoading = () => {
		this.state.setIsLoading(true);
	};
	dismissLoading = () => {
		this.state.setIsLoading(false);
	};

	render() {
		if (!this.state.isLoading) {
			return null;
		}
		return (
			<View
				style={{
					flex: 1,
					width,
					height,
					position: 'absolute',
					backgroundColor: 'rgba(0,0,0,0)'
				}}
			>
				<View style={styles.loading}>
					<ActivityIndicator color="white" />
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	loading: {
		backgroundColor: '#10101099',
		height: 50,
		width: 50,
		borderRadius: 8,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		top: (height - 50) / 2,
		left: (width - 50) / 2
	},

	loadingTitle: {
		marginTop: 5,
		fontSize: 12,
		color: 'white'
	}
});
