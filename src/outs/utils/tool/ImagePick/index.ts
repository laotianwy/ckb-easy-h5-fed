/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-09-26 11:12:02
 * @LastEditors: shiguang
 * @LastEditTime: 2023-11-02 15:42:29
 * @FilePath: \theckbsniffmobile\App\tool\ImagePick\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import * as ImagePicker from 'react-native-image-picker';

const options: ImagePicker.CameraOptions = {
	mediaType: 'photo',
	includeBase64: true,
	includeExtra: false,
	saveToPhotos: false,
	quality: 0.1,
	maxWidth: 600,
	maxHeight: 600
};

export default {
	/** 拍摄照片 */
	launchCamera: async (
		success: (response: ImagePicker.Asset[] | undefined) => void
	) => {
		try {
			const response = await ImagePicker.launchCamera(options);
			if (
				response.didCancel ||
				response.errorCode ||
				response.errorMessage
			) {
				return;
			}
			success?.(response.assets);
		} catch (ex) {
			console.log(JSON.stringify(ex), 'sss');
		}
	},
	/** 从相册选取 */
	launchImageLibrary: async (
		success: (response: ImagePicker.Asset[] | undefined) => void
	) => {
		try {
			const response = await ImagePicker.launchImageLibrary(options);
			if (
				response.didCancel ||
				response.errorCode ||
				response.errorMessage
			) {
				return;
			}
			success?.(response.assets);
		} catch (ex) {
			console.log(JSON.stringify(ex), 'exx');
		}
	}
};
