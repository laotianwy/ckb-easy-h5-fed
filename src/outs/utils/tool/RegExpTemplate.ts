export const REGEXP_TEMPLATE = {
	/** 邮箱验证 */
	email: /^[a-zA-Z0-9]+([-_.][A-Za-zd]+)*@([a-zA-Z0-9]+[-.])+[A-Za-zd]{2,10}$/,
	/** 手机号验证 */
	phoneNumber: /^[\d]{6,20}$/,
	/** 手机和邮箱的验证码 */
	verificationCode: /^[\d\w]{4,8}$/,
	/** 用户名 */
	username:
		/^[\u4e00-\u9fa5|\u3040-\u309f|\u30a0-\u30ff|\u0800-\u4e00|\d|\w]{1,18}$/,
	/** 密码校验 */
	password: /^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9!@#$%^&*.]{6,32}$/
};
