/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-08 13:19:03
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 10:59:27
 * @FilePath: /ckb-mall-app/src/_deprecated/tool/Upload/directUpload.ts
 * @Description: 直采上传图片的方式
 *
 */
import { getEnv } from '@/config/request/env';
import { request } from '@/config/request/interceptors';

interface OSSDataType {
	dir: string;
	expire: string;
	host: string;
	accessId: string;
	policy: string;
	signature: string;
	/** 上传域名 */
	customDomain: string;
}

const isProduction = (() => {
	const env = getEnv();
	return env === 'prod';
	// if (window.location.port) {
	// 	return false;
	// }
	// const pre = window.location.hostname.split('.')[0];
	// return pre.startsWith('pre-') || !pre.includes('-');
})();

const bucketName = isProduction ? 'ckb-service-prod' : 'ckb-service-test';

const mockGetOSSData = async (path: string) => {
	// 这个接口有毒 需要这样传
	return (
		await request.customer.oss.signAnother({}, {
			path: `/oss/sign/another/withoutLogin?path=${path}&bucketName=${bucketName}`
			// path: `/oss/sign?path=${path}&bucketName=${bucketName}&type=0`
		} as any)
	).data;
};

const init = async () => {
	try {
		// 直营商城图搜
		const path = 'easyBuy/fileUpload/';
		const result = await mockGetOSSData(path);
		if (result) {
			return result as OSSDataType;
		}
	} catch (error: any) {
		console.log('上传失败');
	}
};

export const uploadImage = async (props: any, file: any) => {
	const suffix = file.name.slice(file.name.lastIndexOf('.'));
	const filename = Date.now() + suffix;
	const _OSSData = await init();
	const url = _OSSData?.dir + filename;

	const formdata = new FormData();
	formdata.append('name', file.file);
	formdata.append('OSSAccessKeyId', _OSSData!.accessId);
	formdata.append('policy', _OSSData!.policy);
	formdata.append('signature', _OSSData!.signature);
	formdata.append('key', url);
	formdata.append('x-oss-object-acl', 'public-read');
	formdata.append('success_action_status', '200');
	formdata.append('x-oss-forbid-overwrite', 'false');

	formdata.append('file', {
		uri: file.file,
		type: 'multipart/form-data',
		name: filename
	} as any);

	await fetch(_OSSData!.host, {
		method: 'post',
		mode: 'no-cors',
		body: formdata
	});
	console.log([_OSSData?.customDomain, '/', url].join(''), 'test');
	return {
		url: [_OSSData?.customDomain, '/', url].join('')
	};
};
