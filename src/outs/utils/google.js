/*
 * @Author: yusha
 * @Date: 2023-12-14 13:57:01
 * @LastEditors: huajian
 * @LastEditTime: 2024-01-12 18:25:16
 * @Description:
 */
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-11-17 16:52:51
 * @LastEditors: shiguang
 * @LastEditTime: 2023-12-03 11:50:32
 * @FilePath: /ckb-fbox-app/src/utils/google.js
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */

import {
	GoogleSignin,
	NativeModuleError,
	statusCodes
} from '@react-native-google-signin/google-signin';

GoogleSignin.configure({
	iosClientId:
		'523575224834-6uahvj5s0ra959ot5i7d5cao2b6glghb.apps.googleusercontent.com',
	webClientId:
		'523575224834-m4qq603v03q9lr692l6n62cq0mo1flgb.apps.googleusercontent.com',
	offlineAccess: true
});

export const signInGoogle = async () => {
	try {
		await GoogleSignin.hasPlayServices();
		const userInfo = await GoogleSignin.signIn();
		alert(JSON.stringify(userInfo));
	} catch (error) {
		console.log('ERROR-signInGoogle', error);
	}
};

export const googleLogin = async () => {
	try {
		if (GoogleSignin.isSignedIn()) {
			GoogleSignin.signOut();
		}
		await GoogleSignin.hasPlayServices();
		const userInfo = await GoogleSignin.signIn();
		const token = await GoogleSignin.getTokens();
		return [userInfo.user.id, userInfo.user.email];
	} catch (error) {
		if (error.code === statusCodes.SIGN_IN_CANCELLED) {
			console.log('用户取消了登录流程');
		} else if (error.code === statusCodes.IN_PROGRESS) {
			console.log('操作（例如登录）已在进行中');
		} else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
			console.log('登录超时');
		} else {
			// some other error happened
		}
	}
};
