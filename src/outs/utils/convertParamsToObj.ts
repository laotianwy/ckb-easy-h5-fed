/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-07 15:13:14
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2023-12-08 10:09:50
 * @FilePath: /ckb-mall-app/src/utils/convertParamsToObj.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
export const convertParamsToObj = (url: string) => {
	const paramsString = url.split('?')[1]; // 获取问号之后的参数字符串
	if (!paramsString) {
		return {};
	}
	const _paramsString = decodeURIComponent(paramsString);
	const paramsArray = _paramsString.split('&'); // 分割参数键值对数组

	const resultObj: { [key: string]: any } = {}; // 创建空对象

	paramsArray.forEach((param) => {
		const [key, value] = param.split('='); // 分割键值对
		resultObj[key] = value; // 存储到对象中
	});

	return resultObj;
};
