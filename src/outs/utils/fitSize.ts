/*
 * @Author: yusha
 * @Date: 2024-01-09 20:59:36
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-09 21:00:42
 * @Description:
 */
import { Dimensions } from 'react-native';

const deviceWidthDp = Dimensions.get('window').width;
const uiWidthPx = 750;

export const isH5 = () => {
	// todo
	return false;
};

// px 转 dp（设计稿中的 px 转 rn 中的 dp）
export const fitSize = (uiElePx: number): any => {
	if (isH5()) {
		return uiElePx / 100 + 'rem';
	}
	return ((uiElePx * deviceWidthDp) / uiWidthPx) * 2;
};
