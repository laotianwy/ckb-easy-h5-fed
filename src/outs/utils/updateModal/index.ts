/*
 * @Author: shiguang
 * @Date: 2024-01-23 15:04:33
 * @LastEditors: shiguang
 * @LastEditTime: 2024-01-23 17:56:06
 * @Description:
 */

import { Platform } from 'react-native';
import axios from 'axios';
import { getVersion } from '@/outs/hooks/base/useSetAuditVersion';

interface UpdateItem {
	/** 小于某个版本就更新，不包含这个版本 */
	lt: string;
	/** 更新某些版本 */
	includes: string[];
	/** 不某些版本 */
	excludes: string[];
	/** 更新类型，强制更新和提示更新 */
	updateType: 'forceUpdate' | 'tipUpdate';
}

interface UpdateInfo {
	/** 更新弹窗标题 */
	title: string;
	/** 更新说明文案 */
	desc: string;
	/** 取消更新文案 */
	cancelText: string;
	/** 确认更新文案 **/
	okText: string;
	/** 强制更新说明文案 */
	forceUpdateDesc?: string;
	/** 提醒更新说明文案 */
	tipUpdateDesc?: string;
}

/**
 * 更新配置项JSON
 */
interface UpdateModalConfigJSON extends UpdateInfo {
	android: {
		forceUpdate: UpdateItem;
		tipUpdate: UpdateItem;
	};
	ios: {
		forceUpdate: UpdateItem;
		tipUpdate: UpdateItem;
	};
}

const requestUpdateConfigJSON = async () => {
	// const data: UpdateModalConfigJSON = {
	// 	android: {
	// 		forceUpdate: {
	// 			lt: '',
	// 			includes: [],
	// 			excludes: [],
	// 			updateType: 'forceUpdate'
	// 		},
	// 		tipUpdate: {
	// 			lt: '',
	// 			includes: [],
	// 			excludes: [],
	// 			updateType: 'tipUpdate'
	// 		}
	// 	},
	// 	ios: {
	// 		forceUpdate: {
	// 			lt: '',
	// 			includes: [],
	// 			excludes: [],
	// 			updateType: 'forceUpdate'
	// 		},
	// 		tipUpdate: {
	// 			lt: '',
	// 			includes: [],
	// 			excludes: [],
	// 			updateType: 'tipUpdate'
	// 		}
	// 	}
	// };
	const res = await axios.get<UpdateModalConfigJSON>(
		`https://static-jp.theckb.com/static-asset/3fboxApp/config/derictAppUpdateModalConfig.json?x=${Math.random()}${Date.now()}`
	);
	return res.data;
	// return res;
	// return data;
};

const validateVersion = (str: string) => {
	if (typeof str !== 'string') {
		return false;
	}
	const versionArr = str.split('.');

	if (versionArr.length !== 3) {
		return false;
	}
	/** 校验都是数字 */
	const isNumber = (item: string) =>
		item.length ? !isNaN(Number(item)) : false;
	const isEveryNumber = versionArr.every(isNumber);
	if (isEveryNumber) {
		return true;
	}
	return false;
};

const compareGreaterThan = (largeVersion: string, smallVersion: string) => {
	const _smallVersion = smallVersion.split('.').map((item) => Number(item));
	const _largeVersion = largeVersion.split('.').map((item) => Number(item));
	for (let i = 0; smallVersion.length; i++) {
		if (_smallVersion[i] < _largeVersion[i]) {
			return true;
		}
		if (_smallVersion[i] > _largeVersion[i]) {
			return false;
		}
	}
	// 走到这里就说明相等啊
	return false;
};

enum ENUM_COMPARE_RESULT {
	LESS_THAN = 'LESS_THAN',
	EQUAL = 'EQUAL',
	GREATER_THAN = 'GREATER_THAN'
}

/**
 * 对比版本
 * @param versionA
 * @param versionB
 * @returns
 */
const compareVersion = (versionA: string, versionB: string) => {
	if (versionA === versionB) {
		return ENUM_COMPARE_RESULT.EQUAL;
	}
	const isAGreaterB = compareGreaterThan(versionA, versionB);
	if (isAGreaterB) {
		return ENUM_COMPARE_RESULT.GREATER_THAN;
	}
	return ENUM_COMPARE_RESULT.LESS_THAN;
};

console.log(compareVersion('1.1.0', '1.1.1'));

/**
 * 匹配更新
 * @param updateConfigItem
 * @param currentVersion
 * @returns
 */
const matchUpdate = (updateConfigItem: UpdateItem, currentVersion: string) => {
	if (
		updateConfigItem.lt &&
		compareVersion(currentVersion, updateConfigItem.lt) ===
			ENUM_COMPARE_RESULT.LESS_THAN
	) {
		return true;
	}
	return false;
};

const getUpdateConfig = (
	updateConfigJSON: UpdateModalConfigJSON,
	currentVersion: string
) => {
	const config =
		getPlatformOS() === 'android'
			? updateConfigJSON.android
			: updateConfigJSON.ios;
	const { forceUpdate, tipUpdate } = config;
	if (matchUpdate(forceUpdate, currentVersion)) {
		return {
			type: 'forceUpdate'
		} as const;
	}
	if (matchUpdate(tipUpdate, currentVersion)) {
		return {
			type: 'tipUpdate'
		} as const;
	}
};

const getPlatformOS = () => {
	if (Platform.OS === 'android') {
		return 'android';
	}
	return 'ios';
};

/**
 * 默认弹窗文案配置
 */
const defaultUpdateModalDataConfig = {
	/** 更新弹窗标题 */
	title: '最新バージョンに更新',
	/** 更新说明文案 */
	desc: 'より良いサービスを提供するために、最新バージョンのアップデートへ移行してください。',
	/** 取消更新文案 */
	cancelText: 'キャンセル',
	/** 确认更新文案 **/
	okText: 'アップデート'
};

export interface AppUpdateModalConfig
	extends Omit<UpdateInfo, 'forceUpdateDesc' | 'tipUpdateDesc'> {
	updateType: UpdateItem['updateType'];
}
/**
 * 获取 app 更新弹窗配置
 */
export const getAppUpdateModalConfig = async () => {
	const currentVersion = await getVersion();
	const updateConfigJSON = await requestUpdateConfigJSON();
	const updateModalConfig = getUpdateConfig(updateConfigJSON, currentVersion);
	if (!updateModalConfig) {
		return;
	}
	const modalConfig: AppUpdateModalConfig = {
		updateType: updateModalConfig.type,
		...defaultUpdateModalDataConfig
	};
	return modalConfig;
};
