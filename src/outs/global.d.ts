/*
 * @Author: shiguang
 * @Date: 2023-04-26 10:20:06
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-27 21:27:31
 * @Description: 全局变量
 */
import { Lang } from './i18n/i18n';

interface Mprops {
	t: (text: string, options: { data?: Record<string, any> } = {}) => string;
	zh_CN: Record<string, Record<string, string>>;
	ja_JP: Record<string, Record<string, string>>;
	ko_KR: Record<string, Record<string, string>>;
	en_GB: Record<string, Record<string, string>>;
}

declare global {
	export interface Window {
		ReactNativeWebView?: boolean;
		g_locales: {
			zh_CN?: Record<string, Record<string, string>>;
			ja_JP?: Record<string, Record<string, string>>;
			ko_KR?: Record<string, Record<string, string>>;
			en_GB?: Record<string, Record<string, string>>;
		};
		_$m: Mprops;
	}
}

export {};
