/*
 * @Author: yusha
 * @Date: 2024-01-10 16:55:36
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-10 16:55:58
 * @Description: 购物车数量
 */
import { atom } from 'jotai';

export const atomCartCount = atom<number>(0);
