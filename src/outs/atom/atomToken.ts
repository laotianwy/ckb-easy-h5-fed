/*
 * @Author: yusha
 * @Date: 2023-12-12 19:34:18
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-12 19:34:19
 * @Description:
 */
import { atom } from 'jotai';

export const atomToken = atom<string | undefined>(undefined);
