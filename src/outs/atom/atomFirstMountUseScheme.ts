/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-02-26 18:25:10
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-02-27 15:51:57
 * @FilePath: /ckb-fbox-app/src/atom/atomFirstMountUseScheme.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { atom } from 'jotai';

/** 初始化是否使用scheme跳转 */
export const atomFirstMountUseScheme = atom<boolean>(false);
