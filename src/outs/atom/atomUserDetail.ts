/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2023-11-13 14:52:21
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-08 15:50:06
 * @FilePath: /ckb-fbox-app/src/atom/atomUserDetail.ts
 * @Description: 公共数据/用户信息
 */
import { atom } from 'jotai';
import { getItem, setItem } from '@/outs/utils/tool/AsyncStorage';
import { ENUM_SYSTEMSOURCE } from '@/outs/const';
import { request } from '@/config/request/interceptors';
import { CustomerDetailRespDTO } from '@/outs/service/customer';
import { ACCESSTOKEN } from '@/outs/const/config';
import { atomToken } from './atomToken';

/** 用户会员信息 */
export interface CustomerMembershipResDTO {
	/** @format int32 */
	basicFlag?: number;
	/** @format date-time */
	createTime?: string;
	/** @format int64 */
	customerMembershipId?: number;
	/** @format int32 */
	customerMembershipStatus?: number;
	customerName?: string;
	discountActualPrice?: number;
	/** @format date-time */
	expiresDate?: string;
	/** @format int64 */
	membershipTemplateId?: number;
	membershipTemplateName?: string;
	/** @format int64 */
	membershipTemplatePriceId?: number;
	membershipTemplatePriceName?: string;
	sellingPrice?: number;
	stationCode?: string;
	/** @format int64 */
	superCustomerId?: number;
	/** @format date-time */
	updateTime?: string;
	templateLevel: number;
}

export interface CustomerShopRespDTO {
	/** @format int64 */
	abnormalOrderCount?: number;
	/** @format int32 */
	activeSubAccountFlag?: number;
	/** @format int64 */
	apiOrderCount?: number;
	/** @format int32 */
	apiOrderStatusFailCount?: number;
	/** @format int32 */
	authExpireStatus?: number;
	/** @format int64 */
	authStatus?: number;
	availableAmount?: number;
	/** @format int32 */
	badAmount?: number;
	/** @format int64 */
	canDeliveryOrderCount?: number;
	/** @format int32 */
	canceledCount?: number;
	clientId?: string;
	clientSecret?: string;
	/** @format int64 */
	completeOrderCount?: number;
	/** @format date-time */
	createTime?: string;
	/** @format int64 */
	customerId?: number;
	customerName?: string;
	/** @format int32 */
	customerShopAuthStatus?: number;
	/** @format int32 */
	customerShopAutoOrderFlag?: number;
	/** @format int64 */
	customerShopId?: number;
	customerShopName?: string;
	customerShopPermission?: string;
	/** @format int32 */
	customerShopPlatform?: number;
	customerShopUrl?: string;
	/** @format int64 */
	dealOrderCount?: number;
	/** @format int32 */
	deletedFlag?: number;
	/** @format int32 */
	flAmount?: number;
	/** @format int32 */
	isExistOem?: number;
	loginName?: string;
	/** @format int64 */
	mainCustomerId?: number;
	managerIds?: string[];
	/** @format int32 */
	noBindAmazonSkuCount?: number;
	/** @format int32 */
	noBindApiSkuCount?: number;
	/** @format int64 */
	noCompleteOrderCount?: number;
	/** @format int32 */
	noConfirmReplenishPlanCount?: number;
	/** @format int64 */
	nonDoneRechargeCount?: number;
	orderAmount?: number;
	/** @format int32 */
	orderCount?: number;
	/** @format int64 */
	partDeliveryOrderCount?: number;
	/** @format int32 */
	pendingInventoryCount?: number;
	/** @format int32 */
	productAmount?: number;
	/** @format int32 */
	quotedOem?: number;
	/** @format int32 */
	rePurchaseOem?: number;
	/** @format int32 */
	searchSuccessOem?: number;
	/** @format int64 */
	shopPlatform?: number;
	stationCode?: string;
	/** @format int32 */
	systemSource?: number;
	/** @format int32 */
	toBeQuotedOem?: number;
	unificationCustomerFullName?: string;
	/** @format int32 */
	virtualFlag?: number;
	/** @format int32 */
	waitConfirm?: number;
	/** @format int64 */
	waitPayOrderCount?: number;
	/** @format int32 */
	waitedOem?: number;
	wareCode?: string;
	wareName?: string;
	wareNameLangList?: WareNameLangDTO[];
	/** @format int32 */
	yesterdayOrderQuantity?: number;
	/** @format int32 */
	yesterdayShipmentsQuantity?: number;
}

export interface WareNameLangDTO {
	lang?: string;
	wareName?: string;
}

/** 用户信息 */
export const atomUserDetail = atom<CustomerDetailRespDTO | undefined>(
	undefined
);

/** 获取用户信息 */
export const atomRequestUserDetail = atom(
	(get) => get(atomUserDetail),
	async (
		_get,
		set,
		options?: { isSetDefautShop: boolean; token?: string }
	) => {
		const { token } = options ?? {};
		const accessToken: string = token ? token : await getItem(ACCESSTOKEN);
		if (token) {
			await setItem(ACCESSTOKEN, token);
			global.GLOBAL_TOKEN = token;
		}
		if (!accessToken) {
			return;
		}
		// 用户初始化用户信息的时候可能内存中没有 token, 这里需要设置一下 atomToken
		set(atomToken as any, accessToken);
		// const HOMESCREEN_TABVIEWFIRST_GETDETAILS = '/customer/getCustomerDetails'; //用户详情
		const res =
			await request.customer.getCustomerDetails.getCustomerDetails({
				token: accessToken
			} as any);
		const userDetail = res?.data;
		set(atomUserDetail as any, userDetail);
		return userDetail;
	}
);
