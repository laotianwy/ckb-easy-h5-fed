/*
 * @Author: yusha
 * @Date: 2024-01-05 11:34:38
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-05 11:44:08
 * @Description: 消息未读数量
 */
import { atom } from 'jotai';

export const atomUnreadCount = atom<number>(0);
