/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-07 18:32:58
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2023-12-07 18:33:09
 * @FilePath: /ckb-mall-app/src/atom/atomDrawer.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { atom } from 'jotai';

export const atomDrawerFlag = atom<boolean>(false);
