/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-11-22 16:19:34
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-14 10:43:01
 * @FilePath: /theckbsniffmobile/App/atoms/atomThirdLogin.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */

import { atom } from 'jotai';
import { Plat } from '@/outs/const';

export interface ThirdLoginProps {
	/** 平台类型 */
	thirdPlatform?: Plat;
	/** 平台的用户id */
	thirdCustomerId?: string;
	/** 邮箱 */
	thirdEmail?: string;
}

export const atomThirdLogin = atom<ThirdLoginProps>({});
