/*
 * @Author: shiguang
 * @Date: 2023-10-07 15:44:35
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2023-12-12 17:18:58
 * @Description: 存放全局购物车相关数据
 */
import { atom } from 'jotai';
import { CustomerShopRespDTO } from '@/outs/service/customer';

export interface AtomCartData {
	/** 徽标展示的购物车数量 */
	cartNum?: string;
}

/** 店铺信息 */
export const atomShopDetail = atom<CustomerShopRespDTO | undefined>(undefined);
