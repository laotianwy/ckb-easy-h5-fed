import { atom } from 'jotai';

/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-22 17:42:40
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-01-22 20:42:22
 * @FilePath: /ckb-fbox-app/src/atom/atomTDAnalyticsStatus.ts
 * @Description: 埋点初始化状态
 */

/** 埋点初始化状态 */
export const atomTDAnalyticsStatus = atom<boolean>(false);
