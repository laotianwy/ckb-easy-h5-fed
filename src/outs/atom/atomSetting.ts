/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-19 09:53:13
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2023-12-19 09:54:40
 * @FilePath: /ckb-fbox-app/src/atom/atomSetting.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { atom } from 'jotai';

/** 是否审核版本 true-是 false-不是 */
export const isAuditVersion = atom(false);
