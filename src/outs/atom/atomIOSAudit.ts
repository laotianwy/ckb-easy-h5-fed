/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2023-12-22 11:51:54
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2023-12-25 10:02:22
 * @FilePath: /ckb-mall-app/src/atom/atomIOSAudit.ts
 * @Description: ios审核版本状态管理
 */
import { atom } from 'jotai';

export enum ENUM_IOS_AUDIT_VERSION {
	/** 是IOS审核版本 */
	IS_IOS_AUDIT_VERSION = 'IS_IOS_AUDIT_VERSION',
	/** 不是IOS审核版本 */
	NOT_IOS_AUDIT_VERSION = 'NOT_IOS_AUDIT_VERSION',
	/** 请求中，或者没有被用户授权使用网络 */
	IS_UNKNOW = 'IS_UNKNOW'
}

/** ios审核版本状态管理 */
export const atomIOSAuditStatus = atom(ENUM_IOS_AUDIT_VERSION.IS_UNKNOW);
