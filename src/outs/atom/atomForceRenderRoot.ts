/*
 * @Author: shiguang
 * @Date: 2023-11-06 11:48:10
 * @LastEditors: shiguang
 * @LastEditTime: 2023-11-08 17:15:45
 * @Description: atomForceRenderRootKey
 */
import { atom } from 'jotai';

export const atomForceRenderRootKey = atom(0);
export const atomForceRenderRoot = atom(
	(get) => get(atomForceRenderRootKey),
	async (_get, set) => {
		set(atomForceRenderRootKey, (key) => key + 1);
	}
);
