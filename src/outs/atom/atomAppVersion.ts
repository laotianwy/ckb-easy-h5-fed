/*
 * @Author: yusha
 * @Date: 2024-01-10 16:55:36
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-23 21:20:07
 * @Description: 购物车数量
 */
import { atom } from 'jotai';

export const atomCurrentAppVersion = atom<string>('');
