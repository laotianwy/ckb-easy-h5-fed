import { atom } from 'jotai';

interface atomAnalyzeOrignDataProps {
	campaign_id: string;
	remote_campaign_id: string;
	campaign_name: string;
	site_id: string;
	advertising_id: string;
	ad_network: string;
	creative_name: string;
}

export type PartialAnalyzeOrignDataProps = Partial<atomAnalyzeOrignDataProps>;

/** 获取tenjin的来源信息和设备id */
export const atomAnalyzeOrignData = atom<PartialAnalyzeOrignDataProps | null>(
	null
);
