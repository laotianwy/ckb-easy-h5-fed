/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-18 16:15:46
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-11 20:59:20
 * @FilePath: /3fbox-page-mid/src/outs/hooks/base/useFaceBookInit/index.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect } from 'react';
import { Settings } from 'react-native-fbsdk-next';
import { Platform } from 'react-native';

export const useFaceBookInit = () => {
	useEffect(() => {
		(async () => {
			if (Platform.OS === 'ios') {
				try {
					await Settings.setAdvertiserTrackingEnabled(true);
					Settings.initializeSDK();
				} catch (ex) {
					console.log('exxx');
				}
			} else {
				Settings.setAppID('1073985013656633');
				await Settings.setAdvertiserTrackingEnabled(true);
				Settings.initializeSDK();
			}
		})();
	}, []);
};
