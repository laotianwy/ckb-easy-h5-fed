/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-07 16:05:40
 * @LastEditors: yusha
 * @LastEditTime: 2024-01-22 10:21:29
 * @FilePath: /ckb-fbox-app/src/hooks/base/use2dOr2c/index.ts
 * @Description: 页面渲染前
 * 侧边栏切， schema 切，h5跳转 app 切
 * 判断当前用户是否能访问当前页面
 * （tob toc tod）
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect, useState } from 'react';
import { useSetAuditVersion } from '../useSetAuditVersion';

/** 已废弃 */
/** 获取当前用户是 2d、2b、2c */
export const use2dOr2c = () => {
  const setAuditFunc = useSetAuditVersion();
  const [appIsShow, setAppIsShow] = useState(false);

  useEffect(() => {
    (async () => {
      // ：接入直采。这里需要改
      /** 获取app的2b、2c方式 */
      await setAuditFunc();
      /** 获取当前是否是审核版本 */
      setAppIsShow(true);
    })();
  }, []);
  return [appIsShow];
};