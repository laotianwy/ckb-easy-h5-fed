/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-19 10:00:57
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2023-12-22 14:56:47
 * @FilePath: /ckb-fbox-app/src/hooks/base/useSetAuditVersion/index.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { useSetAtom } from 'jotai';
import { NativeModules, Platform } from 'react-native';
import { useEffect } from 'react';
import axios from 'axios';
import { useAsyncEffect } from 'ahooks';
import { isAuditVersion } from '@/outs/atom/atomSetting';
import { isAndroid } from '@/outs/utils/tool/utils';

/** ios 审核 */
export interface IOSAuditData {
  description: string;
  key: 'IOS_AUDIT';
  data: {
    iosVersion: string;
    androidVersion: string;
  };
}

type SettingsData = [IOSAuditData];
const curGlobal = (global as {
  IS_AUDIT_VERSION?: boolean;
});

/**
 * 获取当前版本
 */
export const getVersion = () => {
  return new Promise<string>((resolve, reject) => {
    if (isAndroid()) {
      NativeModules.BridgeManager.getAppVersion((event) => {
        // this.setState({
        // 	version: event
        // });
        resolve(event);
      });
    } else {
      NativeModules.ToolModule.getAppVersion(
        (error: any, version: string) => {
          if (error) {
            reject(error);
          } else {
            resolve(version);
          }
        }
      );
    }
  });
};

const getIsAuditVersion = async () => {
  try {
    const currentVersion = await getVersion();
    const res = await axios.get<SettingsData>(
      `https://static-s.theckb.com/app/config/direct-setting.json?x=${Math.random()}`
    );
    if (!currentVersion) {
      return false;
    }
    const item = res.data.find((item) => item.key === 'IOS_AUDIT');
    if (isAndroid()) {
      if (!item || !item.data.androidVersion) {
        return false;
      }
    } else {
      if (!item || !item.data.iosVersion) {
        return false;
      }
    }
    // 当前版本是 ios 审核版本的时候
    return isAndroid() ?
    item.data.androidVersion === currentVersion :
    item.data.iosVersion === currentVersion;
  } catch (error) {
    return false;
  }
};

export const useSetAuditVersion = () => {
  const setIsAuditVersion = useSetAtom(isAuditVersion);
  return async () => {
    /** 是审核 */
    const IS_AUDIT_VERSION =
    curGlobal.IS_AUDIT_VERSION === undefined ?
    await getIsAuditVersion() :
    curGlobal.IS_AUDIT_VERSION;
    curGlobal.IS_AUDIT_VERSION = IS_AUDIT_VERSION;
    setIsAuditVersion(IS_AUDIT_VERSION);
  };
};