/*
 * @Author: shiguang
 * @Date: 2023-09-14 10:35:16
 * @LastEditors: shiguang
 * @LastEditTime: 2023-09-14 10:35:18
 * @Description:
 */
import { Dispatch, SetStateAction, useCallback, useState } from 'react';

interface UseBooleanOutput {
  value: boolean;
  setValue: Dispatch<SetStateAction<boolean>>;
  setTrue: () => void;
  setFalse: () => void;
  toggle: () => void;
}

export function useBoolean(defaultValue?: boolean): UseBooleanOutput {
  const [value, setValue] = useState(!!defaultValue);

  const setTrue = useCallback(() => setValue(true), []);
  const setFalse = useCallback(() => setValue(false), []);
  const toggle = useCallback(() => setValue((x) => !x), []);

  return { value, setValue, setTrue, setFalse, toggle };
}