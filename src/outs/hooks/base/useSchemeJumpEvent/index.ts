/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-07 15:15:14
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-08 17:15:33
 * @FilePath: /ckb-fbox-app/src/hooks/base/useSchemeJumpEvent/index.ts
 * @Description: h5 scheme 跳转
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */

import { Alert, EmitterSubscription, Linking } from 'react-native';
import { useEffect } from 'react';
import { CommonActions, useNavigation } from '@react-navigation/native';
import { useAtom, useSetAtom } from 'jotai';
import { atomFirstMountUseScheme } from '@/outs/atom/atomFirstMountUseScheme';
import { convertParamsToObj } from '@/outs/utils';
/** TODO 处理传值 */
import {
  atomUpdateGlobalWebview,
  atomWebViewPageState } from
'@/container/user/WebviewBridgeH5/atom/atomWebview';
import useEvent from '../useEvent';

/** h5 通过链接跳转到App 页面 */
export const useSchemeJumpEvent = () => {
  /** 跳转 */
  const navigation = useNavigation();
  /** 强制更新全局的webview组件 */
  const setGlobalWebViewKeyFunc = useSetAtom(atomUpdateGlobalWebview);
  /** 设置webview组件 是否显示和默认的url */
  const setWebViewPageState = useSetAtom(atomWebViewPageState);

  /** app首次启动是否触发过 初始的url跳转 */
  const [firstMountUseScheme, setFirstMountUseScheme] = useAtom(
    atomFirstMountUseScheme
  );
  const jumpToPage = useEvent((name: string, params: any) => {
    setWebViewPageState({ isShow: false, url: '' });
    setGlobalWebViewKeyFunc();
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{ name: 'DirectIndex' }, { name, params }]
      })
    );
  });

  useEffect(() => {
    const getH5Url = (url: string) => {
      if (url.includes('com.theckb.fbox://')) {
        const pagePath = url.split('://')[1].split('?')[0];
        const params = convertParamsToObj(url);
        setTimeout(() => {
          /** 跳转到商品详情 */
          jumpToPage(pagePath, params);
        }, 1000);
      } else if (url.includes('m.3fbox.com/goods/detail')) {
        // 此处是iOS通用链接 universal link 使用的场景
        // https://pre-m.3fbox.com/goods/detail?productCode=AM-559298135859
        const params = convertParamsToObj(url);
        setTimeout(() => {
          /** 跳转到商品详情 */
          jumpToPage('GoodDetail', params);
        }, 1000);
      }
    };
    const _handleAppStateChange = ({ url }: {url: string;}) => {
      if (url) {
        getH5Url(url);
      }
    };

    // 初始化获取从h5到App的参数
    Linking.getInitialURL().
    then((url) => {
      if (firstMountUseScheme) {
        return;
      }
      if (url) {
        getH5Url(url);
        setFirstMountUseScheme(true);
      }
    }).
    catch(() => {});
    let urlAddEventListenerId: EmitterSubscription | undefined;
    setTimeout(() => {
      urlAddEventListenerId = Linking.addEventListener(
        'url',
        _handleAppStateChange
      );
    }, 1000);

    return () => {
      if (urlAddEventListenerId) {
        urlAddEventListenerId.remove();
      }
    };
  }, [jumpToPage, setFirstMountUseScheme, firstMountUseScheme]);
};