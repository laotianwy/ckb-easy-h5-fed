/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-18 10:38:49
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-13 17:28:55
 * @FilePath: /ckb-fbox-app/src/hooks/base/useChannel/index.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { useCallback, useEffect, useState } from 'react';
import { BootConfig, ChannelIO } from 'react-native-channel-plugin';
import { useAtomValue } from 'jotai';
import { atomRequestUserDetail } from '@/outs/atom/atomUserDetail';

/** 初始化客服系统 */
export const useChannel = () => {
	/** 用户信息 */
	const userDetail = useAtomValue(atomRequestUserDetail);
	const [badgeCount, setBadgeCount] = useState(0);
	const [isBoot, setIsBoot] = useState(false);

	/** 初始化ChannelIO */
	const initChannelIO = useCallback(() => {
		let config: BootConfig = {
			pluginKey: '6ada065d-08b5-4f42-9131-302ff4e4e772', // 直采
			// 初步添加客服。但是现在的key是代采的key。需要更改
			// pluginKey: '9ef7076e-b606-45f8-bb06-ec8b98b5fc7f', // 代采
			profile: {
				systemName: '',
				systemEmail: '',
				templateName: '',
				systemMemberId: '',
				name: '',
				email: ''
			}
		};
		if (userDetail) {
			config = {
				...config,
				memberId: userDetail.customerId?.toString() as string,
				profile: {
					systemName: userDetail.loginName as string,
					systemEmail: userDetail.customerEmail as string,
					// eslint-disable-next-line prettier/prettier
					templateName: userDetail?.membership?.membershipTemplateName as string,
					systemMemberId: userDetail.customerId?.toString() as string,
					name: userDetail?.loginName as string,
					email: userDetail?.customerEmail as string
				}
			};
		}

		/** 开机 */
		ChannelIO.boot(config).then((result) => {
			if (result) {
				/** 初始化会造成延迟，通过该状态控制客服按钮显示 */
				setIsBoot(true);
				ChannelIO.hideChannelButton(); // 隐藏客服默认聊天框
			}
		});
		// 获取未读消息数量
		ChannelIO.onBadgeChanged((count) => {
			setBadgeCount(count);
		});
	}, [userDetail]);

	useEffect(() => {
		initChannelIO();
	}, [initChannelIO]);
};

/** 显示客服聊天弹窗 */
export const showMessenger = () => {
	ChannelIO.updateUser({ language: 'jp' });
	ChannelIO.showMessenger();
};
