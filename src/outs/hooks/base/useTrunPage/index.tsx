/*
 * @Author: huajian
 * @Date: 2023-11-23 11:44:47
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-01-23 11:25:07
 * @Description: 列表翻页hook
 */

import React, { useCallback, useEffect, useRef, useState } from 'react';
import { FlatList, FlatListProps, RefreshControl } from 'react-native';
import { tool } from '@/outs/utils/tool';

interface Props extends Omit<FlatListProps<any>, 'data'> {
  request: any;
  pageSize?: number;
  /** 请求的参数 pageNum不用传 */
  params: any;
  useMock?: boolean;
}

export function useTrunPage<T>(props: Props) {
  const pageNumRef = useRef(1);
  const scrollRef = useRef(null);
  /** 是否滚动，滚动距离 */
  const [scrollHeight, setScrollHeight] = useState<number>(0);
  const [records, setRecords] = useState<T[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [params, setParams] = useState(props.params);
  const [response, setRespons] = useState(props.params);
  const [isEnd, setIsEnd] = useState(false);
  const pageSize = props.pageSize || 20;
  const propsRequest = props.request;
  const request = useCallback(async () => {
    if (!params) {
      return;
    }
    setIsLoading(true);
    const res = await propsRequest(
      {
        ...params,
        pageNum: pageNumRef.current,
        pageSize
      },
      {
        useMock: props.useMock
      }
    );
    setIsLoading(false);
    if (res.code !== '0') {
      return;
    }
    setRespons(res);
    if (res.code !== '0') {
      return;
    }
    if (!res?.data?.records || res?.data?.records?.length < pageSize) {
      setIsEnd(true);
    }
    if (pageNumRef.current === 1) {
      pageNumRef.current++;
      setRecords(res.data.records || []);
    } else {
      pageNumRef.current++;
      setRecords((val) => {
        //
        return val.length >= pageSize ?
        [...val, ...(res.data.records || [])] :
        [...(res.data.records || [])];
      });
    }
  }, [params, propsRequest, pageSize, props.useMock]);
  const onRefresh = useCallback(() => {
    pageNumRef.current = 1;
    request();
  }, [request]);
  useEffect(onRefresh, [onRefresh]);
  const RenderTrunPage =
  <FlatList
    nestedScrollEnabled={true}
    ref={scrollRef}
    onScroll={(event) => {
      const { contentOffset } = event.nativeEvent;
      // 大于0代表滚动了
      // const value = contentOffset.y > 0 ? true : false;
      setScrollHeight(contentOffset.y);
    }}
    showsVerticalScrollIndicator={false}
    refreshControl={
    <RefreshControl
      refreshing={isLoading}
      onRefresh={() =>
      // 当不是loading状态，且当前数量大于pageSize的数量，
      !isLoading && records?.length > pageSize && onRefresh()}

      enabled={true}
      title={tool.strings('加载更多商品中，请稍后')}
      tintColor={tool.defColor()}
      colors={['#60B161']} />}


    onEndReached={() => {
      if (isLoading || isEnd) {
        return;
      }
      request();
    }}
    {...props}
    data={records} />;



  return ([
  RenderTrunPage,
  (params: any) => {
    setIsEnd(false);
    setParams(params);
  },
  {
    records,
    setRecords,
    pageNum: pageNumRef.current,
    pageSize,
    onRefresh,
    isLoading,
    response,
    pageNumRef,
    scrollRef,
    scrollHeight
  }] as
  const);
}