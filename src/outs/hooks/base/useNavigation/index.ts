/*
 * @Author: shiguang
 * @Date: 2023-09-15 15:43:12
 * @LastEditors: shiguang
 * @LastEditTime: 2023-09-16 14:10:38
 * @Description:
 */
import { useNavigation as useNativeNavigation } from '@react-navigation/native';
// import { StackNavigation } from '../../page/route';

export const useNavigation = <T = any,>() => {
  const navigation = useNativeNavigation<T>();
  return navigation;
};