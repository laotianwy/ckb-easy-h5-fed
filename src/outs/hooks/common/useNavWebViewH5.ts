/*
 * @Author: huajian
 * @Date: 2024-02-29 16:51:03
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-29 17:56:10
 * @Description:
 */
import * as SDK from '@snifftest/sdk/lib/rn';
import {
	useNavigationWebViewH5,
	INCLUDE_DOMAIN as include_domain
} from '@/outs/hooks/common/useNavigationWebViewH5';
export const INCLUDE_DOMAIN = include_domain;
export const useNavWebViewH5 = () => {
	const nav = SDK.useNav();
	const navigationWebViewH5 = useNavigationWebViewH5();
	return (url: string) => {
		if (SDK.isH5()) {
			nav({ path: url });
			return;
		}
		navigationWebViewH5(url);
	};
};
