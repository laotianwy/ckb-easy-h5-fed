/*
 * @Author: shiguang
 * @Date: 2024-01-23 17:22:15
 * @LastEditors: shiguang
 * @LastEditTime: 2024-01-23 17:49:29
 * @Description:
 */

import { useEffect, useState } from 'react';
import {
  AppUpdateModalConfig,
  getAppUpdateModalConfig } from
'@/outs/utils/updateModal';

const useAppUpdateModal = () => {
  const [appUpdateModalConfig, setAppUpdateModalConfig] =
  useState<AppUpdateModalConfig>();
  useEffect(() => {
    getAppUpdateModalConfig().then((res) => {
      setAppUpdateModalConfig(res);
    });
  }, []);
  return ([appUpdateModalConfig, setAppUpdateModalConfig] as const);
};

export default useAppUpdateModal;