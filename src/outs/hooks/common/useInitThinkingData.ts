/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-04 14:09:37
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-01-23 11:24:47
 * @FilePath: /ckb-fbox-app/src/hooks/common/useInitThinkingData.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import TDAnalytics from '@snifftest/react-native-thinking-data';
import { useAtomValue, useSetAtom } from 'jotai';
import { useEffect } from 'react';
import { atomAnalyzeOrignData } from '@/outs/atom/atomAnalyzeOrignData';
import { atomTDAnalyticsStatus } from '@/outs/atom/atomTDAnalyticsStatus';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import { initThinkingData } from '@/outs/utils/TDAnalytics';

export enum EnumProductPurchaseType {
  /** 直采 */
  DIRECT,
  /** 代采 */
  PROXY,
}

export const useInitThinkingData = () => {
  const analyzeData = useAtomValue(atomAnalyzeOrignData);
  const userInfo = useAtomValue(atomUserDetail);
  /** 设置埋点初始化状态 */
  const setTDAnalyticsStatus = useSetAtom(atomTDAnalyticsStatus);

  useEffect(() => {
    if (!analyzeData) {
      return;
    }
    initThinkingData(analyzeData);

    setTDAnalyticsStatus(true);
  }, [analyzeData, setTDAnalyticsStatus]);

  useEffect(() => {
    if (!userInfo || !analyzeData) {
      return;
    }
    TDAnalytics.userSet({
      bu_type: EnumProductPurchaseType.DIRECT,
      customer_id: userInfo?.customerId,
      customer_name: userInfo?.customerName,
      station_code: userInfo?.stationCode,
      platform_type: 2,
      system_source: userInfo?.systemSource,
      internal_flag: userInfo.internalFlag,
      ...(analyzeData ?? {})
    });
  }, [analyzeData, userInfo]);
};