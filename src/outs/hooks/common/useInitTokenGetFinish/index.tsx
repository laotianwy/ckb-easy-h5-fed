/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-20 10:58:41
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-01 13:16:09
 * @FilePath: /ckb-fbox-app/src/hooks/common/useInitTokenGetFinish/index.tsx
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect, useState } from 'react';
import { useAsyncEffect } from 'ahooks';
import * as SDK from '@snifftest/sdk/lib/rn';
import { ACCESSTOKEN } from '@/outs/const/config';
import { getItem } from '@/outs/utils/tool/AsyncStorage';

/** 初始化获取token 是否获取完毕 */
export const useInitTokenGetFinish = () => {
  const [initGetTokenFlag, setInitGetTokenFlag] = useState(false);
  const [appToken, setAppToken] = useState('');
  const [distinctId, setDistinctId] = useState('');

  useAsyncEffect(async () => {
    const token = await getItem(ACCESSTOKEN);
    setAppToken(token);
    setInitGetTokenFlag(true);

    try {
      // TODO huajian 重点测试
      const id = await SDK.TA.tdGetDistinctId();
      setDistinctId((id as any));
    } catch (err) {}
  }, []);

  return [appToken, initGetTokenFlag, distinctId];
};