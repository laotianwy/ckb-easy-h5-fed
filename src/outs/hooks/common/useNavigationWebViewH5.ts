/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-19 14:17:02
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-05 16:53:47
 * @FilePath: /ckb-fbox-app/src/hooks/common/useNavigationWebViewH5.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useNavigation } from '@react-navigation/native';
import { useSetAtom } from 'jotai';
import { atomWebViewPageState } from '@/container/user/WebviewBridgeH5/atom/atomWebview';

/** app跳转链接 如轮播图 如果包含INCLUDE_DOMAIN 那么就使用下方的hooks跳转 */
export const INCLUDE_DOMAIN = 'm.3fbox.com';

/** app跳转h5webview hooks */
export const useNavigationWebViewH5 = () => {
  const setWebViewPageState = useSetAtom(atomWebViewPageState);
  const navigation = (useNavigation() as any);
  return (url: string) => {
    if (url.includes(INCLUDE_DOMAIN)) {
      const splitList = url.split(INCLUDE_DOMAIN);
      const newJumpUrl = splitList[splitList.length - 1];
      setWebViewPageState({ isShow: true, url: newJumpUrl });
    } else {
      setWebViewPageState({ isShow: true, url });
    }
    navigation.navigate('WebviewBridgeH5');
  };
};