/*
 * @Author: shiguang
 * @Date: 2023-12-12 15:29:47
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-08 15:45:22
 * @Description: 退出登录
 */
import * as SDK from '@snifftest/sdk/lib/rn';
import { CommonActions, useNavigation } from '@react-navigation/native';
import { useSetAtom } from 'jotai';
import { atomShopDetail } from '@/outs/atom/atomShopId';
import { atomUserDetail } from '@/outs/atom/atomUserDetail';
import { ACCESSTOKEN } from '@/outs/const/config';
import { removeItem } from '@/outs/utils/tool/AsyncStorage';

export const useLogout = () => {
  const setShopDetail = useSetAtom((atomShopDetail as any));
  const setUserDetail = useSetAtom((atomUserDetail as any));
  const navigation = (useNavigation() as any);
  return async () => {
    await removeItem(ACCESSTOKEN);
    // todo
    // await removeItem(USERSTORE);
    await removeItem(ACCESSTOKEN);
    setShopDetail(undefined);
    setUserDetail(undefined);

    try {
      SDK.TA.logout();
    } catch (err) {}

    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{ name: 'DirectIndex' }]
      })
    );
  };
};