/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-23 16:05:18
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-01 13:18:21
 * @FilePath: /ckb-fbox-app/src/hooks/common/useGetDistinctId.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useState } from 'react';
import { useAsyncEffect } from 'ahooks';
import { useAtomValue } from 'jotai';
import * as SDK from '@snifftest/sdk/lib/rn';
import { atomCurrentAppVersion } from '@/outs/atom/atomAppVersion';

/** global webview 初始化获取 distinctId */
export const useGetDistinctId = () => {
  const [getDataFinish, setGetDataFinish] = useState(false);
  const [distinctId, setDistinctId] = useState('');
  const currentAppVersion = useAtomValue(atomCurrentAppVersion);
  useAsyncEffect(async () => {
    try {
      // TODO huajian 重点测试
      const id = await SDK.TA.tdGetDistinctId();
      if (id) {
        setDistinctId(id);
      }
    } finally {
      setGetDataFinish(true);
    }
  }, []);
  return { distinctId, getDataFinish, currentAppVersion };
};