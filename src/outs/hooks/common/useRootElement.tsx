/*
 * @Author: shiguang
 * @Date: 2023-12-21 16:20:22
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-04 10:44:40
 * @Description:
 */
import { useAtom, useSetAtom } from 'jotai';
import React, { Fragment, useCallback } from 'react';
import {
  atomForceRenderRoot,
  atomForceRenderRootKey } from
'@/outs/atom/atomForceRenderRoot';

/**
 * 获取APP根DOM
 */
export const useRootElement = () => {
  const [forceRenderRootKey] = useAtom(atomForceRenderRootKey);
  const rootContainer = useCallback(
    (dom: React.ReactElement) => {
      return <Fragment key={forceRenderRootKey}>{dom}</Fragment>;
    },
    [forceRenderRootKey]
  );
  return rootContainer;
};

/** 强制重新渲染 APP */
export const useForceUpdateApp = () => {
  const forceUpdateApp = useSetAtom(atomForceRenderRoot);
  return forceUpdateApp;
};