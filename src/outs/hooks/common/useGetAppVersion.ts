/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-23 21:20:47
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 13:46:07
 * @FilePath: /ckb-fbox-app/src/hooks/common/useGetAppVersion.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import { useSetAtom } from 'jotai';
import { useAsyncEffect } from 'ahooks';
import { atomCurrentAppVersion } from '@/outs/atom/atomAppVersion';
import { getVersion } from '../base/useSetAuditVersion';

export const useGetAppVersion = () => {
  const currentAppVersionSet = useSetAtom(atomCurrentAppVersion);
  useAsyncEffect(async () => {
    const appVersion = await getVersion();
    currentAppVersionSet(appVersion);
  }, []);
};