/*
 * @Author: shiguang
 * @Date: 2023-12-20 19:19:56
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-04 11:48:34
 * @Description:
 */
import { useNetInfo } from '@react-native-community/netinfo';
import { useEffect, useState } from 'react';
import { useSetAtom } from 'jotai';
import { Platform } from 'react-native';
import axios from 'axios';
import { tool } from '@/outs/utils/tool';
import { setItem, getItem } from '@/outs/utils/tool/AsyncStorage';
import {
  ENUM_IOS_AUDIT_VERSION,
  atomIOSAuditStatus } from
'@/outs/atom/atomIOSAudit';
import { isAuditVersion } from '@/outs/atom/atomSetting';
import { getVersion } from '../base/useSetAuditVersion';
import { useForceUpdateApp } from './useRootElement';
// import SplashScreen from 'react-native-splash-screen';

export interface IOSAuditData {
  description: string;
  key: 'IOS_AUDIT';
  data: {
    iosVersion: string;
    androidVersion: string;
  };
}

type SettingsData = [IOSAuditData];

// 是否需要请求审核
const getIsNeedReqAudit = (
iosAuditVersionStatusValue: ENUM_IOS_AUDIT_VERSION) =>
{
  return (
    iosAuditVersionStatusValue !==
    ENUM_IOS_AUDIT_VERSION.NOT_IOS_AUDIT_VERSION);

};

export const STORAGE_KEY_IOS_AUDIT_VERSION_STATUS = 'IOS_AUDIT_VERSION';

/**
 * @returns 请求是否是 ios 审核版本
 */
export const requestIOSAuditVersionStatus = async () => {
  const currentVersion = await getVersion();
  if (!currentVersion) {
    return ENUM_IOS_AUDIT_VERSION.NOT_IOS_AUDIT_VERSION;
  }
  // const currentVersion = '9999';
  const res = await axios.get<SettingsData>(
    `https://static-s.theckb.com/app/config/direct-setting.json?x=${Math.random()}${Date.now()}`
  );
  const item = res.data.find((item) => item.key === 'IOS_AUDIT');
  if (!item || !item.data.iosVersion) {
    return ENUM_IOS_AUDIT_VERSION.NOT_IOS_AUDIT_VERSION;
  }
  // 当前版本是 ios 审核版本的时候
  return item.data.iosVersion === currentVersion ?
  ENUM_IOS_AUDIT_VERSION.IS_IOS_AUDIT_VERSION :
  ENUM_IOS_AUDIT_VERSION.NOT_IOS_AUDIT_VERSION;
};

export const useIsIOSAuditVersion = () => {
  const natInfo = useNetInfo();
  /** 获取网络链接状态 */
  const isConnected = natInfo?.isConnected;
  /** iOS的审核状态 */
  const [iosAuditVersionStatus, setAppSetting] =
  useState<ENUM_IOS_AUDIT_VERSION>(
    Platform.OS !== 'ios' ?
    ENUM_IOS_AUDIT_VERSION.NOT_IOS_AUDIT_VERSION :
    ENUM_IOS_AUDIT_VERSION.IS_UNKNOW
  );
  /** jotai全局里的iOS审核状态-枚举值 */
  const setIOSAuditStatus = useSetAtom(atomIOSAuditStatus);
  /** jotai里的审核版本-布尔值 */
  const setIsAuditVersion = useSetAtom(isAuditVersion);

  /** 当网络链接发生改变的时候触发。缓存是否审核版本 */
  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'ios') {
        await setItem(
          STORAGE_KEY_IOS_AUDIT_VERSION_STATUS,
          ENUM_IOS_AUDIT_VERSION.NOT_IOS_AUDIT_VERSION
        );
        return;
      }
      /** 获取缓存的状态。如果没有那么就是请求中 */
      const iosAuditVersionStatusValue: ENUM_IOS_AUDIT_VERSION =
      (await getItem(STORAGE_KEY_IOS_AUDIT_VERSION_STATUS)) ??
      ENUM_IOS_AUDIT_VERSION.IS_UNKNOW;
      const isNeedReqAudit = getIsNeedReqAudit(
        iosAuditVersionStatusValue
      );
      setAppSetting(iosAuditVersionStatusValue);
      /** 不需要请求 或者 没网。写入缓存中 */
      if (!isNeedReqAudit || !isConnected) {
        await setItem(
          STORAGE_KEY_IOS_AUDIT_VERSION_STATUS,
          iosAuditVersionStatusValue
        );
        return;
      }
      // debugger;
      console.log(
        tool.strings('开始请求 ios 审核版本'),

        'isNeedReqAudit',
        isNeedReqAudit,
        'isConnected',
        isConnected
      );
      // 请求 ios 审核版本
      const curIOSAuditVersionStatus =
      await requestIOSAuditVersionStatus();
      setAppSetting(curIOSAuditVersionStatus);
      await setItem(
        STORAGE_KEY_IOS_AUDIT_VERSION_STATUS,
        curIOSAuditVersionStatus
      );
    })();
  }, [isConnected]);

  const forceUpdateApp = useForceUpdateApp();

  // 当发现不是IOS审核版本，强制更新APP
  useEffect(() => {
    /** 更新ios审核版本状态 */
    setIOSAuditStatus(iosAuditVersionStatus);
    setIsAuditVersion(
      iosAuditVersionStatus ===
      ENUM_IOS_AUDIT_VERSION.IS_IOS_AUDIT_VERSION
    );
    if (
    iosAuditVersionStatus ===
    ENUM_IOS_AUDIT_VERSION.NOT_IOS_AUDIT_VERSION)
    {
      forceUpdateApp();
    }
  }, [
  iosAuditVersionStatus,
  forceUpdateApp,
  setIOSAuditStatus,
  setIsAuditVersion]
  );
  // console.log(
  // 	'====global.IS_AUDIT_VERSION====',
  // 	global.IS_AUDIT_VERSION,
  // 	'\n',
  // 	'====iosAuditVersionStatus====',
  // 	iosAuditVersionStatus
  // );
  return ([iosAuditVersionStatus] as const);
};