/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-12-26 14:00:49
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-08 15:42:40
 * @FilePath: /ckb-fbox-app/src/hooks/common/useTenjin.ts
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
import { SetStateAction, useSetAtom } from 'jotai';
import { NativeModules, Platform } from 'react-native';
import Tenjin from 'react-native-tenjin';
import { useMount } from 'ahooks';
import {
  PartialAnalyzeOrignDataProps,
  atomAnalyzeOrignData } from
'@/outs/atom/atomAnalyzeOrignData';

type SetAtom<Args extends any[], Result> = (...args: Args) => Result;

/**
 * @param isIos 是否iOS
 * @param key tenjin初始化的key
 * @param setAnalyzeOriginData 获取tenjin的来源数据并设置到全局jotai里
 */
export const tenjinInit = (
isIos: boolean,
key: string,
setAnalyzeOriginData: SetAtom<
  [SetStateAction<PartialAnalyzeOrignDataProps | null>],
  void>) =>

{
  Tenjin.initialize(key);
  if (!isIos) {
    Tenjin.setAppStore('googleplay');
  }
  Tenjin.optIn();
  Tenjin.connect();
  Tenjin.eventWithName('App Open');
  Tenjin.getAttributionInfo(
    (originInfo: PartialAnalyzeOrignDataProps) => {
      setAnalyzeOriginData(originInfo);
    },
    () => {
      setAnalyzeOriginData({});
    }
  );
};

export const IS_IOS = Platform.OS === 'ios';
export const TENJIN_KEY = IS_IOS ?
'E9QWF5CYNMIZKQUHPABWWIKCMCFEFJC9' :
'R27SETQQP4BC3ZXV1NMPHDXB8CAXHHSH';

/** 初始化tenjin sdk */
export const useTenjinInit = () => {
  const setAnalyzeOriginData = useSetAtom((atomAnalyzeOrignData as any));

  useMount(() => {
    if (IS_IOS) {
      setTimeout(() => {
        NativeModules.ToolModule.getIDFA().then((res) => {
          tenjinInit(IS_IOS, TENJIN_KEY, setAnalyzeOriginData);
        });
      }, 4000);
    } else {
      tenjinInit(IS_IOS, TENJIN_KEY, setAnalyzeOriginData);
    }
  });
};