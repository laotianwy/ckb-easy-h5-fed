/*
 * @Author: shiguang
 * @Date: 2023-11-22 16:49:25
 * @LastEditors: huajian
 * @LastEditTime: 2023-12-08 09:49:51
 * @Description: Splash Screen hook
 */

import { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';

interface UseSplashScreenOptions {
  /**
   * 延迟多少毫后显示 Splash Screen
   * @default 3000
   */
  delay?: number;
}

/**
 * Splash Screen hook
 */
const useSplashScreen = (options?: UseSplashScreenOptions) => {
  const { delay = 0 } = options ?? {};
  /**
   * 在页面加载时执行
   */
  useEffect(() => {
    /**
     * 延迟3秒后隐藏Splash Screen
     */
    const timeoutId = setTimeout(() => {
      SplashScreen?.hide();
    }, delay);
    /**
     * 在组件卸载时清除定时器
     */
    return () => {
      clearInterval(timeoutId);
    };
  }, [delay]);
};

export default useSplashScreen;