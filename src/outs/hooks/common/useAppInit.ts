/*
 * @Author: shiguang
 * @Date: 2023-11-22 16:52:45
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-26 10:11:40
 * @Description: useAppInit
 */
import { useAtomValue, useSetAtom } from 'jotai';
import { useEffect } from 'react';
import { atomRequestUserDetail } from '@/outs/atom/atomUserDetail';
import { atomToken } from '@/outs/atom/atomToken';
import { setItem } from '@/outs/utils/tool/AsyncStorage';
import { ACCESSTOKEN } from '@/outs/const/config';
import { useFaceBookInit } from '../base/useFaceBookInit';
import { useChannel } from '../base/useChannel';
import useSplashScreen from './useSplashScreen';
import { useInitThinkingData } from './useInitThinkingData';
import { useTenjinInit } from './useTenjin';
import { useGetAppVersion } from './useGetAppVersion';
// import { useTenjinInit } from './useTenjin';

// app 初始化的时候执行的 hook
const useAppInit = () => {
  useGetAppVersion();
  useChannel();
  /** facebook sdk init */
  useFaceBookInit();
  /** 获取token 改变 */
  const token = useAtomValue(atomToken);
  /** app是否显示。不再判断app 初始化是否审核版本 */

  // 调用 useSplashScreen 组件
  const requestUserDetail = useSetAtom(atomRequestUserDetail);

  /** 请求用户信息 */
  useEffect(() => {
    requestUserDetail({ isSetDefautShop: true });
  }, [requestUserDetail]);

  /** 启动图 */
  useSplashScreen();

  /** 初始化数数科技-三方分析 */
  useInitThinkingData();

  /** 初始化tenjinsdk */
  useTenjinInit();

  useEffect(() => {
    if (token) {
      setItem(ACCESSTOKEN, token);
      global.GLOBAL_TOKEN = token;
    }
  }, [token]);
};

// 将 useAppInit 函数导出
export default useAppInit;