/*
 * @Author: yusha
 * @Date: 2023-10-16 19:43:15
 * @LastEditors: yusha
 * @LastEditTime: 2024-03-27 10:10:37
 * @Description: 二次封装modal.confirm
 */
import { Button, Modal, ModalProps } from 'antd-mobile';
import { CSSProperties, ReactNode, useState } from 'react';
import './index.scss';
export type ModalConfirmProps = Omit<
	ModalProps,
	'visible' | 'closeOnAction' | 'actions'
> & {
	confirmText?: ReactNode;
	cancelText?: ReactNode;
	onConfirm?: () => void;
	onCancel?: () => void;
	isShowFooter?: boolean;
	confirmBtnStyle: CSSProperties;
	cancelBtnStyle: CSSProperties;
	/** 是否展示一个按钮 */
	// isShowCancleButton?: boolean;
};
export const confirm = (props: ModalConfirmProps) => {
	const {
		showCloseButton = true,
		isShowFooter,
		confirmBtnStyle,
		cancelBtnStyle
	} = props;
	const getActions = () => {
		if (isShowFooter === false) {
			return undefined;
		}
		return showCloseButton
			? [
					{
						key: 'cancel',
						text: props.cancelText || window._$m.t('取消'),
						onClick: () => {
							props.onCancel?.();
						},
						style: {
							background: '#ffffff',
							...cancelBtnStyle
						}
					},
					{
						key: 'confirm',
						text: props.confirmText || window._$m.t('确定'),
						onClick: () => {
							props.onConfirm?.();
						},
						style: {
							color: '#fff',
							width: '1.1rem',
							...confirmBtnStyle
						}
					}
				]
			: [
					{
						key: 'cancel',
						text: props.cancelText || window._$m.t('我知道了'),
						onClick: () => {
							props.onCancel?.();
						},
						style: {
							color: '#fff',
							width: '1rem',
							paddingLeft: '0.16rem',
							paddingRight: '0.16rem',
							...cancelBtnStyle
						}
					}
				];
	};
	return Modal.show({
		...props,
		bodyClassName: 'custom-modal',
		closeOnAction: true,
		onClose: () => {
			props.onClose?.();
		},
		actions: getActions()
	});
};
