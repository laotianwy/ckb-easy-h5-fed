/*
 * @Author: yusha
 * @Date: 2023-10-16 19:52:51
 * @LastEditors: yusha
 * @LastEditTime: 2023-10-16 19:55:10
 * @Description: 自定义modal
 */
import { Modal } from 'antd-mobile';
import { attachPropertiesToComponent } from '@/utils/attachPropertiesToComponent';
import { confirm } from './confirm';
export default attachPropertiesToComponent(Modal, {
	confirm
});
