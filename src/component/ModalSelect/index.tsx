import React, { useState, memo } from 'react';
import { Mask } from 'antd-mobile';
import styles from './index.module.scss';
interface ChooseItemProps {
	id: number;
	text: string;
}
interface ModalSelectProps {
	visible: boolean;
	setVisible: React.Dispatch<React.SetStateAction<boolean>>;
	chooseList: ChooseItemProps[];
	onClickSelectItem?: (item: ChooseItemProps) => void;
}
const ModalSelect = (props: ModalSelectProps) => {
	const { visible, setVisible, chooseList, onClickSelectItem } = props;
	const onClickItem = (item: ChooseItemProps) => {
		onClickSelectItem?.(item);
		setVisible(false);
	};
	return (
		<Mask
			visible={visible}
			style={{
				'--z-index': '1000'
			}}
			getContainer={() => document.getElementById('root')!}
		>
			<div className={styles.wrap}>
				<div className={styles['choose-list']}>
					{chooseList.map((chooseItem) => (
						<div
							onClick={(e) => onClickItem(chooseItem)}
							key={chooseItem.id}
							className={styles['choose-item']}
						>
							{chooseItem.text}
						</div>
					))}
				</div>
				<div
					onClick={(e) => setVisible(false)}
					className={styles.close}
				>
					{window._$m.t('取消')}
				</div>
			</div>
		</Mask>
	);
};
export default memo(ModalSelect);
