/*
 * @Author: yusha
 * @Date: 2024-03-27 14:13:18
 * @LastEditors: yusha
 * @LastEditTime: 2024-03-27 14:34:17
 * @Description: 蒙层（antd-mobile的mask蒙层关闭有问题）
 */
import ReactDOM from 'react-dom';
import './index.scss';

const Mask: React.FC<Partial<any>> = (props) => {
	const { visible, className, onMaskClick } = props;
	return ReactDOM.createPortal(
		<>
			{visible && (
				<div
					className={`component-custom-mask ${className}`}
					onClick={onMaskClick}
				>
					{props.children}
				</div>
			)}
		</>,
		document.getElementById('root')
	);
};
export default Mask;
