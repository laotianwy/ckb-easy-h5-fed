/*
 * @Author: yusha
 * @Date: 2023-10-22 16:55:54
 * @LastEditors: yusha
 * @LastEditTime: 2023-10-23 14:47:51
 * @Description: 选择 都道府县
 */

import { memo, useState } from 'react';
import { Cascader, Picker, Toast } from 'antd-mobile';
import { CheckListValue } from 'antd-mobile/es/components/check-list';
import { useMount } from 'ahooks';
import MoreMedium from '@/common/icons/MoreMedium';
import { request } from '@/config/request';
import './index.scss';
const CustomCascader = (props) => {
	// const [selectValue, setSelectValue] = useState<string>();
	const [visible, setVisible] = useState(false);
	const {
		placeholder = window._$m.t('请选择'),
		data,
		value,
		onChange,
		emptyTextTip
	} = props;
	const list =
		data?.length > 0
			? data?.map((item) => {
					const newItem = {
						...item,
						label: item.nameJa,
						value: {
							...item
						},
						key: item.intAreaId
					};
					return newItem;
				})
			: [];
	return (
		<>
			<div
				className="custom-cascader-style"
				onClick={() => {
					if (list?.length <= 0 && emptyTextTip) {
						Toast.show({
							content: emptyTextTip
						});
						return;
					}
					setVisible(true);
				}}
			>
				{value?.length > 0 && value?.[0] && (
					<div>
						{value?.map((item, index) => {
							return item?.nameJa;
						})}
					</div>
				)}

				{(value?.length <= 0 || !value?.[0]) && (
					<div className="placeholder-style">{placeholder}</div>
				)}

				<MoreMedium />
			</div>
			<Picker
				defaultValue={value}
				columns={
					list?.length > 0
						? [list]
						: [
								[
									{
										label: window._$m.t('暂无数据'),
										value: null
									}
								]
							]
				}
				visible={visible}
				onClose={() => {
					setVisible(false);
				}}
				// extra={list?.length === 0 ? '暂无数据' : ''}
				// value={selectValue}
				onConfirm={(v) => {
					// setValue(v);
					onChange?.(v);
				}}
			/>
		</>
	);
};
export default memo(CustomCascader);
