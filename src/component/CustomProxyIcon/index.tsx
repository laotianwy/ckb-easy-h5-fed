import styles from './index.module.scss';
interface Props extends React.HTMLAttributes<HTMLDivElement> {}
export const CustomProxyIcon: React.FC<Props> = ({ title }) => {
	return <div className={styles.icon}>{title}</div>;
};
