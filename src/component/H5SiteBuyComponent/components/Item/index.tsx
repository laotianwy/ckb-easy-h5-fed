/*
 * @Author: huajian
 * @Date: 2024-03-25 10:40:27
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-04-11 13:30:02
 * @Description:
 */
import { useRef, useState } from 'react';
import './index.scss';
import { useNavigate } from 'react-router-dom';
import { CollectionActivityRespDTO } from '@/service/easyMarket';
import { TaEvent, taTrack } from '@/config/buryingPoint';
interface RenderItemProps {
	active: boolean;
	item: CollectionActivityRespDTO;
	setActiveId: (num: number) => void;
	collect: (item: CollectionActivityRespDTO) => void;
	dislike: (num: number) => void;
	positionIndex: number;
}

const Item = (props: RenderItemProps) => {
	const { active, item, setActiveId, collect, dislike, positionIndex } =
		props;
	const [likeMaxWidth, setLikeMaxWidth] = useState(0);
	const [isTop, setIsTop] = useState<boolean>(false);
	const navigate = useNavigate();

	const jumpPage = (e) => {
		e.stopPropagation();

		taTrack({
			event: TaEvent.BTN_CLICK,
			value: {
				position: 'one_step_shopping',
				collection_name: item.moduleName
			}
		});
		if (item.productCollection) {
			if (!item.singleProductCode) {
				return;
			}
			navigate(`/goods/detail?productCode=${item.singleProductCode}`);
		} else {
			const url = `/goods/collectionGoodsList?id=${item.id}`;
			navigate(url);
		}
	};
	return (
		<div
			style={{
				marginBottom: '0.12rem',
				position: 'absolute',
				top: positionIndex * 1.12 + 'rem',
				transition: 'all 0.5s',
				zIndex: isTop ? 10 : 5
			}}
		>
			<div
				onClick={jumpPage}
				style={{
					position: 'relative',
					width: '3.35rem',
					height: '1rem'
				}}
			>
				<img
					style={{
						width: '100%',
						height: '100%',
						borderRadius: '0.08rem'
					}}
					alt=""
					src={item.homeImgUrl}
					onClick={(e) => {
						e.stopPropagation();

						taTrack({
							event: TaEvent.BTN_CLICK,
							value: {
								position: 'one_step_shopping',
								collection_name: item.moduleName
							}
						});
						if (item.productCollection) {
							if (!item.singleProductCode) {
								return;
							}
							navigate(
								`/goods/detail?productCode=${item.singleProductCode}`
							);
						} else {
							navigate(
								`/goods/collectionGoodsList?id=${item.id}`
							);
						}
					}}
				/>

				<img
					style={{
						width: '0.16rem',
						height: '0.16rem',
						position: 'absolute',
						right: '0.08rem',
						bottom: '0.12rem'
					}}
					src="https://static-jp.theckb.com/static-asset/easy-app/icon/more_icon.png"
					onClick={(e) => {
						e.stopPropagation();
						taTrack({
							event: TaEvent.BTN_CLICK,
							value: {
								position: 'one_step_shopping_operate'
							}
						});
						setActiveId(item.id);
					}}
					alt=""
				/>

				{active && (
					<div
						className="active-modal"
						onClick={(e) => {
							e.stopPropagation();
							setActiveId(0);
						}}
					>
						<div className="active-modal-content">
							<div
								className="active-moddal-item"
								style={{ marginBottom: '0.08rem' }}
								onClick={(e) => {
									e.stopPropagation();
									collect(item);
									setActiveId(0);
								}}
							>
								<img
									src={`https://static-jp.theckb.com/static-asset/easy-app/icon/like_icon${
										item.collectedFlag === 1 ? '' : '_un'
									}.png`}
									alt=""
								/>

								<div className="text">
									{window._$m.t('收藏该特集')}
								</div>
							</div>
							<div
								className="active-moddal-item"
								onClick={async (e) => {
									e.stopPropagation();
									setIsTop(true);
									await dislike(item.id);
									setTimeout(() => {
										setIsTop(false);
									}, 3000);
									setActiveId(0);
								}}
							>
								<img
									src={`https://static-jp.theckb.com/static-asset/easy-app/icon/unlike_icon${
										item.dislikeFlag === 1 ? '' : '_un'
									}.png`}
									alt=""
								/>

								<div className="text">
									{window._$m.t('不喜欢该特集')}
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		</div>
	);
};

export default Item;
