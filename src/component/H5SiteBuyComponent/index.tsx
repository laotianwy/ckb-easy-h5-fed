/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-03-21 21:27:27
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-04-11 13:31:08
 * @FilePath: /ckb-easy-h5-fed/src/component/H5SiteBuyComponent/index.tsx
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect, useState } from 'react';
import './index.scss';
import dayjs from 'dayjs';
import { useNavigate } from 'react-router-dom';
import { HOME_ENUM } from '@/outs/const';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import { request } from '@/config/request/interceptors';
import { collectItem, dislikeItem } from './common';
import Item from './components/Item';
interface SITE_BUY_PROPS {
	data: CollectionActivityRespDTO[];
}
const H5SiteBuyComponent = (props: SITE_BUY_PROPS) => {
	const { data } = props;
	const navigate = useNavigate();
	const [list, setList] = useState<CollectionActivityRespDTO[]>([]);
	const [activeId, setActiveId] = useState<number>(0);
	const collect = async (item: CollectionActivityRespDTO) => {
		await collectItem(item);
		setList(
			list.map((v) => {
				const flag = v.collectedFlag === 1 ? 0 : 1;
				return {
					...v,
					collectedFlag: item.id === v.id ? flag : v.collectedFlag
				};
			})
		);
	};
	const dislike = async (id: number) => {
		const res = await dislikeItem(String(id));
		if (!res) return;
		const newList = list.map((i) => {
			return {
				...i,
				disLikeTime:
					i.id === id
						? dayjs(new Date().getTime()).format(
								'YYYY-MM-DD HH:mm:ss'
							)
						: i.disLikeTime,
				dislikeFlag: i.id === id ? 1 : i.dislikeFlag
			};
		});
		setList(newList);
	};
	const [idMapList, setIdMapList] = useState<number[]>();
	useEffect(() => {
		setList(props.data);
	}, [props.data]);
	useEffect(() => {
		const noDislikeList: number[] = list
			.filter((item) => item.dislikeFlag === 0)
			.map((i) => i.id);
		const dislikeList: number[] = list
			.filter((item) => item.dislikeFlag === 1)
			.sort(
				(a, b) =>
					new Date(a.disLikeTime).getTime() -
					new Date(b.disLikeTime).getTime()
			)
			.map((i) => i.id);
		console.log(
			window._$m.t('新数组'),
			[...noDislikeList, ...dislikeList],
			list
		);
		setIdMapList([...noDislikeList, ...dislikeList]);
	}, [list]);

	const jumpPage = () => {
		taTrack({
			event: TaEvent.BTN_CLICK,
			value: {
				position: 'one_step_shopping_more'
			}
		});
		const url = `/goods/collection?one=${HOME_ENUM.ONE_PRODUCT}&title=${encodeURIComponent(list?.[0]?.collectionClassifyName)}`;
		navigate(url);
	};
	if (!list || list?.length === 0) {
		return;
	}
	return (
		<div
			style={{
				backgroundColor: '#fff',
				borderRadius: '0.08rem',
				padding: '0.16rem 0.08rem',
				marginTop: '0.12rem'
			}}
		>
			<div
				style={{
					display: 'flex',
					flexDirection: 'row',
					alignItems: 'center',
					marginBottom: '0.16rem'
				}}
			>
				<div
					onClick={jumpPage}
					style={{
						padding: '0 0.08rem',
						fontSize: '.17rem',
						fontWeight: '600',
						color: 'rgba(0, 0, 0, 0.88)'
					}}
				>
					{list?.[0]?.collectionClassifyName}
				</div>
				<img
					onClick={jumpPage}
					style={{
						width: '0.22rem',
						height: '0.22rem'
					}}
					src="https://static-jp.theckb.com/static-asset/easy-app/icon/arrow.png"
					alt=""
				/>
			</div>
			{/* <Transition list={list} render={render} itemKey="id" /> */}
			<div
				style={{
					position: 'relative',
					overflow: 'hidden',
					height:
						1.12 * (list.length < 4 ? list.length : 4) -
						0.12 +
						'rem'
				}}
			>
				{list.map((item, index) => {
					return (
						<Item
							active={activeId === item.id}
							key={index}
							item={item}
							setActiveId={setActiveId}
							collect={collect}
							dislike={dislike}
							positionIndex={idMapList.findIndex(
								(i) => i === item.id
							)}
						/>
					);
				})}
			</div>
		</div>
	);
};

export default H5SiteBuyComponent;
