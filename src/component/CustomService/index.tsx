/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-11-06 13:51:37
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-24 17:41:51
 * @FilePath: \ckb-easy-h5-fed\src\component\CustomService\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { memo, useState } from 'react';
import { FloatingBubble } from 'antd-mobile';
import { useMount } from 'ahooks';
import { useAtomValue } from 'jotai';
import { User } from '@/Atoms';
import CustomerServiceIcon from '@/common/icons/CustomerService';
import './index.scss';
const CustomService = () => {
	const [layoutBottomHeight, setLayoutBottomHeight] = useState(0);
	const serviceNum = useAtomValue(User.serviceNum);
	const splitStringPX = (str: string) => {
		return Number(str.split('px')[0]);
	};
	const getDomHeight = (domRef: HTMLElement): number => {
		if (!domRef) return 0;
		const domHeight =
			domRef?.offsetHeight ??
			0 + splitStringPX(getComputedStyle(domRef, null).marginTop);
		return domHeight;
	};
	const getBottomHeight = () => {
		const layoutTabbarBottomDom = document.getElementById('layout-footer');
		const layoutFixedBottom = document.getElementById('fixed-bottom');
		const getTabbarHeight = getDomHeight(layoutTabbarBottomDom!);
		const getFixedHeight = getDomHeight(layoutFixedBottom!);
		console.log(
			getTabbarHeight + getFixedHeight,
			'getTabbarHeight + getFixedHeight'
		);
		setLayoutBottomHeight(getTabbarHeight + getFixedHeight);
	};
	useMount(() => {
		setTimeout(() => {
			getBottomHeight();
		}, 0);
	});
	const showService = () => {
		// TODO：如果改此处。客服页面的也要改
		const w: any = window;
		const ChannelIO = w.ChannelIO;
		// 根据站点判断展示哪个语言
		// {language: 'ko' | 'ja' | 'en'}
		const language = 'ja';
		// eslint-disable-next-line new-cap
		ChannelIO('updateUser', {
			language
		});
		// eslint-disable-next-line new-cap
		ChannelIO('showMessenger');
	};
	return (
		<FloatingBubble
			axis="xy"
			magnetic="x"
			style={
				{
					'--initial-position-bottom': `calc(1.16rem + ${layoutBottomHeight}px)`,
					'--initial-position-right': '.16rem',
					'--edge-distance': '24px',
					'--adm-color-primary': '#fff',
					'--z-index': '99999',
					'--size': 60
				} as any
			}
		>
			<div className="custom-servicemove-view" onClick={showService}>
				<div className="tag">
					<img
						src="https://static-s.theckb.com/BusinessMarket/Easy/H5/client.png"
						alt="service"
					/>

					{serviceNum > 0 ? <div className="red-tag" /> : null}
				</div>
				<span className="text">{window._$m.t('客服')}</span>
			</div>
		</FloatingBubble>
	);
};
export default memo(CustomService);
