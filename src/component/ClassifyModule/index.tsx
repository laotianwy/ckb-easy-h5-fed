import { Checkbox, SpinLoading } from 'antd-mobile';
import { useState, Fragment, useEffect } from 'react';
import { useRequest } from 'ahooks';
import {
	createSearchParams,
	useNavigate,
	useSearchParams
} from 'react-router-dom';
import { useEvent } from '@/utils/hooks/useEvent';
import { request } from '@/config/request';
import { ProductCategoryFrontendShortRespDTO } from '@/service/easyGoods';
import styles from './index.module.scss';
interface ClassifyModuleProps {
	classifyClickCallBack?: (productCategoryFrontendId, cateNameJp) => void;
	cateId?: number;
}
const arrowDownImage =
	'https://static-s.theckb.com/BusinessMarket/Easy/H5/arrow-down.png';
const arrowUpImage =
	'https://static-s.theckb.com/BusinessMarket/Easy/H5/arrow-up.png';
const ClassifyModule = ({
	classifyClickCallBack,
	cateId
}: ClassifyModuleProps) => {
	const navigate = useNavigate();
	const [searchParams] = useSearchParams();
	const { runAsync: apiGetCategory, loading } = useRequest(
		request.easyGoods.productCategoryFrontend.tree,
		{
			manual: true
		}
	);
	const [classifyList, setClassifyList] = useState<any[]>([]);
	const [isOpen, setIsOpen] = useState(false);
	useEffect(() => {
		if (searchParams.get('barCode')) {
			request.easyGoods.productCategoryFrontend
				.activityTree({
					channelTag: searchParams.get('barCode')!
				})
				.then((res) => {
					setClassifyList(res.data || []);
				});
			return;
		}
		apiGetCategory({}).then((res) => {
			setClassifyList(res.data || []);
			setIsOpen(true);
		});
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [apiGetCategory, searchParams]);
	const goDetail = (record) => {
		const { productCategoryFrontendId, cateNameJp } = record;
		window.sessionStorage.setItem(
			'productCategoryFrontendId',
			record.productCategoryFrontendId
		);
		if (classifyClickCallBack) {
			classifyClickCallBack(productCategoryFrontendId, cateNameJp);
			return;
		}
		const params: any = {
			cateId: String(productCategoryFrontendId),
			cateName: cateNameJp
		};
		if (searchParams.get('barCode')) {
			params.channelTag = searchParams.get('barCode');
		}
		navigate({
			pathname: '/goods/list',
			search: `?${createSearchParams(params)}`
		});
	};
	const taggleClassifyExpand = useEvent(
		(firstIndex: number, secondIndex?: number, thirdIndex?: number) => {
			const newClassifyList = [...classifyList];
			let currentNode;
			if (
				typeof thirdIndex !== 'undefined' &&
				typeof secondIndex !== 'undefined'
			) {
				currentNode =
					newClassifyList?.[firstIndex]?.children?.[secondIndex || 0]
						?.children?.[thirdIndex || 0];
			} else if (typeof secondIndex !== 'undefined') {
				currentNode =
					newClassifyList?.[firstIndex]?.children?.[secondIndex || 0];
			} else {
				currentNode = newClassifyList?.[firstIndex];
			}
			currentNode.show = !currentNode.show;
			setClassifyList(newClassifyList);
		}
	);
	useEffect(() => {
		if (isOpen && sessionStorage.getItem('productCategoryFrontendId')) {
			const indexList: number[] = [];
			const cateogryId = sessionStorage.getItem(
				'productCategoryFrontendId'
			);
			const list = getCategoryPosition(
				classifyList,
				cateogryId,
				indexList
			);
			if (!list?.length) return;
			list.forEach((item, index) => {
				if (index === 0) {
					taggleClassifyExpand(Number(item));
				} else if (index === 1) {
					taggleClassifyExpand(Number(list[index - 1]), Number(item));
				} else {
					taggleClassifyExpand(
						Number(list[index - 2]),
						Number(list[index - 1]),
						Number(item)
					);
				}
			});
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isOpen, taggleClassifyExpand]);
	const getCategoryPosition = (
		categoryList: ProductCategoryFrontendShortRespDTO[],
		categoryId: string | null,
		indexList: number[]
	) => {
		for (let i = 0; i < categoryList.length; i++) {
			if (
				String(categoryList[i].productCategoryFrontendId) === categoryId
			) {
				indexList.push(i);
				return indexList;
			}
			const children = categoryList[i].children;
			if (!children || !children.length) return [];
			const list = getCategoryPosition(
				children,
				categoryId,
				indexList.concat([i])
			);
			if (list?.length) {
				return list;
			}
		}
	};
	return (
		<div className={styles['classify-all']}>
			{loading && <SpinLoading className={styles['classify-loading']} />}
			{classifyList.map((first, firstIndex) => (
				<div className={styles['classify-box']} key={first.cateNameJp}>
					<div className={styles['classify-title']}>
						<div
							onClick={() => goDetail(first)}
							className={styles['title-left-first']}
						>
							<Checkbox
								checked={
									cateId ===
										first.productCategoryFrontendId ||
									String(cateId) ===
										String(first.productCategoryFrontendId)
								}
							>
								{first.cateNameJp}
							</Checkbox>
						</div>
						<div
							className={styles['collapse-text']}
							onClick={() => taggleClassifyExpand(firstIndex)}
						>
							{first.show
								? window._$m.t('收起')
								: window._$m.t('展开')}
							<img
								className={styles['arrow-img']}
								src={first.show ? arrowUpImage : arrowDownImage}
								alt=""
							/>
						</div>
					</div>
					{first.show ? (
						<div
							style={{
								paddingLeft: '.12rem'
							}}
						>
							{first.children?.map((second, secondIndex) => (
								<Fragment key={second.cateNameJp}>
									<div className={styles['classify-title']}>
										<div
											onClick={() => goDetail(second)}
											className={styles['title-left']}
										>
											<Checkbox
												checked={
													cateId ===
														second.productCategoryFrontendId ||
													String(cateId) ===
														String(
															second.productCategoryFrontendId
														)
												}
											>
												{second.cateNameJp}
											</Checkbox>
										</div>
										<div
											className={styles['collapse-text']}
											onClick={() =>
												taggleClassifyExpand(
													firstIndex,
													secondIndex
												)
											}
										>
											{second.show
												? window._$m.t('收起')
												: window._$m.t('展开')}
											<img
												className={styles['arrow-img']}
												src={
													second.show
														? arrowUpImage
														: arrowDownImage
												}
												alt=""
											/>
										</div>
									</div>
									{second.show ? (
										<>
											{second.children?.map(
												(third, thirdIndex) => (
													<div
														key={third.cateNameJp}
														className={
															styles[
																'classify-last'
															]
														}
														style={{
															color: `${cateId === third.productCategoryFrontendId || String(cateId) === third.productCategoryFrontendId ? '#ff5010' : ''}`
														}}
														onClick={() => {
															goDetail(third);
														}}
													>
														{third.cateNameJp}
													</div>
												)
											)}
										</>
									) : null}
								</Fragment>
							))}
						</div>
					) : null}
				</div>
			))}
		</div>
	);
};
export default ClassifyModule;
