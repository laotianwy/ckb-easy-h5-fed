/*
 * @Date: 2023-12-05 17:55:17
 * @LastEditors: yusha
 * @LastEditTime: 2024-04-03 13:51:15
 * @FilePath: /ckb-easy-h5-fed/src/component/ActiveTag/index.tsx
 * @Description:
 */
import Wenhaoquestion from '@/common/icons/Wenhaoquestion';
import CustomModal from '@/component/CustomModal';
import './index.scss';
interface ActiveTagProps {
	/** 内容 */
	text: string;
	/** 提示 */
	tips?: string;
	className?: string;
}
const ActiveTag = (props: ActiveTagProps) => {
	const { className = '' } = props;
	const clickShowTips = () => {
		CustomModal.confirm({
			content: props.tips,
			showCloseButton: false
		});
	};
	return (
		<div className={`active-tag ${className}`}>
			{props.text && <div className="active-text">{props.text}</div>}
			{props.tips && (
				<Wenhaoquestion
					onClick={clickShowTips}
					className="active-tips"
				/>
			)}
		</div>
	);
};
export default ActiveTag;
