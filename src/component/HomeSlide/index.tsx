/* eslint-disable no-nested-ternary */
/*
 * @Author: yusha
 * @Date: 2023-12-04 15:19:40
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-28 19:00:44
 * @Description: 侧边栏
 */

import Popup from '@nutui/nutui-react-native/lib/module/popup';
import { useAsyncEffect } from 'ahooks';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import React, { useState } from 'react';
import { useAtomValue } from 'jotai';
import { useNavigate } from 'react-router-dom';
import * as SDK from '@snifftest/sdk/lib/rn';
import { User } from '@/Atoms';
import { request } from '@/config/request';
import { CustomRMImg } from '@/component/CustomRMImg';
import { jumpToLogin } from '@/config/request/env';
import { ICON_AVATAR, membershipList, menuOptions } from './config';
import Order from './modules/Order';
import Workbench from './modules/Workbench';
interface HomeSidebarProps {
	visible: boolean;
	toggle: () => void;
}
export const ICON_ARROW_RIGHT =
	'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_arrow_right_gray.png';
const HomeSidebar = (props: HomeSidebarProps) => {
	const { visible, toggle } = props;
	const navigate = useNavigate();
	/** 用户信息 */
	const userDetail = useAtomValue(User.userDetail);

	/** 是否登录 */
	const isLogin = useAtomValue(User.isLogin);
	// 菜单配置
	const _menuOptions = isLogin ? menuOptions : [...menuOptions].splice(2, 2);

	/** 是否开通过会员 */
	const [isFirstVip, setIsFirstVip] = useState(false);

	/** 会员信息 */
	const getVipObj = () => {
		// 未登录直接展示等级为0
		if (!isLogin) {
			return membershipList.find((item) => {
				return 0 === item.level;
			});
		}
		return membershipList.find((item) => {
			return userDetail?.membership?.templateLevel === item.level;
		});
	};

	/** 获取用户是否开通过会员 */
	const getIsFirstPayVip = async () => {
		const res =
			await request.customer.getCustomerIsNewMembership.getCustomerIsNewMembership(
				{}
			);
		if (res.code === '0' && !res.data) {
			setIsFirstVip(true);
			return;
		}
		setIsFirstVip(false);
	};
	useAsyncEffect(async () => {
		if (!isLogin) return;
		await getIsFirstPayVip();
	}, []);
	return (
		<Popup
			destroyOnClose
			position="left"
			visible={visible}
			onClickOverlay={() => {
				toggle();
			}}
			closeOnClickOverlay={true}
		>
			<View
				style={{
					width: SDK.fitSize(300),
					height: window.innerHeight,
					backgroundColor: '#fff',
					padding: SDK.fitSize(16),
					paddingTop: SDK.fitSize(56)
				}}
			>
				{/* 头像 */}
				<View
					style={{
						width: SDK.fitSize(76),
						height: SDK.fitSize(76),
						marginBottom: SDK.fitSize(8),
						borderRadius: SDK.fitSize(38),
						borderWidth: SDK.fitSize(2),
						borderColor: 'rgba(255, 255, 255, 0.20)',
						alignContent: 'center',
						justifyContent: 'center',
						backgroundColor: '#fff'
					}}
				>
					<CustomRMImg
						alt=""
						src={ICON_AVATAR}
						style={{
							width: SDK.fitSize(72),
							height: SDK.fitSize(72),
							borderRadius: SDK.fitSize(36),
							border: '0.01rem solid rgba(240,235,235,.7)'
						}}
						imgSize={100}
					/>
				</View>
				{/* 登录名和vip等级 */}
				{!isLogin && (
					<Text
						style={{
							fontSize: SDK.fitSize(20),
							color: '#000',
							fontWeight: '500'
						}}
					>
						{window._$m.t('暂未登录')}
					</Text>
				)}

				{isLogin && (
					<View
						style={{
							display: 'flex',
							flexDirection: 'row',
							alignItems: 'center',
							justifyContent: 'space-between'
						}}
					>
						<Text
							style={{
								fontSize: SDK.fitSize(20),
								fontWeight: '500',
								color: '#000',
								marginRight: SDK.fitSize(16),
								width: SDK.fitSize(140)
							}}
							numberOfLines={1}
						>
							{userDetail?.loginName}
						</Text>
						<TouchableOpacity
							style={{
								paddingHorizontal: SDK.fitSize(6),
								paddingVertical: SDK.fitSize(2),
								borderRadius: SDK.fitSize(25),
								borderWidth: SDK.fitSize(1),
								borderColor: '#CDD2DA',
								flexDirection: 'row',
								alignItems: 'center'
							}}
							onPress={() => {
								jumpToLogin('/accountInfo');
							}}
						>
							<Text
								style={{
									color: '#505762'
								}}
							>
								{window._$m.t('账户信息')}
							</Text>
							<Image
								source={{
									uri: ICON_ARROW_RIGHT
								}}
								style={{
									width: SDK.fitSize(12),
									height: SDK.fitSize(12)
								}}
								resizeMode="contain"
							/>
						</TouchableOpacity>
					</View>
				)}

				{/* 我的订单 */}
				<Order toggle={toggle} />
				{/* 工作台 */}
				<Workbench toggle={toggle} />
			</View>
		</Popup>
	);
};
export default HomeSidebar;
