/*
 * @Author: yusha
 * @Date: 2023-12-04 18:16:51
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-13 16:40:17
 * @Description: 配置文件
 */

import * as SDK from '@snifftest/sdk/lib/rn';
import { EnumOrderStatus } from '@/common/Enums';

/** 会员列表 */
export const membershipList = [
	{
		level: 0,
		vipSignImg: null,
		// desc: '会員未登録状態',
		vipBgcolor: '#999999',
		textColor: '#FFFFFF'
	},
	{
		level: 1,
		vipSignImg:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame11@3.x.png',
		// desc: 'エコノミープラン',
		vipBgcolor: '#DFF2EC',
		textColor: '#0D3A2C',
		textWidth: SDK.fitSize(110),
		avatarFrame:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame1@3.x.png'
	},
	{
		level: 2,
		vipSignImg:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame2-1@3.x.png',
		// desc: 'スタンダードプラン',
		vipBgcolor: '#E2EAF2',
		textColor: '#1E3752',
		textWidth: SDK.fitSize(120),
		avatarFrame:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame2@3.x.png'
	},
	{
		level: 3,
		vipSignImg:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame3-1@3.x.png',
		// desc: 'ビジネスプラン',
		vipBgcolor: '#F8E8D1',
		textColor: '#422F16',
		textWidth: SDK.fitSize(105),
		avatarFrame:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame3@3.x.png'
	},
	{
		level: 4,
		textWidth: SDK.fitSize(115),
		vipSignImg:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame4-1@3.x.png',
		// desc: 'エコノミープラン',
		vipBgcolor: '#F9EBE2',
		textColor: '#452115',
		avatarFrame:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/avatarFrame4@3.x.png'
	}
];

interface MenuOptions {
	icon: string;
	text: string;
	linkPage: string;
	showBage?: boolean;
	showValue?: boolean;
}
/** 菜单配置 */
export const menuOptions: MenuOptions[] = [
	// {
	// 	icon: 'https://static-s.theckb.com/BusinessMarket/App/Icon/consume@3.png',
	// 	text: window._$m.t('资金钱包'),
	// 	linkPage: '/user/wallet'
	// },
	{
		icon: 'https://static-s.theckb.com/BusinessMarket/App/Icon/like@3.png',
		text: window._$m.t('收藏商品'),
		linkPage: '/user/collection'
	},
	{
		icon: 'https://static-jp.theckb.com/static-asset/easy-h5/icon_coupon.svg',
		text: window._$m.t('优惠券'),
		linkPage: '/user/CouponList',
		showValue: true
	},
	{
		icon: 'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_refund.png',
		text: window._$m.t('退款记录'),
		linkPage: '/user/refund'
	},
	{
		icon: 'https://static-s.theckb.com/BusinessMarket/App/home/notice3625845.png',
		text: window._$m.t('消息通知'),
		linkPage: '/user/message/center',
		showBage: true
	},
	{
		icon: 'https://static-s.theckb.com/BusinessMarket/App/home/kefu362582.png',
		text: window._$m.t('在线客服'),
		linkPage: 'customService'
	},
	{
		icon: 'https://static-s.theckb.com/BusinessMarket/App/home/tickets.png',
		text: window._$m.t('使用指南'),
		linkPage: '/user/message/usageGuide'
	},
	{
		icon: 'https://static-s.theckb.com/BusinessMarket/App/Icon/logout@3.png',
		text: window._$m.t('退出登录'),
		linkPage: 'logout'
	}
];

/** 添加店铺icon */
export const ADD_STORE_ICON =
	'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_plus.png';
export const ICON_AVATAR =
	// 'https://static-s.theckb.com/BusinessMarket/Easy/H5/touxiang2.png';
	'https://static-s.theckb.com/BusinessMarket/Easy/H5/3Fbox.png';

/** 店铺列表的图片 */
export const storeImgList = [
	'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_shop_other@3x.png',
	'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_shop_base@3x.png',
	'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_shop_shopify@3x.png',
	'',
	'https://static-s.theckb.com/BusinessMarket/App/Icon/icon_shop_amazon@3x.png'
];

/** 订单相关icon */
export const orderStatusList = [
	{
		status: EnumOrderStatus.waitPay.code,
		title: EnumOrderStatus.waitPay.cn,
		badgeName: 'waitPayCount',
		showBage: true,
		iconUrl:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/wallet-one.png'
	},
	{
		status: EnumOrderStatus.waitShip.code,
		title: EnumOrderStatus.waitShip.cn,
		badgeName: 'waitDeliverCount',
		showBage: false,
		iconUrl:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/inbox-out@3.png'
	},
	{
		status: EnumOrderStatus.shipped.code,
		title: EnumOrderStatus.shipped.cn,
		badgeName: 'deliveredCount',
		showBage: false,
		iconUrl:
			'https://static-s.theckb.com/BusinessMarket/App/Icon/inbox-download-r@3.png'
	}
];
