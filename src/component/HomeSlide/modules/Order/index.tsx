/*
 * @Author: yusha
 * @Date: 2023-12-09 19:39:15
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-28 18:27:05
 * @Description:
 */

import Badge from '@nutui/nutui-react-native/lib/module/badge';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { useState } from 'react';
import { useAsyncEffect } from 'ahooks';
import { useAtomValue } from 'jotai';
import { useNavigate } from 'react-router-dom';
import * as SDK from '@snifftest/sdk/lib/rn';
import { OrderCountResp } from '@/service/easyOrder';
import { request } from '@/config/request';
import MoreMedium from '@/common/icons/MoreMedium';
import { User } from '@/Atoms';
import { goToLogin } from '@/config/request/interceptors';
import { orderStatusList } from '../../config';
const Order = ({ toggle }) => {
	const navigation = useNavigate();
	/** 是否登录 */
	const isLogin = useAtomValue(User.isLogin);
	/**  订单数量 */
	const [orderCount, setOrderCount] = useState<OrderCountResp>();
	useAsyncEffect(async () => {
		if (!isLogin) return;
		// 请求订单各个状态的数量
		const data = await request.easyOrder.orderSearch.queryOrderCount();
		setOrderCount(data.data);
	}, []);
	/** 跳转订单列表 */
	const goOrder = (status: number | undefined) => {
		toggle();
		// 未登录跳转登录
		if (!isLogin) {
			goToLogin();
			return;
		}
		if (status === undefined) {
			navigation(`/order/list`);
			return;
		}
		navigation(`/order/list?activeKey=${status}`);
	};
	return (
		<View
			style={{
				padding: SDK.fitSize(12),
				marginTop: SDK.fitSize(16),
				marginBottom: SDK.fitSize(16),
				backgroundColor: '#FAFBFC',
				borderRadius: SDK.fitSize(12)
			}}
		>
			<View
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					alignItems: 'center',
					marginBottom: SDK.fitSize(12)
				}}
			>
				<Text
					style={{
						fontSize: SDK.fitSize(13.5),
						color: '#1C2026',
						fontWeight: '600',
						fontFamily: 'PingFang SC'
					}}
				>
					{window._$m.t('我的订单')}
				</Text>
				<TouchableOpacity
					onPress={() => goOrder(undefined)}
					style={{
						flexDirection: 'row',
						alignItems: 'center'
					}}
				>
					<Text
						style={{
							marginRight: SDK.fitSize(8),
							color: '#7E8694',
							fontSize: SDK.fitSize(12)
						}}
					>
						{window._$m.t('全部')}
					</Text>
					<MoreMedium />
				</TouchableOpacity>
			</View>
			<View
				style={{
					flexDirection: 'row'
				}}
			>
				{orderStatusList.map((item, index) => {
					return (
						<TouchableOpacity
							onPress={() => goOrder(item.status)}
							key={index}
							style={{
								width: '33%',
								alignItems: 'center',
								justifyContent: 'center'
							}}
						>
							<Badge
								value={
									item.showBage
										? orderCount?.[item.badgeName] || ''
										: ''
								}
								max={99}
								right={SDK.fitSize(-5)}
								style={{
									marginRight: 0,
									width: 'auto',
									height: 'auto'
								}}
							>
								<Image
									style={{
										width: SDK.fitSize(28),
										height: SDK.fitSize(28)
									}}
									source={{
										uri: item.iconUrl
									}}
								/>
							</Badge>

							<Text
								style={{
									fontSize: SDK.fitSize(12),
									marginTop: SDK.fitSize(4)
								}}
							>
								{item.title}
							</Text>
						</TouchableOpacity>
					);
				})}
			</View>
		</View>
	);
};
export default Order;
