/*
 * @Author: yusha
 * @Date: 2023-12-09 19:39:06
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-28 16:48:29
 * @Description:
 */
import Badge from '@nutui/nutui-react-native/lib/module/badge';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { useNavigate } from 'react-router-dom';
import { useAtom, useAtomValue, useSetAtom } from 'jotai';
import { useAsyncEffect, useRequest } from 'ahooks';
import { useEffect, useState } from 'react';
import { Toast } from 'antd-mobile';
import * as SDK from '@snifftest/sdk/lib/rn';
import { Img } from '@snifftest/img';
import { Order, User } from '@/Atoms';
import { goToLogin, request } from '@/config/request/interceptors';
import CustomModal from '@/component/CustomModal';
import { logout } from '@/config/request/login';
import { menuOptions } from '../../config';
const Workbench = ({ toggle }) => {
	const navigation = useNavigate();
	/** 是否登录 */
	const [isLogin, setIsLogin] = useAtom(User.isLogin);
	const setUserInfo = useSetAtom(User.userDetail);
	// 菜单配置
	const _menuOptions = isLogin ? menuOptions : [...menuOptions].splice(2, 2);
	/** 消息未读数量 */
	const [unreadCount, setUnreadCount] = useState<number>(0);
	/** 获取私有未读消息数量 */
	const { runAsync: getPrivateUnreadCount } = useRequest(
		request.customer.notify.getPrivateUnreadCount,
		{
			manual: true
		}
	);
	const { data: couponList, runAsync: apiGetCoupon } = useRequest(
		request.market.coupon.couponCustomerPage,
		{
			manual: true
		}
	);
	const setCartOriginalData = useSetAtom(Order.atomCartOriginalData);
	useAsyncEffect(async () => {
		if (!isLogin) return;
		const params: any = {
			usedStatus: 0,
			pageNum: 1,
			pageSize: 10
		};
		apiGetCoupon(params);
		const data = await getPrivateUnreadCount();
		setUnreadCount(data?.data ?? 0);
	}, []);
	const showService = () => {
		// TODO：如果改此处。客服页面的也要改
		const w: any = window;
		const ChannelIO = w.ChannelIO;
		// 根据站点判断展示哪个语言
		// {language: 'ko' | 'ja' | 'en'}
		const language = 'ja';
		// eslint-disable-next-line new-cap
		ChannelIO('updateUser', {
			language
		});
		// eslint-disable-next-line new-cap
		ChannelIO('showMessenger');
	};
	return (
		<View>
			{_menuOptions.map((item, index) => {
				return (
					<TouchableOpacity
						onPress={() => {
							toggle();
							// 未登录跳转登录
							if (!isLogin) {
								goToLogin();
								return;
							}
							if (item.linkPage === 'logout') {
								CustomModal.confirm({
									content:
										window._$m.t('请确认要退出登录吗？'),
									cancelText: window._$m.t('取消'),
									confirmText: window._$m.t('确定'),
									onConfirm: async () => {
										await request.customer.logout.logout();
										setIsLogin(false);
										setUserInfo({} as any);
										logout();
										navigation('/goods/home');
										Toast.show({
											content:
												window._$m.t('当前账号已退出')
										});
										const cartData = {
											cartNum: 0,
											cartList: []
										};
										setCartOriginalData(cartData);
									}
								});
								return;
							}
							/** 联系客服 */
							if (item.linkPage === 'customService') {
								showService();
								return;
							}
							navigation(item.linkPage);
						}}
						key={index}
						style={{
							paddingTop: SDK.fitSize(11),
							paddingBottom: SDK.fitSize(11),
							flexDirection: 'row',
							alignItems: 'center',
							borderBottomColor: 'rgba(0, 0, 0, 0.05)',
							borderBottomWidth: SDK.fitSize(1)
						}}
					>
						<Badge
							dot={item.showBage && unreadCount}
							right={SDK.fitSize(1)}
							top={SDK.fitSize(1)}
							color="#ff5010"
							style={{
								marginRight: 0,
								width: 'auto',
								height: 'auto'
							}}
						>
							{item.showValue &&
								Boolean(couponList?.data?.total) && (
									<span
										style={{
											color: '#fff',
											backgroundColor: '#FF5010',
											padding: '0 0.06rem',
											borderRadius: '50%',
											position: 'absolute',
											left: '0.1rem',
											top: '-0.04rem',
											zIndex: '1'
										}}
									>
										{couponList?.data?.total}
									</span>
								)}

							<Image
								source={{
									uri: item.icon
								}}
								style={{
									width: SDK.fitSize(20),
									height: SDK.fitSize(20)
								}}
							/>
						</Badge>
						<Text
							style={{
								fontSize: SDK.fitSize(13.5),
								color: '#1C2026',
								fontWeight: '500',
								paddingLeft: SDK.fitSize(8)
							}}
						>
							{item.text}
						</Text>
					</TouchableOpacity>
				);
			})}
		</View>
	);
};
export default Workbench;
