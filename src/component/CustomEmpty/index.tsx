/*
 * @Author: huajian
 * @Date: 2023-11-09 18:37:24
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-03 13:23:50
 * @Description:占位图
 */

import classNames from 'classnames';
import { SpinLoading } from 'antd-mobile';
import styles from './index.module.scss';
export const CustomEmpty = (props) => {
	return (
		<div
			className={classNames(
				styles['container'],
				// styles['empty-page'],
				'special-style'
			)}
		>
			{props.loading ? (
				<SpinLoading
					style={{
						position: 'relative',
						left: '45%',
						top: '50%'
					}}
				/>
			) : (
				<>
					<div className={styles.iconContainer}>
						<img
							className={styles.icon}
							alt="empty"
							src="https://static-jp.theckb.com/static-asset/easy-h5/empty_order.svg"
						/>

						<div className={styles.text}>
							<span>
								{props.emptyTips || window._$m.t('暂无数据')}
							</span>
						</div>
					</div>
				</>
			)}
		</div>
	);
};
