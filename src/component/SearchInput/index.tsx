/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-18 11:27:29
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 15:41:11
 * @FilePath: /ckb-easy-h5-fed/src/component/SearchInput/index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { useRef, useEffect, CSSProperties } from 'react';
import { Input } from 'antd-mobile';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import styles from './index.module.scss';
// import Camera from './Camera';
interface SearchInputProps {
	/** 点击跳转 */
	onClick?: () => void;
	/** 输入内容后进行搜索 */
	onSearch?: (arg: string | undefined) => void;
	/** 是否是纯文本 */
	isText: boolean;
	/** 是否存在照相机 */
	// 一直存在
	// isCamera?: boolean;
	value?: string;
	// 文本样式
	textStyle?: CSSProperties;
}
function SearchInput({
	onClick,
	onSearch,
	value,
	textStyle,
	// isCamera = false
	isText = true
}: SearchInputProps) {
	const searchInput = useRef<any>();
	useEffect(() => {
		if (searchInput.current) {
			searchInput.current.nativeElement.value = value || '';
		}
	}, [value]);
	const searchCapture = (e) => {
		if (isText) {
			e.stopPropagation();
			onClick?.();
		}
	};
	const searchAction = () => {
		const value = searchInput.current?.nativeElement.value;
		onSearch?.(value);
	};
	return (
		<div className={styles['content']} onClick={searchCapture}>
			<div className={styles['contentLeft']}>
				{/* <img
                                                                                                                      alt="search"
                                                                                                                      style={{
                                                                                                                      	width: '.2rem',
                                                                                                                      	marginLeft: '.12rem',
                                                                                                                      	marginTop: '0.05rem'
                                                                                                                      }}
                                                                                                                      src="https://static-s.theckb.com/BusinessMarket/Client/country/flag_JP_tab.png"
                                                                                                                      /> */}
			</div>
			{isText ? (
				<span style={textStyle} className={styles['searchContent']}>
					{value}
				</span>
			) : (
				<Input
					placeholder={window._$m.t('请输入商品名称')}
					className={styles['searchContent']}
					style={{
						'--font-size': '.12rem'
					}}
					ref={searchInput}
					clearable
				/>
			)}

			<div className={styles['contentRight']}>
				{/* {isCamera ? <Camera /> : null} */}
				{/* <Camera /> */}
				<div className={styles['clickSearch']} onClick={searchAction}>
					<img
						alt="search"
						style={{
							width: '.16rem',
							height: '.16rem'
						}}
						src="https://static-s.theckb.com/BusinessMarket/Easy/H5/search.png"
					/>
				</div>
			</div>
		</div>
	);
}
export default SearchInput;
