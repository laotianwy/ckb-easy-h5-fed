/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-16 19:53:31
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 15:41:43
 * @FilePath: /ckb-easy-h5-fed/src/component/SearchInput/Camera/index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/*eslint-disable*/
import { memo, useState, useRef } from 'react';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { Toast } from 'antd-mobile';
import { useRequest } from 'ahooks';
import ModalSelect from '@/component/ModalSelect';
import { useEvent } from '@/utils/hooks/useEvent';
import { uploadImage } from '@/utils/imageUpload';
import { request } from '@/config/request';
import ImageSearch from '@/common/icons/ImageSearch';
import { searchByBase64 } from '@/utils';
import { jsBridge } from '@/utils/jsBridge';
import Xiangjicamera1 from '@/common/icons/Xiangjicamera1';
import styles from './index.module.scss';
interface ChooseItemProps {
  id: number;
  text: string;
}
const CHOOSE_LIST: ChooseItemProps[] = [
{
  id: 1,
  text: window._$m.t('拍摄图片')
},
{
  id: 2,
  text: window._$m.t('从文件中选取')
}];


interface CameraProps {
  color?: string;
  onChange?: (id?: string, imageUrl?: string) => void;
}
const Camera = (props: CameraProps) => {
  const { color = '#B0B7C2', onChange } = props;
  const [visible, setVisible] = useState(false);
  const navigate = useNavigate();
  const chooseCameraRef = useRef(null);
  const chooseImageRef = useRef(null);
  /** 上传图片-图搜 */
  const { runAsync: searchImageUpload, loading: searchImageUploadLoading } =
  useRequest(request.easyGoods.product.searchImageUpload, {
    manual: true
  });
  const openCamera = async (e) => {
    // 阻止冒泡
    e.stopPropagation();
    // 如果是app，跳转到app的图搜列表
    if (window?.ReactNativeWebView) {
      await jsBridge.postMessage({
        type: 'EMIT_CHOOSE_IMAGE_SEARCH',
        payload: {}
      });
      return;
    }
    setVisible(true);
  };
  const onChooseItem = useEvent<(item: ChooseItemProps) => void>((item) => {
    if (item.id === 1) {
      const currentRef = (chooseCameraRef.current as any);
      currentRef?.click();
    } else if (item.id === 2) {
      const currentRef = (chooseImageRef.current as any);
      currentRef?.click();
    }
  });

  // 选择照相机回调
  const chooseCameraImage = (event) => {
    chooseFileCallback(event);
  };

  // 选择图片回调
  const chooseFileCallback = async (event) => {
    if (event.target?.files?.length) {
      const file = event.target.files[0];
      try {
        const uploadToBaseRes = await searchByBase64(file);
        if (!uploadToBaseRes.status) {
          event.target.value = '';
          return;
        }
        const res = await uploadImage(
          {

























            // pathParams: {
            // 	serviceName: 'order',
            // 	bizName: 'payment',
            // 	bizType: 'oemGoodsImg'
            // }
          }, file);const { url } = res;const uploadUrlRes = await searchImageUpload({ // imageUrl: url,
            imageBase64Content: uploadToBaseRes?.data?.split('data:image/jpeg;base64,')[1] });if (uploadUrlRes.success) {if (onChange) {event.target.value = '';onChange?.(uploadUrlRes.data, url);return;}navigate({ pathname: '/goods/imageSearchList', search: `?${createSearchParams({ imageUrl: url, imageId: uploadUrlRes.data || '' })}` });}} catch (ex) {event.target.value = '';Toast.show({ content: JSON.stringify(ex) });}}};
  return (
    <>
			{searchImageUploadLoading &&
      <div
        style={{
          position: 'fixed',
          width: '100vw',
          height: '100vh',
          top: 0,
          left: 0,
          bottom: 0,
          zIndex: '1000',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}>

					<div
          style={{
            width: '.7rem',
            height: '.7rem',
            background: 'rgba(0,0,0,0.65)',
            borderRadius: '.04rem',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }}>

						<img
            style={{ width: '.36rem', height: '.36rem' }}
            src="https://static-s.theckb.com/BusinessMarket/Easy/H5/image-search-loading.gif"
            alt="loading" />

					</div>
				</div>}


			<div
        onClick={openCamera}
        className={styles.camera}
        style={{ color }}>

				{/* 从照相机选择   */}
				<label htmlFor="cameraInput" ref={chooseCameraRef}>
					<input
            type="file"
            accept="image/*"
            id="cameraInput"
            /** @ts-ignore */
            capture="camera"
            style={{ display: 'none' }}
            onChange={chooseCameraImage} />

				</label>
				{/* 从文件选择 */}
				<label htmlFor="photoInput" ref={chooseImageRef}>
					<input
            type="file"
            accept="image/*"
            id="photoInput"
            style={{
              display: 'none'
            }}
            onChange={chooseFileCallback} />

				</label>
				<ModalSelect
          visible={visible}
          setVisible={setVisible}
          chooseList={CHOOSE_LIST}
          onClickSelectItem={onChooseItem} />


				<Xiangjicamera1 />
			</div>
		</>);

};
export default memo(Camera);