/*
 * @Author: yusha
 * @Date: 2023-10-23 11:17:34
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-03 16:47:07
 * @Description: 自定义textarea
 */

import { TextArea, TextAreaProps } from 'antd-mobile';
import { memo } from 'react';
import './index.scss';
interface CustomTextAreaProps extends TextAreaProps {
	ref?: any;
}
const CustomTextArea = (props: CustomTextAreaProps) => {
	const { placeholder = window._$m.t('请输入'), ref } = props;
	return (
		<div className="custom-textarea-style">
			<TextArea ref={ref} placeholder={placeholder} {...props} />
		</div>
	);
};
export default memo(CustomTextArea);
