/*
 * @Author: shiguang
 * @Date: 2023-10-23 15:33:33
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-07 09:53:54
 * @Description:
 */
import { Button, Space } from 'antd-mobile';
import { ReactNode } from 'react';
import styles from './index.module.scss';
interface ResultProps {
	title: ReactNode;
	description: ReactNode;
	cancelText?: ReactNode;
	confirmText: ReactNode;
	onCancel: () => void | Promise<void>;
	onConfirm: () => void | Promise<void>;
	type: 'success' | 'fail';
}
function Result({
	title,
	description,
	cancelText = window._$m.t('返回首页'),
	confirmText,
	onCancel,
	onConfirm,
	type
}: ResultProps) {
	const buttonStyle = {
		padding: '0.12rem 0.16rem'
	};
	return (
		<div className={styles.container}>
			<img
				className={styles.icon}
				alt="success"
				src={
					type === 'success'
						? window._$m.t(
								'https://static-s.theckb.com/BusinessMarket/Easy/H5/成功.png'
							)
						: window._$m.t(
								'https://static-s.theckb.com/BusinessMarket/Easy/H5/失败.png'
							)
				}
			/>

			<div className={styles.title}>{title}</div>
			<div className={styles.desc}>{description}</div>
			<Space>
				<Button shape="rounded" onClick={onCancel} style={buttonStyle}>
					{cancelText}
				</Button>
				<Button
					color="primary"
					shape="rounded"
					onClick={onConfirm}
					style={buttonStyle}
				>
					{confirmText}
				</Button>
			</Space>
		</div>
	);
}
export default Result;
