/*
 * @Author: yusha
 * @Date: 2023-10-18 16:13:41
 * @LastEditors: yusha
 * @LastEditTime: 2024-03-27 14:21:38
 * @Description: 自定义弹出层
 */

import { memo, ReactNode, useState } from 'react';
import CloseFill from '@/common/icons/CloseFill';
import './index.scss';
import CloseOneguanbi from '@/common/icons/CloseOneguanbi';
// import { Mask } from 'antd-mobile';
import Mask from '../Mask';
interface CustomPopupProps {
	/** 显示隐藏 */
	visible: boolean;
	/** 关闭 */
	onClose: () => void;
	/** 外部容器类名 */
	className?: string;
	/** 自定义关闭按钮 */
	customCloseIcon?: ReactNode;
	id?: string;
	children: ReactNode;
	/** 是否支持蒙层关闭 */
	isNeedMaskClose?: boolean;
}
const CustomPopup = (props: CustomPopupProps) => {
	const {
		visible,
		onClose,
		className = '',
		customCloseIcon,
		isNeedMaskClose,
		id = ''
	} = props;
	/** 获取关闭按钮 */
	const getCustomCloseIcon = () => {
		if (customCloseIcon) {
			return customCloseIcon;
		}
		return (
			<div onClick={onClose} className="popup-close">
				<CloseOneguanbi
					style={{
						width: '0.26rem',
						height: '0.26rem'
					}}
				/>
			</div>
		);
	};
	return (
		<>
			{visible && (
				<Mask
					// destroyOnClose
					visible={visible}
					// getContainer={() => document.getElementById('root')!}
					// style={
					// 	{
					// 		'--z-index': 9999
					// 	} as any
					// }
					className={`custom-popup ${className}`}
					onMaskClick={(e) => {
						e.preventDefault();
						e.stopPropagation();
						if (!isNeedMaskClose) {
							return;
						}
						onClose();
					}}
				>
					<div className="popup-content">
						<div className="popup-close-area">
							{getCustomCloseIcon()}
						</div>
						{props.children}
					</div>
				</Mask>
			)}
		</>
	);
};
export default memo(CustomPopup);
