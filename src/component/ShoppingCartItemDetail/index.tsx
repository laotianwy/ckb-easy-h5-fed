/*
 * @Author: yusha
 * @Date: 2023-10-16 16:38:40
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-14 20:19:30
 * @Description: 单个item
 */
import { memo, useState } from 'react';
import qs from 'query-string';
import { Button, Checkbox, Stepper } from 'antd-mobile';
import { createSearchParams, useLocation, useNavigate } from 'react-router-dom';
import { useRequest, useCountDown } from 'ahooks';
import { changeBase64ToImageId, formatMoney, navigateAppOrH5 } from '@/utils';
import { OrderProductDTO, CouponInfoDTO } from '@/service/easyOrder';
import ZhanweituCkb from '@/common/icons/ZhanweituCkb';
import { Enum_SellStatus } from '@/common/Enums';
import CheckboxDefault from '@/common/icons/CheckboxDefault';
import CheckboxChecked from '@/common/icons/CheckboxChecked';
import { request } from '@/config/request';
import Loading from '@/component/Loading';
import ActiveTag from '@/component/ActiveTag';
import GoodsCode from '@/component/GoodsCode';
import { jsBridge } from '@/utils/jsBridge';
import { CustomRMImg } from '@/component/CustomRMImg';
import './index.scss';
interface ItemDetailProps {
	// value: string | number;
	couponInfo?: CouponInfoDTO;
	disabled?: boolean;
	item: any;
	handleChangeQuantity?: (val) => void;
	/** 哪个页面引用 */
	fromPage?: string;
}
/** 商品售卖状态 */
export enum Enum_SellStatusString {
	/** 正常 */
	Normal = '1',
	/** 售罄 */
	SoldOut = '2',
	/** 下架 */
	delist = '3'
}
const ItemDetail = (props: ItemDetailProps) => {
	const {
		disabled = false,
		item,
		couponInfo,
		handleChangeQuantity,
		fromPage = ''
	} = props;
	const { cartIds, orderNo } = qs.parse(useLocation().search);
	const [countdown, formattedRes] = useCountDown({
		targetDate: item.productDiscount?.endTime,
		onEnd: () => {
			window.location.reload();
		}
	});
	const { days, hours, minutes, seconds } = formattedRes;
	/** 来自下单页 */
	const isFromConfirmOrder = fromPage === 'confirmOrder';
	const {
		productImg,
		cartId,
		productName,
		productNo,
		productPropertiesName,
		productTotalAmount,
		productQuantity = 0,
		oneBuyFlag,
		freeShippingAmount = 0,
		sellStatus,
		productSku,
		productCode,
		productPrice,
		newUserFlag,
		discountTotalAmount,
		couponDiscountTotalAmount,
		discountAmount,
		payableAmount,
		isUseCoupon,
		freeShipping
	} = item;
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);

	const disabledText = {
		[Enum_SellStatus.Normal]: window._$m.t('正常'),
		[Enum_SellStatus.SoldOut]: window._$m.t('售罄'),
		[Enum_SellStatus.delist]: window._$m.t('已下架')
	};
	/** 上传图片-图搜 */
	const { runAsync: searchImageUpload, loading: searchImageUploadLoading } =
		useRequest(request.easyGoods.product.searchImageUpload, {
			manual: true
		});
	/** 搜同款 */
	const searchSameType = async () => {
		setLoading(true);
		try {
			const data = await changeBase64ToImageId({
				url: productImg,
				request: searchImageUpload
			});
			setLoading(searchImageUploadLoading);

			// 如果是app，跳转到app的图搜列表
			if (window?.ReactNativeWebView) {
				await jsBridge.postMessage({
					type: 'EMIT_CHOOSE_IMAGE_SEARCH',
					payload: {
						imageUrl: productImg ?? '',
						imageId: data.imageId ?? ''
					}
				});
				return;
			}
			// 跳转图搜列表
			navigate({
				pathname: '/goods/imageSearchList',
				search: `?${createSearchParams({
					imageUrl: productImg ?? '',
					imageId: data.imageId ?? ''
				})}`
			});
		} catch {
			setLoading(false);
		}
	};
	/** 获取折扣金额 */
	const getDiscountAmount = () => {
		// 如果来自下单页，需要做特殊处理，只展示couponDiscountTotalAmount
		if (isFromConfirmOrder && !orderNo) {
			return couponDiscountTotalAmount;
		}
		return (
			couponDiscountTotalAmount || discountAmount || discountTotalAmount
		);
	};
	return (
		<>
			{loading && <Loading />}
			<div
				className={`sigle-item-cart ${disabled ? 'disabled-item-cart' : ''}`}
				id={`sku-${productSku}`}
			>
				<div
					style={{
						display: 'flex'
					}}
				>
					<div>
						<div
							style={{
								display: 'flex'
							}}
						>
							{!isFromConfirmOrder && (
								<Checkbox
									className="item-checkbox"
									icon={(checked) =>
										checked ? (
											<CheckboxChecked
												color={'#1C2026'}
												width={'0.16rem'}
											/>
										) : (
											<CheckboxDefault
												width={'0.16rem'}
											/>
										)
									}
									value={cartId}
								/>
							)}

							<div className="item-img">
								{productImg ? (
									<CustomRMImg
										style={{
											height: '0.64rem',
											width: '0.64rem',
											borderRadius: '0.04rem'
										}}
										src={productImg}
										imgSize={70}
										alt=""
									/>
								) : (
									<ZhanweituCkb className="item-img-default" />
								)}

								{disabled && (
									<div className="disable-status-style">
										<div className="style-text">
											<span>
												{disabledText[sellStatus ?? '']}
											</span>
										</div>
									</div>
								)}
							</div>
						</div>
					</div>
					<div className="item-content">
						<div
							onClick={() => {
								if (isFromConfirmOrder) return;
								if (disabled) return;
								navigateAppOrH5({
									h5Url: '/goods/detail',
									params: {
										productCode
									},
									appPageType: 'GoodDetail',
									navigate
								});
							}}
							className="item-good-title"
						>
							{productName}
						</div>
						{productNo && (
							<div
								style={{
									width: 'fit-content',
									display: 'inline-block'
								}}
							>
								<GoodsCode product={productNo} />
								<div />
							</div>
						)}
					</div>
				</div>
				<div
					style={{
						paddingLeft: `${isFromConfirmOrder ? '' : '.28rem'}`
					}}
				>
					{!orderNo && (
						<>
							{!!freeShippingAmount &&
								sellStatus === Enum_SellStatus.Normal &&
								freeShipping !== false && (
									<ActiveTag
										text={
											(newUserFlag
												? window._$m.t('新人专享！')
												: '') +
											(freeShippingAmount
												? window._$m.t(
														'订单满{{num}}円以上包邮',
														{
															data: {
																num: formatMoney(
																	freeShippingAmount
																)
															}
														}
													)
												: '')
										}
									/>
								)}
							{sellStatus === Enum_SellStatus.Normal &&
								freeShipping === false && (
									<ActiveTag
										text={window._$m.t('不参与包邮活动')}
										className="no-free-style"
									/>
								)}

							{!!oneBuyFlag && (
								<div className="active-tag">
									<div className="inline-block">
										<div className="postage-tag">
											<span>
												{window._$m.t('1日元活动')}
											</span>
										</div>
									</div>
								</div>
							)}
						</>
					)}

					{!!productPropertiesName && !disabled && (
						<div className="item-good-sku">
							{productPropertiesName}
						</div>
					)}

					{isFromConfirmOrder && !disabled && (
						<div className="item-amount">
							<span className="amount">
								{formatMoney(productPrice)}
								{window._$m.t('円')}
								<span
									style={{
										marginLeft: '0.04rem',
										fontWeight: '500'
									}}
								>
									x{productQuantity}
								</span>
							</span>
							{!!item.productDiscount && (
								<div className="limit-time">
									<div
										style={{
											marginRight: '0.04rem'
										}}
									>
										{window._$m.t('限时')}
									</div>
									<div>
										<span className="time-box">
											{days < 10 ? '0' + days : days}
										</span>
										{window._$m.t('天')}
									</div>
									<div>
										<span className="time-box">
											{hours < 10 ? '0' + hours : hours}
										</span>
										{window._$m.t('时')}
									</div>
									<div>
										<span className="time-box">
											{minutes < 10
												? '0' + minutes
												: minutes}
										</span>
										{window._$m.t('分')}
									</div>
									<div>
										<span className="time-box">
											{seconds < 10
												? '0' + seconds
												: seconds}
										</span>
										{window._$m.t('秒')}
									</div>
								</div>
							)}

							<span
								style={{
									fontSize: '0.14rem',
									fontWeight: '600'
								}}
							>
								{productTotalAmount}
								{window._$m.t('円')}
							</span>
						</div>
					)}

					{!isFromConfirmOrder && !disabled && (
						<div
							className="item-amount"
							onClick={(e) => {
								e.preventDefault();
								e.stopPropagation();
							}}
						>
							<div className="amount-time">
								<span className="amount">
									{formatMoney(productPrice)}
									{window._$m.t('円')}
								</span>
								{!!item.productDiscount && (
									<div className="limit-time">
										<div>{window._$m.t('限时')}</div>
										<div>
											<span className="time-box">
												{days < 10 ? '0' + days : days}
											</span>
											{window._$m.t('天')}
										</div>
										<div>
											<span className="time-box">
												{hours < 10
													? '0' + hours
													: hours}
											</span>
											{window._$m.t('时')}
										</div>
										<div>
											<span className="time-box">
												{minutes < 10
													? '0' + minutes
													: minutes}
											</span>
											{window._$m.t('分')}
										</div>
										<div>
											<span className="time-box">
												{seconds < 10
													? '0' + seconds
													: seconds}
											</span>
											{window._$m.t('秒')}
										</div>
									</div>
								)}
							</div>

							<Stepper
								digits={0}
								onChange={(val) => {
									handleChangeQuantity?.({
										cartId,
										num: val
									});
								}}
								value={productQuantity}
								disabled={disabled}
								min={1}
								max={99999}
							/>
						</div>
					)}

					{isUseCoupon && (
						<>
							<div className="coupon-box">
								<span className="coupon-couponName">
									{couponInfo?.couponName}
								</span>
								<span className="coupon-discount">
									-{getDiscountAmount()}
									{window._$m.t('円')}
								</span>
							</div>
							<div
								style={{
									display: 'flex',
									justifyContent: 'end',
									fontWeight: '600',
									fontSize: '0.14rem',
									marginTop: '0.04rem'
								}}
							>
								<span
									style={{
										marginRight: '0.04rem'
									}}
								>
									{window._$m.t('优惠后金额')}
								</span>
								<span>
									{payableAmount}
									{window._$m.t('円')}
								</span>
							</div>
						</>
					)}
				</div>
			</div>
		</>
	);
};
export default memo(ItemDetail);
