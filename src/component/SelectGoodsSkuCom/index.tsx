/*
 * @Author: yusha
 * @Date: 2024-04-03 15:02:56
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-10 18:56:11
 * @Description: 商品，可以选择sku组件
 */

import { memo, useEffect, useState } from 'react';
import { useRequest } from 'ahooks';
import { ProductDetailRespDTO } from '@/service/easyGoods';
import { request } from '@/config/request';
import { SpuProps } from '@/outs/container/GoodsDetail/context';
import { ProductReq } from '@/service/easyOrder';
import { formatMoney, getMergedArray } from '@/utils';
import { ProductVO } from '@/service/market';
import { ON_HAND_GOODS_SELECT_SESSION_KEY } from '@/const/config';
import { CustomImg } from '../CustomImg';
import ModalSelectSku from './ModalSelectSku';
import './index.scss';

/** 未选择icon */
const SELECT_DEFAULT_ICON =
	'https://static-jp.theckb.com/static-asset/easy-h5/Checkone@2x.png';
/** 已选择icon */
const SELECTED_ICON =
	'https://static-jp.theckb.com/static-asset/easy-h5/selectedIcon@3.x.png';

interface SelectGoodsSkuComProps {
	/** 跳转商详 */
	goToGoodsDetail?: (productCode: string) => void;
	/** 确认选择 */
	handleChange?: (val: ProductReq[]) => void;
	/** 商品信息 */
	goodsInfo: ProductVO;
	/** 点击选择sku给添加个loading，防止请求过久用户体验不好 */
	setLoading: (val: boolean) => void;
}
const SelectGoodsSkuCom = (props: SelectGoodsSkuComProps) => {
	const { goToGoodsDetail, handleChange, goodsInfo, setLoading } = props;
	const {
		productMainImg,
		productTitle,
		productCode,
		// highPriceShow,
		activitySellPriceJpy,
		productLowestPrice
	} = goodsInfo;
	const [visible, setVisible] = useState(false);
	/** 是否加购 */
	const [hasBought, setHasBought] = useState(goodsInfo.hasBought ?? false);
	const [product, setProduct] = useState<ProductDetailRespDTO>();
	/** 一级类目售罄数量状态 */
	const [firstSpuSoldOut, setFirstSpuSoldOut] = useState<SpuProps>({});
	const { runAsync: apiGetProduct } = useRequest(
		request.easyGoods.product.detail,
		{
			manual: true
		}
	);
	// 更新是否加购
	useEffect(() => {
		setHasBought(goodsInfo.hasBought);
	}, [goodsInfo.hasBought]);
	/** 获取sku信息，重新请求商详接口 */
	const getProductInfo = async () => {
		try {
			const res = await apiGetProduct({
				productCode
			}).finally(() => {
				setLoading(false);
			});
			if (!(res.success && res.data)) {
				setProduct({});
			}
			setProduct({
				...res.data,
				skuList: res.data.skuList ?? []
			});
			const firstSpu: SpuProps = {};
			if ((res.data?.productPropGroupJaList ?? []).length > 1) {
				res.data?.productPropGroupJaList![0].props?.forEach((item) => {
					firstSpu[item.propKey ?? ''] = 0;
				});
			}
			for (const key in firstSpu) {
				if (Object.prototype.hasOwnProperty.call(firstSpu, key)) {
					res.data?.skuList?.forEach((item) => {
						if (item.productProperties?.includes(key)) {
							firstSpu[key] += item.availableQty || 0;
						}
					});
				}
			}
			setFirstSpuSoldOut(firstSpu);
		} catch (error) {
			setProduct({});
		}
	};
	/** 去选择sku */
	const goToSelectSku = async () => {
		setLoading(true);
		await getProductInfo();
		setVisible(true);
	};
	/** 取消选择 */
	const cancelSelect = () => {
		// 获取存储的商品
		const preSelectedSkuList = window.sessionStorage.getItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY
		);
		let _preSelectedSkuList = preSelectedSkuList
			? JSON.parse(preSelectedSkuList)
			: [];
		_preSelectedSkuList = _preSelectedSkuList.filter(
			(item) => item.productCode !== productCode
		);
		// 获取合并的数组，若是两个数组里面存在相同的sku，则去val里面的内容，两者都不相同则取并集
		const mergedArray = getMergedArray(
			_preSelectedSkuList,
			[],
			'productSku',
			true
		);
		setHasBought(false);
		handleChange(mergedArray);
	};

	return (
		<div className="component-select-goods-sku" onClick={goToSelectSku}>
			<CustomImg className="img-style" src={productMainImg} />
			<div className="good-title">{productTitle}</div>
			<div className="good-price">
				<div>
					{
						// highPriceShow &&
						activitySellPriceJpy &&
						activitySellPriceJpy < productLowestPrice ? (
							<span>
								<span className="current-price">
									{activitySellPriceJpy}
									{window._$m.t('元')}
								</span>
								<span className="original-price">
									{productLowestPrice}
									{window._$m.t('元')}
								</span>
							</span>
						) : (
							<span className="current-price">
								{formatMoney(productLowestPrice)}
								{window._$m.t('円')}
							</span>
						)
					}
				</div>
				<img
					alt="select"
					className="select-img"
					src={hasBought ? SELECTED_ICON : SELECT_DEFAULT_ICON}
					onClick={(e) => {
						e.stopPropagation();
						if (hasBought) {
							cancelSelect();
							return;
						}
						goToSelectSku();
					}}
				/>
			</div>
			{product && (
				<ModalSelectSku
					goToGoodsDetail={goToGoodsDetail}
					firstSpuSoldOut={firstSpuSoldOut}
					visible={visible}
					onClose={() => setVisible(false)}
					product={product}
					handleChange={(val) => {
						const isHasBought = val.length
							? val.find(
									(item) => item.productCode === productCode
								)
							: false;
						// 如果确认选择，则将状态设置为true
						setHasBought(Boolean(isHasBought));
						handleChange(val);
					}}
				/>
			)}
		</div>
	);
};
export default memo(SelectGoodsSkuCom);
