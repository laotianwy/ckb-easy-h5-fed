/* eslint-disable max-lines */
/*
 * @Author: yusha
 * @Date: 2024-04-03 15:41:07
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-13 15:11:23
 * @Description: 选择sku弹窗
 */
import { memo, useState, useMemo, useEffect, useRef } from 'react';
import { Stepper, Toast } from 'antd-mobile';
import classNames from 'classnames';
import { flushSync } from 'react-dom';
import {
	ProductDetailRespDTO,
	ProductPropDTO,
	ProductPropGroupDTO,
	SkuDetailRespDTO
} from '@/service/easyGoods';
import { formatMoney, getMergedArray } from '@/utils';
import { CartAddReq, ProductReq } from '@/service/easyOrder';
import { PRODUCT_TYPE } from '@/common/Enums';
import { SpuProps } from '@/outs/container/GoodsDetail/context';
import { useEvent } from '@/utils/hooks/useEvent';
import MoreSmallsize from '@/common/icons/MoreSmallsize';
import { ON_HAND_GOODS_SELECT_SESSION_KEY } from '@/const/config';
import { request } from '@/config/request';
import CustomPopup from '../../CustomPopup';
import { CustomRMImg } from '../../CustomRMImg';
import './index.scss';

interface ModalSelectSkuProps {
	visible: boolean;
	onClose: () => void;
	product: ProductDetailRespDTO;
	firstSpuSoldOut?: SpuProps;
	goToGoodsDetail?: (productCode: string) => void;
	handleChange?: (val: ProductReq[]) => void;
}

/** 选择sku后的SPU数量集合 */
type AmountProps = Record<string, number>;

/** 选择后的props */
interface SPUProps extends ProductPropGroupDTO {
	selected?: ProductPropDTO;
}

/** 包含销售数量的 */
interface SkuNumDetail extends SkuDetailRespDTO {
	num?: number;
}

/** 最后的一个SPU */
interface lastSpuListItemProps extends ProductPropDTO {
	sku: SkuNumDetail;
}
const ModalSelectSku = (props: ModalSelectSkuProps) => {
	const {
		visible,
		onClose,
		product = {},
		firstSpuSoldOut,
		goToGoodsDetail,
		handleChange
	} = props;
	/** 存在sessionStorage已经选择的sku */
	const selectedSku = window.sessionStorage.getItem(
		ON_HAND_GOODS_SELECT_SESSION_KEY
	);
	/** 用于处理顺手捎页面选择sku反显，不会一直触发 */
	const httpcurr = useRef(0);
	/** 图片预览 */
	const [previewUrl, setPreviewUrl] = useState('');
	const {
		/** spu列表 */
		productPropGroupJaList = [],
		/** sku列表 */
		skuList = []
	} = product;
	/** sku数量统计 */
	const [skuAmount, setSkuAmount] = useState<AmountProps>({});
	/** SPU数量 */
	const [spuAmount, setSpuAmount] = useState<AmountProps>({});
	/** 除最后一个外的spu */
	const [otherSpuList, setOtherSpuList] = useState<SPUProps[]>([]);
	const [lastSpuBySku, setLastSpuBySku] = useState<lastSpuListItemProps[]>(
		[]
	);

	/** 利用sku的key去除 不需要的规格属性productPropGroupList */
	const filterNoSkuProps = (
		propList: ProductPropGroupDTO[],
		skus: SkuDetailRespDTO[]
	) => {
		if (!skus?.length || !propList?.length) {
			return [];
		}
		let keys: string[] = [];
		skus.forEach((item) => {
			keys = [...keys, ...(item?.productProperties?.split(';') ?? [])];
		});
		return propList.map((item) => {
			return {
				...item,
				props: item?.props?.filter((i) =>
					keys.includes(i.propKey || '')
				)
			};
		});
	};
	/** 计算过滤后的spu。sku包含的spu */
	const filterSPUList = useMemo(
		() => filterNoSkuProps(productPropGroupJaList, skuList),
		[productPropGroupJaList, skuList]
	);
	/** 初始化后默认选择第一个spu */
	useEffect(() => {
		const props = filterSPUList.slice(0, -1).map((item: SPUProps) => {
			item.selected = item.props?.[0];
			return item;
		});
		setOtherSpuList(props);
		// 初始化默认选择第一个spu之后，规格图片也要进行改变
		setPreviewUrl(props?.[0]?.props?.[0].url || product.productMainImg!);
	}, [filterSPUList, product.productMainImg]);

	/** 计算之前选择中的spu的props属性 */
	const seletSpuProps = useMemo(() => {
		return otherSpuList.map((item) => item.selected?.propKey);
	}, [otherSpuList]);
	/** arr2map 获取映射 */
	function arr2map<T>(proplist: T[], propfn = (v: T) => ''): Map<string, T> {
		const map = new Map();
		proplist.forEach((v) => {
			map.set(propfn(v), v);
		});
		return map;
	}
	/** 所有的sku都转化为productProperties的集合为主键 */
	const skuToMap = useMemo(() => {
		return arr2map(skuList, (v) => v.productProperties || '');
	}, [skuList]);

	/** 最后一个spu */
	const lastSpuProps = useMemo(
		() => filterSPUList[filterSPUList.length - 1],
		[filterSPUList]
	);

	/** 最后一个spu，挂载所有的sku */
	useEffect(() => {
		const lastPropsSku = lastSpuProps?.props
			?.map((prop) => {
				// 之前选中的props的key+最后一级spu中key组合城sku的key，然后获取sku数据
				const sku =
					(skuToMap.get(
						seletSpuProps.concat(prop.propKey).join(';')
					) as SkuNumDetail) ?? {};
				const nProps = {
					...prop,
					sku
				};
				return nProps;
			})
			.filter((prop) => prop.sku.productCode);
		lastPropsSku && setLastSpuBySku(lastPropsSku);
	}, [lastSpuProps, seletSpuProps, skuToMap]);
	/** 初始化 */

	/** 计算SPU数量 */
	const getSpuNum = (key: string, skuAmount: AmountProps) => {
		return Object.keys(skuAmount)
			.filter((item) => item.includes(key))
			.reduce((pre, cur) => {
				return pre + skuAmount[cur];
			}, 0);
	};
	/** 改变spu数量，计步器加减 */
	const changeSku = useEvent((e: number, key: string = '') => {
		const newSkuAmount = {
			...skuAmount,
			[key]: e
		};
		const newSpuAmount = {
			...spuAmount
		};
		// 需要重新赋值，不然选中的数量，spu上不会展示
		const firstProps = otherSpuList[0];
		if (firstProps) {
			// 修改sku为041能匹配到04和041的问题
			const firstSkuAmount = convertedObject(newSkuAmount);
			firstProps?.props?.forEach(({ propKey = '' }) => {
				newSpuAmount[propKey] = getExactEqualitySpuNum(
					propKey,
					firstSkuAmount
				);
			});
			// firstProps?.props?.forEach(({ propKey = '' }) => {
			// 	newSpuAmount[propKey] = getSpuNum(propKey, newSkuAmount);
			// });
		}
		setSpuAmount({
			...newSpuAmount
		});
		setSkuAmount(newSkuAmount);
	});
	/** 用于将 */
	// {'0:0;1:0': 1, '0:41;1:0': 1}
	const convertedObject = (originaObj) => {
		const nowNewObj = {};

		for (const key in originaObj) {
			if (Object.prototype.hasOwnProperty.call(originaObj, key)) {
				const splitKeys = key.split(';');
				const newKey = splitKeys[0];
				const value = originaObj[key];
				// 获取原始对象中的值
				if (Object.prototype.hasOwnProperty.call(nowNewObj, newKey)) {
					// 如果转换后的对象中已经存在该键，则将值累加
					nowNewObj[newKey] += value;
				} else {
					// 如果转换后的对象中不存在该键，则直接赋值
					nowNewObj[newKey] = value;
				}
			}
		}

		return nowNewObj;
	};
	/** 获取完全相等的key的spu数量 */
	const getExactEqualitySpuNum = (key: string, skuAmount: AmountProps) => {
		return Object.keys(skuAmount)
			.filter((item) => item === key)
			.reduce((pre, cur) => {
				return pre + skuAmount[cur];
			}, 0);
	};
	// 当为顺手捎页面，选择sku，需要将之前选择的值带上，并展示
	useEffect(() => {
		const _data = selectedSku ? JSON.parse(selectedSku) : [];
		// 当之前没有选择sku或者已经请求过或者弹窗关闭时不走下面逻辑
		if (!_data.length || httpcurr.current > 1 || !visible) return;
		httpcurr.current = httpcurr.current + 1;
		// 用于存放匹配中的sku的详细信息
		const arr = [];
		// 轮询，将之前加购的数量反显在页面上
		_data.forEach((item) => {
			const itemProductSku = item.productSku;
			const obj = product.skuList.find(
				(skuItem) => skuItem.productSku === itemProductSku
			);
			if (!obj || !obj?.productProperties) {
				return;
			}
			// 将匹配上的信息加入arr
			arr.push({ [obj.productProperties]: item.quantity });
		});
		// 将sku数组转为对象
		const obj = arr.reduce((acc, cur) => {
			return Object.assign(acc, cur);
		}, {});
		// 新的sku
		const newSkuAmount = {
			...skuAmount,
			...obj
		};
		// 转换一下spu
		const firstProps = otherSpuList[0];
		const newSpuAmount = {
			...spuAmount
		};
		if (firstProps) {
			// 第一层的sku，比如将{'0:0;1:0': 1, '0:41;1:0': 1}变为{'0:0': 1, '0:41': 1}
			const firstSkuAmount = convertedObject(newSkuAmount);
			firstProps?.props?.forEach(({ propKey = '' }) => {
				newSpuAmount[propKey] = getExactEqualitySpuNum(
					propKey,
					firstSkuAmount
				);
			});
		}
		setSpuAmount(newSpuAmount);
		setSkuAmount(newSkuAmount);
		// 会自动添加SkuAmount依赖，导致死循环
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [otherSpuList, product.skuList, selectedSku]);

	/** 选择spu */
	const selectPropItem = (
		idx: number,
		j: ProductPropDTO,
		url: string | undefined
	) => {
		if (idx >= 0) {
			const newProps = otherSpuList.slice(0);
			newProps[idx].selected = j;
			setOtherSpuList(newProps);
		}
		url && setPreviewUrl && setPreviewUrl(url);
	};
	/** 商品售价计算 */
	const sellprice = useMemo<string>(() => {
		const min = formatMoney(product.productSellLowestPriceJa);
		const max = formatMoney(product.productSellHighestPriceJa);
		if (min === max) {
			return `${min}`;
		}
		return `${min} - ${max}`;
	}, [product.productSellHighestPriceJa, product.productSellLowestPriceJa]);

	/** 活动售价计算 */
	const activePrice = useMemo<string>(() => {
		const min = formatMoney(product.activityInfo?.activityLowestPriceJa);
		const max = formatMoney(product.activityInfo?.activityHighestPriceJa);
		if (min === max) {
			return `${min}`;
		}
		return `${min} - ${max}`;
	}, [
		product.activityInfo?.activityLowestPriceJa,
		product.activityInfo?.activityHighestPriceJa
	]);
	/** 获取最大数量 */
	const getNumMax = (num: number) => {
		// 1元活动，最大数量只能为1
		if (product.oneBuyFlag) {
			return 1;
		}
		if (product.productType === PRODUCT_TYPE.ON_LINE) {
			return 99999;
		}
		return num;
	};
	const allAccount = useMemo(() => {
		const allAccount = Object.keys(skuAmount).reduce((pre, cur) => {
			return pre + skuAmount[cur];
		}, 0);
		return allAccount;
	}, [skuAmount]);
	/** 获取购物选择商品列表 */
	const getShopList = () => {
		const list: CartAddReq = {
			skuList: []
		};
		if (Object.keys(skuAmount).length) {
			Object.keys(skuAmount).forEach((i) => {
				const productSku = skuList.find(
					(sku) => sku.productProperties === i
				)?.productSku;
				skuAmount[i] &&
					list.skuList?.push({
						productSku,
						quantity: skuAmount[i]
					});
			});
		}
		return list;
	};
	/** 校验选择的商品 */
	const verifyHanderSelectSku = async (list) => {
		if (list.length > 200) {
			Toast.show(window._$m.t('商品SKU不能超过200'));
			return { status: false, data: [] };
		}
		if (product.oneBuyFlag && allAccount > 1) {
			Toast.show(window._$m.t('活动商品不能超过1个'));
			return { status: false, data: [] };
		}
		const orderShopList: ProductReq[] = list.map((i) => {
			return {
				...i,
				productCode: product.productCode,
				// 商品类型，1：顺手捎，不传：商详页下单
				productType: 1
			};
		});
		const preSelectedSkuList = window.sessionStorage.getItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY
		);
		let _preSelectedSkuList = preSelectedSkuList
			? JSON.parse(preSelectedSkuList)
			: [];
		// 如果数量全部清空,需要将之前存储的加购商品清空
		if (!list?.length) {
			_preSelectedSkuList = _preSelectedSkuList.filter(
				(item) => item.productCode !== product.productCode
			);
		}
		// 获取合并的数组，若是两个数组里面存在相同的sku，则去val里面的内容，两者都不相同则取并集
		const mergedArray = getMergedArray(
			_preSelectedSkuList,
			orderShopList,
			'productSku',
			true
		);
		// 如果数量为空，不需要请求接口
		if (!mergedArray.length) {
			return { status: true, data: [] };
		}

		try {
			const data = await request.easyOrder.order.calculatePrice({
				productList: mergedArray
			});
			if (!data.success) {
				Toast.show(data.msg);
				return { status: false, data: [] };
			}
			return { status: true, data: mergedArray };
		} catch (error) {
			error?.msg && Toast.show(error.msg);
			return { status: false, data: [] };
		}
	};
	/** 确认选择 */
	const handleSelect = async () => {
		const list = getShopList().skuList;
		const { status, data } = await verifyHanderSelectSku(list);
		// 报错，则直接return
		if (!status) return;
		handleChange(data);
		// 清空弹窗选择信息
		setSpuAmount({});
		setSkuAmount({});
		httpcurr.current = 0;
		onClose();
	};
	return (
		<CustomPopup
			visible={visible}
			onClose={() => {
				httpcurr.current = 0;
				setSpuAmount({});
				setSkuAmount({});
				onClose();
			}}
			className="modal-select-sku-com"
		>
			<div className="modal-cart">
				{/* 商品标题区域 */}
				<div className="shopp-title">
					<div className="shop-img">
						<CustomRMImg
							customizeMaskClass="mask-zindex"
							src={previewUrl}
							imgSize={200}
							isPreView={true}
						/>
					</div>
					<div className="right-view">
						<div className="title">
							<span className="product-title">
								{product.productTitle}
							</span>
							{goToGoodsDetail && (
								<span
									className="look-detail"
									onClick={() => {
										onClose();
										httpcurr.current = 0;
										setSpuAmount({});
										setSkuAmount({});
										goToGoodsDetail(product.productCode);
									}}
								>
									{window._$m.t('查看商详')}

									<MoreSmallsize />
								</span>
							)}
						</div>
						<div className="price-range">
							{product.activityInfo ? activePrice : sellprice}
							{window._$m.t('円')}
						</div>
					</div>
				</div>

				{/* 规格区域 */}
				{otherSpuList.map((item, otherRowIndex) => (
					<div className="spec-view" key={item.type}>
						<div className="spec-title">{item.type}</div>
						<div className="spec-list">
							{item.props?.map((prop) => {
								const num = spuAmount[prop.propKey ?? ''];
								return (
									<div
										key={prop.propKey}
										className={classNames({
											'spec-name': true,
											active: item.selected === prop
										})}
										onClick={() =>
											selectPropItem(
												otherRowIndex,
												prop,
												productPropGroupJaList.length >=
													2 && otherRowIndex === 0
													? prop.url
													: ''
											)
										}
									>
										{productPropGroupJaList.length >= 2 &&
											otherRowIndex === 0 && (
												<img src={prop.url} alt="" />
											)}

										<div>
											{num ? (
												<div className="choose-num">
													<span className="icon">
														x
													</span>
													{num > 99 ? '99+' : num}
												</div>
											) : null}
										</div>
										{otherRowIndex === 0 &&
										(firstSpuSoldOut[prop.propKey ?? ''] ??
											0) <= 0 ? (
											<div className="no-store">
												{window._$m.t('售罄')}
											</div>
										) : null}
										{prop.propValue}
									</div>
								);
							})}
						</div>
					</div>
				))}

				{/* sku加减数量区域 */}
				<div className="spec-add-reduce-area">
					<div className="last-spec-name">{lastSpuProps?.type}</div>
					{lastSpuBySku.map((item, index) => (
						<div
							className="spec-row"
							key={item.propKey}
							onClick={() =>
								selectPropItem(
									-1,
									item,
									productPropGroupJaList.length === 1
										? item.url
										: ''
								)
							}
						>
							{item.url && productPropGroupJaList.length === 1 ? (
								<CustomRMImg
									key={index}
									className="img"
									src={item.url}
									imgSize={50}
									alt=""
								/>
							) : null}
							<div className="title-middle">
								<div className="many-spec-title">
									{item.propValue}
								</div>
								<div className="price-store">
									<span className="left">
										{!product.activityInfo
											? formatMoney(item.sku.sellPriceJa)
											: formatMoney(
													item.sku.activityPriceJa
												)}
										{window._$m.t('円')}
									</span>
								</div>
							</div>
							<div className="add-reduce">
								<Stepper
									disabled={(item.sku.availableQty ?? 0) <= 0}
									inputReadOnly={false}
									max={getNumMax(item.sku.availableQty)}
									min={0}
									defaultValue={0}
									value={
										skuAmount[
											item.sku.productProperties ?? ''
										] || 0
									}
									digits={0}
									onChange={(value) => {
										changeSku(
											value,
											item.sku.productProperties
										);
									}}
								/>
							</div>
						</div>
					))}
				</div>
				{/* 确认按钮 */}
				<div className="fixed-btn-view">
					<div>
						<div className="join-number">
							{window._$m.t('已选{{num}}件', {
								data: {
									num: allAccount
								}
							})}
						</div>
					</div>
					<div className="btn" onClick={handleSelect}>
						{window._$m.t('确认选择')}
					</div>
				</div>
			</div>
		</CustomPopup>
	);
};
export default memo(ModalSelectSku);
