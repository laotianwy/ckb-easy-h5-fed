/*
 * @Author: yusha
 * @Date: 2023-10-23 14:50:27
 * @LastEditors: yusha
 * @LastEditTime: 2023-10-28 13:23:33
 * @Description: 自定义select
 */

import { Picker, PickerProps } from 'antd-mobile';
import { memo, useState } from 'react';
import {
	PickerColumnItem,
	PickerColumn,
	PickerValue
} from 'antd-mobile/es/components/picker-view';
import MoreMedium from '@/common/icons/MoreMedium';
import './index.scss';
type PickerPropss = Omit<PickerProps, 'columns' | 'value' | 'onChange'>;
interface CustomSelectProps extends PickerPropss {
	options?: PickerColumn;
	value?: PickerValue;
	placeholder?: string;
	onChange?: (val) => void;
}
// type CustomSelectPropss = Omit<CustomSelectProps, 'columns'>;
const CustomSelect = (props: CustomSelectProps) => {
	const {
		value,
		placeholder = window._$m.t('请选择'),
		onChange,
		options = [],
		...reset
	} = props;
	// 选择的值
	const selectValue =
		value !== undefined && value !== null
			? (
					options?.find((item: any) => {
						return item.value === value;
					}) as PickerColumnItem
				)?.label
			: null;
	const [visible, setVisible] = useState(false);
	return (
		<>
			<div
				className="custom-select-style"
				onClick={() => {
					setVisible(true);
				}}
			>
				{selectValue !== undefined && selectValue !== null && (
					<div>{selectValue as string}</div>
				)}

				{(selectValue === undefined || selectValue === null) && (
					<div className="placeholder-style">{placeholder}</div>
				)}

				<MoreMedium />
			</div>
			<Picker
				{...reset}
				defaultValue={
					value !== undefined && value !== null
						? ([value] as PickerValue[])
						: []
				}
				columns={[options]}
				visible={visible}
				onClose={() => {
					setVisible(false);
				}}
				onConfirm={(v) => {
					onChange?.(v?.[0]);
				}}
			/>
		</>
	);
};
export default memo(CustomSelect);
