/*
 * @Author: huajian
 * @Date: 2024-03-26 15:33:38
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 15:34:32
 * @Description:
 */
import { Toast } from 'antd-mobile';
import copy from 'copy-to-clipboard';
import './index.scss';
import Xingzhuangjiehe2x from '@/common/icons/Xingzhuangjiehe2x';
const GoodsCode = ({ product }) => {
	const handleCopy = () => {
		if (copy(product!)) {
			Toast.show(window._$m.t('复制成功'));
		}
	};
	return (
		<div className="product-code">
			<span className="active-text">
				{window._$m.t('商品番号:')}
				{product}
			</span>
			<Xingzhuangjiehe2x onClick={handleCopy} color="#7E8694" />
			{/* <img
                                                                                                      onClick={handleCopy}
                                                                                                      src="https://static-jp.theckb.com/static-asset/easy-h5/copy.svg"
                                                                                                      alt="copy"
                                                                                                      /> */}
		</div>
	);
};
export default GoodsCode;
