/*
 * @Author: yusha
 * @Date: 2023-10-22 15:12:01
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-06 13:04:04
 * @Description: 自定义input
 */
import { Input, InputProps } from 'antd-mobile';
import { ReactNode, memo } from 'react';
import './index.scss';
interface CustomInputRops extends InputProps {
	/** 带标签的 input，设置前置标签 */
	prefix?: ReactNode;
	customClassName?: string;
}
const CustomInput = (props: CustomInputRops) => {
	const { disabled, prefix, customClassName, value = '', ...reset } = props;
	return (
		<div
			className={`custom-input ${disabled ? 'custom-input-disabled' : ''} ${customClassName}`}
		>
			{prefix && <>{prefix}</>}
			<Input
				placeholder={window._$m.t('请输入')}
				style={{
					'--placeholder-color': 'rgba(0,0,0,0.25)'
				}}
				{...reset}
				disabled={disabled}
				value={value ?? ''}
			/>
		</div>
	);
};
export default memo(CustomInput);
