/*
 * @Author: yusha
 * @Date: 2023-11-20 17:36:03
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-23 14:13:20
 * @Description: 代采特惠模块（商详和商品列表用到）
 */

import { Grid } from 'antd-mobile';
import { useNavigate, useNavigation } from 'react-router-dom';
import MoreSmallsize from '@/common/icons/MoreSmallsize';
import ShoppItem from '@/component/ShoppItem';
import { useEvent } from '@/utils/hooks/useEvent';
import { ProductKeywordRespDTO, SearchImageRespDTO } from '@/service/easyGoods';
import { navigateAppOrH5 } from '@/utils';
import styles from './index.module.scss';
interface PurcharseModulesProps {
	data: SearchImageRespDTO[] | ProductKeywordRespDTO[];
	rootFontSize: string;
	/** 收藏方法 */
	changeCollect: (val) => void;
	title: string;
	/** 查看更多 */
	searchMore: () => void;
}
const PurcharseModules = (props: PurcharseModulesProps) => {
	const navigate = useNavigate();
	const { data = [], rootFontSize, changeCollect, title, searchMore } = props;
	const handleShoppItem = useEvent(({ productCode }) => {
		navigateAppOrH5({
			h5Url: '/goods/detail',
			params: {
				productCode
			},
			appPageType: 'GoodDetail',
			navigate
		});
	});
	if (!data.length) return null;
	return (
		<div className={styles['purchase-goods']}>
			<div className={styles['purchase-title']}>
				<div className={styles.tag}>
					{/* <span className={styles.dot} /> */}
					<div className={styles.txt}>{title}</div>
				</div>
				<div className={styles['right-more']} onClick={searchMore}>
					<span>{window._$m.t('前往代采商城')}</span>
					<MoreSmallsize
						style={{
							color: '#B0B7C2',
							marginLeft: '0.04rem'
						}}
					/>
				</div>
			</div>
			<div>
				<div className={styles['shopp-view']}>
					<Grid columns={2} gap={Number(rootFontSize) * 0.08}>
						{data.map((shoppItem) => (
							<Grid.Item key={shoppItem.productCode}>
								<ShoppItem
									favoriteFlag={shoppItem.favoriteFlag}
									onCollect={changeCollect}
									onClick={handleShoppItem}
									sellPrice={shoppItem.sellPrice}
									productSellPriceJa={
										shoppItem.productSellPriceJa
									}
									productPurchaseType={
										shoppItem.productPurchaseType
									}
									title={shoppItem.productTitle}
									productCode={shoppItem.productCode}
									productMainImg={shoppItem.productMainImg}
									status={shoppItem.status}
									showGoBackPrice={shoppItem.showGoBackPrice}
								/>
							</Grid.Item>
						))}
					</Grid>
				</div>
			</div>
		</div>
	);
};
export default PurcharseModules;
