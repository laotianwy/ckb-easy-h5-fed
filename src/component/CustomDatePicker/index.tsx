/*
 * @Author: shiguang
 * @Date: 2023-10-17 14:33:40
 * @LastEditors: huajian
 * @LastEditTime: 2023-12-19 18:02:57
 * @Description: DatePicker
 */
import { useCallback, useEffect, useRef, useState } from 'react';
import dayjs from 'dayjs';
import { Button, DatePicker, DatePickerProps, Input, Toast } from 'antd-mobile';
import { useBoolean, useMount, useToggle } from 'ahooks';
import { PickerDate } from 'antd-mobile/es/components/date-picker/util';
import classNames from 'classnames';
import ExpandDefault from '@/common/icons/ExpandDefault';
import { formatDateJP2CN } from '@/utils';
import styles from './index.module.scss';
export interface CusDataPickerProps extends DatePickerProps {
	// YYYY-MM-DD HH:mm:ss
	onChange: (val: { startTime?: string; endTime?: string }) => void;
}
export const CustomDatePicker: React.FC<CusDataPickerProps> = (props) => {
	const [visible, { toggle }] = useToggle(false);
	const [startTime, setStartTime] = useState<string>();
	const [endTime, setEndTime] = useState<string>();
	const [isStart, { setFalse, setTrue }] = useBoolean(true);
	const ref = useRef(null);
	const RenderTitle = () => {
		useEffect(() => {
			const el = document.createElement('img');
			el.src =
				'https://static-s.theckb.com/BusinessMarket/Easy/H5/Close.png';
			el.className = styles.closeIcon;
			el.onclick = toggle;
			try {
				document.querySelector('.adm-popup-body')!.appendChild(el);
			} catch (err) {}
			return () => {
				try {
					document.querySelector('.adm-popup-body')!.removeChild(el);
				} catch (err) {}
			};
		}, []);
		return (
			<div className={styles.headerWrap}>
				<div className={styles.title}>{window._$m.t('日期选择')}</div>
				<div className={styles.btnWrap}>
					<Button
						shape="rounded"
						className={styles.clearBtn}
						onClick={() => {
							setStartTime(undefined);
							setEndTime(undefined);
							props.onChange({
								startTime: undefined,
								endTime: undefined
							});
							toggle();
						}}
					>
						{window._$m.t('清空')}
					</Button>
					<Button
						shape="rounded"
						color="primary"
						className={styles.okBtn}
						onClick={() => {
							console.log(startTime, endTime);
							if (dayjs(endTime) <= dayjs(startTime)) {
								Toast.show({
									content: window._$m.t(
										'開始時刻が終了時刻を超えることはできません。'
									)
								});
								return;
							}
							props.onChange({
								startTime: startTime
									? formatDateJP2CN(startTime)
									: undefined,
								endTime: endTime
									? formatDateJP2CN(
											dayjs(endTime).format(
												'YYYY-MM-DD 23:59:59'
											)
										)
									: undefined
							});
							toggle();
						}}
					>
						{window._$m.t('确定')}
					</Button>
				</div>
				<div className={styles.inputWrap}>
					<Input
						className={classNames(
							styles.input,
							isStart ? styles.focus : ''
						)}
						value={startTime}
						onFocus={setTrue}
						placeholder={window._$m.t('開始時刻')}
					/>
					-
					<Input
						className={classNames(
							styles.input,
							!isStart ? styles.focus : ''
						)}
						value={endTime}
						onFocus={setFalse}
						placeholder={window._$m.t('終了時刻')}
					/>
				</div>
			</div>
		);
	};
	const onConfirm = (val: PickerDate) => {
		const time = dayjs(val).format('YYYY-MM-DD');
		if (isStart) {
			setStartTime(time);
			return;
		}
		setEndTime(time);
	};
	const labelRenderer = useCallback((type: string, data: number) => {
		switch (type) {
			case 'year':
				return data + window._$m.t('年');
			case 'month':
				return data + window._$m.t('月');
			case 'day':
				return data + window._$m.t('日');
			default:
				return data;
		}
	}, []);
	return (
		<DatePicker
			title={<RenderTitle />}
			style={{
				height: 300,
				paddingBottom: '.5rem'
			}}
			visible={visible}
			max={new Date('2050-12-31 23:59:59')}
			min={new Date('2011-01-01 00:00:00')}
			onSelect={onConfirm}
			onClose={toggle}
			renderLabel={labelRenderer}
			className={styles.datePicker}
		>
			{() => {
				return (
					<>
						<div
							onClick={toggle}
							style={{
								fontWeight: 500,
								fontFamily: 'PingFang SC',
								fontSize: '.13rem',
								lineHeight: '.2rem',
								color: '#1C2026'
							}}
						>
							{!!startTime &&
								dayjs(startTime).format('YYYY-MM-DD')}
							{!!startTime && !!endTime && '，'}
							{!!endTime && dayjs(endTime).format('YYYY-MM-DD')}
							{!startTime && !endTime && window._$m.t('日期选择')}
						</div>
						<ExpandDefault className={styles.arrow} />
					</>
				);
			}}
		</DatePicker>
	);
};
CustomDatePicker.defaultProps = {};
