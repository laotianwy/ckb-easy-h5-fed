/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-01-16 10:57:22
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 10:47:14
 * @FilePath: /ckb-easy-h5-fed/src/component/AppComponent/BackTop/index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Image, TouchableOpacity } from 'react-native';
import React from 'react';
import * as SDK from '@snifftest/sdk/lib/rn';

/** 返回顶部ICON */
const ICON_BACK_TOP =
	'https://static-s.theckb.com/BusinessMarket/App/dirct/icon/icon_backTop.png';
interface BackTopProps {
	/** 返回顶部目标 */
	target: React.MutableRefObject<any>;
	/** 是否显示 */
	visible?: boolean;
	setIsShowBackTop: React.Dispatch<React.SetStateAction<boolean>>;
}
/** 返回顶部 */
const BackTop = (props: BackTopProps) => {
	const { target } = props;

	/** 返回顶部 */
	const onPresBackTop = () => {
		target!.current!.scrollToOffset({
			offset: 0,
			animated: true
		});
		props.setIsShowBackTop(false);
	};
	return (
		<>
			{props.visible && (
				<TouchableOpacity
					style={{
						position: 'absolute',
						right: SDK.fitSize(12),
						// bottom: SDK.fitSize(100),
						width: SDK.fitSize(48),
						height: SDK.fitSize(48),
						borderRadius: SDK.fitSize(24),
						backgroundColor: '#fff',
						justifyContent: 'center',
						alignItems: 'center',
						shadowColor: 'rgba(0,0,0,.1)',
						shadowOffset: {
							width: 0,
							height: SDK.fitSize(0.7)
						}
					}}
					activeOpacity={0.7}
					onPress={onPresBackTop}
				>
					<Image
						source={{
							uri: ICON_BACK_TOP,
							width: SDK.fitSize(24),
							height: SDK.fitSize(24)
						}}
					/>
				</TouchableOpacity>
			)}
		</>
	);
};
export default BackTop;
