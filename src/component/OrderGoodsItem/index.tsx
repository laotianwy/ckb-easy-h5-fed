/*
 * @Author: shiguang
 * @Date: 2023-10-18 11:52:18
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-22 16:41:06
 * @Description: OrderGoodsItem
 */
import { useToggle } from 'ahooks';
import { Button, Ellipsis, Toast } from 'antd-mobile';
import React, { Fragment, Key } from 'react';
import classNames from 'classnames';
import { useNavigate } from 'react-router-dom';
import copy from 'copy-to-clipboard';
import CustomModal from '@/component/CustomModal';
import { EnumOrderStatus } from '@/common/Enums';
import { CouponInfoDTO, OrderDTO, OrderItemDTO } from '@/service/easyOrder';
import ExpandDefault from '@/common/icons/ExpandDefault';
import CollapseDefault from '@/common/icons/CollapseDefault';
import { formatMoney, formatDateCN2JP, navigateAppOrH5 } from '@/utils';
import { CustomImg } from '@/component/CustomImg';
import { CustomRMImg } from '@/component/CustomRMImg';
import GoodsCode from '@/component/GoodsCode';
import Xingzhuangjiehe2x from '@/common/icons/Xingzhuangjiehe2x';
import styles from './index.module.scss';
const getViewMoreDom = (isExpand: boolean, length: number) => {
	return (
		<div className={styles.expendAction}>
			<div
				style={{
					fontSize: '.12rem',
					color: '#1C2026',
					marginRight: '.04rem'
				}}
			>
				{isExpand
					? window._$m.t('展开剩余{{num}}件', {
							data: {
								num: length
							}
						})
					: window._$m.t('收起')}
			</div>
			{isExpand ? (
				<ExpandDefault color="#7E8694" />
			) : (
				<CollapseDefault color="#7E8694" />
			)}
		</div>
	);
};
interface GetPreviewGoodsParams {
	onClickExpand?: () => void;
	isExpand: boolean;
	orderItemList: OrderItemDTO[];
	payableAmount: number;
	source: string;
	orderNo: string;
	couponInfo?: CouponInfoDTO;
}
const onClickTagTip = (item) => {
	if (Number(item.orderItemStatus) !== EnumOrderStatus.cancelled.code) {
		return;
	}
	CustomModal.confirm({
		content: (
			<div
				style={{
					textAlign: 'center'
				}}
			>
				<div>
					{window._$m.t('取消原因：')}
					{window._$m.t(item.cancelReason as string)}
				</div>
				<div>
					{window._$m.t('取消时间：')}
					{formatDateCN2JP(item.cancelTime)}
				</div>
			</div>
		),

		showCloseButton: false
	});
};
const PreviewGoodsItem = ({ product, couponInfo, showLine, source }) => {
	const {
		productImg,
		productNo,
		productName,
		productPropertiesName,
		productPrice,
		orderItemStatus,
		productCode,
		payableAmount,
		discountAmount,
		productOriginalTotalAmount,
		productTotalAmount
	} = product;
	const navigate = useNavigate();
	return (
		<div
			style={{
				padding: '0.12rem 0 0.06rem 0',
				borderBottom: `${showLine ? '1px solid #F0F2F5' : ''}`
			}}
		>
			<div className={styles.previewWrap}>
				<div className={styles.imgWrap}>
					<CustomRMImg
						src={productImg}
						imgSize={70}
						alt=""
						className={styles.itemImg}
					/>
				</div>
				<div className={styles.detail}>
					<div
						className={styles.title}
						onClick={() => {
							navigateAppOrH5({
								h5Url: '/goods/detail',
								params: {
									productCode
								},
								appPageType: 'GoodDetail',
								navigate
							});
						}}
					>
						{productName}
					</div>
					{productNo && (
						<div
							style={{
								width: 'fit-content',
								display: 'inline-block'
							}}
						>
							<GoodsCode product={productNo} />
							<div />
						</div>
					)}
				</div>
			</div>
			<div className={styles.sku}>
				<div className={styles.skuImg}>
					<CustomRMImg
						src={productImg}
						imgSize={40}
						alt=""
						className={styles.itemImg}
					/>
				</div>
				<div className={styles.skuTitle}>{productPropertiesName}</div>
			</div>
			<div className={styles.amountBox}>
				<span>
					{formatMoney(productPrice)}
					{window._$m.t('円')} x
					{source === 'orderDelivery'
						? product.deliveryNum
						: product.productQuantity}
				</span>
				<span>
					{formatMoney(productTotalAmount)}
					{window._$m.t('円')}
				</span>
			</div>
			{Boolean(discountAmount) && couponInfo && (
				<div className={styles.amountBox}>
					<span className={styles.coupon}>
						{couponInfo.couponName}
					</span>
					<span
						style={{
							color: '#FF5000'
						}}
					>
						-{formatMoney(discountAmount)}
						{window._$m.t('円')}
					</span>
				</div>
			)}

			{Boolean(discountAmount) && couponInfo && (
				<div className={styles.payableAmount}>
					{window._$m.t('优惠后金额')}
					{payableAmount}
					{window._$m.t('円')}
				</div>
			)}

			{source !== 'orderDelivery' && (
				<div
					style={{
						display: 'flex',
						justifyContent: 'flex-end',
						fontSize: '.12rem',
						marginTop: '.08rem',
						borderRadius: '.12rem',
						color:
							Number(orderItemStatus) ===
							EnumOrderStatus.cancelled.code
								? 'rgba(255, 80, 16, 0.80)'
								: '#7E8694'
					}}
				>
					<div
						style={{
							marginLeft: '.08rem',
							borderRadius: '.12rem',
							border: `1px solid ${
								Number(orderItemStatus) ===
								EnumOrderStatus.cancelled.code
									? 'rgba(255, 80, 16, 0.30)'
									: '#F0F2F5'
							}`,
							color:
								Number(orderItemStatus) ===
								EnumOrderStatus.cancelled.code
									? 'rgba(255, 80, 16, 0.80)'
									: '#7E8694',
							padding: '0.02rem 0.1rem 0.02rem 0.1rem'
						}}
						onClick={() => onClickTagTip(product)}
					>
						{EnumOrderStatus[orderItemStatus!]?.cn}
						{Number(orderItemStatus) === 2 ? (
							<span>{product.deliverQuantity}</span>
						) : (
							<span>{product.productQuantity}</span>
						)}
					</div>
				</div>
			)}
		</div>
	);
};
const getPreviewGoods = (params: GetPreviewGoodsParams) => {
	const {
		source,
		orderItemList,
		isExpand,
		onClickExpand,
		payableAmount,
		orderNo,
		couponInfo
	} = params;
	const handleCopy = () => {
		if (copy(orderNo!)) {
			Toast.show(window._$m.t('复制成功'));
		}
	};
	const getShowLine = (idx) => {
		if (orderItemList.length === 1) {
			return false;
		}
		if (idx + 1 === orderItemList.length - 1) {
			return false;
		}
		return true;
	};
	return (
		<div>
			<PreviewGoodsItem
				product={orderItemList[0]}
				couponInfo={couponInfo}
				showLine={getShowLine(0) && !isExpand}
				source={source}
			/>

			{!isExpand && (
				<div>
					{orderItemList
						.slice(1, orderItemList.length)
						.map((it, idx) => {
							return (
								<Fragment key={idx}>
									<PreviewGoodsItem
										key={idx}
										product={it}
										couponInfo={couponInfo}
										showLine={getShowLine(idx)}
										source={source}
									/>
								</Fragment>
							);
						})}
				</div>
			)}

			{orderItemList.length > 1 && (
				<div
					className={styles.seeMore}
					onClick={(e) => {
						e.stopPropagation();
						onClickExpand?.();
					}}
				>
					{getViewMoreDom(isExpand, orderItemList.length - 1)}
				</div>
			)}

			{Boolean(payableAmount) && !source && (
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between'
					}}
				>
					<span
						style={{
							fontSize: '.14rem',
							fontWeight: 'bold'
						}}
					>
						{window._$m.t('订单金额')}
					</span>
					<span
						style={{
							fontSize: '.15rem',
							fontWeight: 'bold'
						}}
					>
						{formatMoney(payableAmount)}
						{window._$m.t('円')}
					</span>
				</div>
			)}

			{!source && (
				<div
					style={{
						display: 'flex',
						marginTop: '.14rem',
						color: '#595959',
						justifyContent: 'space-between'
					}}
				>
					<span
						style={{
							fontSize: '.14rem'
						}}
					>
						{window._$m.t('订单编号')}
					</span>
					<span
						style={{
							fontSize: '.14rem',
							display: 'flex',
							alignItems: 'center'
						}}
					>
						<span
							style={{
								marginRight: '.05rem'
							}}
						>
							{orderNo}
						</span>
						<Xingzhuangjiehe2x
							onClick={handleCopy}
							color="#0586FE"
						/>
					</span>
				</div>
			)}
		</div>
	);
};
interface RenderGoodsProps {
	goods: OrderItemDTO;
}
const RenderGoods: React.FC<RenderGoodsProps> = ({ goods }) => {
	return (
		<div className={styles.goods}>
			<CustomImg
				className={styles.productImg}
				src={goods.productImg}
				alt=""
			/>

			<div className={styles.infoWrap}>
				<div className={styles.titleWrap}>
					<Ellipsis
						direction="end"
						content={goods.productName || ''}
					/>

					{/** TODO */}
					<div
						style={{
							fontWeight: 'bold',
							flexShrink: 0
						}}
					>
						{formatMoney(goods.productPrice)}
						{window._$m.t('元')}
					</div>
				</div>
				<div className={styles.sizeWrap}>
					<Ellipsis
						style={{
							fontSize: '.12rem'
						}}
						direction="end"
						rows={2}
						content={goods.productPropertiesName || ''}
					/>

					<div
						style={{
							flexShrink: 0
						}}
					>
						×{goods.productQuantity}
					</div>
				</div>
				<div className={styles.priceWrap}>
					<Ellipsis
						style={{
							fontSize: '.12rem',
							lineHeight: 1,
							flex: '1 1 auto',
							wordBreak: 'break-all'
						}}
						direction="end"
						content={goods.productSku || ''}
					/>

					<div className={styles.price}>
						{formatMoney(goods.productTotalAmount)}
						{window._$m.t('元')}
					</div>
				</div>
			</div>
		</div>
	);
};
interface OrderGoodsItemProps {
	source?: string;
	info: OrderDTO;
}
export const OrderGoodsItem = (props: OrderGoodsItemProps) => {
	const [isExpand, { toggle, setLeft, setRight }] = useToggle(true);
	const goodsList = props.info.orderItemList || [];
	// 预览的商品
	const previewGoodsListDom = getPreviewGoods({
		isExpand,
		onClickExpand: toggle,
		orderItemList:
			props.source === 'orderDelivery'
				? (props.info as OrderItemDTO[])
				: props.info.orderItemList!,
		payableAmount: props.info.payableAmount || 0,
		source: props.source || '',
		orderNo: props.info.orderNo || '',
		couponInfo: props.info.couponInfo
	});
	return (
		<div
			style={{
				marginBottom: '.1rem'
			}}
			onClick={(e) => e.stopPropagation()}
		>
			<div>{previewGoodsListDom}</div>
		</div>
	);
};
