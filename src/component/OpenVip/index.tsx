import { memo, useState, Dispatch, SetStateAction } from 'react';
import { Mask } from 'antd-mobile';
import CancelCircle from '@/common/icons/CancelCircle';
import styles from './index.module.scss';
interface OpenVipProps {
	visible: boolean;
	setVisible: Dispatch<SetStateAction<boolean>>;
	onClick?: () => void;
}
const OpenVip = (props: OpenVipProps) => {
	const { visible, setVisible, onClick } = props;
	const nowOpenVip = () => {
		onClick?.();
	};
	const closeModal = () => {
		setVisible(false);
	};
	return (
		<Mask
			visible={visible}
			style={{
				'--z-index': '10000'
			}}
		>
			<div className={styles['wrap']}>
				<div className={styles['vip-modal']}>
					<img
						className={styles['open-vip']}
						alt="open-vip"
						src="https://static-s.theckb.com/BusinessMarket/Easy/H5/open-vip.svg"
					/>

					<div className={styles['tips']}>
						{window._$m.t('开通会员，即可享受')}
					</div>
					<div className={styles['tips-sub']}>
						{window._$m.t('采购更透明，更便捷')}
					</div>
					<div className={styles['tips-sub mt8']}>
						{window._$m.t('会员权益超有料，快来开通吧！')}
					</div>
					<div className={styles['vip-attr']}>
						<div className={styles['name']}>
							{window._$m.t('直行便会员卡')}
						</div>
						<div className={styles['list']}>
							{window._$m.t('· 超低商品价格，更透明！')}
						</div>
						<div className={styles['list']}>
							{window._$m.t('· 大量采购更省钱！')}
						</div>
						<div className={styles['list']}>
							{window._$m.t('· 专属客服服务，更快响应！')}
						</div>
					</div>
					<div className={styles['now-open']} onClick={nowOpenVip}>
						{window._$m.t('立即开通')}
					</div>
				</div>

				<div className={styles['close']} onClick={closeModal}>
					<CancelCircle />
				</div>
			</div>
		</Mask>
	);
};
export default memo(OpenVip);
