/*
 * @Author: yusha
 * @Date: 2023-11-22 10:19:26
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-28 10:03:19
 * @Description: 售罄提示弹窗
 */

import { Mask, ModalProps } from 'antd-mobile';
import { memo } from 'react';
import './index.scss';
interface ModalSoldOutProps extends ModalProps {}
const ModalSoldOut = (props: ModalSoldOutProps) => {
	const { visible, onClose } = props;
	return (
		<>
			{visible && (
				<Mask
					className="modal-sold-out-mask"
					destroyOnClose
					getContainer={() => document.getElementById('root')!}
				>
					<img
						className="sold-out-img"
						alt=""
						src="https://static-s.theckb.com/BusinessMarket/Easy/H5/soldout.png"
					/>

					<span className="sold-out-title">
						{window._$m.t('很遗憾，该商品已售罄')}
					</span>
					<div className="sold-out-top">
						<span>
							{window._$m.t(
								'加入社群，及时获取1日元秒杀活动消息！'
							)}
						</span>
					</div>
					<div className="sold-out-content">
						<div
							className="content-detail"
							dangerouslySetInnerHTML={{
								__html: window._$m.t(
									"快人一步获得秒杀活动预告，还有<span className='special-text-style'>优质商品上新提醒、生动的商品测评分享、日本讲师教学课程，</span>并配有专属人员答疑解惑。"
								)
							}}
						/>

						<div className="text-center position-relative">
							<img
								style={{
									width: '1.64rem',
									height: '1.64rem'
								}}
								alt=""
								src="https://static-s.theckb.com/BusinessMarket/Easy/H5/code.png"
							/>

							<img
								className="qr-code"
								alt=""
								src="https://static-s.theckb.com/BusinessMarket/Easy/PC/QRCode.png"
							/>
						</div>
					</div>
					<img
						className="close-icon"
						alt=""
						src="https://static-s.theckb.com/BusinessMarket/Easy/H5/close4.png"
						onClick={onClose}
					/>
				</Mask>
			)}
		</>
	);
};
export default memo(ModalSoldOut);
