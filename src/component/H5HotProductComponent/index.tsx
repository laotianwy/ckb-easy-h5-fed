import { useEffect, useState } from 'react';
import './index.scss';
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-03-21 21:27:27
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-04-11 13:29:39
 * @FilePath: /ckb-easy-h5-fed/src/component/H5HotProductComponent/index.tsx
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import dayjs from 'dayjs';
import * as SDK from '@snifftest/sdk/lib/rn';
import { useNavigate } from 'react-router-dom';
import { CollectionActivityRespDTO } from '@/outs/service/easyMarket';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import Item from './components/Item';
import { collectItem, dislikeItem } from './common';
interface SITE_BUY_PROPS {
	title: string;
	data: CollectionActivityRespDTO[];
	id: number;
}
const H5SiteBuyComponent = (props: SITE_BUY_PROPS) => {
	const { data, title, id } = props;
	const nav = SDK.useNav();
	const [list, setList] = useState<CollectionActivityRespDTO[]>([]);
	const [activeId, setActiveId] = useState<number>(0);
	const navigate = useNavigate();
	const collect = async (item) => {
		await collectItem(item);
		setList(
			list.map((v) => {
				const flag = v.collectedFlag === 1 ? 0 : 1;
				return {
					...v,
					collectedFlag: item.id === v.id ? flag : v.collectedFlag
				};
			})
		);
	};
	const dislike = async (id: number) => {
		const res = await dislikeItem(String(id));
		if (!res) return;
		const newList = list.map((i) => {
			return {
				...i,
				disLikeTime:
					i.id === id
						? dayjs(new Date().getTime()).format(
								'YYYY-MM-DD HH:mm:ss'
							)
						: i.disLikeTime,
				dislikeFlag: i.id === id ? 1 : i.dislikeFlag
			};
		});
		setList(newList);
	};
	const [idMapList, setIdMapList] = useState<number[]>();
	useEffect(() => {
		setList(props.data);
		console.log('props.data11', props.data);
	}, [props.data]);
	useEffect(() => {
		const noDislikeList: number[] = list
			.filter((item) => item.dislikeFlag === 0)
			.map((i) => i.id);
		const dislikeList: number[] = list
			.filter((item) => item.dislikeFlag === 1)
			.sort(
				(a, b) =>
					new Date(a.disLikeTime).getTime() -
					new Date(b.disLikeTime).getTime()
			)
			.map((i) => i.id);
		setIdMapList([...noDislikeList, ...dislikeList]);
	}, [list]);
	return (
		<div
			style={{
				backgroundColor: '#fff',
				borderRadius: '0.08rem',
				padding: '0.16rem 0.08rem',
				marginTop: '0.12rem'
			}}
		>
			<div
				style={{
					display: 'flex',
					flexDirection: 'row',
					alignItems: 'center',
					marginBottom: '0.16rem'
				}}
			>
				<div
					onClick={() => {
						taTrack({
							event: TaEvent.BTN_CLICK,
							value: {
								position: 'self_define_module_more'
							}
						});
						// TODO：跳转到特辑列表。需要带一个id
						const url = `/goods/hot?id=${id}`;
						navigate(url);
					}}
					style={{
						padding: '0 0.08rem',
						fontSize: '.17rem',
						fontWeight: '600',
						color: 'rgba(0, 0, 0, 0.88)'
					}}
				>
					{title}
				</div>
				<img
					onClick={() => {
						// TODO：跳转到特辑列表。需要带一个id
						const url = `/goods/hot?id=${id}`;
						navigate(url);
					}}
					alt=""
					style={{ width: '0.22rem', height: '0.22rem' }}
					src="https://static-jp.theckb.com/static-asset/easy-app/icon/arrow.png"
				/>
			</div>
			{/* <Transition list={list} render={render} itemKey="id" /> */}
			<div
				style={{
					position: 'relative',
					height:
						2.82 * Math.min(Math.floor(data.length / 2), 2) -
						0.12 +
						'rem',
					overflow: 'hidden',
					paddingBottom: 0
				}}
			>
				{list.map((item, index) => {
					return (
						<Item
							active={activeId === item.id}
							key={index}
							item={item}
							setActiveId={setActiveId}
							collect={collect}
							dislike={dislike}
							positionIndex={idMapList.findIndex(
								(i) => i === item.id
							)}
						/>
					);
				})}
			</div>
		</div>
	);
};

export default H5SiteBuyComponent;
