/*
 * @Author: huajian
 * @Date: 2024-03-25 10:11:58
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-27 17:45:38
 * @Description:
 */
import { Toast } from 'antd-mobile';
import { request } from '@/config/request/interceptors';
import { CollectionActivityRespDTO } from '@/service/market';
// 收藏特辑
export const collectItem = async (item: CollectionActivityRespDTO) => {
	try {
		if (item.collectedFlag === 1) {
			await request.easyMarket.collection.removeCollectionList([
				Number(item.id)
			]);

			Toast.show({
				content: window._$m.t('取消收藏成功')
			});
		} else {
			await request.easyMarket.collection.addActivityAreaById({
				id: Number(item.id)
			});
			Toast.show({
				content: window._$m.t('收藏成功')
			});
		}
		console.log(window._$m.t('收藏成功！'));
	} catch (error) {
		console.log(window._$m.t('收藏失败！'), error);
	}
};

// 不喜欢

export const dislikeItem = async (id: string) => {
	console.log('common dislikeItem', id);
	try {
		const data =
			await request.easyMarket.collection.dislikeActivityAreaById({
				id: Number(id)
			});
		Toast.show({
			content: window._$m.t('操作成功')
		});
		return true;
		console.log(window._$m.t('不喜欢成功'));
	} catch (error) {
		console.log(window._$m.t('不喜欢失败！'), error);
		return false;
	}
};
