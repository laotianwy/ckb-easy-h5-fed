/*
 * @Author: shiguang
 * @Date: 2023-10-25 17:56:21
 * @LastEditors: shiguang
 * @LastEditTime: 2023-10-28 16:24:54
 * @Description: PayPal
 */
import PayPalCardPay from '../../../common/PayPalCardPay';
import PayPalPay from '../../../common/PayPalPay';

const PayPal = () => {
	return (
		<div>
			{/* <PayPalPay /> */}
			<PayPalCardPay orderNo="12341" />
		</div>
	);
};

export default PayPal;
