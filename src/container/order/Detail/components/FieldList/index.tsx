/*
 * @Author: shiguang
 * @Date: 2023-10-20 14:30:11
 * @LastEditors: shiguang
 * @LastEditTime: 2023-10-20 17:57:43
 * @Description: FieldList
 */
import React from 'react';
interface Field {
	label: React.ReactNode;
	labelStyle?: React.CSSProperties;
	value?: React.ReactNode;
	valueStyle?: React.CSSProperties;
}
interface FieldListProps {
	data: Field[];
	style?: React.CSSProperties;
}
const FieldList = (props: FieldListProps) => {
	const { data, style } = props;
	return (
		<div
			style={{
				padding: '.08rem .12rem',
				fontSize: '.12rem',
				// backgroundColor: '#F8F8F8',
				borderRadius: '.08rem',
				...style
			}}
		>
			{data.map((item, index) => {
				return (
					<div
						key={index}
						style={{
							// height: '.28rem',
							marginBottom: '.08rem',
							display: 'flex',
							justifyContent: 'space-between',
							alignItems: 'baseline'
						}}
					>
						<div
							style={{
								color: '#1C2026',
								...item.labelStyle
							}}
						>
							{item.label}
						</div>
						<div
							style={{
								color: '#1C2026',
								fontWeight: 'bold',
								...item.valueStyle
							}}
						>
							{item.value}
						</div>
					</div>
				);
			})}
		</div>
	);
};
export default FieldList;
