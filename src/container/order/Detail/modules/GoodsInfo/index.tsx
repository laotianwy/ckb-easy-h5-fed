/*
 * @Author: shiguang
 * @Date: 2023-10-20 14:05:39
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-23 14:13:07
 * @Description: GoodsInfo
 */
import { Ellipsis } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import { OrderItemDTO } from '@/service/easyOrder';
import { EnumOrderStatus } from '@/common/Enums';
import CustomModal from '@/component/CustomModal';
import { formatDateCN2JP, formatMoney, navigateAppOrH5 } from '@/utils';
import { CustomImg } from '@/component/CustomImg';
import FieldList from '../../components/FieldList';
interface GetGoodsItemParams {
	index: number;
	/** 引用来源 */
	source?: 'goodsInfo' | 'shippingInfo';
	style?: React.CSSProperties;
	info: OrderItemDTO;
}
export const getGoodsItem = (params: GetGoodsItemParams) => {
	const { index, source = 'shippingInfo', style, info } = params;
	if (!info) {
		return null;
	}
	return (
		<div
			key={index}
			style={{
				borderTop: '.01rem solid #E9E9E9',
				overflow: 'hidden',
				...style
			}}
		>
			<div
				style={{
					padding: '.16rem 0',
					boxSizing: 'border-box',
					display: 'flex',
					overflow: 'hidden'
				}}
			>
				<CustomImg
					src={info.productImg}
					alt=""
					className="deail-img"
					svgClassName="deail-img-svg deail-img"
				/>

				<div
					style={{
						height: '.8rem',
						flex: 1,
						display: 'flex',
						flexDirection: 'column',
						// alignItems: 'center',
						justifyContent: 'center',
						color: '#777777'
					}}
				>
					<div
						style={{
							display: 'flex',
							justifyContent: 'space-between',
							fontSize: '.14rem',
							color: '#333',
							alignItems: 'self-start',
							flexWrap: 'nowrap'
						}}
					>
						<Ellipsis
							direction="end"
							rows={source === 'goodsInfo' ? 2 : 1}
							content={info.productName || ''}
							style={{
								flex: 1
							}}
						/>

						<div
							style={{
								fontWeight: 'bold',
								flexShrink: 0,
								flex: '0 0 auto'
							}}
						>
							{formatMoney(info.productPrice)}
							{window._$m.t('元')}
						</div>
					</div>
					<div
						style={{
							flex: 1,
							display: 'flex',
							justifyContent: 'space-between'
						}}
					>
						<Ellipsis
							style={{
								fontSize: '.12rem',
								flex: 1
							}}
							direction="end"
							rows={2}
							content={info.productPropertiesName || ''}
						/>

						<div
							style={{
								flexShrink: 0,
								paddingLeft: '.2rem'
							}}
						>
							×{info['deliveryNum'] || info['productQuantity']}
						</div>
					</div>
					{source !== 'goodsInfo' && (
						<div
							style={{
								display: 'flex',
								justifyContent: 'space-between',
								alignItems: 'flex-end'
							}}
						>
							<Ellipsis
								style={{
									fontSize: '.12rem',
									lineHeight: 1
								}}
								direction="end"
								content={
									window._$m.t('商品SKU：') + info.productSku
								}
							/>

							<div
								style={{
									fontSize: '.16rem',
									color: '#333',
									lineHeight: 1,
									fontWeight: 'bold',
									flexShrink: 0,
									flex: '0 0 auto'
								}}
							>
								{formatMoney(info.productTotalAmount)}
								{window._$m.t('元')}
							</div>
						</div>
					)}
				</div>
			</div>
			{source === 'goodsInfo' && (
				<>
					<div
						style={{
							display: 'flex',
							fontSize: '.12rem',
							color: '#777',
							justifyContent: 'space-between'
						}}
					>
						<div>{window._$m.t('商品SKU：') + info.productSku}</div>
						<div
							style={{
								fontSize: '.16rem',
								color: '#333',
								fontWeight: 'bold',
								flex: '0 0 auto'
							}}
						>
							{formatMoney(info.actualAmount) +
								window._$m.t('元')}
						</div>
					</div>
					<div
						style={{
							display: 'flex',
							fontSize: '.12rem',
							marginBottom: '.1rem'
						}}
					>
						<div
							style={{
								marginRight: '.08rem',
								background:
									// 3为已取消
									Number(info.orderItemStatus) ===
									EnumOrderStatus.cancelled.code
										? '#FFF5F2'
										: '#EFEFEF',
								borderRadius: '.04rem',
								color:
									Number(info.orderItemStatus) ===
									EnumOrderStatus.cancelled.code
										? '#FF5010'
										: '#666',
								padding: '.06rem .08rem'
							}}
							onClick={() => {
								if (
									Number(info.orderItemStatus) !==
									EnumOrderStatus.cancelled.code
								) {
									return;
								}
								CustomModal.confirm({
									content: (
										<div
											style={{
												textAlign: 'center'
											}}
										>
											<div>
												{window._$m.t('取消原因：')}
												{window._$m.t(
													info.cancelReason as string
												)}
											</div>
											<div>
												{window._$m.t('取消时间：')}
												{formatDateCN2JP(
													info.cancelTime
												)}
											</div>
										</div>
									),

									showCloseButton: false
								});
							}}
						>
							{EnumOrderStatus[info.orderItemStatus!]?.cn}
							{`(${info.productQuantity})`}
						</div>
					</div>
				</>
			)}
		</div>
	);
};
interface GoodsInfoProps {
	items: OrderItemDTO[];
}
const GoodsInfo = (props: GoodsInfoProps) => {
	const navigate = useNavigate();
	/** 点击标签，弹出tips */
	const onClickTagTip = (item) => {
		if (Number(item.orderItemStatus) !== EnumOrderStatus.cancelled.code) {
			return;
		}
		CustomModal.confirm({
			content: (
				<div
					style={{
						textAlign: 'center'
					}}
				>
					<div>
						{window._$m.t('取消原因：')}
						{window._$m.t(item.cancelReason as string)}
					</div>
					<div>
						{window._$m.t('取消时间：')}
						{formatDateCN2JP(item.cancelTime)}
					</div>
				</div>
			),

			showCloseButton: false
		});
	};
	return (
		<div className="order-detai-goods-info">
			<div
				style={{
					color: '#333',
					fontWeight: 'bold',
					height: '.4rem',
					lineHeight: '.4rem'
				}}
			>
				{window._$m.t('商品信息')}
			</div>
			{props.items.map((item) => {
				return (
					<div
						key={item.id}
						style={{
							borderTop: '.01rem solid #E9E9E9',
							overflow: 'hidden'
						}}
					>
						<div
							style={{
								padding: '.16rem 0',
								boxSizing: 'border-box',
								display: 'flex',
								overflow: 'hidden'
							}}
							onClick={() => {
								navigateAppOrH5({
									h5Url: '/goods/detail',
									params: {
										productCode: item.productCode
									},
									appPageType: 'GoodDetail',
									navigate
								});
							}}
						>
							<CustomImg
								src={item.productImg}
								alt=""
								className="deail-img"
								svgClassName="deail-img-svg deail-img"
							/>

							<div
								style={{
									height: '.8rem',
									flex: 1,
									display: 'flex',
									flexDirection: 'column',
									// alignItems: 'center',
									justifyContent: 'center',
									color: '#777777'
								}}
							>
								<div
									style={{
										display: 'flex',
										justifyContent: 'space-between',
										fontSize: '.14rem',
										color: '#333',
										alignItems: 'self-start',
										flexWrap: 'nowrap'
									}}
								>
									<Ellipsis
										direction="end"
										rows={2}
										content={item.productName || ''}
										style={{
											flex: 1
										}}
									/>

									<div
										style={{
											fontWeight: 'bold',
											flexShrink: 0,
											flex: '0 0 auto'
										}}
									>
										{formatMoney(item.productPrice)}
										{window._$m.t('元')}
									</div>
								</div>
								<div
									style={{
										flex: 1,
										display: 'flex',
										justifyContent: 'space-between'
									}}
								>
									<Ellipsis
										style={{
											fontSize: '.12rem',
											flex: 1
										}}
										direction="end"
										rows={2}
										content={
											item.productPropertiesName || ''
										}
									/>

									<div
										style={{
											flexShrink: 0,
											paddingLeft: '.2rem'
										}}
									>
										×
										{item['deliveryNum'] ||
											item['productQuantity']}
									</div>
								</div>
							</div>
						</div>

						<>
							<div
								style={{
									display: 'flex',
									fontSize: '.12rem',
									color: '#777',
									justifyContent: 'space-between'
								}}
							>
								<div>
									{window._$m.t('商品SKU：') +
										item.productSku}
								</div>
								<div
									style={{
										fontSize: '.16rem',
										color: '#333',
										fontWeight: 'bold',
										flex: '0 0 auto'
									}}
								>
									{formatMoney(item.actualAmount) +
										window._$m.t('元')}
								</div>
							</div>
							<div
								style={{
									display: 'flex',
									fontSize: '.12rem',
									marginBottom: '.1rem'
								}}
							>
								<div
									style={{
										marginRight: '.08rem',
										background:
											// 3为已取消
											Number(item.orderItemStatus) ===
											EnumOrderStatus.cancelled.code
												? '#FFF5F2'
												: '#EFEFEF',
										borderRadius: '.04rem',
										color:
											Number(item.orderItemStatus) ===
											EnumOrderStatus.cancelled.code
												? '#FF5010'
												: '#666',
										padding: '.06rem .08rem'
									}}
									onClick={() => onClickTagTip(item)}
								>
									{EnumOrderStatus[item.orderItemStatus!]?.cn}
									{`(${item.productQuantity})`}
								</div>
							</div>
						</>
					</div>
				);
			})}
		</div>
	);
};
export default GoodsInfo;
