/*
 * @Author: shiguang
 * @Date: 2023-10-20 14:05:39
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-08 14:01:21
 * @Description: BaseInfo
 */

import copy from 'copy-to-clipboard';
import { Toast } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import { isNumber } from 'lodash';
import { OrderDetailDTO } from '@/service/easyOrder';
import { EnumOrderStatus, EnumRefundChannel } from '@/common/Enums';
import Address from '@/common/icons/Address';
import Avatar from '@/common/icons/Avatar';
import { EnumClearanceType } from '@/container/order/ConfirmOrder/config';
import { formatDateCN2JP, formatMoney } from '@/utils';
import Shoujianrenxinxi from '@/common/icons/Shoujianrenxinxi';
import Xiadanrenxinxi from '@/common/icons/Xiadanrenxinxi';
import FieldList from '../../components/FieldList';
interface Props {
	info: OrderDetailDTO;
}
const BaseInfo: React.FC<Props> = ({ info }) => {
	const navigate = useNavigate();
	console.log(info.refundStatistics?.applyingQuantity);

	// 退款显示
	const isShowRefund = (info) => {
		return (
			(isNumber(info.refundStatistics?.hasRefundAmount) &&
				info.refundStatistics?.hasRefundAmount > 0) ||
			(isNumber(info.refundStatistics?.refundingAmount) &&
				info.refundStatistics?.refundingAmount > 0)
		);
	};

	/** 拼接收件人姓名 */
	function formatFullName(name: string) {
		if (name.includes('+')) {
			return name.split('+').join('');
		}
		return name;
	}
	const fieldListData = [
		{
			label: window._$m.t('订单编号'),
			value: (
				<div
					style={{
						display: 'flex',
						alignItems: 'center'
					}}
				>
					<div
						style={{
							color: '#1C2026'
						}}
					>
						{info.orderNo}
					</div>
					<div
						style={{
							width: '.01rem',
							height: '.08rem',
							borderRight: '.01rem solid #DCDCDC',
							margin: '0 .08rem'
						}}
					/>

					<div
						style={{
							color: '#FF5010'
						}}
						onClick={() => {
							if (copy(info.orderNo!)) {
								Toast.show(window._$m.t('复制成功'));
							}
						}}
					>
						{window._$m.t('复制')}
					</div>
				</div>
			)
		},
		{
			label: window._$m.t('下单时间'),
			value: formatDateCN2JP(info.createdTime)
		},
		{
			label: window._$m.t('商品代金'),
			value: formatMoney(info.productTotalAmount) + window._$m.t('元')
		},
		{
			label: window._$m.t('国际运费'),
			value:
				formatMoney(info.internationalShippingAmount) +
				window._$m.t('元')
		},
		{
			label: window._$m.t('优惠金额'),
			value:
				window._$m.t('减') +
				formatMoney(info.discountAmount) +
				window._$m.t('元'),
			valueStyle: {
				fontWeight: 'bold'
			}
		},
		{
			label: window._$m.t('合计金额'),
			value: formatMoney(info.payableAmount) + window._$m.t('元'),
			valueStyle: {
				fontWeight: 'bold'
			}
		},
		{
			label: window._$m.t('支付方式'),
			value: info.payTypeCode && EnumRefundChannel[info.payTypeCode!]?.cn
		}
	];

	if (isShowRefund(info)) {
		fieldListData.push({
			label: window._$m.t('退款金额'),
			value: (
				<div
					style={{
						display: 'flex',
						alignItems: 'flex-end',
						flexDirection: 'column'
					}}
				>
					{info.refundStatistics?.refundingAmount &&
					info.refundStatistics?.refundingAmount > 0 ? (
						<div
							onClick={() => {
								navigate('/user/refund', {
									state: {
										orderNo: info.orderNo,
										status: 1
									}
								});
							}}
							style={{
								display: 'flex',
								alignItems: 'flex-end',
								padding: '.02rem .04rem',
								borderRadius: '.04rem',
								background: '#E6F7FF',
								justifyContent: 'center',
								marginBottom: '.06rem',
								width: 'fit-content',
								gap: '.04rem',
								color: '#0069D9'
							}}
						>
							<span>{window._$m.t('退款中')}</span>
							{info.refundStatistics.refundingAmount}
							{window._$m.t('円')}
							<img
								src="https://static-jp.theckb.com/static-asset/easy-h5/details.svg"
								alt="details"
							/>
						</div>
					) : null}

					{info.refundStatistics?.hasRefundAmount &&
					info.refundStatistics?.hasRefundAmount > 0 ? (
						<div
							onClick={() => {
								navigate('/user/refund', {
									state: {
										orderNo: info.orderNo,
										status: 2
									}
								});
							}}
							style={{
								display: 'flex',
								alignItems: 'center',
								padding: '.02rem .04rem',
								borderRadius: '.04rem',
								background: '#F6FFED',
								justifyContent: 'center',
								gap: '.04rem',
								color: '#389E0D'
							}}
						>
							<span>{window._$m.t('退款完成')}</span>
							{info.refundStatistics.hasRefundAmount}
							{window._$m.t('円')}
							<img
								src="https://static-jp.theckb.com/static-asset/easy-h5/refunded.svg"
								alt="details"
							/>
						</div>
					) : null}
				</div>
			)
		});
	}
	return (
		<div className="order-detai-base-info">
			<div className="order-status">
				{EnumOrderStatus[info.orderStatus!]?.cn}
			</div>
			<div className="recipient-info">
				<div className="info-content">
					<div className="title">
						<Shoujianrenxinxi
							className="icon"
							width={'0.16rem'}
							height={'0.16rem'}
						/>

						{window._$m.t('收件人信息')}
					</div>
					<div className="address-content">
						<div className="content-name">
							<span className="receiveName">
								{formatFullName(
									info.receivingAddress?.receiveName || ''
								)}
							</span>
							<span>+81 {info.receivingAddress?.receiveTel}</span>
						</div>
						<div className="content-address">
							<span className="x-space">
								{info.receivingAddress?.countryName}
							</span>
							<span className="x-space">
								{info.receivingAddress?.provinceName}
							</span>
							<span className="x-space">
								{info.receivingAddress?.cityName}
							</span>
							<span className="x-space">
								{info.receivingAddress?.address}
							</span>
							<span className="x-space">
								{info.receivingAddress?.postalCode}
							</span>
						</div>
					</div>
				</div>
			</div>
			<div>
				<FieldList data={fieldListData} />
			</div>
		</div>
	);
};
export default BaseInfo;
