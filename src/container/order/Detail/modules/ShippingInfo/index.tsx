/*
 * @Author: shiguang
 * @Date: 2023-10-20 14:05:39
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-14 14:25:41
 * @Description: ShippingInfo
 */
import { useToggle } from 'ahooks';
import { OrderItemDTO, OrderLogisticsDTO } from '@/service/easyOrder';
import { EnumDeliveryStatus } from '@/common/Enums';
import { formatDateCN2JP } from '@/utils';
import { OrderGoodsItem } from '@/component/OrderGoodsItem';
import FieldList from '../../components/FieldList';
interface Props {
	logistics: OrderLogisticsDTO[];
}
const ShippingInfo: React.FC<Props> = ({ logistics }) => {
	const [visible, { toggle }] = useToggle(false);
	if (!logistics.length) {
		return null;
	}
	return (
		<div className="order-detai-shipping-info">
			<div
				style={{
					height: '.4rem',
					padding: '.04rem .12rem',
					borderBottom: '.01rem solid #F5F5F5',
					display: 'flex',
					alignItems: 'center',
					color: '#333',
					fontWeight: 'bold',
					fontSize: '.14rem'
				}}
			>
				<span
					style={{
						paddingLeft: '0.08rem',
						borderLeft: '0.03rem solid #FF5010'
					}}
				>
					{window._$m.t('发货详细信息')}
				</span>
			</div>
			{logistics.map((item) => {
				return (
					<div
						key={item.id}
						style={{
							borderBottom: '.01rem solid #E9E9E9'
						}}
					>
						<FieldList
							style={{
								margin: '0 .12rem',
								backgroundColor: '#FAFBFC'
							}}
							data={[
								{
									label: window._$m.t('发货单号'),
									value: item.deliveryTaskCode
								},
								{
									label: window._$m.t('生成时间'),
									value: formatDateCN2JP(
										item.createDeliveryTaskTime
									)
								},
								{
									label: window._$m.t('国际运单号'),
									value: item.logisticsCode
								},
								{
									label: window._$m.t('运输方式'),
									value: item.wmsDistribution
								},
								{
									label: window._$m.t('配送状况'),
									value: EnumDeliveryStatus[
										item.deliveryStatus!
									]?.cn
								},
								{
									label: window._$m.t('配送时间'),
									value: formatDateCN2JP(item.deliveryTime)
								}
							]}
						/>

						<div
							style={{
								padding: '0 .12rem'
							}}
						>
							<OrderGoodsItem
								info={item.productList as OrderItemDTO}
								source="orderDelivery"
							/>
						</div>
					</div>
				);
			})}
		</div>
	);
};
export default ShippingInfo;
