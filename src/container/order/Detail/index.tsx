/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:46
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-20 16:09:04
 * @Description: 订单详情
 */
import { Button } from 'antd-mobile';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useAsyncEffect } from 'ahooks';
import { useState } from 'react';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import { OrderDetailDTO } from '@/service/easyOrder';
import { EnumOrderStatus } from '@/common/Enums';
import ScrollPage from '@/common/ScrollPage';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import { OrderGoodsItem } from '@/component/OrderGoodsItem';
import BaseInfo from './modules/BaseInfo';
import ShippingInfo from './modules/ShippingInfo';
import './index.scss';
const Detail = () => {
	const [orderInfo, setOrderInfo] = useState<OrderDetailDTO>();
	const navigate = useNavigate();
	const [params] = useSearchParams();
	const orderNo = params.get('orderNo')!;
	useAsyncEffect(async () => {
		const res = await request.easyOrder.orderSearch.queryDetailByOrderNo({
			orderNo
		});
		setOrderInfo(res.data);
	}, [orderNo]);
	if (!orderInfo) {
		return null;
	}
	return (
		<div
			className="layout-style page-order-detail"
			style={{
				paddingBottom: '.6rem'
			}}
		>
			<ScrollPage
				title={<CustomNavbar title={window._$m.t('订单详情')} />}
			>
				<div
					className="layout-content page-order-detail-content"
					style={{
						marginBottom:
							orderInfo.orderStatus ===
							EnumOrderStatus.waitPay.code
								? undefined
								: 0
					}}
				>
					<BaseInfo info={orderInfo} />
					<div className="order-goods-item-box">
						<div className="order-goods-title">
							{window._$m.t('商品信息')}
						</div>
						<OrderGoodsItem
							info={orderInfo}
							source="orderProduct"
						/>
					</div>
					<ShippingInfo logistics={orderInfo.logisticsList!} />
				</div>
				{orderInfo.orderStatus === EnumOrderStatus.waitPay.code && (
					<div className="page-order-detail-footer">
						<Button
							shape="rounded"
							block
							onClick={() => {
								taTrack({
									event: TaEvent.CANCELL_PAY,
									value: {
										order_no: orderInfo.orderNo,
										order_product_detail:
											orderInfo.orderItemList?.map(
												(i) => {
													return {
														product_code:
															i.productCode,
														product_title_ja:
															i.productName
													};
												}
											)
									}
								});
								navigate(-1);
							}}
						>
							{window._$m.t('取消支付')}
						</Button>
						<div
							style={{
								width: '.1rem'
							}}
						>
							{' '}
						</div>
						<Button
							block
							color="primary"
							shape="rounded"
							onClick={() => {
								const orderUp = orderInfo.orderItemList?.map(
									(i) => {
										return {
											product_code: i.productCode,
											product_title_ja: i.productName,
											product_num: i.productQuantity
										};
									}
								);
								localStorage.setItem(
									`order_${orderInfo.orderNo}`,
									JSON.stringify(orderUp)
								);
								taTrack({
									event: TaEvent.ORDER_PAY_CLICK,
									value: {
										source: 'my_order',
										order_no: orderInfo.orderNo,
										order_product_detail:
											orderInfo.orderItemList?.map(
												(i) => {
													return {
														product_code:
															i.productCode,
														product_title_ja:
															i.productName
													};
												}
											)
									}
								});
								navigate(
									`/order/create?orderNo=${orderInfo.orderNo}`
								);
							}}
						>
							{window._$m.t('去支付')}
						</Button>
					</div>
				)}
			</ScrollPage>
		</div>
	);
};
export default Detail;
