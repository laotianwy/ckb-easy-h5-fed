/*
 * @Author: yusha
 * @Date: 2023-10-16 14:14:38
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-24 17:29:43
 * @Description: 购物车
 * 
1、进入后先通过购物车列表接口获取所有购物车商品，通过selectedFlag判断勾选状态，获取所有商品后通过sellStatus过滤不同状态商品
2、点击或取消勾选，先调取修改购物车选中状态接口（取消勾选需要使用取消的id），接口返回后再调用购物车算价接口和购物车列表接口更新购物车和价格
3、增减数量，调用修改购物车，使用修改后的返回替换购物车
4、删除购物车，调用删除接口返回后，再调用购物车列表接口
5、全选或全不选，调用修改购物车选中状态接口，全选时调用算价及购物车列表，全不选则前端自己清空，不需要算价，然后调用购物车列表接口
* 3-增减数量的情况（补充）：
	1、增减数量 修改未勾选的，，调用修改数量的接口直接替换，，，
	2、勾选的去修改数量，调用修改数量的接口，调用算价的接口，调用 购物车列表接口更新购物车和价格
* 4-删除购物车情况：
	1、删除未选的，调用删除接口返回后，再调用购物车列表接口
	2、删除选中的，调用删除接口返回后，再调用购物车列表接口(重新获取选中的购物车id) 算价接口（全部删除，不需要调用算价接口）
 * 
初始化购物车页面：如果有 勾选了有效的购物车id，则需要调用算价接口
 */
import { memo, useState } from 'react';
import { Button, Toast } from 'antd-mobile';
import { useNavigate, useLocation } from 'react-router-dom';
import { useMount, useRequest, useUpdateEffect } from 'ahooks';
import qs from 'query-string';
import { CheckboxValue } from 'antd-mobile/es/components/checkbox';
import { useAtom } from 'jotai';
import CustomModal from '@/component/CustomModal';
import { request } from '@/config/request';
import ScrollPage from '@/common/ScrollPage';
import { OrderProductDTO, UpdateCartSelectedReq } from '@/service/easyOrder';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { Order } from '@/Atoms';
import { Enum_SellStatus } from '@/common/Enums';
import DeleteLine from '@/common/icons/DeleteLine';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import Loading from '@/component/Loading';
import { jsBridge } from '@/utils/jsBridge';
import CartFooter from './modules/CartFooter';
import CartContent from './modules/CartContent';
import './index.scss';
export interface ProductSum {
	// 购物车商品总数量
	productTotalQuantity?: number;
	// 购物车商品总金额
	productTotalAmount?: number;
}
/** 购物车选择状态 */
export enum Enum_SelectedFlag {
	/** 未选 */
	Unselected = 0,
	/** 已选 */
	Selected = 1
}
const ShoppingCart = () => {
	const { fromPage = '' } = qs.parse(useLocation().search);
	/** 跳转方法 */
	const navigate = useNavigate();
	const [cartOriginalData, setCartOriginalData] = useAtom(
		Order.cartOriginalData
	);
	const { cartList = [] } = cartOriginalData;
	/** 选中的商品 */
	const [selectCartIds, setSelectCartIds] = useState<CheckboxValue[]>([]);
	/** 购物车总计数量以及金额 */
	const [productSum, setProductSum] = useState<ProductSum>({});
	/** 勾选的接口 */
	const { runAsync: updateSelected, loading: updateSelectedLoading } =
		useRequest(request.easyOrder.cart.updateSelected, {
			manual: true
		});
	/** 算价的接口 */
	const {
		data: calculatePriceData,
		runAsync: calculatePrice,
		loading: calculatePriceLoading
	} = useRequest(request.easyOrder.cart.calculatePrice, {
		manual: true
	});
	/** 列表的接口 */
	const { runAsync: getList, loading: listLoading } = useRequest(
		request.easyOrder.cart.list,
		{
			manual: true
		}
	);
	/** 删除购物车的接口 */
	const { runAsync: deleteCart, loading: deleteLoading } = useRequest(
		request.easyOrder.cart.delete,
		{
			manual: true
		}
	);
	/** 更新勾选的接口 */
	const { runAsync: updateCart, loading: updateLoading } = useRequest(
		request.easyOrder.cart.update,
		{
			manual: true
		}
	);
	/** 获取购物车列表 */
	const getCartList = async ({
		params,
		cartId,
		isNotUpdateCartList,
		isCancelSelected
	}: {
		params?: UpdateCartSelectedReq;
		cartId?: number[];
		isNotUpdateCartList?: boolean;
		isCancelSelected?: boolean;
	}) => {
		// 更新勾选的
		if (params) {
			await updateSelected(params);
		}
		// 是否需要更新算计接口
		let _newProductList: OrderProductDTO[] = [];
		if (cartId && cartId.length) {
			_newProductList = (await getSelectedCart(cartId)) ?? [];
		}
		if (isNotUpdateCartList) {
			return;
		}
		const data = await getList();
		/** 有效的购物车id */
		const effectCartIds = getEffectCartIds(data.data ?? []);
		setSelectCartIds((effectCartIds as CheckboxValue[]) ?? []);
		let _data = data.data;
		// 取消勾选需要重新匹配数据,直接去算价接口返回的勾选的商品信息，其余的使用接口返回的
		if (isCancelSelected) {
			_data = getMatchCartList(_newProductList, data.data);
		}
		changeSetCartOriginalData(_data ?? []);
		return _data;
	};
	/** 获取有效的选中的cartids */
	const getEffectCartIds = (data: OrderProductDTO[]) => {
		const effectSelectedList = data?.filter(
			(item) =>
				item.sellStatus === Enum_SellStatus.Normal &&
				!!item.selectedFlag
		);
		const effectCartIds = effectSelectedList?.map((item) => item.cartId);
		return effectCartIds;
	};
	useUpdateEffect(() => {
		getDefaultSelectedCart();
	}, [cartList.length]);
	/** 获取默认选中的cartid */
	const getDefaultSelectedCart = () => {
		if (cartList.length) {
			// 有效的选中的cartids
			const effectCartIds = getEffectCartIds(cartList);
			setSelectCartIds((effectCartIds as CheckboxValue[]) ?? []);
			if (effectCartIds?.length) {
				getSelectedCart(effectCartIds as number[]);
				return;
			}
			return;
		}
		setSelectCartIds([]);
	};
	const initPage = async () => {
		// 初始化，或者回退到购物车页面时需要触发
		const res = await getCartList({});
		taTrack({
			event: TaEvent.SHOPPING_CART_PAGEVIEW,
			value: {
				shopping_cart_detail: res?.map((i) => {
					return {
						product_code: i.productCode,
						product_title_ja: i.productName
					};
				})
			}
		});
		await getDefaultSelectedCart();
	};
	useMount(() => {
		initPage();
	});

	/** 删除购物车 */
	const deleteShopCart = (invalidCartids?: number[]) => {
		const _invalidCartids = invalidCartids ?? [];
		// 是否为清除失效商品
		const isDeleteInvalidCartids = _invalidCartids?.length > 0;
		// 删除商品和清除失效商品文案不同
		const text = isDeleteInvalidCartids
			? window._$m.t('确定从购物车清除失效商品吗?')
			: window._$m.t('确定从购物车中删除吗?');
		if (!isDeleteInvalidCartids && !selectCartIds.length) {
			Toast.show({
				content: window._$m.t('请先勾选商品')
			});
			return;
		}
		CustomModal.confirm({
			content: text,
			onConfirm: async () => {
				const params = {
					ids: (isDeleteInvalidCartids
						? invalidCartids
						: selectCartIds) as number[]
				};
				const data = await deleteCart(params);
				if (data.success) {
					Toast.show({
						icon: 'success',
						content: window._$m.t('删除成功')
					});
					const _params = isDeleteInvalidCartids
						? undefined
						: ({
								ids: selectCartIds,
								selectedFlag: Enum_SelectedFlag.Unselected
							} as UpdateCartSelectedReq);

					// 删除成功后重新请求页面
					await getCartList({
						params: _params
					});
					if (!isDeleteInvalidCartids) {
						setProductSum({
							// 购物车商品总数量
							productTotalQuantity: 0,
							// 购物车商品总金额
							productTotalAmount: 0
						});
					}
				}
			}
		});
	};

	/** 更改数量 */
	const handleChangeQuantity = async (item: {
		cartId: number;
		num: number;
	}) => {
		// 请求修改购物车的接口
		const data = await updateCart({
			id: item.cartId,
			quantity: item.num
		});
		// 如果是更改数量接口报错，则提示
		if (!data.success && data.msg) {
			CustomModal.confirm({
				content: data.msg,
				showCloseButton: false
			});
		}
		// 如果勾选之后去增减数量，则需要先调用数量增减接口，再去调用计算价格的接口
		console.log(
			selectCartIds.includes(item.cartId),
			'selectCartIds.includes(item.cartId)'
		);
		if (selectCartIds.includes(item.cartId)) {
			await getSelectedCart();
		} else {
			const newCartList = cartList.map((product) => {
				console.log(item, product, 'product.cartId');
				if (item.cartId === product.cartId) {
					return {
						...product,
						productQuantity: item.num
					};
				}
				return product;
			});
			changeSetCartOriginalData(newCartList as OrderProductDTO[]);
		}
	};
	/** 获取匹配的购物车列表 */
	const getMatchCartList = (
		productIdsList: OrderProductDTO[],
		newCartList?: OrderProductDTO[]
	) => {
		// 深拷贝下
		const _cartList = newCartList?.length
			? JSON.parse(JSON.stringify(newCartList))
			: JSON.parse(JSON.stringify(cartList));
		// 遍历大数组
		for (let i = 0; i < _cartList.length; i++) {
			const currentItem: OrderProductDTO = _cartList[i];
			// 检查当前对象是否与子数组中的对象匹配
			for (const subItem of productIdsList) {
				if (currentItem.cartId === subItem.cartId) {
					// 如果匹配成功，则替换大数组中对应对象
					_cartList[i] = subItem;
					break;
				}
			}
		}
		return _cartList;
	};
	/** 购物车列表更新需要重新存储数据 */
	const changeSetCartOriginalData = (
		data: OrderProductDTO[],
		ids?: number[]
	) => {
		// 有效的购物车列表
		// const effectCartList = data?.filter(
		// 	(item) => item.sellStatus === Enum_SellStatus.Normal
		// );
		/** 有效的购物车id */
		let effectCartIds = getEffectCartIds(data);
		if (ids?.length) {
			effectCartIds = ids;
		}
		setSelectCartIds(effectCartIds as CheckboxValue[]);
		setCartOriginalData({
			...cartOriginalData,
			cartList: data,
			cartNum: data.length ?? 0
		});
	};
	/** 勾选购物车调用计算价格接口 */
	const getSelectedCart = async (effectSelectCartIds?: number[]) => {
		const ids = (
			effectSelectCartIds && effectSelectCartIds.length
				? effectSelectCartIds
				: selectCartIds
		) as number[];
		const data = await calculatePrice({
			ids
		});
		console.log(data, 'data');
		if (!data.success) {
			return;
		}
		const {
			productList = [],
			productTotalAmount = 0,
			productTotalQuantity = 0
		} = data.data ?? {};
		// 重新匹配下，更改改过的商品
		const _cartList = getMatchCartList(productList);
		changeSetCartOriginalData(_cartList, ids);
		setProductSum({
			// 购物车商品总数量
			productTotalQuantity,
			// 购物车商品总金额
			productTotalAmount
		});
		return productList;
	};
	/** 勾选购物车方法 */
	const handleChangeSelectedCart = async (
		cartIds: CheckboxValue[],
		isNotRequest?: boolean
	) => {
		// 如果都是失效商品，则不需要重新请求
		if (isNotRequest) {
			return;
		}
		// 如果取消全部勾选，则重新请求下接口
		if (!cartIds?.length) {
			const ids = cartList.map((item) => item.cartId) as number[];
			const params = {
				ids,
				selectedFlag: Enum_SelectedFlag.Unselected
			};
			await getCartList({
				params
			});
			setProductSum({
				// 购物车商品总数量
				productTotalQuantity: 0,
				// 购物车商品总金额
				productTotalAmount: 0
			});
			return;
		}
		// 如果多个勾选，取消某个勾选，则重新请求，需要更新取消勾选那项的信息
		if (cartIds.length < selectCartIds.length) {
			// 过滤出取消勾选的购物车id
			const normalCartIdList = selectCartIds?.filter(
				(item) => !cartIds?.includes(item)
			);
			const _params = {
				ids: normalCartIdList,
				selectedFlag: Enum_SelectedFlag.Unselected
			} as UpdateCartSelectedReq;
			const cartId = selectCartIds.filter(
				(item) => !normalCartIdList.includes(item)
			) as number[];
			await getCartList({
				params: _params,
				cartId,
				isCancelSelected: true
			});
			return;
		}
		// 过滤出勾选的购物车id
		const normalCartIdList = cartIds?.filter(
			(item) => !selectCartIds?.includes(item)
		);
		const _params = {
			ids: normalCartIdList,
			selectedFlag: Enum_SelectedFlag.Selected
		} as UpdateCartSelectedReq;
		const cartId = normalCartIdList.concat(selectCartIds);
		await getCartList({
			params: _params,
			cartId: cartId as number[],
			isNotUpdateCartList: true
		});
	};
	/** 获取过滤后的购物车 */
	const getFilterCartList = (
		status: Enum_SellStatus,
		isNeedNegation?: boolean
	) => {
		if (!cartList.length) {
			return [];
		}
		// 取反
		if (isNeedNegation) {
			return cartList?.filter((item) => item.sellStatus !== status);
		}
		return cartList?.filter((item) => item.sellStatus === status);
	};
	console.log(
		cartList.map((i) => i.freeShippingAmount),
		'cartList'
	);
	return (
		<div className="layout-style">
			<ScrollPage
				title={
					<CustomNavbar
						navBarRight={
							Boolean(cartList.length) && (
								<div
									className="shop-cart-delete"
									onClick={() => deleteShopCart()}
								>
									<DeleteLine
										width={'0.16rem'}
										height={'0.16rem'}
									/>
								</div>
							)
						}
						title={window._$m.t('购物车')}
						showBackHome={true}
					/>
				}
			>
				<div
					className={`layout-content shopcart-layout-content ${!cartList?.length ? 'shopcart-empty-layout-content' : ''}`}
				>
					{(updateSelectedLoading ||
						calculatePriceLoading ||
						listLoading ||
						deleteLoading ||
						updateLoading) && <Loading />}
					{!cartList?.length ? (
						<div className="shop-cart-empty">
							<img
								className="empty-img"
								alt="empty"
								src="https://static-jp.theckb.com/static-asset/easy-h5/empty_shopcard.svg"
							/>

							<div className="empty-desc">
								{window._$m.t(
									'购物车空空如也，快去加购商品吧～'
								)}
							</div>
							<Button
								className="empty-btn"
								color="primary"
								shape="rounded"
								onClick={async () => {
									// 如果是嵌入app
									if (window?.ReactNativeWebView) {
										await jsBridge.postMessage({
											type: 'DIRCT_goHome',
											payload: {}
										});
										return;
									}
									navigate('/goods/home');
								}}
							>
								{window._$m.t('去购物')}
							</Button>
						</div>
					) : (
						<div className="shop-cart">
							<div className="shop-cart-content">
								<CartContent
									selectCartIds={selectCartIds}
									handleChangeSelectedCart={
										handleChangeSelectedCart
									}
									effectCartList={getFilterCartList(
										Enum_SellStatus.Normal
									)}
									invalidCartList={getFilterCartList(
										Enum_SellStatus.Normal,
										true
									)}
									couponInfo={
										calculatePriceData?.data?.couponInfo
									}
									handleChangeQuantity={handleChangeQuantity}
									removeInvalidGoods={deleteShopCart}
								/>
							</div>
							<div
								className={`shop-cart-footer shopcart-footer-notabbar`}
							>
								<CartFooter
									selectCartIds={selectCartIds}
									handleChangeSelectedCart={
										handleChangeSelectedCart
									}
									calculatePriceData={
										calculatePriceData?.data || {}
									}
									effectCartList={getFilterCartList(
										Enum_SellStatus.Normal
									)}
									productSum={productSum}
									getCartList={getCartList}
								/>
							</div>
						</div>
					)}
				</div>
			</ScrollPage>
		</div>
	);
};
export default memo(ShoppingCart);
