/*
 * @Author: yusha
 * @Date: 2023-10-16 16:35:50
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-10 17:43:21
 * @Description: 购物车内容
 */
import { Checkbox } from 'antd-mobile';
import { CheckboxValue } from 'antd-mobile/es/components/checkbox';
import { memo } from 'react';
import { CouponInfoDTO, OrderProductDTO } from '@/service/easyOrder';
import ItemDetail from '@/component/ShoppingCartItemDetail';
import './index.scss';
interface CartContentProps {
	// /** 勾选方法 */
	handleChangeSelectedCart: (val: CheckboxValue[]) => void;
	/** 选中的 */
	selectCartIds: CheckboxValue[];
	/** 有效购物车列表 */
	effectCartList: OrderProductDTO[];
	/** 失效的购物车列表 */
	invalidCartList: OrderProductDTO[];
	/** 更改数量 */
	handleChangeQuantity?: (val) => void;
	/** 清除失效商品 */
	removeInvalidGoods: (val) => void;
	couponInfo?: CouponInfoDTO;
}
const CartContent = (props: CartContentProps) => {
	const {
		handleChangeSelectedCart,
		effectCartList,
		invalidCartList,
		selectCartIds,
		handleChangeQuantity,
		removeInvalidGoods,
		couponInfo
	} = props;
	console.log(couponInfo, 'couponInfo');
	return (
		<>
			{Boolean(effectCartList?.length) && (
				<div className="cart-effect">
					<Checkbox.Group
						value={selectCartIds}
						onChange={(val) => {
							handleChangeSelectedCart(val);
						}}
					>
						{effectCartList.map((item, index) => {
							return (
								<ItemDetail
									handleChangeQuantity={handleChangeQuantity}
									item={item}
									key={index}
									couponInfo={couponInfo}
								/>
							);
						})}
					</Checkbox.Group>
				</div>
			)}

			{Boolean(invalidCartList?.length) && (
				<div className="cart-invalid">
					<div className="invalid-title">
						<span className="desc">{window._$m.t('失效商品')}</span>
						<span
							className="icon"
							onClick={() => {
								const invalidCartids =
									invalidCartList.length > 0
										? invalidCartList.map(
												(item) => item.cartId
											)
										: [];
								removeInvalidGoods(invalidCartids);
							}}
						>
							{/* <ClearexpiredItems /> */}
							{window._$m.t('清除失效商品')}
						</span>
					</div>
					<div>
						<Checkbox.Group disabled>
							{invalidCartList.map((item, index) => {
								return (
									<ItemDetail
										item={item}
										disabled
										key={index}
									/>
								);
							})}
						</Checkbox.Group>
					</div>
				</div>
			)}
		</>
	);
};
export default memo(CartContent);
