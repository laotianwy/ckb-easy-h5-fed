import CollapseSmall from '@/common/icons/CollapseSmall';
import ExpandSmall from '@/common/icons/ExpandSmall';
const PopupImgs = (props) => {
	const { goodsExpend, discountPrice, setGoodsExpend } = props;
	return (
		<div className="popup-imgs">
			{goodsExpend
				? discountPrice?.productList?.map((item, index) => {
						return (
							<img
								alt=""
								className="popup-img"
								key={index}
								src={item.productImg}
							/>
						);
					})
				: discountPrice?.productList &&
					discountPrice?.productList
						.slice(0, 4)
						.map((item, index) => {
							return (
								<img
									alt=""
									className="popup-img"
									key={index}
									src={item.productImg}
								/>
							);
						})}
			{discountPrice.productList &&
				discountPrice.productList.length > 4 && (
					<div
						className="popup-expend"
						onClick={() => {
							setGoodsExpend(!goodsExpend);
						}}
					>
						<div>
							{window._$m.t('已选')}

							{discountPrice.productList.length}
							{window._$m.t('件')}
						</div>
						<div
							style={{
								marginTop: '0.06rem'
							}}
						>
							{goodsExpend ? (
								<CollapseSmall width="0.1rem" height="0.1rem" />
							) : (
								<ExpandSmall width="0.1rem" height="0.1rem" />
							)}
						</div>
					</div>
				)}
		</div>
	);
};
export default PopupImgs;
