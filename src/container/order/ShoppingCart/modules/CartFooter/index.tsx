/*
 * @Author: yusha
 * @Date: 2023-10-16 16:02:16
 * @LastEditors: yusha
 * @LastEditTime: 2024-04-08 16:41:55
 * @Description: 购物车底部footer
 */

import { Button, Checkbox, Mask, Popup, Toast } from 'antd-mobile';
import { CheckboxValue } from 'antd-mobile/es/components/checkbox';
import { memo, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAtom } from 'jotai';
import { useRequest } from 'ahooks';
import { request } from '@/config/request';
import CustomModal from '@/component/CustomModal';
import {
	CartCalculatePriceResp,
	OrderProductDTO,
	ProductReq
} from '@/service/easyOrder';
import CheckboxDefault from '@/common/icons/CheckboxDefault';
import CheckboxChecked from '@/common/icons/CheckboxChecked';
import { formatMoney } from '@/utils';
import { Order } from '@/Atoms';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import ExpandSmall from '@/common/icons/ExpandSmall';
import CollapseSmall from '@/common/icons/CollapseSmall';
import CloseOneguanbi from '@/common/icons/CloseOneguanbi';
import { ProductSum } from '../..';
import PopupImgs from './popupImgs';
import './index.scss';
interface CartFooterProps {
	/** 勾选方法 */
	handleChangeSelectedCart: (
		val: CheckboxValue[],
		isNotRequest?: boolean
	) => void;
	/** 选中的 */
	selectCartIds: CheckboxValue[];
	calculatePriceData: CartCalculatePriceResp;
	/** 有效的商品 */
	effectCartList: OrderProductDTO[];
	/** 购物车总计金额以及数量 */
	productSum: ProductSum;
	/** 请求购物车列表 */
	getCartList: (val) => void;
	/** 判断是否购买过活动商品，用于定位到该活动商品 */
	// setHavePurchasedCartId: (val) => void;
}
const CartFooter = (props: CartFooterProps) => {
	const navigate = useNavigate();
	const {
		selectCartIds,
		handleChangeSelectedCart,
		effectCartList = [],
		productSum,
		getCartList,
		calculatePriceData
	} = props;
	const [maskVisible, setMaskVisible] = useState(false);
	const [goodsExpend, setGoodsExpend] = useState(false);
	const [discountPrice, setDiscountPrice] = useState<CartCalculatePriceResp>(
		{}
	);
	const [discountExpend, setDiscountExpend] = useState(false);
	const { productTotalQuantity = 0, productTotalAmount = 0 } =
		productSum || {};
	useEffect(() => {
		setDiscountPrice({ ...calculatePriceData });
		if (productSum.productTotalQuantity < 1) {
			setDiscountPrice({});
		}
	}, [calculatePriceData, productSum]);
	// 购物车去下单接口
	const { runAsync: calculatePrice, loading: calculatePriceLoading } =
		useRequest(request.easyOrder.order.calculatePrice, {
			manual: true
		});

	/** 提交订单 */
	const submitOrder = async () => {
		if (!selectCartIds.length) {
			Toast.show({
				content: window._$m.t('请先勾选商品')
			});
			return;
		}
		taTrack({
			event: TaEvent.ORDER_SUBMIT,
			value: {
				order_product_detail: selectCartIds.map((item) => {
					const productItem = effectCartList.find((i) => {
						return i?.cartId === item;
					});
					return {
						product_code: productItem?.productCode,
						product_title_ja: productItem?.productName
					};
				})
			}
		});
		const productList = selectCartIds.map((item) => {
			return {
				cartId: item
			};
		}) as ProductReq[];
		const data = await calculatePrice({
			productList
		});
		if (!data.success && data.msg) {
			CustomModal.confirm({
				content: data.msg,
				showCloseButton: false,
				onCancel: () => {
					// 关闭后请求列表
					if (data.code === '33000010' || data.code === '33000009') {
						getCartList({});
						return;
					}
					// 关闭后需要定位到对应sku下商品
					if (data.code === '33000003') {
						// setHavePurchasedCartId(data.data);
						const dom = document.getElementById(`sku-${data.data}`);
						if (dom) {
							dom.scrollIntoView();
						}
					}
				}
			});
			return;
		}
		navigate(`/order/create?cartIds=${JSON.stringify(selectCartIds)}`);
		// console.log(window._$m.t('跳转下单页面'));
	};
	console.log(discountPrice,'discountPrice');
	
	return (
		<>
			<div className="footer-content">
				<div>
					<div className="footer-checkbox">
						<Checkbox
							checked={
								Boolean(effectCartList.length) &&
								selectCartIds.length === effectCartList.length
							}
							icon={(checked) =>
								checked ? (
									<CheckboxChecked
										color={'#1C2026'}
										width={'0.16rem'}
										height={'0.16rem'}
									/>
								) : (
									<CheckboxDefault
										width={'0.16rem'}
										height={'0.16rem'}
									/>
								)
							}
							onChange={(val) => {
								// 如果为true代表全选
								if (val) {
									const ids = effectCartList.map(
										(item) => item.cartId
									) as CheckboxValue[];
									// 如果都是失效商品，则不需要重新请求
									const isNotRequest = !ids?.length;
									handleChangeSelectedCart(ids, isNotRequest);
								} else {
									handleChangeSelectedCart([]);
									setDiscountPrice({});
								}
							}}
						>
							<span
								style={{
									fontSize: '0.14rem',
									fontWeight: '600',
									marginTop: '-0.02rem'
								}}
							>
								{window._$m.t('全选')}
							</span>
						</Checkbox>
					</div>
					<div className="total-quantity">
						{window._$m.t('已选{{num}}件', {
							data: {
								num: productTotalQuantity
							}
						})}
					</div>
				</div>
				<div className="footer-detail">
					<div
						style={{
							textAlign: 'right'
						}}
					>
						<span
							className="total-amount"
							// 用于解决跳转购物车之后，价格未变化的问题
							// key={discountPrice?.payableAmount}
						>
							{formatMoney(discountPrice?.payableAmount || 0)}
							{window._$m.t('円')}
						</span>
						{Boolean(discountPrice?.discountTotalAmount) && (
							<div
								className="total-quantity"
								onClick={() => setMaskVisible(!maskVisible)}
							>
								<span>{window._$m.t('共减')}</span>
								<span
									style={{
										color: '#FF5010',
										marginRight: '0.04rem'
									}}
								>
									{formatMoney(
										discountPrice?.discountTotalAmount
									)}
									{window._$m.t('円')}
								</span>
								<span
									style={{
										color: '#FF5010',
										marginRight: '0.02rem'
									}}
								>
									{window._$m.t('金额明细')}
								</span>
								{!maskVisible ? (
									<ExpandSmall
										width="0.1rem"
										height="0.1rem"
										color="#FF5010"
									/>
								) : (
									<CollapseSmall
										width="0.1rem"
										height="0.1rem"
										color="#FF5010"
									/>
								)}
							</div>
						)}
					</div>
				</div>
				<div className="ml-12">
					<Button
						className="footer-btn"
						color="primary"
						shape="rounded"
						onClick={submitOrder}
						loading={calculatePriceLoading}
					>
						{window._$m.t('提交订单')}
					</Button>
				</div>
			</div>
			<Mask
				destroyOnClose
				visible={maskVisible}
				getContainer={() => document.getElementById('root')!}
				className="confirm-order-detail-mask"
			>
				<div className="confirm-order-footer">
					{maskVisible && (
						<div
							className="footer-close"
							onClick={() => setMaskVisible(!maskVisible)}
						>
							<CloseOneguanbi />
						</div>
					)}

					{maskVisible && (
						<div className="footer-popup-content">
							<div className="popup-title">
								{window._$m.t('费用明细')}
							</div>
							<div className="popup-tip">
								{window._$m.t('实际优惠金额请以下单页为准')}
							</div>
							<PopupImgs
								goodsExpend={goodsExpend}
								discountPrice={discountPrice}
								setGoodsExpend={setGoodsExpend}
							/>

							<div className="popup-content">
								<div className="flex-between">
									<span className="title-desc">
										{window._$m.t('商品总价')}
									</span>
									<span className="title-amount">
										{formatMoney(productTotalAmount)}
										{window._$m.t('円')}
									</span>
								</div>
								<div className="flex-between">
									<span className="title-desc">
										{window._$m.t('共减')}
									</span>
									<span
										className="title-amount"
										style={{
											color: '#FF5010'
										}}
										onClick={() => {
											setDiscountExpend(!discountExpend);
										}}
									>
										<span
											style={{
												marginRight: '0.04rem'
											}}
										>
											{formatMoney(
												discountPrice?.discountTotalAmount
											)}
											{window._$m.t('円')}
										</span>
										{discountExpend ? (
											<CollapseSmall
												width="0.1rem"
												height="0.1rem"
												color="#FF5010"
											/>
										) : (
											<ExpandSmall
												width="0.1rem"
												height="0.1rem"
												color="#FF5010"
											/>
										)}
									</span>
								</div>
								{discountExpend && (
									<div className="discount-content">
										<div className="discount-goods">
											<div className="discount-imgs">
												{discountPrice?.productCouponDiscountDetail?.couponProductList?.map(
													(item, index) => {
														return (
															<img
																alt=""
																className="discount-img"
																key={index}
																src={
																	item.productImg
																}
															/>
														);
													}
												)}
											</div>
											<div className="discount-info">
												<span className="discount-coupon">
													{
														discountPrice
															?.couponInfo
															?.couponName
													}
												</span>
												<span className="discount-price">
													{window._$m.t('小计')}

													{
														discountPrice
															?.productCouponDiscountDetail
															?.productTotalAmount
													}

													{window._$m.t('円，减')}

													{
														discountPrice?.discountTotalAmount
													}

													{window._$m.t('円')}
												</span>
											</div>
										</div>
									</div>
								)}
							</div>
							<div className="popup-total">
								{window._$m.t('合计')}

								{formatMoney(discountPrice?.payableAmount)}
								{window._$m.t('円')}
							</div>
						</div>
					)}

					<div className="footer-content">
						<div>
							<div className="footer-checkbox">
								<Checkbox
									checked={
										Boolean(effectCartList.length) &&
										selectCartIds.length ===
											effectCartList.length
									}
									icon={(checked) =>
										checked ? (
											<CheckboxChecked
												color={'#1C2026'}
												width={'0.16rem'}
												height={'0.16rem'}
											/>
										) : (
											<CheckboxDefault
												width={'0.16rem'}
												height={'0.16rem'}
											/>
										)
									}
									onChange={(val) => {
										// 如果为true代表全选
										if (val) {
											const ids = effectCartList.map(
												(item) => item.cartId
											) as CheckboxValue[];
											// 如果都是失效商品，则不需要重新请求
											const isNotRequest = !ids?.length;
											handleChangeSelectedCart(
												ids,
												isNotRequest
											);
										} else {
											handleChangeSelectedCart([]);
											setDiscountPrice({});
										}
										setMaskVisible(false);
									}}
								>
									<span
										style={{
											fontSize: '0.14rem',
											fontWeight: '600',
											marginTop: '-0.02rem'
										}}
									>
										{window._$m.t('全选')}
									</span>
								</Checkbox>
							</div>
							<div className="total-quantity">
								{window._$m.t('已选{{num}}件', {
									data: {
										num: productTotalQuantity
									}
								})}
							</div>
						</div>
						<div className="footer-detail">
							<div
								style={{
									textAlign: 'right'
								}}
							>
								<span className="total-amount">
									{formatMoney(discountPrice?.payableAmount)}
									{window._$m.t('円')}
								</span>
								<div
									className="total-quantity"
									onClick={() => setMaskVisible(!maskVisible)}
								>
									<span>{window._$m.t('共减')}</span>
									<span
										style={{
											color: '#FF5010',
											marginRight: '0.04rem'
										}}
									>
										{formatMoney(
											discountPrice?.discountTotalAmount
										)}
										{window._$m.t('円')}
									</span>
									<span
										style={{
											color: '#FF5010',
											marginRight: '0.02rem'
										}}
									>
										{window._$m.t('金额明细')}
									</span>
									{!maskVisible ? (
										<ExpandSmall
											color={'#FF5010'}
											width="0.1rem"
											height="0.1rem"
										/>
									) : (
										<CollapseSmall
											color={'#FF5010'}
											width="0.1rem"
											height="0.1rem"
										/>
									)}
								</div>
							</div>
						</div>
						<div className="ml-12">
							<Button
								className="footer-btn"
								color="primary"
								shape="rounded"
								onClick={submitOrder}
								loading={calculatePriceLoading}
							>
								{window._$m.t('提交订单')}
							</Button>
						</div>
					</div>
				</div>
			</Mask>
		</>
	);
};
export default memo(CartFooter);
