/*
 * @Author: yusha
 * @Date: 2023-10-20 17:32:26
 * @LastEditors: yusha
 * @LastEditTime: 2023-10-20 17:36:00
 * @Description: interface
 */
/** 通关名义 */
export enum EnumClearanceType {
	Company = 1,
	Person = 2
}
export enum EnumDefaultFlag {
	No = 0,
	Yes = 1
}
export enum ENUM_PAY_WAY {
	WALLET = 'WALLET',
	PAYPAL = 'PAYPAL',
	PAYPAL_CREDIT = 'PAYPAL_CREDIT'
}
