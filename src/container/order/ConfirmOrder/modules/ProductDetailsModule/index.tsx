/*
 * @Author: yusha
 * @Date: 2023-10-18 15:04:46
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-13 14:11:24
 * @Description: 产品详情模块
 */
import { memo } from 'react';
import {
	OrderProductDTO,
	CouponInfoDTO,
	OrderCalculatePriceResp
} from '@/service/easyOrder';
import ItemDetail from '@/component/ShoppingCartItemDetail';
import './index.scss';
import { Enum_SellStatus } from '@/common/Enums';
import { formatMoney } from '@/utils';
import ActiveTag from '@/component/ActiveTag';
interface ProductDetailsModuleProps {
	/** 订单信息 */
	orderInfo?: OrderCalculatePriceResp;
}
const ProductDetailsModule = (props: ProductDetailsModuleProps) => {
	const { orderInfo = {} } = props;
	const { handyProductList = [], productList = [], couponInfo } = orderInfo;
	return (
		<div className="product-details-module">
			{/* 普通商品,包邮商品 */}
			{Boolean(productList?.length) && (
				<div className="normal-goods">
					{productList?.map((item, index) => {
						return (
							<ItemDetail
								item={item}
								couponInfo={couponInfo}
								fromPage="confirmOrder"
								key={index}
								disabled={
									item.sellStatus !== Enum_SellStatus.Normal
								}
							/>
						);
					})}
					<div className="no-postage-tip">
						{Boolean(orderInfo.achieveFreeShippingAmount) && (
							<div className="freight-advice">
								<div style={{ marginBottom: '8px' }}>
									<ActiveTag
										text={
											(orderInfo.newUserFlag
												? window._$m.t('新人专享！')
												: '') +
											window._$m.t(
												'订单满{{num}}円以上包邮',
												{
													data: {
														num: formatMoney(
															orderInfo.freeShippingAmount
														)
													}
												}
											)
										}
									/>
								</div>
								<div>
									<span>
										{window._$m.t('还差')}
										<span className="special-style">
											&nbsp;
											{formatMoney(
												orderInfo.achieveFreeShippingAmount
											)}
											{window._$m.t('円')}
										</span>
										{window._$m.t('即可达到包邮门槛')}
									</span>
								</div>
							</div>
						)}
					</div>
				</div>
			)}
			{/* 特殊商品（不参与包邮活动的商品） */}
			{Boolean(handyProductList?.length) && (
				<div className="special-goods">
					{handyProductList?.map((item, index) => {
						return (
							<ItemDetail
								item={item}
								couponInfo={couponInfo}
								fromPage="confirmOrder"
								key={index}
								disabled={
									item.sellStatus !== Enum_SellStatus.Normal
								}
							/>
						);
					})}
				</div>
			)}
		</div>
	);
};
export default memo(ProductDetailsModule);
