import { Checkbox } from 'antd-mobile';
import './index.scss';
const CouponInfo = (props) => {
	const { couponChecked, setCouponChecked, couponInfo, orderInfo } = props;
	return (
		<div className="coupon-select">
			<div className="coupon-title">{window._$m.t('优惠券')}</div>
			<div className="coupon-info">
				<Checkbox
					checked={couponChecked}
					onClick={() => {
						setCouponChecked(!couponChecked);
					}}
				>
					{couponInfo?.couponName}
				</Checkbox>
				{Boolean(orderInfo?.discountAmount) && (
					<div className="coupon-price">
						-{orderInfo?.discountAmount}
						{window._$m.t('円')}
					</div>
				)}
			</div>
		</div>
	);
};
export default CouponInfo;
