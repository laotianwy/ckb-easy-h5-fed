/*
 * @Author: yusha
 * @Date: 2023-10-18 11:24:59
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-15 16:32:20
 * @Description: 确认订单footer
 */

import { Mask, Toast } from 'antd-mobile';
import { memo, useState } from 'react';
import ExpandSmall from '@/common/icons/ExpandSmall';
import CollapseSmall from '@/common/icons/CollapseSmall';
import {
	OrderCalculatePriceResp,
	UserPlaceOrderAddressDTO,
	UserReceivingAddressDTO
} from '@/service/easyOrder';
import { formatMoney } from '@/utils';
import CloseOneguanbi from '@/common/icons/CloseOneguanbi';
import ActiveTag from '@/component/ActiveTag';
import { ENUM_PAY_WAY } from '../../config';
import './index.scss';
interface ConfirmOrderFooterProps {
	/** 订单信息 */
	orderInfo?: OrderCalculatePriceResp;
	/** 创建订单 */
	createOrder: () => void;
	/** 选择的地址 */
	selectedAddress?: UserReceivingAddressDTO;
	/** 选中的下单人信息 */
	selectedPersonInfo?: UserPlaceOrderAddressDTO;
	selectPayWay?: ENUM_PAY_WAY;
}
interface NeedExpendType {
	/** 商品图片是否展开 */
	goodsExpend: boolean;
	/** 优惠券信息是否展开 */
	couponExpend: boolean;
	/** 国际运费是否展开 */
	internationalAmountExpend: boolean;
}
const ConfirmOrderFooter = (props: ConfirmOrderFooterProps) => {
	const { orderInfo, createOrder, selectedAddress, selectPayWay } = props;
	const {
		freeShippingAmount = 0,
		internationalShippingAmount = 0,
		payableAmount = 0,
		productTotalAmount = 0,
		productNum,
		achieveFreeShippingAmount = 0,
		newUserFlag,
		couponInfo,
		discountAmount,
		// 国际运费减多少
		internationalDiscountAmount = 0,
		// 顺手捎件数
		handyProductNum = 0
	} = orderInfo ?? {};
	const [maskVisible, setMaskVisible] = useState(false);
	/** 是否展开状态集合 */
	const [expendStatus, setExpendStatus] = useState<NeedExpendType>({
		goodsExpend: false,
		couponExpend: false,
		internationalAmountExpend: false
	});
	/** 点击支付 */
	const submitCreateOrder = () => {
		setMaskVisible(false);
		const isNotExistAddress = !selectedAddress?.countryId;
		if (isNotExistAddress) {
			Toast.show(window._$m.t('请选择地址'));
			return;
		}
		if (!selectPayWay) {
			Toast.show(window._$m.t('请选择交易方式'));
			return;
		}
		createOrder();
	};
	/** 获取选中的商品信息 */
	const getSelectProductList = () => {
		// 普通商品
		const _productList = orderInfo?.productList ?? [];
		// 不包邮商品
		const _handyProductList = orderInfo?.handyProductList ?? [];
		const newData = _productList.concat(_handyProductList);
		return newData;
	};
	return (
		<div className="confirm-order-footer" id="confirmOrderFooter">
			<Mask
				destroyOnClose
				visible={maskVisible}
				getContainer={() => document.getElementById('root')!}
				className="confirm-order-detail-mask"
			>
				<div className="confirm-order-footer1">
					{maskVisible && (
						<div
							className="footer-close"
							onClick={() => {
								setMaskVisible(!maskVisible);
								setExpendStatus({
									goodsExpend: false,
									couponExpend: false,
									internationalAmountExpend: false
								});
							}}
						>
							<CloseOneguanbi />
						</div>
					)}

					{maskVisible && (
						<div className="footer-detail">
							<div className="detail-title">
								{window._$m.t('费用明细')}
							</div>
							<div className="confirm-popup-imgs">
								{expendStatus.goodsExpend
									? getSelectProductList().map(
											(item, index) => {
												return (
													<img
														alt=""
														className="confirm-popup-img"
														key={index}
														src={item.productImg}
													/>
												);
											}
										)
									: getSelectProductList()
											?.slice(0, 4)
											.map((item, index) => {
												return (
													<img
														alt=""
														className="confirm-popup-img"
														key={index}
														src={item.productImg}
													/>
												);
											})}
								{getSelectProductList() &&
									getSelectProductList().length > 4 && (
										<div
											className="confirm-popup-expend"
											onClick={() => {
												setExpendStatus({
													...expendStatus,
													goodsExpend:
														!expendStatus.goodsExpend
												});
											}}
										>
											<div>
												{window._$m.t('已选')}

												{getSelectProductList().length}
												{window._$m.t('件')}
											</div>
											<div
												style={{
													marginTop: '0.06rem'
												}}
											>
												{expendStatus.goodsExpend ? (
													<CollapseSmall
														width="0.1rem"
														height="0.1rem"
													/>
												) : (
													<ExpandSmall
														width="0.1rem"
														height="0.1rem"
													/>
												)}
											</div>
										</div>
									)}
							</div>
							<div className="detail-content">
								<div className="flex-between">
									<span className="title-desc">
										{window._$m.t('商品金额')}
									</span>
									<span className="title-amount">
										{formatMoney(productTotalAmount)}
										{window._$m.t('円')}
									</span>
								</div>
								{/* 优惠券 */}
								{Boolean(discountAmount) && (
									<div>
										<div className="flex-between">
											<span className="title-desc">
												{window._$m.t('优惠券')}
											</span>
											<div
												style={{
													display: 'flex',
													alignItems: 'center'
												}}
											>
												<span
													className="title-amount-special"
													onClick={() => {
														setExpendStatus({
															...expendStatus,
															couponExpend:
																!expendStatus.couponExpend
														});
													}}
												>
													{-discountAmount}
													{window._$m.t('円')}
												</span>
												{expendStatus.couponExpend ? (
													<CollapseSmall
														width="0.13rem"
														height="0.13rem"
														color="#7E8694"
														className="ml-8"
													/>
												) : (
													<ExpandSmall
														width="0.13rem"
														height="0.13rem"
														color="#7E8694"
														className="ml-8"
													/>
												)}
											</div>
										</div>
										{expendStatus.couponExpend && (
											<div className="expend-content flex-align-center">
												<span>
													{window._$m.t('已选择')}
												</span>
												<div className="confirm-coupon-box">
													<span className="confirm-coupon-couponName">
														{couponInfo?.couponName}
													</span>
												</div>
											</div>
										)}
									</div>
								)}
								{/* 国际运费 */}
								<div>
									<div className="flex-between margin-bootom-8">
										<span className="title-desc">
											{window._$m.t('国际运费')}
										</span>
										<div
											style={{
												display: 'flex',
												alignItems: 'center'
											}}
										>
											<span
												className="title-amount-special"
												onClick={() => {
													// 如果没有包邮金额
													if (!freeShippingAmount) {
														return;
													}
													// 且满足包邮条件
													if (
														orderInfo.achieveFreeShippingAmount
													) {
														return;
													}
													setExpendStatus({
														...expendStatus,
														internationalAmountExpend:
															!expendStatus.internationalAmountExpend
													});
												}}
											>
												{formatMoney(
													internationalShippingAmount
												)}
												{window._$m.t('円')}
											</span>
											{/* 包邮金额大于0，才需要展开详细内容 */}
											{/* 且满足包邮条件 */}
											{freeShippingAmount > 0 &&
												!orderInfo.achieveFreeShippingAmount && (
													<div className="ml-8">
														{expendStatus.internationalAmountExpend ? (
															<CollapseSmall
																width="0.13rem"
																height="0.13rem"
																color="#7E8694"
															/>
														) : (
															<ExpandSmall
																width="0.13rem"
																height="0.13rem"
																color="#7E8694"
															/>
														)}
													</div>
												)}
										</div>
									</div>
									{expendStatus.internationalAmountExpend && (
										<div className="expend-content flex-column">
											<div>
												{/* 当达到包邮条件才展示 */}
												<ActiveTag
													tips={
														!selectedAddress?.countryId &&
														!newUserFlag
															? window._$m.t(
																	'活动适用于东京都，具体运费以您的收货地区为准'
																)
															: ''
													}
													text={
														(newUserFlag
															? window._$m.t(
																	'新人专享！'
																)
															: '') +
														window._$m.t(
															'订单满{{num}}円以上包邮',
															{
																data: {
																	num: formatMoney(
																		freeShippingAmount
																	)
																}
															}
														)
													}
												/>

												{(internationalDiscountAmount ??
													0) > 0 && (
													<span className="freight-discount-desc">
														{window._$m.t(
															'已减{{num}}円',
															{
																data: {
																	num:
																		internationalDiscountAmount ??
																		0
																}
															}
														)}
													</span>
												)}
											</div>

											{(handyProductNum ?? 0) > 0 && (
												<span className="free-special-desc">
													{window._$m.t(
														'已减免{{num}}件特殊商品邮费',
														{
															data: {
																num:
																	handyProductNum ??
																	0
															}
														}
													)}
												</span>
											)}
										</div>
									)}
									{/* 满足包邮条件展示 */}
									{Boolean(freeShippingAmount) &&
										!orderInfo.achieveFreeShippingAmount && (
											<div className="freight-desc">
												<img
													className="warning-icon"
													alt="warning"
													src="https://static-jp.theckb.com/static-asset/easy-h5/warningIcon@3x.png"
												/>
												<span>
													{window._$m.t(
														'包邮活动以商品代金优惠后的金额为准'
													)}
												</span>
											</div>
										)}
								</div>
							</div>
						</div>
					)}

					<div>
						<div className="footer-content1">
							<div className="footer-total">
								<span className="total-num">
									{window._$m.t('已选{{num}}件', {
										data: {
											num: productNum
										}
									})}
								</span>
								<div className="footer-total-money">
									<span
										style={{
											fontSize: '0.12rem'
										}}
									>
										{window._$m.t('合计：')}
									</span>
									<div className="total-amount">
										{formatMoney(payableAmount)}
										{window._$m.t('円')}
									</div>
								</div>
								<div
									className="total-quantity"
									onClick={() => {
										// 当点击明细时，全部收起
										setExpendStatus({
											goodsExpend: false,
											couponExpend: false,
											internationalAmountExpend: false
										});
										setMaskVisible(!maskVisible);
									}}
								>
									<span className="mr-4">
										{window._$m.t('明细')}
									</span>
									{!maskVisible && (
										<ExpandSmall
											width="0.08rem"
											height="0.08rem"
										/>
									)}

									{maskVisible && (
										<CollapseSmall
											width="0.08rem"
											height="0.08rem"
										/>
									)}
								</div>
							</div>
							<div
								className="footer-btn"
								onClick={submitCreateOrder}
							>
								{window._$m.t('支付')}
							</div>
						</div>
					</div>
				</div>
			</Mask>
			<div className="confirm-order-footer2">
				{Number(handyProductNum ?? 0) > 0 && (
					<div className="reduce-special-goods">
						<div className="goods-content">
							<img
								className="car-img-style"
								alt="img"
								src="https://static-jp.theckb.com/static-asset/easy-h5/car@3x.png"
							/>
							<div
								dangerouslySetInnerHTML={{
									__html: window._$m.t(
										'已为您<span class="red-style">减免</span>了{{num}}件特殊商品的运费',
										{
											data: { num: handyProductNum ?? 0 }
										}
									)
								}}
							/>
						</div>
						<div className="triangle-style" />
					</div>
				)}

				<div className="footer-content2">
					<div className="footer-total">
						<div className="footer-total-money">
							<span
								style={{
									fontSize: '0.12rem'
								}}
							>
								{window._$m.t('合计：')}
							</span>
							<div className="total-amount">
								{formatMoney(payableAmount)}
								{window._$m.t('円')}
							</div>
						</div>
						<div
							className="total-quantity"
							onClick={() => setMaskVisible(!maskVisible)}
						>
							<span className="mr-4">{window._$m.t('明细')}</span>
							{!maskVisible ? (
								<ExpandSmall width="0.08rem" height="0.08rem" />
							) : (
								<CollapseSmall
									width="0.08rem"
									height="0.08rem"
								/>
							)}
						</div>
					</div>
					<div className="footer-btn" onClick={submitCreateOrder}>
						{window._$m.t('支付')}
					</div>
				</div>
			</div>
		</div>
	);
};
export default memo(ConfirmOrderFooter);
