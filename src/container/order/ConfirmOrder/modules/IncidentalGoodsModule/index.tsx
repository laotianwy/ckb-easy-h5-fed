/*
 * @Author: yusha
 * @Date: 2024-04-03 14:38:55
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-15 15:41:49
 * @Description: 顺手捎模块
 */

import { memo, useState } from 'react';
import qs from 'query-string';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useMount, useRequest } from 'ahooks';
import { Toast } from 'antd-mobile';
import MoreBig from '@/common/icons/MoreBig';
import Wenhaoquestion from '@/common/icons/Wenhaoquestion';
import CustomModal from '@/component/CustomModal';
import SelectGoodsSkuCom from '@/component/SelectGoodsSkuCom';
import {
	ON_HAND_GOODS_SELECT,
	ON_HAND_GOODS_SELECT_SESSION_KEY
} from '@/const/config';
import { request } from '@/config/request';
import Loading from '@/component/Loading';
import { OrderCalculatePriceResp } from '@/service/easyOrder';
import './index.scss';

interface IncidentalGoodsModuleProps {
	/** 订单信息 */
	orderInfo?: OrderCalculatePriceResp;
	getConfirmOrderInfo: () => void;
}
const IncidentalGoodsModule = (props: IncidentalGoodsModuleProps) => {
	const { orderInfo = {}, getConfirmOrderInfo } = props;
	const { freeShippingAmount = 0, achieveFreeShippingAmount = 0 } =
		orderInfo ?? {};
	const navigate = useNavigate();
	/** 点击选择sku给添加个loading，防止请求过久用户体验不好 */
	const [loading, setLoading] = useState(false);
	const [searchParams, setSearchParams] = useSearchParams();
	/** 用于存储的顺手捎商品 */
	const onHandOrder = searchParams.get(ON_HAND_GOODS_SELECT);
	const selectHanderGoods = window.sessionStorage.getItem(onHandOrder);
	const _selectHanderGoods = selectHanderGoods
		? JSON.parse(selectHanderGoods)
		: [];
	const _selectHanderGoodsNum = _selectHanderGoods
		?.map((item) => item.quantity)
		.reduce((pre, cur) => pre + cur, 0);
	/** 接口 */
	const { runAsync: apiGetGoodsList, data } = useRequest(
		request.market.handyActivity.activityProductList,
		{
			manual: true
		}
	);
	const initGoodsList = async () => {
		/** 存在内存里面已经选择的商品 */
		const selectedSku = window.sessionStorage.getItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY
		);
		const _selectedSku = selectedSku ? JSON.parse(selectedSku) : [];
		const res = await apiGetGoodsList({
			productList: _selectedSku,
			// 只会初始化一条数据，没有新增入口，可以写死
			handyBuyActivityId: 1
		});
		const records = res?.data?.records ?? [];
		// 当活动失效，但是有已加购的顺手捎商品。需要清空
		if (!records.length && _selectedSku.length) {
			window.sessionStorage.removeItem(ON_HAND_GOODS_SELECT_SESSION_KEY);
			Toast.show({
				content: window._$m.t('该活动已失效')
			});
		}
	};
	useMount(() => {
		initGoodsList();
	});
	/** 顺手捎活动说明 */
	const clickIncidentalDesc = () => {
		CustomModal.confirm({
			content: window._$m.t(
				'顺手捎中部分商品可一起包邮，超出部分需要另外计算邮费'
			),
			showCloseButton: false,
			onClose: () => {},
			cancelText: window._$m.t('我知道了')
		});
	};
	/** 前往顺手捎活动列表 */
	const goToOnHand = () => {
		const search = qs.parse(window.location.search);
		const newSearch = {
			...search
		};
		// 当前页面跳转并且刷新页面
		const url = '/goods/onHandList?' + qs.stringify(newSearch);
		navigate(url, { replace: true });
	};
	/** 选择sku商品 */
	const handleChange = (val) => {
		window.sessionStorage.setItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY,
			JSON.stringify(val)
		);
		const search = qs.parse(window.location.search);
		const newSearch = {
			...search,
			fromPage: 'onHandList',
			[ON_HAND_GOODS_SELECT]: ON_HAND_GOODS_SELECT_SESSION_KEY
		};
		// 当前页面跳转并且刷新页面
		const url = '/order/create?' + qs.stringify(newSearch);
		navigate(url, {
			replace: true
		});
		getConfirmOrderInfo();
	};

	const list = data?.data?.records ?? [];
	/** 获取还可以选择几件商品  */
	const getHandyAmount = () => {
		// 当不包邮或者不满足包邮金额则不展示
		if (!freeShippingAmount || achieveFreeShippingAmount) {
			return 0;
		}
		// 已选择的顺手捎商品总数量
		const totalSelectedNum =
			orderInfo.handyProductList?.reduce(
				(pre, cur) => pre + (cur.productQuantity ?? 0),
				0
			) ?? 0;

		// 顺手捎配置数量
		const quantityDeployed = list[0]?.handyAmount ?? 0;
		const num = quantityDeployed - totalSelectedNum;
		if (num > 0) {
			return num;
		}
		return 0;
	};
	return (
		<>
			{Boolean(list.length) && (
				<div className="confirm-incidental-goods-module">
					<div className="incidental-title">
						<span className="title">
							{window._$m.t('顺手捎')}
							{Boolean(getHandyAmount()) &&
								!orderInfo.achieveFreeShippingAmount && (
									<Wenhaoquestion
										onClick={(e) => {
											clickIncidentalDesc();
										}}
										className="question-style"
									/>
								)}
						</span>
						<div className="more-style" onClick={goToOnHand}>
							{Boolean(_selectHanderGoodsNum) && (
								<span className="gray">
									{window._$m.t('已选')}
									<span className="red">
										{window._$m.t('{{x}}件', {
											data: {
												x: _selectHanderGoodsNum
											}
										})}
									</span>
								</span>
							)}
							<MoreBig className="more-icon" />
						</div>
					</div>
					{/* 达到包邮门槛，且还可以选择数量则显示 */}
					{Boolean(getHandyAmount()) &&
						!orderInfo.achieveFreeShippingAmount && (
							<div className="incidental-desc">
								<span>
									{window._$m.t('任选{{X}}件商品享包邮活动', {
										data: { X: getHandyAmount() }
									})}
								</span>
							</div>
						)}

					<div className="goods-detail" id="cate-view-hidden-scroll">
						{list.slice(0, 5)?.map((item, index) => {
							return (
								<SelectGoodsSkuCom
									setLoading={setLoading}
									goodsInfo={item}
									key={index}
									handleChange={handleChange}
								/>
							);
						})}
					</div>
				</div>
			)}
			{loading && <Loading />}
		</>
	);
};
export default memo(IncidentalGoodsModule);
