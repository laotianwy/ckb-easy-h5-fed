/*
 * @Author: yusha
 * @Date: 2023-10-20 17:55:07
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-28 20:32:40
 * @Description: 交易方式模块
 */
import { memo, useState } from 'react';
import { Radio } from 'antd-mobile';
import { useMount } from 'ahooks';
import { useNavigate } from 'react-router-dom';
import MoreBig from '@/common/icons/MoreBig';
import QuestionMark from '@/common/icons/QuestionMark';
import CustomModal from '@/component/CustomModal';
import { PayTypeResp } from '@/service/pay';
import { formatMoney } from '@/utils';
import { request } from '@/config/request';
import './index.scss';
import { ENUM_PAY_WAY } from '../../config';
interface TransactionModeModuleProps {
	/** 选择支付方式 */
	setSelectPayWay: (val) => void;
	/** 钱包余额 */
	availableBalance: number;
	/** 是否禁用钱包余额支付 */
	isNeedDisabledWallet: boolean;
}
const TransactionModeModule = (props: TransactionModeModuleProps) => {
	const {
		setSelectPayWay,
		availableBalance = 0,
		isNeedDisabledWallet
	} = props;
	const [payWayList, setPayWayList] = useState<PayTypeResp[]>([]);
	const navigate = useNavigate();
	/** 点击问号弹出描述 */
	const clickPayWayDesc = (content) => {
		CustomModal.confirm({
			content: window._$m.t(content),
			showCloseButton: false
		});
	};
	/** 获取支付方式 */
	const getPayWay = async () => {
		const resData = await request.pay.front.payQueryType();
		let data = resData.data || [];

		/** 如果是审核版本那么过滤 paypay支付 */
		if (window._sniff_app_.isAppAuditVersion) {
			// 审核版本先写死余额支付
			data = [
				{
					key: 'WALLET',
					icon: 'https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_yu_e.png'
				}
			];
		}
		data.forEach((item) => {
			if (item.key === ENUM_PAY_WAY.WALLET) {
				item.icon =
					'https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_yu_e.png';
			}
			if (item.key === ENUM_PAY_WAY.PAYPAL) {
				item.icon =
					'https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_paypal_1.png';
			}
			if (item.key === ENUM_PAY_WAY.PAYPAL_CREDIT) {
				item.icon =
					'https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_card.png';
			}
		});
		setPayWayList(data);
	};
	useMount(() => {
		getPayWay();
	});
	/** 获取支付方式描述 */
	const getPaywayDesc = (item: PayTypeResp) => {
		console.log(availableBalance, 'availableBalance');

		// 如果是钱包余额
		if (item.key === 'WALLET') {
			return (
				<>
					<span>{window._$m.t('钱包余额')}：</span>
					<span>
						{formatMoney(availableBalance)}
						{window._$m.t('円')}
					</span>
				</>
			);
		}
		return window._$m.t(item.desc!);
	};
	return (
		<div className="transaction-mode-module">
			<div className="transaction-title">{window._$m.t('交易方式')}</div>
			<div>
				<Radio.Group onChange={(val) => setSelectPayWay(val)}>
					{payWayList.map((item, index) => {
						return (
							<div key={index} className="sigle-transaction">
								<div
									className={`transaction-way ${isNeedDisabledWallet && item.key === 'WALLET' ? 'disabled-style' : ''}`}
								>
									<Radio
										value={item.key}
										disabled={
											isNeedDisabledWallet &&
											item.key === 'WALLET'
										}
										className="transaction-way-content"
									>
										<img
											className="type-img"
											alt=""
											src={item.icon}
										/>

										<div
											className={`way-text ${item.key === 'WALLET' && isNeedDisabledWallet ? 'special-way-text' : ''}`}
										>
											<div className="yue-pay">
												{getPaywayDesc(item)}
											</div>
											{isNeedDisabledWallet &&
												item.key === 'WALLET' && (
													<div
														style={{
															color: '#E83D51'
														}}
													>
														{window._$m.t(
															'钱包余额不足，请立即前往充值'
														)}
													</div>
												)}

											<div className="flex-item">
												<img
													className="icon-style"
													src={item.img}
													alt=""
												/>

												{item.content && (
													<QuestionMark
														onClick={(e) => {
															e.preventDefault();
															clickPayWayDesc(
																item.content
															);
														}}
														className="question-style"
													/>
												)}
											</div>
										</div>
									</Radio>
								</div>
								{item.key === 'WALLET' && (
									<div
										className="flex-item-action"
										onClick={() => {
											navigate('/user/offlineRecharge');
										}}
									>
										<span className="mr-5">
											{window._$m.t('充值')}
										</span>
										<MoreBig />
									</div>
								)}
							</div>
						);
					})}
				</Radio.Group>
			</div>
		</div>
	);
};
export default memo(TransactionModeModule);
