/*
 * @Author: yusha
 * @Date: 2023-10-20 11:27:56
 * @LastEditors: yusha
 * @LastEditTime: 2024-03-27 17:59:53
 * @Description: 地址展示
 */
import { memo, useEffect, useState } from 'react';
import { Toast } from 'antd-mobile';
import qs from 'query-string';
import { useLocation, useNavigate } from 'react-router-dom';
import { useMount } from 'ahooks';
import MoreMedium from '@/common/icons/MoreMedium';
import { UserReceivingAddressDTO } from '@/service/easyOrder';
import CheckCheck from '@/common/icons/CheckCheck';
import CheckedDefault from '@/common/icons/CheckedDefault';
import { request } from '@/config/request';
import CustomModal from '@/component/CustomModal';
import MoreBig from '@/common/icons/MoreBig';
import Shoujianrenxinxi from '@/common/icons/Shoujianrenxinxi';
import Shanchutrash from '@/common/icons/Shanchutrash';
import BianjiEdit from '@/common/icons/BianjiEdit';
import XinzengdizhiAdd from '@/common/icons/XinzengdizhiAdd';
import ScrollPopup from '../../../component/ScrollPopup';
import { EnumDefaultFlag } from '../../../config';
interface AddressInfoProps {
	/** 地址列表 */
	addressList?: UserReceivingAddressDTO[];
	/** 选择的地址 */
	selectedAddress?: UserReceivingAddressDTO;
	/** 选中地址的方法 */
	setSelectedAddress: (val: UserReceivingAddressDTO) => void;
	/** 删除选中的地址时需要刷新列表 */
	getAddressList: () => void;
}
const AddressInfo = (props: AddressInfoProps) => {
	const {
		addressList = [],
		setSelectedAddress,
		selectedAddress,
		getAddressList
	} = props;
	const navigate = useNavigate();
	const { orderNo } = qs.parse(useLocation().search);
	// 如果是新增地址返回需要将弹窗展开
	const _addressVisible =
		window.sessionStorage.getItem('addAddresOrPerson') === 'address';
	/** 地址弹层显示隐藏 */
	const [addressVisible, setAddressVisible] = useState(false);
	/** 选择的地址临时存储（地址列表中选择） */
	const [temporarilySelectedAddress, setTemporarilySelectedAddress] =
		useState<UserReceivingAddressDTO>({});
	const _selectedAddressId =
		window.sessionStorage.getItem('selectedAddressId');
	/** 拼接收件人姓名 */
	function formatFullName(name: string) {
		if (name.includes('+')) {
			return name.split('+').join(' ');
		}
		return name;
	}
	useEffect(() => {
		const data = addressList.find(
			(item) => String(item.id) === _selectedAddressId
		);
		if (data) {
			setTemporarilySelectedAddress(data);
			return;
		}
		setTemporarilySelectedAddress(addressList?.[0] ?? {});
	}, [_selectedAddressId, addressList]);
	useMount(() => {
		if (_addressVisible) {
			setAddressVisible(true);
			window.sessionStorage.removeItem('addAddresOrPerson');
		}
	});
	/** 删除地址 */
	const deleteAddress = async (item) => {
		setAddressVisible(false);
		CustomModal.confirm({
			content: window._$m.t('确定从收货地址中删除吗？'),
			onConfirm: async () => {
				const resData =
					await request.easyOrder.userReceivingAddress.delete(item);
				if (resData.success) {
					if (item.id === selectedAddress?.id) {
						setSelectedAddress({});
						window.sessionStorage.removeItem('selectedAddressId');
					}
					getAddressList();
					Toast.show({
						icon: 'success',
						content: window._$m.t('删除成功')
					});
				}
			}
		});
	};
	/** 选择地址 */
	const selectAddress = (item) => {
		setSelectedAddress(item);
		window.sessionStorage.setItem('selectedAddressId', String(item.id));
		setAddressVisible(false);
	};
	return (
		<>
			{selectedAddress?.id || selectedAddress?.countryId ? (
				<div
					className="logistics-address"
					onClick={() => {
						// disabled状态地址不可修改
						if (orderNo) {
							return;
						}
						setAddressVisible(true);
					}}
				>
					<div className="selected-address-info">
						<Shoujianrenxinxi
							className="address-icon"
							width={'0.16rem'}
							height={'0.16rem'}
						/>

						<div className="info-content">
							<div className="info-title">
								<span>{window._$m.t('收件人信息')}</span>
								<span>{!orderNo && <MoreBig />}</span>
							</div>
							<div className="address-content-top">
								<div className="content-name">
									<span>
										{formatFullName(
											selectedAddress.receiveName || ''
										)}
									</span>
									&nbsp;&nbsp;
									<span>
										+81 {selectedAddress.receiveTel}
									</span>
								</div>
							</div>
							<div className="selected-info-content">
								<div>
									<span>
										{selectedAddress.countryName}{' '}
										{selectedAddress.provinceName}{' '}
										{selectedAddress.cityName}{' '}
									</span>
									<span>
										{selectedAddress.address}&nbsp;&nbsp;
										{selectedAddress.postalCode}
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			) : (
				<div
					className="logistics-address"
					onClick={() => {
						navigate(
							`/user/address/edit?fromByCreateOrder=${true}`
						);
					}}
				>
					<div className="address-content">
						<Shoujianrenxinxi
							className="address-icon"
							width={'0.16rem'}
							height={'0.16rem'}
						/>

						<span className="content-tips">
							{window._$m.t('请填写收货人地址')}
						</span>
					</div>
					<MoreMedium className="address-more-medium" />
				</div>
			)}

			<ScrollPopup
				visible={addressVisible}
				onClose={() => {
					setAddressVisible(false);
					setTemporarilySelectedAddress(selectedAddress ?? {});
					window.sessionStorage.removeItem('addAddresOrPerson');
				}}
				isNeedMaskClose={true}
				okText={window._$m.t('新增地址')}
				className="special-popup-style"
				title={window._$m.t('收件人信息')}
				onOk={() => {
					navigate('/user/address/edit');
				}}
			>
				<>
					<div>
						{addressList?.map((item, index) => {
							return (
								<div key={index}>
									<div className="sigle-address">
										<div
											className="address-content"
											onClick={(e) => {
												e.preventDefault();
												e.stopPropagation();
												selectAddress(item);
											}}
										>
											<div>
												<div className="content-name">
													{item.defaultFlag ===
														EnumDefaultFlag.Yes && (
														<div className="default-tag-style">
															{window._$m.t(
																'默认'
															)}
														</div>
													)}
													<span>
														{formatFullName(
															item.receiveName ||
																''
														)}
													</span>
													&nbsp;&nbsp;&nbsp;
													<span>
														+81 {item.receiveTel}{' '}
													</span>
												</div>
												<div className="content-detail">
													{item.countryName}{' '}
													{item.provinceName}{' '}
													{item.cityName}{' '}
													{item.address}{' '}
													{item.postalCode}
												</div>
											</div>
										</div>
										<div className="actions-btn">
											<div
												className="flex-align-center mr-16"
												onClick={(e) => {
													e.stopPropagation();
													deleteAddress(item);
												}}
											>
												<Shanchutrash
													className="delete-icon"
													fill="#1C2026"
												/>

												<span>
													{window._$m.t('删除')}
												</span>
											</div>
											<div
												className="flex-align-center "
												onClick={(e) => {
													e.stopPropagation();
													navigate(
														`/user/address/edit?addressId=${item.id}`
													);
												}}
											>
												<BianjiEdit
													fill="#1C2026"
													className="edit-icon"
												/>

												<span>
													{window._$m.t('编辑')}
												</span>
											</div>
										</div>
									</div>
								</div>
							);
						})}
					</div>
				</>
			</ScrollPopup>
		</>
	);
};
export default memo(AddressInfo);
