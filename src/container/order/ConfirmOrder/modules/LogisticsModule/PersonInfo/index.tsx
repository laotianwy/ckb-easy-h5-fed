/*
 * @Author: yusha
 * @Date: 2023-10-20 11:40:36
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-19 21:57:08
 * @Description: 下单人信息展示
 */
import { memo, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Toast } from 'antd-mobile';
import qs from 'query-string';
import { useMount } from 'ahooks';
import Avatar from '@/common/icons/Avatar';
import MoreMedium from '@/common/icons/MoreMedium';
import AddAddress from '@/common/icons/AddAddress';
import { UserPlaceOrderAddressDTO } from '@/service/easyOrder';
import EditAddress from '@/common/icons/EditAddress';
import DeleteLine from '@/common/icons/DeleteLine';
import CheckCheck from '@/common/icons/CheckCheck';
import CheckedDefault from '@/common/icons/CheckedDefault';
import CustomModal from '@/component/CustomModal';
import { request } from '@/config/request';
import Xiadanrenxinxi from '@/common/icons/Xiadanrenxinxi';
import MoreBig from '@/common/icons/MoreBig';
import XinzengdizhiAdd from '@/common/icons/XinzengdizhiAdd';
import Shanchutrash from '@/common/icons/Shanchutrash';
import BianjiEdit from '@/common/icons/BianjiEdit';
import ScrollPopup from '../../../component/ScrollPopup';
import { EnumClearanceType, EnumDefaultFlag } from '../../../config';
interface PersonInfoProps {
	/** 下单人信息列表 */
	personInfoList?: UserPlaceOrderAddressDTO[];
	/** 选中的下单人信息 */
	selectedPersonInfo?: UserPlaceOrderAddressDTO;
	/** 选择下单人信息方法 */
	setSelectedPersonInfo: (val: UserPlaceOrderAddressDTO) => void;
	/** 删除选中的下单人信息时需要刷新列表 */
	getPersonInfoList: () => void;
}
const PersonInfo = (props: PersonInfoProps) => {
	const {
		personInfoList = [],
		setSelectedPersonInfo,
		selectedPersonInfo,
		getPersonInfoList
	} = props;
	const navigate = useNavigate();
	const { orderNo } = qs.parse(useLocation().search);
	/** 下单人信息弹层显示隐藏 */
	const [infoVisible, setInfoVisible] = useState(false);
	/** 选择的下单人信息临时存储（下单人信息列表中选择） */
	const [selectedTemporarilyPersonInfo, setSelectedTemporarilyPersonInfo] =
		useState<UserPlaceOrderAddressDTO>({});
	const _selectedPersonInfoId = window.sessionStorage.getItem(
		'selectedPersonInfoId'
	);
	// 如果是新增下单人信息返回需要将弹窗展开
	const _infoVisible =
		window.sessionStorage.getItem('addAddresOrPerson') === 'personInfo';
	useMount(() => {
		if (_infoVisible) {
			setInfoVisible(true);
			window.sessionStorage.removeItem('addAddresOrPerson');
		}
	});
	useEffect(() => {
		const data = personInfoList.find(
			(item) => String(item.id) === _selectedPersonInfoId
		);
		if (data) {
			setSelectedTemporarilyPersonInfo(data);
			return;
		}
		setSelectedTemporarilyPersonInfo(personInfoList?.[0] ?? {});
	}, [_selectedPersonInfoId, personInfoList]);
	/** 删除下单人信息 */
	const deletePersonInfo = (item) => {
		setInfoVisible(false);
		CustomModal.confirm({
			content: window._$m.t('确定从下单人信息中删除吗？'),
			onConfirm: async () => {
				const resData =
					await request.easyOrder.userPlaceOrderAddress.delete(item);
				if (resData.success) {
					if (item.id === selectedPersonInfo?.id) {
						setSelectedPersonInfo({});
						window.sessionStorage.removeItem(
							'selectedPersonInfoId'
						);
					}
					getPersonInfoList();
					Toast.show({
						icon: 'success',
						content: window._$m.t('删除成功')
					});
				}
			}
		});
	};
	return (
		<>
			{selectedPersonInfo?.id || selectedPersonInfo?.countryId ? (
				<div
					className="selected-person-info"
					onClick={() => {
						// disabled状态地址不可修改
						if (orderNo) {
							return;
						}
						setInfoVisible(true);
					}}
				>
					<div className="person-info-top">
						<div className="top-icon-name">
							<Xiadanrenxinxi
								className="person-icon"
								width={'0.16rem'}
								height={'0.16rem'}
							/>

							<div className="top-info">
								<span>{window._$m.t('下单人信息')}</span>
								<span>{!orderNo && <MoreBig />}</span>
							</div>
						</div>
					</div>
					<div className="selected-info-content">
						<div className="content-name">
							<span>{selectedPersonInfo?.placeOrderName}</span>
							&nbsp;&nbsp;
							<span>+81 {selectedPersonInfo?.tel}</span>
						</div>
						<div>
							<span>{window._$m.t('通关名义')}：</span>
							<span>
								{selectedPersonInfo?.clearanceType ===
									EnumClearanceType.Company && (
									<span>{window._$m.t('企业')}</span>
								)}
							</span>
							<span>
								{selectedPersonInfo?.clearanceType ===
									EnumClearanceType.Person && (
									<span>{window._$m.t('个人')}</span>
								)}
							</span>
						</div>
						<div>
							<span>{window._$m.t('邮箱')}：</span>
							<span>{selectedPersonInfo.email}</span>
						</div>
						{selectedPersonInfo?.clearanceType ===
							EnumClearanceType.Company && (
							<>
								<div>
									<span>{window._$m.t('公司名')}：</span>
									{selectedPersonInfo.placeOrderCompanyName}
								</div>
								<div>
									<span>{window._$m.t('会社编号')}：</span>
									<span>{selectedPersonInfo.companyNo}</span>
								</div>
							</>
						)}

						<div>
							<span>{window._$m.t('地址')}：</span>
							<span>
								{selectedPersonInfo?.countryName}{' '}
								{selectedPersonInfo?.provinceName}{' '}
								{selectedPersonInfo?.cityName}{' '}
								{selectedPersonInfo?.address}{' '}
								{selectedPersonInfo?.postalCode}
							</span>
						</div>
					</div>
				</div>
			) : (
				<div
					className="logistics-address logistics-person-info"
					onClick={() => {
						navigate(`/user/person/edit?fromByCreateOrder=${true}`);
					}}
				>
					<div className="address-content">
						<Xiadanrenxinxi
							className="person-icon"
							width={'0.16rem'}
							height={'0.16rem'}
						/>

						<span className="content-tips">
							{window._$m.t('请填写下单人信息')}{' '}
						</span>
					</div>
					<MoreMedium className="address-more-medium" />
				</div>
			)}

			<ScrollPopup
				visible={infoVisible}
				onClose={() => {
					setInfoVisible(false);
					setSelectedTemporarilyPersonInfo(selectedPersonInfo ?? {});
					window.sessionStorage.removeItem('addAddresOrPerson');
				}}
				className="special-popup-style"
				title={window._$m.t('下单人信息')}
				onOk={() => {
					window.sessionStorage.setItem(
						'selectedPersonInfoId',
						String(selectedTemporarilyPersonInfo.id)
					);
					setSelectedPersonInfo(selectedTemporarilyPersonInfo);
				}}
			>
				<>
					<div className="address-add">
						<span className="left">
							{window._$m.t('下单人信息')}
						</span>
						<span
							className="right"
							onClick={() => {
								navigate('/user/person/edit');
							}}
						>
							<XinzengdizhiAdd className="add-icon" />
							{window._$m.t('新增')}
						</span>
					</div>
					<div>
						{personInfoList?.map((item, index) => {
							return (
								<div key={index}>
									<div className="sigle-address">
										<div
											className="address-content"
											onClick={() =>
												setSelectedTemporarilyPersonInfo(
													item
												)
											}
										>
											<div>
												<div className="content-name mb-8">
													{item.defaultFlag ===
														EnumDefaultFlag.Yes && (
														<div className="default-tag-style">
															{window._$m.t(
																'默认'
															)}
														</div>
													)}

													<span className="mr-8">
														{item.placeOrderName}
													</span>
													<span className="mr-4">
														+81
													</span>
													<span>{item.tel}</span>
												</div>
												<div className="content-detail">
													<span>
														{window._$m.t('邮箱')}：
													</span>
													<span>{item.email}</span>
												</div>
												<div className="content-detail">
													<span>
														{window._$m.t(
															'通关名义'
														)}
														：
													</span>
													<span>
														{item.clearanceType ===
														EnumClearanceType.Company
															? window._$m.t(
																	'企业'
																)
															: window._$m.t(
																	'个人'
																)}
													</span>
												</div>
												{item.clearanceType ===
													EnumClearanceType.Company && (
													<div className="content-detail">
														<span>
															{window._$m.t(
																'公司名'
															)}
															：
														</span>
														<span>
															{
																item.placeOrderCompanyName
															}
														</span>
													</div>
												)}

												{item.clearanceType ===
													EnumClearanceType.Company && (
													<div className="content-detail">
														<span>
															{window._$m.t(
																'会社编号'
															)}
															：
														</span>
														<span>
															{item.companyNo}
														</span>
													</div>
												)}

												<div className="content-detail">
													<div>
														<span>
															{window._$m.t(
																'地址'
															)}
															：
														</span>
														{item.countryName}{' '}
														{item.provinceName}{' '}
														{item.cityName}{' '}
														{item.address}
													</div>
												</div>
											</div>
											<div>
												{selectedTemporarilyPersonInfo.id ===
													item.id &&
												selectedTemporarilyPersonInfo.id !==
													undefined &&
												selectedTemporarilyPersonInfo.id !==
													null ? (
													<CheckCheck
														fill="#1C2026"
														className="select-icon"
													/>
												) : (
													<CheckedDefault className="select-icon select-default" />
												)}
											</div>
										</div>
										<div className="actions-btn">
											<div
												className="flex-align-center mr-16"
												onClick={() =>
													deletePersonInfo(item)
												}
											>
												<Shanchutrash
													className="delete-icon"
													fill="#1C2026"
												/>

												<span>
													{window._$m.t('删除')}
												</span>
											</div>
											<div
												className="flex-align-center "
												onClick={() => {
													navigate(
														`/user/person/edit?personId=${item.id}`
													);
												}}
											>
												<BianjiEdit
													fill="#1C2026"
													className="edit-icon"
												/>

												<span>
													{window._$m.t('编辑')}
												</span>
											</div>
										</div>
									</div>
								</div>
							);
						})}
					</div>
				</>
			</ScrollPopup>
		</>
	);
};
export default memo(PersonInfo);
