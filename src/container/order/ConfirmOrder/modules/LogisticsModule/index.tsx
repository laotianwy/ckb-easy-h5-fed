/*
 * @Author: yusha
 * @Date: 2023-10-18 14:50:25
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 15:42:36
 * @Description: 物流信息模块
 */
import { memo } from 'react';
import {
	UserPlaceOrderAddressDTO,
	UserReceivingAddressDTO
} from '@/service/easyOrder';
import AddressInfo from './AddressInfo';
import PersonInfo from './PersonInfo';
import './index.scss';
interface LogisticsModuleProps {
	/** 地址列表 */
	addressList?: UserReceivingAddressDTO[];
	/** 下单人信息列表 */
	personInfoList?: UserPlaceOrderAddressDTO[];
	/** 选择的地址 */
	selectedAddress?: UserReceivingAddressDTO;
	/** 选中地址的方法 */
	setSelectedAddress: (val: UserReceivingAddressDTO) => void;
	/** 选中的下单人信息 */
	selectedPersonInfo?: UserPlaceOrderAddressDTO;
	/** 选择下单人信息方法 */
	setSelectedPersonInfo: (val: UserPlaceOrderAddressDTO) => void;
	/** 删除选中的地址时需要刷新列表 */
	getAddressList: () => void;
	/** 删除选中的下单人信息时需要刷新列表 */
	getPersonInfoList: () => void;
}
const LogisticsModule = (props: LogisticsModuleProps) => {
	const {
		addressList = [],
		personInfoList = [],
		selectedAddress,
		setSelectedAddress,
		selectedPersonInfo,
		setSelectedPersonInfo,
		getAddressList,
		getPersonInfoList
	} = props;
	return (
		<div className="logistics-module">
			<div className="pl-pr">
				<AddressInfo
					selectedAddress={selectedAddress}
					addressList={addressList}
					setSelectedAddress={setSelectedAddress}
					getAddressList={getAddressList}
				/>
			</div>
			{/* <div className="pl-pr">
                                                                                              <PersonInfo
                                                                                              	personInfoList={personInfoList}
                                                                                              	selectedPersonInfo={selectedPersonInfo}
                                                                                              	setSelectedPersonInfo={setSelectedPersonInfo}
                                                                                              	getPersonInfoList={getPersonInfoList}
                                                                                              />
                                                                                              </div> */}
		</div>
	);
};
export default memo(LogisticsModule);
