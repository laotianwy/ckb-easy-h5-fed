/*
 * @Author: yusha
 * @Date: 2023-10-18 17:30:17
 * @LastEditors: yusha
 * @LastEditTime: 2024-03-27 15:03:38
 * @Description: 滚动弹层
 */
import { Button } from 'antd-mobile';
import { ReactNode, memo, useRef, useState } from 'react';
import CustomPopup from '@/component/CustomPopup';
interface ScrollState {
	isNeedTopBorder?: boolean;
	isNeedBottomBorder?: boolean;
}
interface ScrollPopupProps {
	visible: boolean;
	/** 关闭方法 */
	onClose: () => void;
	/** 标题 */
	title: string;
	/** 类名 */
	className: string;
	/** 确认方法 */
	onOk?: () => void;
	children: ReactNode;
	okText?: string;
	/** 是否支持蒙层关闭 */
	isNeedMaskClose?: boolean;
}
const ScrollPopup = (props: ScrollPopupProps) => {
	const {
		visible,
		onClose,
		title,
		className,
		onOk,
		okText,
		isNeedMaskClose
	} = props;
	const containerRef = useRef(null);
	const [scrollState, setScrollState] = useState<ScrollState>();
	return (
		<CustomPopup
			visible={visible}
			onClose={onClose}
			className={className}
			isNeedMaskClose={isNeedMaskClose}
		>
			<div
				className="adress-popup"
				onClick={(e) => {
					e.preventDefault();
					e.stopPropagation();
				}}
			>
				<div
					className={`title ${scrollState?.isNeedTopBorder ? 'title-border' : ''}`}
				>
					{title}
				</div>
				<div
					ref={containerRef}
					className="adress-popup-content"
					onScroll={(e) => {
						const container: any = containerRef.current;
						const scrollTop = container.scrollTop;
						// 顶部
						const isNeedTopBorder = scrollTop !== 0;
						// 底部
						const isNeedBottomBorder =
							container.scrollTop + container.clientHeight !==
							container.scrollHeight;
						setScrollState({
							isNeedTopBorder,
							isNeedBottomBorder
						});
					}}
				>
					{props.children}
				</div>

				<div
					className={`address-btn ${scrollState?.isNeedBottomBorder ? 'btn-border' : ''}`}
				>
					<Button
						color="primary"
						shape="rounded"
						onClick={() => {
							onOk?.();
							onClose();
						}}
					>
						{okText || window._$m.t('确定')}
					</Button>
				</div>
			</div>
		</CustomPopup>
	);
};
export default memo(ScrollPopup);
