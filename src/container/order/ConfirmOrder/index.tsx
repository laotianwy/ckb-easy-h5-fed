/* eslint-disable max-lines */
/*
 * @Author: yusha
 * @Date: 2023-10-17 15:55:59
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-17 16:04:15
 * @Description: 下单确认页
 */
import { useEffect, useState } from 'react';
import qs from 'query-string';
import { useMount, useUpdateEffect } from 'ahooks';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import dayjs from 'dayjs';
import { Checkbox, Toast } from 'antd-mobile';
import { useAtomValue } from 'jotai';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import {
	CouponInfoDTO,
	OrderCalculatePriceResp,
	OrderProductDTO,
	UserPlaceOrderAddressDTO,
	UserReceivingAddressDTO
} from '@/service/easyOrder';
import CustomModal from '@/component/CustomModal';
import PayPalPay from '@/common/PayPalPay';
import {
	LOCAL_SESSION_GOODS_DETAIL_BUY_NOW,
	ON_HAND_GOODS_SELECT,
	ON_HAND_GOODS_SELECT_SESSION_KEY
} from '@/const/config';
import ScrollPage from '@/common/ScrollPage';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import { User } from '@/Atoms';
import { ENUM_PAY_WAY } from './config/index';
import ConfirmOrderFooter from './modules/ConfirmOrderFooter';
import LogisticsModule from './modules/LogisticsModule';
import ProductDetailsModule from './modules/ProductDetailsModule';
import TransactionModeModule from './modules/TransactionModeModule';
import CouponInfo from './modules/CouponInfo';
import IncidentalGoodsModule from './modules/IncidentalGoodsModule';
import './index.scss';

const ConfirmOrder = () => {
	const navigate = useNavigate();
	const userDetail = useAtomValue(User.userDetail);
	const [searchParams] = useSearchParams();
	let fromPage = searchParams.get('fromPage');

	/** 用于从商详点击立即订购获取相关信息 */
	const localOrder = searchParams.get(LOCAL_SESSION_GOODS_DETAIL_BUY_NOW);
	const { cartIds, orderNo } = qs.parse(useLocation().search);
	/** 下单信息 */
	const [orderInfo, setOrderInfo] = useState<OrderCalculatePriceResp>();
	/** 地址列表 */
	const [addressList, setAddressList] = useState<UserReceivingAddressDTO[]>(
		[]
	);
	/** 下单人信息列表 */
	const [personInfoList, setPersonInfoList] = useState<
		UserPlaceOrderAddressDTO[]
	>([]);
	/** 选择的地址 */
	const [selectedAddress, setSelectedAddress] =
		useState<UserReceivingAddressDTO>({});
	/** 选择的下单人信息 */
	const [selectedPersonInfo, setSelectedPersonInfo] =
		useState<UserPlaceOrderAddressDTO>({});
	/** 选择的交易方式 */
	const [selectPayWay, setSelectPayWay] = useState<ENUM_PAY_WAY>();
	/** 钱包余额 */
	const [availableBalance, setAvailableBalance] = useState(0);
	const [couponChecked, setCouponChecked] = useState(true);
	const [autoTag, setAutoTag] = useState(true);
	const [couponInfo, setCouponInfo] = useState<CouponInfoDTO | null>(null);
	/** 获取地址列表 */
	const getAddressList = async () => {
		const resData = await request.easyOrder.userReceivingAddress.list();
		setAddressList(resData.data ?? []);
		return resData.data ?? [];
	};
	/** 获取下单人信息 */
	const getPersonInfoList = async () => {
		const data = await request.easyOrder.userPlaceOrderAddress.list();
		setPersonInfoList(data.data ?? []);
	};
	/** 判断是否有值 */
	const isNonEmptyObject = (value) => {
		return (
			typeof value === 'object' &&
			value !== null &&
			Object.keys(value).length > 0
		);
	};
	const couponClick = (boolean) => {
		setCouponChecked(boolean);
		setAutoTag(false);
	};
	/** 获取合并后的数据 */
	const getMergeProductList = (list, type) => {
		const resultMap = {};
		for (const item of list) {
			const quantity = item[type] || 0;
			const productSku = item.productSku;

			if (productSku) {
				if (resultMap[productSku]) {
					resultMap[productSku][type] += quantity;
				} else {
					resultMap[productSku] = {
						...item,
						[type]: quantity,
						productSku
					};
				}
			} else {
				resultMap[productSku] = item;
			}
		}
		return Object.values(resultMap);
	};
	/** 获取订单信息 */
	const getConfirmOrderInfo = async () => {
		const accountQuery = await request.pay.front.accountQuery();
		/** 钱包余额 */
		const _availableBalance = accountQuery?.data?.availableBalance ?? 0;
		setAvailableBalance(_availableBalance);
		// 重新支付，直接调用订单详情接口
		if (orderNo) {
			const data =
				await request.easyOrder.orderSearch.queryDetailByOrderNo({
					orderNo: orderNo as string
				});
			const {
				receivingAddress = {},
				placeOrderAddress = {},
				orderItemList = [],
				...reset
			} = data.data || {};
			setSelectedAddress(receivingAddress);
			setSelectedPersonInfo(placeOrderAddress);
			const _info = {
				...reset,
				productList: orderItemList
			};
			setOrderInfo(_info);
			return;
		}
		const _cartIds = cartIds ? JSON.parse(cartIds as string) : [];
		const _cartIdList = _cartIds.map((id) => ({
			cartId: id
		}));
		const params = {
			customerId: userDetail.customerId,
			// cartIds: [],
			// 不能传空对象，要传undefined
			orderReceivingAddress: isNonEmptyObject(selectedAddress)
				? selectedAddress
				: undefined,
			autoCoupon: autoTag,
			couponCustomerId: couponChecked
				? couponInfo?.couponCustomerId
				: null,
			productList: _cartIdList
		};
		const searchObj = qs.parse(window.location.search) || {};
		// 因为再次请求时这个字段取不到最新的
		fromPage = searchObj.fromPage as string;
		/** 用于存储的顺手捎商品 */
		const onHandOrder = searchObj[ON_HAND_GOODS_SELECT] as string;
		// 如果是从商详点击立即订购 或者在下单页选择顺手捎商品
		if (fromPage && localOrder) {
			const skuInfo = window.sessionStorage.getItem(localOrder);
			// 如果存在cartid且
			params.productList = JSON.parse(skuInfo!);
		}
		// 如果是顺手捎商品在下单页加购，或者从顺手捎列表加购跳转过来
		// 需要和之前的数据进行一个合并
		if (fromPage && onHandOrder) {
			const newList = (params.productList ?? []).concat([]);
			const skuInfo = window.sessionStorage.getItem(onHandOrder);
			params.productList = newList.concat(JSON.parse(skuInfo) ?? []);
		}
		// 如果商品存在，需要将商详下单的顺手捎商品和在下单页加购的顺手捎商品进行汇总
		if (params.productList && params.productList.length) {
			// 过滤出商详下单的商品或者加购的顺手捎商品
			const selectedList =
				params.productList?.filter(
					(item) => item.productCode || item.productSku
				) || [];
			// 过滤出cartid数组
			const selectedListCartIds =
				params.productList?.filter(
					(item) => !(item.productCode || item.productSku)
				) || [];
			// 如果从商详下单，需要将商品和选购的顺手捎商品和汇总
			const mergeArr = getMergeProductList(selectedList, 'quantity');
			params.productList = selectedListCartIds.concat(mergeArr);
		}
		const resData = await request.easyOrder.order.calculatePrice(params);
		if (resData.data?.couponInfo && autoTag) {
			setCouponInfo(resData.data.couponInfo);
		}
		setOrderInfo(resData.data ?? {});
	};
	/** 初始化页面 */
	const initPage = async () => {
		if (orderNo) {
			getConfirmOrderInfo();
			return;
		}
		const newAddressList = await getAddressList();
		getPersonInfoList();
		// 当地址列表没有的时候需要请求下下单信息，否则走下面useUpdateEffect
		if (!newAddressList?.length) {
			getConfirmOrderInfo();
		}
	};
	// 初始化页面
	useMount(() => {
		initPage();
	});
	useUpdateEffect(() => {
		// 只有当selectedAddress变化时在请求
		if (selectedAddress.id !== undefined && selectedAddress.id !== null) {
			getConfirmOrderInfo();
		}
	}, [selectedAddress]);
	useUpdateEffect(() => {
		getConfirmOrderInfo();
	}, [couponChecked]);
	useEffect(() => {
		const _selectedPersonInfoId = window.sessionStorage.getItem(
			'selectedPersonInfoId'
		);
		// 如果存储的下单人信息id存在，且下单人信息列表大于0
		if (_selectedPersonInfoId && personInfoList?.length) {
			const data = personInfoList.find(
				(item) => String(item.id) === _selectedPersonInfoId
			);
			// 匹配到了则将选中的信息填进去
			if (data) {
				setSelectedPersonInfo(data);
				return;
			}
		}
		// 从新增或者编辑下单人信息页面跳转到下单页，下单人信息列表还未请求到的场景，需要存下selectedPersonInfoId
		if (_selectedPersonInfoId && !personInfoList?.length) {
			window.sessionStorage.setItem(
				'selectedPersonInfoId',
				_selectedPersonInfoId
			);
			return;
		}
		// 否则默认走第一条数据
		const _id = personInfoList?.[0]?.id
			? String(personInfoList?.[0]?.id)
			: '';
		window.sessionStorage.setItem('selectedPersonInfoId', _id);
		setSelectedPersonInfo(personInfoList?.[0] ?? {});
	}, [personInfoList]);
	useUpdateEffect(() => {
		const _selectedAddressId =
			window.sessionStorage.getItem('selectedAddressId');
		// 如果存储的地址信息id存在，且地址列表大于0
		if (_selectedAddressId && addressList?.length) {
			const data = addressList.find(
				(item) => String(item.id) === _selectedAddressId
			);
			if (data) {
				setSelectedAddress(data);
				return;
			}
		}
		// 从新增或者编辑地址页面跳转到下单页，地址列表还未请求到的场景，需要存下selectedPersonInfoId
		if (_selectedAddressId && !addressList?.length) {
			window.sessionStorage.setItem(
				'selectedAddressId',
				_selectedAddressId
			);
			return;
		}
		// 否则存第一条数据
		const _id = addressList?.[0]?.id ? String(addressList?.[0]?.id) : '';
		window.sessionStorage.setItem('selectedAddressId', _id);
		setSelectedAddress(addressList?.[0] ?? {});
		if (!addressList.length) {
			getConfirmOrderInfo();
		}
	}, [addressList]);
	/** 调起支付 */
	const goToPay = async ({ orderNo }) => {
		if (selectPayWay === ENUM_PAY_WAY.PAYPAL) {
			payByPaypal(orderNo);
		}
		if (selectPayWay === ENUM_PAY_WAY.PAYPAL_CREDIT) {
			payByPaypalCard(orderNo);
		}
		if (selectPayWay === ENUM_PAY_WAY.WALLET) {
			payByVallet(orderNo);
		}
	};
	/** 信用卡支付 */
	const payByPaypalCard = (orderNo) => {
		navigate('/settlement/payPalCardPay', {
			replace: true,
			state: {
				orderNo,
				pay_amount: orderInfo?.payableAmount
			}
		});
	};
	const payByPaypal = (orderNo) => {
		// 为了解决 类型“Promise<boolean>”上不存在属性“close”问题而加any
		const confirm: any = CustomModal.confirm({
			title: window._$m.t('paypal 支付'),
			content: (
				<div
					style={{
						padding: '0.12rem 0'
					}}
				>
					<PayPalPay
						style={{
							height: '.4rem'
						}}
						orderNo={orderNo}
						onCaptrueEnd={(err) => {
							confirm?.close?.();
							const res =
								localStorage.getItem(`order_${orderNo}`) || '';
							if (err) {
								navigate('/settlement/payFail', {
									replace: true
								});
								taTrack({
									event: TaEvent.ORDER_PAY,
									value: {
										pay_amount: orderInfo?.payableAmount,
										currency: 'JPY',
										order_no: orderNo,
										pay_result: 'fail',
										fail_reason: err,
										order_product_detail: JSON.parse(res)
									}
								});
								return;
							}
							taTrack({
								event: TaEvent.ORDER_PAY,
								value: {
									pay_amount: orderInfo?.payableAmount,
									currency: 'JPY',
									order_no: orderNo,
									pay_result: 'success',
									order_product_detail: JSON.parse(res)
								}
							});
							navigate(
								`/settlement/paySuccess?orderNo=${orderNo}`,
								{
									replace: true
								}
							);
						}}
					/>
				</div>
			),

			cancelText: window._$m.t('关闭'),
			showCloseButton: false,
			onCancel: () => {
				// 当前页面跳转并且刷新页面
				navigate(`/order/create?orderNo=${orderNo}`, {
					replace: true
				});
				window.location.reload();
			}
		});
	};
	/** 钱包支付 */
	const payByVallet = async (orderNo) => {
		const payParams = {
			bizNo: orderNo,
			// 业务类型 EASY_ORDER:直采商城
			bizType: 'EASY_ORDER',
			payTypeKey: selectPayWay,
			requestTime: dayjs().format('YYYY-MM-DD HH:mm:ss')
		};
		const res = localStorage.getItem(`order_${orderNo}`) || '';
		try {
			const data = await request.pay.front.postFront(payParams);
			// 该笔订单已支付，请勿重复支付
			if (data.code === '3006') {
				CustomModal.confirm({
					content: data.msg,
					showCloseButton: false,
					onClose: () => {
						navigate('/order/detail?orderNo=' + orderNo, {
							replace: true
						});
					},
					cancelText: window._$m.t('查看订单详情')
				});
				return;
			}
			if (!data.success) {
				navigate('/settlement/payFail', {
					replace: true
				});
				taTrack({
					event: TaEvent.ORDER_PAY,
					value: {
						pay_amount: orderInfo?.payableAmount,
						currency: 'JPY',
						order_no: orderNo,
						pay_result: 'fail',
						order_product_detail: JSON.parse(res)
					}
				});
				return;
			}
			taTrack({
				event: TaEvent.ORDER_PAY,
				value: {
					pay_amount: orderInfo?.payableAmount,
					currency: 'JPY',
					order_no: orderNo,
					pay_result: 'success',
					order_product_detail: JSON.parse(res)
				}
			});
			navigate(`/settlement/paySuccess?orderNo=${orderNo}`, {
				replace: true
			});
		} catch {
			taTrack({
				event: TaEvent.ORDER_PAY,
				value: {
					pay_amount: orderInfo?.payableAmount,
					currency: 'JPY',
					order_no: orderNo,
					pay_result: 'fail',
					order_product_detail: JSON.parse(res)
				}
			});
			navigate('/settlement/payFail', {
				replace: true
			});
		}
	};
	/** 获取顺手捎商品 */
	const getHandyProductList = (handyProductList: OrderProductDTO[]) => {
		const searchObj = qs.parse(window.location.search) || {};
		// 因为再次请求时这个字段取不到最新的
		fromPage = searchObj.fromPage as string;
		/** 用于存储的顺手捎商品 */
		const onHandOrder = searchObj[ON_HAND_GOODS_SELECT] as string;
		const skuInfo = window.sessionStorage.getItem(onHandOrder);
		const _skuInfo = JSON.parse(skuInfo) ?? [];
		// 如果在下单页没有加购过顺手捎商品，则直接返回不需要做处理
		if (!_skuInfo.length) {
			return handyProductList;
		}
		// 需要过滤出从购物车加的顺手捎商品，和在下单页加购的顺手捎商品
		let newDataFormCart: OrderProductDTO[] = [];
		let newDataFormOrder: OrderProductDTO[] = [];
		// 循环获取对应数据
		handyProductList.forEach((item) => {
			// 过滤出下单页加的顺手捎商品
			const info = _skuInfo.find(
				(sonItem) => sonItem.productSku === item.productSku
			);
			if (!info) {
				newDataFormCart.push(item);
				return;
			}
			const num = item.productQuantity;
			const newItemFormCart = {
				...item,
				productQuantity: num - (Number(info?.quantity ?? 0) ?? 0)
			};
			// 顺手捎商品将cartIds为null
			const newItemFormOrder = {
				...item,
				cartId: null,
				productQuantity: Number(info?.quantity ?? 0) ?? 0
			};
			newDataFormCart.push(newItemFormCart);
			newDataFormOrder.push(newItemFormOrder);
		});
		// 需要过滤数量不为0的商品
		newDataFormCart = newDataFormCart.filter(
			(item) => !!item.productQuantity
		);
		newDataFormOrder = newDataFormOrder.filter(
			(item) => !!item.productQuantity
		);
		return newDataFormCart.concat(newDataFormOrder);
	};

	/** 下单支付 */
	const createOrder = async () => {
		// 然后订单编号存在，则已经创建的订单，直接支付就行
		if (orderNo) {
			goToPay({
				orderNo
			});
			return;
		}
		const _cartIds = cartIds ? JSON.parse(cartIds as string) : [];
		// 获取所有已选择的顺手捎商品
		const selectedHandyList = getHandyProductList(
			orderInfo?.handyProductList
		);
		// 过滤出从购物车加购的顺手捎商品
		const selectedHandyListByCart =
			selectedHandyList.filter((item) => item.cartId) ?? [];
		// 在下单页加购的顺手捎商品
		const selectedHandyListByOrder =
			selectedHandyList.filter((item) => !item.cartId) ?? [];
		// 相同sku的需要数量汇总（为了解决商详页下单，在下单页加购相同sku的顺手捎商品，数量需要汇总）
		const lastData =
			getMergeProductList(selectedHandyListByOrder, 'productQuantity') ??
			[];
		const _selectedHandyList = selectedHandyListByCart.concat(lastData);
		const newData = (orderInfo?.productList ?? []).concat(
			_selectedHandyList ?? []
		);
		// 重新整合下商品数据
		const productList = newData?.map((item) => {
			const _item = {
				productCode: item.productCode,
				productSku: item.productSku,
				quantity: item.productQuantity,
				cartId: item.cartId ?? undefined,
				activityCode: item.productDiscount?.activityCode,
				freeShipping: item.freeShipping
			};
			return _item;
		});
		const params = {
			cartIds: _cartIds,
			orderReceivingAddress: selectedAddress,
			placeOrderAddress: selectedPersonInfo,
			productList,
			// 写死 渠道来源 1 pc客户端 2 h5
			sourceChannel: 2,
			couponCustomerId: couponChecked
				? couponInfo?.couponCustomerId
				: null
		};
		const data = await request.easyOrder.order.create(params);

		// 订单活动出现出现问题
		if (
			data.code &&
			[
				'31011004',
				'31011001',
				'31011002',
				'31011003',
				'31030000',
				'31030002'
			].includes(data.code)
		) {
			return CustomModal.confirm({
				content: data.msg,
				showCloseButton: false,
				onCancel: () => {
					window.location.reload();
				}
			});
		}
		// 当前没有获取到运费，无法下单，请刷新后重试
		if (data.code === '33000018') {
			CustomModal.confirm({
				content: data.msg,
				showCloseButton: false
			});
			return;
		}
		// 商品不存在，请刷新页面后重试
		if (data.code === '33000017') {
			CustomModal.confirm({
				content: data.msg,
				showCloseButton: false
			});
			return;
		}
		// 关闭后请求列表
		if (data.code === '33000010' || data.code === '33000009') {
			CustomModal.confirm({
				content: data.msg,
				showCloseButton: false,
				onCancel: () => {
					navigate(-1);
				},
				cancelText: window._$m.t('返回上页')
			});
			return;
		}
		if (!data.success && data.msg) {
			Toast.show(data.msg);
			return;
		}
		const orderUp = newData?.map((i) => {
			return {
				product_code: i.productCode,
				product_title_ja: i.productName,
				product_num: i.productQuantity
			};
		});
		// 下单嘞需要情况存储的顺手捎商品
		window.sessionStorage.removeItem(ON_HAND_GOODS_SELECT_SESSION_KEY);

		localStorage.setItem(
			`order_${data.data?.orderNo}`,
			JSON.stringify(orderUp)
		);
		taTrack({
			event: TaEvent.ORDER_PAY_CLICK,
			value: {
				source: 'shopping_cart',
				order_no: data.data?.orderNo,
				order_product_detail: orderInfo?.productList?.map((i) => {
					return {
						product_code: i.productCode,
						product_title_ja: i.productName
					};
				})
			}
		});
		// 如果创建订单接口返回的应付总金额和订单显示的应付金额不一样
		if (data.data?.payableAmount !== orderInfo?.payableAmount) {
			CustomModal.confirm({
				content: (
					<div className="modal-content">
						<div className="title">{window._$m.t('价格变动')}</div>
						<div className="content-content">
							{window._$m.t(
								'当前订单价格由{{x}}日元变更为{{y}}日元，请确认是否要继续支付',
								{
									data: {
										x: orderInfo?.payableAmount,
										y: data.data?.payableAmount
									}
								}
							)}
						</div>
					</div>
				),

				cancelText: window._$m.t('暂不支付'),
				confirmText: window._$m.t('继续支付'),
				className: 'custom-confirm-order-modal',
				onCancel: () => {
					navigate('/order/detail?orderNo=' + data.data?.orderNo, {
						replace: true
					});
				},
				onConfirm: async () => {
					await goToPay({
						orderNo: data.data?.orderNo
					});
				}
			});
			return;
		}
		await goToPay({
			orderNo: data.data?.orderNo
		});
	};
	return (
		<ScrollPage
			title={<CustomNavbar title={window._$m.t('确认订单')} />}
			className="confirm-order-page"
		>
			<div
				className={`layout-content confirm-order-layout-content ${orderInfo?.achieveFreeShippingAmount ? 'extra-margin-bootom' : ''}`}
			>
				<div className="confirm-order">
					<div className="confirm-order-content">
						<LogisticsModule
							addressList={addressList}
							personInfoList={personInfoList}
							selectedAddress={selectedAddress}
							setSelectedAddress={setSelectedAddress}
							selectedPersonInfo={selectedPersonInfo}
							setSelectedPersonInfo={setSelectedPersonInfo}
							getAddressList={getAddressList}
							getPersonInfoList={getPersonInfoList}
						/>

						<ProductDetailsModule orderInfo={orderInfo} />

						{/* 顺手捎商品模块 */}
						{/* 重新支付不展示 */}
						{!orderNo && (
							<IncidentalGoodsModule
								getConfirmOrderInfo={getConfirmOrderInfo}
								orderInfo={orderInfo}
							/>
						)}

						<TransactionModeModule
							setSelectPayWay={setSelectPayWay}
							availableBalance={availableBalance}
							// 钱包余额不足的情况，需要禁用余额支付方法
							isNeedDisabledWallet={
								availableBalance <
								(orderInfo?.payableAmount ?? 0)
							}
						/>

						{couponInfo && (
							<CouponInfo
								orderInfo={orderInfo}
								couponChecked={couponChecked}
								setCouponChecked={couponClick}
								couponInfo={couponInfo}
							/>
						)}
					</div>
					<ConfirmOrderFooter
						createOrder={createOrder}
						orderInfo={orderInfo}
						selectedAddress={selectedAddress}
						selectedPersonInfo={selectedPersonInfo}
						selectPayWay={selectPayWay}
					/>
				</div>
			</div>
		</ScrollPage>
		// eslint-disable-next-line max-lines
	);
};
export default ConfirmOrder;
