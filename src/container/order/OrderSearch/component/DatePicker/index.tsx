/*
 * @Author: shiguang
 * @Date: 2023-10-17 14:33:40
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-01 18:45:08
 * @Description: DatePicker
 */
import { useState } from 'react';
import dayjs from 'dayjs';
import {
	DatePicker as AntDatePicker,
	DatePickerProps as AntDatePickerProps
} from 'antd-mobile';
import { useEvent } from '@/utils/hooks/useEvent';
import ExpandDefault from '@/common/icons/ExpandDefault';
const useWrapperFormItem = <
	Props extends {
		value?: any;
		onChange?: (value: Props['value']) => void;
	}
>(
	props: Props,
	options?: {
		trigger: keyof Props;
	}
) => {
	const [internalValue, setInternalValue] = useState<Props['value']>();

	// 是否存在外部onchange
	const isExistOnChange = Boolean(props.onChange);
	const value = isExistOnChange ? props.value : internalValue;
	const onChange = useEvent((value: Props['value']) => {
		const trigger = options?.trigger;
		if (
			isExistOnChange &&
			trigger &&
			typeof props[trigger] === 'function'
		) {
			const func = props[trigger] as Required<Props>['onChange'];
			return func?.(value);
		}
		return setInternalValue(value);
	});
	return [value, onChange] as const;
};
interface DatePickerProps extends AntDatePickerProps {
	placeholder?: React.ReactNode;
	onChange?: (val: any) => void;
}
const DatePicker = (props: DatePickerProps) => {
	const { style } = props;
	const [visible, setVisible] = useState(false);
	const [value, onChange] = useWrapperFormItem(props, {
		trigger: 'onConfirm'
	});
	const setFalse = setVisible.bind(null, false);
	const setTrue = setVisible.bind(null, true);
	return (
		<div
			onClick={setTrue}
			style={{
				borderRadius: '.08rem',
				backgroundColor: '#F8F8F8',
				fontSize: '.14rem',
				padding: '.14rem .12rem',
				display: 'flex',
				justifyContent: 'space-between',
				alignItems: 'center',
				...style
			}}
		>
			<AntDatePicker
				visible={visible}
				value={value}
				onConfirm={(value) => {
					onChange(value);
					props.onChange && props.onChange(value);
					setFalse();
				}}
				max={new Date('2050-12-31 23:59:59')}
				min={new Date('2011-01-01 00:00:00')}
				onCancel={setFalse}
				{...props}
			>
				{(value) => (
					<span
						style={{
							color: value ? '#333333' : '#999999'
						}}
					>
						{value
							? dayjs(value).format('YYYY/MM/DD')
							: props.placeholder ?? window._$m.t('请选择')}
					</span>
				)}
			</AntDatePicker>
			<ExpandDefault />
		</div>
	);
};
export default DatePicker;
