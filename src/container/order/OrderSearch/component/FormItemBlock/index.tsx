/*
 * @Author: shiguang
 * @Date: 2023-10-16 19:57:43
 * @LastEditors: shiguang
 * @LastEditTime: 2023-10-16 20:42:24
 * @Description: 表单块
 */
import { CSSProperties } from 'react';
import './index.scss';
export interface FormItemBlockProps {
	title?: React.ReactNode;
	children?: React.ReactNode;
	style?: CSSProperties;
}
const FormItemBlock = (props: FormItemBlockProps) => {
	return (
		<div
			className="comp-form-item-block"
			style={{
				backgroundColor: 'white',
				fontSize: '.14rem',
				color: '#333333',
				borderRadius: '.08rem',
				marginBottom: '.12rem'
			}}
		>
			<div
				style={{
					marginBottom: '.04rem'
				}}
			>
				{props.title}
			</div>
			<div className="comp-form-item-children-box">{props.children}</div>
		</div>
	);
};
export default FormItemBlock;
