/*
 * @Author: shiguang
 * @Date: 2023-10-16 19:51:36
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-21 13:14:44
 * @Description:
 */
import { Button, Form, Picker, TextArea, Toast } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { formatDateJP2CN } from '@/utils';
import ScrollPage from '@/common/ScrollPage';
import { EnumOrderStatus } from '@/common/Enums';
import ExpandDefault from '@/common/icons/ExpandDefault';
import FormItemBlock from './component/FormItemBlock';
import DatePicker from './component/DatePicker';
import './index.scss';
const OrderSearch = () => {
	const [form] = Form.useForm();
	const navigate = useNavigate();
	const onSubmit = () => {
		const values = form.getFieldsValue();
		[
			'orderCreateTimeStart',
			'orderCreateTimeEnd',
			'deliverTimeStart',
			'deliverTimeEnd'
		].forEach((key) => {
			if (values[key]) {
				values[key] = dayjs(values[key]).format(
					key.indexOf('End') > -1
						? 'YYYY-MM-DD 23:59:59'
						: 'YYYY-MM-DD 00:00:00'
				);
				values[key] = formatDateJP2CN(values[key]);
			}
		});
		if (values.orderCreateTimeStart && values.orderCreateTimeEnd) {
			if (
				dayjs(values.orderCreateTimeStart).isAfter(
					values.orderCreateTimeEnd
				)
			) {
				Toast.show({
					icon: 'fail',
					content: window._$m.t('下单日开始时间不能大于结束时间')
				});
				return;
			}
		}
		if (values.deliverTimeStart && values.deliverTimeEnd) {
			if (dayjs(values.deliverTimeStart).isAfter(values.deliverTimeEnd)) {
				Toast.show({
					icon: 'fail',
					content: window._$m.t('发货日开始时间不能大于结束时间')
				});
				return;
			}
		}
		values.orderItemStatus = values.orderItemStatus
			? values.orderItemStatus[0]
			: '';
		navigate(
			'/order/list?search=' + encodeURIComponent(JSON.stringify(values)),
			{
				replace: true
			}
		);
	};
	return (
		<ScrollPage
			title={<CustomNavbar title={window._$m.t('订单搜索')} />}
			className="page-box page-order-search"
		>
			<Form
				form={form}
				// layout='horizontal'
				style={
					{
						padding: '0.67rem 0.12rem 0.12rem 0.12rem',
						'--adm-font-size-9': '.14rem',
						'--placeholder-color': '#999999'
					} as any
				}
			>
				<FormItemBlock title={window._$m.t('一键搜索')}>
					<div
						style={{
							padding: '.1rem .12rem',
							backgroundColor: '#F8F8F8',
							borderRadius: '.08rem'
						}}
					>
						<Form.Item
							name="searchKey"
							label={window._$m.t('姓名')}
							noStyle
						>
							<TextArea
								placeholder={window._$m.t(
									'支持输入订单编号、订单标题、商品SKU、商品编号'
								)}
								maxLength={256}
								style={
									{
										'--placeholder-color': '#999999'
									} as any
								}
							/>
						</Form.Item>
					</div>
				</FormItemBlock>
				<FormItemBlock title={window._$m.t('下单日')}>
					<Form.Item name="orderCreateTimeStart" noStyle>
						<DatePicker
							placeholder={window._$m.t('请选择开始日期')}
							style={{
								marginBottom: '.1rem'
							}}
						/>
					</Form.Item>
					<Form.Item name="orderCreateTimeEnd" noStyle>
						<DatePicker
							placeholder={window._$m.t('请选择结束日期')}
						/>
					</Form.Item>
				</FormItemBlock>
				<FormItemBlock title={window._$m.t('发货日')}>
					<Form.Item name="deliverTimeStart" noStyle>
						<DatePicker
							placeholder={window._$m.t('请选择开始日期')}
							style={{
								marginBottom: '.1rem'
							}}
						/>
					</Form.Item>
					<Form.Item name="deliverTimeEnd" noStyle>
						<DatePicker
							placeholder={window._$m.t('请选择结束日期')}
						/>
					</Form.Item>
				</FormItemBlock>
				<FormItemBlock title={window._$m.t('商品状态')}>
					<Form.Item
						name="orderItemStatus"
						noStyle
						trigger="onConfirm"
					>
						<Picker
							columns={[
								[
									{
										label: window._$m.t('全部'),
										value: ''
									},
									{
										label: EnumOrderStatus.waitPay.cn,
										value: EnumOrderStatus.waitPay.code
									},
									{
										label: EnumOrderStatus.waitShip.cn,
										value: EnumOrderStatus.waitShip.code
									},
									{
										label: EnumOrderStatus.shipped.cn,
										value: EnumOrderStatus.shipped.code
									},
									{
										label: EnumOrderStatus.cancelled.cn,
										value: EnumOrderStatus.cancelled.code
									},
									{
										label: EnumOrderStatus.closed.cn,
										value: EnumOrderStatus.closed.code
									}
								]
							]}
						>
							{(_, actions) => {
								const label = _?.[0]?.label;
								return (
									<div
										onClick={actions.open}
										style={{
											borderRadius: '.08rem',
											backgroundColor: '#f8f8f8',
											padding: '.14rem .12rem',
											color: label ? undefined : '#999999'
										}}
									>
										<div
											style={{
												display: 'flex',
												justifyContent: 'space-between'
											}}
										>
											{label ?? window._$m.t('请选择')}
											<ExpandDefault />
										</div>
									</div>
								);
							}}
						</Picker>
					</Form.Item>
				</FormItemBlock>
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
			</Form>
			<div
				style={{
					height: '.64rem',
					display: 'flex',
					alignItems: 'center',
					padding: '0 .12rem',
					position: 'fixed',
					backgroundColor: '#fff',
					left: 0,
					right: 0,
					bottom: 0
				}}
			>
				<Button
					shape="rounded"
					block
					onClick={() => form.resetFields()}
					style={{
						padding: '0.12rem 0.16rem'
					}}
				>
					{window._$m.t('重置')}
				</Button>
				<div
					style={{
						width: '.1rem'
					}}
				>
					{' '}
				</div>
				<Button
					block
					color="primary"
					shape="rounded"
					onClick={onSubmit}
				>
					{window._$m.t('搜索')}
				</Button>
			</div>
		</ScrollPage>
	);
};
export default OrderSearch;
