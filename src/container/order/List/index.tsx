/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-03 13:26:52
 * @Description: 订单列表 缺少搜索按钮，倒计时
 */
import qs from 'query-string';
import { useEffect, useState, useRef } from 'react';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { useAsyncEffect, useMount } from 'ahooks';
import classNames from 'classnames';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import { OrderDTO, OrderSearchReq } from '@/service/easyOrder';
import { useTurnPage } from '@/utils/hooks/useTurnPage';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import styles from './index.module.scss';
import { MoTabs } from './MoTabs';
import { MoFilterShoer } from './MoFilterShower';
import { MoList } from './MoList';
const List = () => {
	const searchObj = qs.parse(window.location.search);
	const containerRef = useRef(null);
	const [params] = useSearchParams();
	const [search] = useState(() => {
		return JSON.parse(decodeURIComponent(params.get('search') || '{}'));
	});
	const navigator = useNavigate();
	const [scrollOpcity, setScrollOpcity] = useState(0);
	const [activeKey, setActiveKey] = useState<string | undefined>(
		(searchObj?.activeKey as string) ?? ''
	);
	useMount(() => {
		taTrack({
			event: TaEvent.PAGE_VIEW,
			value: {
				position: 'my_order_page'
			}
		});
	});
	const [orderListInfo, RenderTurnpage, { setParams }] = useTurnPage<
		OrderDTO,
		OrderSearchReq
	>({
		params: {
			orderStatusList:
				activeKey !== undefined && activeKey !== ''
					? [Number(activeKey)]
					: undefined
		},
		emptyTips:
			activeKey === undefined || activeKey === ''
				? window._$m.t('全て暂无数据')
				: window._$m.t('其他暂无数据'),
		request: (params) => {
			return request.easyOrder.orderSearch.list(params);
		}
	});
	useEffect(() => {
		if (search.orderItemStatus === '') {
			search.orderItemStatus = undefined;
		}
		setParams({
			...search,
			orderStatusList:
				activeKey !== undefined && activeKey !== ''
					? [activeKey]
					: undefined
		});
	}, [activeKey, search, setParams]);
	const onReset = () => {
		navigator('/order/list', {
			replace: true
		});
		setParams({});
	};
	const CustomNavbarContent = (
		<span onClick={() => navigator('/order/search')}>
			<img
				style={{
					width: '0.2rem',
					height: '0.2rem'
				}}
				alt=""
				src="https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_search@2x.png"
			/>
		</span>
	);

	return (
		<div
			className="layout-style"
			ref={containerRef}
			onScroll={(e) => {
				const container: any = containerRef.current;
				const scrollTop = container.scrollTop;
				// 顶部
				const opcity = scrollTop / 100 > 1 ? 1 : scrollTop / 100;
				setScrollOpcity(opcity);
			}}
		>
			<div className={styles.page}>
				<div
					style={{
						position: 'fixed',
						left: 0,
						right: 0,
						backgroundColor: `rgba(240, 242, 245,${scrollOpcity})`,
						top: 0,
						zIndex: 1,
						paddingTop: '0.12rem'
					}}
				>
					<CustomNavbar
						title={window._$m.t('我的订单')}
						navBarRight={CustomNavbarContent}
					/>

					<MoTabs
						activeKey={activeKey ?? ''}
						changeActiveKey={setActiveKey}
					/>
				</div>
				<div className={classNames(styles.list, 'layout-content')}>
					<MoFilterShoer onReset={onReset} />
					{!!orderListInfo && <MoList orders={orderListInfo} />}
				</div>
			</div>
			{RenderTurnpage}
		</div>
	);
};
export default List;
