/*
 * @Author: huajian
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-01 14:46:29
 * @Description: 订单列表 模块
 */
import { Tabs } from 'antd-mobile';
import React, { useState } from 'react';
import { OrderDTO } from '@/service/easyOrder';
import { OrderItem } from './OrderItem';
interface Props {
	orders: OrderDTO[];
}
export const MoList: React.FC<Props> = ({ orders }) => {
	return orders.map((order, index) => {
		return <OrderItem key={index} info={order} />;
	});
};
