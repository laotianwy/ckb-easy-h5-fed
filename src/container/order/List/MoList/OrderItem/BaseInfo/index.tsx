/*
 * @Author: shiguang
 * @Date: 2023-10-18 11:52:18
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-09 14:11:43
 * @Description: OrderBaseInfo
 */
import copy from 'copy-to-clipboard';
import { Toast } from 'antd-mobile';
import { OrderDTO } from '@/service/easyOrder';
import { formatDateCN2JP } from '@/utils';
import styles from './index.module.scss';
interface Props {
	info: OrderDTO;
}
export const BaseInfo: React.FC<Props> = ({ info }) => {
	return (
		<div className={styles.container}>
			<div className={styles.itemWrap}>
				<div>{window._$m.t('收件人')}</div>
				<div>{info.receiveName}</div>
			</div>
			<div className={styles.itemWrap}>
				<div>{window._$m.t('订单编号')}</div>
				<div
					style={{
						display: 'flex',
						alignItems: 'center'
					}}
				>
					<div
						style={{
							color: '#777'
						}}
					>
						{info.orderNo}
					</div>
					<div
						style={{
							margin: '0 .08rem',
							color: '#DCDCDC'
						}}
					>
						|
					</div>
					<div
						style={{
							color: '#FF5010'
						}}
						onClick={(e) => {
							e.stopPropagation();
							if (copy(info.orderNo || '')) {
								Toast.show(window._$m.t('复制成功'));
							}
						}}
					>
						{window._$m.t('复制')}
					</div>
				</div>
			</div>
			<div className={styles.itemWrap}>
				<div>{window._$m.t('下单时间')}</div>
				<div
					style={{
						color: '#777'
					}}
				>
					{formatDateCN2JP(info.createdTime)}
				</div>
			</div>
		</div>
	);
};
