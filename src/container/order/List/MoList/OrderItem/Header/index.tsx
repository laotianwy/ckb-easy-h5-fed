/*
 * @Author: shiguang
 * @Date: 2023-10-18 11:52:18
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 15:42:56
 * @Description: OrderItem
 */
import { useBoolean, useInterval, useToggle } from 'ahooks';
import { MouseEventHandler, useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { useEvent } from '@/utils/hooks/useEvent';
import { OrderDTO } from '@/service/easyOrder';
import { useRefresh } from '@/utils/hooks/useRefresh';
import { request } from '@/config/request';
import { EnumOrderStatus } from '@/common/Enums';
import styles from './index.module.scss';
const getCountDownDateTime = (targetTimeStampMs: number) => {
	const curTimeStamp = new Date().getTime();
	const ms = targetTimeStampMs - curTimeStamp;
	const leftTimeS = Math.floor(ms / 1000);
	if (ms <= 0) return [0, 0, 0, 0] as const;
	const d = Math.floor(leftTimeS / (24 * 60 * 60));
	const h = Math.floor((leftTimeS / (60 * 60)) % 24);
	const m = Math.floor((leftTimeS / 60) % 60);
	const s = Math.floor(leftTimeS % 60);
	return [d, h, m, s] as const;
};
const wrapperNumberAdd0 = (num: number, numberStringLen: number = 2) => {
	const stringNumber = num.toString();
	if (stringNumber.length >= numberStringLen) return stringNumber;
	return `${Array(numberStringLen - stringNumber.length)
		.fill('0')
		.join('')}${stringNumber}`;
};
const useTimeCountDown = () => {
	const [endTimeStampMs, setEndTimeStampMs] = useState<number>();
	const [countDownDateTime, setCountDownDateTime] =
		useState<ReturnType<typeof getCountDownDateTime>>();
	const getIsEnd = () =>
		countDownDateTime ? countDownDateTime?.every((n) => n === 0) : false;
	const clearInterval = useInterval(() => {
		const isEnd = getIsEnd();
		if (endTimeStampMs && !isEnd) {
			setCountDownDateTime(getCountDownDateTime(endTimeStampMs));
		}
	}, 1000);
	useEffect(() => {
		return clearInterval;
	}, [clearInterval]);

	/** 开始倒计时 */
	const startCountDown = useEvent((endTime: dayjs.ConfigType) => {
		const dateTime = dayjs(endTime);
		const ms = dateTime.valueOf();
		setEndTimeStampMs(ms);
	});
	return [
		startCountDown,
		countDownDateTime ?? [0, 0, 0, 0],
		getIsEnd()
	] as const;
};
interface Props {
	info: OrderDTO;
}
export const Header: React.FC<Props> = ({ info }) => {
	const [startCountDown, [d, h, m, s], isEnd] = useTimeCountDown();
	const refresh = useRefresh();
	const orderStatus = info.orderStatus;
	useEffect(() => {
		if (orderStatus === EnumOrderStatus.waitPay.code) {
			startCountDown(info.timeoutTime);
		}
	}, [startCountDown, orderStatus, info.timeoutTime]);
	useEffect(() => {
		if (!isEnd) {
			return;
		}
		request.easyOrder.order
			.close({
				orderNo: info.orderNo
			})
			.then(() => {
				info.orderStatus = EnumOrderStatus.closed.code;
				refresh();
			});
	}, [isEnd, refresh, info]);
	return (
		<div className={styles.stateWrap}>
			<div className={styles.stateText}>
				{/* <img
                                                                                                                      style={{
                                                                                                                      	width: '0.26rem',
                                                                                                                      	height: '0.26rem',
                                                                                                                      	marginRight: '0.08rem'
                                                                                                                      }}
                                                                                                                      src={EnumOrderStatus[info.orderStatus!]?.icon}
                                                                                                                      alt="icon"
                                                                                                                      /> */}
				{EnumOrderStatus[orderStatus!]?.cn}
			</div>
			<div
				style={{
					display:
						isEnd || orderStatus !== EnumOrderStatus.waitPay.code
							? 'none'
							: undefined
				}}
			>
				<span
					style={{
						paddingRight: '.04rem'
					}}
				>
					{window._$m.t('剩余')}
				</span>
				<span
					style={{
						color: '#FF5010'
					}}
				>
					{wrapperNumberAdd0(h)}:{wrapperNumberAdd0(m)}:
					{wrapperNumberAdd0(s)}
				</span>
			</div>
		</div>
	);
};
