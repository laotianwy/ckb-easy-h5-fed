/*
 * @Author: shiguang
 * @Date: 2023-10-18 11:52:18
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-20 16:11:31
 * @Description: OrderGoodsItem
 */
import { useToggle } from 'ahooks';
import { Button, Ellipsis, Toast } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import CustomModal from '@/component/CustomModal';
import { OrderDTO, OrderItemDTO } from '@/service/easyOrder';
import { request } from '@/config/request';
import { EnumOrderStatus } from '@/common/Enums';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import styles from './index.module.scss';
interface Props {
	info: OrderDTO;
}
export const Bottom = (props: Props) => {
	const { info } = props;
	const navigate = useNavigate();
	const onCancelPay = () => {
		taTrack({
			event: TaEvent.CANCELL_PAY,
			value: {
				order_no: info.orderNo,
				order_product_detail: info.orderItemList?.map((i) => {
					return {
						product_code: i.productCode,
						product_title_ja: i.productName
					};
				})
			}
		});
		CustomModal.confirm({
			content: window._$m.t('请确认是否取消支付'),
			cancelText: window._$m.t('暂不取消'),
			confirmText: window._$m.t('确认取消'),
			className: 'not-need-button-width-modal',
			onConfirm: async () => {
				request.easyOrder.order
					.close({
						orderNo: info.orderNo
					})
					.then(() => {
						// Toast.show({
						// 	icon: 'success',
						// 	content: window._$m.t('取消成功')
						// });
						info.orderStatus = EnumOrderStatus.closed.code;
						window.location.reload();
					});
			}
		});
	};
	return (
		<div className={styles.container}>
			<Button
				shape="rounded"
				style={{
					marginRight: '.12rem',
					height: '0.3rem',
					lineHeight: '0.3rem',
					display: 'flex',
					alignItems: 'center',
					fontSize: '0.13rem',
					fontWeight: '600'
				}}
				onClick={() => {
					navigate('/order/detail?orderNo=' + info.orderNo);
				}}
			>
				{window._$m.t('订单详情')}
			</Button>
			{info.orderStatus === EnumOrderStatus.waitPay.code && (
				<Button
					shape="rounded"
					style={{
						marginRight: '.12rem',
						height: '0.3rem',
						lineHeight: '0.3rem',
						display: 'flex',
						alignItems: 'center',
						fontSize: '0.13rem',
						fontWeight: '600'
					}}
					onClick={onCancelPay}
				>
					{window._$m.t('取消支付')}
				</Button>
			)}

			{info.orderStatus === EnumOrderStatus.waitPay.code && (
				<Button
					shape="rounded"
					style={{
						marginRight: '.12rem',
						height: '0.3rem',
						lineHeight: '0.3rem',
						display: 'flex',
						alignItems: 'center',
						fontSize: '0.13rem',
						fontWeight: '500',
						color: '#fff',
						backgroundColor: '#1C2026'
					}}
					onClick={() => {
						const orderUp = info.orderItemList?.map((i) => {
							return {
								product_code: i.productCode,
								product_title_ja: i.productName,
								product_num: i.productQuantity
							};
						});
						localStorage.setItem(
							`order_${info.orderNo}`,
							JSON.stringify(orderUp)
						);
						taTrack({
							event: TaEvent.ORDER_PAY_CLICK,
							value: {
								source: 'my_order',
								order_no: info.orderNo,
								order_product_detail: info.orderItemList?.map(
									(i) => {
										return {
											product_code: i.productCode,
											product_title_ja: i.productName
										};
									}
								)
							}
						});
						navigate(`/order/create?orderNo=${info.orderNo}`);
					}}
				>
					{window._$m.t('去支付')}
				</Button>
			)}
		</div>
	);
};
