/*
 * @Author: shiguang
 * @Date: 2023-10-18 11:52:18
 * @LastEditors: huajian 258220040@qq.com
 * @LastEditTime: 2023-11-14 11:36:57
 * @Description: OrderItem
 */
import { OrderDTO } from '@/service/easyOrder';
import { OrderGoodsItem } from '@/component/OrderGoodsItem';
import { Bottom } from './Bottom';
import { Header } from './Header';
import styles from './index.module.scss';
interface Props {
	info: OrderDTO;
}
export const OrderItem = (props: Props) => {
	return (
		<div className={styles.container}>
			<Header info={props.info} />
			{/* <BaseInfo info={props.info} /> */}
			<OrderGoodsItem info={props.info} />
			<Bottom info={props.info} />
		</div>
	);
};
