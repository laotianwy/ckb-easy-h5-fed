/*
 * @Author: huajian
 * @Date: 2023-10-31 16:49:40
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-09 15:24:46
 * @Description:
 */
import { Ellipsis } from 'antd-mobile';
import { useLocation } from 'react-router';
import { useBoolean } from 'ahooks';
import { useSearchParams } from 'react-router-dom';
import { useEffect } from 'react';
import dayjs from 'dayjs';
import { EnumOrderStatus } from '@/common/Enums';
import styles from './index.module.scss';
interface Props {
	onReset: () => void;
}
export const MoFilterShoer: React.FC<Props> = (props) => {
	const location = useLocation();
	const [params] = useSearchParams();
	const search = JSON.parse(decodeURIComponent(params.get('search') || '{}'));
	search.orderItemStatus = EnumOrderStatus[search.orderItemStatus]
		? EnumOrderStatus[search.orderItemStatus]?.cn
		: '';
	Object.keys(search).forEach((key) => {
		if (!search[key]) {
			delete search[key];
			return;
		}
		if (key.indexOf('Time') > -1) {
			// 开始时间+1小时还原显示
			search[key] = dayjs(search[key])
				.add(1, 'hour')
				.format('YYYY-MM-DD');
			// 时间去 时分秒
			search[key] = dayjs(search[key]).format('YYYY-MM-DD');
		}
	});
	const [isExistSearchParams, { setFalse }] = useBoolean(
		Boolean(Object.keys(search).length)
	);
	if (!isExistSearchParams) {
		return null;
	}
	return (
		<div className={styles.filterShower}>
			<div className={styles.filter}>
				<div
					style={{
						flex: '0 0 auto'
					}}
				>
					{window._$m.t('已筛选：')}：
				</div>
				<Ellipsis
					style={{
						color: '#FF5010',
						flex: '1 1 auto',
						wordBreak: 'break-all'
					}}
					rows={1}
					direction="end"
					content={Object.keys(search)
						.map((key) => search[key] || '')
						.join('，')}
				/>
			</div>
			<div
				onClick={() => {
					setFalse();
					props.onReset();
				}}
				className={styles.filterBtn}
			>
				{window._$m.t('重置')}
			</div>
		</div>
	);
};
