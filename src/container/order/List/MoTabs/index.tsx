/*
 * @Author: huajian
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-09 14:10:33
 * @Description: tabs
 */
import { Tabs } from 'antd-mobile';
import React from 'react';
import { EnumOrderStatus } from '@/common/Enums';
import styles from './index.module.scss';
interface Props {
	activeKey: string;
	changeActiveKey: (key: string) => void;
}
export const MoTabs: React.FC<Props> = ({ activeKey, changeActiveKey }) => {
	return (
		<div className={styles.tabs}>
			<span
				className={`${styles.tab} ${String(activeKey) === '' ? styles.selected : ''}`}
				onClick={() => changeActiveKey('')}
			>
				{window._$m.t('全部')}
			</span>
			<span
				className={`${styles.tab} ${String(activeKey) === String(EnumOrderStatus.waitPay.code) ? styles.selected : ''}`}
				onClick={() =>
					changeActiveKey(String(EnumOrderStatus.waitPay.code))
				}
			>
				{EnumOrderStatus.waitPay.cn}
			</span>
			<span
				className={`${styles.tab} ${String(activeKey) === String(EnumOrderStatus.waitShip.code) ? styles.selected : ''}`}
				onClick={() =>
					changeActiveKey(String(EnumOrderStatus.waitShip.code))
				}
			>
				{EnumOrderStatus.waitShip.cn}
			</span>
			<span
				className={`${styles.tab} ${String(activeKey) === String(EnumOrderStatus.shipped.code) ? styles.selected : ''}`}
				onClick={() =>
					changeActiveKey(String(EnumOrderStatus.shipped.code))
				}
			>
				{EnumOrderStatus.shipped.cn}
			</span>
		</div>
	);
};
