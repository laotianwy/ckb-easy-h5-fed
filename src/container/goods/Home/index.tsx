/*
 * @Author: huajian
 * @Date: 2023-12-04 13:43:40
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-28 11:50:33
 * @Description: 直采首页
 */
import { Toast } from 'antd-mobile';
import HomeMidPage from '@/outs/container/Home';
import { User } from '@/Atoms/index';
import HomeSlider from '@/component/HomeSlide';
import H5SiteBuyComponent from '@/component/H5SiteBuyComponent';
import H5HotProductComponent from '@/component/H5HotProductComponent';
import './index.scss';
const Home = () => {
	return (
		<div className="home-container-component">
			<HomeMidPage
				H5SiteBuyComponent={H5SiteBuyComponent}
				H5HotProductComponent={H5HotProductComponent}
				atomUserDetail={User.userDetail as any}
				HomeSlider={HomeSlider}
				toast={(content: string) => {
					return Toast.show({
						content,
						maskClassName: 'zIndexImportant-9999'
					});
				}}
			/>
		</div>
	);
};
export default Home;
