/*
 * @Author: huajian
 * @Date: 2023-11-16 14:51:54
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-27 10:13:12
 * @Description: styles
 */
import { StyleSheet } from 'react-native';
import { px2dp } from '@/outs/utils';
export const styles = StyleSheet.create({
	root: {
		backgroundColor: '#f0f2f5',
		paddingHorizontal: px2dp(12),
		flex: 1
	}
});
