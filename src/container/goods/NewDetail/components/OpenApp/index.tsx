/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-03-12 10:42:19
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-03-12 13:59:42
 * @FilePath: /3fbox-page-mid/subgit/ckb-easy-h5-fed/src/container/goods/NewDetail/components/OpenApp/index.tsx
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect, useRef } from 'react';
import './index.scss';

export const openUrlToApp = (productCode: string) => {
	const iosLinkUrl =
		'https://apps.apple.com/us/app/3fbox-%E7%9B%B4%E5%A3%B2%E9%80%9A%E8%B2%A9%E3%81%A772-off/id6474284467';
	const androidLinkurl =
		'https://play.google.com/store/apps/details?id=com.theckb.fbox';
	const userAgent = window.navigator.userAgent;
	/** ios终端 */
	const isIOS = !!userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
	const _url = `com.theckb.fbox://GoodDetail?productCode=${productCode}&random=${new Date().valueOf()}`;
	window.location.href = _url;
	// openAppRef.current =
	return setTimeout(() => {
		// 没找到打开应用商店
		window.location.href = isIOS ? iosLinkUrl : androidLinkurl;
	}, 2000);
};

interface OpenAppProps {
	productCode?: string;
}

const OpenApp = (props: OpenAppProps) => {
	const { productCode } = props;
	const openAppRef = useRef<any>(null);
	useEffect(() => {
		const pageHide = function () {
			clearTimeout(openAppRef.current);
		};

		document.addEventListener('visibilitychange', pageHide, false);
		document.addEventListener('webkitvisibilitychange', pageHide, false);
		window.addEventListener('pagehide', pageHide, false);

		return () => {
			document.removeEventListener('visibilitychange', pageHide, false);
			document.removeEventListener(
				'webkitvisibilitychange',
				pageHide,
				false
			);
			window.removeEventListener('pagehide', pageHide, false);
		};
	}, []);
	if (window?.ReactNativeWebView) return;
	return (
		<div
			className="open-app"
			onClick={() => {
				openAppRef.current = openUrlToApp(productCode!);
			}}
		>
			<div className="open-app-header">
				<img
					src="https://static-s.theckb.com/BusinessMarket/Easy/H5/open-app.png"
					alt="open-app"
				/>
			</div>
			<div className="open-app-text">{window._$m.t('打开App')}</div>
		</div>
	);
};

export default OpenApp;
