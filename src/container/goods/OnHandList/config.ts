/*
 * @Author: yusha
 * @Date: 2024-04-07 15:17:01
 * @LastEditors: yusha
 * @LastEditTime: 2024-04-07 16:32:05
 * @Description: 配置
 */
export enum ENUM_LEFT_SORT_LIST {
	/** 综合 */
	SYNTHETIC_SORT = 1,
	/** 价格升序 */
	ASCE_ORDER = 2,
	/** 价格降序 */
	DESC_ORDER = 3
}
/** 左侧搜索分类 */
export const LEFT_SORT_LIST = [
	{
		id: ENUM_LEFT_SORT_LIST.SYNTHETIC_SORT,
		text: window._$m.t('综合')
	},
	{
		id: ENUM_LEFT_SORT_LIST.ASCE_ORDER,
		text: window._$m.t('价格升序')
	},
	{
		id: ENUM_LEFT_SORT_LIST.DESC_ORDER,
		text: window._$m.t('价格降序')
	}
] as const;

export const SCROLL_TO_VIEW_ID = 'goods-onhand-scroll-view';
export const LISTEN_VIEW_ID = 'onhand-goods-list';
export const PAGESIZE = 20;
