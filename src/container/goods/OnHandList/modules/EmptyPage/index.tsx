/*
 * @Author: yusha
 * @Date: 2023-11-21 11:24:27
 * @LastEditors: yusha
 * @LastEditTime: 2024-04-11 15:15:00
 * @Description: 数据为空展示的页面
 */
import { memo } from 'react';
import classNames from 'classnames';
import styles from './index.module.scss';

const EmptyPage = (props) => {
	// 商品列表长度
	const { goodsListLength } = props;
	return (
		<div
			className={classNames(
				styles['shop-cart-empty'],
				styles['empty-no-purchase']
			)}
		>
			<div
				className={classNames(
					styles['empty-content'],
					goodsListLength ? styles['empty-content-height'] : ''
				)}
			>
				<img
					alt="empty"
					src="https://static-jp.theckb.com/static-asset/easy-h5/empty_status_goods.svg"
				/>

				<div className={styles['empty-desc']}>
					{window._$m.t('搜索不到任何商品，再看看其他商品吧')}
				</div>
			</div>
		</div>
	);
};
export default memo(EmptyPage);
