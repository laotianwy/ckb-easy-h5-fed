/*
 * @Author: yusha
 * @Date: 2024-04-08 09:57:20
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-16 14:20:52
 * @Description:  顺手捎活动footer
 */

import { Mask } from 'antd-mobile';
import { memo, useContext, useEffect, useMemo, useState } from 'react';
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { useLocation, useNavigate } from 'react-router-dom';
import qs from 'query-string';
import CloseOneguanbi from '@/common/icons/CloseOneguanbi';
import CollapseSmall from '@/common/icons/CollapseSmall';
import ExpandSmall from '@/common/icons/ExpandSmall';
import { formatMoney } from '@/utils';
import {
	ON_HAND_GOODS_SELECT,
	ON_HAND_GOODS_SELECT_SESSION_KEY
} from '@/const/config';
import { request } from '@/config/request';
import { useEvent } from '@/utils/hooks/useEvent';
import { OrderCalculatePriceResp } from '@/service/easyOrder';
import { commonGoBack } from '@/common/Layout/CustomNavbar';
import './index.scss';
import { RouterHistoryContext } from '@/App';

const Footer = (props: { goBack: () => void }) => {
	const { goBack } = props;
	const location = useLocation();
	const navigate = useNavigate();
	const routerHistory = useContext(RouterHistoryContext);
	const [curPage, prepage] = routerHistory;
	// 如果是路由带过来的参数
	const searchObj = qs.parse(location.search);
	const fromSniffApp = searchObj.fromSniffApp as string;
	// 是否展开明细
	const [maskVisible, setMaskVisible] = useState(false);
	/** 已选择的sku信息 */
	const [selectedSkuInfo, setSelectedSkuInfo] = useState([]);
	const [calculatePriceData, setCalculatePriceData] =
		useState<OrderCalculatePriceResp>({});
	/** 算价的接口 */
	const { runAsync: calculatePriceApi, loading: calculatePriceLoading } =
		useRequest(request.easyOrder.order.calculatePrice, {
			manual: true
		});
	// 想要在当前页面监听localStorage的数据，只能重写setItem方法
	const originalSetItem = sessionStorage.setItem;
	sessionStorage.setItem = function (key, newValue) {
		const setItemEvent = new Event('setSessionStorageItemEvent');
		setItemEvent[key] = newValue;
		window.dispatchEvent(setItemEvent);
		originalSetItem.apply(this, [key, newValue]);
	};
	/** 监听SessionStorage变化 */
	const changesetSessionStorageItemEvent = (event) => {
		// 取出顺手捎已选择的商品
		const selectedSku = event[ON_HAND_GOODS_SELECT_SESSION_KEY];
		const _selectedSku = selectedSku ? JSON.parse(selectedSku) : [];
		setSelectedSkuInfo(_selectedSku);
	};
	// 实时监听
	useEffect(() => {
		window.addEventListener(
			'setSessionStorageItemEvent',
			changesetSessionStorageItemEvent
		);
		return () => {
			window.removeEventListener(
				'setSessionStorageItemEvent',
				changesetSessionStorageItemEvent
			);
		};
	});
	// 页面初始时，需要重新获取选中的select信息
	// useMount(() => {
	// 	const selectedSku = window.sessionStorage.getItem(
	// 		ON_HAND_GOODS_SELECT_SESSION_KEY
	// 	);
	// 	const _selectedSku = selectedSku ? JSON.parse(selectedSku) : [];
	// 	setSelectedSkuInfo(_selectedSku);
	// });
	// 页面初始时，需要重新获取选中的select信息
	useEffect(() => {
		const selectedSku = window.sessionStorage.getItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY
		);
		const _selectedSku = selectedSku ? JSON.parse(selectedSku) : [];
		setSelectedSkuInfo(_selectedSku);
	}, []);
	/** 监听选中的sku，请求算价的接口 */
	useAsyncEffect(async () => {
		if (!selectedSkuInfo.length) {
			setCalculatePriceData({});
			return;
		}
		const data = await calculatePriceApi({
			productList: selectedSkuInfo
		});
		setCalculatePriceData(data.data ?? {});
	}, [selectedSkuInfo]);
	/** 获取已加购的顺手捎商品 */
	// const getHandyList = () => {
	// 	if (!calculatePriceData.handyProductList?.length) {
	// 		return [];
	// 	}
	// 	return calculatePriceData.handyProductList.filter((curItem) => {
	// 		// 当productType为1代表是加购的是顺手捎商品
	// 		return Number(curItem.productType) === 1;
	// 	});
	// };
	/** 获取最终价格 */
	const getTotalAmount = () => {
		// 需要减去国际运费
		const num =
			(calculatePriceData.payableAmount ?? 0) -
			(calculatePriceData.internationalShippingAmount ?? 0);
		return formatMoney(num);
	};
	return (
		<div className="onhand-list-footer">
			<Mask
				destroyOnClose
				visible={maskVisible}
				getContainer={() => document.getElementById('root')!}
				className="onhand-list-mask"
			>
				<div className="onhand-list-footer1">
					{maskVisible && (
						<>
							<div
								className="footer-close"
								onClick={() => setMaskVisible(!maskVisible)}
							>
								<CloseOneguanbi />
							</div>
							<div className="footer-detail">
								<div className="detail-title">
									{window._$m.t('商品明细')}
								</div>
								<div className="product-detail-content">
									{(
										calculatePriceData.handyProductList ??
										[]
									).map((item, index) => {
										return (
											<div
												className="single-product"
												key={index}
											>
												<div className="product-top">
													<img
														className="product-img"
														src={item.productImg}
														alt="img"
													/>
													<span>
														{item.productName}
													</span>
												</div>
												<div className="product-sku">
													{item.productPropertiesName}
												</div>
												<div className="product-amount">
													<span>
														{item.productPrice}
														{window._$m.t('円')}

														<span className="font-weight-400 ml-4">
															x
															{
																item.productQuantity
															}
														</span>
													</span>
													<span>
														{
															item.productTotalAmount
														}
														{window._$m.t('円')}
													</span>
												</div>
											</div>
										);
									})}
								</div>
							</div>
						</>
					)}

					<div className="mask-footer">
						<span className="total-num">
							{window._$m.t('已选{{num}}件', {
								data: {
									num: calculatePriceData.productNum ?? 0
								}
							})}
						</span>
						<div className="footer-content1">
							<div className="footer-total">
								<div className="footer-total-money">
									<div className="total-amount">
										{getTotalAmount()}
										{window._$m.t('円')}
									</div>
								</div>
								<div
									className="total-quantity"
									onClick={() => {
										setMaskVisible(!maskVisible);
									}}
								>
									<span className="mr-4">
										{window._$m.t('明细')}
									</span>
									{!maskVisible && (
										<ExpandSmall
											width="0.08rem"
											height="0.08rem"
										/>
									)}

									{maskVisible && (
										<CollapseSmall
											width="0.08rem"
											height="0.08rem"
										/>
									)}
								</div>
							</div>
							<div
								className="footer-btn"
								onClick={() => {
									goBack();
								}}
							>
								{window._$m.t('去结算')}
							</div>
						</div>
					</div>
				</div>
			</Mask>
			<div className="onhand-list-footer2" id="fixed-bottom">
				<span>
					{window._$m.t('已选{{num}}件', {
						data: { num: calculatePriceData.productNum ?? 0 }
					})}
				</span>
				<div className="footer-content2">
					<div className="footer-total">
						<div className="footer-total-money">
							<div className="total-amount">
								{getTotalAmount()}
								{window._$m.t('円')}
							</div>
						</div>
						<div
							className="total-quantity"
							onClick={() => setMaskVisible(!maskVisible)}
						>
							<span className="mr-4">{window._$m.t('明细')}</span>
							{!maskVisible ? (
								<ExpandSmall width="0.08rem" height="0.08rem" />
							) : (
								<CollapseSmall
									width="0.08rem"
									height="0.08rem"
								/>
							)}
						</div>
					</div>
					<div className="footer-btn" onClick={goBack}>
						{window._$m.t('去结算')}
					</div>
				</div>
			</div>
		</div>
	);
};
export default memo(Footer);
