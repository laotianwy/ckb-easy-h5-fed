/*
 * @Author: yusha
 * @Date: 2024-04-07 15:53:16
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-10 16:18:41
 * @Description: 列表
 */

import {
	CSSProperties,
	memo,
	useEffect,
	useMemo,
	useRef,
	useState
} from 'react';
import {
	List as VirtualizedList,
	AutoSizer,
	WindowScroller
} from 'react-virtualized';
import queryString from 'query-string';
import { Grid, List, InfiniteScroll, Popup } from 'antd-mobile';
import { useMount } from 'ahooks';
import { useSearchParams } from 'react-router-dom';
import { getMergedArray, groupArray } from '@/utils';
import SelectGoodsSkuCom from '@/component/SelectGoodsSkuCom';
import NewDetail from '@/container/goods/NewDetail';
import { ON_HAND_GOODS_SELECT_SESSION_KEY } from '@/const/config';
import Loading from '@/component/Loading';
import { ProductVO } from '@/service/market';
import styles from './index.module.scss';

interface VirtualizedListProps {
	index: number;
	key: string;
	style: CSSProperties;
}
interface ListModuleProps {
	data: ProductVO[];
	hasMore: boolean;
	loadMore: () => Promise<void>;
	total: number;
}
const ListModule = (props: ListModuleProps) => {
	/** 点击选择sku给添加个loading，防止请求过久用户体验不好 */
	const [loading, setLoading] = useState(false);
	const { data, hasMore, loadMore, total } = props;
	const [searchParams, setSearchParams] = useSearchParams();
	const scrollRef = useRef<HTMLDivElement>(null);
	/** 已选择的sku信息 */
	// const [selectedSkuInfo, setSelectedSkuInfo] = useState([]);
	const [rootFontSize, setRootFontSize] = useState<number>(0);
	/** 商详弹窗是否展示 */
	const [detailVisible, setDetailVisible] = useState(
		Boolean(searchParams.get('productCode'))
	);
	const filterList = useMemo(() => {
		return groupArray(data, 2);
	}, [data]);

	const getRootFontSize = () => {
		const rootPX = Number(
			document.documentElement.style.fontSize.split('px')[0]
		);
		setRootFontSize(rootPX);
	};
	useMount(() => {
		getRootFontSize();
	});
	/** 前往商详 */
	const goToGoodsDetail = (productCode) => {
		const currentPageParams =
			queryString.parse(window.location.search) ?? {};
		setSearchParams({
			...searchParams,
			...currentPageParams,
			productCode
		});
		setDetailVisible(true);
	};
	/** 确认选择sku */
	const handleChange = (val) => {
		window.sessionStorage.setItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY,
			JSON.stringify(val)
		);
	};
	/** 关闭商详弹窗 */
	const handleCloseDetailModal = (val) => {
		const preSelectedSkuList = window.sessionStorage.getItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY
		);
		const _preSelectedSkuList = preSelectedSkuList
			? JSON.parse(preSelectedSkuList)
			: [];
		// 获取合并的数组，若是两个数组里面存在相同的sku，则去val里面的内容，两者都不相同则取并集
		const mergedArray = getMergedArray(
			_preSelectedSkuList,
			val,
			'productSku'
		);
		// 当为确认选择，有数据才需要执行下面操作
		mergedArray && handleChange(mergedArray);
		setDetailVisible(false);
		window.history.back();
	};
	const rowRenderer = ({ index, key, style }: VirtualizedListProps) => {
		const item = filterList[index];
		if (!item) return;
		return (
			<div key={key} style={style} className={styles['shopp-view']}>
				<Grid columns={2} gap={rootFontSize * 0.08}>
					{item.map((shoppItem) => {
						// 获取sessionStorage存储的顺手捎加购的商品
						const _selectedSku = sessionStorage.getItem(
							ON_HAND_GOODS_SELECT_SESSION_KEY
						);
						// 转义一下
						const selectedSkuInfo = _selectedSku
							? JSON.parse(_selectedSku)
							: [];
						let isHasBought = false;
						// 判断是否加购
						if (
							selectedSkuInfo.length &&
							selectedSkuInfo.some(
								(curItem) =>
									curItem.productCode ===
									shoppItem.productCode
							)
						) {
							isHasBought = true;
						} else {
							isHasBought = shoppItem.hasBought;
						}
						// 重新整合下
						const newShoppItem = {
							...shoppItem,
							hasBought: isHasBought
						};

						return (
							<Grid.Item key={shoppItem.productCode}>
								<div>
									<SelectGoodsSkuCom
										setLoading={setLoading}
										goodsInfo={newShoppItem}
										goToGoodsDetail={goToGoodsDetail}
										handleChange={handleChange}
									/>
								</div>
							</Grid.Item>
						);
					})}
				</Grid>
			</div>
		);
	};
	return (
		<>
			<div
				ref={scrollRef}
				style={
					{
						'--adm-color-background': 'transparent'
					} as any
				}
			>
				<WindowScroller scrollElement={scrollRef.current || undefined}>
					{({ height }) => (
						<List>
							<AutoSizer disableHeight>
								{({ width }) => (
									<VirtualizedList
										autoHeight
										rowCount={filterList.length}
										rowRenderer={rowRenderer}
										width={width}
										height={height}
										rowHeight={rootFontSize * 2.6}
										overscanRowCount={20}
									/>
								)}
							</AutoSizer>
						</List>
					)}
				</WindowScroller>
				<InfiniteScroll
					loadMore={loadMore}
					hasMore={hasMore}
					// eslint-disable-next-line react/no-children-prop
					children={() => {
						if (!data?.length) {
							return null;
						}
						if (total === data.length) {
							return '';
						}
						return (
							<div>
								{window._$m.t('~没有啦，我也是有底线的~')}
							</div>
						);
					}}
				/>
			</div>
			{detailVisible && (
				<Popup
					destroyOnClose={true}
					visible={detailVisible}
					mask={false}
					style={{
						'--z-index': '10'
					}}
					position="right"
					bodyStyle={{
						width: '100vw'
					}}
					getContainer={() => document.getElementById('root')!}
				>
					<NewDetail
						backClick={handleCloseDetailModal}
						fromPage="onHandList"
					/>
				</Popup>
			)}
			{loading && <Loading />}
		</>
	);
};
export default memo(ListModule);
