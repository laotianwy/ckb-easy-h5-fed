/*
 * @Author: yusha
 * @Date: 2024-04-07 13:49:48
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-15 16:40:27
 * @Description: 筛选条件
 */

import { memo, useMemo, useState } from 'react';
import { Button, Input } from 'antd-mobile';
import ClassifyModule from '@/component/ClassifyModule';
import Check from '@/common/icons/Check';
import CollapseDefault from '@/common/icons/CollapseDefault';
import ExpandDefault from '@/common/icons/ExpandDefault';
import { LEFT_SORT_LIST } from '../../config';
import styles from './index.module.scss';

const Filter = (props: any) => {
	const { resetFormValue, setPageNum } = props;
	/** 类目标题 */
	const [classifyTitle, setClassifyTitle] = useState('');
	/** 左侧综合排序 */
	const [comprehensiveSort, setComprehensiveSort] = useState(
		LEFT_SORT_LIST[0].id as number
	);
	/** 分类id */
	const [cateId, setCateId] = useState<number | undefined>();
	/** 筛选弹窗状态 */
	const [filterShowStatus, setFilterShowStatus] = useState({
		leftFilterShow: false,
		classifyShow: false,
		rightFilterShow: false
	});

	/** 右侧价格区间对象。-(用于价格筛选临时存储) */
	const [temporarilyPrice, setTemporarilyPrice] = useState({
		startPrice: '',
		endPrice: ''
	});
	/** 右侧价格区间对象-(真正筛选条件) */
	const [rightRealPriceFilter, setRightRealPriceFilter] = useState({
		startPrice: '',
		endPrice: ''
	});
	/** 是否展示模态框 */
	const showMask =
		filterShowStatus.classifyShow ||
		filterShowStatus.leftFilterShow ||
		filterShowStatus.rightFilterShow;
	/** 点击类目回调 */
	const classifyClickCallBack = (productCategoryFrontendId, cateNameJp) => {
		setPageNum(1);
		setCateId(productCategoryFrontendId);
		setClassifyTitle(cateNameJp);
		// 重置弹窗状态
		setFilterShowStatus({
			leftFilterShow: false,
			classifyShow: !filterShowStatus.classifyShow,
			rightFilterShow: false
		});
		// 重新请求接口
		resetFormValue({
			cateId: productCategoryFrontendId
		});
	};
	/** 点击右侧弹窗，重置其他弹窗状态 */
	const setRightShow = () => {
		setTemporarilyPrice({ ...rightRealPriceFilter });
		setFilterShowStatus({
			leftFilterShow: false,
			classifyShow: false,
			rightFilterShow: !filterShowStatus.rightFilterShow
		});
	};
	/** 左侧排序文字 */
	const leftFilterText = useMemo(() => {
		const leftFilterText = LEFT_SORT_LIST?.find(
			(sortItem) => sortItem.id === comprehensiveSort
		)!['text'];
		return leftFilterText;
	}, [comprehensiveSort]);
	/** 关闭modal */
	const setAllClose = () => {
		setFilterShowStatus({
			leftFilterShow: false,
			classifyShow: false,
			rightFilterShow: false
		});
	};
	/** 点击价格升序降序 */
	const changeLeftPriceFilter = (id: number) => {
		setPageNum(1);
		setComprehensiveSort(id);
		// 相当于刷新状态，需要重置右侧筛选框的内容
		setTemporarilyPrice({ ...rightRealPriceFilter });
		setAllClose();
		// 需要重新请求
		resetFormValue({
			comprehensiveSort: id
		});
	};
	/** 右侧弹窗筛选-确认 */
	const rightViewClickFilter = () => {
		setRightRealPriceFilter({ ...temporarilyPrice });

		setPageNum(1);
		setAllClose();
		// 重新请求接口
		resetFormValue({
			...temporarilyPrice
		});
	};
	/** 判断是否进行筛选 */
	const judgeFiltered = () => {
		return rightRealPriceFilter.startPrice || rightRealPriceFilter.endPrice;
	};

	return (
		<>
			<div
				className={`${styles.header} ${showMask ? styles['border-radio'] : ''}`}
			>
				<div className={styles.filter}>
					<div className={styles.content}>
						<div
							className={`${styles.left} ${filterShowStatus.leftFilterShow ? styles['active-color'] : ''}`}
							onClick={() => {
								setFilterShowStatus({
									leftFilterShow:
										!filterShowStatus.leftFilterShow,
									classifyShow: false,
									rightFilterShow: false
								});
							}}
						>
							<span>{leftFilterText}</span>
							{filterShowStatus.leftFilterShow ? (
								<CollapseDefault
									color="#000"
									className={styles['active-image']}
								/>
							) : (
								<ExpandDefault
									color="#000"
									className={styles['img']}
								/>
							)}
						</div>

						<div
							className={`${styles.left} ${filterShowStatus.classifyShow || classifyTitle ? styles['active-color'] : ''}`}
							onClick={() => {
								setFilterShowStatus({
									leftFilterShow: false,
									classifyShow:
										!filterShowStatus.classifyShow,
									rightFilterShow: false
								});
							}}
						>
							<span className={styles['classify-title']}>
								{classifyTitle || window._$m.t('类目')}
							</span>
							{filterShowStatus.classifyShow ? (
								<CollapseDefault
									className={styles['active-image']}
								/>
							) : (
								<ExpandDefault className={styles['img']} />
							)}
						</div>

						<div
							className={`${styles['filter-area']}`}
							onClick={setRightShow}
						>
							{filterShowStatus.rightFilterShow ||
							judgeFiltered() ? (
								<img
									className={styles['active-img']}
									src="https://static-jp.theckb.com/static-asset/easy-h5/filter_selected.svg"
									alt=""
								/>
							) : (
								<img
									className={styles['img']}
									src="https://static-jp.theckb.com/static-asset/easy-h5/filter.svg"
									alt=""
								/>
							)}
						</div>
					</div>
					{filterShowStatus.leftFilterShow && (
						<div
							className={styles['filter-list']}
							style={{
								paddingBottom: '0'
							}}
						>
							{LEFT_SORT_LIST.map((sortItem) => (
								<div
									key={sortItem.id}
									className={styles['list-row']}
									onClick={() => {
										changeLeftPriceFilter(sortItem.id);
									}}
								>
									<span>{sortItem.text}</span>
									{comprehensiveSort === sortItem.id && (
										<Check color="#FF5010" />
									)}
								</div>
							))}
						</div>
					)}
					{filterShowStatus.classifyShow && (
						<div className={styles['filter-list']}>
							<ClassifyModule
								cateId={cateId}
								classifyClickCallBack={classifyClickCallBack}
							/>

							<div className={styles['classify-reset']}>
								<Button
									color="primary"
									shape="rounded"
									onClick={() => {
										setClassifyTitle('');
										setCateId(undefined);
										setAllClose();
										setPageNum(1);
										// 重新刷新请求
										resetFormValue({
											cateId: undefined
										});
									}}
									style={{
										// width: '1.88rem',
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'center'
									}}
								>
									{window._$m.t('重置')}
								</Button>
							</div>
						</div>
					)}
					{filterShowStatus.rightFilterShow && (
						<div className={styles['right-filter-view']}>
							<span className={styles['title']}>
								{window._$m.t('价格范围')}
							</span>
							<div className={styles['price-range']}>
								<Input
									placeholder={window._$m.t('自定义')}
									className={styles['searchContent']}
									style={{
										'--font-size': '.12rem',
										'--text-align': 'center'
									}}
									value={temporarilyPrice.startPrice}
									type="number"
									onChange={(value) => {
										const filteredValue = value.replace(
											/\D/g,
											''
										);
										setTemporarilyPrice({
											...temporarilyPrice,
											startPrice: filteredValue
										});
									}}
									pattern="[0-9]*"
									clearable
								/>

								<div className={styles['line']} />

								<Input
									placeholder={window._$m.t('自定义')}
									className={styles['searchContent']}
									style={{
										'--font-size': '.12rem',
										'--text-align': 'center'
									}}
									value={temporarilyPrice.endPrice}
									type="number"
									pattern="[0-9]*"
									max={200000000}
									onChange={(value) => {
										const filteredValue = value.replace(
											/\D/g,
											''
										);
										setTemporarilyPrice({
											...temporarilyPrice,
											endPrice: filteredValue
										});
									}}
									clearable
								/>
							</div>
							<div className={styles['price-choose-area']}>
								<div
									className={styles['price-choose-item']}
									onClick={() => {
										setTemporarilyPrice({
											startPrice: '0',
											endPrice: '1000'
										});
									}}
								>
									{window._$m.t('1000円以下')}
								</div>
								<div
									className={styles['price-choose-item']}
									onClick={() => {
										setTemporarilyPrice({
											startPrice: '1000',
											endPrice: '3000'
										});
									}}
								>
									{window._$m.t('1000-3000円')}
								</div>
								<div
									className={styles['price-choose-item']}
									onClick={() => {
										setTemporarilyPrice({
											startPrice: '3000',
											endPrice: '5000'
										});
									}}
								>
									{window._$m.t('3000-5000円')}
								</div>
								<div
									className={styles['price-choose-item']}
									onClick={() => {
										setTemporarilyPrice({
											startPrice: '5000',
											endPrice: '10000'
										});
									}}
								>
									{window._$m.t('5000-10000円')}
								</div>
								<div
									className={styles['price-choose-item']}
									onClick={() => {
										setTemporarilyPrice({
											startPrice: '10000',
											endPrice: ''
										});
									}}
								>
									{window._$m.t('10000円以上')}
								</div>
							</div>

							<div className={styles.btns}>
								<div
									className={styles.btn}
									onClick={() => {
										setRightRealPriceFilter({
											startPrice: '',
											endPrice: ''
										});
										setPageNum(1);
										setAllClose();
										// 重新刷新请求
										resetFormValue({
											startPrice: '',
											endPrice: ''
										});
									}}
								>
									{window._$m.t('重置')}
								</div>
								<div
									className={styles.btn}
									onClick={rightViewClickFilter}
								>
									{window._$m.t('搜索')}
								</div>
							</div>
						</div>
					)}
				</div>
			</div>
			{showMask && <div className={styles.mask} onClick={setAllClose} />}
		</>
	);
};
export default memo(Filter);
