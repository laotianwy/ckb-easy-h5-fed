/*
 * @Author: yusha
 * @Date: 2024-04-07 11:50:56
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-11 16:31:48
 * @Description: 顺手捎活动列表
 */

import { memo, useState, useRef, CSSProperties, useMemo } from 'react';
import qs from 'query-string';
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { omit } from 'ramda';
import { useNavigate } from 'react-router-dom';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ScrollPage from '@/common/ScrollPage';
import BackToTop from '@/component/BackToTop';
import { ShoppInfoProps } from '@/component/ShoppItem';
import { request } from '@/config/request';
import Loading from '@/component/Loading';
import {
	ON_HAND_GOODS_SELECT,
	ON_HAND_GOODS_SELECT_SESSION_KEY
} from '@/const/config';
import { ProductVO } from '@/service/easyMarket';
import styles from './index.module.scss';
import EmptyPage from './modules/EmptyPage';
import Filter from './modules/Filter';
import {
	ENUM_LEFT_SORT_LIST,
	LISTEN_VIEW_ID,
	PAGESIZE,
	SCROLL_TO_VIEW_ID
} from './config';
import ListModule from './modules/ListModule';
import Footer from './modules/Footer';

const OnHandList = () => {
	const [total, setTotal] = useState(0);
	const navigate = useNavigate();
	/** 列表 */
	const [data, setData] = useState<ProductVO[]>([]);
	/** 是否能加载更多 */
	const [hasMore, setHasMore] = useState(false);

	const [pageNum, setPageNum] = useState(1);
	/** 筛选条件 */
	const [formValue, setFormValue] = useState<any>({});

	const httpGetListRef = useRef(false);
	/** 是否是第一页请求完毕 */
	const httpFirstRequstRef = useRef(false);
	/** 接口 */
	const { runAsync: apiGetGoodsList, loading: apiGetGoodsListLoading } =
		useRequest(request.market.handyActivity.activityProductList, {
			manual: true
		});

	useAsyncEffect(async () => {
		const { comprehensiveSort, startPrice, endPrice, cateId } =
			formValue || {};

		httpGetListRef.current = true;
		let params: any = {};
		/** 存在内存里面已经选择的商品 */
		const selectedSku = window.sessionStorage.getItem(
			ON_HAND_GOODS_SELECT_SESSION_KEY
		);
		const _selectedSku = selectedSku ? JSON.parse(selectedSku) : [];
		// 入参
		params = {
			pageNum,
			pageSize: PAGESIZE,
			descendOrder: comprehensiveSort === ENUM_LEFT_SORT_LIST.DESC_ORDER,
			endPrice: Number(endPrice),
			startPrice: Number(startPrice),
			productCategoryFrontendId: Number(cateId),
			productList: _selectedSku,
			/** 排序字段:0 综合 1 价格(默认) 2 时间 */
			sortType:
				comprehensiveSort === ENUM_LEFT_SORT_LIST.SYNTHETIC_SORT
					? 0
					: 1,
			// 只会初始化一条数据，没有新增入口，可以写死
			handyActivityId: 1
		};
		// 如果没有开始价格，删除该字段
		if (startPrice === '') {
			params = omit(['startPrice'], params);
		}
		// 如果没有结束价格删除该字段
		if (endPrice === '') {
			params = omit(['endPrice'], params);
		}
		// 如果是综合。删除倒序字段
		if (comprehensiveSort === 1) {
			params = omit(['descendOrder'], params);
		}
		try {
			const res = await apiGetGoodsList(params);
			if (!res.success) return;
			httpGetListRef.current = false;
			// 防止records返回为null
			const newData = res.data.records ?? [];
			if (pageNum === 1) {
				httpFirstRequstRef.current = true;
				setData(newData);
			} else {
				httpFirstRequstRef.current = false;
				setData((data) => [...data, ...newData]);
			}
			setTotal(res.data?.total ?? 0);
			setHasMore(newData.length > 0);
		} catch (error) {
			httpFirstRequstRef.current = true;
			setData([]);
			setTotal(0);
			setHasMore(false);
		}
	}, [pageNum, formValue]);
	/** 加载更多 */
	const loadMore = async () => {
		if (httpGetListRef.current) return;
		setPageNum((num) => num + 1);
	};

	/** 判断是否展示回退按钮 */
	const getIsShowBackIcon = () => {
		const isConfigureLinkJump = window.sessionStorage.getItem(
			'isConfigureLinkJump'
		);
		if (isConfigureLinkJump) {
			window.sessionStorage.removeItem('isConfigureLinkJump');
			return false;
		}
		return true;
	};

	const goBack = () => {
		const search = qs.parse(window.location.search);
		const newSearch = {
			...search,
			fromPage: 'onHandList',
			[ON_HAND_GOODS_SELECT]: ON_HAND_GOODS_SELECT_SESSION_KEY
		};
		// 当前页面跳转并且刷新页面
		const url = '/order/create?' + qs.stringify(newSearch);
		navigate(url, {
			replace: true
		});
	};

	return (
		<ScrollPage
			id={LISTEN_VIEW_ID}
			title={
				<>
					<CustomNavbar
						title={window._$m.t('顺手捎')}
						isShowBackIcon={getIsShowBackIcon()}
						importantGoBack={goBack}
					/>
					<Filter
						resetFormValue={(value) => {
							setFormValue({ ...formValue, ...value });
						}}
						setPageNum={setPageNum}
					/>
				</>
			}
		>
			<BackToTop
				scrollViewIdName={SCROLL_TO_VIEW_ID}
				listenScrollId={LISTEN_VIEW_ID}
			/>

			<div
				className={`layout-content `}
				style={{
					width: '100%',
					marginTop: '1.09rem',
					height: 'calc(100% - 1.09rem)',
					marginBottom: '0.33rem'
				}}
			>
				{apiGetGoodsListLoading && <Loading />}
				<div className={styles.goods}>
					{/* 用于一键置顶定位用 */}
					<div id={SCROLL_TO_VIEW_ID} />
					<div className={styles['shopp-list']}>
						<div
							style={{
								height: '100%'
							}}
						>
							{httpFirstRequstRef.current && data.length === 0 ? (
								// 只有接口为空才展示该模块
								<EmptyPage goodsListLength={data.length ?? 0} />
							) : (
								<ListModule
									data={data}
									hasMore={hasMore}
									loadMore={loadMore}
									total={total}
								/>
							)}
						</div>
					</div>
				</div>
			</div>
			<Footer goBack={goBack} />
		</ScrollPage>
	);
};
export default memo(OnHandList);
