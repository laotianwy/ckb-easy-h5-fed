import { useAtom, useAtomValue } from 'jotai';
import { Button, CapsuleTabs, InfiniteScroll, Popup, Toast } from 'antd-mobile';
import { useEffect, useState } from 'react';
import { useMount, useRequest } from 'ahooks';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { request } from '@/config/request';
import { Order, User } from '@/Atoms';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import './index.scss';
import Gengduo1moreOne1 from '@/common/icons/Gengduo1moreOne1';
import Xihuanlike3 from '@/common/icons/Xihuanlike3';
import Xihuanlike2 from '@/common/icons/Xihuanlike2';
import { api } from '@/service';

// 后端接口参数映射。除了found都是当前的值
enum ENUM_BARCODE_MAP {
	'BAR005' = '',
	'BAR001' = 'BAR001',
	'BAR002' = 'BAR002',
	'BAR003' = 'BAR003',
	'BAR004' = 'BAR004'
}

const PriceCollectionList = () => {
	const isLogin = useAtomValue(User.isLogin);
	const [searchParams] = useSearchParams();
	const [acitve, setActive] = useState(-1);
	const [cartOriginalData] = useAtom(Order.cartOriginalData);
	const barCode = ENUM_BARCODE_MAP[searchParams.get('barCode')];

	const {
		data,
		runAsync: qryHundredModulesList,
		loading: apiGetGoodsListLoading
	} = useRequest(request.easyMarket.collection.qryHundredModulesList, {
		manual: true
	});
	const navigate = useNavigate();

	useMount(() => {
		qryHundredModulesList({ barCode });
	});
	const goCartList = () => {
		navigate('/order/shopcart?fromPage=goodslist');
	};
	// 切换激活
	const changeActive = (
		e: React.MouseEvent<SVGSVGElement, MouseEvent>,
		item
	) => {
		e.stopPropagation();
		setActive(item.id === acitve ? -1 : item.id);
	};

	// 喜欢点击
	const clickLike = async (i) => {
		const res = await api.easyMarket.collection.addActivityAreaById({
			id: i.id
		});
		if (res.success) {
			Toast.show(window._$m.t('收藏成功'));
			setActive(-1);
		}
	};

	const clickNoLike = async (i) => {
		const res = await api.easyMarket.collection.dislikeActivityAreaById({
			id: i.id
		});
		if (res.success) {
			// Toast.show(window._$m.t('操作成功'));
			qryHundredModulesList({
				barCode
			});
			setActive(-1);
		}
	};
	return (
		<div>
			<CustomNavbar
				title={searchParams.get('title') || ''}
				navBarRight={
					isLogin ? (
						<div className="header-right-cart" onClick={goCartList}>
							<div className="cart-num-area">
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									src="https://static-jp.theckb.com/static-asset/easy-h5/cart.svg"
									alt=""
								/>

								{cartOriginalData.cartNum > 0 ? (
									<div className="cart-num">
										{cartOriginalData.cartNum > 99
											? '99+'
											: cartOriginalData.cartNum}
									</div>
								) : null}
							</div>
						</div>
					) : null
				}
				isShowBackIcon={true}
				showBackHome={false}
			/>

			<div
				className="layout-style"
				style={{
					width: '100%',
					marginTop: '0.46rem'
				}}
			>
				<div className="price-collection-list">
					{data?.data?.map((item, index) => {
						return (
							<div
								key={item.id}
								className="price-collection-item"
							>
								{acitve === item.id && (
									<div className="price-collection-mark">
										<div
											className="price-collection-operate"
											onClick={() => {
												clickLike(item);
											}}
										>
											<Xihuanlike3 className="icon" />
											{window._$m.t('收藏该特辑')}
										</div>
										<div
											onClick={() => clickNoLike(item)}
											className="price-collection-operate"
										>
											<Xihuanlike2 className="icon" />
											{window._$m.t('不喜欢该特辑')}
										</div>
									</div>
								)}

								<div
									onClick={() => {
										navigate(
											`/goods/list?fromCollection=true&originSearch=false&noInput=true&pageTitle=${item.moduleName}&hotId=${item.id}&joinHot=true&showPrice=true`
										);
									}}
								>
									<img alt="" src={item.homeImgUrl} />
									<div className="price-collection-item-name">
										{item.barName}
									</div>
								</div>
								<div className="flex">
									<div className="price-collection-item-desc">
										{item.moduleName}
									</div>
									<div className="icon">
										<Gengduo1moreOne1
											width={'0.18rem'}
											height={'0.18rem'}
											onClick={(e) =>
												changeActive(e, item)
											}
										/>
									</div>
								</div>
							</div>
						);
					})}
				</div>
			</div>
		</div>
	);
};
export default PriceCollectionList;
