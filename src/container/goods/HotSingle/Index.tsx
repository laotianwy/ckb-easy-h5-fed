import classNames from 'classnames';
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { useCallback, useEffect, useState } from 'react';
import { Badge, Toast } from 'antd-mobile';
import { useAtomValue } from 'jotai';
import { useNavigate, useSearchParams } from 'react-router-dom';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ScrollPage from '@/common/ScrollPage';
import { api } from '@/service';
import {
	CollectionActivityRespDTO,
	CollectionClassifylMergeRespDTO
} from '@/service/easyMarket';
import { CustomEmpty } from '@/component/CustomEmpty';
import Gengduo1moreOne1 from '@/common/icons/Gengduo1moreOne1';
import GouwucheNormal from '@/common/icons/GouwucheNormal';
import { Order, User } from '@/Atoms';
import { goToLogin } from '@/config/request/interceptors';
import Xihuanlike3 from '@/common/icons/Xihuanlike3';
import Xihuanlike2 from '@/common/icons/Xihuanlike2';
import { PRODUCT_COLLECTION } from '@/common/Enums';
import styles from './Index.module.scss';

const HotSingle = () => {
	// 列表
	const [selfDefinedProductList, setSelfDefinedProductList] = useState<
		CollectionActivityRespDTO[]
	>([]);
	const navigate = useNavigate();
	const [query] = useSearchParams();
	const id = query.get('id');
	// 喜欢弹窗
	const [acitve, setActive] = useState<number>(-1);
	const isLogin = useAtomValue(User.isLogin);
	/** 购物车数据 */
	const cartOriginalData = useAtomValue(Order.cartOriginalData);
	const [partInfo, setPartInfo] = useState<CollectionClassifylMergeRespDTO>(
		{}
	);
	const { runAsync: qrySelfDefinedModulesList, loading } = useRequest(
		api.easyMarket.collection.qrySelfDefinedModulesList,
		{ manual: true }
	);

	const getList = useCallback(async () => {
		const res = await qrySelfDefinedModulesList();
		if (res.success) {
			// @ts-ignore
			const data = res.data.find((i) => i.id === id);
			console.log(data, res.data, 'data');

			setPartInfo(data);
			setSelfDefinedProductList(data.activityList);
		}
	}, [id, qrySelfDefinedModulesList]);

	useEffect(() => {
		getList();
	}, [getList, id]);

	// 切换弹窗
	const changeActive = (
		e: React.MouseEvent<SVGSVGElement, MouseEvent>,
		i
	) => {
		e.stopPropagation();
		setActive(i.id === acitve ? -1 : i.id);
	};

	// 喜欢点击
	const clickLike = async (i) => {
		const res = await api.easyMarket.collection.addActivityAreaById({
			id: i.id
		});
		if (res.success) {
			Toast.show(window._$m.t('收藏成功'));
			setActive(-1);
		}
	};

	const clickNoLike = async (i) => {
		const res = await api.easyMarket.collection.dislikeActivityAreaById({
			id: i.id
		});
		if (res.success) {
			// Toast.show('操作成功');
			getList();
			setActive(-1);
		}
	};

	const goToCart = () => {
		if (!isLogin) {
			goToLogin();
			return;
		}
		navigate('/order/shopcart?fromPage=goodsdetail');
	};

	return (
		<div className={classNames('layout-style')}>
			<ScrollPage
				title={
					<CustomNavbar
						title={partInfo.collectionClassifyName}
						navBarRight={
							<Badge content={cartOriginalData.cartNum || ''}>
								<div className={styles.cart} onClick={goToCart}>
									<GouwucheNormal />
								</div>
							</Badge>
						}
					/>
				}
			>
				<div className={styles.container}>
					{selfDefinedProductList?.length ? (
						<div className={styles.list}>
							{selfDefinedProductList.map((i) => {
								return (
									<div
										className={classNames(styles.hotItem)}
										key={i.id}
									>
										{acitve === i.id && (
											<div className={styles.mark}>
												<div
													className={styles.operate}
													onClick={() => {
														clickLike(i);
													}}
												>
													<Xihuanlike3
														className={styles.icon}
													/>

													{window._$m.t('收藏该特辑')}
												</div>
												<div
													onClick={() =>
														clickNoLike(i)
													}
													className={styles.operate}
												>
													<Xihuanlike2
														className={styles.icon}
													/>

													{window._$m.t(
														'不喜欢该特辑'
													)}
												</div>
											</div>
										)}

										<div
											onClick={() => {
												if (
													i.productCollection ===
													PRODUCT_COLLECTION.SINGLE
												) {
													navigate(
														`/goods/detail?productCode=${i.singleProductCode}`
													);
												} else {
													navigate(
														`/goods/collectionGoodsList?id=${i.id}`
													);
												}
											}}
										>
											<img
												className={styles.hotImg}
												src={i.homeImgUrl}
												alt=""
											/>

											<div className={styles.bottom}>
												<div
													className={styles.hotTitle}
												>
													{i.moduleName}
												</div>
												<div className={styles.icon}>
													<Gengduo1moreOne1
														width={'0.18rem'}
														height={'0.18rem'}
														onClick={(e) =>
															changeActive(e, i)
														}
													/>
												</div>
											</div>
										</div>
									</div>
								);
							})}
						</div>
					) : (
						<div>
							<CustomEmpty
								emptyTips={window._$m.t('暂无数据')}
								loading={loading}
							/>
						</div>
					)}
				</div>
			</ScrollPage>
		</div>
	);
};

export default HotSingle;
