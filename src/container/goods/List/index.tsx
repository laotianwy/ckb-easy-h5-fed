/* eslint-disable max-lines */
/**
 *	商品列表进来的几种方式
	1.首页 -> 搜索中转页 -> 商品列表 -> 再次点击搜索goback搜索中转页 -> 商品列表	(带有输入框和筛选搜索)
	2.首页（点击爆款火拼） -> 商品列表（无输入框搜索、但是有筛选搜索）
	3.分类页面（点击每个分类都会进入） -> 商品列表 -> 搜索中转页 -> 新的商品列表（再次点击搜索是goback）
 */
import {
	memo,
	useState,
	useRef,
	CSSProperties,
	useMemo,
	useEffect
} from 'react';
import {
	Input,
	Grid,
	List,
	InfiniteScroll,
	Toast,
	Checkbox,
	Button,
	Popup
} from 'antd-mobile';
import {
	List as VirtualizedList,
	AutoSizer,
	WindowScroller
} from 'react-virtualized';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useAtom, useAtomValue } from 'jotai';
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { of, omit } from 'ramda';
import { flushSync } from 'react-dom';
import queryString from 'query-string';
import SearchInput from '@/component/SearchInput';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ShoppingCartLarge from '@/common/icons/ShoppingCartLarge';
import ExpandDefault from '@/common/icons/ExpandDefault';
import CollapseDefault from '@/common/icons/CollapseDefault';
import Check from '@/common/icons/Check';
import ScrollPage from '@/common/ScrollPage';
import BackToTop from '@/component/BackToTop';
import { Order, User } from '@/Atoms';
import ShoppItem, { ShoppInfoProps } from '@/component/ShoppItem';
import { request } from '@/config/request';
import {
	ActivityAreaMoreQuery,
	BarSimpleVO,
	ProductVO
} from '@/service/easyMarket';
import {
	AddProductFavoriteReqDTO,
	ProductKeyWordSearchReqDTO,
	ProductKeywordRespDTO
} from '@/service/easyGoods';
import { changeImageCdn, groupArray } from '@/utils';
import { useEvent } from '@/utils/hooks/useEvent';
import Filter from '@/common/icons/Filter';
import Loading from '@/component/Loading';
import { EnumProductPurchaseType } from '@/common/Enums';
import ClassifyModule from '@/component/ClassifyModule';
import { jsBridge } from '@/utils/jsBridge';
import Detail from '@/container/goods/NewDetail';
import ComExpose from '@/component/ComExpose';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import styles from './index.module.scss';
import EmptyPage from './modules/EmptyPage';
interface VirtualizedListProps {
	index: number;
	key: string;
	style: CSSProperties;
}
enum Enum_RelationType {
	/** 运营标签 */
	Market = '1',
	/** 活动标签 */
	Active = '2'
}
/** 左侧搜索分类 */
const LEFT_SORT_LIST = [
	{
		id: 1,
		text: window._$m.t('综合')
	},
	{
		id: 2,
		text: window._$m.t('价格升序')
	},
	{
		id: 3,
		text: window._$m.t('价格降序')
	}
] as const;
const SCROLL_TO_VIEW_ID = 'goods-index-scroll-view';
const LISTEN_VIEW_ID = 'goods-list';
const GoodList = () => {
	const [searchParams, setSearchParams] = useSearchParams();
	const [cartOriginalData] = useAtom(Order.cartOriginalData);
	const [rootFontSize, setRootFontSize] = useState<number>(0);
	const [total, setTotal] = useState(0);
	const [detailVisible, setDetailVisible] = useState(
		Boolean(searchParams.get('productCode'))
	);
	const isLogin = useAtomValue(User.isLogin);
	const navigate = useNavigate();
	/** 价格筛选，1000以上 -结束价格 不穿这个字段 */

	/** 页面标题  */
	const [pageTitle, setPageTitle] = useState(
		decodeURIComponent(
			decodeURIComponent(searchParams.get('pageTitle') || '')
		)
	);
	const [classifyTitle, setClassifyTitle] = useState(
		searchParams.get('cateName') || ''
	);
	const [activeCode, setActiveCode] = useState('');
	/** 搜索关键字 */
	const [keyword, setKeyword] = useState(
		searchParams.get('keyword') || window._$m.t('请输入商品名称')
	);
	/** 是否来源搜索页 */
	const [originSearch] = useState(
		searchParams.get('originSearch') === 'true'
	);

	/** 是否没有输入框 */
	const [noInput] = useState(() => searchParams.get('noInput') === 'true');
	/** 分类id */
	const [cateId, setCateId] = useState<number | undefined>(() =>
		Number(searchParams.get('cateId'))
	);

	/** 活动爆款火拼id */
	const [hotId] = useState(() => Number(searchParams.get('hotId')));
	/** 是否从首页爆款火拼进来 */
	const [fromHot] = useState(() => searchParams.get('joinHot') === 'true');
	/** 是否需要置顶商品。爆款火拼列表接口需要 */
	const [needTopProduct, setNeedTopProduct] = useState(true);
	/** 是否显示价格 */
	const showPrice = searchParams.get('showPrice');
	/** 推荐热词map */
	const { runAsync: apiGetGoodsList, loading: apiGetGoodsListLoading } =
		useRequest(
			fromHot
				? request.easyMarket.activity.areaMoreProduct
				: request.easyGoods.product.searchKeyword,
			{
				manual: true
			}
		);
	/** 收藏取消 */
	const { runAsync: postCancelCollect } = useRequest(
		request.easyGoods.favoriteProduct.cancel,
		{
			manual: true
		}
	);

	/** 收藏 */
	const { runAsync: postAddCollect } = useRequest(
		request.easyGoods.favoriteProduct.postFavoriteProduct,
		{
			manual: true
		}
	);
	const [leftFilterShow, setLeftFilterShow] = useState(false);
	const [classifyShow, setClassifyShow] = useState(false);
	const [rightFilterShow, setRightFilterShow] = useState(false);
	const [data, setData] = useState<ShoppInfoProps[]>([]);
	const [hasMore, setHasMore] = useState(false);
	const scrollRef = useRef<HTMLDivElement>(null);
	const [pageNum, setPageNum] = useState(1);
	const [pageSize, setPageSize] = useState(20);
	// 收藏商品ID
	const [collectCode, setCollectCode] = useState<string>('');
	const httpGetListRef = useRef(false);
	/** 是否是第一页请求完毕 */
	const httpFirstRequstRef = useRef(false);

	/** 左侧综合排序 */
	const [comprehensiveSort, setComprehensiveSort] = useState(
		LEFT_SORT_LIST[0].id as number
	);

	/** 左侧排序文字 */
	const leftFilterText = useMemo(() => {
		const leftFilterText = LEFT_SORT_LIST?.find(
			(sortItem) => sortItem.id === comprehensiveSort
		)!['text'];
		return leftFilterText;
	}, [comprehensiveSort]);

	/** 价格范围1 */
	const [rightPriceRange1, setRightPriceRange1] = useState('');
	/** 价格范围2 */
	const [rightPriceRange2, setRightPriceRange2] = useState('');
	/** 是否只看直采商品 */
	const [onlyWatchDirectGoods, setOnlyWatchDirectGoods] = useState(false);
	/** 是否只看限时折扣商品 */
	const [onlyWatchActiveGoods, setOnlyWatchActiveGoods] = useState(false);
	const [activeList, setActiveList] = useState<any>([]);
	/** 活动标签 */
	const [activeTag, setActiveTag] = useState(
		searchParams.get('channelTag') || ''
	);
	const [relationTitle, setRelationTitle] = useState(
		searchParams.get('relationTitle') || ''
	);
	const [relationType, setRelationType] = useState(
		searchParams.get('relationType') || ''
	);

	/** 右侧价格区间对象。点击了确定筛选那么需要上面的 价格范围1和价格范围2 */
	const [rightPriceFilterView, setRightPriceFilterView] = useState({
		rightPriceRange1: '',
		rightPriceRange2: '',
		onlyDirctSale: true,
		onlyActive: false,
		activeTag: ''
	});
	/** 是否展示模态框 */
	const showMask = leftFilterShow || rightFilterShow || classifyShow;
	const setLeftShow = () => {
		setLeftFilterShow(!leftFilterShow);
		setClassifyShow(false);
		setRightFilterShow(false);
	};
	const classifyClickCallBack = (productCategoryFrontendId, cateNameJp) => {
		setPageNum(1);
		setCateId(productCategoryFrontendId);
		setClassifyTitle(cateNameJp);
		classifyClick();
	};
	const classifyClick = () => {
		setLeftFilterShow(false);
		setRightFilterShow(false);
		setClassifyShow(!classifyShow);
	};
	const resetRightView = () => {
		setRightPriceFilterView({
			...rightPriceFilterView,
			rightPriceRange1: '',
			rightPriceRange2: '',
			onlyActive: false,
			activeTag: ''
		});
	};
	const setRightShow = () => {
		setClassifyShow(false);
		setRightFilterShow(!rightFilterShow);
		setRightPriceFilterView({
			rightPriceRange1,
			rightPriceRange2,
			onlyDirctSale: true,
			onlyActive: onlyWatchActiveGoods,
			activeTag
		});
		setLeftFilterShow(false);
	};
	const getRootFontSize = () => {
		const rootPX = Number(
			document.documentElement.style.fontSize.split('px')[0]
		);
		setRootFontSize(rootPX);
	};
	useMount(() => {
		getRootFontSize();
	});
	useAsyncEffect(async () => {
		const res = await request.easyMarket.front.barActivity();
		setActiveList(res.data || []);
	}, []);
	useEffect(() => {
		if (!searchParams.get('productCode')) {
			setCateId(Number(searchParams.get('cateId')));
			setPageTitle(
				decodeURIComponent(searchParams.get('pageTitle') || '')
			);
		}
	}, [searchParams]);
	useAsyncEffect(async () => {
		httpGetListRef.current = true;
		interface ActivityAreaMoreQueryMy extends ActivityAreaMoreQuery {
			/** 营销活动标签列表 */
			activityTagList?: string[];
			/** 商品运营标签列表 */
			marketTagList?: string[];
			/** 营销频道标签列表 */
			channelTagList?: string[];
		}
		let params: ActivityAreaMoreQueryMy | ProductKeyWordSearchReqDTO = {};
		if (fromHot) {
			// 热门活动专区商品列表
			params = {
				pageNum,
				pageSize,
				areaId: hotId,
				descendOrder: comprehensiveSort === 3,
				productCategoryFrontendId: cateId,
				endPrice: Number(rightPriceRange2),
				startPrice: Number(rightPriceRange1),
				/** 排序字段:0 综合 1 价格(默认) 2 时间 */
				sortType: comprehensiveSort === 1 ? 0 : 1,
				needTopProduct
			};
		} else {
			// 词搜搜索
			params = {
				pageNum,
				pageSize,
				descendOrder: comprehensiveSort === 3,
				endPrice: Number(rightPriceRange2),
				startPrice: Number(rightPriceRange1),
				keyword,
				productCategoryFrontendId: cateId,
				onlyDirctSale: true,
				onlyDiscountSale: onlyWatchActiveGoods,
				/** 排序字段:0 综合 1 价格(默认) 2 时间 */
				sortType: comprehensiveSort === 1 ? 0 : 1
			};
		}

		// 如果没有分类id。那么删除该字段
		if (cateId === 0 && 'productCategoryFrontendId' in params) {
			params = omit(['productCategoryFrontendId'], params);
		}

		// 如果没有开始价格，删除该字段
		if (rightPriceRange1 === '') {
			params = omit(['startPrice'], params);
		}
		// 如果没有结束价格删除该字段
		if (rightPriceRange2 === '') {
			params = omit(['endPrice'], params);
		}
		// 如果是综合。删除倒序字段
		if (comprehensiveSort === 1) {
			params = omit(['descendOrder'], params);
		}
		// 如果没有关键字todo
		if (keyword === window._$m.t('请输入商品名称') && 'keyword' in params) {
			params = omit(['keyword'], params);
		}

		// 遍历res.data, 取name作为key,relationId作为value
		const map = new Map();
		if (relationTitle) {
			setOnlyWatchDirectGoods(true);
			const params: any = {
				bizScene: 1,
				stationCode: 'japanStation'
			};
			const res = await request.easyMarket.hot.keyFrontList(params);
			res.data?.forEach((item) => {
				map.set(item.name, item.relationId);
			});
		}
		if (relationType === Enum_RelationType.Market) {
			params.marketTagList = map.get(relationTitle).split(',');
		}
		if (relationType === Enum_RelationType.Active) {
			params.activityTagList = map.get(relationTitle).split(',');
		}
		if (activeTag) {
			// BAR000为写死活动，目前为一日元活动
			if (activeTag === 'BAR000') {
				console.log(activeCode?.split(','));
				params.activityTagList = activeCode?.split(',');
			} else {
				params.channelTagList = activeTag?.split(',');
			}
		}
		if (searchParams.get('newProductSort')) {
			params['newProductSort'] = 1;
		}
		const res = await apiGetGoodsList(params);
		if (res.success) {
			httpGetListRef.current = false;
			let newList: ShoppInfoProps[] = [];
			if ('countId' in (res?.data ?? {})) {
				if (res.data?.records) {
					res.data.records = (res.data?.records ?? []).filter(
						(item) => item.status === 1 || item.status === 2
					);
				}
				((res.data?.records ?? []) as ProductVO[]).forEach((item) => {
					newList.push({
						...item,
						/** 商品最低价 */
						productSellPriceJa: item.productLowestPrice,
						/** 商品标题 */
						title: item.productTitle,
						/** 商品id	 */
						productCode: item.productCode,
						/** 商品主图 */
						productMainImg: item.productMainImg,
						/** 商品售罄状态 */
						status: item.availableQuantity,
						/** 是否展示及时返金 */
						showGoBackPrice: false
					});
				});
			} else {
				const resonseList = (
					(res?.data?.records ??
						([] as ProductKeywordRespDTO[])) as any
				).filter((item: ProductKeywordRespDTO) => {
					// 如果是代采商品，直接返回
					if (
						item.productPurchaseType ===
						EnumProductPurchaseType.PROXY
					) {
						return true;
					}
					return item.status === 1 || item.status === 2;
				});
				(resonseList as ProductKeywordRespDTO[]).forEach((item) => {
					newList.push({
						...item,
						/** 商品最低价 */
						productSellPriceJa: item.productSellPriceJa,
						sellPrice: item.sellPrice,
						/** 商品标题 */
						title: item.productTitle,
						/** 商品id	 */
						productCode: item.productCode,
						/** 商品主图 */
						productMainImg: item.productMainImg,
						/** 商品售罄状态 */
						status: item.availableQuantity,
						/** 是否展示及时返金 */
						showGoBackPrice: false
					});
				});
			}
			newList = newList.map((item) => {
				return {
					...item
				};
			});
			if (pageNum === 1) {
				httpFirstRequstRef.current = true;
				setData(newList);
			} else {
				httpFirstRequstRef.current = false;
				setData((data) => [...data, ...newList]);
			}
			setTotal(res.data?.total ?? 0);
			setHasMore(newList.length > 0);
		}
	}, [
		pageNum,
		pageSize,
		apiGetGoodsList,
		needTopProduct,
		hotId,
		fromHot,
		comprehensiveSort,
		rightPriceRange2,
		rightPriceRange1,
		onlyWatchDirectGoods,
		onlyWatchActiveGoods,
		keyword,
		cateId,
		activeTag
	]);
	const loadMore = async () => {
		if (httpGetListRef.current) return;
		setPageNum((num) => num + 1);
		setNeedTopProduct(false);
	};
	const filterList = useMemo(() => {
		return groupArray(data, 2);
	}, [data]);
	const handleShoppItem = useEvent(async ({ productCode }) => {
		// TODO 如果是app内的h5页面。并且版本是1.1.2 那么走app的商详
		const currentPageParams =
			queryString.parse(window.location.search) ?? {};
		setSearchParams({
			...searchParams,
			...currentPageParams,
			productCode
		});
		setDetailVisible(true);
	});
	// 选择收藏商品
	const changeCollectItem = (code: string | undefined) => {
		if (code === collectCode) {
			setCollectCode('');
		} else {
			setCollectCode(code || '');
		}
	};

	/** 收藏切换 */
	const changeCollect = async (code?: string) => {
		const collectItem = data.find(
			(shoppItem) => shoppItem.productCode === code
		);
		if (!collectItem) return;
		const favoriteFlagBoolean = Boolean(collectItem['favoriteFlag']);
		// 取消收藏
		if (favoriteFlagBoolean) {
			const res = await postCancelCollect({
				productCode: code
			});
			if (!res.success) return;
			const newList = data.map((shoppItem) => {
				if (shoppItem.productCode !== code) return shoppItem;
				return {
					...shoppItem,
					favoriteFlag: 0
				};
			});
			flushSync(() => {
				setData(newList);
			});
			Toast.show({
				content: window._$m.t('取消收藏成功')
			});
			return;
		}
		// 收藏
		const { productCode, productMainImg, productSellPriceJa, title } =
			collectItem;
		const payload: AddProductFavoriteReqDTO = {
			productCode,
			productMainImg,
			productSellPrice: productSellPriceJa,
			productTitle: title
		};
		const res = await postAddCollect({
			...payload,
			favoriteProductType: 0
		});
		if (!res.success) return;
		const newList = data.map((shoppItem) => {
			if (shoppItem.productCode !== code) return shoppItem;
			return {
				...shoppItem,
				favoriteFlag: 1,
				openFavoriteFlagModal: true
			};
		});
		flushSync(() => {
			setData(newList);
		});
		Toast.show({
			content: window._$m.t('收藏成功')
		});
	};
	const rowRenderer = ({ index, key, style }: VirtualizedListProps) => {
		const item = filterList[index];
		if (!item) return;
		return (
			<div key={key} className={styles['shopp-view']} style={style}>
				<Grid columns={2} gap={rootFontSize * 0.08}>
					{item.map((shoppItem) => (
						<Grid.Item key={shoppItem.productCode}>
							<ComExpose
								always
								onExpose={() => {
									const {
										productCode,
										title,
										createTime,
										productMainImg
									} = shoppItem;
									taTrack({
										event: TaEvent.PAGE_VIEW,
										value: {
											product_code: productCode,
											product_title_ja: title,
											online_time: createTime,
											product_main_image_url:
												productMainImg
										}
									});
								}}
								onHide={() => console.log('hide')}
							>
								<ShoppItem
									favoriteFlag={shoppItem.favoriteFlag}
									activityFlag={shoppItem.activityFlag}
									onCollect={changeCollect}
									onClick={handleShoppItem}
									onSelectCollect={changeCollectItem}
									sellPrice={shoppItem.sellPrice}
									maskStatus={
										collectCode === shoppItem.productCode
									}
									productSellPriceJa={
										shoppItem.activityPriceJa ||
										shoppItem.activitySellPriceJpy ||
										shoppItem.productLowestPrice ||
										0
									}
									productLowestPrice={
										shoppItem.productSellPriceJa
									}
									activitySellPriceJpy={
										shoppItem.activityPriceJa
									}
									highPriceShow={shoppItem.highPriceShow}
									productPurchaseType={
										shoppItem.productPurchaseType
									}
									fromHot={fromHot}
									title={shoppItem.title}
									productCode={shoppItem.productCode}
									productMainImg={shoppItem.productMainImg}
									status={shoppItem.status}
									showGoBackPrice={shoppItem.showGoBackPrice}
								/>
							</ComExpose>
						</Grid.Item>
					))}
				</Grid>
			</div>
		);
	};

	/** 如果展示模态框 不让content滚动 */
	const dynamicClassName = 'no-scroll';
	const fullClassName = `${styles.goods} ${showMask ? styles[dynamicClassName] : ''}`;
	const goSearch = () => {
		if (cateId) {
			sessionStorage.setItem('cateId', String(cateId));
			sessionStorage.setItem('classifyTitle', classifyTitle);
		}
		if (activeTag) {
			sessionStorage.setItem('channelTag', activeTag);
		}
		if (originSearch) {
			navigate(-1);
		} else {
			navigate('/goods/search');
		}
	};

	/** 关闭modal */
	const setAllClose = () => {
		setLeftFilterShow(false);
		setRightFilterShow(false);
		setClassifyShow(false);
	};
	const goCartList = () => {
		navigate('/order/shopcart?fromPage=goodslist');
	};
	const changeLeftPriceFilter = (id: number) => {
		setPageNum(1);
		setComprehensiveSort(id);
		// 点击综合排序
		if (id === 1 && fromHot) {
			setNeedTopProduct(true);
		} else {
			// setNeedTopProduct(false);
			// eslint-disable-next-line line-comment-position
		} // 点击价格升序或者降序且不是hot商品
		// 点击价格排序时，默认勾选只看直采，且置灰，用户无法再点击
		const isPriceSort = id !== 1 && !fromHot;
		setOnlyWatchDirectGoods(isPriceSort);
		setRightPriceFilterView({
			...rightPriceFilterView,
			onlyDirctSale: true
		});
		setRightPriceRange1(rightPriceFilterView.rightPriceRange1);
		setRightPriceRange2(rightPriceFilterView.rightPriceRange2);
		setAllClose();
	};
	const rightViewClickFilter = () => {
		if (rightPriceFilterView.activeTag) {
			sessionStorage.removeItem('productCategoryFrontendId');
			setClassifyTitle('');
			setCateId(undefined);
			setRelationTitle('');
			setRelationType('');
		}
		// setNeedTopProduct(false);
		setActiveTag(rightPriceFilterView.activeTag);
		setRightPriceRange1(rightPriceFilterView.rightPriceRange1);
		setRightPriceRange2(rightPriceFilterView.rightPriceRange2);
		setOnlyWatchDirectGoods(Boolean(rightPriceFilterView.onlyDirctSale));
		setOnlyWatchActiveGoods(Boolean(rightPriceFilterView.onlyActive));
		setPageNum(1);
		setAllClose();
	};
	const getHeight = (): string => {
		if (noInput) {
			if (searchParams.get('fromCollection')) {
				return `0.7rem`;
			}
			if (!isLogin) {
				return '0.7rem';
			}
			return `1.13rem`;
		}
		if (isLogin) {
			return `1.52rem`;
		}
		return '1.52rem';
	};
	/** 判断是否展示回退按钮 */
	const getIsShowBackIcon = () => {
		const isConfigureLinkJump = window.sessionStorage.getItem(
			'isConfigureLinkJump'
		);
		if (isConfigureLinkJump) {
			window.sessionStorage.removeItem('isConfigureLinkJump');
			return false;
		}
		return true;
	};
	const judgeFiltered = () => {
		return (
			rightPriceRange1 ||
			rightPriceRange2 ||
			onlyWatchActiveGoods ||
			activeTag
		);
	};
	/** 计算layout-content高度 */
	const getLayoutContentHeight = () => {
		console.log(1);
		if (noInput) {
			return `calc(100% - 1.49rem)`;
		}
		if (isLogin) {
			return `calc(100% - 1.52rem)`;
		}
		return 'calc(100% - 1.49rem)';
	};
	return (
		<ScrollPage
			id={LISTEN_VIEW_ID}
			title={
				<>
					<CustomNavbar
						title={pageTitle || window._$m.t('商品列表')}
						navBarRight={
							isLogin ? (
								<div
									className={styles['header-right-cart']}
									onClick={goCartList}
								>
									<div className={styles['cart-num-area']}>
										<ShoppingCartLarge
											width={'0.16rem'}
											height={'0.16rem'}
										/>

										{cartOriginalData.cartNum > 0 ? (
											<div className={styles['cart-num']}>
												{cartOriginalData.cartNum > 99
													? '99+'
													: cartOriginalData.cartNum}
											</div>
										) : null}
									</div>
								</div>
							) : null
						}
						isShowBackIcon={getIsShowBackIcon()}
						showBackHome={true}
					/>

					<div
						className={`${styles.header} ${showMask ? styles['border-radio'] : ''}`}
					>
						{!noInput ? (
							<SearchInput
								value={relationTitle || keyword}
								onClick={goSearch}
								isText={true}
							/>
						) : null}

						{!searchParams.get('fromCollection') && (
							<div className={styles.filter}>
								<div
									className={styles.content}
									style={{
										height: `${!isLogin && fromHot ? '0' : '0.46rem'}`
									}}
								>
									{isLogin && (
										<div
											className={`${styles.left} ${leftFilterShow ? styles['active-color'] : ''}`}
											onClick={setLeftShow}
										>
											<span>{leftFilterText}</span>
											{leftFilterShow ? (
												<CollapseDefault
													color="#000"
													className={
														styles['active-image']
													}
												/>
											) : (
												<ExpandDefault
													color="#000"
													className={styles['img']}
												/>
											)}
										</div>
									)}

									{!fromHot &&
										!searchParams.get('newProductSort') && (
											<div
												className={`${styles.left} ${classifyShow || classifyTitle ? styles['active-color'] : ''}`}
												onClick={classifyClick}
											>
												<span
													className={
														styles['classify-title']
													}
												>
													{classifyTitle ||
														window._$m.t('类目')}
												</span>
												{classifyShow ? (
													<CollapseDefault
														className={
															styles[
																'active-image'
															]
														}
													/>
												) : (
													<ExpandDefault
														className={
															styles['img']
														}
													/>
												)}
											</div>
										)}

									{(!isLogin && fromHot) ||
									(!isLogin &&
										searchParams.get(
											'newProductSort'
										)) ? null : (
										<div
											className={`${styles['filter-area']}`}
											onClick={setRightShow}
										>
											{
												rightFilterShow ||
												judgeFiltered() ? (
													<img
														className={
															styles['active-img']
														}
														src="https://static-jp.theckb.com/static-asset/easy-h5/filter_selected.svg"
														alt=""
													/>
												) : (
													// <Filter
													// 	className={styles['active-img']}
													// />
													<img
														className={
															styles['img']
														}
														src="https://static-jp.theckb.com/static-asset/easy-h5/filter.svg"
														alt=""
													/>
												)

												// <Filter className={styles['img']} />
											}
										</div>
									)}
								</div>
								{leftFilterShow ? (
									<div
										className={styles['filter-list']}
										style={{
											paddingBottom: '0'
										}}
									>
										{LEFT_SORT_LIST.map((sortItem) => (
											<div
												key={sortItem.id}
												className={styles['list-row']}
												onClick={() => {
													changeLeftPriceFilter(
														sortItem.id
													);
												}}
											>
												<span>{sortItem.text}</span>
												{comprehensiveSort ===
												sortItem.id ? (
													<Check color="#FF5010" />
												) : null}
											</div>
										))}
									</div>
								) : null}
								{classifyShow ? (
									<div className={styles['filter-list']}>
										<ClassifyModule
											cateId={cateId}
											classifyClickCallBack={
												classifyClickCallBack
											}
										/>

										<div
											className={styles['classify-reset']}
										>
											<Button
												color="primary"
												shape="rounded"
												onClick={() => {
													setClassifyTitle('');
													setCateId(undefined);
													setAllClose();
													setPageNum(1);
													sessionStorage.removeItem(
														'productCategoryFrontendId'
													);
												}}
												style={{
													width: '1.88rem',
													display: 'flex',
													alignItems: 'center',
													justifyContent: 'center'
												}}
											>
												{window._$m.t('重置')}
											</Button>
										</div>
									</div>
								) : null}
								{rightFilterShow ? (
									<div
										className={styles['right-filter-view']}
									>
										{isLogin && (
											<>
												<span
													className={styles['title']}
												>
													{window._$m.t('价格范围')}
												</span>
												<div
													className={
														styles['price-range']
													}
												>
													<Input
														placeholder={window._$m.t(
															'自定义'
														)}
														className={
															styles[
																'searchContent'
															]
														}
														style={{
															'--font-size':
																'.12rem',
															'--text-align':
																'center'
														}}
														value={
															rightPriceFilterView.rightPriceRange1
														}
														type="number"
														onChange={(value) => {
															const filteredValue =
																value.replace(
																	/\D/g,
																	''
																);
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	activeTag:
																		'',
																	rightPriceRange1:
																		filteredValue
																}
															);
														}}
														pattern="[0-9]*"
														clearable
													/>

													<div
														className={
															styles['line']
														}
													/>

													<Input
														placeholder={window._$m.t(
															'自定义'
														)}
														className={
															styles[
																'searchContent'
															]
														}
														style={{
															'--font-size':
																'.12rem',
															'--text-align':
																'center'
														}}
														value={
															rightPriceFilterView.rightPriceRange2
														}
														type="number"
														pattern="[0-9]*"
														max={200000000}
														onChange={(value) => {
															const filteredValue =
																value.replace(
																	/\D/g,
																	''
																);
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	activeTag:
																		'',
																	rightPriceRange2:
																		filteredValue
																}
															);
														}}
														clearable
													/>
												</div>
												<div
													className={
														styles[
															'price-choose-area'
														]
													}
												>
													<div
														className={
															styles[
																'price-choose-item'
															]
														}
														onClick={() => {
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	activeTag:
																		'',
																	rightPriceRange1:
																		'0',
																	rightPriceRange2:
																		'1000'
																}
															);
														}}
													>
														{window._$m.t(
															'1000円以下'
														)}
													</div>
													<div
														className={
															styles[
																'price-choose-item'
															]
														}
														onClick={() => {
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	activeTag:
																		'',
																	rightPriceRange1:
																		'1000',
																	rightPriceRange2:
																		'3000'
																}
															);
														}}
													>
														{window._$m.t(
															'1000-3000円'
														)}
													</div>
													<div
														className={
															styles[
																'price-choose-item'
															]
														}
														onClick={() => {
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	activeTag:
																		'',
																	rightPriceRange1:
																		'3000',
																	rightPriceRange2:
																		'5000'
																}
															);
														}}
													>
														{window._$m.t(
															'3000-5000円'
														)}
													</div>
													<div
														className={
															styles[
																'price-choose-item'
															]
														}
														onClick={() => {
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	activeTag:
																		'',
																	rightPriceRange1:
																		'5000',
																	rightPriceRange2:
																		'10000'
																}
															);
														}}
													>
														{window._$m.t(
															'5000-10000円'
														)}
													</div>
													<div
														className={
															styles[
																'price-choose-item'
															]
														}
														onClick={() => {
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	activeTag:
																		'',
																	rightPriceRange1:
																		'10000',
																	rightPriceRange2:
																		''
																}
															);
														}}
													>
														{window._$m.t(
															'10000円以上'
														)}
													</div>
												</div>
											</>
										)}

										{!fromHot &&
											!searchParams.get(
												'newProductSort'
											) && (
												<>
													<span
														className={
															styles['title']
														}
													>
														{window._$m.t(
															'活动商品'
														)}
													</span>
													<div
														className={
															styles[
																'price-choose-area'
															]
														}
													>
														{activeList.map(
															(item) => (
																<div
																	key={
																		item.barCode
																	}
																	className={
																		item.barCode ===
																		rightPriceFilterView.activeTag
																			? styles[
																					'active-selected-item'
																				]
																			: styles[
																					'active-choose-item'
																				]
																	}
																	onClick={() => {
																		setActiveCode(
																			item.activityCodeList
																		);
																		setRightPriceFilterView(
																			{
																				...rightPriceFilterView,
																				rightPriceRange1:
																					'',
																				rightPriceRange2:
																					'',
																				onlyActive:
																					false,
																				activeTag:
																					rightPriceFilterView.activeTag ===
																					item.barCode
																						? ''
																						: item.barCode ??
																							''
																			}
																		);
																	}}
																>
																	{window._$m.t(
																		item.barName ??
																			''
																	)}
																</div>
															)
														)}
													</div>
												</>
											)}

										{!fromHot &&
											!searchParams.get(
												'newProductSort'
											) && (
												<div
													className={
														styles[
															'only-watch-purchase'
														]
													}
												>
													<Checkbox
														style={{
															'--icon-size':
																'0.14rem',
															display: 'flex'
														}}
														disabled={Boolean(
															rightPriceFilterView.activeTag
														)}
														checked={
															rightPriceFilterView.onlyActive
														}
														onChange={(val) => {
															setRightPriceFilterView(
																{
																	...rightPriceFilterView,
																	onlyActive:
																		val
																}
															);
														}}
													>
														{window._$m.t(
															'只看限时折扣商品'
														)}
													</Checkbox>
												</div>
											)}

										<div className={styles.btns}>
											<div
												className={styles.btn}
												onClick={() => {
													setRightPriceRange1('');
													setRightPriceRange2('');
													setOnlyWatchActiveGoods(
														false
													);
													setPageNum(1);
													setActiveTag('');
													setAllClose();
												}}
											>
												{window._$m.t('重置')}
											</div>
											<div
												className={styles.btn}
												onClick={rightViewClickFilter}
											>
												{window._$m.t('搜索')}
											</div>
										</div>
									</div>
								) : null}
							</div>
						)}
					</div>
					{showMask && (
						<div className={styles.mask} onClick={setAllClose} />
					)}
				</>
			}
		>
			<BackToTop
				scrollViewIdName={SCROLL_TO_VIEW_ID}
				listenScrollId={LISTEN_VIEW_ID}
			/>

			<div
				className={`layout-content `}
				style={{
					width: '100%',
					height: getLayoutContentHeight(),
					marginTop: getHeight()
				}}
			>
				{apiGetGoodsListLoading && <Loading />}
				<div className={fullClassName}>
					<div className={styles['shopp-list']}>
						<div
							id={SCROLL_TO_VIEW_ID}
							style={{
								height: '100%'
							}}
						>
							{httpFirstRequstRef.current && data.length === 0 ? (
								// 只有词搜接口为空才展示该模块
								<>
									{
										<EmptyPage
											goodsListLength={data.length ?? 0}
											rootFontSize={rootFontSize}
											onlyWatchDirectGoods={
												onlyWatchDirectGoods
											}
											onlyWatchActiveGoods={
												onlyWatchActiveGoods
											}
											activeTag={activeTag}
											cateId={cateId}
										/>
									}
								</>
							) : (
								<div
									ref={scrollRef}
									className={styles['cate-shopp-list']}
									style={
										{
											'--adm-color-background':
												'transparent'
										} as any
									}
								>
									<WindowScroller
										scrollElement={
											scrollRef.current || undefined
										}
									>
										{({ height }) => (
											<List>
												<AutoSizer disableHeight>
													{({ width }) => (
														<VirtualizedList
															autoHeight
															rowCount={
																filterList.length
															}
															rowRenderer={
																rowRenderer
															}
															width={width}
															height={height}
															rowHeight={
																rootFontSize *
																2.46
															}
															overscanRowCount={
																20
															}
														/>
													)}
												</AutoSizer>
											</List>
										)}
									</WindowScroller>
									<InfiniteScroll
										loadMore={loadMore}
										hasMore={hasMore}
										// eslint-disable-next-line react/no-children-prop
										children={() => {
											if (!data?.length) {
												return null;
											}
											if (total === data.length) {
												return '';
											}
											return (
												<div>
													{window._$m.t(
														'~没有啦，我也是有底线的~'
													)}
												</div>
											);
										}}
									/>
								</div>
							)}
						</div>
					</div>
				</div>
			</div>
			{detailVisible && (
				<Popup
					destroyOnClose={true}
					visible={detailVisible}
					mask={false}
					style={{
						'--z-index': '10'
					}}
					position="right"
					bodyStyle={{
						width: '100vw'
					}}
					getContainer={() => document.getElementById('root')!}
				>
					<Detail
						backClick={() => {
							setDetailVisible(false);
							window.history.back();
						}}
					/>
				</Popup>
			)}
		</ScrollPage>
	);
};
export default memo(GoodList);
