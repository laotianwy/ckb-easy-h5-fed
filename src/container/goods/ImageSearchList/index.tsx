/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-20 15:52:02EditTime: 2023-11-09 10:58:54
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-23 14:13:02
 * @FilePath: /ckb-easy-h5-fed/src/container/goods/ImageSearchList/index.tsx
 * @Description: 图搜页面
 */
import { useRef, useState, CSSProperties, useMemo, useContext } from 'react';
import qs from 'query-string';
import { useSearchParams, useNavigate, useLocation } from 'react-router-dom';
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { Grid, List, InfiniteScroll, Toast } from 'antd-mobile';
import {
	List as VirtualizedList,
	AutoSizer,
	WindowScroller
} from 'react-virtualized';
import { useAtomValue } from 'jotai';
import { flushSync } from 'react-dom';
import { request } from '@/config/request';
import Camera from '@/component/SearchInput/Camera';
import OpenVip from '@/component/OpenVip';
import { CustomRMImg } from '@/component/CustomRMImg';
import { EnumProductPurchaseType } from '@/common/Enums';
import { groupArray, jumpToApp, navigateAppOrH5 } from '@/utils';
import { useEvent } from '@/utils/hooks/useEvent';
import BackToTop, { BackToTopRefProps } from '@/component/BackToTop';
import Return from '@/common/icons/Return';
import ShoppItem, { ShoppInfoProps } from '@/component/ShoppItem';
import { User } from '@/Atoms';
import {
	AddProductFavoriteReqDTO,
	SearchImageRespDTO
} from '@/service/easyGoods';
import './index.scss';
import { RouterHistoryContext } from '@/App';
import Loading from '@/component/Loading';
import { commonGoBack } from '@/common/Layout/CustomNavbar';
interface VirtualizedListProps {
	index: number;
	key: string;
	style: CSSProperties;
}
const SCROLL_TO_VIEW_ID = 'goods-index-scroll-view';
const LISTEN_VIEW_ID = 'image-search-list-page';
interface ImageSearchShoppItemProps
	extends SearchImageRespDTO,
		ShoppInfoProps {}
const ImageSearchList = () => {
	const [searchParams, setSearchParams] = useSearchParams();
	const userInfo = useAtomValue(User.userDetail);
	const navigate = useNavigate();
	const location = useLocation();
	const [rootFontSize, setRootFontSize] = useState(0);

	/** 商品id */
	const [imageId, setImageId] = useState(searchParams.get('imageId') || '');

	/** 商品url */
	const [imageUrl, setImageUrl] = useState(
		searchParams.get('imageUrl') || ''
	);
	const routerHistory = useContext(RouterHistoryContext);
	const [curPage, prepage] = routerHistory;
	const [pageNum, setPageNum] = useState(1);
	const [pageSize] = useState(20);
	const [data, setData] = useState<ImageSearchShoppItemProps[]>([]);
	const [hasMore, setHasMore] = useState(false);
	const [openVipModal, setOpenVipModal] = useState(false);
	const goBackRef = useRef<BackToTopRefProps | null>(null);
	const scrollRef = useRef(null);
	const isHttpGetList = useRef(false);
	const firstHttpRequest = useRef(false);

	/** 图搜列表 */
	const { runAsync: apiGetProduct, loading: getProductLoading } = useRequest(
		request.easyGoods.product.searchImage,
		{
			manual: true
		}
	);

	/** 收藏取消 */
	const { runAsync: postCancelCollect } = useRequest(
		request.easyGoods.favoriteProduct.cancel,
		{
			manual: true
		}
	);

	/** 收藏 */
	const { runAsync: postAddCollect } = useRequest(
		request.easyGoods.favoriteProduct.postFavoriteProduct,
		{
			manual: true
		}
	);

	/** 线下商品点击同步接口 */
	const { runAsync: asyncOtherShopp } = useRequest(
		request.easyGoods.product.searchCheckSync,
		{
			manual: true
		}
	);
	const clickAsyncShopp = async (productCode: string) => {
		try {
			const res = await asyncOtherShopp({
				productCode
			});
		} catch (ex) {}
	};
	useAsyncEffect(async () => {
		setPageNum(1);
		setImageId(searchParams.get('imageId') as string);
		setImageUrl(searchParams.get('imageUrl') as string);
	}, [searchParams]);
	useAsyncEffect(async () => {
		isHttpGetList.current = true;
		const res = await apiGetProduct({
			pageNum,
			pageSize,
			imageId
		});
		if (res.success) {
			const newList: ImageSearchShoppItemProps[] = [];
			(res.data?.records ?? []).forEach(
				(item: ImageSearchShoppItemProps) => {
					newList.push({
						...item,
						/**
						 * 采购类型大写,代采 1, 直采：0
						 * @format int32
						 */
						productPurchaseType: item.productPurchaseType,
						/** 日本价格 */
						productSellPriceJa: item.productSellPriceJa,
						sellPrice: item.sellPrice,
						/** 商品标题 */
						title: item.productTitle,
						/** 商品SPU	 */
						productCode: item.productCode,
						/** 商品主图 */
						productMainImg: item.productMainImg,
						/** 商品状态 */
						status: item.availableQuantity || 0,
						/** 是否展示及时返金 */
						showGoBackPrice: false
					});
				}
			);
			if (pageNum === 1) {
				setData(newList);
				firstHttpRequest.current = true;
			} else {
				firstHttpRequest.current = false;
				setData((data) => [...data, ...newList]);
			}
			setHasMore(newList.length > 0);
		}
		isHttpGetList.current = false;
	}, [pageNum, pageSize, imageId]);
	const handleShoppItem = useEvent(
		({ productCode, productPurchaseType }: ShoppInfoProps) => {
			// 如果是直采商品直接去商品详情
			if (productPurchaseType === EnumProductPurchaseType.DIRECT)
				return navigateAppOrH5({
					h5Url: '/goods/detail',
					params: {
						productCode
					},
					appPageType: 'GoodDetail',
					navigate
				});

			// 同步线下商品信息
			if (typeof productCode === 'string' && productCode.length > 0) {
				clickAsyncShopp(productCode);
			}

			// 判断用户是否为会员。不是会员弹出卡片
			if (!userInfo?.membership?.templateLevel) {
				setOpenVipModal(true);
				// 弹会员卡片
				return;
			}
			// 代采商品
			jumpToApp({
				isVip: Boolean(userInfo?.membership?.templateLevel),
				jumpLink: `GoodsItemDetail?productCode=${productCode}`
			});
		}
	);

	/** 收藏切换 */
	const changeCollect = async (code?: string) => {
		const collectItem = data.find(
			(shoppItem) => shoppItem.productCode === code
		);
		if (!collectItem) return;
		const favoriteFlagBoolean = Boolean(collectItem['favoriteFlag']);
		// 取消收藏
		if (favoriteFlagBoolean) {
			const res = await postCancelCollect({
				productCode: code
			});
			if (res.success) {
				const newList = data.map((shoppItem) => {
					if (shoppItem.productCode !== code) return shoppItem;
					return {
						...shoppItem,
						favoriteFlag: 0
					};
				});
				flushSync(() => {
					setData(newList);
				});
				Toast.show({
					content: window._$m.t('取消收藏成功')
				});
			}
			return;
		}
		// 收藏
		const {
			platformType,
			productCode,
			productMainImg,
			productSellPriceJa,
			productTitle
		} = collectItem;
		const payload: AddProductFavoriteReqDTO = {
			platformType,
			productCode,
			productMainImg,
			productSellPrice: productSellPriceJa,
			productTitle
		};
		const res = await postAddCollect({
			...payload,
			favoriteProductType: 0
		});
		if (res.success) {
			const newList = data.map((shoppItem) => {
				if (shoppItem.productCode !== code) return shoppItem;
				return {
					...shoppItem,
					favoriteFlag: 1,
					openFavoriteFlagModal: true
				};
			});
			flushSync(() => {
				setData(newList);
			});
			Toast.show({
				content: window._$m.t('收藏成功')
			});
		}
	};
	const rowRenderer = ({ index, key, style }: VirtualizedListProps) => {
		const item = filterGoodsList[index];
		if (!item) return;
		return (
			<div key={key} className="shopp-view" style={style}>
				<Grid columns={2} gap={rootFontSize * 0.08}>
					{item.map((item, idx) => (
						<Grid.Item key={item.productCode}>
							<ShoppItem
								onCollect={changeCollect}
								onClick={handleShoppItem}
								sellPrice={item.sellPrice}
								productSellPriceJa={item.productSellPriceJa}
								productPurchaseType={item.productPurchaseType}
								title={item.title}
								productCode={item.productCode}
								productMainImg={item.productMainImg}
								status={item.status}
								showGoBackPrice={item.showGoBackPrice}
								favoriteFlag={item.favoriteFlag}
							/>
						</Grid.Item>
					))}
				</Grid>
			</div>
		);
	};
	const loadMore = async () => {
		if (isHttpGetList.current) return;
		setPageNum((pageNum) => pageNum + 1);
	};
	const filterGoodsList = useMemo(() => {
		return groupArray(data, 2);
	}, [data]);
	const getRootFontSize = () => {
		const rootPX = Number(
			document.documentElement.style.fontSize.split('px')[0]
		);
		setRootFontSize(rootPX);
	};
	useMount(() => {
		getRootFontSize();
	});
	const goback = () => {
		// 如果是路由带过来的参数
		const searchObj = qs.parse(location.search);
		const fromSniffApp = searchObj.fromSniffApp as string;
		commonGoBack({
			navigate,
			curPage,
			fromSniffApp
		});
		// navigate(-1);
	};
	const getCameraParams = useEvent((id: string, url: string) => {
		goBackRef.current?.clickToTop();
		setSearchParams({
			imageId: id,
			imageUrl: url
		});
	});
	const openVipModalClick = useEvent(() => {
		jumpToApp({
			isVip: Boolean(userInfo?.membership?.templateLevel)
		});
	});
	return (
		<div className="layout-content">
			{getProductLoading && <Loading />}
			<OpenVip
				visible={openVipModal}
				setVisible={setOpenVipModal}
				onClick={openVipModalClick}
			/>

			<div className="image-search-list-page" id={LISTEN_VIEW_ID}>
				<div className="image-search-header">
					<div className="header-search">
						<div className="view return" onClick={goback}>
							<Return className="return" />
						</div>

						<div className="view">
							<Camera color="#fff" onChange={getCameraParams} />
						</div>
					</div>
					<div className="filter-image-area">
						<img src={imageUrl} alt="" />
						<div />
					</div>
					<div className="search-image-url-view">
						<div className="search-image-url">
							<img src={imageUrl} alt="" />
						</div>
					</div>
				</div>
				<div className="content-list">
					<div className="empty-view" id={SCROLL_TO_VIEW_ID} />
					<BackToTop
						listenScrollId={LISTEN_VIEW_ID}
						scrollViewIdName={SCROLL_TO_VIEW_ID}
						ref={goBackRef}
					/>

					{firstHttpRequest.current &&
					filterGoodsList.length === 0 ? (
						<div className="shop-cart-empty">
							<CustomRMImg
								className="empty-img"
								alt={window._$m.t('占位图')}
								src="https://static-jp.theckb.com/static-asset/easy-h5/empty_content.svg"
							/>

							<div className="empty-desc">
								{window._$m.t(
									'搜索不到任何商品，再看看其他商品吧'
								)}
							</div>
						</div>
					) : (
						<div ref={scrollRef} className="content">
							<WindowScroller
								// Element to attach scroll event listeners. Defaults to window.
								scrollElement={scrollRef.current || undefined}
								onScroll={({ scrollTop }) => {
									console.log('scrollTop', scrollTop);
								}}
							>
								{({ height }) => (
									<List
										style={
											{
												'--adm-color-background':
													'F0F2F5'
											} as any
										}
									>
										<AutoSizer disableHeight>
											{({ width }) => (
												<VirtualizedList
													autoHeight
													rowCount={
														filterGoodsList.length
													}
													rowRenderer={rowRenderer}
													width={width}
													height={height}
													rowHeight={
														rootFontSize * 2.56
													}
													overscanRowCount={20}
												/>
											)}
										</AutoSizer>
									</List>
								)}
							</WindowScroller>
							<InfiniteScroll
								loadMore={loadMore}
								hasMore={hasMore}
							/>
						</div>
					)}
				</div>
			</div>
		</div>
	);
};
export default ImageSearchList;
