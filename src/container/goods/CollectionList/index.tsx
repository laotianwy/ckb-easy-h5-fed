import { useAtom, useAtomValue } from 'jotai';
import { Button, CapsuleTabs, Popup, Toast } from 'antd-mobile';
import { useCallback, useEffect, useState } from 'react';
import { useRequest } from 'ahooks';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { flushSync } from 'react-dom';
import { CloseOutline } from 'antd-mobile-icons';
import { Order, User } from '@/Atoms';
import { request } from '@/config/request';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ShoppItem from '@/component/ShoppItem';
import { navigateAppOrH5 } from '@/utils';
import { useEvent } from '@/utils/hooks/useEvent';
import './index.scss';
import { AddProductFavoriteReqDTO } from '@/service/easyGoods';
import { CollectionActivityRespDTO } from '@/service/easyMarket';
import { PRODUCT_COLLECTION } from '@/common/Enums';
import Xihuanlike3 from '@/common/icons/Xihuanlike3';
import Xihuanlike2 from '@/common/icons/Xihuanlike2';
import { api } from '@/service';
import Gengduo1moreOne1 from '@/common/icons/Gengduo1moreOne1';

const CollectionList = () => {
	const isLogin = useAtomValue(User.isLogin);
	const navigation = useNavigate();
	const [searchParams] = useSearchParams();
	const [cartOriginalData] = useAtom(Order.cartOriginalData);
	const { runAsync: getCollectionList } = useRequest(
		request.easyMarket.collection.qrySpecialModulesList,
		{
			manual: true
		}
	);
	/** 收藏取消 */
	const { runAsync: postCancelCollect } = useRequest(
		request.easyGoods.favoriteProduct.cancel,
		{
			manual: true
		}
	);
	/** 收藏 */
	const { runAsync: postAddCollect } = useRequest(
		request.easyGoods.favoriteProduct.postFavoriteProduct,
		{
			manual: true
		}
	);
	const [popupVisible, setPopupVisible] = useState(false);
	// const [data, setData] = useState([]);
	const [collectionList, setCollectionList] = useState<
		CollectionActivityRespDTO[]
	>([]);
	// 激活蒙层
	const [acitve, setActive] = useState(-1);
	const [tabKey, setTabKey] = useState('0');
	const navigate = useNavigate();
	const goCartList = () => {
		navigate('/order/shopcart?fromPage=goodslist');
	};
	const getData = useCallback(async () => {
		getCollectionList({
			collectionId: Number(searchParams.get('one'))
		}).then((res) => {
			setCollectionList(res.data);
		});
	}, [getCollectionList, searchParams]);

	useEffect(() => {
		getData();
	}, [getData]);

	useEffect(() => {
		const ele = document.querySelector(
			`#collection${tabKey}`
		) as HTMLElement;
		if (!ele) return;
		const top = ele.offsetTop;
		document.querySelector('#homeWrap').scrollTo({
			top,
			behavior: 'smooth'
		});
	}, [tabKey]);
	/** 收藏切换 */
	const changeCollect = async (
		code?: string,
		item?: CollectionActivityRespDTO
	) => {
		const data = collectionList.find(
			(i) => i.id === item.id
		).homePageProductsInfoList;
		const collectItem = data.find(
			(shoppItem) => shoppItem.productCode === code
		);
		if (!collectItem) return;
		const favoriteFlagBoolean = Boolean(collectItem['favoriteFlag']);
		// 取消收藏
		if (favoriteFlagBoolean) {
			const res = await postCancelCollect({
				productCode: code
			});
			if (!res.success) return;
			const newList = data.map((shoppItem) => {
				if (shoppItem.productCode !== code) return shoppItem;
				return {
					...shoppItem,
					favoriteFlag: 0
				};
			});
			collectionList.find(
				(i) => i.id === item.id
			).homePageProductsInfoList = newList;
			setCollectionList(collectionList);
			Toast.show({
				content: window._$m.t('取消收藏成功')
			});
			return;
		}
		// 收藏
		const {
			productCode,
			productMainImg,
			productLowestPrice,
			productTitle
		} = collectItem;
		const payload: AddProductFavoriteReqDTO = {
			productCode,
			productMainImg,
			productSellPrice: productLowestPrice,
			productTitle
		};
		const res = await postAddCollect({
			...payload,
			favoriteProductType: 0
		});
		if (!res.success) return;
		const newList = data.map((shoppItem) => {
			if (shoppItem.productCode !== code) return shoppItem;
			return {
				...shoppItem,
				favoriteFlag: 1,
				openFavoriteFlagModal: true
			};
		});
		collectionList.find((i) => i.id === item.id).homePageProductsInfoList =
			newList;

		setCollectionList(collectionList);
		Toast.show({
			content: window._$m.t('收藏成功')
		});
	};
	const handleShoppItem = useEvent(({ productCode }) => {
		navigateAppOrH5({
			h5Url: '/goods/detail',
			params: {
				productCode
			},
			appPageType: 'GoodDetail',
			navigate
		});
	});

	// 切换弹窗
	const changeActive = (
		e: React.MouseEvent<SVGSVGElement, MouseEvent>,
		i
	) => {
		e.stopPropagation();
		setActive(i.id === acitve ? -1 : i.id);
	};

	// 喜欢点击
	const clickLike = async (i) => {
		const res = await api.easyMarket.collection.addActivityAreaById({
			id: i.id
		});
		if (res.success) {
			Toast.show(window._$m.t('收藏成功'));
			setActive(-1);
		}
	};

	const clickNoLike = async (i) => {
		const res = await api.easyMarket.collection.dislikeActivityAreaById({
			id: i.id
		});
		if (res.success) {
			// Toast.show('操作成功');
			// getList();
			getData();
			setActive(-1);
		}
	};

	return (
		<div>
			<CustomNavbar
				title={searchParams.get('title') || ''}
				navBarRight={
					isLogin ? (
						<div className="header-right-cart" onClick={goCartList}>
							<div className="cart-num-area">
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									src="https://static-jp.theckb.com/static-asset/easy-h5/cart.svg"
									alt=""
								/>

								{cartOriginalData.cartNum > 0 ? (
									<div className="cart-num">
										{cartOriginalData.cartNum > 99
											? '99+'
											: cartOriginalData.cartNum}
									</div>
								) : null}
							</div>
						</div>
					) : null
				}
				isShowBackIcon={true}
				showBackHome={false}
			/>

			<div style={{ position: 'relative' }}>
				<CapsuleTabs
					className="capsule-tabs"
					onChange={(key) => {
						setTabKey(key);
					}}
					activeKey={tabKey}
				>
					{collectionList?.map((item, index) => {
						return (
							<CapsuleTabs.Tab
								title={item.moduleName}
								key={index}
							/>
						);
					})}
				</CapsuleTabs>
				<div
					className="capsule-tabs-btn"
					onClick={() => {
						setPopupVisible(true);
					}}
				>
					<img
						style={{ width: '0.16rem', height: '0.16rem' }}
						src="https://static-s.theckb.com/BusinessMarket/App/home/hamburger36252636.png"
						alt=""
					/>
				</div>
			</div>
			<Popup
				visible={popupVisible}
				onMaskClick={() => {
					setPopupVisible(false);
				}}
				position="right"
				bodyStyle={{ width: '86vw' }}
			>
				<CloseOutline
					fontSize={16}
					color="#000"
					style={{
						position: 'absolute',
						top: '0.54rem',
						left: '0.12rem'
					}}
					onClick={() => {
						setPopupVisible(false);
					}}
				/>

				<div className="popup-title">{window._$m.t('特辑选择')}</div>
				<div className="popup-title-name">
					{window._$m.t('特辑名称')}
				</div>
				<div className="popup-list">
					{collectionList?.map((item, index) => {
						return (
							<span
								key={index}
								className="popup-select"
								style={{
									color:
										tabKey === String(index)
											? '#fff'
											: '#000',
									backgroundColor:
										tabKey === String(index)
											? '#000'
											: '#f1f1f1'
								}}
								onClick={() => {
									setPopupVisible(false);
									setTabKey(String(index));
								}}
							>
								{item.moduleName}
							</span>
						);
					})}
				</div>
			</Popup>
			<div
				className="layout-style"
				style={{
					width: '100%',
					height: '100vh',
					marginTop: '1.02rem'
				}}
				id="homeWrap"
			>
				<div className="collection-list">
					{collectionList?.map((item, index) => {
						return (
							<div
								id={`collection${index}`}
								key={index}
								className="collection-list-item"
							>
								<div className="collection-list-item-img">
									{acitve === item.id && (
										<div className="collection-list-item-mark">
											<div
												className="collection-list-item-operate"
												onClick={() => {
													clickLike(item);
												}}
											>
												<Xihuanlike3 className="mark-icon" />
												{window._$m.t('收藏该特辑')}
											</div>
											<div
												onClick={() =>
													clickNoLike(item)
												}
												className="collection-list-item-operate"
											>
												<Xihuanlike2 className="mark-icon" />
												{window._$m.t('不喜欢该特辑')}
											</div>
										</div>
									)}

									<div className="collection-list-item-icon">
										<Gengduo1moreOne1
											fontSize={'0.2rem'}
											onClick={(e) =>
												changeActive(e, item)
											}
										/>
									</div>
									<img
										onClick={() => {
											if (
												item.productCollection ===
												PRODUCT_COLLECTION.ALL
											) {
												const url = `/goods/collectionGoodsList?id=${item.id}`;
												return navigation(url);
											}
											if (!item.singleProductCode) return;
											navigation(
												`/goods/detail?productCode=${item.singleProductCode}`
											);
										}}
										src={item.homeImgUrl}
										alt=""
									/>
								</div>
								<div className="collection-list-item-goods">
									{item.homePageProductsInfoList &&
										item.homePageProductsInfoList
											.slice(0, 4)
											.map((shopItem, index) => {
												return (
													<div
														className="collection-list-item-good"
														key={index}
													>
														<ShoppItem
															favoriteFlag={
																shopItem.favoriteFlag
															}
															activityFlag={
																shopItem.activityFlag
															}
															onCollect={
																changeCollect
															}
															onClick={
																handleShoppItem
															}
															item={item}
															productSellPriceJa={
																shopItem.activitySellPriceJpy ||
																shopItem.productLowestPrice
															}
															productLowestPrice={
																shopItem.productLowestPrice
															}
															activitySellPriceJpy={
																shopItem.activitySellPriceJpy
															}
															highPriceShow={
																shopItem.highPriceShow
															}
															productPurchaseType={
																shopItem.productPurchaseType
															}
															title={
																shopItem.productTitle
															}
															productCode={
																shopItem.productCode
															}
															productMainImg={
																shopItem.productMainImg
															}
															status={
																shopItem.status
															}
														/>
													</div>
												);
											})}
								</div>
								{item.homePageProductsInfoList &&
									item.homePageProductsInfoList.length >
										4 && (
										<div
											className="collection-list-item-btn"
											onClick={() => {
												if (
													item.productCollection ===
													PRODUCT_COLLECTION.ALL
												) {
													const url = `/goods/collectionGoodsList?id=${item.id}`;
													return navigation(url);
												}
												if (!item.singleProductCode)
													return;
												navigation(
													`/goods/detail?productCode=${item.singleProductCode}`
												);
											}}
										>
											{window._$m.t('查看更多')}
										</div>
									)}
							</div>
						);
					})}
				</div>
			</div>
		</div>
	);
};
export default CollectionList;
