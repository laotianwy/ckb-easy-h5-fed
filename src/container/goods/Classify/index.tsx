/*
 * @Date: 2023-12-21 21:28:59
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-30 20:43:43
 * @FilePath: /ckb-easy-h5-fed/src/container/goods/Classify/index.tsx
 * @Description:
 */
import { useState, Fragment, useEffect } from 'react';
import {
	createSearchParams,
	useNavigate,
	useSearchParams
} from 'react-router-dom';
import classNames from 'classnames';
import { useAsyncEffect, useRequest } from 'ahooks';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import { ProductCategoryFrontendShortRespDTO } from '@/service/goods';
import ScrollPage from '@/common/ScrollPage';
import ClassifyModule from '@/component/ClassifyModule';
import { CustomRMImg } from '@/component/CustomRMImg';
import QianwangdaicaiarrowRight from '@/common/icons/QianwangdaicaiarrowRight';
import styles from './index.module.scss';
function Search() {
	const [searchParams] = useSearchParams();
	const [title, setTitle] = useState(window._$m.t('分类搜索'));
	const [categoryList, setCategoryList] = useState<
		ProductCategoryFrontendShortRespDTO[]
	>([]);
	const { runAsync: apiGetCategory, loading } = useRequest(
		request.easyGoods.productCategoryFrontend.tree,
		{
			manual: true
		}
	);
	const navigate = useNavigate();
	useAsyncEffect(async () => {
		const res = await apiGetCategory({});
		const level = searchParams.get('level');
		const cateId = searchParams.get('cateId');
		const title =
			decodeURIComponent(searchParams.get('pageTitle') ?? '') ||
			window._$m.t('分类搜索');
		if (level === '1' && cateId) {
			const children =
				res.data?.find(
					(i) =>
						Number(i.productCategoryFrontendId) === Number(cateId)
				) || {};
			setCategoryList(children.children || []);
		} else if (level === '2' && cateId) {
			let list: ProductCategoryFrontendShortRespDTO[] = [];
			res.data?.forEach((i) => {
				list = list.concat(i.children || []);
			});
			const children =
				list?.find(
					(i) =>
						Number(i.productCategoryFrontendId) === Number(cateId)
				) || {};
			setCategoryList(children.children || []);
		} else {
			setCategoryList(res.data || []);
		}
		setTitle(title);
	}, [searchParams]);
	// 跳转分类或者详情
	const goDetail = (record: ProductCategoryFrontendShortRespDTO) => {
		const {
			productCategoryFrontendId = '',
			cateNameJp = '',
			children,
			level = 1
		} = record;
		navigate({
			pathname: children?.length ? '/goods/classify' : '/goods/list',
			search: `?${createSearchParams({
				cateId: String(productCategoryFrontendId),
				pageTitle: cateNameJp,
				level: level.toString()
			})}`
		});
	};
	return (
		<ScrollPage
			title={
				<CustomNavbar
					fixed={false}
					title={title}
					isShowBackIcon={true}
				/>
			}
		>
			<div
				className={classNames(
					styles['classify-layout-content'],
					'layout-content'
				)}
			>
				{!loading && (
					<div className={styles['classify-container']}>
						{categoryList.map((item, index) => {
							return (
								<div
									className={styles['classify-item']}
									key={index}
									onClick={() => goDetail(item)}
								>
									{searchParams.get('level') !== '2' ? (
										<>
											<div className={styles['item-img']}>
												<CustomRMImg
													width={'0.48rem'}
													height={'0.48rem'}
													imgSize={200}
													src={item.cateIcon}
												/>
											</div>
											<div
												className={
													styles['classify-info']
												}
											>
												<div
													className={styles['title']}
												>
													{item.cateNameJp}
												</div>
												<div
													className={styles['deital']}
												>
													{item.children
														?.map(
															(i) => i.cateNameJp
														)
														.join('.')}
												</div>
											</div>
											<div
												className={
													styles['arrow-right']
												}
											>
												<QianwangdaicaiarrowRight
													size={'0.16rem'}
													color="#000"
												/>
											</div>
										</>
									) : (
										<>
											<div
												className={
													styles['classify-info']
												}
											>
												<div
													className={styles['title']}
												>
													{item.cateNameJp}
												</div>
											</div>
											<div
												className={
													styles['arrow-right']
												}
											>
												<QianwangdaicaiarrowRight
													size={'0.16rem'}
													color="#000"
												/>
											</div>
										</>
									)}
								</div>
							);
						})}
					</div>
				)}
			</div>
		</ScrollPage>
	);
}
export default Search;
