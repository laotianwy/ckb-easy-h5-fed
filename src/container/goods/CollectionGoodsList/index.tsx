import { useAtom, useAtomValue } from 'jotai';
import {
	Button,
	CapsuleTabs,
	InfiniteScroll,
	List,
	Popup,
	Toast
} from 'antd-mobile';
import { useEffect, useState } from 'react';
import { useRequest } from 'ahooks';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { flushSync } from 'react-dom';
import { request } from '@/config/request';
import { Order, User } from '@/Atoms';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ShoppItem from '@/component/ShoppItem';
import './index.scss';
import { navigateAppOrH5 } from '@/utils';
import { useEvent } from '@/utils/hooks/useEvent';
import Shoucanglike2 from '@/common/icons/Shoucanglike2';
import ScrollPage from '@/common/ScrollPage';
import { AddProductFavoriteReqDTO } from '@/service/easyGoods';
import { ActivityAreaMoreQuery } from '@/service/easyMarket';

const CollectionList = () => {
	const isLogin = useAtomValue(User.isLogin);
	const [searchParams] = useSearchParams();
	const [data, setData] = useState<any[]>([]);
	const [pageNum, setPageNum] = useState(1);
	const [hasMore, setHasMore] = useState(true);
	const [like, setLike] = useState(false);
	const [noData, setNoData] = useState(false);
	const [needTopProduct, setNeedTopProduct] = useState(true);

	const { data: collectionList, runAsync: addActivityAreaById } = useRequest(
		request.easyMarket.collection.addActivityAreaById,
		{
			manual: true
		}
	);
	const { runAsync: removeCollectionList } = useRequest(
		request.easyMarket.collection.removeCollectionList,
		{
			manual: true
		}
	);
	const { data: collectionInfo, runAsync: qryPersonageInfo } = useRequest(
		request.easyMarket.collection.qryPersonageInfo,
		{
			manual: true
		}
	);
	const { runAsync: apiGetGoodsList, loading: apiGetGoodsListLoading } =
		useRequest(request.easyMarket.activity.areaMoreProduct, {
			manual: true
		});
	/** 收藏取消 */
	const { runAsync: postCancelCollect } = useRequest(
		request.easyGoods.favoriteProduct.cancel,
		{
			manual: true
		}
	);
	/** 收藏 */
	const { runAsync: postAddCollect } = useRequest(
		request.easyGoods.favoriteProduct.postFavoriteProduct,
		{
			manual: true
		}
	);
	async function loadMore() {
		const params: ActivityAreaMoreQuery = {
			pageNum,
			pageSize: 10,
			areaId: Number(searchParams.get('id')),
			/** 排序字段:0 综合 1 价格(默认) 2 时间 */
			sortType: 1,
			needTopProduct
		};
		const res = await apiGetGoodsList(params);
		const append = res.data.records;
		setData((val) => [...val, ...append]);
		setPageNum(pageNum + 1);
		setNoData(
			!res.data?.total && !res.data.records.length && needTopProduct
		);
		setNeedTopProduct(false);
		setHasMore(append.length > 0);
	}

	useEffect(() => {
		const id = Number(searchParams.get('id'));
		qryPersonageInfo({ activityAreaId: id }).then((res) => {
			setLike(res.data.collectionFlag);
		});
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	const [cartOriginalData] = useAtom(Order.cartOriginalData);
	const navigate = useNavigate();
	const goCartList = () => {
		navigate('/order/shopcart?fromPage=goodslist');
	};
	const likeBtnClick = async (flag) => {
		const id = Number(searchParams.get('id'));
		let res;
		if (flag) {
			res = await removeCollectionList([id]);
		} else {
			res = await addActivityAreaById({ id });
		}
		if (res.success) {
			setLike(!like);
			if (flag) {
				Toast.show({
					content: window._$m.t('取消收藏成功')
				});
			} else {
				Toast.show({
					content: window._$m.t('收藏成功')
				});
			}
		}
	};
	/** 收藏切换 */
	const changeCollect = async (code?: string) => {
		const collectItem = data.find(
			(shoppItem) => shoppItem.productCode === code
		);
		if (!collectItem) return;
		const favoriteFlagBoolean = Boolean(collectItem['favoriteFlag']);
		// 取消收藏
		if (favoriteFlagBoolean) {
			const res = await postCancelCollect({
				productCode: code
			});
			if (!res.success) return;
			const newList = data.map((shoppItem) => {
				if (shoppItem.productCode !== code) return shoppItem;
				return {
					...shoppItem,
					favoriteFlag: 0
				};
			});
			flushSync(() => {
				setData(newList);
			});
			Toast.show({
				content: window._$m.t('取消收藏成功')
			});
			return;
		}
		// 收藏
		const {
			productCode,
			productMainImg,
			productLowestPrice,
			productTitle
		} = collectItem;
		const payload: AddProductFavoriteReqDTO = {
			productCode,
			productMainImg,
			productSellPrice: productLowestPrice,
			productTitle
		};
		const res = await postAddCollect({
			...payload,
			favoriteProductType: 0
		});
		if (!res.success) return;
		const newList = data.map((shoppItem) => {
			if (shoppItem.productCode !== code) return shoppItem;
			return {
				...shoppItem,
				favoriteFlag: 1,
				openFavoriteFlagModal: true
			};
		});
		flushSync(() => {
			setData(newList);
		});
		Toast.show({
			content: window._$m.t('收藏成功')
		});
	};
	const handleShoppItem = useEvent(({ productCode }) => {
		navigateAppOrH5({
			h5Url: '/goods/detail',
			params: {
				productCode
			},
			appPageType: 'GoodDetail',
			navigate
		});
	});
	return (
		<ScrollPage
			className="collection-goods-list"
			title={
				<CustomNavbar
					navBarRight={
						isLogin ? (
							<div
								className="header-right-cart"
								onClick={goCartList}
							>
								<div
									className="cart-num-area"
									style={
										window.location.href.includes(
											'/goods/collectionGoodsList'
										) && { backgroundColor: '#00000040' }
									}
								>
									<img
										style={{
											width: '0.16rem',
											height: '0.16rem'
										}}
										src="https://static-jp.theckb.com/static-asset/easy-h5/cart_white.svg"
										alt=""
									/>

									{cartOriginalData.cartNum > 0 ? (
										<div className="cart-num">
											{cartOriginalData.cartNum > 99
												? '99+'
												: cartOriginalData.cartNum}
										</div>
									) : null}
								</div>
							</div>
						) : null
					}
					isShowBackIcon={true}
					showBackHome={false}
				/>
			}
		>
			<div
				className="layout-style"
				style={{
					width: '100%'
				}}
			>
				<div
					className="collection-goods-header"
					style={{
						backgroundImage: `url(${collectionInfo?.data?.headUrl})`
					}}
				>
					<div className="collection-header-content">
						<div className="collection-header-content-name">
							{collectionInfo?.data?.moduleName}
						</div>
						<div className="collection-header-content-desc">
							{collectionInfo?.data?.moduleDesc}
						</div>
						<span
							className="collection-header-content-btn"
							onClick={() => {
								likeBtnClick(like);
							}}
						>
							{like ? (
								<Shoucanglike2
									color={'#FF5010'}
									width={'0.18rem'}
									height={'0.18rem'}
									style={{
										marginBottom: '-0.04rem',
										marginRight: '0.04rem'
									}}
								/>
							) : (
								<img
									src="https://static-jp.theckb.com/static-asset/easy-h5/Vector.svg"
									style={{
										width: '0.18rem',
										height: '0.18rem',
										marginBottom: '-0.04rem',
										marginRight: '0.04rem'
									}}
									alt=""
								/>
							)}

							{window._$m.t('收藏特辑')}
						</span>
					</div>
				</div>
				{noData && (
					<div className="collectionGoods-list-item-goods empty">
						<img
							src="https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_noData.svg"
							alt=""
						/>

						<div className="tips">
							{window._$m.t(
								'当前活动商品已下架，再看看其他商品吧～'
							)}
						</div>
					</div>
				)}

				{!!data.length && (
					<div className="collectionGoods-list-item-goods">
						{data.map((shopItem, index) => {
							return (
								<div
									className="collectionGoods-list-item-good"
									key={shopItem.productCode}
								>
									<ShoppItem
										favoriteFlag={shopItem.favoriteFlag}
										activityFlag={shopItem.activityFlag}
										onCollect={changeCollect}
										onClick={handleShoppItem}
										productLowestPrice={
											shopItem.productLowestPrice
										}
										productSellPriceJa={
											shopItem.activitySellPriceJpy ||
											shopItem.productLowestPrice
										}
										activitySellPriceJpy={
											shopItem.activitySellPriceJpy
										}
										highPriceShow={shopItem.highPriceShow}
										productPurchaseType={
											shopItem.productPurchaseType
										}
										title={shopItem.productTitle}
										productCode={shopItem.productCode}
										productMainImg={shopItem.productMainImg}
										status={shopItem.status}
									/>
								</div>
							);
						})}
					</div>
				)}

				{!noData && (
					<InfiniteScroll loadMore={loadMore} hasMore={hasMore} />
				)}
			</div>
		</ScrollPage>
	);
};
export default CollectionList;
