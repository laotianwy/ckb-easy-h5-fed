import { memo, useState } from 'react';
import { Grid } from 'antd-mobile';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { useAsyncEffect } from 'ahooks';
import { request } from '@/config/request';
import { ActivityAreaVO } from '@/service/easyMarket';
import Chakangengduomore from '@/common/icons/Chakangengduomore';
import styles from '../../index.module.scss';

// 本组件样式写死，不会随数据多少个而更改！！！
const HotView = () => {
	const navigate = useNavigate();
	const [viewListPos1, setViewListPos1] = useState<ActivityAreaVO>({});
	const [viewListPos2, setViewListPos2] = useState<ActivityAreaVO>({});
	const [viewListPos3, setViewListPos3] = useState<ActivityAreaVO>({});
	const [viewListPos4, setViewListPos4] = useState<ActivityAreaVO>({});
	const goShoppList = (currentViewData: ActivityAreaVO) => {
		navigate({
			pathname: '/goods/list',
			search: `?${createSearchParams({
				keyword: '',
				originSearch: String(false),
				noInput: String(true),
				pageTitle: String(currentViewData.moduleName || ''),
				hotId: String(currentViewData.id || ''),
				joinHot: String(true)
			})}`
		});
	};
	useAsyncEffect(async () => {
		const res = await request.easyMarket.activity.areaList({
			bizScene: '1',
			sourceChannel: '2'
		});
		if (res.success) {
			let hotView1 = {};
			let hotView2 = {};
			let hotView3 = {};
			let hotView4 = {};
			res.data?.forEach((item) => {
				if (item.position === 1) {
					hotView1 = {
						...item
					};
				} else if (item.position === 2) {
					hotView2 = {
						...item
					};
				} else if (item.position === 3) {
					hotView3 = {
						...item
					};
				} else if (item.position === 4) {
					hotView4 = {
						...item
					};
				}
			});
			setViewListPos1(hotView1);
			setViewListPos2(hotView2);
			setViewListPos3(hotView3);
			setViewListPos4(hotView4);
		}
	}, []);
	/** 活动专区，做个兜底的，数量只有三个的时候 */
	const getDefaultViewListPos1 = (length = 0) => {
		if (length === 3) {
			return (
				<Grid.Item>
					<div className={styles['default-hot-activity']}>
						<Chakangengduomore width={'0.2rem'} height={'0.2rem'} />
						<span
							style={{
								marginTop: '0.04rem'
							}}
						>
							{window._$m.t('查看更多')}
						</span>
					</div>
				</Grid.Item>
			);
		}
		return null;
	};

	// 爆款火拼 -> 点击搜索 -> 全量词搜

	// 售罄 线下商品 是否可以跳转
	// 商详： 运费东京都 不写死， 每一级没有展示主图，有就展示当前
	//	售罄页面显示：sku库位都为0，页面正常显示，不让加购
	return (
		<div id="hot-view" className={styles['category-area']}>
			{viewListPos1.topProductList &&
			viewListPos1.topProductList?.length > 0 ? (
				<div
					className={styles['areaTemplate']}
					onClick={(e) => goShoppList(viewListPos1)}
				>
					<div className={styles['top']}>
						<span className={styles.title}>
							{viewListPos1.moduleName}
						</span>
						<span className={styles.subTitle}>
							{viewListPos1.moduleDesc}
						</span>
					</div>
					<div className={styles.content}>
						<Grid columns={4} gap={8}>
							{viewListPos1.topProductList?.map((item, index) => {
								if (index > 3) return null;
								return (
									<Grid.Item key={index}>
										{item?.productMainImg ? (
											<img
												alt="hot"
												src={item?.productMainImg || ''}
												className={styles.activityItem}
											/>
										) : null}
									</Grid.Item>
								);
							})}
							{getDefaultViewListPos1(
								viewListPos1.topProductList?.length
							)}
						</Grid>
					</div>
				</div>
			) : null}
			{viewListPos2.topProductList &&
			viewListPos2.topProductList.length > 0 ? (
				<div
					className={styles['areaTemplate']}
					onClick={(e) => goShoppList(viewListPos2)}
				>
					<div className={styles['top']}>
						<span className={styles['title']}>
							{viewListPos2.moduleName}
						</span>
						<span className={styles['subTitle']}>
							{viewListPos2.moduleDesc}
						</span>
					</div>
					<div className={styles['content']}>
						<Grid columns={4} gap={8}>
							{viewListPos2.topProductList?.map((item, index) => {
								if (index > 3) return null;
								return (
									<Grid.Item key={index}>
										{item?.productMainImg ? (
											<img
												alt="hot"
												src={item?.productMainImg || ''}
												className={styles.activityItem}
											/>
										) : null}
									</Grid.Item>
								);
							})}
						</Grid>
					</div>
				</div>
			) : null}

			<Grid columns={2} gap={8}>
				{viewListPos3.topProductList &&
				viewListPos3.topProductList.length > 0 ? (
					<Grid.Item>
						<div
							className={styles['half-template']}
							onClick={(e) => goShoppList(viewListPos3)}
						>
							<div className={styles['top']}>
								<span className={styles['title']}>
									{viewListPos3.moduleName}
								</span>
								<div className={styles['sub-title']}>
									{viewListPos3.moduleDesc}
								</div>
							</div>
							<div className={styles['content']}>
								<Grid
									columns={2}
									gap={8}
									className={styles['grid']}
								>
									{viewListPos3.topProductList?.map(
										(item, index) => {
											if (index > 1) return null;
											return (
												<Grid.Item key={index}>
													{item?.productMainImg ? (
														<img
															alt="hot"
															src={
																item?.productMainImg ||
																''
															}
															className={
																styles[
																	'activity-item'
																]
															}
														/>
													) : null}
												</Grid.Item>
											);
										}
									)}
								</Grid>
							</div>
						</div>
					</Grid.Item>
				) : null}

				{viewListPos4.topProductList &&
				viewListPos4.topProductList.length > 0 ? (
					<Grid.Item>
						<div
							className={styles['half-template']}
							onClick={(e) => goShoppList(viewListPos4)}
						>
							<div className={styles['top']}>
								<span className={styles['title']}>
									{viewListPos4.moduleName}
								</span>
								<div className={styles['sub-title']}>
									{viewListPos4.moduleDesc}
								</div>
							</div>
							<div className={styles['content']}>
								<Grid
									columns={2}
									gap={8}
									className={styles['grid']}
								>
									{viewListPos4.topProductList?.map(
										(item, index) => {
											if (index > 1) return null;
											return (
												<Grid.Item key={index}>
													{item?.productMainImg ? (
														<img
															alt="hot"
															src={
																item?.productMainImg ||
																''
															}
															className={
																styles[
																	'activity-item'
																]
															}
														/>
													) : null}
												</Grid.Item>
											);
										}
									)}
								</Grid>
							</div>
						</div>
					</Grid.Item>
				) : null}
			</Grid>
		</div>
	);
};
export default memo(HotView);
