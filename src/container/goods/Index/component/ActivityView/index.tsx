/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-22 16:35:35
 * @LastEditors: huajian
 * @LastEditTime: 2024-01-10 17:44:25
 * @FilePath: \ckb-easy-h5-fed\src\container\goods\Index\component\ActivityView\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { memo, useState } from 'react';
import { Grid } from 'antd-mobile';
import classnames from 'classnames';
import { useMount } from 'ahooks';
import { SwiperItemProps } from '../../modules/IndexContent';
import styles from '../../index.module.scss';
import './index.scss';
interface ActivityViewProps {
	activityList?: SwiperItemProps[][];
}
const ActivityView = (props: ActivityViewProps) => {
	const { activityList = [] } = props;
	const [expand, setExpand] = useState(false);
	const [rootFontSize, setRootFontSize] = useState<number>(0);
	const clickTraggle = () => {
		setExpand(!expand);
	};
	const openOutLink = (jumpUrl?: string) => {
		if (typeof jumpUrl === 'string' && jumpUrl.trim().length > 0) {
			if (!jumpUrl.includes('m.3fbox.com/goods/list')) {
				window.open(jumpUrl);
				return;
			}
			window.open(jumpUrl);
			window.sessionStorage.setItem('isConfigureLinkJump', 'true');
		}
	};
	const getRootFontSize = () => {
		const rootPX = Number(
			document.documentElement.style.fontSize.split('px')[0]
		);
		setRootFontSize(rootPX);
	};
	useMount(() => {
		getRootFontSize();
	});
	const getView = () => {
		if (expand) {
			return (
				<>
					{activityList.map((activityItem, index) => (
						<Grid columns={2} key={index} gap={rootFontSize * 0.08}>
							{activityItem.map((activity, childIndex) => (
								<Grid.Item
									key={childIndex}
									onClick={(e) =>
										openOutLink(activity.jumpUrl)
									}
								>
									<img
										alt="activity-img"
										src={activity.picUrl}
										className={styles['activityItem']}
									/>
								</Grid.Item>
							))}
						</Grid>
					))}
				</>
			);
		}
		if (activityList.length > 0) {
			return (
				<Grid columns={2} gap={rootFontSize * 0.08}>
					{activityList[0].map((item) => (
						<Grid.Item
							key={item.picUrl}
							onClick={(e) => openOutLink(item.jumpUrl)}
						>
							<img
								alt="activity-img"
								src={item.picUrl}
								className={styles['activityItem']}
							/>
						</Grid.Item>
					))}
				</Grid>
			);
		}
		return null;
	};
	return (
		<>
			{activityList.length > 0 ? (
				<div
					id="activity"
					className={classnames(
						activityList.length > 1
							? styles['activity']
							: styles['no-expand']
					)}
				>
					{getView()}
					{activityList.length > 1 ? (
						<div
							className={styles['expandArea']}
							onClick={clickTraggle}
						>
							<span
								style={{
									fontSize: '.12rem',
									color: '#FF5010'
								}}
							>
								{expand
									? window._$m.t('收起')
									: window._$m.t('展开更多')}
							</span>
							<img
								style={{
									width: '.16rem',
									height: '.16rem',
									marginLeft: '.04rem'
								}}
								className={classnames({
									'activity-rotate': expand
								})}
								alt="down"
								src="https://static-s.theckb.com/BusinessMarket/Easy/H5/arrow-down.png"
							/>
						</div>
					) : null}
				</div>
			) : null}
		</>
	);
};
export default memo(ActivityView);
