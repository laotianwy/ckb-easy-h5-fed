import { memo, useState } from 'react';
import { Mask, Collapse } from 'antd-mobile';
import { useNavigate } from 'react-router';
import { useAsyncEffect, useRequest } from 'ahooks';
import { useAtomValue } from 'jotai';
import { request } from '@/config/request';
import { User } from '@/Atoms';
import { OrderDTO } from '@/service/easyOrder';
import { formatDateCN2JP } from '@/utils';
import styles from './index.module.scss';
const ModalOrderCancel = () => {
	const [visible, setVisible] = useState(false);
	const [reason, setReason] = useState('');
	const [cancenlOrderList, setCancenlOrderList] = useState<OrderDTO[]>([]);
	const navigate = useNavigate();
	const isLogin = useAtomValue(User.isLogin);
	const { runAsync: showPopCancelApi } = useRequest(
		request.easyOrder.orderSearch.orderCancelPopups,
		{
			manual: true
		}
	);
	const { runAsync: lookOrderApi } = useRequest(
		request.easyOrder.orderSearch.orderCancelPopupsConfirm,
		{
			manual: true
		}
	);
	const lookOrder = async () => {
		const res = await lookOrderApi({
			confirmType: 1
		});
		if (res.success) {
			setVisible(false);
			if (cancenlOrderList.length === 1) {
				navigate(
					`/order/detail?orderNo=${cancenlOrderList[0]['orderNo']}`
				);
				return;
			}
			navigate('/order/list');
		}
	};
	useAsyncEffect(async () => {
		if (isLogin) {
			const res = await showPopCancelApi();
			if (res.success) {
				setReason(res.data?.cancelReason || '');
				const cancelOrderListData = res.data?.orderList ?? [];
				setCancenlOrderList(cancelOrderListData);
				if (cancelOrderListData.length > 0) {
					setVisible(true);
				}
			}
		}
	}, [isLogin]);
	const laterBack = async () => {
		const res = await lookOrderApi({
			confirmType: 2
		});
		if (res.success) {
			setVisible(false);
		}
	};
	return (
		<Mask
			visible={visible}
			style={{
				'--z-index': '10000'
			}}
		>
			<div className={styles.wrap}>
				<img
					className={styles.img}
					src="https://static-s.theckb.com/BusinessMarket/Easy/H5/order-cancel.png"
					alt=""
				/>
				<div className={styles['order-content']}>
					<div className={styles.tips}>
						<div className={styles.title}>
							{window._$m.t(
								'您有{{num}}笔订单被取消，取消原因：{{reason}}',
								{
									data: {
										num: cancenlOrderList.length,
										reason
									}
								}
							)}
						</div>
						<div className={styles['sub-title']}>
							{window._$m.t('退款事宜可与客服进行联系确认')}
						</div>
					</div>
					<div className={styles.content}>
						{cancenlOrderList.length === 1 ? (
							<div className={styles['sing-order']}>
								<div className={styles.row}>
									<span className={styles.left}>
										{window._$m.t('订单编号')}
									</span>
									<span className={styles.right}>
										{cancenlOrderList[0]['orderNo']}
									</span>
								</div>
								<div
									className={styles.row}
									style={{
										marginTop: '.1rem'
									}}
								>
									<span className={styles.left}>
										{window._$m.t('下单时间')}
									</span>
									<span className={styles.right}>
										{formatDateCN2JP(
											cancenlOrderList[0]['createdTime']
										)}
									</span>
								</div>
								<div
									className={styles.row}
									style={{
										marginTop: '.1rem'
									}}
								>
									<span className={styles.left}>
										{window._$m.t('商品图片')}
									</span>
									<div className={styles['image-list']}>
										{(
											cancenlOrderList[0][
												'orderItemList'
											] ?? []
										).map((item, index) => {
											if (index > 3) return null;
											return (
												<div
													key={item.productImg}
													className={
														styles['image-count']
													}
												>
													<img
														src={item.productImg}
														alt=""
													/>
													<span
														className={
															styles['count']
														}
													>
														x{item.productQuantity}
													</span>
												</div>
											);
										})}
									</div>
								</div>
							</div>
						) : (
							<div className={styles['many-order']}>
								<Collapse accordion>
									{cancenlOrderList.map((orderItem) => (
										<Collapse.Panel
											key={String(orderItem.orderNo)}
											style={
												{
													'--border-inner': 'none',
													marginBottom: '.05rem'
												} as any
											}
											title={
												<div
													className={
														styles['order-wrap']
													}
												>
													<div
														className={styles.left}
													>
														{window._$m.t(
															'订单编号'
														)}
													</div>
													<div
														className={styles.right}
													>
														{orderItem['orderNo']}
													</div>
												</div>
											}
										>
											<div
												className={
													styles['order-expand']
												}
											>
												<div
													className={
														styles['many-row']
													}
												>
													<span
														className={styles.left}
													>
														{window._$m.t(
															'下单时间'
														)}
													</span>
													<span
														className={styles.right}
													>
														{formatDateCN2JP(
															orderItem.createdTime
														)}
													</span>
												</div>
												<div
													className={
														styles['many-row']
													}
												>
													<span
														className={styles.left}
													>
														{window._$m.t(
															'商品图片'
														)}
													</span>
													<div
														className={
															styles['image-list']
														}
													>
														{(
															orderItem.orderItemList ??
															[]
														).map(
															(
																orderItemListItem,
																orderItemIndex
															) => {
																if (
																	orderItemIndex >
																	3
																)
																	return null;
																return (
																	<div
																		key={
																			orderItemListItem.productImg
																		}
																		className={
																			styles[
																				'image-count'
																			]
																		}
																	>
																		<img
																			src={
																				orderItemListItem.productImg
																			}
																			alt=""
																		/>
																		<span
																			className={
																				styles[
																					'count'
																				]
																			}
																		>
																			x
																			{
																				orderItemListItem.productQuantity
																			}
																		</span>
																	</div>
																);
															}
														)}
													</div>
												</div>
											</div>
										</Collapse.Panel>
									))}
								</Collapse>
							</div>
						)}
					</div>
					<div className={styles.footer}>
						<div className={styles.btn} onClick={laterBack}>
							{window._$m.t('稍后查看')}
						</div>
						<div className={styles.btn} onClick={lookOrder}>
							{window._$m.t('立即查看')}
						</div>
					</div>
				</div>
			</div>
		</Mask>
	);
};

/** 订单取消提示 */
export default memo(ModalOrderCancel);
