/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-22 16:15:11
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-30 17:45:47
 * @FilePath: \ckb-easy-h5-fed\src\container\goods\Index\component\StickyView\index.tsx
 * @Description: 顶部吸顶位置
 */
import { useNavigate } from 'react-router-dom';
import { memo } from 'react';
import { CSSTransition } from 'react-transition-group';
import SearchInput from '@/component/SearchInput';
import styles from '../../index.module.scss';
import './index.scss';
interface StickyViewProps {
	show: boolean;
}
const StickyView = (props: StickyViewProps) => {
	const { show } = props;
	const navigate = useNavigate();
	const goSearch = () => {
		navigate('/goods/search');
	};
	return (
		<CSSTransition in={show} timeout={200} classNames="fade">
			<div id="search" className={styles['search']}>
				<CSSTransition in={show} timeout={200} classNames="logo-fade">
					<div className={styles['title']}>
						<div className={styles['image']} />
					</div>
				</CSSTransition>

				<CSSTransition
					in={show}
					timeout={200}
					classNames="search-input-fade"
				>
					<SearchInput
						value={window._$m.t('请输入商品名称')}
						onClick={goSearch}
						// isCamera={true}
						isText={true}
					/>
				</CSSTransition>
			</div>
		</CSSTransition>
	);
};

/** 顶部吸顶位置 */
export default memo(StickyView);
