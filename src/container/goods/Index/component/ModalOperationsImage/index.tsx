/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-19 17:54:19
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2023-10-31 11:55:48
 * @FilePath: \ckb-easy-h5-fed\src\container\goods\Index\component\ModalOperationsImage\index.tsx
 * @Description: 运营图弹框
 */
import { memo, useState } from 'react';
import { Mask } from 'antd-mobile';
import { useAsyncEffect } from 'ahooks';
import { request } from '@/config/request';
import CancelCircle from '@/common/icons/CancelCircle';
import styles from './index.module.scss';
const ACTIVITY_OPERA_MODAL_URL = 'ACTIVITY_OPERA_MODAL_URL';
const ModalOperationsImage = () => {
	const [visible, setVisible] = useState(false);
	const [imageUrl, setImageUrl] = useState('');
	const [jumpUrl, setJumpUrl] = useState('');
	useAsyncEffect(async () => {
		const res = await request.easyMarket.popup.show({
			bizScene: '1',
			sourceChannel: '2'
		});
		if (res.success && res.data) {
			if (
				localStorage.getItem(ACTIVITY_OPERA_MODAL_URL) !==
				res.data?.picUrl
			) {
				setVisible(true);
				setImageUrl(res.data?.picUrl || '');
				setJumpUrl(res.data?.jumpUrl || '');
			}
		}
	}, []);
	const handleClickJump = () => {
		if (typeof jumpUrl === 'string' && jumpUrl.trim().length > 0) {
			window.open(jumpUrl);
		}
	};
	const handleClickClose = () => {
		localStorage.setItem(ACTIVITY_OPERA_MODAL_URL, imageUrl);
		setVisible(false);
	};
	return (
		<Mask
			visible={visible}
			className={styles.mask}
			style={{
				'--z-index': '1010'
			}}
		>
			<div className={styles.modal}>
				<div className={styles.close} onClick={handleClickClose}>
					<CancelCircle />
				</div>
				<div className={styles.content} onClick={handleClickJump}>
					<img src={imageUrl} alt="" />
				</div>
			</div>
		</Mask>
	);
};

/** 运营图弹框 */
export default memo(ModalOperationsImage);
