/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-22 16:27:54
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-13 10:06:53
 * @FilePath: /ckb-easy-h5-fed/src/container/goods/Index/component/BannerView/index.tsx
 * @Description: 首页轮播图列表
 */
import { memo } from 'react';
import { Swiper } from 'antd-mobile';
import classNames from 'classnames';
import { SwiperItemProps } from '../../modules/IndexContent';
import './index.scss';
import styles from '../../index.module.scss';
interface BannerViewProps {
	swiperList?: SwiperItemProps[];
}
const BannerView = (props: BannerViewProps) => {
	const swiperList = [
		{
			picUrl: 'https://ckb-service-test.oss-cn-hangzhou.aliyuncs.com/sale_manage_1698811513553.png',
			jumpUrl: 'http://nav123.sniffgroup.com:8080/#/'
		},
		{
			picUrl: 'https://ckb-service-test.oss-cn-hangzhou.aliyuncs.com/sale_manage_1698811513553.png',
			jumpUrl: 'http://nav123.sniffgroup.com:8080/#/'
		},
		{
			picUrl: 'https://ckb-service-test.oss-cn-hangzhou.aliyuncs.com/sale_manage_1698811513553.png',
			jumpUrl: 'http://nav123.sniffgroup.com:8080/#/'
		}
	];
	return (
		<>
			{swiperList.length !== 0 ? (
				<div id="swiper" className={styles['swiper']}>
					<Swiper
						loop
						autoplay
						indicator={(_, current) => {
							// 当数量为1的时候不需要展示切换按钮
							if (swiperList.length === 1) {
								return null;
							}
							return (
								<div className="customIndicator">
									{swiperList.map((_, swiperIndex) => (
										<div
											key={swiperIndex}
											className={classNames({
												'customIndicator-item': true,
												active: swiperIndex === current
											})}
										/>
									))}
								</div>
							);
						}}
						style={{
							'--height': '1.2rem'
						}}
					>
						{swiperList.map((item) => {
							return (
								<Swiper.Item key={item.picUrl}>
									<img
										alt="banner"
										src={item.picUrl}
										style={{
											width: '100%',
											height: '1.2rem'
										}}
										onClick={() => {
											if (
												typeof item.jumpUrl ===
													'string' &&
												item.jumpUrl.trim().length > 0
											) {
												window.open(item.jumpUrl);
											}
										}}
									/>
								</Swiper.Item>
							);
						})}
					</Swiper>
				</div>
			) : null}
		</>
	);
};
export default memo(BannerView);
