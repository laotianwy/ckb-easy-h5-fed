/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-22 16:18:23
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2023-11-01 14:14:58
 * @FilePath: \ckb-easy-h5-fed\src\container\goods\Index\component\OperationsView\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { memo, useState } from 'react';
import dayjs from 'dayjs';
import styles from '../../index.module.scss';
const LOCAL_CHACHE_CLOSE_NAME = 'close-activity-modal';
interface OperationViewProps {
	picUrl?: string;
	jumpUrl?: string;
}
const OperationView = (props: OperationViewProps) => {
	const { picUrl = '', jumpUrl } = props;
	/** 如果今天关闭了。那么今天不再展示 */
	const [visible, setVisible] = useState(
		() =>
			localStorage.getItem(LOCAL_CHACHE_CLOSE_NAME) !==
			dayjs(new Date()).format('YYYY/MM/DD')
	);

	// 关闭之后 当天不显示
	const closeOperaModal = () => {
		setVisible(false);
		localStorage.setItem(
			LOCAL_CHACHE_CLOSE_NAME,
			dayjs(new Date()).format('YYYY/MM/DD')
		);
	};
	const jumpLink = () => {
		if (typeof jumpUrl === 'string' && jumpUrl.trim().length > 0) {
			window.open(jumpUrl);
		}
	};
	return (
		<>
			{visible && picUrl.length > 0 ? (
				<div id="operations" className={styles['operations']}>
					<img
						alt="close"
						onClick={closeOperaModal}
						className={styles['close']}
						src="https://static-s.theckb.com/BusinessMarket/Easy/H5/activity-close.png"
					/>
					<img
						alt="search"
						style={{
							width: '100%',
							height: '100%'
						}}
						src={picUrl}
						onClick={jumpLink}
					/>
				</div>
			) : null}
		</>
	);
};
export default memo(OperationView);
