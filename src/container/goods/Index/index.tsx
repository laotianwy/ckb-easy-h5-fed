/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-13 15:26:53
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-30 10:07:06
 * @FilePath: /ckb-easy-h5-fed/src/container/goods/Index/index.tsx
 * @Description: 首页
 */
import { memo, useRef, useState } from 'react';
import { useAtomValue } from 'jotai';
import { useMount } from 'ahooks';
import queryString from 'query-string';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import CustomTabbar from '@/common/Layout/CustomTabbar';
import { debounce } from '@/utils';
import BackToTop from '@/component/BackToTop';
import { User } from '@/Atoms';
import ModalOrderCancel from './component/ModalOrderCancel';
import ModalOperationsImage from './component/ModalOperationsImage';
import StickyView from './component/StickyView';
import IndexContent from './modules/IndexContent';
import IndexList from './modules/IndexList';
import styles from './index.module.scss';
const SCROLL_TO_VIEW_ID = 'goods-index-scroll-view';
const LISTEN_VIEW_ID = 'goods-index';
const Index = () => {
	const layoutContentRef = useRef(null);
	const [animationVisible, setAnimationVisible] = useState(false);
	const [categoryAnimationShow, setCategoryAnimationShow] = useState(false);
	const enableIndex = useAtomValue(User.enableIndex);
	const splitStringPX = (str: string) => {
		return Number(str.split('px')[0]);
	};
	const getDomHeight = (domRef: HTMLElement): number => {
		if (!domRef) return 0;
		const domHeight =
			domRef?.offsetHeight ??
			0 + splitStringPX(getComputedStyle(domRef!, null).marginTop);
		return domHeight;
	};
	const bindHandleScroll = () => {
		const searchView = document.getElementById('search');
		const operationsView = document.getElementById('operations');
		const swiperView = document.getElementById('swiper');
		const activityView = document.getElementById('activity');
		const hotView = document.getElementById('hot-view');
		const categoryScrollHeight =
			getDomHeight(searchView!) +
			getDomHeight(operationsView!) +
			getDomHeight(swiperView!) +
			getDomHeight(activityView!) +
			getDomHeight(hotView!);

		// 监听活动触发search的动画
		const currentScrollRef = layoutContentRef.current as any;
		if (currentScrollRef?.scrollTop === 0) {
			if (animationVisible) {
				setAnimationVisible(false);
			}
		} else {
			if (!animationVisible) {
				setAnimationVisible(true);
			}
		}
		if (currentScrollRef?.scrollTop >= categoryScrollHeight) {
			if (!categoryAnimationShow) {
				setCategoryAnimationShow(true);
			}
		} else {
			if (categoryAnimationShow) {
				setCategoryAnimationShow(false);
			}
		}
	};
	const gobackProcurement = () => {
		const { channel } = queryString.parse(window.location.search);
		const search = channel ? `?channel=${channel}` : '';
		window.location.replace('https://m.3fbox.com' + search);
	};
	useMount(() => {
		// 清除信息
		sessionStorage.removeItem('isFromOtherDomain');
	});
	return (
		<div
			id={LISTEN_VIEW_ID}
			className="layout-style"
			onScroll={debounce(bindHandleScroll, 30)}
			ref={layoutContentRef}
		>
			<CustomNavbar
				isShowBackIcon={false}
				title={window._$m.t('CKB X 1688直营商城')}
			/>
			<div className="layout-content">
				<div className={styles.root} id={SCROLL_TO_VIEW_ID}>
					{enableIndex ? (
						<>
							<ModalOrderCancel />
							<ModalOperationsImage />

							<StickyView show={animationVisible} />
							{/* 首页内容区域 */}
							<IndexContent />
							{/* 首页分类列表区域 */}
							<IndexList showAnimation={categoryAnimationShow} />
						</>
					) : (
						<div className={styles['no-access']}>
							<img
								src="https://static-s.theckb.com/BusinessMarket/Easy/H5/no-access.png"
								alt="no-access"
							/>
							<span className={styles['text']}>
								{window._$m.t('当前地区，此功能暂未开放')}
							</span>
							<div
								className={styles['btn']}
								onClick={gobackProcurement}
							>
								{window._$m.t('返回代采')}
							</div>
						</div>
					)}
				</div>
			</div>

			{enableIndex ? (
				<>
					<BackToTop
						listenScrollId={LISTEN_VIEW_ID}
						scrollViewIdName={SCROLL_TO_VIEW_ID}
					/>
					{/* <CustomTabbar isFixed /> */}
				</>
			) : null}
		</div>
	);
};
export default memo(Index);
