/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-22 16:24:18
 * @LastEditors: yusha
 * @LastEditTime: 2023-12-12 16:52:39
 * @FilePath: \ckb-easy-h5-fed\src\container\goods\Index\modules\IndexContent\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { memo, useState } from 'react';
import { useAsyncEffect } from 'ahooks';
import { request } from '@/config/request';
import { groupArray } from '@/utils';
import OperationsView from '../../component/OperationsView';
import BannerView from '../../component/BannerView';
import ActivityView from '../../component/ActivityView';
import HotView from '../../component/HotView';
export interface SwiperItemProps {
	picUrl?: string;
	jumpUrl?: string;
}
const IndexContent = () => {
	const [imagePosi1, setImagePosi1] = useState<SwiperItemProps>({});
	const [imagePosi2, setImagePosi2] = useState<SwiperItemProps[]>([]);
	const [imagePosi3, setImagePosi3] = useState<SwiperItemProps[][]>([]);
	useAsyncEffect(async () => {
		// const res = await request.easyMarket.banner.show({
		// 	sourceChannel: '2',
		// 	bizScene: '1'
		// });
		// if (res.success) {
		// 	let image1: SwiperItemProps = {};
		// 	const image2: SwiperItemProps[] = [];
		// 	const image3: SwiperItemProps[] = [];
		// 	res.data?.forEach((item) => {
		// 		if (item.placeType === 1) {
		// 			image1 = {
		// 				picUrl: item.picUrl,
		// 				jumpUrl: item.jumpUrl
		// 			};
		// 		}
		// 		if (item.placeType === 2) {
		// 			image2.push({
		// 				picUrl: item.picUrl,
		// 				jumpUrl: item.jumpUrl
		// 			});
		// 		}
		// 		if (item.placeType === 3) {
		// 			image3.push({
		// 				picUrl: item.picUrl,
		// 				jumpUrl: item.jumpUrl
		// 			});
		// 		}
		// 	});
		// 	setImagePosi1(image1);
		// 	setImagePosi2(image2);
		// 	setImagePosi3(groupArray(image3, 4));
		// }
	}, []);
	return (
		<div
			style={{
				width: '100%',
				overflowX: 'hidden',
				padding: '0 .16rem',
				boxSizing: 'border-box'
			}}
		>
			<div
				style={{
					width: '100%',
					height: '.4rem'
				}}
			/>
			{/* 运营位 */}
			<OperationsView
				picUrl={imagePosi1.picUrl}
				jumpUrl={imagePosi1.jumpUrl}
			/>
			{/* banner展示 */}
			<BannerView swiperList={imagePosi2} />
			{/* 活动区域 */}
			<ActivityView activityList={imagePosi3} />
			{/* 爆款活动区域 */}
			<HotView />
		</div>
	);
};
export default memo(IndexContent);
