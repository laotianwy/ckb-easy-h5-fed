/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2023-10-22 16:32:15
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-23 14:12:49
 * @FilePath: /ckb-easy-h5-fed/src/container/goods/Index/modules/IndexList/index.tsx
 * @Description: 分类列表
 */

import {
	memo,
	useState,
	useRef,
	CSSProperties,
	useEffect,
	useMemo
} from 'react';
import { Grid, JumboTabs, List, InfiniteScroll, Toast } from 'antd-mobile';
import {
	List as VirtualizedList,
	AutoSizer,
	WindowScroller
} from 'react-virtualized';
import { useNavigate } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import { useAtomValue } from 'jotai';
import { omit } from 'ramda';
import { useRequest } from 'ahooks';
import { flushSync } from 'react-dom';
import { groupArray, navigateAppOrH5 } from '@/utils';
import { request } from '@/config/request';
import { useEvent } from '@/utils/hooks/useEvent';
import ShoppItem, { ShoppInfoProps } from '@/component/ShoppItem';
import {
	AddProductFavoriteReqDTO,
	ProductKeyWordSearchReqDTO
} from '@/service/easyGoods';
import { EnumProductPurchaseType } from '@/common/Enums';
import styles from '../../index.module.scss';
import './index.scss';
interface VirtualizedListProps {
	index: number;
	key: string;
	style: CSSProperties;
}
interface CateItemProps {
	/** 分类中文名 */
	cateNameZh?: string;
	/** 根据站点获取当前显示分类的名字 */
	cateNameNoZh?: string;
	/** 分类id */
	productCategoryFrontendId?: number;
}
interface IndexListProps {
	/** 是否启用动画 */
	showAnimation: boolean;
}
const IndexList = (props: IndexListProps) => {
	const { showAnimation } = props;
	const navigate = useNavigate();
	/** 类目数据 */
	const [categoryList, setCategoryList] = useState<CateItemProps[]>([]);
	/** 选中分类id */
	const [defaultActiveCateId, setDefaultActiveCateId] = useState(-1);
	/** 页码 */
	const [page, setPage] = useState(1);
	const [pageSize, setPageSize] = useState(20);
	/** 商品列表 */
	const [data, setData] = useState<ShoppInfoProps[]>([]);
	/** 是否加载更多 */
	const [hasMore, setHasMore] = useState(false);
	/** 前10个特殊商品营销数据 */
	const [spec10ShoppList, setSpec10ShoppList] = useState<ShoppInfoProps[]>(
		[]
	);
	const scrollRef = useRef<HTMLDivElement>(null);
	const [rootFontSize, setRootFontSize] = useState<number>(0);

	// 是否请求商品列表。用于虚拟列表多次请求问题
	const httpGoodsRequest = useRef(false);
	/** 收藏取消 */
	const { runAsync: postCancelCollect } = useRequest(
		request.easyGoods.favoriteProduct.cancel,
		{
			manual: true
		}
	);

	/** 收藏 */
	const { runAsync: postAddCollect } = useRequest(
		request.easyGoods.favoriteProduct.postFavoriteProduct,
		{
			manual: true
		}
	);
	/** 点击商品跳转方法 */
	const handleShoppItem = useEvent(({ productCode }) => {
		navigateAppOrH5({
			h5Url: '/goods/detail',
			params: { productCode },
			appPageType: 'GoodDetail',
			navigate
		});
	});
	/** 收藏切换 */
	const changeCollect = async (code?: string) => {
		const collectItem = data.find(
			(shoppItem) => shoppItem.productCode === code
		);
		if (!collectItem) return;
		const favoriteFlagBoolean = Boolean(collectItem['favoriteFlag']);
		// 取消收藏
		if (favoriteFlagBoolean) {
			const res = await postCancelCollect({
				productCode: code
			});
			if (!res.success) return;
			const newList = data.map((shoppItem) => {
				if (shoppItem.productCode !== code) return shoppItem;
				return {
					...shoppItem,
					favoriteFlag: 0
				};
			});
			flushSync(() => {
				setData(newList);
			});
			Toast.show({
				content: window._$m.t('取消收藏成功')
			});
			return;
		}
		// 收藏
		const { productCode, productMainImg, productSellPriceJa, title } =
			collectItem;
		const payload: AddProductFavoriteReqDTO = {
			productCode,
			productMainImg,
			productSellPrice: productSellPriceJa,
			productTitle: title
		};
		const res = await postAddCollect({
			...payload,
			favoriteProductType: 0
		});
		if (!res.success) return;
		const newList = data.map((shoppItem) => {
			if (shoppItem.productCode !== code) return shoppItem;
			return {
				...shoppItem,
				favoriteFlag: 1,
				openFavoriteFlagModal: true
			};
		});
		flushSync(() => {
			setData(newList);
		});
		Toast.show({
			content: window._$m.t('收藏成功')
		});
	};
	const rowRenderer = ({ index, key, style }: VirtualizedListProps) => {
		const rowShoppItem = SEARCH_GOOD_LIST[index];
		if (!rowShoppItem) return;
		return (
			<div key={key} className={styles['shopp-view']} style={style}>
				<Grid columns={2} gap={rootFontSize * 0.08}>
					{rowShoppItem.map((item) => {
						return (
							<Grid.Item key={item.productCode}>
								<ShoppItem
									onCollect={changeCollect}
									onClick={handleShoppItem}
									sellPrice={item.sellPrice}
									productSellPriceJa={item.productSellPriceJa}
									productPurchaseType={
										item.productPurchaseType
									}
									title={item.title}
									productCode={item.productCode}
									productMainImg={item.productMainImg}
									status={item.status}
									showGoBackPrice={item.showGoBackPrice}
									favoriteFlag={item.favoriteFlag}
								/>
							</Grid.Item>
						);
					})}
				</Grid>
			</div>
		);
	};
	const loadMore = async () => {
		if (httpGoodsRequest.current) {
			return;
		}
		console.log('loadMoresss');
		httpGoodsRequest.current = true;
		setPage(page + 1);
	};
	const getRootFontSize = () => {
		const rootPX = Number(
			document.documentElement.style.fontSize.split('px')[0]
		);
		setRootFontSize(rootPX);
	};
	const getTreeList = async () => {
		const res = await request.easyGoods.productCategoryFrontend.tree({});
		if (res.success) {
			const newCateList: CateItemProps[] = [];
			const h5CateList = res.data || [];
			h5CateList?.forEach((cateItem) => {
				newCateList.push({
					cateNameZh: cateItem.cateNameZh,
					cateNameNoZh: cateItem.cateNameJp,
					productCategoryFrontendId: Number(
						cateItem.productCategoryFrontendId
					)
				});
			});
			// TODO
			newCateList.unshift({
				cateNameZh: window._$m.t('全部'),
				cateNameNoZh: window._$m.t('全て'),
				productCategoryFrontendId: 0
			});
			setDefaultActiveCateId(
				Number(newCateList[0]?.productCategoryFrontendId || 0)
			);
			setCategoryList(newCateList);
		}
	};
	useEffect(() => {
		/** 获取根组件fs */
		getRootFontSize();
		/** 获取分类列表 */
		getTreeList();
	}, []);
	const tabChange = (key: string) => {
		setDefaultActiveCateId(Number(key));
		setPage(1);
	};
	useEffect(() => {
		/**
     * 					
    	降序
    	descendOrder?: boolean;
    	结束价格
    	endPrice?: number;
    	pageNum?: number;
    	pageSize?: number;
    	前端类目编码
    	productCategoryFrontendId?: string;
    	商品标题
    	productTitle?: string;
    	排序字段:1 价格(默认) 2 时间
    	sortType?: string;
    	startIndex?: number;
    	开始价格
    	startPrice?: number;
     */
		if (defaultActiveCateId !== -1) {
			getShoppList();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [defaultActiveCateId, page]);
	const getShoppList = async () => {
		let spec10ShoppList: ShoppInfoProps[] = [];
		// 如果分类id是全部。并且page是第一页。那么请求10条营销商品
		if (defaultActiveCateId === 0 && page === 1) {
			const res = await request.easyMarket.recommend.show({
				bizScene: '1',
				sourceChannel: '2'
			});
			if (res.success) {
				if (res?.data) {
					res.data = res.data.filter(
						(item) => item.status === 1 || item.status === 2
					);
				}
				const newList = (res?.data ?? []).map((item) => ({
					...item,
					/**
					 * 采购类型大写,代采 1, 直采：0
					 * @format int32
					 */
					productPurchaseType: 0,
					/** 商品最低价 */
					productSellPriceJa: item.productLowestPrice,
					/** 商品标题 */
					title: item.productTitle,
					/** 商品SPU	 */
					productCode: item.productCode,
					/** 商品主图 */
					productMainImg: item.productMainImg,
					/** 商品状态 */
					status: item.availableQuantity,
					/** 是否展示及时返金 */
					showGoBackPrice: true
				}));
				spec10ShoppList = (newList as ShoppInfoProps[]).slice(0, 10);
			}
		}
		setSpec10ShoppList(spec10ShoppList);
		// 请求商品列表
		let searchListPayload: ProductKeyWordSearchReqDTO = {
			pageNum: page,
			pageSize,
			productCategoryFrontendId: defaultActiveCateId
		};
		if (defaultActiveCateId === 0) {
			// 如果是全部，那么删除该字段
			searchListPayload = omit(
				['productCategoryFrontendId'],
				searchListPayload
			);
		}
		const res =
			await request.easyGoods.product.searchKeyword(searchListPayload);
		if (res.success) {
			if (res?.data) {
				res.data.records = (res.data?.records ?? []).filter((item) => {
					// 如果是代采商品，直接返回
					if (
						item.productPurchaseType ===
						EnumProductPurchaseType.PROXY
					) {
						return true;
					}
					return item.status === 1 || item.status === 2;
				});
			}
			const cateShoppList: ShoppInfoProps[] = (
				res.data?.records ?? []
			).map((item) => ({
				...item,
				/**
				 * 采购类型大写,代采 1, 直采：0
				 * @format int32
				 */
				// productPurchaseType: 0,
				/** 商品最低价 */
				productSellPriceJa: item.productSellPriceJa,
				sellPrice: item.sellPrice,
				/** 商品标题 */
				title: item.productTitle,
				/** 商品SPU	 */
				productCode: item.productCode,
				/** 商品主图 */
				productMainImg: item.productMainImg,
				/** 商品状态 */
				status: item.availableQuantity,
				/** 是否展示及时返金 */
				showGoBackPrice: false
			})) as ShoppInfoProps[];
			let activeList: any = [];
			if (page !== 1) {
				setData([...data, ...cateShoppList]);
				setHasMore(cateShoppList.length > 0);
				httpGoodsRequest.current = false;
				return;
			}
			// 当类目选择全部才需要请求下面的数据
			if (defaultActiveCateId === 0 || defaultActiveCateId === -1) {
				const res1 = await request.easyMarket.activity.areaList({
					bizScene: '1',
					sourceChannel: '2'
				});
				if (
					res1.success &&
					res1.data?.length &&
					res1.data?.length > 3
				) {
					const res2 =
						await request.easyMarket.activity.areaMoreProduct({
							pageNum: 1,
							pageSize: 100,
							areaId: res1.data[1].id,
							needTopProduct: false
						});
					res2.data?.records?.length &&
						(activeList = activeList.concat(
							res2.data.records.map((item) => {
								return {
									...item,
									productSellPriceJa: item.productLowestPrice,
									title: item.productTitle
								};
							})
						));
					const res3 =
						await request.easyMarket.activity.areaMoreProduct({
							pageNum: 1,
							pageSize: 100,
							areaId: res1.data[2].id,
							needTopProduct: false
						});
					res3.data?.records?.length &&
						(activeList = activeList.concat(
							res3.data.records.map((item) => {
								return {
									...item,
									productSellPriceJa: item.productLowestPrice,
									title: item.productTitle
								};
							})
						));
				}
			}
			if (defaultActiveCateId === 0) {
				setData([...activeList, ...cateShoppList]);
			} else {
				setData(cateShoppList);
			}
			setHasMore(cateShoppList.length > 0);
			httpGoodsRequest.current = false;
		}
	};
	const SEARCH_GOOD_LIST = useMemo(() => {
		return groupArray(data, 2);
	}, [data]);
	return (
		<>
			{/* 分类列表 */}
			{defaultActiveCateId !== -1 ? (
				<CSSTransition
					in={showAnimation}
					timeout={50}
					classNames="cate-header-fade"
				>
					<div className={styles['cate-list']}>
						<JumboTabs
							onChange={tabChange}
							defaultActiveKey={String(defaultActiveCateId)}
							// defaultActiveKey={'1'}
						>
							{categoryList.map((cateItem) => (
								<JumboTabs.Tab
									title=""
									key={String(
										cateItem.productCategoryFrontendId
									)}
									style={
										{
											'--adm-color-primary': '#F1E5E3',
											'--adm-color-text-light-solid':
												'#FF5010',
											'--adm-color-fill-content':
												'#F0F2F5'
										} as any
									}
									description={cateItem.cateNameNoZh}
								/>
							))}
						</JumboTabs>
					</div>
				</CSSTransition>
			) : null}

			{/* 全部分类的10条商品、如果点击别的分类那么不显示 */}
			{spec10ShoppList.length > 0 ? (
				<div className={styles['spec-list-view']}>
					<div className={styles['shopp-view']}>
						<Grid columns={2} gap={rootFontSize * 0.08}>
							{spec10ShoppList.map((item, index) => (
								<Grid.Item key={item.productCode}>
									<ShoppItem
										onClick={handleShoppItem}
										sellPrice={item.sellPrice}
										productSellPriceJa={
											item.productSellPriceJa
										}
										productPurchaseType={
											item.productPurchaseType
										}
										title={item.title}
										productCode={item.productCode}
										productMainImg={item.productMainImg}
										status={item.status}
										showGoBackPrice={item.showGoBackPrice}
									/>
								</Grid.Item>
							))}
						</Grid>
					</div>
				</div>
			) : null}

			{/* 商品列表 */}
			<div
				ref={scrollRef}
				className={styles['cate-shopp-list']}
				style={
					{
						'--adm-color-background': 'transparent'
					} as any
				}
			>
				<WindowScroller
					// Element to attach scroll event listeners. Defaults to window.
					scrollElement={scrollRef.current || undefined}
				>
					{({ height }) => (
						<List>
							<AutoSizer disableHeight>
								{({ width }) => (
									<VirtualizedList
										autoHeight
										rowCount={SEARCH_GOOD_LIST.length}
										rowRenderer={rowRenderer}
										width={width}
										height={height}
										rowHeight={rootFontSize * 2.76}
										overscanRowCount={20}
									/>
								)}
							</AutoSizer>
						</List>
					)}
				</WindowScroller>
				<InfiniteScroll loadMore={loadMore} hasMore={hasMore} />
			</div>
		</>
	);
};
export default memo(IndexList);
