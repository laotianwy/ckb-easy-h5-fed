/*
 * @Author: shiguang
 * @Date: 2023-10-18 11:02:31
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-25 17:00:49
 * @Description: 搜索中转页
 */
import { useEffect, useState } from 'react';
import { Space, Toast } from 'antd-mobile';
import {
	createSearchParams,
	useNavigate,
	useSearchParams
} from 'react-router-dom';
import { useAtomValue } from 'jotai';
import { request } from '@/config/request';
import ScrollPage from '@/common/ScrollPage';
import CustomModal from '@/component/CustomModal';
import SearchInput from '@/component/SearchInput';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { User } from '@/Atoms';
import { HotKeyVO } from '@/service/easyMarket';
import { TaEvent, taTrack } from '@/config/buryingPoint';
import styles from './index.module.scss';
function Search() {
	const [searchParams, setSearchParams] = useSearchParams();
	const isLogin = useAtomValue(User.isLogin);
	const [searchValue, setSearchValue] = useState('');
	const [historyList, setHistoryList] = useState<string[]>([]);
	const [hotList, setHotList] = useState<string[]>([]);
	const [relationList, setRelationList] = useState<HotKeyVO[]>([]);
	const navigate = useNavigate();
	useEffect(() => {
		request.easyGoods.product.searchKeywordHot().then((res) => {
			setHotList(res.data || []);
		});
		const params: any = {
			bizScene: 1,
			stationCode: 'japanStation'
		};
		request.easyMarket.hot.keyFrontList(params).then((res) => {
			setRelationList(res.data || []);
		});
	}, []);
	useEffect(() => {
		if (isLogin) {
			getHistoryList();
		}
	}, [isLogin]);
	const getHistoryList = () => {
		request.easyGoods.product.searchKeywordHistory().then((res) => {
			setHistoryList(res.data || []);
		});
	};
	const onSearch = (val?: string) => {
		taTrack({
			event: TaEvent.PRODUCT_SEACH,
			value: {
				search_content: val
			}
		});
		const params: any = {
			keyword: String(val),
			originSearch: String(true)
		};
		if (sessionStorage.getItem('cateId')) {
			params.cateId = sessionStorage.getItem('cateId');
			params.cateName = sessionStorage.getItem('classifyTitle');
			sessionStorage.removeItem('cateId');
			sessionStorage.removeItem('classifyTitle');
		}
		// if (
		// 	sessionStorage.getItem('channelTag') ||
		// 	searchParams.get('channelTag')
		// ) {
		// 	// params.originSearch = false;
		// 	params.channelTag =
		// 		sessionStorage.getItem('channelTag') ||
		// 		searchParams.get('channelTag');
		// 	sessionStorage.removeItem('channelTag');
		// }
		navigate({
			pathname: '/goods/list',
			search: `?${createSearchParams(params)}`
		});
	};
	const cleanHistory = () => {
		CustomModal.confirm({
			content: window._$m.t('确定要清空历史记录吗？'),
			onConfirm: async () => {
				const res =
					await request.easyGoods.product.searchKeywordHistoryDelete();
				if (res.success) {
					Toast.show({
						icon: 'success',
						content: window._$m.t('删除成功')
					});
					getHistoryList();
				}
			}
		});
	};
	const relationClick = (item) => {
		taTrack({
			event: TaEvent.LIMITED_TIME_ACTIVITY_CLICK,
			value: {
				activity_name: item.name
			}
		});
		const params: any = {
			relationType: item.type,
			relationTitle: item.name,
			originSearch: String(true)
		};
		sessionStorage.removeItem('channelTag');
		navigate({
			pathname: '/goods/list',
			search: `?${createSearchParams(params)}`
		});
	};
	return (
		<ScrollPage
			title={
				<>
					<CustomNavbar
						fixed={false}
						title={window._$m.t('搜索')}
						isShowBackIcon={true}
					/>

					<SearchInput
						isText={false}
						onSearch={onSearch}
						value={searchValue}
					/>
				</>
			}
		>
			<div className="layout-content">
				<div className={styles['search-container']}>
					{
						<div className={styles['tags-container']}>
							{relationList.length > 0 && (
								<>
									<div className={styles['title-line']}>
										<div className={styles['title']}>
											{window._$m.t('限时活动')}
										</div>
									</div>
									<Space wrap>
										{relationList.map((item, index) => (
											<img
												key={index}
												alt=""
												onClick={() =>
													relationClick(item)
												}
												style={{
													height: '0.32rem'
												}}
												src={item.imgUrl}
											/>
										))}
									</Space>
								</>
							)}

							{isLogin && historyList.length > 0 ? (
								<>
									<div className={styles['title-line']}>
										<div className={styles['title']}>
											{window._$m.t('搜索历史')}
										</div>
										<img
											alt=""
											className={styles['delete']}
											onClick={cleanHistory}
											src="https://static-s.theckb.com/BusinessMarket/Easy/H5/delete.png"
										/>
									</div>
									<Space wrap>
										{historyList.map((item, index) => (
											<div
												key={index}
												onClick={() => onSearch(item)}
												className={styles['item']}
											>
												{item}
											</div>
										))}
									</Space>
								</>
							) : null}
							<div className={styles['title-line']}>
								<div className={styles['title']}>
									{window._$m.t('人气搜索')}
								</div>
							</div>
							<Space wrap>
								{hotList.map((item) => (
									<div
										key={item}
										onClick={() => onSearch(item)}
										className={styles['item']}
									>
										{item}
									</div>
								))}
							</Space>
						</div>
					}
				</div>
			</div>
		</ScrollPage>
	);
}
export default Search;
