/*
 * @Author: shiguang
 * @Date: 2023-10-28 17:22:23
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-19 10:50:25
 * @Description: PayPalCardPay
 */
import { useLocation, useNavigate } from 'react-router-dom';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import PayPalCardPayModule from '@/common/PayPalCardPay';
import { TaEvent, taTrack } from '@/config/buryingPoint';
const PayPalCardPay = () => {
	const { state } = useLocation();
	const navigate = useNavigate();
	return (
		<div>
			<CustomNavbar title={window._$m.t('paypal 信用卡支付')} />
			<div
				style={{
					marginTop: '0.45rem'
				}}
			>
				<PayPalCardPayModule
					orderNo={state.orderNo}
					onCaptrueEnd={(err) => {
						const res =
							localStorage.getItem(`order_${state.orderNo}`) ||
							'';
						if (err) {
							console.log(err);
							taTrack({
								event: TaEvent.ORDER_PAY,
								value: {
									order_no: state.orderNo,
									pay_amount: state.pay_amount,
									currency: 'JPY',
									pay_result: 'fail',
									fail_reason: err,
									order_product_detail: JSON.parse(res)
								}
							});
							navigate('/settlement/payFail', {
								replace: true
							});
							return;
						}
						taTrack({
							event: TaEvent.ORDER_PAY,
							value: {
								pay_amount: state.pay_amount,
								currency: 'JPY',
								order_no: state.orderNo,
								pay_result: 'success',
								order_product_detail: JSON.parse(res)
							}
						});
						navigate(
							`/settlement/paySuccess?orderNo=${state.orderNo}`,
							{
								replace: true
							}
						);
					}}
				/>
			</div>
		</div>
	);
};
export default PayPalCardPay;
