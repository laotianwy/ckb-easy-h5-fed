/*
 * @Author: yusha
 * @Date: 2023-12-28 10:46:26
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-24 17:16:55
 * @Description:
 */
import { useNavigate } from 'react-router-dom';
import qs from 'query-string';
import PayResult from '@/component/Result';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { jsBridge } from '@/utils/jsBridge';
function PaySuccess() {
	const navigate = useNavigate();
	const onCancel = async () => {
		const search = window.location.search;
		// 如果是路由带过来的参数
		const searchObj = qs.parse(search);
		const fromSniffApp = searchObj.fromSniffApp as string;
		// 如果是嵌入app
		if (window?.ReactNativeWebView && fromSniffApp) {
			await jsBridge.postMessage({
				type: 'DIRCT_goHome',
				payload: {}
			});
			return;
		}
		navigate('/goods/home', {
			replace: true
		});
	};
	const onConfirm = () => {
		navigate('/order/list', {
			replace: true
		});
	};
	return (
		<div
			className="layout-style"
			style={{
				backgroundColor: '#fff'
			}}
		>
			<CustomNavbar title={window._$m.t('支付失败')} />
			<PayResult
				type="fail"
				title={window._$m.t('支付失败')}
				description={window._$m.t('支付失败，请重新支付')}
				confirmText={window._$m.t('重新支付')}
				onCancel={onCancel}
				onConfirm={onConfirm}
			/>
		</div>
	);
}
export default PaySuccess;
