/*
 * @Author: yusha
 * @Date: 2023-11-01 09:49:12
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-04-11 13:34:51
 * @Description: 支付成功页面
 */
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import qs from 'query-string';
import { useRequest } from 'ahooks';
import { useEffect, useState } from 'react';
import { AutoCenter, Modal, List, SpinLoading } from 'antd-mobile';
import PayResult from '@/component/Result';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { jsBridge } from '@/utils/jsBridge';
import { request } from '@/config/request';
import './index.scss';
import { CouponInfoDTO } from '@/service/easyOrder';
import { formatDateCN2JP } from '@/utils';
function PaySuccess() {
	const [searchParams, setSearchParams] = useSearchParams();
	const [loading, setLoading] = useState(true);
	const [orderNo] = useState(() => searchParams.get('orderNo'));
	const [couponList, setCouponList] = useState<CouponInfoDTO[]>([]);
	const [times, setTimes] = useState(0);
	const navigate = useNavigate();
	const {
		data,
		runAsync: apiGetOrder,
		cancel
	} = useRequest(
		request.easyOrder.orderSearch.queryStatusAndCouponByOrderNo,
		{
			manual: true,
			pollingInterval: 1000
		}
	);
	const openUrlToApp = () => {
		const iosLinkUrl =
			'https://apps.apple.com/us/app/3fbox-%E7%9B%B4%E5%A3%B2%E9%80%9A%E8%B2%A9%E3%81%A772-off/id6474284467';
		const androidLinkurl =
			'https://play.google.com/store/apps/details?id=com.theckb.fbox';
		const userAgent = window.navigator.userAgent;
		/** ios终端 */
		const isIOS = !!userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
		const _url = `com.theckb.fbox://DirectHome`;
		window.location.href = _url;
		// openAppRef.current =
		return setTimeout(() => {
			// 没找到打开应用商店
			window.location.href = isIOS ? iosLinkUrl : androidLinkurl;
		}, 2000);
	};
	useEffect(() => {
		if (!orderNo) return;
		apiGetOrder({
			orderNo
		});
	}, [apiGetOrder, orderNo]);
	useEffect(() => {
		setTimes(times + 1);
		if (data?.data?.orderStatus === 1) {
			setCouponList(data.data?.firstOrderSendCouponList || []);
			if (data?.data?.bizExt?.firstOrder && !window?.ReactNativeWebView) {
				Modal.alert({
					content: window._$m.t('打开 3Fbox app？'),
					confirmText: window._$m.t('打开'),
					showCloseButton: true,
					onConfirm: () => {
						openUrlToApp();
					}
				});
			}
			setLoading(false);
			cancel();
		}
		if (times > 10) {
			cancel();
			navigate('/settlement/payFail', {
				replace: true
			});
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [data]);
	const onCancel = async () => {
		const search = window.location.search;
		// 如果是路由带过来的参数
		const searchObj = qs.parse(search);
		const fromSniffApp = searchObj.fromSniffApp as string;
		// 如果是嵌入app
		if (window?.ReactNativeWebView && fromSniffApp) {
			await jsBridge.postMessage({
				type: 'DIRCT_goHome',
				payload: {}
			});
			return;
		}
		navigate('/goods/home', {
			replace: true
		});
	};
	const onConfirm = () => {
		navigate('/order/list', {
			replace: true
		});
	};
	const renderLeft = (type, item) => {
		// 优惠劵具体优惠类型 1:满多少钱打多少折 2:满多少件减多少钱 3:满多少件打多少折 4:满多少钱减多少钱
		switch (type) {
			case 1:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							<span>
								<span>%</span>
								<span>OFF</span>
							</span>
						</div>
						<div className="coupon-list-item-cut">
							{item.feeToCut}
							{window._$m.t('円可用')}
						</div>
					</>
				);

				break;
			case 2:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							{window._$m.t('円')}
						</div>
						<div className="coupon-list-item-cut">
							{item.numToCut}
							{window._$m.t('件可用')}
						</div>
					</>
				);

				break;
			case 3:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							<span>
								<span>%</span>
								<span>OFF</span>
							</span>
						</div>
						<div className="coupon-list-item-cut">
							{item.numToCut}
							{window._$m.t('件可用')}
						</div>
					</>
				);

				break;
			case 4:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							{window._$m.t('円')}
						</div>
						<div className="coupon-list-item-cut">
							{item.feeToCut}
							{window._$m.t('円可用')}
						</div>
					</>
				);

				break;
			default:
				break;
		}
	};
	const renderCoupon = (item, index) => {
		return (
			<div
				style={{
					height: '100%',
					position: 'relative'
				}}
			>
				<div
					className="coupon-list-item-content"
					style={{
						backgroundImage: `url('https://static-jp.theckb.com/static-asset/easy-h5/coupon_bg.png')`
					}}
				>
					<div
						className="coupon-list-item-left"
						style={{
							color: '#FF5010'
						}}
					>
						{renderLeft(item.couponDetailType, item)}
					</div>
					<div
						className="coupon-list-item-right"
						style={{
							color: '#5B504D'
						}}
					>
						<div
							style={{
								fontSize: '0.15rem',
								color: '#242526',
								margin: '0.04rem 0 0.02rem 0',
								fontWeight: '600',
								width: '90%',
								textOverflow: 'ellipsis',
								overflow: 'hidden',
								whiteSpace: 'nowrap'
							}}
						>
							{item.couponName}
						</div>
						<div
							style={{
								margin: '0.02rem 0',
								paddingRight: '0.76rem',
								color: '#7C7371'
							}}
						>
							{window._$m.t('有效期')}{' '}
							{formatDateCN2JP(
								item.startDate,
								'YYYY-MM-DD HH:mm'
							)}{' '}
							-{' '}
							{formatDateCN2JP(item.endDate, 'YYYY-MM-DD HH:mm')}
						</div>
					</div>
				</div>
				<div
					className="coupon-list-item-btn"
					onClick={() => {
						const url = `/goods/list/coupon?couponId=${item.couponId}&couponCustomId=${item.couponCustomerId}`;
						navigate(url);
					}}
				>
					{window._$m.t('去使用')}
				</div>
			</div>
		);
	};
	return (
		<div
			className="layout-style"
			style={{
				backgroundColor: '#fff'
			}}
		>
			{loading ? (
				<div
					style={{
						height: '100vh',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						flexFlow: 'column'
					}}
				>
					<SpinLoading
						style={{
							'--size': '48px'
						}}
					/>

					<div
						style={{
							marginTop: '0.2rem'
						}}
					>
						{window._$m.t('支付结果获取中...')}
					</div>
				</div>
			) : (
				<>
					<CustomNavbar title={window._$m.t('支付成功')} />
					<PayResult
						title={window._$m.t('支付成功')}
						description={window._$m.t('订单准备中...')}
						confirmText={window._$m.t('查看订单')}
						onCancel={onCancel}
						onConfirm={onConfirm}
						type="success"
					/>

					{couponList.length > 0 && (
						<div>
							<AutoCenter
								style={{
									fontSize: '0.16rem',
									fontWeight: '600',
									marginBottom: '0.12rem'
								}}
							>
								{window._$m.t('首单奖励已到账！快去使用吧！')}
							</AutoCenter>
							<div
								style={{
									height: '2.2rem',
									overflowY: 'auto'
								}}
							>
								{couponList.map((item, index) => (
									<>
										<div
											className="coupon-list-item"
											key={index}
										>
											{renderCoupon(item, index)}
										</div>
									</>
								))}
							</div>
						</div>
					)}
				</>
			)}
		</div>
	);
}
export default PaySuccess;
