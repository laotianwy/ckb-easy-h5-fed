/*
 * @Author: shiguang
 * @Date: 2023-04-28 14:21:21
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-13 10:09:37
 * @Description: Login
 */

import MD5 from 'crypto-js/md5';
import qs from 'query-string';
import { useLocation } from 'react-router-dom';
import { Button } from 'antd-mobile';
import { useState } from 'react';
import { useToggle } from 'ahooks';
import { request } from '@/config/request';
import { getLoginToken } from '@/config/request/login';
const getInputValue = (selector: string) => {
	const element = window.document.querySelector(selector) as HTMLInputElement;
	return element?.value;
};
const Login = () => {
	const { url } = qs.parse(useLocation().search);
	// const [loginWay, setLoginWay] = useState('account');
	const [accountLogin, { toggle }] = useToggle(false);
	return (
		<div>
			<Button onClick={toggle}>切换登录方式</Button>

			<br />
			{accountLogin && (
				<>
					<div>{window._$m.t('账号:')}</div>
					<input type="text" id="nameOrEmail" name="nameOrEmail" />
					<br />
					<div>{window._$m.t('密码:')}</div>
					<br />
					<input type="text" id="password" name="password" />
				</>
			)}
			{!accountLogin && (
				<>
					<div>{window._$m.t('手机号码:')}</div>
					<input
						type="text"
						id="customerMobile"
						name="customerMobile"
					/>
					<br />
					<div>{window._$m.t('验证码:')}</div>
					<br />
					<input
						type="text"
						id="verificationCode"
						name="verificationCode"
					/>
				</>
			)}
			<button
				value="Submit"
				onClick={async () => {
					const nameOrEmail = getInputValue('#nameOrEmail');
					const password = getInputValue('#password');
					const customerMobile = getInputValue('#customerMobile');
					const verificationCode = getInputValue('#verificationCode');
					try {
						const token = await getLoginToken({
							nameOrEmail,
							password,
							verificationCode,
							customerMobile
						});
						if (token) {
							window.location.reload();
						}

						// production_route/curShop
					} catch (e) {
						console.log(e);
						// eslint-disable-next-line no-alert
						// window.alert(window._$m.t('登录失败～'));
						return;
					}
					// eslint-disable-next-line no-alert
					// window.alert(window._$m.t('登录成功～'));
					if (url) {
						const _url = decodeURIComponent(url as string);
						window.location.href = _url;
					}
					// jumpPage(ENUM_PAGE.HOME);
				}}
			>
				{window._$m.t('提交')}
			</button>
		</div>
	);
};
export default Login;
