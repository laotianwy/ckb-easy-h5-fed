/*
 * @Author: shiguang
 * @Date: 2023-05-15 18:50:05
 * @LastEditors: shiguang
 * @LastEditTime: 2023-05-15 19:02:57
 * @Description: CssTheme
 */
import { useState } from 'react';

const CssTheme = () => {
	const [isB2B, setIsB2B] = useState(true);
	return (
		<div>
			<div
				id={isB2B ? 'J_B2B' : 'J_D2C'}
				style={{
					textAlign: 'center',
					backgroundColor: 'var(--primary-color)'
				}}
			>
				<button onClick={() => setIsB2B(!isB2B)}>
					change css theme to {isB2B ? 'B2B' : 'D2C'}
				</button>
			</div>
		</div>
	);
};

export default CssTheme;
