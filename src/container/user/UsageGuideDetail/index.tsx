/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2024-04-11 11:17:15
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-04-11 11:57:25
 * @FilePath: /ckb-easy-h5-fed/src/container/user/UsageGuideDetail/index.tsx
 * @Description: 利用規約详情
 */
import { useAsyncEffect, useRequest } from 'ahooks';
import React, { memo, useState } from 'react';
import axios from 'axios';
import { SpinLoading } from 'antd-mobile';
import { request } from '@/config/request';
import ScrollPage from '@/common/ScrollPage';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import './index.scss';
import { ENUM_TREM_TYPE } from '@/outs/const';

const UsageGuideDetail = () => {
	const [html, setHtml] = useState('');
	const search = window.location.search;
	const params = new URLSearchParams(search);
	const termType = params.get('termType');

	const { runAsync: apiGetCategory, loading } = useRequest(
		request.customer.base.termGetEffectTermByType,
		{
			manual: true
		}
	);
	useAsyncEffect(async () => {
		const res = await apiGetCategory({ type: termType });
		// const res = await apiGetCategory({ type: 'dspecific' });
		if (res.data && res.data[0]?.fileUrl) {
			const text = await axios.get(
				res.data[0].fileUrl + '?t=' + Date.now()
			);
			const value = text.data.replace(/target=""/g, 'target="_blank"');
			setHtml(value);
		}
	}, []);
	return (
		<ScrollPage
			className="usage-guide-detail"
			title={
				<CustomNavbar
					title={
						termType === ENUM_TREM_TYPE.DUTILIZE
							? '利用规约'
							: '特定商取引法に基づく表示'
					}
					fixed={false}
				/>
			}
		>
			{loading && <SpinLoading className="classify-loading" />}
			<div
				className="layout-content"
				style={{
					marginTop: '0.69rem',
					height: 'calc(100vh - 0.69rem)'
				}}
			>
				<div
					className="guide-content"
					dangerouslySetInnerHTML={{ __html: html }}
				/>
			</div>
		</ScrollPage>
	);
};

export default memo(UsageGuideDetail);
