/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: huajian
 * @LastEditTime: 2023-12-21 10:30:16
 * @Description: 线下充值
 */
import { Button, Form, Input, Toast } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import { useRequest } from 'ahooks';
import CustomInput from '@/component/CustomInput';
// import Download from '@/common/icons/Download';
import CustomModal from '@/component/CustomModal';
import { request } from '@/config/request';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ScrollPage from '@/common/ScrollPage';
import { Upload } from './Upload';
import './index.scss';
import { MoDownloadPop } from './MoDownloadPop';
const OfflineRecharge = () => {
	const { loading, run } = useRequest(
		(params) => {
			return request.pay.front.accountRecharge(params).then((res) => {
				navigate('/user/rechargeResut', {
					replace: true
				});
			});
		},
		{
			debounceWait: 1000,
			manual: true
		}
	);
	const [form] = Form.useForm();
	const navigate = useNavigate();
	const onSubmit = () => {
		form.submit();
	};
	const onFinish = (value) => {
		run({
			...value,
			voucherUrl: value.voucherUrl[0].url
		});
	};
	return (
		<ScrollPage
			className="page-offline-recharge"
			title={
				<CustomNavbar
					title={window._$m.t('充值')}
					navBarRight={
						<div
							className="navbar-right-text"
							onClick={() => {
								navigate('/user/rechargeRecord');
							}}
						>
							{window._$m.t('充值记录')}
						</div>
					}
				/>
			}
		>
			<div className="layout-content page-offline-recharge-content">
				<div
					style={{
						color: '#1C2026',
						fontSize: '.14rem',
						fontWeight: '500',
						lineHeight: '.22rem',
						marginBottom: '.12rem'
					}}
				>
					{window._$m.t('银行转账（瑞穗银行）')}
				</div>
				<div
					style={{
						backgroundColor: '#FAFBFC',
						color: '#7E8694',
						fontSize: '.12rem',
						padding: '.05rem .08rem',
						borderRadius: '.04rem',
						marginBottom: '.12rem'
					}}
				>
					<div>{window._$m.t('存款反映：大约1个工作日')}</div>
					<div>
						{window._$m.t(
							'*周末、节假日和替代节假日的转账将在下一个工作日处理。'
						)}
					</div>
				</div>
				<Form
					form={form}
					layout="horizontal"
					className="address-form-style"
					initialValues={{
						currency: window._$m.t('JPY')
					}}
					onFinish={onFinish}
				>
					<Form.Item
						label={window._$m.t('汇款人姓名')}
						name="remitterName"
						// required
						rules={[
							{
								required: true,
								message: window._$m.t('请输入')
							}
						]}
					>
						<CustomInput
							placeholder={window._$m.t('请输入姓名')}
							maxLength={20}
						/>
					</Form.Item>
					<Form.Item
						label={window._$m.t('银行名')}
						name="remitterBankName"
						required
						rules={[
							{
								required: true,
								message: window._$m.t('请输入')
							}
						]}
					>
						<CustomInput
							placeholder={window._$m.t('请输入银行名')}
							maxLength={20}
						/>
					</Form.Item>
					<Form.Item
						label={window._$m.t('充值金额')}
						name="remittanceAmount"
						required
						rules={[
							{
								required: true,
								message: window._$m.t('请输入')
							},
							{
								pattern: /^[1-9][0-9]{0,8}$/,
								message: window._$m.t('请输入')
							}
						]}
					>
						<CustomInput
							min={1}
							placeholder={window._$m.t('请输入充值金额')}
							type="number"
							pattern="[0-9]*"
						/>
					</Form.Item>
					<Form.Item label={window._$m.t('币种')} name="currency">
						<CustomInput
							placeholder={window._$m.t('请输入')}
							disabled={true}
						/>
					</Form.Item>
					<>
						<div className="offline-recharge-static-field-box">
							<div className="static-field-label">
								{window._$m.t('我社收款银行')}
							</div>
							<div className="static-field-value">
								{window._$m.t('楽天銀行')}
							</div>
						</div>
						<div className="offline-recharge-static-field-box">
							<div className="static-field-label">
								{window._$m.t('第二営業支店')}
							</div>
							<div className="static-field-value">
								{window._$m.t('店番252')}
							</div>
						</div>
						<div className="offline-recharge-static-field-box">
							<div className="static-field-label">
								{window._$m.t('口座番号')}
							</div>
							<div className="static-field-value">
								{window._$m.t('普通預金 7924602')}
							</div>
						</div>
						<div className="offline-recharge-static-field-box">
							<div className="static-field-label">
								{window._$m.t('口座名')}
							</div>
							<div className="static-field-value">
								{window._$m.t('株式会社 SNIFF JAPAN')}
							</div>
						</div>
					</>
					<Form.Item
						required
						rules={[
							{
								required: true
							}
						]}
						className="zzpz"
						name="voucherUrl"
						label={
							<div
								style={{
									display: 'flex',
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<span>{window._$m.t('转账凭证')}</span>
								<img
									style={{
										width: '0.16rem',
										marginLeft: '.05rem'
									}}
									onClick={() => {
										CustomModal.confirm({
											content: window._$m.t(
												'请提供包含转账日期、转账请求人、收件人姓名和转账金额信息的图片。'
											),
											confirmText:
												window._$m.t('我知道了'),
											showCloseButton: false,
											className: 'confirm-voucher-url'
										});
									}}
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/question@3x.png"
									alt="question"
								/>
							</div>
						}
					>
						<Upload
							pathParams={{
								serviceName: 'order',
								bizName: 'payment',
								bizType: 'oemGoodsImg'
							}}
						>
							<div
								style={{
									border: '.02rem solid #CDD2DA',
									borderRadius: '.24rem',
									background: '#FFF',
									padding: '.08rem .16rem',
									fontSize: '.13rem',
									color: '#1C2026',
									display: 'flex',
									alignItems: 'center'
								}}
							>
								<img
									style={{
										width: '.16rem',
										marginRight: '.04rem'
									}}
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/upload.png"
									alt="upload"
								/>

								{window._$m.t('点击上传')}
							</div>
						</Upload>
					</Form.Item>
					<Form.Item label={window._$m.t('请求书')}>
						<div
							style={{
								display: 'inline-block'
							}}
						>
							<div
								style={{
									border: '.02rem solid #CDD2DA',
									borderRadius: '.24rem',
									background: '#FFF',
									padding: '.08rem .16rem',
									fontSize: '.13rem',
									display: 'flex',
									alignItems: 'center'
								}}
							>
								<img
									style={{
										width: '.16rem',
										marginRight: '.04rem'
									}}
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/download.png"
									alt="download"
								/>

								<MoDownloadPop>
									<span>{window._$m.t('点击下载')}</span>
								</MoDownloadPop>
							</div>
						</div>
					</Form.Item>
				</Form>
			</div>
			<div
				style={{
					height: '.64rem',
					backgroundColor: '#fff',
					display: 'flex',
					alignItems: 'center',
					padding: '0 .12rem',
					position: 'fixed',
					left: 0,
					right: 0,
					bottom: 0,
					boxShadow: '0px -3px 10px 0px rgba(0,0,0,0.06)',
					paddingBottom: '.2rem'
				}}
			>
				<Button
					disabled={loading}
					block
					color="primary"
					shape="rounded"
					onClick={onSubmit}
					style={{
						padding: '0.1rem 0'
					}}
				>
					{window._$m.t('确定')}
				</Button>
			</div>
		</ScrollPage>
	);
};
export default OfflineRecharge;
