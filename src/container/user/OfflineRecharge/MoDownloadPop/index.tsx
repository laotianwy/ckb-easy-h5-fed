/*
 * @Author: dongguang.fu
 * @Date: 2023-10-30 15:47:39
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-12 15:50:19
 * @Description: 证书下载弹窗
 */

import { Button, Form } from 'antd-mobile';
import { useState } from 'react';
import CustomInput from '@/component/CustomInput';
import CustomPopup from '@/component/CustomPopup';
import { request } from '@/config/request';
import styles from './index.module.scss';
export const downloadFile = (data: BlobPart, fileName: string) => {
	const url = window.URL.createObjectURL(new Blob([data]));
	const a = document.createElement('a');
	a.href = url;
	a.style.display = 'none';
	a.setAttribute('download', fileName);
	document.body.appendChild(a);
	a.click();
	URL.revokeObjectURL(a.href);
	document.body.removeChild(a);
};
interface Props {
	children: React.ReactNode;
}
export const MoDownloadPop: React.FC<Props> = (props) => {
	const [form] = Form.useForm();
	const [visible, setVisible] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const onFinish = async () => {
		setIsLoading(true);
		setTimeout(() => setIsLoading(false), 1000);
		const values = form.getFieldsValue();
		const file: any = await request.pay.front
			.accountQueryRechargeRequestDownload(values, {
				format: 'blob'
			})
			.catch((err) => console.log(err));
		setVisible(false);
		form.resetFields();
		downloadFile(file, window._$m.t('请求书.pdf'));
	};
	return (
		<>
			<CustomPopup
				visible={visible}
				onClose={() => {
					setVisible(false);
					form.resetFields();
				}}
			>
				<div className={styles.container}>
					<div className={styles.title}>
						{window._$m.t('请求书下载')}
					</div>
					<Form
						form={form}
						layout="horizontal"
						onFinish={onFinish}
						className="address-form-style"
					>
						<Form.Item
							name="remitterName"
							label={window._$m.t('请求对象姓名')}
							required
							rules={[
								{
									required: true,
									message: window._$m.t('请输入')
								}
							]}
						>
							<CustomInput placeholder={window._$m.t('请输入')} />
						</Form.Item>
						<Form.Item
							name="remittanceAmount"
							label={window._$m.t('请求金额（JPY）')}
							required
							rules={[
								{
									required: true,
									message: window._$m.t('请输入')
								}
							]}
						>
							<CustomInput
								type="number"
								placeholder={window._$m.t('请输入')}
							/>
						</Form.Item>
						<Form.Item>
							<div className={styles.btnWrap}>
								<Button
									shape="rounded"
									className={styles.cancelBtn}
									onClick={() => {
										setVisible(false);
										form.resetFields();
									}}
								>
									{window._$m.t('取消')}
								</Button>
								<Button
									shape="rounded"
									disabled={isLoading}
									className={styles.submitBtn}
									color="primary"
									type="submit"
								>
									{window._$m.t('提交并下载')}
								</Button>
							</div>
						</Form.Item>
					</Form>
				</div>
			</CustomPopup>
			<span onClick={() => setVisible(true)}>{props.children}</span>
		</>
	);
};
