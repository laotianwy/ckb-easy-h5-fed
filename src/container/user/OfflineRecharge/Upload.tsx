/*
 * @Author: huajian
 * @Date: 2023-10-26 14:14:13
 * @LastEditors: huajian
 * @LastEditTime: 2023-10-31 15:48:52
 * @FilePath: /ckb-easy-h5-fed/src/container/user/OfflineRecharge/Upload.tsx
 * @Description: 上传
 */
import React, { useEffect, useState } from 'react';
import {
	ImageUploader,
	Space,
	Toast,
	Dialog,
	ImageUploaderProps,
	ImageUploadItem
} from 'antd-mobile';
import { flushSync } from 'react-dom';
import { file } from '@babel/types';
import { request } from '@/config/request';
type Props = Omit<ImageUploaderProps, 'upload' | 'onChange'> & {
	value?: ImageUploadItem[];
	/** 上传的数据是否覆盖 默认不覆盖 */
	isOverride?: number;
	pictureUploadLabel?: React.ReactNode;
	pathParams: {
		/** 服务名，例如 order, purchase */
		serviceName: 'order' | 'purchase';
		/**
		 * 业务名称，(如果没有就自己命名)
		 * tableTrade : 表格下单
		 * deliveryInvoice: 国际发货单
		 * stockInRecord：入库单
		 * checkAbnormal：质检异常
		 * OEM：oem
		 * */
		bizName:
			| 'tableTrade'
			| 'deliveryInvoice'
			| 'stockInRecord'
			| 'checkAbnormal'
			| 'OEM'
			| 'payment';
		/**
		 * 业务类型：(属于bizName 下属 如果没有就自己命名)
		 * 如果是表格下单业务，分为
		 * 导入到购物车：importShopCart
		 * 导入下单: importTrade
		 * oem 商品图片: oemGoodsImg
		 * oem log 附件: oemLogAttachment
		 * oem 式样书: styleBook
		 * oem 色卡图片: styleBook
		 */
		bizType:
			| 'oemGoodsImg'
			| 'oemSizeTemplate'
			| 'styleBook'
			| 'colorCardSelect';
		/**
		 *  如果是Client端登陆，就是customerId
		 * 如果是System端登陆 userId
		 * 如果是WMS登陆 userId
		 */
		operatorId?: 'system' | 'client' | string;
	};
};
const isProduction = (() => {
	if (window.location.port) return false;
	const pre = window.location.hostname.split('.')[0];
	return pre.startsWith('pre-') || !pre.includes('-');
})();
const bucketName = isProduction ? 'ckb-service-prod' : 'ckb-service-test';
// 'https://theckbstest-oss.theckbs.com/order/OEM/system/oemGoodsImg/1685367040762.png'
// https://theckbstest-oss.theckbs.com/order/OEM/system/oemGoodsImg/1685367125855.png
// https://theckbstest-oss.theckbs.com/order/OEM/system/oemGoodsImg/1685367192676.png

interface OSSDataType {
	dir: string;
	expire: string;
	host: string;
	accessId: string;
	policy: string;
	signature: string;
	/** 上传域名 */
	customDomain: string;
}
export const Upload: React.FC<Props> = (props) => {
	const [OSSData, setOSSData] = useState<OSSDataType>();
	const { pathParams, children, pictureUploadLabel, value, ...otherProps } =
		props;
	const { serviceName, bizName, operatorId, bizType } = pathParams;
	const path =
		[serviceName, bizName, operatorId ?? 'system', bizType].join('/') + '/';
	const mockGetOSSData = async () => {
		// 这个接口有毒 需要这样传
		return (
			await request.customer.oss.signAnother({}, {
				path: `/oss/sign?path=${path}&bucketName=${bucketName}&type=0`
			} as any)
		).data;
	};
	const beforeUpload: ImageUploaderProps['beforeUpload'] = async (
		file: any,
		files
	) => {
		const _OSSData = await init();
		const suffix = file.name.slice(file.name.lastIndexOf('.'));
		const filename = Date.now() + suffix;
		// @ts-ignore
		file.url = _OSSData?.dir + filename;
		/**  有图片上传，提交上传时使用这个字段 */
		file.wholeUrl = _OSSData?.customDomain + '/' + _OSSData?.dir + filename;
		return file;
	};
	const upload: ImageUploaderProps['upload'] = async (file) => {
		const suffix = file.name.slice(file.name.lastIndexOf('.'));
		const filename = Date.now() + suffix;
		const _OSSData = await init();
		const url = _OSSData?.dir + filename;
		const formdata = new FormData();
		formdata.append('OSSAccessKeyId', _OSSData!.accessId);
		formdata.append('policy', _OSSData!.policy);
		formdata.append('signature', _OSSData!.signature);
		formdata.append('key', url);
		formdata.append('file', file);
		await fetch(_OSSData!.host, {
			method: 'post',
			mode: 'no-cors',
			body: formdata
		});
		return {
			url: [_OSSData?.customDomain, '/', url].join('')
		};
	};
	const init = async () => {
		try {
			const result = await mockGetOSSData();
			if (result) {
				flushSync(() => {
					setOSSData(result as OSSDataType);
				});
				return result as OSSDataType;
			}
		} catch (error: any) {
			Toast.show({
				icon: 'fail',
				content: window._$m.t('上传失败')
			});
		}
	};
	return (
		<ImageUploader
			multiple={false}
			maxCount={1}
			onChange={(res) => {
				console.log(res);
			}}
			beforeUpload={beforeUpload}
			upload={upload}
			{...props}
		>
			{props.children}
		</ImageUploader>
	);
};
