/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-24 17:29:51
 * @Description: 充值结果页
 */
import { useNavigate } from 'react-router-dom';
import Result from '@/component/Result';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ScrollPage from '@/common/ScrollPage';
import { jsBridge } from '@/utils/jsBridge';
// const onCancel = () => {};

// const onConfirm = () => {};

const RechargeResut = () => {
	const navigate = useNavigate();
	const onCancel = async () => {
		// 如果是嵌入app
		if (window?.ReactNativeWebView) {
			await jsBridge.postMessage({
				type: 'DIRCT_goHome',
				payload: {}
			});
			return;
		}
		navigate('/goods/home', {
			replace: true
		});
		// window.history.length = 0;
		window.history.pushState(null, null as any, '/goods/home');
	};
	const onConfirm = () => {
		navigate('/user/rechargeRecord', {
			replace: true
		});
	};
	return (
		<ScrollPage title={<CustomNavbar title={window._$m.t('提交成功')} />}>
			<Result
				title={window._$m.t('提交成功')}
				description={window._$m.t('充值信息提交成功')}
				confirmText={window._$m.t('查看充值记录')}
				onCancel={onCancel}
				onConfirm={onConfirm}
				type="success"
			/>
		</ScrollPage>
	);
};
export default RechargeResut;
