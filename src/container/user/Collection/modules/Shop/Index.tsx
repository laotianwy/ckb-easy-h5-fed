import { useEffect, useState } from 'react';
import { Tabs, Image, Ellipsis, Toast } from 'antd-mobile';
import { useBoolean, useRequest, useToggle, useUpdateEffect } from 'ahooks';
import classNames from 'classnames';
import { useNavigate } from 'react-router-dom';
import { useAtomValue } from 'jotai';
import { request } from '@/config/request';
import PriceSorting from '@/common/icons/PriceSorting';
import { User } from '@/Atoms';
import {
	ProductFavoritePageReqDTO,
	ProductFavoriteRespDTO
} from '@/service/easyGoods';
import { useTurnPage } from '@/utils/hooks/useTurnPage';
import { formatMoney, jumpToApp, navigateAppOrH5 } from '@/utils';
import OpenVip from '@/component/OpenVip';
import { EnumProductPurchaseType, Enum_SellStatus } from '@/common/Enums';
import { CustomRMImg } from '@/component/CustomRMImg';
import PriceDescendingOrder from '@/common/icons/PriceDescendingOrder';
import PriceAscendingOrder from '@/common/icons/PriceAscendingOrder';
import ModalGoToPurchase from '@/component/ModalGoToPurchase';
import Shoucanglike2 from '@/common/icons/Shoucanglike2';
import styles from '../../index.module.scss';
const Delete_Status = 4;

interface Props {
	isDesc: boolean;
}
function Shop({ isDesc }: Props) {
	const navigate = useNavigate();
	/** 前往代采的弹窗 */
	const [visible, setVisible] = useState(false);
	const [activeKey, setActiveKey] = useState(0);
	const [showMaskId, setShowMaskId] = useState('');
	const [starLoading, setStarLoading] = useState(false);
	const [openVipVisible, openVipCtl] = useToggle(false);
	const userInfo = useAtomValue(User.userDetail);

	// 获取直采用户跳转代采是否弹框标识
	const { runAsync: getCustomer2AgencyGoodsSign } = useRequest(
		request.customer.getCustomer2AgencyGoodsSign
			.getCustomer2AgencyGoodsSign,
		{
			manual: true
		}
	);
	const [goodsList, RenderTurnpage, { setParams, setRecords }] = useTurnPage<
		ProductFavoriteRespDTO,
		ProductFavoritePageReqDTO
	>({
		params: {
			favoriteProductType: activeKey
		},
		request(parmas) {
			parmas.orderByType = isDesc ? 'desc' : 'asc';
			return request.easyGoods.favoriteProduct.page(parmas);
		}
	});

	useUpdateEffect(() => {
		setParams({
			favoriteProductType: activeKey,
			orderByType: isDesc ? 'desc' : 'asc'
		});
	}, [activeKey, isDesc, setParams]);
	const toggleStar = async (index: number, activeKey: string) => {
		if (starLoading) return;
		setStarLoading(true);
		const currentGoods: any = goodsList![index];
		if (currentGoods.collected) {
			const res = await request.easyGoods.favoriteProduct.cancel({
				productCode: currentGoods.productCode
			});
			if (!res.success) return;
			currentGoods.collected = false;
			setRecords([...goodsList]);
			setStarLoading(false);
			Toast.show({
				content: window._$m.t('取消收藏成功')
			});
			return;
		}
		const res = await request.easyGoods.favoriteProduct.postFavoriteProduct(
			{
				...currentGoods
			}
		);
		if (!res.success) return;
		currentGoods.collected = true;
		setRecords([...goodsList]);
		setStarLoading(false);
		Toast.show({
			content: window._$m.t('收藏成功')
		});
	};

	/** 线下商品点击同步接口 */
	const { runAsync: asyncOtherShopp } = useRequest(
		request.easyGoods.product.searchCheckSync,
		{
			manual: true
		}
	);
	const clickAsyncShopp = async (productCode: string) => {
		try {
			const res = await asyncOtherShopp({
				productCode
			});
		} catch (err) {
			console.log(err);
		}
	};

	/**
	 * 处理商品调整流程
	 * @param productCode
	 * @param productPurchaseType
	 */
	const clickGoodsItem = async (productCode: string) => {
		const productPurchaseType = activeKey;
		// 如果是直采商品直接去商品详情
		if (productPurchaseType === EnumProductPurchaseType.DIRECT)
			return navigateAppOrH5({
				h5Url: '/goods/detail',
				params: {
					productCode
				},
				appPageType: 'GoodDetail',
				navigate
			});

		// 同步线下商品信息
		if (typeof productCode === 'string' && productCode.length > 0) {
			clickAsyncShopp(productCode);
		}
		const isNeedShowModalGoToPurchasel =
			await getCustomer2AgencyGoodsSign();
		// 如果是代采商品，点击显示弹窗,且直采用户跳转代采是否弹框标识为false，false表示展示
		if (!isNeedShowModalGoToPurchasel.data) {
			setVisible(true);
			return;
		}
		if (productCode) {
			jumpToApp({
				jumpLink: `GoodsItemDetail?productCode=${productCode}`
			});
		}
	};
	const renderGoodsList = (list: Array<any>, key: string) => {
		const disabledText = {
			[Enum_SellStatus.Normal]: window._$m.t('正常'),
			[Enum_SellStatus.SoldOut]: window._$m.t('售罄'),
			[Enum_SellStatus.delist]: window._$m.t('已下架')
		};
		return (
			<div className={styles['collection-box']}>
				{list.map((item, index) => (
					<div key={index} className={styles['collection-item']}>
						<div>
							<div
								className={styles['goods-image-content']}
								onClick={() => clickGoodsItem(item.productCode)}
							>
								<CustomRMImg
									src={item.productMainImg}
									imgSize={200}
									alt=""
									className={styles['goods-image']}
								/>

								{item.status === Delete_Status &&
									showMaskId !== item.productCode &&
									Number(key) ===
										EnumProductPurchaseType.DIRECT && (
										<div
											className={styles['mask-sold-out']}
										>
											<div className={styles['sold-out']}>
												<span>
													{window._$m.t('已删除')}
												</span>
											</div>
										</div>
									)}

								{item.availableQuantity <= 0 &&
									showMaskId !== item.productCode &&
									Number(key) ===
										EnumProductPurchaseType.DIRECT && (
										<div
											className={styles['mask-sold-out']}
										>
											<div className={styles['sold-out']}>
												<span>
													{window._$m.t('售罄')}
												</span>
											</div>
										</div>
									)}

								{item.status === Enum_SellStatus.delist &&
									showMaskId !== item.productCode &&
									Number(key) ===
										EnumProductPurchaseType.DIRECT && (
										<div
											className={styles['mask-sold-out']}
										>
											<div className={styles['sold-out']}>
												<span>
													{window._$m.t('已下架')}
												</span>
											</div>
										</div>
									)}
							</div>
							<div
								className={styles['goods-title']}
								onClick={() => clickGoodsItem(item.productCode)}
							>
								{item.productTitle ?? ''}
							</div>
							<div className={styles['collection-bottom']}>
								{activeKey === 1 ? (
									<div className={styles['collection-price']}>
										<span>
											<span>{window._$m.t('低至')}</span>
											<span>
												{formatMoney(
													item.productSellPrice
												)}
												{window._$m.t('元')}
											</span>
											<span>
												{window._$m.t('(约{{x}}円)', {
													data: {
														x: item.productSellPriceJpy
													}
												})}
											</span>
										</span>
									</div>
								) : (
									<div className={styles['collection-price']}>
										<span>
											{formatMoney(item.productSellPrice)}
											{window._$m.t('円')}
										</span>
									</div>
								)}

								<div
									className={styles['collect-status']}
									onClick={(event) => {
										event.stopPropagation();
										toggleStar(index, key);
									}}
								>
									<Shoucanglike2
										color={
											!item.collected
												? '#E6EAF0'
												: '#FF5010'
										}
									/>
								</div>
							</div>
						</div>

						{/* 前往代采的弹窗 */}
						<ModalGoToPurchase
							visible={visible}
							onClose={() => setVisible(false)}
							onOk={(val) => {
								setVisible(false);
								if (item.productCode) {
									jumpToApp({
										jumpLink: `GoodsItemDetail?productCode=${item.productCode}`
									});
								}
							}}
						/>
					</div>
				))}
			</div>
		);
	};
	const renderDescend = () => {
		const sortWay = isDesc ? 'desc' : 'asc';
		if (sortWay === 'desc') {
			return <PriceDescendingOrder width={14} height={14} />;
		} else if (sortWay === 'asc') {
			return <PriceAscendingOrder width={14} height={14} />;
		}
		return <PriceSorting width={7} height={9} />;
	};
	return (
		<>
			<div
				className={classNames('layout-content', styles.Shop)}
				style={{
					backgroundColor: !goodsList?.length ? '#f8f8f8' : undefined
				}}
			>
				{!userInfo?.membership?.templateLevel &&
					activeKey === EnumProductPurchaseType.PROXY &&
					!!goodsList?.length && (
						<div className={styles.noticeWrao}>
							<div
								className={styles.noticeImg}
								onClick={openVipCtl.toggle}
							>
								{window._$m.t(
									'开通会员即可享受代采商品购买，价格更低更透明！'
								)}
							</div>
						</div>
					)}

				{!!goodsList && renderGoodsList(goodsList, String(activeKey))}
			</div>
			{RenderTurnpage}
			<OpenVip
				visible={openVipVisible}
				setVisible={openVipCtl.toggle}
				onClick={() => {
					jumpToApp({
						isVip: Boolean(userInfo?.membership?.templateLevel)
					});
				}}
			/>
		</>
	);
}
export default Shop;
