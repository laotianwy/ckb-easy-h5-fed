import {
	Dispatch,
	SetStateAction,
	forwardRef,
	useImperativeHandle
} from 'react';
import classNames from 'classnames';
import { Ellipsis, Toast } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import { api } from '@/service';
import { useTurnPage } from '@/utils/hooks/useTurnPage';
import {
	BizResponsePageMktCollectionClassifyDTO,
	MktCollectionClassifyDTO,
	MktCustomerCollectionDTO
} from '@/service/easyMarket';

import CheckboxChecked from '@/common/icons/CheckboxChecked';
import CheckboxDefault from '@/common/icons/CheckboxDefault';
import { PRODUCT_COLLECTION } from '@/common/Enums';
import { CustomRMImg } from '@/component/CustomRMImg';
import styles from '../../index.module.scss';

interface Props {
	/** 是不是编辑模式 */
	isEdit: boolean;
	delIds: number[];
	setDelIds: Dispatch<SetStateAction<number[]>>;
}

export interface Ref {
	delSpecial: () => void;
}

const SpecialEdition = forwardRef(
	({ isEdit, delIds, setDelIds }: Props, ref) => {
		const navigate = useNavigate();
		const [goodsList, RenderTurnpage, { setParams, setRecords }] =
			useTurnPage<MktCollectionClassifyDTO, MktCustomerCollectionDTO>({
				params: {},
				request(parmas) {
					return api.easyMarket.collection.qrySpeciallyCollectonsPage(
						parmas
					);
				}
			});

		// 切换 Ids 状态
		const changeSelect = (item: MktCollectionClassifyDTO, e) => {
			e.stopPropagation();
			const ndelIds = delIds.includes(item.activityAreaId)
				? delIds.filter((i) => i !== item.activityAreaId)
				: [...delIds, item.activityAreaId];
			setDelIds(ndelIds);
		};

		useImperativeHandle(
			ref,
			() => {
				return {
					delSpecial: (delIds) => {
						setRecords((items) => {
							return items.filter(
								(i) => !delIds.includes(i.activityAreaId)
							);
						});
					}
				};
			},
			[setRecords]
		);

		// 跳转特辑
		const gotoSpecail = (i: MktCollectionClassifyDTO) => {
			if (i.activetyAlive) {
				localStorage.setItem('collectionTabKey', '2');
				if (i.productCollection === PRODUCT_COLLECTION.SINGLE) {
					navigate(
						`/goods/detail?productCode=${i.singleProductCode}`
					);
				} else {
					navigate(
						`/goods/collectionGoodsList?id=${i.activityAreaId}`
					);
				}
			} else {
				Toast.show({
					content: window._$m.t(
						'抱歉，该特辑活动已结束失效了， 请再逛逛其他特集吧～'
					)
				});
			}
		};

		// 列表 Item 渲染
		const renderSpecialList = (
			list: MktCollectionClassifyDTO[] = [{}, {}]
		) => {
			return (
				<div className={styles['special-box']}>
					{list.map((i) => {
						return (
							<div
								key={i.activityAreaId}
								className={styles['special-item']}
								onClick={() => gotoSpecail(i)}
							>
								<div
									style={{
										filter: 'blur(2px)',
										height: '100%'
									}}
								>
									<div className={styles['special-img-box']}>
										<CustomRMImg
											className={styles['special-img']}
											src={i.homeImgUrl || i.headUrl}
											alt=""
										/>
									</div>
								</div>
								{isEdit && (
									<div
										className={styles.selectedBox}
										onClick={(e) => changeSelect(i, e)}
									>
										{delIds.includes(i.activityAreaId) ? (
											<CheckboxChecked
												color="#000"
												style={{
													background: '#fff',
													borderRadius: '0.03rem'
												}}
											/>
										) : (
											<CheckboxDefault />
										)}
									</div>
								)}

								{!i.activetyAlive && (
									<div className={styles.activetyAlive}>
										<div className={styles.tag}>
											{window._$m.t('已失效')}
										</div>
									</div>
								)}

								<div className={styles.specialTitle}>
									{i.barCode && <div>{i.barName}</div>}
									<div className={styles.ellipsis}>
										{i.moduleName}
									</div>
								</div>
							</div>
						);
					})}
				</div>
			);
		};

		return (
			<>
				<div
					className={classNames(
						'layout-content',
						styles.SpecialEdition
					)}
					style={{
						backgroundColor: !goodsList?.length
							? '#f8f8f8'
							: undefined
					}}
				>
					{!!goodsList.length && renderSpecialList(goodsList)}
				</div>

				{RenderTurnpage}
			</>
		);
	}
);

export default SpecialEdition;
