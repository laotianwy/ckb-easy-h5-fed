import { useEffect, useRef, useState } from 'react';
import { Tabs, Toast } from 'antd-mobile';
import { useBoolean } from 'ahooks';
import classNames from 'classnames';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import PriceSorting from '@/common/icons/PriceSorting';
import PriceDescendingOrder from '@/common/icons/PriceDescendingOrder';
import PriceAscendingOrder from '@/common/icons/PriceAscendingOrder';
import ScrollPage from '@/common/ScrollPage';
import EditAddress from '@/common/icons/EditAddress';
import DeleteLine from '@/common/icons/DeleteLine';
import CustomModal from '@/component/CustomModal';
import { api } from '@/service';
import styles from './index.module.scss';
import Shop from './modules/Shop/Index';
import SpecialEdition from './modules/SpecialEdition/Index';
function Collection() {
	const [activeKey, setActiveKey] = useState(0);
	const [isDesc, { toggle, setFalse }] = useBoolean(true);
	const [activeIndex, setActiveIndex] = useState('1');
	// 显示删除
	const [isEdit, setIsEdit] = useState(false);
	// 删除 ids
	const [delIds, setDelIds] = useState<number[]>([]);
	// 特辑组件引用
	const specialRef = useRef(null);
	useEffect(() => {
		if (!localStorage.getItem('collectionTabKey')) return;
		setActiveIndex(localStorage.getItem('collectionTabKey'));
		localStorage.removeItem('collectionTabKey');
	}, []);
	const renderDescend = () => {
		const sortWay = isDesc ? 'desc' : 'asc';
		if (sortWay === 'desc') {
			return <PriceDescendingOrder width={14} height={14} />;
		} else if (sortWay === 'asc') {
			return <PriceAscendingOrder width={14} height={14} />;
		}
		return <PriceSorting width={7} height={9} />;
	};
	// 右侧操作点击
	const operateClick = () => {
		if (!delIds.length) {
			setIsEdit(!isEdit);
		} else {
			CustomModal.confirm({
				content: window._$m.t('确定从特辑中删除吗?'),
				cancelText: window._$m.t('取消'),
				confirmText: window._$m.t('确定'),
				onConfirm: async () => {
					const res =
						await api.easyMarket.collection.removeCollectionList(
							delIds.map((i) => Number(i))
						);
					if (res.success) {
						specialRef.current.delSpecial(delIds);
						setIsEdit(false);
						setDelIds([]);
						Toast.show({
							content: window._$m.t('删除成功')
						});
					}
				}
			});
		}
	};

	return (
		<div className={classNames('layout-style')}>
			<ScrollPage
				title={
					<>
						<CustomNavbar
							title={window._$m.t('收藏')}
							navBarRight={
								activeIndex === '2' ? (
									<div
										className="editIcon"
										onClick={operateClick}
									>
										{!isEdit ? (
											<EditAddress />
										) : (
											<DeleteLine />
										)}
									</div>
								) : (
									<></>
								)
							}
						/>

						<div className={styles.header}>
							<Tabs
								activeKey={activeKey.toString()}
								onChange={(key) => {
									setActiveKey(Number(key));
									setFalse();
								}}
								activeLineMode="fixed"
								style={{
									'--active-line-height': '0.04rem',
									'--title-font-size': '0.16rem',
									'--active-title-color': '#1C2026',
									'--active-line-color': '#FF5010',
									'--fixed-active-line-width': '0.24rem',
									'--content-padding': '.1rem 0 0 0'
								}}
							/>

							<Tabs
								activeKey={activeIndex}
								onChange={(key) => {
									setActiveIndex(key);
								}}
								style={{
									'--active-line-color': '#FF5010',
									'--active-line-height': '0.04rem',
									'--fixed-active-line-width': '0.24rem'
								}}
							>
								<Tabs.Tab
									title={window._$m.t('商品')}
									key="1"
								/>

								<Tabs.Tab
									title={window._$m.t('特辑')}
									key="2"
								/>
							</Tabs>
							{activeIndex === '1' && (
								<div className={styles['collection-time']}>
									<div
										style={{
											marginRight: '.04rem'
										}}
										onClick={toggle}
									>
										{window._$m.t('收藏时间')}
									</div>
									{renderDescend()}
								</div>
							)}
						</div>
					</>
				}
			>
				{activeIndex === '1' ? (
					<Shop isDesc={isDesc} />
				) : (
					<SpecialEdition
						ref={specialRef}
						isEdit={isEdit}
						delIds={delIds}
						setDelIds={setDelIds}
					/>
				)}
			</ScrollPage>
		</div>
	);
}
export default Collection;
