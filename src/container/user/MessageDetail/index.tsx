/*
 * @Author: yusha
 * @Date: 2023-11-23 14:36:38
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-08 11:40:08
 * @Description: 消息详情
 */

import { memo, useState } from 'react';
import {
	createSearchParams,
	useNavigate,
	useSearchParams
} from 'react-router-dom';
import { useAsyncEffect, useRequest } from 'ahooks';
import classNames from 'classnames';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import { NotifyPrivateVO } from '@/service/customer';
import Loading from '@/component/Loading';
import { formatDateCN2JP } from '@/utils';
import ScrollPage from '@/common/ScrollPage';
import styles from './index.module.scss';
import {
	ENUM_HYPER_LINK_KEY,
	NotifyLinkValue,
	formatNotifyContent,
	getNotifyValMap
} from './config';
const MessageDetail = () => {
	const [detail, setDetail] = useState<NotifyPrivateVO>({});
	const [searchParams] = useSearchParams();
	const navigate = useNavigate();
	/** 消息详情 */
	const { runAsync: privateQueryById, loading } = useRequest(
		request.customer.notify.privateQueryById,
		{
			manual: true
		}
	);
	/** 通知内容 */
	const notifyContent = formatNotifyContent(detail);
	/** 消息跳转页面过滤 */
	const hyperLinkArray = Object.values(ENUM_HYPER_LINK_KEY);
	useAsyncEffect(async () => {
		const id = searchParams.get('id');
		const data = await privateQueryById({
			notifyTemplatePrivateId: id!
		});
		setDetail(data.data ?? {});
	}, []);
	/** 跳转到对应页面 */
	const formatHyperlink = () => {
		const notifyMapVal = getNotifyValMap.get(
			detail.hyperlink as ENUM_HYPER_LINK_KEY
		) as NotifyLinkValue;
		const obj: {
			params0: string;
		} = JSON.parse(detail?.hyperlinkParams as string);
		if (obj.params0 && notifyMapVal.extra) {
			navigate({
				pathname: notifyMapVal.routeLink,
				search: `?${createSearchParams({
					[notifyMapVal.extra]: obj.params0
				})}`
			});
			return;
		}
		navigate({
			pathname: notifyMapVal.routeLink
		});
	};
	return (
		<ScrollPage
			title={<CustomNavbar fixed={false} title={window._$m.t('详情')} />}
		>
			{loading && <Loading />}
			<div className={classNames('layout-style')}>
				<div className={styles['message-detail']}>
					<div className={styles['title']}>{detail?.title}</div>
					<div className={styles['create-time']}>
						{formatDateCN2JP(detail?.createTime)}
					</div>
					<div className={styles['content']}>
						{notifyContent?.map((item, index) => {
							return (
								<div key={index}>
									{
										/** 最后一条消息为按钮，点击跳转到对应页面 */
										index === notifyContent.length - 1 &&
										hyperLinkArray.includes(
											detail.hyperlink as ENUM_HYPER_LINK_KEY
										) ? (
											<a
												className={styles['link']}
												onClick={formatHyperlink}
											>
												{item}
											</a>
										) : (
											<span>{item}</span>
										)
									}
								</div>
							);
						})}
					</div>
				</div>
			</div>
		</ScrollPage>
	);
};
export default memo(MessageDetail);
