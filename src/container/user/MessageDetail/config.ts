/*
 * @Author: liuliangliang liuliangliang@sniffgroup.com
 * @Date: 2023-11-09 13:31:53
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-23 20:24:50
 * @FilePath: /theckbsniffmobile/App/page/MessageScreen/MessageDetail/config.tsx
 * @Description: 消息通知/详情/配置
 */

import { NotifyPrivateVO } from '@/service/customer';

/** 消息通知接口返回枚举KEY */
export enum ENUM_HYPER_LINK_KEY {
	/** 钱包-充值 */
	WALLET_INDEX = 'walletIndex',
	/** 订单详情 */
	ORDER_DETAIL = 'orderDetail',
	/** 退款详情 */
	REFUND_DETAIL = 'refundDetail'
}

/** App路由跳转路由枚举 */
export enum ENUM_NOTIFY_ROUTE {
	/** 钱包-充值 */
	WALLET_INDEX = '/user/wallet',
	/** 订单详情 */
	ORDER_DETAIL = '/order/detail',
	/** 退款详情 */
	REFUND_DETAIL = '/user/refund'
}

/** 消息通知MapValue */
export interface NotifyLinkValue {
	/** app对应页面路由 */
	routeLink?: ENUM_NOTIFY_ROUTE;
	/** 业务场景描述 */
	description?: string;
	/** 文章对应ID */
	frogArticleId?: number;
	/** 对应操作 */
	action?: boolean;
	/** 额外的字段 */
	extra?: string;
}

/** 消息通知-获取场景- */
export const getNotifyValMap: Map<ENUM_HYPER_LINK_KEY, NotifyLinkValue> =
	new Map([
		[
			ENUM_HYPER_LINK_KEY.WALLET_INDEX,
			{
				routeLink: ENUM_NOTIFY_ROUTE.WALLET_INDEX,
				description: window._$m.t(
					'钱包-余额充值成功，钱包-提现成功，钱包-提现失败'
				)
			}
		],

		[
			ENUM_HYPER_LINK_KEY.ORDER_DETAIL,
			{
				routeLink: ENUM_NOTIFY_ROUTE.ORDER_DETAIL,
				description: window._$m.t('订单详情'),
				extra: 'orderNo',
				action: true
			}
		],

		[
			ENUM_HYPER_LINK_KEY.REFUND_DETAIL,
			{
				routeLink: ENUM_NOTIFY_ROUTE.REFUND_DETAIL,
				description: window._$m.t('退款记录'),
				extra: 'orderNo',
				action: true
			}
		]
	]);

/** 处理消息通知-内容 */
export const formatNotifyContent = (routeParams: NotifyPrivateVO) => {
	const str = routeParams.content;
	if (str?.indexOf('\n') !== -1) return str?.split('\n');
	return str
		.replace(/(.*)([!！.。;；，,@#$%^&*])(.*)$/, '$1\n$3')
		.split('\n');
};

/** 处理消息通知-公共方法 */
export const handleCommon = (notifyLinkValue: NotifyLinkValue) => {
	// NavigationService.navigate(notifyLinkValue.routeLink);
};
