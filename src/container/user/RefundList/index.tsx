/*
 * @Date: 2024-01-30 18:31:37
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-04-02 13:39:18
 * @FilePath: /ckb-easy-h5-fed/src/container/user/RefundList/index.tsx
 * @Description:
 */
import { memo, useState, useRef, useEffect } from 'react';
import { useLocation, useSearchParams } from 'react-router-dom';
import { Input, Toast } from 'antd-mobile';
import classNames from 'classnames';
import copy from 'copy-to-clipboard';
import ExpandDefault from '@/common/icons/ExpandDefault';
import { EnumRefundChannel } from '@/common/Enums';
import { request } from '@/config/request';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import CustomModal from '@/component/CustomModal';
import { QueryRefundReq, RefundDTO } from '@/service/easyOrder';
import { useTurnPage } from '@/utils/hooks/useTurnPage';
import { formatDateCN2JP } from '@/utils';
import { RefundTime } from './RefundTime';
import { RefundStatus } from './RefundStatus';
import { Channel } from './Channel';
import styles from './index.module.scss';
import RefundInfoModal from './RefundInfoModal';
const RefundList = () => {
	const routeLocation = useLocation();
	const { orderNo, status } = routeLocation.state || {};
	const [locationSearchParams] = useSearchParams();
	const searchOrderNo = locationSearchParams.get('orderNo')!;
	const containerRef = useRef(null);
	const [scrollOpcity, setScrollOpcity] = useState(0);
	const [val, setVal] = useState(orderNo || searchOrderNo);

	// 订单搜素
	const handleSearch = () => {
		setSearchParams({
			...searchParams,
			orderNo: val
		});
	};
	/** 退款详情 */
	async function refundTypeClick(item: RefundDTO) {
		const { data } =
			await request.easyOrder.refundSearch.queryRefundDeatail({
				refundNo: item.refundNo
			});
		CustomModal.confirm({
			title: window._$m.t('商品代金'),
			content: (
				<RefundInfoModal
					productNumber={data?.productNo ?? ''}
					productSKU={data?.productSku ?? ''}
					productImage={data?.productImg ?? ''}
					unitPrice={String(data?.productPrice) ?? ''}
					refundQuantity={data?.refundNum ?? 0}
					refundAmount={data.refundAmount ?? 0}
				/>
			),

			confirmText: window._$m.t('我知道了'),
			showCloseButton: false,
			className: 'confirm-voucher-url'
		});
	}
	const [searchParams, setSearchParams] = useState<QueryRefundReq>({
		orderNo: orderNo || searchOrderNo || '',
		status: status || ''
	});
	const [refundList, RenderTurnpage, { setParams: setSearchParamsCallback }] =
		useTurnPage<RefundDTO, QueryRefundReq>({
			params: searchParams,
			pageSize: 10,
			emptyTips: window._$m.t('暂无数据'),
			request: (params) => {
				return request.easyOrder.refundSearch.queryRefundPage(params, {
					useMock: false
				});
			}
		});
	useEffect(() => {
		setSearchParamsCallback(searchParams);
	}, [searchParams, setSearchParamsCallback]);

	// 复制商品号
	const handleCopy = (orderCode) => {
		if (copy(orderCode!)) {
			Toast.show(window._$m.t('复制成功'));
		}
	};
	return (
		<>
			<div
				className="layout-style"
				ref={containerRef}
				onScroll={(e) => {
					const container: any = containerRef.current;
					const scrollTop = container.scrollTop;
					// 顶部
					const opcity = scrollTop / 100 > 1 ? 1 : scrollTop / 100;
					setScrollOpcity(opcity);
				}}
			>
				<div className={styles.page}>
					<div
						style={{
							position: 'fixed',
							left: 0,
							right: 0,
							backgroundColor: `rgba(240, 242, 245,${scrollOpcity})`,
							top: 0,
							zIndex: 1,
							paddingTop: '0.12rem'
						}}
					>
						<CustomNavbar title={window._$m.t('退款记录')} />
					</div>
					<div className={styles.inputWrap}>
						<Input
							style={{
								'--font-size': '0.12rem'
							}}
							defaultValue={searchOrderNo || orderNo}
							placeholder={window._$m.t('订单编号搜索')}
							className={styles.input}
							onChange={setVal}
							clearable
						/>

						<div
							style={{
								flexShrink: 0,
								color: '#232323',
								width: '0.16rem',
								height: '0.16rem'
							}}
							onClick={handleSearch}
						>
							<img
								style={{
									width: '0.16rem',
									height: '0.16rem'
								}}
								alt=""
								src="https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_search@2x.png"
							/>
						</div>
					</div>
					<div className={classNames(styles.tableHeader)}>
						<div
							style={{
								display: 'flex',
								justifyContent: 'space-between'
							}}
						>
							<div
								style={{
									display: 'flex',
									justifyContent: 'space-between',
									flexDirection: 'row'
								}}
							>
								<div
									className={styles.fieldName}
									style={{
										marginRight: '.12rem'
									}}
								>
									<RefundStatus
										value={status}
										onChange={(bizType) => {
											setSearchParams({
												...searchParams,
												status: bizType
											});
										}}
									>
										<div
											style={{
												marginRight: '.04rem',
												fontWeight: 500,
												fontSize: '.13rem',
												lineHeight: '.2rem',
												color: '#1C2026'
											}}
										>
											{window._$m.t('状态')}
										</div>
									</RefundStatus>
									<ExpandDefault className={styles.arrow} />
								</div>
								<div className={styles.fieldName}>
									<Channel
										onChange={(bizType) => {
											setSearchParams({
												...searchParams,
												refundTypeCode: bizType
											});
										}}
									>
										<div
											style={{
												marginRight: '.04rem',
												fontWeight: 500,
												fontSize: '.13rem',
												lineHeight: '.2rem',
												color: '#1C2026'
											}}
										>
											{window._$m.t('渠道')}
										</div>
									</Channel>
									<ExpandDefault className={styles.arrow} />
								</div>
							</div>

							<div className={styles.fieldName}>
								<RefundTime
									onChange={(time) => {
										setSearchParams({
											...searchParams,
											refundTimeStart: time.startTime,
											refundTimeEnd: time.endTime
										});
									}}
								/>
							</div>
						</div>
					</div>
					<div className={styles.refundList}>
						{refundList.map((item, index) => {
							return (
								<div key={index} className={styles.recordItems}>
									<div
										className={classNames(
											styles.recordFirst
										)}
									>
										<div className={styles.channel}>
											{
												EnumRefundChannel[
													item.refundTypeCode!
												]?.cn
											}
										</div>
										<div className={styles.desc}>
											<span>
												{window._$m.t('退款单编号 ：')}
											</span>
											<span className={styles.value}>
												{item.refundNo}
											</span>
										</div>
										<div className={styles.desc}>
											<span>
												{window._$m.t('退款账户 ：')}
											</span>
											<span className={styles.value}>
												{item.customerName}
											</span>
										</div>
										<div className={styles.desc}>
											<span>
												{window._$m.t(
													'申请退款时间 ：'
												)}
											</span>
											<span className={styles.value}>
												{formatDateCN2JP(
													item.applyRefundTime
												) || '-'}
											</span>
										</div>
										<div className={styles.desc}>
											<span>
												{window._$m.t(
													'退款成功时间 ：'
												)}
											</span>
											<span className={styles.value}>
												{formatDateCN2JP(
													item.refundTime
												) || '-'}
											</span>
										</div>
										<div className={styles.desc}>
											<span>
												{window._$m.t('订单编号 ：')}
											</span>
											<span className={styles.value}>
												{item.orderNo}
											</span>
											<img
												className={styles.copy}
												onClick={() =>
													handleCopy(item.orderNo)
												}
												src="https://static-jp.theckb.com/static-asset/easy-h5/copy.svg"
												alt="copy"
											/>
										</div>
										<div className={styles.desc}>
											<span>
												{window._$m.t('退款类型 ：')}
											</span>
											<span className={styles.value}>
												{item.refundType === 1 &&
													window._$m.t('商品代金')}

												{item.refundType === 2 &&
													window._$m.t('国际运费')}
											</span>
											{item.refundType === 1 && (
												<div
													className={styles.details}
													onClick={() => {
														refundTypeClick(item);
													}}
												>
													<span>
														{window._$m.t('详情')}
													</span>
													<img
														src="https://static-jp.theckb.com/static-asset/easy-h5/details.svg"
														alt="details"
													/>
												</div>
											)}
										</div>
										<div className={styles.desc}>
											<span>
												{window._$m.t('退款金额 ：')}
											</span>
											<span className={styles.value}>
												{item.refundAmount}
												{window._$m.t('円')}
											</span>
										</div>
										<div
											className={styles.desc}
											style={{
												display: 'grid',
												gridTemplateColumns:
													'auto minmax(0, 1fr)',
												alignItems: 'center',
												gap: '0.06rem',
												color: '#898989',
												fontSize: '0.12rem',
												lineHeight: '0.18rem'
											}}
										>
											<span>
												{window._$m.t('退款流水号 ：')}
											</span>
											<span className={styles.value}>
												{item.refundChannelNo || '-'}
											</span>
										</div>
										<div
											className={styles.desc}
											style={{
												display: 'grid',
												gridTemplateColumns:
													'auto minmax(0, 1fr)',
												alignItems: 'center',
												gap: '0.06rem',
												color: '#898989',
												fontSize: '0.12rem',
												lineHeight: '0.18rem'
											}}
										>
											<span>
												{window._$m.t('原因 ：')}
											</span>
											<span className={styles.value}>
												{item.reasonDesc || '-'}
											</span>
										</div>
									</div>

									{item.status === 1 && (
										<div className={styles.refunding}>
											{window._$m.t('退款中')}
										</div>
									)}

									{item.status === 2 && (
										<div className={styles.refunded}>
											{window._$m.t('已退款')}
										</div>
									)}
								</div>
							);
						})}
					</div>
					{RenderTurnpage}
				</div>
			</div>
		</>
	);
};
export default memo(RefundList);
