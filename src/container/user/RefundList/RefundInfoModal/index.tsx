import React from 'react';
import CustomModal from '@/component/CustomModal';
interface ProductInfoModalProps {
	productNumber: string;
	productSKU: string;
	productImage: string;
	unitPrice: string;
	refundQuantity: number;
	refundAmount: number;
}
const RefundInfoModal: React.FC<ProductInfoModalProps> = ({
	productNumber,
	productSKU,
	productImage,
	unitPrice,
	refundQuantity,
	refundAmount
}) => {
	const clickShowTips = () => {
		CustomModal.confirm({
			content: '表示された金額は実際のお支払い金額です。',
			showCloseButton: false
		});
	};
	return (
		<div>
			<div
				style={{
					textAlign: 'left',
					display: 'flex',
					flexDirection: 'column',
					gap: '.06rem',
					color: '#898989',
					fontSize: '.12rem',
					lineHeight: '.18rem'
				}}
			>
				<div>
					{window._$m.t('商品番号：')}
					<span
						style={{
							color: '#111'
						}}
					>
						{productNumber}
					</span>
				</div>
				<div
					style={{
						display: 'grid',
						gridTemplateColumns: 'auto minmax(0, 1fr)',
						alignItems: 'center',
						gap: '0.06rem',
						color: '#898989',
						fontSize: '0.12rem',
						lineHeight: '0.18rem'
					}}
				>
					<span>{window._$m.t('商品SKU：')}</span>
					<span
						style={{
							color: '#111',
							wordBreak: 'break-word',
							whiteSpace: 'normal',
							width: '80%'
						}}
					>
						{productSKU}
					</span>
				</div>
				<div
					style={{
						display: 'grid',
						gridTemplateColumns: 'auto minmax(0, 1fr)',
						alignItems: 'center',
						gap: '0.06rem',
						color: '#898989',
						fontSize: '0.12rem',
						lineHeight: '0.18rem'
					}}
				>
					<span>{window._$m.t('退款商品图像：')}</span>
					<img
						style={{
							width: '.4rem',
							height: '.4rem',
							borderRadius: '.04rem',
							backgroundColor: '#eee'
						}}
						src={productImage}
						alt=""
					/>
				</div>
				<div>
					{window._$m.t('单价：')}
					<span
						style={{
							color: '#111'
						}}
					>
						{unitPrice}
						{window._$m.t('円')}
					</span>
				</div>
				<div>
					{window._$m.t('申请退款数量：')}
					<span
						style={{
							color: '#111'
						}}
					>
						{refundQuantity}
					</span>
				</div>
				<div style={{ display: 'flex' }}>
					<div>{window._$m.t('退款金额 ：')}</div>
					<span
						style={{
							color: '#111'
						}}
					>
						{refundAmount}
						{window._$m.t('円')}
					</span>
				</div>
				<div
					style={{
						display: 'flex',
						alignItems: 'center',
						marginTop: '-0.05rem'
					}}
				>
					<img
						src="https://static-jp.theckb.com/static-asset/easy-h5/refound-import.png"
						alt="imports"
						style={{ width: '0.12rem', height: '0.12rem' }}
					/>
					<span
						style={{
							fontSize: '0.10rem',
							color: 'rgba(255, 80, 16, 1)',
							marginLeft: '0.04rem'
						}}
					>
						{window._$m.t('显示金额为实际支付金额')}
					</span>
				</div>
			</div>
		</div>
	);
};
export default RefundInfoModal;
