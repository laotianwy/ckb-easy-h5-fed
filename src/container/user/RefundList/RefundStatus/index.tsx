/*
 * @Author: huajian
 * @Date: 2023-10-30 15:47:39
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-08 17:19:10
 * @Description: 状态弹窗
 */
import { useState } from 'react';
import CustomPopup from '@/component/CustomPopup';
import { EnumRefundStatus } from '@/common/Enums';
import styles from './index.module.scss';
interface Props {
	children: React.ReactNode;
	value?: string;
	onChange: (bizType: number) => void;
}
const getCNByCode = (code: string | undefined | number) => {
	const keys = Object.keys(EnumRefundStatus);
	for (const key of keys) {
		if (EnumRefundStatus[key].code === code) {
			return EnumRefundStatus[key].cn;
		}
	}
};
export const RefundStatus: React.FC<Props> = (props) => {
	const [visible, setVisible] = useState(false);
	const [child, setChild] = useState<string | React.ReactNode>(
		getCNByCode(props.value) ?? props.children
	);
	return (
		<>
			<CustomPopup visible={visible} onClose={() => setVisible(false)}>
				<div className={styles.container}>
					<div className={styles.title}>
						{window._$m.t('退款状态')}
					</div>
					<div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumRefundStatus.ALL.cn
									? 'true'
									: 'fasle'
							}
							onClick={() => {
								setChild(EnumRefundStatus.ALL.cn);
								setVisible(false);
								props.onChange(
									EnumRefundStatus.ALL.code as any
								);
							}}
						>
							<div className={styles.item}>
								{EnumRefundStatus.ALL.cn}
							</div>
							{child === EnumRefundStatus.ALL.cn && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumRefundStatus.RECHARGE.cn()
									? 'true'
									: 'fasle'
							}
							onClick={() => {
								setChild(EnumRefundStatus.RECHARGE.cn);
								setVisible(false);
								props.onChange(
									EnumRefundStatus.RECHARGE.code as any
								);
							}}
						>
							<div className={styles.item}>
								{EnumRefundStatus.RECHARGE.cn()}
							</div>
							{child === EnumRefundStatus.RECHARGE.cn() && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumRefundStatus.PAY.cn()
									? 'true'
									: 'fasle'
							}
							onClick={() => {
								setChild(EnumRefundStatus.PAY.cn);
								setVisible(false);
								props.onChange(
									EnumRefundStatus.PAY.code as any
								);
							}}
						>
							<div className={styles.item}>
								{EnumRefundStatus.PAY.cn()}
							</div>
							{child === EnumRefundStatus.PAY.cn() && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
					</div>
				</div>
			</CustomPopup>
			<span onClick={() => setVisible(true)}>{child}</span>
		</>
	);
};
