import { Form } from 'antd-mobile';
import { useState } from 'react';
import CustomPopup from '@/component/CustomPopup';
import { EnumPayType } from '@/common/Enums';
import styles from './index.module.scss';
export const downloadFile = (data: BlobPart, fileName: string) => {
	const url = window.URL.createObjectURL(new Blob([data]));
	const a = document.createElement('a');
	a.href = url;
	a.setAttribute('download', fileName);
	document.body.appendChild(a);
	a.click();
	URL.revokeObjectURL(a.href);
	document.body.removeChild(a);
};
interface Props {
	children: React.ReactNode;
	onChange: (bizType: keyof typeof EnumPayType) => void;
}
export const Channel: React.FC<Props> = (props) => {
	const [form] = Form.useForm();
	const [visible, setVisible] = useState(false);
	const [child, setChild] = useState<string | React.ReactNode>(
		props.children
	);
	return (
		<>
			<CustomPopup visible={visible} onClose={() => setVisible(false)}>
				<div className={styles.container}>
					<div className={styles.title}>{window._$m.t('渠道')}</div>
					<div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumPayType.ALL.cn ? 'true' : 'fasle'
							}
							onClick={() => {
								setChild(EnumPayType.ALL.cn);
								setVisible(false);
								props.onChange(EnumPayType.ALL.code as any);
							}}
						>
							<div className={styles.item}>
								{EnumPayType.ALL.cn}
							</div>
							{child === EnumPayType.ALL.cn && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumPayType.PAYPAL.cn
									? 'true'
									: 'fasle'
							}
							onClick={() => {
								setChild(EnumPayType.PAYPAL.cn);
								setVisible(false);
								props.onChange(EnumPayType.PAYPAL.code as any);
							}}
						>
							<div className={styles.item}>
								{EnumPayType.PAYPAL.cn}
							</div>
							{child === EnumPayType.PAYPAL.cn && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumPayType.PAYPAL_CREDIT.cn
									? 'true'
									: 'fasle'
							}
							onClick={() => {
								setChild(EnumPayType.PAYPAL_CREDIT.cn);
								setVisible(false);
								props.onChange(
									EnumPayType.PAYPAL_CREDIT.code as any
								);
							}}
						>
							<div className={styles.item}>
								{EnumPayType.PAYPAL_CREDIT.cn}
							</div>
							{child === EnumPayType.PAYPAL_CREDIT.cn && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
					</div>
				</div>
			</CustomPopup>
			<span onClick={() => setVisible(true)}>{child}</span>
		</>
	);
};
