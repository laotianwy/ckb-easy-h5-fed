/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-08 13:27:48
 * @Description: 钱包
 */
import { useRef, useState } from 'react';
import { useMount } from 'ahooks';
import { AccountResp, AccountTransLogResp } from '@/service/pay';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import { useTurnPage } from '@/utils/hooks/useTurnPage';
import ScrollPage from '@/common/ScrollPage';
import { MoHeaderCard } from './MoHeaderCard';
import { MoRecords } from './MoRecords';
import './index.scss';
const Wallet = () => {
	// const ref = useRef<HTMLDivElement>(null);
	const [info, setInfo] = useState<AccountResp>();
	useMount(() => {
		request.pay.front.accountQuery().then((res) => {
			setInfo(res.data);
		});
	});
	const [records, RenderTurnpage, { setParams, reqParams }] =
		useTurnPage<AccountTransLogResp>({
			params: {},
			request: (params) => {
				return request.pay.front.accountQueryLogPage(params);
			}
		});
	return (
		<ScrollPage
			className="wallet-page"
			title={
				<CustomNavbar
					title={window._$m.t('资金钱包')}
					className="wallet-background"
				/>
			}
		>
			<div
				style={{
					padding: '0 .12rem',
					marginTop: '0.57rem'
				}}
				className="layout-content"
			>
				<MoHeaderCard info={info} />
				<MoRecords
					records={records}
					onChange={(val) =>
						setParams({
							...reqParams,
							bizType: val
						})
					}
					onChangeTime={(time) => {
						setParams({
							...reqParams,
							beginTime: time?.startTime,
							endTime: time?.endTime
						});
					}}
					onSearchBizNo={(val) =>
						setParams({
							...reqParams,
							relationBizNo: val
						})
					}
				/>
			</div>
			<div
				className={`empty-page ${!records.length ? '' : 'data-style'}`}
			>
				{RenderTurnpage}
			</div>
		</ScrollPage>
	);
};
export default Wallet;
