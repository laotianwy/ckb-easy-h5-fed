/*
 * @Author: huajian
 * @Date: 2023-10-30 15:47:39
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-08 17:19:10
 * @Description: 花费弹窗
 */

import { Form } from 'antd-mobile';
import { useState } from 'react';
import CustomPopup from '@/component/CustomPopup';
import { EnumBizType } from '@/common/Enums';
import styles from './index.module.scss';
export const downloadFile = (data: BlobPart, fileName: string) => {
	const url = window.URL.createObjectURL(new Blob([data]));
	const a = document.createElement('a');
	a.href = url;
	a.setAttribute('download', fileName);
	document.body.appendChild(a);
	a.click();
	URL.revokeObjectURL(a.href);
	document.body.removeChild(a);
};
interface Props {
	children: React.ReactNode;
	onChange: (bizType: keyof typeof EnumBizType) => void;
}
export const CostPop: React.FC<Props> = (props) => {
	const [form] = Form.useForm();
	const [visible, setVisible] = useState(false);
	const [child, setChild] = useState<string | React.ReactNode>(
		props.children
	);
	return (
		<>
			<CustomPopup visible={visible} onClose={() => setVisible(false)}>
				<div className={styles.container}>
					<div className={styles.title}>
						{window._$m.t('费用类型')}
					</div>
					<div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumBizType.ALL.cn ? 'true' : 'fasle'
							}
							onClick={() => {
								setChild(EnumBizType.ALL.cn);
								setVisible(false);
								props.onChange(EnumBizType.ALL.code as any);
							}}
						>
							<div className={styles.item}>
								{EnumBizType.ALL.cn}
							</div>
							{child === EnumBizType.ALL.cn && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumBizType.RECHARGE.cn
									? 'true'
									: 'fasle'
							}
							onClick={() => {
								setChild(EnumBizType.RECHARGE.cn);
								setVisible(false);
								props.onChange(
									EnumBizType.RECHARGE.code as any
								);
							}}
						>
							<div className={styles.item}>
								{EnumBizType.RECHARGE.cn}
							</div>
							{child === EnumBizType.RECHARGE.cn && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
						<div
							className={styles.itemWrap}
							data-selected={
								child === EnumBizType.PAY.cn ? 'true' : 'fasle'
							}
							onClick={() => {
								setChild(EnumBizType.PAY.cn);
								setVisible(false);
								props.onChange(EnumBizType.PAY.code as any);
							}}
						>
							<div className={styles.item}>
								{EnumBizType.PAY.cn}
							</div>
							{child === EnumBizType.PAY.cn && (
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
								/>
							)}
						</div>
					</div>
				</div>
			</CustomPopup>
			<span onClick={() => setVisible(true)}>{child}</span>
		</>
	);
};
