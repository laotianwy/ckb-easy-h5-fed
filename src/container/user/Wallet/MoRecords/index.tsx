/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-20 17:37:42
 * @Description: 钱包
 */
import { Input } from 'antd-mobile';
import classNames from 'classnames';
import { useState } from 'react';
import { AccountTransLogResp } from '@/service/pay';
import ExpandDefault from '@/common/icons/ExpandDefault';
import { EnumBizType } from '@/common/Enums';
import { formatDateCN2JP, formatMoney } from '@/utils';
import { CustomDatePicker } from '@/component/CustomDatePicker';
import styles from './index.module.scss';
import { CostPop } from './CostPop';
enum Direct {
	AMOUNT_IN = '+',
	AMOUNT_OUT = '-'
}
interface Props {
	records: AccountTransLogResp[];
	onChange: (bizType: EnumBizType) => void;
	onChangeTime: (time: { startTime?: string; endTime?: string }) => void;
	onSearchBizNo: (bizNO: string) => void;
}
export const MoRecords: React.FC<Props> = ({
	records,
	onChange,
	onChangeTime,
	onSearchBizNo
}) => {
	const [val, setVal] = useState('');
	return (
		<div className={classNames(styles.container)}>
			<div className={styles.searchWrap}>
				<div
					style={{
						fontSize: '.14rem',
						lineHeight: '.22rem',
						fontWeight: 'bold',
						marginBottom: '.12rem',
						color: '#1C2026'
					}}
				>
					{window._$m.t('流水明细')}
				</div>
				<div className={styles.inputWrap}>
					<Input
						style={{
							'--font-size': '0.12rem'
						}}
						placeholder={window._$m.t('订单编号搜索')}
						className={styles.input}
						onChange={setVal}
						clearable
					/>

					<div
						onClick={() => onSearchBizNo(val)}
						style={{
							flexShrink: 0,
							color: '#232323',
							width: '0.16rem',
							height: '0.16rem'
						}}
					>
						<img
							style={{
								width: '0.16rem',
								height: '0.16rem'
							}}
							alt=""
							src="https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_search@2x.png"
						/>
					</div>
				</div>
			</div>
			<div
				className={classNames(
					styles.tableHeader,
					records.length ? '' : styles['no-box-shadow']
				)}
			>
				<div className={styles.fieldName}>
					<CostPop onChange={onChange}>
						<div
							style={{
								marginRight: '.04rem',
								fontWeight: 500,
								fontSize: '.13rem',
								lineHeight: '.2rem',
								color: '#1C2026'
							}}
						>
							{window._$m.t('费用类型')}
						</div>
					</CostPop>
					<ExpandDefault className={styles.arrow} />
				</div>
				<div className={styles.fieldName}>
					<CustomDatePicker onChange={onChangeTime} />
				</div>
			</div>
			<div className={styles.billList}>
				{records.map((item, index) => {
					return (
						<div key={index} className={styles.recordItems}>
							<div
								className={classNames(
									styles.recordItem,
									styles.recordFirst
								)}
							>
								{!!EnumBizType[item.bizType!] && (
									<div>{EnumBizType[item.bizType!].cn}</div>
								)}

								<div className={styles.desc}>
									{window._$m.t('订单编号')}：
									{item.relationBizNo}
								</div>
								<div className={styles.desc}>
									{window._$m.t('流水号：')}
									{item.transLogId}
								</div>
							</div>

							<div className={styles.recordItem}>
								<div>
									{Direct[item.direct!]}
									{formatMoney(item.amount)}
									{window._$m.t('円')}
								</div>
								<div className={styles.desc}>
									{formatDateCN2JP(item.transTime)}
								</div>
							</div>
						</div>
					);
				})}
			</div>
		</div>
	);
};
