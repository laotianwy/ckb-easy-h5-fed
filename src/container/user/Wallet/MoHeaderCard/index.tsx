/*
 * @Author: huajian
 * @Date: 2023-10-31 20:26:54
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-14 20:59:54
 * @Description: 头部卡片
 */
import { useNavigate } from 'react-router-dom';
import { AccountResp } from '@/service/pay';
import { toThousands } from '@/utils';
import styles from './index.module.scss';
interface Props {
	info?: AccountResp;
}
export const MoHeaderCard: React.FC<Props> = ({ info }) => {
	const navigate = useNavigate();
	const goRecharge = () => {
		navigate('/user/offlineRecharge');
	};
	return (
		<div className={styles.container}>
			<div className={styles.chargeBtn} onClick={goRecharge}>
				{window._$m.t('充值')}
			</div>
			<div className={styles.title}>{window._$m.t('可用余额')}</div>
			<div className={styles.priceWrap}>
				<span className={styles.price}>
					{toThousands(info?.availableBalance || 0)}
				</span>
				<span className={styles.unit}>{window._$m.t('円')}</span>
			</div>
			<div className={styles.preSettleBalanceWrap}>
				<span className={styles.preSettleDesc}>
					{window._$m.t('未入账金额')}
				</span>
				<span
					style={{
						fontSize: '.12rem'
					}}
				>
					{toThousands(info?.preSettleBalance) || 0}
					{window._$m.t('円')}
				</span>
			</div>
		</div>
	);
};
