/*
 * @Date: 2024-01-08 15:51:04
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 15:43:05
 * @FilePath: /ckb-easy-h5-fed/src/container/user/UsageGuide/index.tsx
 * @Description:
 */
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { useEffect, useRef, useState } from 'react';
import dayjs from 'dayjs';
import './index.scss';
import {
	AutoCenter,
	InfiniteScroll,
	List,
	SpinLoading,
	Tabs
} from 'antd-mobile';
import { DownFill } from 'antd-mobile-icons';
import axios from 'axios';
import * as SDK from '@snifftest/sdk/lib/rn';
import { useNavigate } from 'react-router-dom';
import { TaEvent } from '@/outs/utils/TDAnalytics';
import { formatDateCN2JP } from '@/utils';
import ScrollPage from '@/common/ScrollPage';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import { CouponRespDTO } from '@/service/market';

const CouponList = () => {
	const navigation = useNavigate();
	const [couponType, setCouponType] = useState('0');
	const [pageNum, setPageNum] = useState(0);
	const [couponList, setCouponList] = useState<CouponRespDTO[]>([]);
	const [couponOpenList, setCouponOpenList] = useState<Number[]>([]);
	const [couponLoading, setCouponLoading] = useState(true);
	const [hasMore, setHasMore] = useState(true);
	const { runAsync: apiGetCoupon, loading } = useRequest(
		request.market.coupon.couponCustomerPage,
		{
			manual: true
		}
	);
	useEffect(() => {
		/** 进入注册页面埋点 */
		SDK.TA.track({
			event: TaEvent.PAGE_VIEW,
			properties: {
				position: 'my_coupon'
			}
		});
	}, []);
	useEffect(() => {
		const params: any = {
			usedStatus: Number(couponType),
			pageNum: 1,
			pageSize: 2000
		};
		setCouponLoading(true);
		apiGetCoupon(params, { useMock: false }).then((res) => {
			setPageNum(2);
			setCouponList(res.data?.records || []);
			setCouponLoading(false);
		});
	}, [couponType, apiGetCoupon]);

	const loadMore = async () => {
		if (pageNum < 2) return;
		const params: any = {
			usedStatus: Number(couponType),
			pageNum,
			pageSize: 10
		};

		const res = await apiGetCoupon(params, { useMock: false });
		setPageNum(pageNum + 1);
		setCouponList([...couponList, ...(res.data?.records || [])]);
		setHasMore(
			(res.data?.records && res.data?.records?.length > 0) || false
		);
	};
	// 如果open列表中包含index，代表已经展开，点击后则不再展开，如果不包含，则添加index到列表
	const ruleClick = (index) => {
		if (couponOpenList.includes(index)) {
			const list = couponOpenList.filter((item) => item !== index);
			setCouponOpenList(list);
		} else {
			setCouponOpenList([...couponOpenList, index]);
		}
		console.log(index, couponOpenList);
	};
	const isAllCanUse = (item) => {
		if (
			item.containProductList ||
			item.containCategoryList ||
			item.unContainProductList ||
			item.unContainCategoryList
		) {
			return false;
		}
		return true;
	};
	const renderTimeTag = (item) => {
		// 获取时间天数间隔，如果小于一天，返回间隔小时
		const date1 = dayjs(item.startDate);
		const date2 = dayjs(item.endDate);
		const diffDay = date2.diff(date1, 'day');
		const diffHour = date2.diff(date1, 'hour');
		if (diffDay < 3 && diffDay > 0) {
			return (
				<div className="coupon-list-item-time">
					{window._$m.t('剩余')}
					{diffDay + 1}
					{window._$m.t('天')}
				</div>
			);
		} else if (diffDay < 1) {
			return (
				<div className="coupon-list-item-time">
					{window._$m.t('即将到期')}
				</div>
			);
		}
		// else if (diffHour < 24 && diffHour > -1) {
		// 	return (
		// 		<div className="coupon-list-item-time">
		// 			剩余{diffHour + 1}小时
		// 		</div>
		// 	);
		// }
	};
	const renderLeft = (type, item) => {
		// 优惠劵具体优惠类型 1:满多少钱打多少折 2:满多少件减多少钱 3:满多少件打多少折 4:满多少钱减多少钱
		switch (type) {
			case 1:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							<span>
								<span>%</span>
								<span>OFF</span>
							</span>
						</div>
						<div className="coupon-list-item-cut">
							{item.feeToCut}
							{window._$m.t('円可用')}
						</div>
					</>
				);

				break;
			case 2:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							{window._$m.t('円')}
						</div>
						<div className="coupon-list-item-cut">
							{item.numToCut}
							{window._$m.t('件可用')}
						</div>
					</>
				);

				break;
			case 3:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							<span>
								<span>%</span>
								<span>OFF</span>
							</span>
						</div>
						<div className="coupon-list-item-cut">
							{item.numToCut}
							{window._$m.t('件可用')}
						</div>
					</>
				);

				break;
			case 4:
				return (
					<>
						<div>
							<span
								style={{
									fontSize: '0.22rem',
									fontWeight: 'bold'
								}}
							>
								{item.discountValue}
							</span>
							{window._$m.t('円')}
						</div>
						<div className="coupon-list-item-cut">
							{item.feeToCut}
							{window._$m.t('円可用')}
						</div>
					</>
				);

				break;
			default:
				break;
		}
	};
	const ruleText = (list, type) => {
		if (type === 'goods') {
			const text = list.map((item) => {
				return item.productTitle;
			});
			return text.join('、');
		}
		const text = list.map((item) => {
			return item.cateName;
		});
		return text.join('、');
	};
	const renderCoupon = (item, index, type) => {
		return (
			<div style={{ height: '100%', position: 'relative' }}>
				<div
					className="coupon-list-item-content"
					style={{
						backgroundImage: `url(${type ? 'https://static-jp.theckb.com/static-asset/easy-h5/coupon_bg_used.png' : 'https://static-jp.theckb.com/static-asset/easy-h5/coupon_bg.png'})`
					}}
				>
					<div
						className="coupon-list-item-left"
						style={{
							color: `${type ? '#BFBFBF' : '#FF5010'}`
						}}
					>
						{renderLeft(item.couponDetailType, item)}
					</div>
					<div
						className="coupon-list-item-right"
						style={{
							color: `${type ? '#BFBFBF' : '#5B504D'}`
						}}
					>
						<div
							style={{
								fontSize: '0.15rem',
								color: `${type ? '#BFBFBF' : '#242526'}`,
								margin: '0.04rem 0 0.02rem 0',
								fontWeight: '600',
								width: '90%',
								textOverflow: 'ellipsis',
								overflow: 'hidden',
								whiteSpace: 'nowrap'
							}}
						>
							{item.title}
						</div>
						{/* <div
                                                                                                  style={{
                                                                                                  	margin: '0.02rem 0',
                                                                                                  	width: '64%',
                                                                                                  	textOverflow: 'ellipsis',
                                                                                                  	overflow: 'hidden',
                                                                                                  	whiteSpace: 'nowrap'
                                                                                                  }}
                                                                                                  >
                                                                                                  {item.description}
                                                                                                  </div> */}
						<div
							style={{
								margin: '0.02rem 0',
								paddingRight: '0.76rem',
								color: `${type ? 'rgb(191, 191, 191)' : '#7C7371'}`
							}}
						>
							{window._$m.t('有效期')}{' '}
							{formatDateCN2JP(
								item.startDate,
								'YYYY-MM-DD HH:mm'
							)}{' '}
							-{' '}
							{formatDateCN2JP(item.endDate, 'YYYY-MM-DD HH:mm')}
						</div>
						<div
							style={{
								margin: '0.02rem 0 0.06rem 0'
							}}
							onClick={() => {
								ruleClick(index);
							}}
						>
							<span
								style={{
									color: `${type ? 'rgb(191, 191, 191)' : '#7C7371'}`
								}}
							>
								{window._$m.t('规则说明')}
							</span>
							{couponOpenList.includes(index) ? (
								<img
									style={{
										position: 'relative',
										left: '0.03rem',
										top: '0.03rem'
									}}
									src={
										type === 0
											? 'https://static-jp.theckb.com/static-asset/easy-h5/shouqi2.svg'
											: 'https://static-jp.theckb.com/static-asset/easy-h5/shouqi.svg'
									}
									alt=""
								/>
							) : (
								<img
									style={{
										position: 'relative',
										left: '0.03rem',
										top: '0.03rem'
									}}
									src={
										type === 0
											? 'https://static-jp.theckb.com/static-asset/easy-h5/zhankai2.svg'
											: 'https://static-jp.theckb.com/static-asset/easy-h5/zhankai.svg'
									}
									alt=""
								/>
							)}
						</div>
					</div>
				</div>
				{couponOpenList.includes(index) && (
					<div
						className="coupon-list-item-expend"
						style={{
							color: `${type ? 'rgb(191, 191, 191)' : '#8C8C8C'}`
						}}
					>
						<div>
							<div>
								1.{window._$m.t('不可与其他优惠券叠加使用')}
							</div>
							<div>
								2.
								{window._$m.t(
									'点击“去使用”即可查看具体可用商品'
								)}
							</div>
						</div>
					</div>
				)}

				{type === 0 ? (
					<div
						className="coupon-list-item-btn"
						onClick={() => {
							/** 优惠券去使用点击埋点 */
							SDK.TA.track({
								event: TaEvent.BTN_CLICK,
								properties: {
									position: 'go_use',
									source: 'my_coupon'
								}
							});
							const url = `/goods/list/coupon?couponId=${item.couponId}&couponCustomId=${item.id}`;
							navigation(url);
						}}
					>
						{window._$m.t('去使用')}
					</div>
				) : (
					<img
						src={
							type === 1
								? 'https://static-jp.theckb.com/static-asset/easy-h5/tag_used.png'
								: 'https://static-jp.theckb.com/static-asset/easy-h5/tag_invalid.png'
						}
						alt=""
						className="coupon-list-item-tag"
					/>
				)}

				{type === 0 && renderTimeTag(item)}
			</div>
		);
	};
	const renderList = (type) => {
		return (
			<div className="coupon-list-box">
				{couponLoading ? (
					<AutoCenter>
						<SpinLoading style={{ marginTop: '20vh' }} />
					</AutoCenter>
				) : (
					<div>
						{couponList.map((item, index) => (
							<div className="coupon-list-item" key={index}>
								{renderCoupon(item, index, type)}
							</div>
						))}
					</div>
				)}

				{/* <InfiniteScroll loadMore={loadMore} hasMore={hasMore} /> */}
			</div>
		);
	};
	return (
		<ScrollPage
			className="usage-guide-page"
			title={
				<CustomNavbar
					title={window._$m.t('我的优惠券')}
					fixed={false}
				/>
			}
		>
			{/* {loading && <SpinLoading className="classify-loading" />} */}
			<div
				className="layout-content"
				style={{
					marginTop: '0.69rem',
					height: 'calc(100vh - 0.69rem)'
				}}
			>
				<Tabs
					style={{
						'--active-line-color': '#FF5010',
						'--fixed-active-line-width': '0.14rem',
						'--title-font-size': '0.16rem',
						'--active-title-color': '#1C2026',
						'--active-line-height': '0.03rem',
						fontWeight: '600',
						color: '#8C8C8C',
						overflowX: 'hidden'
					}}
					activeLineMode="fixed"
					onChange={(key) => {
						setCouponType(key);
					}}
				>
					<Tabs.Tab title={window._$m.t('未使用')} key="0">
						{renderList(0)}
					</Tabs.Tab>
					<Tabs.Tab title={window._$m.t('已使用')} key="1">
						{renderList(1)}
					</Tabs.Tab>
					<Tabs.Tab title={window._$m.t('已失效')} key="2">
						{renderList(2)}
					</Tabs.Tab>
				</Tabs>
			</div>
		</ScrollPage>
	);
};
export default CouponList;
