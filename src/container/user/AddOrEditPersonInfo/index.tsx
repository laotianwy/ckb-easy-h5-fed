/* eslint-disable max-lines */
/*
 * @Author: yusha
 * @Date: 2023-10-22 14:35:17
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-12 15:29:14
 * @Description: 新增/编辑地址
 */
import { memo, useEffect, useState } from 'react';
import qs from 'query-string';
import { useLocation, useNavigate } from 'react-router-dom';
import { Button, Form, Switch, Toast } from 'antd-mobile';
import { useAsyncEffect } from 'ahooks';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import CustomInput from '@/component/CustomInput';
import PickProvince from '@/component/PickProvince';
import { request } from '@/config/request';
import { IntAreaDTO } from '@/service/customer';
import CustomTextArea from '@/component/CustomTextArea';
import CustomSelect from '@/component/CustomSelect';
import { EnumClearanceType } from '@/container/order/ConfirmOrder/config';
import './index.scss';
import ScrollPage from '@/common/ScrollPage';
const AddOrEditPersonInfo = () => {
	const [form] = Form.useForm();
	const navigate = useNavigate();
	const { personId = '', fromByCreateOrder } = qs.parse(useLocation().search);
	const [japanAddress, setJapanAddress] = useState<IntAreaDTO[] | undefined>(
		[]
	);
	/** 监听都道府县 */
	const _province = Form.useWatch('province', form);
	const _city = Form.useWatch('city', form);
	/** 监听通关类型 */
	const _clearanceType = Form.useWatch('clearanceType', form);
	/** 请求日本地址 */
	const getAreaById = async () => {
		const resData = await request.customer.getAreaById.getAreaById({
			intAreaId: 2279
		});
		const data = resData.data?.children;
		setJapanAddress(data);
		// 如果是从创建订单来的，代表没有地址，新增时将默认设值为true
		if (fromByCreateOrder) {
			form.setFieldsValue({
				defaultFlag: true
			});
		}
		return data;
	};
	/** 通过id获取下单人信息 */
	const getInfoByPersonId = async (_japanAddress) => {
		const _id = personId as string;
		const resData = await request.easyOrder.userPlaceOrderAddress.findById({
			id: Number(_id)
		});
		const { data = {} } = resData;
		const province = _japanAddress?.filter(
			(item) => item.intAreaId === data.provinceId
		);
		const city = [
			{
				intAreaId: data.cityId,
				nameJa: data.cityName,
				nameEn: data.cityNameEn
			}
		];

		// const province = [
		// 	{
		// 		intAreaId: data.provinceId,
		// 		nameJa: data.provinceName,
		// 		nameEn: data.provinceNameEn
		// 	}
		// ];
		const formValue = {
			...data,
			city,
			defaultFlag: Boolean(data.defaultFlag),
			province
		};
		form.setFieldsValue(formValue);
	};
	useAsyncEffect(async () => {
		const data = await getAreaById();
		if (personId) {
			await getInfoByPersonId(data);
		}
	}, []);
	/** 获取级联的地址数据 */
	const getAddressData = () => {
		const _data =
			_province?.length > 0
				? _province?.find?.(
						(item) => item.intAreaId === _province?.[0]?.intAreaId
					)?.children
				: [];
		return _data;
	};
	/** 获取入参 */
	const getParams = (data) => {
		const { city, defaultFlag, province } = data;
		const cityId = city?.length > 0 ? city?.[0]?.intAreaId : undefined;
		const cityName = city?.length > 0 ? city?.[0]?.nameJa : undefined;
		const provinceId =
			province?.length > 0 ? province?.[0]?.intAreaId : undefined;
		const provinceName =
			province?.length > 0 ? province?.[0]?.nameJa : undefined;
		// boolean需要转为1或者0
		const _defaultFlag = defaultFlag ? 1 : 0;
		const _id = personId ? Number(personId) : undefined;
		const params = {
			...data,
			// 先写死日本
			countryName: window._$m.t('日本'),
			countryCode: 'JP',
			countryId: '2279',
			cityId,
			cityName,
			provinceId,
			provinceName,
			defaultFlag: _defaultFlag,
			id: _id
		};
		delete params?.city;
		delete params?.province;
		delete params?.name;
		delete params?.country;
		return params;
	};
	/** 提交地址信息表单 */
	const submitPersonInfo = async () => {
		const value = await form.validateFields();
		const params = getParams(value);
		const resData =
			await request.easyOrder.userPlaceOrderAddress.save(params);
		if (!resData.success && resData.msg) {
			Toast.show(resData.msg);
			return;
		}
		if (!personId) {
			window.sessionStorage.setItem('addAddresOrPerson', 'personInfo');
		}
		navigate(-1);
	};
	useEffect(() => {
		if (_province && _province?.length > 0) {
			form.setFieldsValue({
				provinceNameEn: _province?.[0]?.nameEn
			});
			return;
		}
		form.setFieldsValue({
			provinceNameEn: ''
		});
	}, [_province, form]);
	useEffect(() => {
		if (_city && _city?.length > 0) {
			form.setFieldsValue({
				cityNameEn: _city?.[0]?.nameEn
			});
			return;
		}
		form.setFieldsValue({
			cityNameEn: ''
		});
	}, [_city, form]);
	return (
		<ScrollPage
			title={<CustomNavbar title={window._$m.t('下单人信息')} />}
			className="add-person-page"
		>
			<div className="layout-content add-person">
				<div
					className="person-page-content"
					onFocus={(e) => {
						if (
							['input', 'textarea'].includes(
								e.target.tagName.toLowerCase()
							)
						) {
							// 在获取焦点时滚动输入框到可视区域内
							e.target.scrollIntoView({
								behavior: 'smooth',
								block: 'center'
							});
						}
					}}
				>
					<div className="form-title">
						<div>{window._$m.t('下单人信息')}</div>
						<div className="form-title-tips special-style-warning">
							{window._$m.t('*以下信息用作清关使用，请谨慎填写')}
						</div>
					</div>
					<Form
						form={form}
						className="person-form-style"
						layout="horizontal"
						footer={null}
						initialValues={{
							clearanceType: EnumClearanceType.Person
						}}
					>
						<Form.Item
							name="country"
							label={window._$m.t('国/地区')}
							rules={[
								{
									required: true,
									message: window._$m.t('国/地区不能为空')
								}
							]}
							initialValue={window._$m.t('Japan 日本')}
						>
							<CustomInput
								disabled={true}
								placeholder={window._$m.t('请输入地区')}
							/>
						</Form.Item>
						<Form.Item
							name="clearanceType"
							label={window._$m.t('通关名义')}
							rules={[
								{
									required: true,
									message: window._$m.t('通关名义不能为空')
								}
							]}
						>
							<CustomSelect
								onChange={() => {
									form.setFieldsValue({
										placeOrderName: '',
										placeOrderNameEn: '',
										placeOrderCompanyName: '',
										placeOrderCompanyNameEn: '',
										companyNo: '',
										tel: '',
										email: '',
										province: [],
										city: [],
										address: '',
										addressEn: '',
										postalCode: ''
									});
								}}
								options={[
									{
										label: window._$m.t('企业'),
										value: EnumClearanceType.Company
									},
									{
										label: window._$m.t('个人'),
										value: EnumClearanceType.Person
									}
								]}
							/>
						</Form.Item>
						<Form.Item
							label={window._$m.t('姓名')}
							required
							name="name"
							className="special-items-style"
						>
							<Form.Item
								name="placeOrderName"
								rules={[
									{
										required: true,
										message: window._$m.t('日文名不能为空')
									}
								]}
							>
								<CustomInput
									maxLength={50}
									placeholder={window._$m.t('日文名')}
								/>
							</Form.Item>
							<Form.Item
								name="placeOrderNameEn"
								rules={[
									{
										required: true,
										message: window._$m.t('英文名不能为空')
									}
								]}
							>
								<CustomInput
									maxLength={50}
									placeholder={window._$m.t('英文名')}
								/>
							</Form.Item>
						</Form.Item>
						{_clearanceType === EnumClearanceType.Company && (
							<>
								<Form.Item
									label={window._$m.t('公司名')}
									required
									name="name"
									className="special-items-style"
								>
									<Form.Item
										name="placeOrderCompanyName"
										rules={[
											{
												required: true,
												message:
													window._$m.t(
														'日文名不能为空'
													)
											}
										]}
									>
										<CustomInput
											maxLength={128}
											placeholder={window._$m.t('日文名')}
										/>
									</Form.Item>
									<Form.Item
										name="placeOrderCompanyNameEn"
										rules={[
											{
												required: true,
												message:
													window._$m.t(
														'英文名不能为空'
													)
											}
										]}
									>
										<CustomInput
											maxLength={128}
											placeholder={window._$m.t('英文名')}
										/>
									</Form.Item>
								</Form.Item>
								<Form.Item
									name="companyNo"
									label={window._$m.t('会社编号')}
									rules={[
										{
											required: true,
											message:
												window._$m.t(
													'请输入有效的会社编号'
												)
										}
									]}
								>
									<CustomInput
										placeholder=""
										maxLength={128}
									/>
								</Form.Item>
							</>
						)}

						<Form.Item
							name="tel"
							label={window._$m.t('联系电话')}
							rules={[
								{
									required: true,
									message: window._$m.t('联系电话不能为空')
								},
								{
									pattern: /^[0-9]{6,15}$/,
									message:
										window._$m.t('请输入有效的电话号码')
								}
							]}
						>
							<CustomInput
								prefix={
									<div className="pre-text-style">+81</div>
								}
								pattern="[0-9]*"
								placeholder={window._$m.t('请输入手机号')}
								maxLength={15}
								type="number"
							/>
						</Form.Item>
						<Form.Item
							name="email"
							label={window._$m.t('邮箱')}
							className="specical-form-item-email"
							rules={[
								{
									required: true,
									message: window._$m.t('邮箱不能为空')
								},
								{
									pattern: /[\d\w-]+@[-a-zA-ZA-z0-9]+[.a-z]+/,
									message: window._$m.t('请输入有效的邮箱')
								}
							]}
						>
							<CustomInput placeholder="" maxLength={128} />
						</Form.Item>
						<Form.Item
							name="province"
							label={window._$m.t('都道府县')}
							className="special-items-style"
							required
						>
							<Form.Item
								name="province"
								rules={[
									{
										required: true,
										message:
											window._$m.t('都道府县不能为空')
									}
								]}
							>
								<PickProvince
									placeholder={window._$m.t('都道府县日文')}
									data={japanAddress}
									onChange={() => {
										form.setFieldsValue({
											city: null,
											cityNameEn: ''
										});
									}}
								/>
							</Form.Item>
							<Form.Item name="provinceNameEn" disabled>
								<CustomInput
									placeholder={window._$m.t('都道府县英文')}
								/>
							</Form.Item>
						</Form.Item>
						<Form.Item
							name="city"
							label={window._$m.t('市区町村')}
							className="special-items-style"
						>
							<Form.Item name="city">
								<PickProvince
									emptyTextTip={window._$m.t(
										'请先选择都道府县'
									)}
									data={getAddressData()}
									placeholder={window._$m.t('市区町村日文')}
								/>
							</Form.Item>
							<Form.Item name="cityNameEn" disabled>
								<CustomInput
									placeholder={window._$m.t('市区町村英文')}
								/>
							</Form.Item>
						</Form.Item>
						<Form.Item
							name="address"
							label={window._$m.t('地址')}
							required
							className="special-items-style"
						>
							<Form.Item
								name="address"
								rules={[
									{
										required: true,
										message:
											window._$m.t('详细地址不能为空')
									}
								]}
							>
								<CustomTextArea
									maxLength={256}
									placeholder={window._$m.t('请输入日文地址')}
									rows={3}
								/>
							</Form.Item>
							<Form.Item
								name="addressEn"
								rules={[
									{
										required: true,
										message:
											window._$m.t('详细地址不能为空')
									}
								]}
							>
								<CustomTextArea
									maxLength={256}
									placeholder={window._$m.t('请输入英文地址')}
									rows={3}
								/>
							</Form.Item>
						</Form.Item>
						<Form.Item
							name="postalCode"
							label={window._$m.t('邮编')}
							rules={[
								{
									required: true,
									message: window._$m.t('邮编不能为空')
								},
								{
									pattern: /(^\d{7}$)|(^\d{3}-\d{4}$)/,
									message: window._$m.t('请输入有效的邮编')
								}
							]}
						>
							<CustomInput
								placeholder={window._$m.t('请输入邮编')}
								maxLength={8}
							/>
						</Form.Item>
						<Form.Item
							name="defaultFlag"
							label={window._$m.t('设为默认')}
							childElementPosition="right"
							className="special-item-default"
							valuePropName="checked"
						>
							<Switch
								checked={form.getFieldValue('defaultFlag')}
							/>
						</Form.Item>
					</Form>
				</div>
			</div>
			<div className="person-page-footer">
				<Button shape="rounded" onClick={submitPersonInfo}>
					{window._$m.t('保存')}
				</Button>
			</div>
		</ScrollPage>
	);
};
export default memo(AddOrEditPersonInfo);
