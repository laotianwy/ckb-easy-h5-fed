/*
 * @Author: yusha
 * @Date: 2024-03-26 10:40:50
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-04-02 13:24:51
 * @Description: 地址列表
 */

import { memo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Grid, Toast } from 'antd-mobile';
import { useMount, useRequest } from 'ahooks';
import BianjiEdit from '@/common/icons/BianjiEdit';
import { EnumDefaultFlag } from '@/container/order/ConfirmOrder/config';
import { UserReceivingAddressDTO } from '@/service/easyOrder';
import Shanchutrash from '@/common/icons/Shanchutrash';
import CheckedDefault from '@/common/icons/CheckedDefault';
import CheckCheck from '@/common/icons/CheckCheck';
import { request } from '@/config/request';
import CustomModal from '@/component/CustomModal';
import ScrollPage from '@/common/ScrollPage';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { CustomRMImg } from '@/component/CustomRMImg';
import './index.scss';

/** 拼接收件人姓名 */
function formatFullName(name: string) {
	if (name.includes('+')) {
		return name.split('+').join(' ');
	}
	return name;
}

const AddressList = () => {
	const [addressList, setAddressList] = useState<UserReceivingAddressDTO[]>(
		[]
	);
	const navigate = useNavigate();
	/** 是否为管理状态 */
	const [isManageStatus, setIsManageStatus] = useState(false);
	/** 默认地址的id */
	const [defaultAddressId, setDefaultAddressId] = useState<
		number | undefined
	>();
	const { runAsync: setDefault } = useRequest(
		request.easyOrder.userReceivingAddress.setDefault,
		{
			manual: true
		}
	);
	/** 删除地址 */
	const deleteAddress = async (item: UserReceivingAddressDTO) => {
		CustomModal.confirm({
			content: window._$m.t('确定从收货地址中删除吗？'),
			confirmText: window._$m.t('确定删除'),
			cancelText: window._$m.t('取消删除'),
			onConfirm: async () => {
				const resData =
					await request.easyOrder.userReceivingAddress.delete(item);
				if (resData.success) {
					// if (item.id === selectedAddress?.id) {
					//     window.sessionStorage.removeItem('selectedAddressId');
					// }
					getAddressList();
					Toast.show({
						icon: 'success',
						content: window._$m.t('删除成功')
					});
				}
			}
		});
	};
	/** 获取地址列表 */
	const getAddressList = async () => {
		const resData = await request.easyOrder.userReceivingAddress.list();
		const newData = resData.data ?? [];
		setAddressList(newData);
		const defaultAddress =
			newData.find((item) => item.defaultFlag === EnumDefaultFlag.Yes) ||
			{};
		setDefaultAddressId(defaultAddress.id ?? undefined);
	};
	useMount(() => {
		getAddressList();
	});
	/** 设置默认地址 */
	const setAddressDefault = (item: UserReceivingAddressDTO) => {
		setDefaultAddressId(item.id);
		setDefault({ ...item, defaultFlag: EnumDefaultFlag.Yes });
	};
	return (
		<ScrollPage
			title={<CustomNavbar title={window._$m.t('我的收货地址')} />}
			className="page-address-list"
		>
			{!addressList.length && (
				<div className="address-list-empty">
					<CustomRMImg
						className="empty-img"
						alt={window._$m.t('占位图')}
						src="https://static-jp.theckb.com/static-asset/easy-h5/empty_content.svg"
					/>

					<div className="empty-desc">
						{window._$m.t('收货地址没有登记')}
						<br />
						{window._$m.t('请添加收货地址')}
					</div>
				</div>
			)}

			{!!addressList.length && (
				<div className="address-list">
					{addressList.map((item, index) => {
						return (
							<div key={index} className="sigle-address-contain">
								<div className="sigle-address">
									<div className="address-content">
										{defaultAddressId === item.id && (
											<div className="default-tag-style">
												{window._$m.t('默认')}
											</div>
										)}
										<div className="content-title">
											<div className="content-name">
												<span className="receive-name-omit default-min-omit">
													{formatFullName(
														item.receiveName || ''
													)}
												</span>
												&nbsp;&nbsp;&nbsp;
												<span
													style={{ maxWidth: '2rem' }}
												>
													+81 {item.receiveTel}{' '}
												</span>
											</div>
											<div
												className="flex-align-center edit"
												onClick={() => {
													navigate(
														`/user/address/edit?addressId=${item.id}`
													);
												}}
											>
												<BianjiEdit
													fill="#1C2026"
													className="edit-icon"
												/>

												<span
													style={{
														marginLeft: '0.04rem',
														fontSize: '0.14rem',
														whiteSpace: 'nowrap'
													}}
												>
													{window._$m.t('编辑')}
												</span>
											</div>
										</div>
										<div className="content-detail">
											<div>
												{item.countryName}{' '}
												{item.provinceName}{' '}
												{item.cityName} {item.address}{' '}
												{item.postalCode}
											</div>
										</div>
									</div>
								</div>
								{isManageStatus && (
									<div className="actions-btn">
										<div
											className="flex-align-center"
											onClick={() =>
												setAddressDefault(item)
											}
										>
											{defaultAddressId === item.id &&
											Boolean(defaultAddressId) ? (
												<CheckCheck
													color="#000"
													className="select-icon"
												/>
											) : (
												<CheckedDefault className="select-icon select-default" />
											)}

											<span>
												{window._$m.t('设为默认')}
											</span>
										</div>
										<div
											className="flex-align-center"
											onClick={() => deleteAddress(item)}
										>
											<Shanchutrash
												className="delete-icon"
												fill="#1C2026"
											/>

											<span>{window._$m.t('删除')}</span>
										</div>
									</div>
								)}
							</div>
						);
					})}
				</div>
			)}

			<div className="address-actions">
				<Grid columns={addressList.length === 0 ? 1 : 2} gap={8}>
					{addressList.length > 0 ? (
						<Grid.Item>
							<Button
								color="default"
								shape="rounded"
								className="action-btn"
								onClick={() => {
									setIsManageStatus(!isManageStatus);
									// 当点击退出管理需要刷新数据
									isManageStatus && getAddressList();
								}}
							>
								{isManageStatus
									? window._$m.t('退出管理')
									: window._$m.t('管理')}
							</Button>
						</Grid.Item>
					) : null}
					<Grid.Item>
						<Button
							color="primary"
							shape="rounded"
							onClick={() => {
								// 若是没有地址去创建地址，需要带个标识去
								const url = addressList.length
									? '/user/address/edit'
									: `/user/address/edit?isNoAddress=${true}`;
								navigate(url);
							}}
							className="action-btn"
						>
							{window._$m.t('添加收货地址')}
						</Button>
					</Grid.Item>
				</Grid>
			</div>
		</ScrollPage>
	);
};
export default memo(AddressList);
