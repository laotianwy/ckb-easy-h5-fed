/*
 * @Author: yusha
 * @Date: 2023-10-22 14:35:17
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-04-02 10:14:48
 * @Description: 新增/编辑地址
 */
import { memo, useState } from 'react';
import qs from 'query-string';
import { useLocation, useNavigate } from 'react-router-dom';
import { Button, Form, Switch, Toast } from 'antd-mobile';
import { useAsyncEffect } from 'ahooks';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import CustomInput from '@/component/CustomInput';
import PickProvince from '@/component/PickProvince';
import { request } from '@/config/request';
import { IntAreaDTO } from '@/service/customer';
import CustomTextArea from '@/component/CustomTextArea';
import './index.scss';
import ScrollPage from '@/common/ScrollPage';
import Shanchutrash from '@/common/icons/Shanchutrash';
import CustomModal from '@/component/CustomModal';
const AddOrEditAddress = () => {
	const [form] = Form.useForm();
	const navigate = useNavigate();
	const {
		addressId = '',
		fromByCreateOrder,
		isNoAddress
	} = qs.parse(useLocation().search);
	const [japanAddress, setJapanAddress] = useState<IntAreaDTO[] | undefined>(
		[]
	);
	/** 监听都道府县 */
	const province = Form.useWatch('province', form);
	/** 请求日本地址 */
	const getAreaById = async () => {
		const resData = await request.customer.getAreaById.getAreaById({
			intAreaId: 2279
		});
		const data = resData.data?.children;
		setJapanAddress(data);

		// 如果是从创建订单来的，代表没有地址，新增时将默认设值为true
		// 如果是从收货地址列表来，判断isNoAddress是否存在，存在代表没有地址来添加地址
		if (fromByCreateOrder || isNoAddress) {
			form.setFieldsValue({
				defaultFlag: true
			});
		}
		return data;
	};
	/** 通过id获取地址信息 */
	const getInfoByAddressId = async (_japanAddress) => {
		const _id = addressId as string;
		const resData = await request.easyOrder.userReceivingAddress.findById({
			id: Number(_id)
		});
		const { data = {} } = resData;
		const province = _japanAddress?.filter(
			(item) => item.intAreaId === data.provinceId
		);
		const city = [
			{
				intAreaId: data.cityId,
				nameJa: data.cityName
			}
		];

		const names = data.receiveName ? data.receiveName.split('+') : [];
		const formValue = {
			address: data.address,
			city,
			defaultFlag: Boolean(data.defaultFlag),
			postalCode: data.postalCode,
			province,
			receiveName: names.length >= 2 ? names[0] : '',
			name: names.length >= 2 ? names[1] : names[0] || '',
			receiveTel: data.receiveTel
		};
		form.setFieldsValue(formValue);
		// form.setFieldsValue({ defaultFlag: true });
	};
	useAsyncEffect(async () => {
		const data = await getAreaById();
		if (addressId) {
			await getInfoByAddressId(data);
		}
	}, []);

	/** 获取级联的地址数据 */
	const getAddressData = () => {
		const _data =
			province?.length > 0
				? province?.find?.(
						(item) => item.intAreaId === province?.[0]?.intAreaId
					)?.children
				: [];
		return _data;
	};
	/** 获取入参 */
	const getParams = (data) => {
		const {
			address,
			city,
			defaultFlag,
			postalCode,
			province,
			receiveName,
			name,
			receiveTel
		} = data;
		const cityId = city?.length > 0 ? city?.[0]?.intAreaId : undefined;
		const cityName = city?.length > 0 ? city?.[0]?.nameJa : undefined;
		const provinceId =
			province?.length > 0 ? province?.[0]?.intAreaId : undefined;
		const provinceName =
			province?.length > 0 ? province?.[0]?.nameJa : undefined;
		// boolean需要转为1或者0
		const _defaultFlag = defaultFlag ? 1 : 0;
		const _id = addressId ? Number(addressId) : undefined;
		const fullName = receiveName + '+' + name;
		return {
			postalCode,
			receiveName: fullName,
			receiveTel,
			// 先写死日本
			countryName: window._$m.t('日本'),
			countryCode: 'JP',
			countryId: 2279,
			cityId,
			cityName,
			provinceId,
			provinceName,
			defaultFlag: _defaultFlag,
			address,
			id: _id
		};
	};
	/** 提交地址信息表单 */
	const submitAddressInfo = async () => {
		const value = await form.validateFields();
		const params = getParams(value);
		const resData =
			await request.easyOrder.userReceivingAddress.save(params);
		if (!resData.success && resData.msg) {
			Toast.show(resData.msg);
			return;
		}
		if (!addressId) {
			window.sessionStorage.setItem('addAddresOrPerson', 'address');
		}
		navigate(-1);
	};
	/** 删除地址 */
	const deleteAddress = () => {
		const value = form.getFieldsValue();
		const params = getParams(value);
		CustomModal.confirm({
			content: window._$m.t('确定从收货地址中删除吗？'),
			confirmText: window._$m.t('确定删除'),
			cancelText: window._$m.t('取消删除'),
			onConfirm: async () => {
				const resData =
					await request.easyOrder.userReceivingAddress.delete(params);
				if (resData.success) {
					Toast.show({
						icon: 'success',
						content: window._$m.t('删除成功')
					});
					navigate(-1);
				}
			}
		});
	};
	return (
		<ScrollPage
			title={
				<CustomNavbar
					title={
						addressId
							? window._$m.t('编辑地址')
							: window._$m.t('新增地址')
					}
					navBarRight={
						!addressId ? null : (
							<div
								onClick={deleteAddress}
								style={{
									width: '0.32rem',
									height: '0.32rem',
									borderRadius: '50%',
									backgroundColor: '#fff',
									display: 'flex',
									justifyContent: 'center',
									alignItems: 'center'
								}}
							>
								<Shanchutrash
									className="delete-icon"
									fill="#1C2026"
								/>
							</div>
						)
					}
				/>
			}
			className="add-address-page"
		>
			<div className="layout-content add-address">
				<div
					className="address-page-content"
					onFocus={(e) => {
						if (
							['input', 'textarea'].includes(
								e.target.tagName.toLowerCase()
							)
						) {
							// 在获取焦点时滚动输入框到可视区域内
							e.target.scrollIntoView({
								behavior: 'smooth',
								block: 'center'
							});
						}
					}}
				>
					<div className="form-title">
						{window._$m.t('收件人信息')}
					</div>
					<Form
						form={form}
						className="address-form-style"
						layout="horizontal"
						footer={null}
					>
						<Form.Item
							name="country"
							label={window._$m.t('国/地区')}
							rules={[
								{
									required: true,
									message: window._$m.t('姓名不能为空')
								}
							]}
							initialValue={window._$m.t('Japan 日本')}
						>
							<CustomInput
								disabled={true}
								placeholder={window._$m.t('请输入地区')}
							/>
						</Form.Item>
						<div
							style={{
								display: 'flex'
							}}
						>
							<Form.Item
								name="receiveName"
								label={window._$m.t('收件人')}
								rules={[
									{
										required: true,
										message: window._$m.t('姓氏不能为空')
									}
								]}
							>
								<CustomInput
									maxLength={50}
									placeholder={window._$m.t('姓')}
								/>
							</Form.Item>
							<Form.Item
								name="name"
								style={{
									padding: '0'
								}}
								rules={[
									{
										required: true,
										message: window._$m.t('名字不能为空')
									}
								]}
							>
								<CustomInput
									maxLength={50}
									placeholder={window._$m.t('名')}
								/>
							</Form.Item>
						</div>

						<Form.Item
							name="receiveTel"
							label={window._$m.t('联系电话')}
							rules={[
								{
									required: true,
									message: window._$m.t('联系电话不能为空')
								},
								{
									pattern: /^[0-9]{6,15}$/,
									message:
										window._$m.t('请输入有效的电话号码')
								}
							]}
						>
							<CustomInput
								prefix={
									<div className="pre-text-style">+81</div>
								}
								pattern="[0-9]*"
								placeholder={window._$m.t('请输入手机号')}
								maxLength={15}
								type="number"
							/>
						</Form.Item>
						<Form.Item
							name="province"
							label={window._$m.t('都道府县')}
							rules={[
								{
									required: true,
									message: window._$m.t('都道府县不能为空')
								}
							]}
						>
							<PickProvince
								data={japanAddress}
								onChange={() => {
									form.setFieldsValue({
										city: null
									});
								}}
							/>
						</Form.Item>
						<Form.Item
							name="city"
							label={window._$m.t('市区町村')}
							rules={[
								{
									required: true,
									message: window._$m.t('市区町村不能为空')
								}
							]}
						>
							<PickProvince
								emptyTextTip={window._$m.t('请先选择都道府县')}
								data={getAddressData()}
							/>
						</Form.Item>
						<Form.Item
							name="postalCode"
							label={window._$m.t('邮编')}
							rules={[
								{
									required: true,
									message: window._$m.t('邮编不能为空')
								},
								{
									pattern: /(^\d{7}$)|(^\d{3}-\d{4}$)/,
									message: window._$m.t('请输入有效的邮编')
								}
							]}
						>
							<CustomInput
								placeholder={window._$m.t('请输入邮编')}
								maxLength={8}
							/>
						</Form.Item>
						<Form.Item
							name="address"
							label={window._$m.t('详细地址')}
							rules={[
								{
									required: true,
									message: window._$m.t('详细地址不能为空')
								}
							]}
						>
							<CustomTextArea
								maxLength={256}
								placeholder={window._$m.t('请输入详细住址')}
								rows={3}
							/>
						</Form.Item>
						<Form.Item
							name="defaultFlag"
							label={window._$m.t('设为默认')}
							childElementPosition="right"
							className="special-item-default"
							valuePropName="checked"
							// noStyle
						>
							<Switch
								checked={form.getFieldValue('defaultFlag')}
							/>
						</Form.Item>
					</Form>
				</div>

				{/* <div
					style={{
						width: '3.75rem',
						height: '9rem',
						backgroundColor: 'red'
					}}
				/> */}
			</div>
			<div className="address-page-footer">
				<Button shape="rounded" onClick={submitAddressInfo}>
					{window._$m.t('保存')}
				</Button>
			</div>
		</ScrollPage>
	);
};
export default memo(AddOrEditAddress);
