/*
 * @Author: yusha
 * @Date: 2023-11-22 16:17:20
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-03 13:43:40
 * @Description: 消息列表为空的页面
 */

import { memo } from 'react';
import classNames from 'classnames';
import styles from './index.module.scss';
const MessageEmptyPage = () => {
	return (
		<div className="layout-style">
			<div
				className={classNames(
					'layout-content',
					styles['message-empty-page']
				)}
			>
				<div className={styles['shop-cart-empty']}>
					<img
						className={styles['empty-img']}
						alt="empty"
						src="https://static-jp.theckb.com/static-asset/easy-h5/empty_message.svg"
					/>

					<div className={styles['empty-desc']}>
						{window._$m.t('暂无消息')}
					</div>
				</div>
			</div>
		</div>
	);
};
export default memo(MessageEmptyPage);
