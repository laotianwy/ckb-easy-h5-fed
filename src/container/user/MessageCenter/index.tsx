/*
 * @Author: yusha
 * @Date: 2023-11-22 15:39:18
 * @LastEditors: huajian
 * @LastEditTime: 2024-02-28 18:45:54
 * @Description: 消息中心
 */

import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { CSSProperties, memo, useRef, useState } from 'react';
import { InfiniteScroll } from 'antd-mobile';
import Badge from '@nutui/nutui-react-native/lib/module/badge';
import { useAtomValue } from 'jotai';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { request } from '@/config/request';
import { NotifyPrivateVO } from '@/service/customer';
import { User } from '@/Atoms';
import Loading from '@/component/Loading';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import ScrollPage from '@/common/ScrollPage';
import { formatDateCN2JP } from '@/utils';
import { ENUM_HYPER_LINK_KEY } from '../MessageDetail/config';
import MessageEmptyPage from './modules/MessageEmptyPage';
import './index.scss';
// eslint-disable-next-line import/order
import * as SDK from '@snifftest/sdk/lib/rn';
const getTime = (timeString) => {
	return timeString.substring(11, 16);
};
const extractContent = (text) => {
	const index = text.indexOf('\n');
	return text.substring(0, index);
};
const MessageCenter = () => {
	const navigate = useNavigate();
	/** 用户信息 */
	const userInfo = useAtomValue(User.userDetail);
	const scrollRef = useRef<HTMLDivElement>(null);
	const [rootFontSize, setRootFontSize] = useState<number>(0);
	const httpGetData = useRef(false);
	const [hasMore, setHasMore] = useState(false);
	const [pageNum, setPageNum] = useState(1);
	/** 是否需要刷新 */
	const [isNeedRefresh, setIsNeedRefresh] = useState(false);
	/** 消息列表 */
	const [newsList, setNewsList] = useState<NotifyPrivateVO[]>([]);
	const [total, setTotal] = useState(0);
	/** 消息列表 */
	const { runAsync: apiPrivateQuery, loading: privateQueryLoading } =
		useRequest((params) => request.customer.notify.privateQuery(params), {
			manual: true
		});
	/** 消息全部已读 */
	const { runAsync: privateReadAll, loading: privateReadAllLoading } =
		useRequest(request.customer.notify.privateReadAll, {
			manual: true
		});
	/** 消息单个已读 */
	const { runAsync: privateRead, loading: privateReadLoading } = useRequest(
		request.customer.notify.privateRead,
		{
			manual: true
		}
	);
	const getRootFontSize = () => {
		const rootPX = Number(
			document.documentElement.style.fontSize.split('px')[0]
		);
		setRootFontSize(rootPX);
	};
	const getData = async (_pageNum) => {
		const targetCustomerShopId =
			userInfo.customerShopList?.[0]?.customerId ?? undefined;
		// 获取消息列表
		const res = await apiPrivateQuery({
			pageNum,
			pageSize: 20,
			targetCustomerShopId
		});
		setTotal(res.data?.total ?? 0);
		httpGetData.current = false;
		const _data = res?.data?.records ?? [];
		// // 是否刷新页面
		// if (isNeedRefresh) {
		// 	setNewsList(_data);
		// 	setIsNeedRefresh(false);
		// 	return;
		// }
		if (_data.length >= 0) {
			setNewsList((list) => [...list, ..._data]);
		}
		setHasMore(res.data?.total !== newsList.length);
	};
	useMount(() => {
		getRootFontSize();
	});
	useAsyncEffect(async () => {
		if (httpGetData.current) return;
		httpGetData.current = true;
		getData(pageNum);
	}, [pageNum]);
	const loadMoreItems = async () => {
		if (httpGetData.current) return;
		if (newsList.length !== total) {
			setPageNum((page) => page + 1);
		}
	};

	/** 跳转消息详情 */
	const goToDetail = async (item) => {
		const params = {
			notifyTemplatePrivateId: item.notifyTemplatePrivateId,
			operateType: item.operateType
		};
		await privateRead(params);
		navigate({
			pathname: '/user/message/detail',
			search: `?${createSearchParams({
				id: item.notifyTemplatePrivateId
			})}`
		});
	};
	/** 消息跳转页面过滤 */
	const hyperLinkArray = Object.values(ENUM_HYPER_LINK_KEY);
	const rowRenderer = (item, index) => {
		return (
			<div
				key={index}
				// style={style}
				className="message-item"
				onClick={() => goToDetail(item)}
			>
				<Badge
					dot={!item.targetMainReadStatus}
					right={SDK.fitSize(1)}
					top={SDK.fitSize(1)}
					style={{
						marginRight: 0,
						width: 'auto',
						height: 'auto'
					}}
				>
					<div className="item-detail">
						<div className="item-title">
							<span>{item.title}</span>
							<span className="time">
								{getTime(formatDateCN2JP(item.createTime))}
							</span>
						</div>
						<div className="item-content">
							<span>{extractContent(item.content)}</span>
						</div>
					</div>
				</Badge>
			</div>
		);
	};
	/** 全部已读 */
	const changeAllRead = async () => {
		await privateReadAll();
		setNewsList([]);
		if (pageNum === 1) {
			getData(1);
		} else {
			setPageNum(1);
		}
	};
	return (
		<ScrollPage
			className="message-center-page"
			title={
				<CustomNavbar
					title={window._$m.t('消息通知')}
					fixed={false}
					navBarRight={
						<div
							className="all-read-right-navbar"
							onClick={changeAllRead}
						>
							{window._$m.t('全部已读')}
						</div>
					}
				/>
			}
		>
			{(privateQueryLoading || privateReadAllLoading) && <Loading />}
			<div
				className="layout-content"
				style={{
					paddingTop: '0.57rem',
					height: 'calc(100vh - 0.57rem)'
				}}
			>
				{newsList.length ? (
					<div
						ref={scrollRef}
						className="content-class"
						style={
							{
								'--adm-color-background': 'transparent',
								paddingTop: '0.1rem'
							} as any
						}
					>
						{newsList.map((i, index) => {
							return rowRenderer(i, index);
						})}
						<InfiniteScroll
							loadMore={loadMoreItems}
							hasMore={hasMore}
						>
							{() => {
								if (!newsList?.length) {
									return null;
								}
								if (total === newsList.length) {
									return '';
								}
								return (
									<div>
										{window._$m.t(
											'~没有啦，我也是有底线的~'
										)}
									</div>
								);
							}}
						</InfiniteScroll>
					</div>
				) : (
					<MessageEmptyPage />
				)}
			</div>
		</ScrollPage>
	);
};
export default memo(MessageCenter);
