/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-30 18:05:03
 * @Description: 我的页面
 */
import { useEffect, useState } from 'react';
import { request } from '@/config/request';
import { CustomerDetailRespDTO } from '@/service/customer';
import { OrderCountResp } from '@/service/easyOrder';
import styles from './index.module.scss';
import { MoHeader } from './MoHeader';
import { MoMyOrder } from './MoMyOrder';
import { MoActions } from './MoActions';
const Mine = () => {
	/** 用户信息 */
	const [info, setInfo] = useState<
		CustomerDetailRespDTO & {
			membership: {
				templateLevel: number;
			};
		}
	>();
	/**  订单数量 */
	const [orderCount, setOrderCount] = useState<OrderCountResp>();
	useEffect(() => {
		request.customer.getCustomerDetails.getCustomerDetails().then((res) => {
			setInfo(res.data as any);
		});
		request.easyOrder.orderSearch.queryOrderCount().then((res) => {
			setOrderCount(res.data);
		});
	}, []);
	return (
		<div className="layout-style">
			<div className={styles.container}>
				<MoHeader info={info} />
				<MoMyOrder orderCount={orderCount} />
				<MoActions />
				{/* <CustomTabbar isFixed /> */}
			</div>
		</div>
	);
};
export default Mine;
