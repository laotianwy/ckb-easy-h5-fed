/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-26 15:43:14
 * @Description: 我的页面
 */
import { useNavigate } from 'react-router-dom';
import MoreSmall from '@/common/icons/MoreSmall';
import CustomModal from '@/component/CustomModal';
import { request } from '@/config/request';
import { logout } from '@/config/request/login';
import MoreSmallsize from '@/common/icons/MoreSmallsize';
import { thinkingdata } from '@/config/buryingPoint';
import styles from './index.module.scss';
export const MoActions = () => {
	const navigate = useNavigate();
	const goCollection = () => {
		navigate('/user/collection');
	};
	const goWallet = () => {
		navigate('/user/wallet');
	};
	const goOut = () => {
		CustomModal.confirm({
			content: window._$m.t('请确认要退出登录吗？'),
			cancelText: window._$m.t('取消'),
			confirmText: window._$m.t('确定'),
			onConfirm: async () => {
				await request.customer.logout.logout();
				thinkingdata.logout();
				logout();
				navigate('/goods/home');
			}
		});
	};
	return (
		<div className={styles.userItem}>
			<div className={styles.item} onClick={goWallet}>
				<div>{window._$m.t('资金钱包')}</div>
				<MoreSmallsize
					width={'0.13rem'}
					height={'0.13rem'}
					color="#898989"
				/>
			</div>
			<div className={styles.item} onClick={goCollection}>
				<div>{window._$m.t('收藏商品')}</div>
				<MoreSmallsize
					width={'0.13rem'}
					height={'0.13rem'}
					color="#898989"
				/>
			</div>
			{/* <div className={styles.item} onClick={goCollection}>
                                                                                                      <div>{'退款记录'}</div>
                                                                                                      <MoreSmallsize
                                                                                                      	width={'0.13rem'}
                                                                                                      	height={'0.13rem'}
                                                                                                      	color="#898989"
                                                                                                      />
                                                                                                      </div> */}
			<div className={styles.item} onClick={goOut}>
				<div>{window._$m.t('退出登录')}</div>
				<MoreSmallsize
					width={'0.13rem'}
					height={'0.13rem'}
					color="#898989"
				/>
			</div>
		</div>
	);
};
