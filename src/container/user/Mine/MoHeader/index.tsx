/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-22 15:44:57
 * @Description: 我的页面
 */
import classNames from 'classnames';
import { Badge } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { CustomerDetailRespDTO } from '@/service/customer';
import CustomerService from '@/common/icons/CustomerService';
import { request } from '@/config/request';
import styles from './index.module.scss';
interface Props {
	info?: CustomerDetailRespDTO & {
		membership: {
			templateLevel: number;
		};
	};
}
export const MoHeader: React.FC<Props> = (props) => {
	const info = props.info;
	const navigate = useNavigate();
	/** 获取私有未读消息数量 */
	const { data: privateUnreadCount } = useRequest(
		request.customer.notify.getPrivateUnreadCount
	);
	const showService = () => {
		// TODO：如果改此处。客服页面的也要改
		const w: any = window;
		const ChannelIO = w.ChannelIO;
		// 根据站点判断展示哪个语言
		// {language: 'ko' | 'ja' | 'en'}
		const language = 'ja';
		// eslint-disable-next-line new-cap
		ChannelIO('updateUser', {
			language
		});
		// eslint-disable-next-line new-cap
		ChannelIO('showMessenger');
	};
	return (
		<div className={styles.userHeader}>
			<div className={styles.headerLeft}>
				<div
					className={styles.headerImage}
					style={{
						backgroundImage: `url(https://static-s.theckb.com/BusinessMarket/Easy/H5/avatar.png)`
					}}
				/>

				<div>
					<div className={styles.userName}>{info?.loginName}</div>
					{info?.membership?.templateLevel !== 0 && (
						<div
							className={classNames(
								styles.level,
								'level_' + info?.membership?.templateLevel
							)}
						>
							<img
								className={styles.levelImg}
								src={`https://static-s.theckb.com/BusinessMarket/Easy/H5/level_${info?.membership?.templateLevel}.png`}
								alt=""
							/>

							{info?.membership?.membershipTemplateName}
						</div>
					)}
				</div>
			</div>
			<div className={styles['tags']}>
				<div className={styles.customerService} onClick={showService}>
					<CustomerService size={24} />
					{window._$m.t('客服')}
				</div>
				<div
					className={styles['news']}
					onClick={() => {
						navigate('/user/message/center');
					}}
				>
					<Badge
						content={privateUnreadCount?.data ? Badge.dot : null}
					>
						<img
							style={{
								width: '0.22rem',
								height: '0.22rem'
							}}
							alt=""
							src="https://static-s.theckb.com/BusinessMarket/Easy/H5/xiaoxi@2x.png"
						/>
					</Badge>
					<span>{window._$m.t('消息')}</span>
				</div>
			</div>
		</div>
	);
};
