/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: yusha
 * @LastEditTime: 2023-11-13 19:55:07
 * @Description: 我的页面
 */
import { Badge } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import React from 'react';
import { OrderCountResp } from '@/service/easyOrder';
import MoreSmall from '@/common/icons/MoreSmall';
import ToBePaid from '@/common/icons/ToBePaid';
import Notshipped from '@/common/icons/Notshipped';
import Shipped from '@/common/icons/Shipped';
import { EnumOrderStatus } from '@/common/Enums';
import MoreSmallsize from '@/common/icons/MoreSmallsize';
import styles from './index.module.scss';
interface Props {
	orderCount?: OrderCountResp;
}
export const MoMyOrder: React.FC<Props> = (props) => {
	const orderCount = props.orderCount || {};
	const orderStatusList = [
		{
			status: EnumOrderStatus.waitPay.code,
			title: EnumOrderStatus.waitPay.cn,
			badgeName: 'waitPayCount',
			showBage: true,
			render() {
				return <ToBePaid size={23} />;
			}
		},
		{
			status: EnumOrderStatus.waitShip.code,
			title: EnumOrderStatus.waitShip.cn,
			badgeName: 'waitDeliverCount',
			showBage: false,
			render() {
				return <Notshipped size={23} />;
			}
		},
		{
			status: EnumOrderStatus.shipped.code,
			title: EnumOrderStatus.shipped.cn,
			badgeName: 'deliveredCount',
			showBage: false,
			render() {
				return <Shipped size={23} />;
			}
		}
	];

	const navigate = useNavigate();
	const goOrder = (status: number | undefined) => {
		if (status === undefined) {
			navigate(`/order/list`);
			return;
		}
		navigate(`/order/list?activeKey=${status}`);
	};
	return (
		<div className={styles.orderContainer}>
			<div className={styles.orderTitle}>
				<div className={styles.myOrder}>{window._$m.t('我的订单')}</div>
				<div
					className={styles.allOrder}
					onClick={() => goOrder(undefined)}
				>
					{window._$m.t('全部订单')}
					<MoreSmallsize color="#898989" />
				</div>
			</div>
			<div className={styles.orderList}>
				{orderStatusList.map((orderItem) => (
					<div
						key={orderItem.status}
						className={styles.orderItem}
						onClick={() => goOrder(orderItem.status)}
					>
						<Badge
							content={
								orderItem.showBage
									? orderCount[orderItem.badgeName] || ''
									: ''
							}
						>
							<orderItem.render />
						</Badge>
						<div>{orderItem.title}</div>
					</div>
				))}
			</div>
		</div>
	);
};
