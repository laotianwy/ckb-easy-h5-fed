/*
 * @Date: 2024-01-08 15:51:04
 * @LastEditors: liuliangliang liuliangliang@sniffgroup.com
 * @LastEditTime: 2024-04-11 11:59:49
 * @FilePath: /ckb-easy-h5-fed/src/container/user/UsageGuide/index.tsx
 * @Description:
 */
import { useAsyncEffect, useMount, useRequest } from 'ahooks';
import { CSSProperties, memo, useRef, useState } from 'react';
import './index.scss';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import ScrollPage from '@/common/ScrollPage';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import MoreMedium from '@/common/icons/MoreMedium';
import { ENUM_TREM_TYPE } from '@/outs/const';

const ENUM_TERM_TYPE_OPTIONS = [
	{
		label: '利用规约',
		value: ENUM_TREM_TYPE.DUTILIZE
	},
	{
		label: '特定商取引法に基づく表示',
		value: ENUM_TREM_TYPE.DSPECIFIC
	}
];
const UsageGuide = () => {
	const [html, setHtml] = useState('');
	const navigation = useNavigate();
	const { runAsync: apiGetCategory, loading } = useRequest(
		request.customer.base.termGetEffectTermByType,
		{
			manual: true
		}
	);
	useAsyncEffect(async () => {
		const res = await apiGetCategory({ type: 'dutilize' });
		if (res.data && res.data[0]?.fileUrl) {
			const text = await axios.get(
				res.data[0].fileUrl + '?t=' + Date.now()
			);
			const value = text.data.replace(/target=""/g, 'target="_blank"');
			setHtml(value);
		}
	}, []);

	const jumpToTermDetail = (type: ENUM_TREM_TYPE) => {
		const url = `/user/message/usageGuide/detail?termType=${type}`;
		navigation(url);
	};
	return (
		<ScrollPage
			className="usage-guide-page"
			title={<CustomNavbar title="ご利用ガイド" fixed={false} />}
		>
			<div
				className="layout-content"
				style={{
					marginTop: '0.69rem',
					height: 'calc(100vh - 0.69rem)'
				}}
			>
				<div
					className="guide-content"
					// dangerouslySetInnerHTML={{ __html: html }}
				>
					{/* <div className="topic">【1】3Fboxサービス保証</div>
					<div className="title">1.日本基準検品サポート</div>
					<div className="section">
						日本の大手検品会社朝日リンクと提携し、製品の品質保証を目的とし、厳格な検品基準に基づき検品作業を実施しております。アパレルや雑貨、ビューティー、ペット用品、PCなどジャンルに限らずデザイン／サイズ／カラーの相違品確認及び不良品確認し、検品を通過した商品だけをお届けいたします。
					</div>
					<div className="title">2.12時間カスタマーサービス対応</div>
					<div className="section">
						3Fboxは、朝9時から夜9時まで、12時間のカスタマーサービスをご提供しております。
					</div>
					<div className="title">3.安心安全な決済方法</div>
					<div className="section">
						<div>・銀行振込</div>
						<div>
							・クレジットカード--VISA、MasterCard、AMEX、JCB
						</div>
						<div> ・Paypal</div>
					</div> */}
					<section className="term-list">
						{ENUM_TERM_TYPE_OPTIONS.map((item) => {
							return (
								<div
									key={item.value}
									className="term-item"
									onClick={() => jumpToTermDetail(item.value)}
								>
									<label>{item.label}</label>
									<MoreMedium />
								</div>
							);
						})}
					</section>
				</div>
			</div>
		</ScrollPage>
	);
};
export default memo(UsageGuide);
