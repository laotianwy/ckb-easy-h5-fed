/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-20 21:07:08
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-24 17:16:58
 * @FilePath: /ckb-easy-h5-fed/src/container/user/GlobalReplaceView/index.tsx
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useMount } from 'ahooks';
import { jsBridge } from '@/utils/jsBridge';
import ScrollPage from '@/common/ScrollPage';

const GlobalReplaceView = () => {
	useMount(() => {
		setTimeout(async () => {
			await jsBridge.postMessage({
				type: 'DIRCT_goNativeBack',
				payload: {}
			});
		}, 0);
	});
	return (
		<ScrollPage>
			<div className={'layout-content'} />
		</ScrollPage>
	);
};

export default GlobalReplaceView;
