/*
 * @Author: huajian
 * @Date: 2023-10-30 15:47:39
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-02 17:17:54
 * @Description: 花费弹窗
 */

import { Button, Form } from 'antd-mobile';
import { useState } from 'react';
import CustomInput from '@/component/CustomInput';
import CustomPopup from '@/component/CustomPopup';
import { request } from '@/config/request';
import { EnumBizType, EnumRechargeStatus } from '@/common/Enums';
import styles from './index.module.scss';
export const downloadFile = (data: BlobPart, fileName: string) => {
	const url = window.URL.createObjectURL(new Blob([data]));
	const a = document.createElement('a');
	a.href = url;
	a.setAttribute('download', fileName);
	document.body.appendChild(a);
	a.click();
	URL.revokeObjectURL(a.href);
	document.body.removeChild(a);
};
interface Props {
	children: React.ReactNode;
	onChange: (status: string) => void;
}
export const StatusPop: React.FC<Props> = (props) => {
	const [form] = Form.useForm();
	const [visible, setVisible] = useState(false);
	const [child, setChild] = useState<string | React.ReactNode>(
		props.children
	);
	return (
		<>
			<CustomPopup visible={visible} onClose={() => setVisible(false)}>
				<div className={styles.container}>
					<div className={styles.title}>{window._$m.t('状态')}</div>
					<div>
						{Object.keys(EnumRechargeStatus).map((key) => {
							const item = EnumRechargeStatus[key];
							return (
								<div
									key={key}
									className={styles.itemWrap}
									data-selected={
										child === item.cn ? 'true' : 'fasle'
									}
									onClick={() => {
										setVisible(false);
										setChild(item.cn);
										props.onChange(item.code as any);
									}}
								>
									<div className={styles.item}>{item.cn}</div>
									{child === item.cn && (
										<img
											style={{
												width: '0.16rem',
												height: '0.16rem'
											}}
											alt=""
											src="https://static-s.theckb.com/BusinessMarket/Easy/H5/check.png"
										/>
									)}
								</div>
							);
						})}
					</div>
				</div>
			</CustomPopup>
			<span onClick={() => setVisible(true)}>{child}</span>
		</>
	);
};
