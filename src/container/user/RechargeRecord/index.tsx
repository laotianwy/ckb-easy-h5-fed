/*
 * @Author: shiguang
 * @Date: 2023-10-18 10:22:42
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2023-12-12 15:45:09
 * @Description: 充值记录
 */
import { Input } from 'antd-mobile';
import { useState } from 'react';
import CustomNavbar from '@/common/Layout/CustomNavbar';
import { request } from '@/config/request';
import { AccountRechargeQueryResp } from '@/service/pay';
import { EnumRechargeStatus } from '@/common/Enums';
import ExpandDefault from '@/common/icons/ExpandDefault';
import { formatDateCN2JP, formatMoney } from '@/utils';
import { CustomDatePicker } from '@/component/CustomDatePicker';
import { useTurnPage } from '@/utils/hooks/useTurnPage';
import ScrollPage from '@/common/ScrollPage';
import { StatusPop } from './StatusPop';
import './index.scss';
const RechargeRecord = () => {
	const [balanceRechargeCode, setBalanceRechargeCode] = useState<string>();
	const [records, RenderTurnpage, { setParams, reqParams }] = useTurnPage<
		AccountRechargeQueryResp,
		any
	>({
		pageSize: 20,
		params: {},
		request: (params: any) => {
			return request.pay.front.accountQueryRechargePage(params);
		}
	});
	return (
		<ScrollPage
			className="page-recharge-record"
			title={
				<CustomNavbar
					title={window._$m.t('充值记录')}
					style={{
						backgroundColor: '#fff'
					}}
				/>
			}
		>
			<div className="layout-content page-recharge-record-content">
				<div>
					<div className="search-header">
						<div
							style={{
								height: '.40rem',
								borderRadius: '.22rem',
								backgroundColor: '#fff',
								alignItems: 'center',
								display: 'flex',
								padding: '0 .16rem',
								flex: 1
							}}
						>
							<Input
								style={{
									backgroundColor: '#fff',
									color: '#B0B7C2'
								}}
								placeholder={window._$m.t('请输入流水号')}
								onChange={setBalanceRechargeCode}
								clearable
							/>

							<div
								style={{
									flexShrink: 0,
									color: '#232323'
								}}
							>
								<img
									style={{
										width: '0.16rem',
										height: '0.16rem'
									}}
									onClick={() => {
										setParams({
											...reqParams,
											balanceRechargeCode
										});
									}}
									alt=""
									src="https://static-s.theckb.com/BusinessMarket/Easy/H5/icon_search@2x.png"
								/>
							</div>
						</div>
					</div>
					<div className="search-header-filter-section">
						<div className="filter-section-item">
							<div
								style={{
									display: 'flex',
									alignItems: 'center',
									fontSize: '0.13rem',
									fontFamily: 'PingFang SC',
									fontWeight: '500',
									lineHeight: '0.2rem',
									color: '#1C2026'
								}}
							>
								<StatusPop
									onChange={(val) => {
										setParams({
											...reqParams,
											status: val
										});
									}}
								>
									{window._$m.t('状态')}
								</StatusPop>
								<ExpandDefault
									style={{
										marginTop: 2
									}}
								/>
							</div>
						</div>
						<div className="filter-section-item">
							<div
								style={{
									marginRight: '.04rem',
									display: 'flex',
									alignItems: 'center'
								}}
							>
								<CustomDatePicker
									onChange={(time) => {
										setParams({
											...reqParams,
											beginTime: time?.startTime,
											endTime: time?.endTime
										});
									}}
								/>
							</div>
						</div>
					</div>
					{!!records.length && (
						<div
							style={{
								margin: '0 .12rem',
								padding: '.12rem',
								background: '#fff',
								borderRadius: '.08rem'
							}}
						>
							{records.map((item, index) => {
								return (
									<div
										key={index}
										className="wallet-record-item"
									>
										<div
											className="wallet-record-item-left-box"
											style={{
												color: '#333',
												fontSize: '.12rem',
												fontWeight: 'bold',
												display: 'flex',
												justifyContent: 'space-between'
											}}
										>
											<div
												style={{
													color: '#898989',
													fontSize: '.1rem',
													fontFamily: 'PingFang SC',
													fontWeight: '400',
													lineHeight: '.18rem'
												}}
											>
												<div
													style={{
														color: '#232323',
														fontWeight: '500',
														fontSize: '.12rem',
														lineHeight: '.2rem'
													}}
												>
													{window._$m.t('银行转账')}
												</div>
												<div>
													{formatDateCN2JP(
														item.applyTime
													)}
												</div>
												<div>
													{window._$m.t('流水号：')}
													{item.balanceRechargeCode}
												</div>
												<div>
													{window._$m.t('状态：')}
													{
														EnumRechargeStatus[
															item.status!
														].cn
													}
												</div>
												<div>
													{window._$m.t('理由：')}
													{item.refuseReason}
												</div>
											</div>
										</div>
										<div
											className="wallet-record-item-right-box"
											style={{
												display: 'flex',
												flexDirection: 'column',
												alignItems: 'flex-end'
											}}
										>
											<div
												style={{
													color: '#FF5010',
													fontFamily: 'PingFang SC',
													fontSize: '.12rem',
													fontWeight: '500',
													lineHeight: '.2rem'
												}}
											>
												+
												{formatMoney(
													item.remittanceAmount
												)}
												{window._$m.t('円')}
											</div>
											<div
												style={{
													color: '#232323',
													fontFamily: 'PingFang SC',
													fontSize: '.1rem',
													fontWeight: '400',
													lineHeight: '.18rem'
												}}
											>
												{window._$m.t('充值')}
											</div>
										</div>
									</div>
								);
							})}
						</div>
					)}
				</div>
			</div>
			{RenderTurnpage}
		</ScrollPage>
	);
};
export default RechargeRecord;
