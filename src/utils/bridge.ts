/*
 * @Author: huajian
 * @Date: 2023-11-03 15:35:37
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-03 15:41:46
 * @Description:
 */

import { useState } from 'react';

interface Bridge {
	refresh: () => void;
}

export const Bridge: Bridge = {
	refresh() {}
};
