import { request } from '@/config/request';
import { RequestParams } from '@/service/customer';

interface OSSDataType {
	dir: string;
	expire: string;
	host: string;
	accessId: string;
	policy: string;
	signature: string;
	/** 上传域名 */
	customDomain: string;
}

interface Props {
	pathParams: {
		/** 服务名，例如 order, purchase */
		serviceName: 'order' | 'purchase';
		/**
		 * 业务名称，(如果没有就自己命名)
		 * tableTrade : 表格下单
		 * deliveryInvoice: 国际发货单
		 * stockInRecord：入库单
		 * checkAbnormal：质检异常
		 * OEM：oem
		 * */
		bizName:
			| 'tableTrade'
			| 'deliveryInvoice'
			| 'stockInRecord'
			| 'checkAbnormal'
			| 'OEM'
			| 'payment';
		/**
		 * 业务类型：(属于bizName 下属 如果没有就自己命名)
		 * 如果是表格下单业务，分为
		 * 导入到购物车：importShopCart
		 * 导入下单: importTrade
		 * oem 商品图片: oemGoodsImg
		 * oem log 附件: oemLogAttachment
		 * oem 式样书: styleBook
		 * oem 色卡图片: styleBook
		 */
		bizType:
			| 'oemGoodsImg'
			| 'oemSizeTemplate'
			| 'styleBook'
			| 'colorCardSelect';
		/**
		 *  如果是Client端登陆，就是customerId
		 * 如果是System端登陆 userId
		 * 如果是WMS登陆 userId
		 */
		operatorId?: 'system' | 'client' | string;
	};
}
const isProduction = (() => {
	if (window.location.port) return false;
	const pre = window.location.hostname.split('.')[0];
	return pre.startsWith('pre-') || !pre.includes('-');
})();

const bucketName = isProduction ? 'ckb-service-prod' : 'ckb-service-test';

const mockGetOSSData = async (path: string) => {
	// 这个接口有毒 需要这样传
	return (
		await request.customer.oss.signAnother({}, {
			path: `/oss/sign/another/withoutLogin?path=${path}&bucketName=${bucketName}`
			// path: `/oss/sign?path=${path}&bucketName=${bucketName}&type=0`
		} as any)
	).data;
};
const init = async (props: Props) => {
	try {
		// const { pathParams } = props;
		// const { serviceName, bizName, operatorId, bizType } = pathParams;

		// const path =
		// 	[serviceName, bizName, operatorId ?? 'system', bizType].join('/') +
		// 	'/';
		// 直营商城图搜
		const path = 'easyBuy/fileUpload/';
		const result = await mockGetOSSData(path);
		if (result) {
			return result as OSSDataType;
		}
	} catch (error: any) {
		console.log('上传失败');
	}
};

export const uploadImage = async (props: any, file: any) => {
	const suffix = file.name.slice(file.name.lastIndexOf('.'));
	const filename = Date.now() + suffix;
	const _OSSData = await init(props);
	const url = _OSSData?.dir + filename;

	const formdata = new FormData();
	formdata.append('OSSAccessKeyId', _OSSData!.accessId);
	formdata.append('policy', _OSSData!.policy);
	formdata.append('signature', _OSSData!.signature);
	formdata.append('key', url);
	formdata.append('x-oss-object-acl', 'public-read');
	formdata.append('success_action_status', '200');
	formdata.append('x-oss-forbid-overwrite', 'false');

	formdata.append('file', file);

	await fetch(_OSSData!.host, {
		method: 'post',
		mode: 'no-cors',
		body: formdata
	});
	console.log([_OSSData?.customDomain, '/', url].join(''), 'test');
	return {
		url: [_OSSData?.customDomain, '/', url].join('')
	};
};
