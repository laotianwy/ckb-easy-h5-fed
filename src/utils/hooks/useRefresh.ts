/*
 * @Author: huajian
 * @Date: 2023-11-03 16:30:27
 * @LastEditors: huajian 258220040@qq.com
 * @LastEditTime: 2023-11-13 19:04:06
 * @Description: 局部刷新
 */
import { useAtom } from 'jotai';
import { Local } from '@/Atoms';
/**
 * 使用局部刷新的方法
 */
export function useRefresh() {
	const [, setRefreshKey] = useAtom(Local.refreshKey);
	/**
	 * 刷新局部
	 */
	const refreshLocal = () => {
		setRefreshKey(new Date().getTime());
	};
	return refreshLocal;
}
