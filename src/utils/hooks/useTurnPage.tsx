/*
 * @Author: huajian
 * @Date: 2023-11-06 10:32:22
 * @LastEditors: lihwang_wf@126.com
 * @LastEditTime: 2024-01-04 11:18:05
 * @Description:
 */
import { InfiniteScroll } from 'antd-mobile';
import { useCallback, useEffect, useState } from 'react';
import { CustomEmpty } from '@/component/CustomEmpty';
interface Props<T> {
	pageSize?: number;
	request: (parmas: T) => Promise<any>;
	params: T;
	emptyTips?: string;
}
export function useTurnPage<DataType, ReqParamsType = {}>(
	props: Props<ReqParamsType>
) {
	const { pageSize = 20, emptyTips = '' } = props;
	const [pageNum, setPageNum] = useState(1);
	const [hasMore, setHasMore] = useState(true);
	const [records, setRecords] = useState<DataType[]>([]);
	const [loading, setLoading] = useState(true);
	const [reqParams, setReqParams] = useState<ReqParamsType>(props.params);
	const [total, setTotal] = useState(0);
	const loadMore = async () => {
		const res = await props.request({
			pageNum,
			pageSize,
			...reqParams
		});
		setLoading(false);
		setTotal(res.data?.total);
		const resRecords = (res.data?.records || []).map((item: any) => {
			return {
				...item,
				collected: true
			};
		});
		if (pageNum === 1) {
			setRecords(() => [...resRecords]);
		} else {
			setRecords((val) => [...val, ...resRecords]);
		}
		setHasMore(resRecords.length >= pageSize);
		setPageNum((val) => val + 1);
	};
	const setParams = useCallback((params: ReqParamsType) => {
		setHasMore(true);
		setPageNum(1);
		setRecords([]);
		setReqParams(params);
	}, []);

	const Render = (
		<>
			{!records.length && (
				<CustomEmpty emptyTips={emptyTips} loading={loading} />
			)}

			<InfiniteScroll
				loadMore={loadMore}
				hasMore={hasMore}
				// eslint-disable-next-line react/no-children-prop
				children={() => {
					if (!records?.length) {
						return null;
					}
					if (!hasMore) {
						return '';
					}
					return (
						<div>{window._$m.t('~没有啦，我也是有底线的~')}</div>
					);
				}}
			/>
		</>
	);

	return [
		records,
		Render,
		{
			setParams,
			reqParams,
			setRecords
		}
	] as const;
}
