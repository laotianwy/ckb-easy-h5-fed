/*
 * @Author: huajian
 * @Date: 2023-11-23 11:44:47
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-11 10:19:33
 * @Description: 列表翻页hook
 */

import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
	FlatList,
	FlatListProps,
	RefreshControl,
	View,
	Image
} from 'react-native';
import {
	IOFlatList,
	IOFlatListProps
} from 'react-native-intersection-observer';
interface Props extends Omit<IOFlatListProps<any>, 'data'> {
	request: any;
	pageSize?: number;
	/** 请求的参数 pageNum不用传 */
	params: any;
	useMock?: boolean;
}
export function useExposureTrunPage<T>(props: Props) {
	const pageNumRef = useRef(1);
	const scrollRef = useRef(null);
	const [records, setRecords] = useState<T[]>([]);
	const [isLoading, setIsLoading] = useState(false);
	const [params, setParams] = useState(props.params);
	const [response, setRespons] = useState(props.params);
	const [isEnd, setIsEnd] = useState(false);
	const pageSize = props.pageSize || 20;
	const propsRequest = props.request;
	const request = useCallback(async () => {
		if (!params) {
			return;
		}
		setIsLoading(true);
		const res = await propsRequest(
			{
				...params,
				pageNum: pageNumRef.current,
				pageSize
			},
			{
				useMock: props.useMock
			}
		);
		setIsLoading(false);
		if (res.code !== '0') {
			return;
		}
		setRespons(res);
		if (res.code !== '0') {
			return;
		}
		if (!res?.data?.records || res?.data?.records?.length < pageSize) {
			setIsEnd(true);
		}
		if (pageNumRef.current === 1) {
			setRecords(res.data.records || []);
		} else {
			setRecords((val) => {
				//
				return val.length >= pageSize
					? [...val, ...(res.data.records || [])]
					: [...(res.data.records || [])];
			});
		}
		pageNumRef.current++;
	}, [params, propsRequest, pageSize, props.useMock]);
	useEffect(() => {
		if (params) {
			setIsEnd(false);
		}
	}, [params]);
	const onRefresh = useCallback(() => {
		pageNumRef.current = 1;
		request();
	}, [request]);
	useEffect(onRefresh, [onRefresh]);
	const RenderTrunPage = (
		<IOFlatList
			nestedScrollEnabled={true}
			id="flatLit"
			ref={scrollRef}
			refreshControl={
				<RefreshControl
					refreshing={isLoading}
					onRefresh={() =>
						// 当不是loading状态，且当前数量大于pageSize的数量，
						!isLoading && records?.length > pageSize && onRefresh()
					}
					enabled={true}
					title={window._$m.t('加载更多商品中，请稍后')}
					tintColor="#008060"
					colors={['#60B161']}
				/>
			}
			onEndReached={() => {
				if (isLoading || isEnd) {
					return;
				}
				request();
			}}
			{...props}
			data={records}
		/>
	);

	return [
		RenderTrunPage,
		setParams,
		{
			records,
			setRecords,
			pageNum: pageNumRef.current,
			pageSize,
			onRefresh,
			isLoading,
			response,
			scrollRef
		}
	] as const;
}
