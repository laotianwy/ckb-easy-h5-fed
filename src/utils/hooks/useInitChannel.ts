/* eslint-disable prettier/prettier */
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-26 16:03:39
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-26 17:47:03
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/useInitChannel.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useCallback, useEffect } from 'react';
import { useAtom, useAtomValue } from 'jotai';
import { User } from '@/Atoms';
import { useEvent } from './useEvent';

/** 初始化客服信息数据类型 */
interface ChannelProps {
  systemName?: string;
  systemEmail?: string;
  templateName?: string;
  systemMemberId?: string;
  name?: string;
  email?: string;
}

/** 初始化客服数据 */
export const useInitChannel = () => {
  const isLogin = useAtomValue(User.isLogin);
  const userInfo = useAtomValue(User.userDetail);
  const [_, setMsgCount] = useAtom(User.serviceNum);

  /** 初始化客服 */
  const initChannel = useCallback(() => {
    // todo:用户名 登录了
    const w: any = window;
    // 默认日本的客服key 9ef7076e-b606-45f8-bb06-ec8b98b5fc7f
    const pluginKey = '6ada065d-08b5-4f42-9131-302ff4e4e772';
    const channelInfo: ChannelProps = {
      systemName: undefined,
      systemEmail: undefined,
      templateName: undefined,
      systemMemberId: undefined,
      name: undefined,
      email: undefined
    };
    if (isLogin && Object.keys(userInfo).length !== 0) {
      channelInfo.systemName = userInfo.loginName;
      channelInfo.systemEmail = userInfo.customerEmail;
      channelInfo.templateName = userInfo.membership.membershipTemplateName;
      channelInfo.systemMemberId = String(userInfo.customerId);
      channelInfo.name = userInfo.loginName;
      channelInfo.email = window.location.href;
    }
    // eslint-disable-next-line new-cap
    w.ChannelIO('shutdown');
    // eslint-disable-next-line new-cap
    w.ChannelIO('boot', {
      pluginKey,
      profile: {
        systemName: channelInfo.systemName,
        systemEmail: channelInfo.systemEmail,
        templateName: channelInfo.templateName,
        systemMemberId: channelInfo.systemMemberId,
        name: channelInfo.name,
        email: channelInfo.email
      }
    });

    // eslint-disable-next-line new-cap
    w.ChannelIO('hideChannelButton');
  }, [isLogin, userInfo]);
  useEffect(() => {
    initChannel();
  }, [initChannel]);
  useEffect(() => {
    const w: any = window;
    // eslint-disable-next-line new-cap
    w.ChannelIO('onBadgeChanged', (((num: number) => {
      setMsgCount(num);
    }) as any));
  }, [setMsgCount]);
};