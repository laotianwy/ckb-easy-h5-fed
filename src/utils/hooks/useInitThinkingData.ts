/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-03-29 14:46:07
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-17 16:03:52
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/useInitThinkingData.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
/* eslint-disable prettier/prettier */
import { useMount } from 'ahooks';
import { useAtomValue } from 'jotai';
import { useEffect } from 'react';
import queryString from 'query-string';
import { initThinkingData, taSetTouristsUser, taSetUser, thinkingdata } from '@/config/buryingPoint';
import { User } from '@/Atoms';

export const useInitThinkingData = () => {
  const userInfo = useAtomValue(User.userDetail);
  const isLogin = useAtomValue(User.isLogin);

  useMount(() => {
    initThinkingData(!!window?.ReactNativeWebView);
  });

  useEffect(() => {
    if (isLogin) {
      thinkingdata.login(userInfo.customerId);
      const searchObj = queryString.parse(window.location.search);
      const parseSessionData = JSON.parse(
        sessionStorage.getItem('h5ChannelFromOther') || '{}'
      );
      const lastParams = { ...searchObj, ...parseSessionData }
      taSetUser(userInfo, {
        channel: lastParams?.channel,
        channel_group: lastParams.channel_group
      });
      return () => {
        thinkingdata.logout();
      };
    }

    const searchObj: { channel?: string; channel_group?: string } = queryString.parse(window.location.search);
    if (searchObj?.channel_group) {
      taSetTouristsUser(searchObj.channel, searchObj.channel_group)
    }
  }, [isLogin, userInfo]);

};