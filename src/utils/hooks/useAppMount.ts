/* eslint-disable prettier/prettier */
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-26 15:06:55
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-26 15:15:21
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/useAppMount.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useAsyncEffect } from 'ahooks';
import queryString from 'query-string';
import { useState } from 'react';
import { checkLogin } from '@/config/request/login';
import { goToCheckLogin } from '../jsBridge';
export const useAppMount = () => {
  /** 是否显示页面 */
  const [showAppPage, setShowAppPage] = useState(false);
  useAsyncEffect(async () => {
    /** 获取app url传过来的参数 */
    const appToH5Payload = queryString.parse(window.location.search);
    /** 如果url上携带了token，并且有值 */
    const fromAppNoToken = !appToH5Payload.token || appToH5Payload.token === 'null' || appToH5Payload.token === 'undefined';
    // 初始化走路由传过来的token
    const fromSniffApp = (appToH5Payload.fromSniffApp as string);
    // h5内原有逻辑。不在app内。那么如果检测是否有token。如果有token，清除三方站点的domain
    if (!window?.ReactNativeWebView) {
      const token = await checkLogin();
      if (token) {
        sessionStorage.removeItem('isFromOtherDomain');
      }
      setShowAppPage(true);
      return;
    }
    // 如果在app的webview页面。在h5初始化的时候注册当前的版本号
    if (typeof appToH5Payload?.currentAppVersion === 'string' && appToH5Payload.currentAppVersion.length > 0) {
      sessionStorage.setItem('currentAppVersion', appToH5Payload.currentAppVersion);
    }
    // app商品详情跳转下单页面
    if (appToH5Payload.fromPage === 'goodsDetail') {
      const orderShopList = (appToH5Payload.orderShopList as string);
      // local_order字段，用于下单页通过该字段获取内容orderShopList
      const value = (appToH5Payload.local_order as string);
      sessionStorage.setItem(value, orderShopList);
    }
    setShowAppPage(true);
  }, []);
  return [showAppPage];
};