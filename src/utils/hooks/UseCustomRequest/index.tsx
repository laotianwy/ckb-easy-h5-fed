/*
 * @Author: huajian
 * @Date: 2023-11-02 10:27:52
 * @LastEditors: huajian
 * @LastEditTime: 2023-11-02 13:20:05
 * @Description:
 */
/* eslint-disable */
import { useRequest } from 'ahooks';
import { Options, Plugin, Service, Result } from 'ahooks/lib/useRequest/src/types';
import React, { useState } from 'react';
type Props<TData, TParams extends any[]> = {
  service: Service<TData, TParams>;
  params: TParams;
  options?: Options<TData, TParams>;
};
export function UseCustomRequest<TData, TParams extends any[]>(props: Props<TData, TParams>) {
  const res = useRequest(props.service, props.options);
  const Render: React.FC<{
    children: React.ReactNode;
  }> = (renderProps) => {
    return <div onClick={() => {
      if (res.loading) {
        return;
      }
      res.run(...props.params);
    }}>{renderProps.children}</div>;
  };
  return {
    ...res,
    Render
  };
}