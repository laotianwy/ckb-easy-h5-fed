/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-04-01 15:57:03
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-04-01 16:02:57
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/useSetChannel.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect } from 'react';
import queryString from 'query-string';

interface ChannelProps {
	channel?: string;
	channel_group?: string;
}
export const useSetChannel = () => {
	useEffect(() => {
		const urlParams: ChannelProps = queryString.parse(
			window.location.search
		);
		if (urlParams.channel_group) {
			sessionStorage.setItem(
				'h5ChannelFromOther',
				JSON.stringify({
					channel: urlParams.channel,
					channel_group: urlParams.channel_group
				})
			);
		}
	}, []);
};
