/* eslint-disable prettier/prettier */
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-26 15:56:08
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-26 16:14:14
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/JsBridge/useJsBridgeGoBack.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect } from 'react';
import queryString from 'query-string';
import { commonGoBack } from '@/common/Layout/CustomNavbar';
import { jsBridge } from '@/utils/jsBridge';

/** app向h5发送的事件。主要是Android退出事件 */
export const useJsBridgeGoBack = (curPage, navigate) => {
  useEffect(() => {
    const removeListen = jsBridge.addEventListener('DIRCT_goBack', async () => {
      // 如果是路由带过来的参数
      const searchObj = queryString.parse(window.location.search);
      const fromSniffApp = (searchObj.fromSniffApp as string);
      commonGoBack({
        navigate,
        curPage,
        fromSniffApp
      });
    });
    return removeListen;
  }, [curPage, navigate]);
};