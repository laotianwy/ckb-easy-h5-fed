/* eslint-disable prettier/prettier */
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-26 14:07:58
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-26 14:17:57
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/JsBridge/useJsBridgeLogin.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect } from 'react';
import { useSetAtom } from 'jotai';
import { goToCheckLogin, jsBridge } from '@/utils/jsBridge';
import { User } from '@/Atoms';
export const useJsBridgeLogin = () => {
  const setForceUpdateApp = useSetAtom(User.foreceUpdateApp);
  useEffect(() => {
    const removeListen = jsBridge.addEventListener('DIRCT_login', async (request) => {
      const methodData = request;
      await goToCheckLogin({
        token: methodData.payload.token,
        appMount: false
      });
      // 如果是渲染的时候触发的设置token，那么页面不重新加载
      setForceUpdateApp((pre) => pre + 1);
      window.location.reload();
    });
    return removeListen;
  }, [setForceUpdateApp]);
};