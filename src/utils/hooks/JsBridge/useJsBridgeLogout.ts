/* eslint-disable prettier/prettier */
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-26 14:19:06
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-26 15:02:06
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/JsBridge/useJsBridgeLogout.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect } from 'react';
import { useSetAtom } from 'jotai';
import { jsBridge } from '@/utils/jsBridge';
import { Order, User } from '@/Atoms';
import { logout } from '@/config/request/login';
export const useJsBridgeLogout = () => {
  const setIsLogin = useSetAtom(User.isLogin);
  const setUserinfo = useSetAtom(User.userDetail);
  const setCartOriginalData = useSetAtom(Order.atomCartOriginalData);
  const setForceUpdateApp = useSetAtom(User.foreceUpdateApp);
  useEffect(() => {
    const removeListen = jsBridge.addEventListener('DIRCT_loginout', async () => {
      logout();
      setIsLogin(false);
      setUserinfo(({} as any));
      const cartData = {
        cartNum: 0,
        cartList: []
      };
      setCartOriginalData(cartData);
      setForceUpdateApp((pre) => pre + 1);
      window.location.reload();
    });
    return removeListen;
  }, [setIsLogin, setUserinfo, setCartOriginalData, setForceUpdateApp]);
};