/* eslint-disable prettier/prettier */
/*
 * @Author: tianzhitong laotianwy@163.com
 * @Date: 2024-01-26 15:02:19
 * @LastEditors: tianzhitong laotianwy@163.com
 * @LastEditTime: 2024-01-26 15:04:45
 * @FilePath: /ckb-easy-h5-fed/src/utils/hooks/JsBridge/useJsBridgeJumpUrl.ts
 * @Description:
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { useEffect } from 'react';
import queryString from 'query-string';
import { jsBridge } from '@/utils/jsBridge';
import { router } from '@/App';
export const useJsBridgeJumpUrl = () => {
  useEffect(() => {
    const removeListen = jsBridge.addEventListener('DIRCT_JUMP_URL', (request) => {
      // 如果是app内的商详跳转到webview里的创建订单
      const methodData = request;
      if (methodData.payload.url.includes('settlement/payPalCardPay')) {
        router.navigate(methodData.payload.url, {
          replace: true,
          state: methodData?.payload?.payload ?? {}
        });
      } else {
        try {
          const _search = queryString.stringify(methodData?.payload?.payload ?? {});
          if (_search.length > 0) {
            // 如果是创建订单页面。那么设置session属性
            if (methodData.payload.url.includes('/order/create')) {
              const orderShopList = (methodData.payload.payload.orderShopList as string);
              // local_order字段，用于下单页通过该字段获取内容 orderShopList
              const value = (methodData.payload.payload.local_order as string);
              sessionStorage.setItem(value, orderShopList);
            }
            router.navigate(`${methodData.payload.url}?${_search}`, {
              replace: true
            });
          } else {
            router.navigate(methodData.payload.url, {
              replace: true
            });
          }
        } catch (ex) {}
      }
    });
    return removeListen;
  }, []);
};