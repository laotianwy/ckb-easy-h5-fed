/*
 * @Author: yusha
 * @Date: 2023-10-16 19:52:06
 * @LastEditors: yusha
 * @LastEditTime: 2023-10-16 21:14:39
 * @Description:  copy antd mobile来的方法
 */
export function attachPropertiesToComponent<C, P extends Record<string, any>>(
	component: C,
	properties: P
): C & P {
	const ret = component as any;
	for (const key in properties) {
		// eslint-disable-next-line no-prototype-builtins
		if (properties.hasOwnProperty(key)) {
			ret[key] = properties[key];
		}
	}
	return ret;
}
