/*
 * @Author: shiguang
 * @Date: 2023-10-22 18:52:31
 * @LastEditors: shiguang
 * @LastEditTime: 2023-10-22 18:52:35
 * @Description:
 */
/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        extend: {}
    },
    plugins: []
};
