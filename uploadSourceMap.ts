// sourceMap上传 https://help.aliyun.com/zh/arms/developer-reference/api-arms-2019-08-08-upload

const $ARMS20190808 = require('@alicloud/arms20190808');
const ARMS20190808 = $ARMS20190808.default;
const $OpenApi = require('@alicloud/openapi-client');
const $Util = require('@alicloud/tea-util');
const $tea = require('@alicloud/tea-typescript');
const fs = require('fs/promises');
const webpack = require('webpack');
const { nextTick } = require('process');

class Client {
	/**
	 * 使用AK&SK初始化账号Client，上传sourceMap 配置
	 * @param accessKeyId
	 * @param accessKeySecret
	 * @return Client
	 * @throws Exception
	 */
	static createClient() {
		const config = new $OpenApi.Config({
			// 必填，您的 AccessKey ID
			accessKeyId: 'LTAI5tEG7U4xApC8mq9rGHxf',
			// 必填，您的 AccessKey Secret
			accessKeySecret: 'aSwm8hLFVET6EwJrbs1rTgCrDFkXcJ'
		});
		// 访问的域名
		config.endpoint = 'arms.cn-hangzhou.aliyuncs.com';
		return new ARMS20190808(config);
	}
}

const config = {
	regionId: 'cn-hangzhou',
	pid: process.env.REACT_APP_ARMS_PID,
	edition: '1.0.0'
};

console.log(config);

const main = async (path = './dist/js/') => {
	// 工程代码泄露可能会导致AccessKey泄露，并威胁账号下所有资源的安全性。
	const client = Client.createClient();
	const runtime = new $Util.RuntimeOptions({});
	// 先删除对应版本的原先sourceMap文件
	const getSourceMapInfoRequest = new $ARMS20190808.GetSourceMapInfoRequest({
		regionId: config.regionId,
		ID: config.pid + '/'
	});
	const result = await client.getSourceMapInfoWithOptions(
		getSourceMapInfoRequest,
		runtime
	);
	//
	const deleteSourceMapRequest = new $ARMS20190808.DeleteSourceMapRequest({
		...config,
		// 测试版本和生产版本
		fidList: result.body.sourceMapList
			.filter((v) => v.version === config.edition)
			.map((v) => v.fid)
	});
	await client.deleteSourceMapWithOptions(deleteSourceMapRequest, runtime);
	//
	const sourceMaps = await fs.readdir(path);

	sourceMaps
		.filter((v) => /.map$/.test(v))
		.reduce((p, v) => {
			return p.then(async () => {
				try {
					const buf = await fs.readFile(`${path}/${v}`);
					const file = buf.toString();
					const uploadRequest = new $ARMS20190808.UploadRequest({
						...config,
						fileName: v,
						file
					});
					await client.uploadWithOptions(uploadRequest, runtime);
					// 成功后删除文件
					console.log('success deleted');
				} catch (error) {
					console.log(v);
					// 如有需要，请打印 error
					console.log(error);
				}
			});
		}, Promise.resolve());
};

// webpack打包进度监听，完成后上传sourceMap
module.exports = (path) =>
	new webpack.ProgressPlugin(function (percentage) {
		if (percentage === 1) {
			if (
				process.env.NODE_ENV === 'production' &&
				process.env.REACT_APP_ARMS_PID
			) {
				nextTick(() => {
					console.log(
						'******************* uploadSourceMap *******************'
					);
					main(path);
				});
			}
		}
	});
