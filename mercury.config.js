/*
 * @Author: shiguang
 * @Date: 2023-07-04 18:34:24
 * @LastEditors: yusha
 * @LastEditTime: 2023-10-24 15:09:18
 * @Description: x
 */
const path = require('path');
const { defineMercuryConfig } = require('@sniff/mercury-cli/src/core');

const getExtractConfig = () => {
	const extractConfig = require('./src/i18n/locales/extractConfig.json');
	const { common, pages } = extractConfig;

	const wrapConfig = (conf) => {
		return conf.map((item) => {
			return {
				...item,
				relativePath: item.path,
				path: path.join(__dirname, item.path)
			};
		});
	};
	return {
		pages: wrapConfig(pages),
		common: wrapConfig(common)
	};
};
module.exports = defineMercuryConfig({
	extractConfig: getExtractConfig(),
	i18nMethod: 'window._$m.t',
	locales: ['ja_JP'],
	ignoreMethods: ['t'],
	exclude: ['**/icons/*'],
	generatorStaticDirName: `${path.join(__dirname, './src/i18n/locales')}`
});
