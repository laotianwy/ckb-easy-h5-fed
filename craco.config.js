/*
 * @Author: shiguang
 * @Date: 2023-04-03 17:04:41
 * @LastEditTime: 2024-05-08 15:55:19
 * @LastEditors: yusha
 * @Description: 覆盖 cra 配置
 */
const path = require('path');
const CracoCSSModules = require('craco-css-modules');
const { ProjectNameWebpackPlugin } = require('@snifftest/mercury-plugin');
const createProcess = require('@snifftest/sniff-process-env');
const pkg = require('./package.json');

createProcess.createReactProcess();

// TODO:可以移入CICD中
process.env.REACT_APP_ARMS_PID = 'exbrfir0yx@add6d328fdd5d2c';
const uploadSMapPlugin = require('./uploadSourceMap.ts');
const isProdBucket = () => {
	return ['pre', 'prod', 'prod2'].includes(process.env.SNIFF_BUILD_ENV);
};
const getPublicPath = () => {
	// const isTestBucket = () => {
	//     return ['dev', 'test01', 'test02', 'test03', 'test04'].includes(process.env.SNIFF_BUILD_ENV);
	// };
	// 线上和预发环境规则
	const getProdPublicUrl = (env, projectName) => {
		return `https://static-jp.theckb.com/${env}-${pkg.name}/present/`;
	};
	// 测试环境规则
	const getTestPublicUrl = (env, projectName) => {
		return `https://test-static.theckb.com/${env}-${pkg.name}/present/`;
	};
	const SNIFF_BUILD_ENV = process.env.SNIFF_BUILD_ENV;
	console.log('SNIFF_BUILD_ENV：', SNIFF_BUILD_ENV);
	if (SNIFF_BUILD_ENV) {
		// if(isTestBucket()){
		//     return getTestPublicUrl(SNIFF_BUILD_ENV, 'oem');
		// }
		if (isProdBucket()) {
			return getProdPublicUrl(SNIFF_BUILD_ENV);
		}
		return getTestPublicUrl(SNIFF_BUILD_ENV);
	}
};
// 全局化文件根目录
process.env.REACT_APP_PUBLIC_PATH = getPublicPath();
const config = {
	webpack: {
		plugins: {
			// build下生成版本信息
			add:
				process.env.NODE_ENV === 'production'
					? [
							// new ProjectNameWebpackPlugin(
							// 	process.env.REACT_APP_ENV
							// ),
							uploadSMapPlugin('./build/static/js')
						]
					: []
		},
		alias: {
			'@nutui/nutui-react-native/components/swiper':
				'@nutui/nutui-react-native/lib/module/swiper',
			'@nutui/nutui-react-native/components/swiperitem':
				'@nutui/nutui-react-native/lib/module/swiperitem',
			'@/utils/TDAnalytics': '@/empty',
			'@snifftest/react-native-thinking-data': '@/empty',
			'@/atom/atomTDAnalyticsStatus': '@/empty',
			'@/outs/hooks/base/useNavigation': '@/empty',
			'@/hooks/base/useNavigation': '@/empty',
			'@/utils/tool/AsyncStorage': '@/empty',
			'@/components/Mask': '@/empty',
			'react-native-root-siblings': '@/empty',
			'react-native-code-push': '@/empty',
			'react-native-linear-gradient': '@/empty',
			'react-native-tenjin': '@/empty',
			'@/outs/hooks/common/useNavigationWebViewH5': '@/empty',
			'@/outs/hooks/common/useNavWebViewH5': '@/empty',
			'@/common/RootNavigation': '@/empty',
			'@/container/user/WebviewBridgeH5/atom/atomWebview': '@/empty',
			i18next: '@/empty',
			'react-i18next': '@/empty',
			'@/outs/components/CodePush': '@/empty',
			'react-native-fs': '@/empty',
			'@tamagui/font-size': '@snifftest/tamagui-font-size',
			'@react-native-async-storage/async-storage': '@/empty',
			'@env': '@/empty',
			'react-native-radial-gradient': '@/empty',
			'@react-native-clipboard/clipboard': '@/empty',
			'@sniff/teaset/components/Toast/Toast': '@/empty',
			'@sniff/react-native-i18n': '@/empty',
			'@sniff/react-native-fast-image': '@/empty',
			'@react-native-camera-roll/camera-roll': '@/empty',
			'react-native-qrcode-svg': '@/empty',
			'react-native-view-shot': '@/empty',
			'react-native-safe-area-context': '@/empty',
			'react-native': '@/empty',
			'react-native-share': '@/empty',
			'react-native-fbsdk-next': '@/empty',
			'react-native-fs': '@/empty',
			'@': path.resolve(__dirname, 'src/')
		},
		module: {
			rules: [
				// 规则，在写style.module.scss的时候发现引入后缀为.scss会报错，在这里配置一下即可
				{
					test: /.scss$/,
					loaders: ['style-loader', 'css-loader', 'sass-loader']
				}
			]
		},
		configure: (webpackConfig, { env, paths }) => {
			if (
				webpackConfig &&
				webpackConfig.output &&
				env !== 'development'
			) {
				// 主要是为了 cdn 域名转发的时候路径处理，觉得不应该侵入到打包过程中，因为开发者不应该知道转发路径到底是什么
				// 而且后期所有的 文件 emit 都应该丢到 oem 中，配置 plugin 的时候需要注意!!!
				// client-pre/present/logo512.png
				const publicPath = getPublicPath();
				if (publicPath) {
					webpackConfig.output.publicPath = publicPath;
					// 'https://page-client-test.theckb.com/test02-oemclient/present';
					// publicPath;
				}
			}

			return webpackConfig;
		}
	},
	devServer: {
		client: {
			overlay: false
		}
	},
	plugins: [
		{
			plugin: CracoCSSModules
		}
	]
};

module.exports = config;
