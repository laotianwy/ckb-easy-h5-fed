/*
 * @Author: shiguang
 * @Date: 2023-04-25 14:34:51
 * @LastEditors: huajian
 * @LastEditTime: 2024-03-07 15:27:52
 * @Description: eslint 配置
 */
module.exports = {
	env: {
		/** 设置环境，防止全局变量不会找不到 */
		browser: true,
		commonjs: true,
		es6: true,
		jest: true,
		node: true
	},
	extends: [require.resolve('@snifftest/eslint-config')],
	rules: {
		'no-debugger': 1,
		'no-dupe-keys': 1
	},
	ignorePatterns: ['src/common/icons', 'src/out'],
	globals: {
		// 这里填入你的项目需要的全局变量
		// 这里值为 false 表示这个全局变量不允许被重新赋值，比如：
		// Vue: false
	}
};
