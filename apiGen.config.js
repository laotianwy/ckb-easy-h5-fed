/*
 * @Author: shiguang
 * @Date: 2023-11-22 13:46:40
 * @LastEditors: yusha
 * @LastEditTime: 2024-05-16 16:21:31
 * @Description:
 */

/** @type {import("@snifftest/api-gen/src/generate").ApiGenConfig['swaggerList']} */
const swaggerList = [
	{
		title: '用户',
		name: 'customer',
		url: 'https://master-gateway.theckb.com/customer/v2/api-docs'
	},
	{
		title: '商城',
		name: 'market',
		url: 'https://master-gateway.theckb.com/market/v2/api-docs',
		apis: {
			get: [
				/** 单张用户优惠券详情 */
				'/market/coupon/customer/detail'
			],
			post: [
				'/market/coupon/couponCustomerPage',
				'/market/coupon/couponProductPage',
				'/market/coupon/product/search/page',
				/** 顺手捎商品列表 */
				'/market/handyActivity/activityProductList'
			]
		}
	},
	{
		title: '支付',
		name: 'pay',
		url: 'https://master-gateway.theckb.com/pay/v2/api-docs',
		apis: {
			post: [
				/** 我的钱包-余额充值申请 */
				'/pay/front/account/recharge',
				/** 我的钱包-钱包流水查询 */
				'/pay/front/account/query/log/page',
				/** 去支付，返回 paypal 的 orderID */
				'/pay/front/pay',
				/** paypal捕获 支付完成后的回调 */
				'/pay/front/pay/capture',
				/** 余额充值记录查询 */
				'/pay/front/account/query/recharge/page',
				/** 余额充值申请书下载(充值申请页面) */
				'/pay/front/account/query/recharge/request/download'
			],
			get: [
				/** 支付-支付方式拉取 */
				'/pay/front/pay/query/type',
				/** 我的钱包-钱包信息查询 */
				'/pay/front/account/query',
				// '/pay/front/pay/query/clientId',
				/** 获取PayPal 的 clientID */
				'/pay/front/paypal/clientId',
				'/pay/front/paypal/token'
			]
		}
	},
	{
		title: '直营商城',
		name: 'easyOrder',
		url: 'https://master-gateway.theckb.com/easy/order/v2/api-docs',
		apis: {
			post: [
				/** 删除下单人地址 */
				'/easy/order/userPlaceOrderAddress/delete',
				/** 购物车算价 */
				'/easy/order/cart/calculatePrice',
				/** 删除购物车 */
				'/easy/order/cart/delete',
				/** 修改购物车 */
				'/easy/order/cart/update',
				/** 购物车列表 */
				'/easy/order/cart/list',
				/** 查询下单人列表 */
				'/easy/order/userPlaceOrderAddress/list',
				/** 新增、编辑下单人信息 */
				'/easy/order/userPlaceOrderAddress/save',
				/** 删除收货人地址 */
				'/easy/order/userReceivingAddress/delete',
				/** 设置为默认地址收货人地址 */
				'/easy/order/userReceivingAddress/setDefault',
				/** 查询收货人地址列表 */
				'/easy/order/userReceivingAddress/list',
				/** 新增、编辑收货人地址 */
				'/easy/order/userReceivingAddress/save',
				/** 购物车列表 */
				'/easy/order/cart/add',
				/** 提单页算价（） */
				'/easy/order/order/calculatePrice',
				/** 创建订单 */
				'/easy/order/order/create',
				/** 订单详情 */
				'/easy/order/orderSearch/queryDetailByOrderNo',
				/** 根据id查询收货人地址 */
				'/easy/order/userReceivingAddress/findById',
				/** 根据id查询下单人信息 */
				'/easy/order/userPlaceOrderAddress/findById',
				/** 查询工作台订单数量统计 */
				'/easy/order/orderSearch/queryOrderCount',
				/** 修改购物车选中状态 */
				'/easy/order/cart/updateSelected',
				/** 获取包邮信息 */
				'/easy/order/userReceivingAddress/findDefaultFreeShipping',
				/** 查询订单列表 */
				'/easy/order/orderSearch/list',
				/** 查询详情获取 */
				'/easy/order/orderSearch/queryDetailByOrderNo',
				// 取消订单
				'/easy/order/order/close',
				/** 订单取消弹框 */
				'/easy/order/orderSearch/orderCancelPopups',
				/** 订单取消弹框确认 */
				'/easy/order/orderSearch/orderCancelPopupsConfirm',
				/** 退款列表查询 */
				'/easy/order/refundSearch/queryRefundPage',
				/** 退款详情查询 */
				'/easy/order/refundSearch/queryRefundDeatail',
				/** 查询订单详情 增加退款金额 */
				'/easy/order/orderSearch/queryDetailByOrderNo',
				'/easy/order/orderSearch/queryStatusAndCouponByOrderNo',
				'/easy/order/orderSearch/orderCancelPopupsConfirm',
				/** 顺手捎商品列表 */
				'/easy/order/order/handyActivity/page'
			],
			get: ['/easy/order/Probe/test']
		}
	},
	{
		title: '直营商城',
		name: 'easyGoods',
		url: 'https://master-gateway.theckb.com/easy/goods/v2/api-docs',
		apis: {
			post: [
				/** 分类 */
				'/easy/goods/productCategoryFrontend/tree',
				/** 关键词搜索 */
				'/easy/goods/product/search/keyword',
				/** 商品详情 */
				'/easy/goods/product/detail',
				/** 图搜列表 */
				'/easy/goods/product/search/image',
				/** 图搜上传图片 */
				'/easy/goods/product/search/image/upload',
				/** 收藏分页 */
				'/easy/goods/favoriteProduct/page',
				/** 商品收藏 */
				'/easy/goods/favoriteProduct/add',
				/** 收藏取消 */
				'/easy/goods/favoriteProduct/cancel',
				/** 删除搜索历史 */
				'/easy/goods/product/search/keyword/history/delete',
				/** 商品同步接口 */
				'/easy/goods/product/search/check/sync',
				/** 键词搜索代采商品 */
				'/easy/goods/product/search/keyword/agent',
				/** 客户端-客户端活动类目 */
				'/easy/goods/productCategoryFrontend/activity/tree',
				/** 临时mock接口 勿删 */
				'/easy/goods/test/test1',
				// 每周上新
				'/easy/goods/product/search/newProduct'
			],
			get: [
				/** 热门分类 */
				'/easy/goods/productCategoryFrontend/hot',
				/** 搜索历史 */
				'/easy/goods/product/search/keyword/history',
				/** 热词搜索 */
				'/easy/goods/product/search/keyword/hot',
				'/easy/goods/productCategoryFrontend/hot'
			]
		}
	},
	{
		title: '首页营销',
		name: 'easyMarket',
		url: 'https://master-gateway.theckb.com/market/v2/api-docs',
		apis: {
			get: [
				/** 获取用户显示的banner(前台) */
				'/market/banner/show',
				/** 获取用户显示的弹窗(前台) */
				'/market/popup/show',
				/** 推荐商品 */
				'/market/recommend/show',
				/** 活动专区 爆款火拼之类 */
				'/market/activity/area/list',
				/** 营销运费模板 */
				'/market/freight/template/show',
				/** 活动价频道详情 */
				'/market/front/bar/detail',
				/** 活动价板块列表(底部4个) */
				'/market/front/bar/list',
				'/market/front/bar/list/v1',
				/** 前台-推荐热词 */
				'/market/hot/key/front/list',
				/** 活动商品-搜索框 */
				'/market/front/bar/activity',
				/** 新的底部tab */
				'/market/front/bar/list/v1',
				/** 客户端: 活动可获得优惠券查询(未登录下) */
				'/market/coupon/scene',
				/** 客户端: 新人注册可获得的最优惠的优惠券 */
				'/market/coupon/scene/best',
				// 查询特辑
				'/market/collection/qryPersonageInfo',
				'/market/front/bar/list/v2',
				/** 删除收藏 */
				'/market/collection/removeCollectionList'
			],
			post: [
				/** 爆款火拼之类的商品列表 */
				'/market/activity/area/moreProduct',
				/** 客户端：当前商品符合使用条件的的优惠券 */
				'/market/coupon/couponProductPage',
				/** 客户端：查看当前用户已获得的活动优惠券(新用户或者首次下单) */
				'/market/coupon/getCouponCustomerBySenceType',
				'/market/collection/qrySpeciallyCollectonsPage',
				'/market/collection/removeCollectionList',
				/** 客户端：当前用户的优惠卷 */
				'/market/coupon/couponCustomerPage',
				// 客户端首页接口
				'/market/collection/homePageModulesList',
				// 收藏特辑
				'/market/collection/addActivityAreaById',
				// 百元专区
				'/market/collection/qryHundredModulesList',
				// 不喜欢
				'/market/collection/dislikeActivityAreaById',
				/** 自定义列表明细 */
				'/market/collection/qrySelfDefinedModulesList',
				/** 特集一站式和生活用品列表明细 */
				'/market/collection/qrySpecialModulesList',
				/** 查询我的收藏 */
				'/market/collection/qrySpeciallyCollectonsPage',
				/** 批量删除我的收藏 */
				'/market/collection/removeCollectionList',
				'/martet/handyActivity/feeList',
				/** 营销运费模板(新接口) */
				'/market/freight/template/showInfo'
			]
		}
	},
	{
		title: '商品',
		name: 'goods',
		url: 'https://master-gateway.theckb.com/goods/v2/api-docs'
	}
];

/** @type {import("@snifftest/api-gen/src/generate").ApiGenConfig} */
const config = {
	swaggerList,
	requestEnv: '20240401-hunderd-fee'
};

module.exports = config;
