/*
 * @Author: shiguang
 * @Date: 2023-08-28 13:12:04
 * @LastEditors: yusha
 * @LastEditTime: 2024-03-27 10:09:34
 * @Description: prettier
 */

/** @type {import("prettier").Config} */
const config = {
    endOfLine: 'auto',
    trailingComma: 'none',
    tabWidth: 4,
    useTabs: true,
    semi: true,
    singleQuote: true,
}
module.exports = config;